# FindGoogleCpuProfiler
# =====================
#
# Locate and configure Google CPU Profiler
#
# Interface Targets
# -----------------
#   GoogleCpuProfiler::GoogleCpuProfiler
#
# Variables
# ---------
#   GoogleCpuProfiler_FOUND
#   GoogleCpuProfiler_INCLUDE_DIRS
#   GoogleCpuProfiler_LIBRARIES

find_path(GoogleCpuProfiler_INCLUDE_DIR
          NAMES gperftools/profiler.h)

find_library(GoogleCpuProfiler_LIBRARY
             NAMES profiler)

set(GoogleCpuProfiler_INCLUDE_DIRS ${GoogleCpuProfiler_INCLUDE_DIR})
set(GoogleCpuProfiler_LIBRARIES ${GoogleCpuProfiler_LIBRARY})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(GoogleCpuProfiler
                                  FOUND_VAR GoogleCpuProfiler_FOUND
                                  REQUIRED_VARS GoogleCpuProfiler_INCLUDE_DIR GoogleCpuProfiler_LIBRARY)

include(CreateImportTargetHelpers)
generate_import_target(GoogleCpuProfiler STATIC)
