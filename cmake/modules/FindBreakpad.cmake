# FindBreakpad
# ============
#
# Locate and configure Breakpad
#
# Interface Targets
# -----------------
#   Breakpad::Breakpad
#
# Variables
# ---------
#   Breakpad_FOUND
#   Breakpad_INCLUDE_DIRS
#   Breakpad_LIBRARIES

find_path(Breakpad_INCLUDE_DIR
          NAMES google_breakpad/common/minidump_format.h
          PATH_SUFFIXES breakpad)

find_library(Breakpad_LIBRARY
             NAMES breakpad_client libbreakpad_client)

set(Breakpad_INCLUDE_DIRS ${Breakpad_INCLUDE_DIR})
set(Breakpad_LIBRARIES ${Breakpad_LIBRARY})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Breakpad
                                  FOUND_VAR Breakpad_FOUND
                                  REQUIRED_VARS Breakpad_INCLUDE_DIR Breakpad_LIBRARY)

set(Breakpad_INTERFACE_LIBS "pthread")

include(CreateImportTargetHelpers)
generate_import_target(Breakpad STATIC)
