**** BAPLIE IMPORT TEST ****

Load the sto file, and import the baplie. Do the mapping as follows
 Map DKCPH (load port) -> Before
 Map DKCPH (discharge port) -> After
 Map GBSOT (load port) -> GBSOU
 Map DEBRE (discharge) -> DEBRV

After the import, the following should be observed.

Baplie edifact test:

1. New container in EDIFACT. Should appear in position 34,01,82 as NEW000000000, being a 40-foot highcube.

2. Replace. Container  FAKE4579 @ 37,01,82 should change isocode 22G1->22G0.

3. Shift. Container FAKE21749 should be shifted from 50,02,82 (in Befor) to 50,02,02 in GBSOU and then left there to SGSIN

4. Stow (was never onboard). FAKE45710, which was not onboard, should now be stowed in 37,02,82 GBSOU->CNSHA

5. Discharge earlier: FAKE4578@37,03,82 should now be discharged in SGSIN instead of CNSHA

6. Discharge later: FAKE45725@37,05,82 should now be discharge in After instead of CNSHA

7- Swap places: FAKE21748@37,03,02 should move to 37,05,02 and FAKE21747@37,05,02 should move to 37,07,02

8. Discharged: FAKE21746@46,01,82 should now be discharge at GBSOU. Also, a NEW000000001 should be loaded for SGSIN at the same spot in GBSOU

9. Mapped: NEW000000002@35,01,02 should be loaded at GBSOU and discharged DEBRV

10. Detect shift at earlier port: FAKE21745 should be loaded@50,10,82 in before, shifted at DKAAR to 50,10,84 and left there to SGSIN, while NEW000000003 should be loaded at DKAAR in 50,10,82
