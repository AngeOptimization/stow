#include <QRegExp>
#include <QFile>
#include <QTextStream>
#include <QString>
#include <QStringList>
#include <iostream>
#include <QDebug>
#include <Eigen/Core>

/**
 A small app to read bonjean data contained in the following format:
        FRAME  -7.0      FRAME  -6.0      FRAME  -5.0      FRAME  -4.0      FRAME  -3.0      FRAME  -2.0
T      AREA   MOM BL    AREA   MOM BL    AREA   MOM BL    AREA   MOM BL    AREA   MOM BL    AREA   MOM BL   T
3.00    0.00     0.00    0.64     1.17    1.23     2.20    1.81     3.21    2.39     4.21    2.95     5.18  3.00
3.20    0.00     0.00    0.70     1.36    1.34     2.54    1.97     3.69    2.59     4.83    3.19     5.94  3.20
3.40    0.00     0.00    0.77     1.57    1.45     2.91    2.13     4.21    2.79     5.50    3.44     6.75  3.40
3.60    0.00     0.00    0.83     1.79    1.57     3.30    2.28     4.76    3.00     6.21    3.68     7.61  3.60
3.80    0.00     0.00    0.90     2.04    1.68     3.72    2.44     5.35    3.20     6.96    3.93     8.54  3.80
4.00    0.00     0.00    0.96     2.30    1.80     4.17    2.60     5.98    3.41     7.77    4.18     9.51  4.00
etc.
 */
struct BonjeanDataPoint {
    double m_draft;
    double m_area;
    double m_mom_bl;
};

typedef QList<BonjeanDataPoint> BonjeanFrame;

const QString regexp_double("([+-]?[0-9]+(\\.[0-9]+)?)");


bool is_frame_line(const QString& text_line) {
    QRegExp rx_is_frame("\\s\\sFRAME\\s");
    return rx_is_frame.indexIn(text_line, 0) != -1;
}

bool is_double_line(const QString& text_line) {
    QRegExp rx_is_frame(QString("^\\s*") + regexp_double);
    return rx_is_frame.indexIn(text_line, 0) != -1;
}

/**
 * reads a section of bonjean frames (typically 6)
 */
QMap<int, BonjeanFrame> read_bonjean_frames_section(QTextStream* text_stream, const QList<int> frame_ids){
    QMap<int, BonjeanFrame> rv;
    Q_FOREACH(const int frame_id, frame_ids) {
        rv.insert(frame_id, BonjeanFrame());
    }
    QString current_line = text_stream->readLine();
    while(is_double_line(current_line)) {
        QStringList raw_frame_values = current_line.split(QRegExp("\\s+"));
        raw_frame_values.removeFirst(); //remove first bogus empty string
        QStringList::const_iterator it = raw_frame_values.constBegin();
        const double draft = it->toDouble();
        ++it;
        for(int i = 0; i < rv.size(); ++i){
            BonjeanDataPoint bonjean_data_point;
            bonjean_data_point.m_draft = draft;
            bonjean_data_point.m_area = it->toDouble();
            it++;
            bonjean_data_point.m_mom_bl = it->toDouble();
            it++;
            rv[frame_ids.at(i)].append(bonjean_data_point);
        }
        current_line = text_stream->readLine();
    }
    return rv;
}

QList<int> read_frame_ids(const QString& text_line){
    QStringList frame_ids_as_strings = text_line.split(QRegExp("FRAME"));
    frame_ids_as_strings.removeFirst();//remove the spaces preceding the first occurence of "FRAME"
    QList<int> rv;
    Q_FOREACH(QString frame_id_as_string, frame_ids_as_strings){
        rv.append(qRound(frame_id_as_string.toDouble()));
    }
    return rv;
}

int main(const int argc, const char** argv ) {
    if(argc != 2){
        qFatal("Usage: bonjean_parser_data bonjean_data_file.txt");
        return -1;
    }
    const char* input_file_name = argv[1];
    QFile file(input_file_name);
    if (!file.open(QFile::ReadOnly)) {
        qFatal(qPrintable(QString("Unable to open input file %1").arg(input_file_name)));
        return -1;
    };
    QTextStream text_stream(&file);
    QMap<int, BonjeanFrame> bonjean_frames;
    while(!text_stream.atEnd()) {
        QString next_line = text_stream.readLine();
        QList<int> frame_ids;
        if(is_frame_line(next_line)){
            QList<int> frame_ids = read_frame_ids(next_line);
            text_stream.readLine(); //skip column headers
            bonjean_frames.unite(read_bonjean_frames_section(&text_stream, frame_ids));
        }
    }
    //The vessel converter seems not to be able to handle more than 256 columns
    //so we will reduce the number of bonjean columns by a factor of 2
    //by simply eliminating every second column
    //Note: make sure that the number of columns in the original table is even.
    //otherwise implement adding an extra 0-column here
    const int sampling_rate = 1;
    Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> bonjean_table;
    bonjean_table.resize(bonjean_frames.value(0).size() + 1 , bonjean_frames.size()/sampling_rate + 1);
    for(int i = 0; i < bonjean_frames.values().at(0).size(); ++i){
        for(int j = 0; j < bonjean_frames.keys().size()/sampling_rate; ++j){
            //FIXME: we are implicitly assuming that all the values are well ordered. Not sure that is always true
            bonjean_table(i+1, j+1) = bonjean_frames.values().at(sampling_rate*j).at(i).m_area;
        }
    }
    for(int j = 0; j < bonjean_frames.keys().size()/sampling_rate; ++j){
        bonjean_table(0, j+1) = bonjean_frames.keys().at(sampling_rate*j);
    }
    for(int i = 0; i < bonjean_frames.values().at(0).size(); ++i){
        bonjean_table(i+1, 0) = bonjean_frames.values().at(0).at(i).m_draft;
    }
    std::cout << bonjean_table;
}
