#include <QRegExp>
#include <QFile>
#include <QTextStream>
#include <QString>
#include <QStringList>
#include <iostream>
#include <QDebug>
#include <Eigen/Core>

/**
 A small app to read tank data in the following format:
        COMPARTMENT   : NO.2 W.B.T     (P)

        EXTREME POINT : AFT END AT FRAME  319.0
                        FWD END AT FRAME  347.0

        --------------------------------------------------------------------------------------
            LEVEL FROM         VOL     FILL        LCG        TCG       VCG        IY      AWP
          B.L.   BOTTOM
             m        m         m3        %          m          m         m        m4       m2
        --------------------------------------------------------------------------------------
         0.000    0.000        0.0      0.0
         0.500    0.500       55.3      2.6    272.660      2.902      0.279      657      145
         1.000    1.000      136.8      6.4    272.734      3.551      0.565     1226      184
 */

struct TankDataPoint {
    double m_volume;
    double m_lcg;
    double m_vcg;
    double m_tcg;
    double m_fsm;
};

class Tank {
public:
    QString m_description;
    double m_aft_end;
    double m_fwd_end;
    QList<TankDataPoint> m_data_points;
    QString print() const;
    QString print_brief() const;
};

const QString regexp_double("([+-]?[0-9]+(\\.[0-9]+)?)");

bool is_compartment_line(const QString& text_line) {
    return text_line.contains("COMPARTMENT");;
}

bool is_double_line(const QString& text_line) {
    QRegExp rx(QString("^\\s+") + regexp_double);
    return rx.indexIn(text_line, 0) != -1;
}

bool is_extreme_point_line(const QString& text_line) {
    return text_line.contains("END AT FRAME");
}

// double frame_to_longitudinal_pos(const double frame_id) {
//     return 
// }

TankDataPoint read_tank_data_line(const QString& current_line){
    QStringList tank_data_values = current_line.split(QRegExp("\\s+"));
    tank_data_values.removeFirst(); //remove first bogus empty string
    TankDataPoint tank_data_point;
    tank_data_point.m_volume = tank_data_values.at(2).toDouble();
    tank_data_point.m_lcg = tank_data_values.at(4).toDouble();
    tank_data_point.m_tcg = tank_data_values.at(5).toDouble();
    tank_data_point.m_vcg = tank_data_values.at(6).toDouble();
    tank_data_point.m_fsm = tank_data_values.at(7).toDouble();
    return tank_data_point;
}

double read_limit(const QString& text_line){
    QRegExp rx(regexp_double);
    rx.indexIn(text_line);
    return rx.cap(0).toDouble();
}

QString read_compartment_name(const QString& current_line) {
    QRegExp rx(QString("COMPARTMENT\\s+:\\s+(.*)\\s*$"));
    rx.indexIn(current_line, 0);
    QStringList texts = rx.capturedTexts();
    return rx.cap(1);
}

int main(const int argc, const char** argv ) {
    if(argc != 2){
        qFatal("Usage: bonjean_parser_data bonjean_data_file.txt");
        return -1;
    }
    const char* input_file_name = argv[1];
    QFile file(input_file_name);
    if (!file.open(QFile::ReadOnly)) {
        qFatal(qPrintable(QString("Unable to open input file %1").arg(input_file_name)));
        return -1;
    };
    QTextStream text_stream(&file);
    QList<Tank> tanks;
    while(!text_stream.atEnd()) {
        QString next_line = text_stream.readLine();
        if(is_compartment_line(next_line)){
            tanks.append(Tank());
            tanks.last().m_description = read_compartment_name(next_line);
            text_stream.readLine();//skip empty line
            tanks.last().m_aft_end = read_limit(text_stream.readLine());
            tanks.last().m_fwd_end = read_limit(text_stream.readLine());
            for(int i= 0; i<7; ++i) { text_stream.readLine();} //skip 7 lines
            next_line = text_stream.readLine();
            while(is_double_line(next_line)){
                tanks.last().m_data_points.append(read_tank_data_line(next_line));
                next_line = text_stream.readLine();
            }

        }
    }
    Q_FOREACH(Tank tank, tanks){
        std::cout << tank.print_brief().toStdString();
    }
    std::cout << std::endl << std::endl << std::endl;
    Q_FOREACH(Tank tank, tanks){
        std::cout << tank.print().toStdString();
    }

}

QString Tank::print() const
{
    QString description = "Description\t" + m_description;
    QString volume_string = "Volume in m3";
    QString lcg_string = "LCG in m";
    QString vcg_string = "VCG in m";
    QString tcg_string = "TCG in m";
    QString fsm_string = "Max FSM in m4";
    Q_FOREACH(const TankDataPoint data_point, m_data_points){
        volume_string += "\t" + QString::number(data_point.m_volume);
        lcg_string += "\t" + QString::number(data_point.m_lcg);
        vcg_string += "\t" + QString::number(data_point.m_vcg);
        tcg_string += "\t" + QString::number(data_point.m_tcg);
        fsm_string += "\t" + QString::number(data_point.m_fsm);
    }
    return description + "\n" + volume_string + "\n" + lcg_string + "\n" +vcg_string + "\n" +tcg_string + "\n" +fsm_string + "\n\n";
}

QString Tank::print_brief() const
{
    const TankDataPoint& data_point = m_data_points.last();
    return m_description + "\t" + QString::number(data_point.m_volume) + "\t" + "\t" + "\t"
                         + QString::number(m_fwd_end)  + "\t" + QString::number(m_aft_end) + "\t"
                         + QString::number(data_point.m_lcg)  + "\t" + QString::number(data_point.m_vcg) + "\t"
                         + QString::number(data_point.m_tcg) + "\t" + QString::number(data_point.m_fsm) + "\n";
}
