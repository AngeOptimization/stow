#include <QRegExp>
#include <QFile>
#include <QTextStream>
#include <QString>
#include <QStringList>
#include <iostream>
#include <QDebug>
#include <Eigen/Core>

/**
 A small app to read bonjean data contained in the following format:
    Calculated for trim=-1
    --------------------------------------------------------------------------------------------------------
      DRAFT     VOLM     DISP     LCB    VCB     LCF    KMT    MTC   TPC     WSA     CB     CW     CP     CM
          m       m3        t       m      m       m      m  tm/cm  t/cm      m2
    --------------------------------------------------------------------------------------------------------
      2.000  15868.3  16462.4 180.561  1.051 175.733 75.913 1226.4  94.9  9646.2 0.4758 0.5477 0.5161 0.9219
      2.100  16796.5  17416.1 180.295  1.104 175.675 73.080 1248.5  95.8  9760.2 0.4794 0.5527 0.5187 0.9242
      2.200  17730.4  18378.5 180.052  1.158 175.600 70.489 1269.0  96.6  9872.9 0.4828 0.5572 0.5212 0.9263
      2.300  18675.7  19346.7 179.810  1.209 175.446 68.110 1285.3  97.4  9989.5 0.4862 0.5619 0.5236 0.9285
      2.400  19628.7  20325.8 179.609  1.264 175.387 65.910 1304.9  98.2 10098.5 0.4895 0.5663 0.5261 0.9305
 */

struct HydrostaticsDataPoint {
    double m_draft;
    double m_displacement;
    double m_LCB;
    double m_KMT;
    double m_MTC;
};

const QString regexp_double("([+-]?[0-9]+(\\.[0-9]+)?)");

bool is_trim_line(const QString& text_line) {
    QRegExp rx_is_frame("trim=" + regexp_double);
    return rx_is_frame.indexIn(text_line, 0) != -1;
}

bool is_double_line(const QString& text_line) {
    QRegExp rx_is_frame(QString("^\\s+") + regexp_double);
    return rx_is_frame.indexIn(text_line, 0) != -1;
}

bool is_header_line(const QString& text_line) {
    QRegExp rx_is_frame(QString("^\\d+"));
    return rx_is_frame.indexIn(text_line, 0) != -1;
}

HydrostaticsDataPoint read_hydrostatics_line(const QString& current_line){
    QStringList hydrostatic_values = current_line.split(QRegExp("\\s+"));
    hydrostatic_values.removeFirst(); //remove first bogus empty string
    HydrostaticsDataPoint hydrostatics_data_point;
    hydrostatics_data_point.m_draft = hydrostatic_values.at(0).toDouble();
    hydrostatics_data_point.m_displacement = hydrostatic_values.at(2).toDouble();
    hydrostatics_data_point.m_LCB = hydrostatic_values.at(3).toDouble();
    hydrostatics_data_point.m_KMT = hydrostatic_values.at(6).toDouble();
    hydrostatics_data_point.m_MTC = hydrostatic_values.at(7).toDouble();
    return hydrostatics_data_point;
}

double read_trim(const QString& text_line){
    QRegExp trim_rx(regexp_double);
    if(trim_rx.indexIn(text_line) >= 0){
        QString tmp = trim_rx.cap(1);
        return trim_rx.cap(1).toDouble();
    }
    qWarning("Failed to read trim, assuming 0.0.");
    return 0.0;
}

int read_section_header_number(const QString& current_line) {
    QRegExp rx_header_number(QString("^[0-9]+"));
    if(rx_header_number.indexIn(current_line, 0) >= 0) {
        return rx_header_number.cap(0).toInt();
    }
    qWarning("Failed to read section header number, assuming 0");
    return 0;
}

int main(const int argc, const char** argv ) {
    if(argc != 2){
        qFatal("Usage: bonjean_parser_data bonjean_data_file.txt");
        return -1;
    }
    const char* input_file_name = argv[1];
    QFile file(input_file_name);
    if (!file.open(QFile::ReadOnly)) {
        qFatal(qPrintable(QString("Unable to open input file %1").arg(input_file_name)));
        return -1;
    };
    QTextStream text_stream(&file);
    QMap<double, QList<HydrostaticsDataPoint> > multi_trim_hydrostatics;
    double trim = 0;
    while(!text_stream.atEnd()) {
        QString next_line = text_stream.readLine();
        if(is_trim_line(next_line)){
            trim = read_trim(next_line);
        } else {
            if(is_double_line(next_line)){
                multi_trim_hydrostatics[trim].append(read_hydrostatics_line(next_line));
            } else {
                if(is_header_line(next_line)) {
                    if(read_section_header_number(next_line) != 2) {
                        break;
                    }
                }
            }
        }
    }
    Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> hydrostatics_table;
    Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> metacenter_table;
    const int nb_drafts = multi_trim_hydrostatics.values().at(0).size();
    hydrostatics_table.resize(nb_drafts, 4);
    metacenter_table.resize(nb_drafts + 1, multi_trim_hydrostatics.size() + 1);
    for(int j = 0; j < multi_trim_hydrostatics.keys().size(); ++j){
        metacenter_table(0,j+1) = multi_trim_hydrostatics.keys().at(j);
    }
    for(int i = 0; i < nb_drafts; ++i){
        Q_ASSERT(multi_trim_hydrostatics.contains(0.0));
        hydrostatics_table(i, 0) = multi_trim_hydrostatics.value(0.0).at(i).m_draft;
        hydrostatics_table(i, 1) = multi_trim_hydrostatics.value(0.0).at(i).m_displacement;
        hydrostatics_table(i, 2) = multi_trim_hydrostatics.value(0.0).at(i).m_LCB;
        hydrostatics_table(i, 3) = multi_trim_hydrostatics.value(0.0).at(i).m_MTC;
        metacenter_table(i+1,0) = multi_trim_hydrostatics.value(0.0).at(i).m_draft;
        for(int j = 0; j < multi_trim_hydrostatics.keys().size(); ++j){
            double trim = multi_trim_hydrostatics.keys().at(j);
            metacenter_table(i+1,j+1) = multi_trim_hydrostatics.value(trim).at(i).m_KMT;
        }
    }
    std::cout << hydrostatics_table;
    std::cout << std::endl << std::endl;
    std::cout << metacenter_table;
}
