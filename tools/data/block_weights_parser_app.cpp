#include <QRegExp>
#include <QFile>
#include <QTextStream>
#include <QString>
#include <QStringList>
#include <iostream>
#include <QDebug>
#include <Eigen/Core>
#include <ange/vessel/blockweight.h>

using namespace ange::units;
/**
 A small app to read block weight data contained in the following format:
ELEM1   1017    2,671   2716,407        -5,6    10,2
ELEM47  6035    50,04   301991,4        10,2    79,17
ELEM2   20669   201,55  4165836,95      79,17   336,95
ELEM3   1004    344,444 345821,776      336,628 360,074

 */

const QString regexp_double("([+-]?[0-9]+(\\.[0-9]+)?)");


bool is_elem_line(const QString& text_line) {
    QRegExp rx_is_elem("^ELEM");
    return rx_is_elem.indexIn(text_line, 0) != -1;
}

int main(const int argc, const char** argv ) {
    if(argc != 2){
        qFatal("Usage: bonjean_parser_data bonjean_data_file.txt");
        return -1;
    }
    const char* input_file_name = argv[1];
    QFile file(input_file_name);
    if (!file.open(QFile::ReadOnly)) {
        qFatal(qPrintable(QString("Unable to open input file %1").arg(input_file_name)));
        return -1;
    };
    QTextStream text_stream(&file);
    QList<ange::vessel::BlockWeight> block_weights;
    while(!text_stream.atEnd()) {
        QString next_line = text_stream.readLine();
        if(is_elem_line(next_line)) {
            QStringList block_weight_elements = next_line.split(QRegExp("\\s+"));
            Mass weight = block_weight_elements.at(1).toDouble()*ton;
            Length lcg = block_weight_elements.at(2).toDouble()*meter;
            Length aft_end = block_weight_elements.at(4).toDouble()*meter;
            Length fore_end = block_weight_elements.at(5).toDouble()*meter;
            block_weights.append(ange::vessel::BlockWeight(lcg, aft_end, fore_end, 0*meter, 0*meter, weight));
        }
    }
    Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> block_weight_table;
    block_weight_table.resize(block_weights.size(), 4);
    for(int i = 0; i < block_weights.size(); ++i){
        block_weight_table(i, 0) = block_weights.at(i).aftLimit()/meter;
        block_weight_table(i, 1) = block_weights.at(i).foreLimit()/meter;
        block_weight_table(i, 2) = block_weights.at(i).aftDensity()/ton_per_meter;
        block_weight_table(i, 3) = block_weights.at(i).foreDensity()/ton_per_meter;
    }
    std::cout << block_weight_table;
}
