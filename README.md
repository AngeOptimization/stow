Angelstow
==========

The next generation container stowage tool for vessels and organisations of all
sizes.

See <http://angelstow.net/>.

Angelstow is a container vessel stowage tool and a software platform for
container liner operations. It’s development was initiatied during the Baystow
project, funded by Ange Optimization, The Danish Maritime Fund and Maersk Line.

The benefits of Angelstow are
  * better roundtrip overview
  * better stowage decisions
  * seemless stowage operation
  * interoperability with neighboring functions

leading to
  * more efficient stowage planners
  * better utilization of the vessels
  * shorter port stays
  * fewer shiftings

License
-------

Copyright (C) 2010-2014 Ange Optimization ApS <contact@ange.dk>

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

The full GNU General Public License can be found in the file LICENSE.txt.

Contact
-------

Ange Optimization ApS  
Tagensvej 188, 1.  
DK-2400 Copenhagen NV  
Denmark

E-mail: contact@ange.dk

Phone: +45 50 57 11 95
