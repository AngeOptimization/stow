set(resources_qrcs
    icons.qrc
    data.qrc
    widgets.qrc
)

qt5_add_resources(resources_src ${resources_qrcs})

add_library(resources STATIC
    ${resources_src}
)
target_link_libraries(resources Qt5::Core)
