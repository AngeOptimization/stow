#include "dummy_loadable_test_plugin.h"

dummy_loadable_test_plugin_t::dummy_loadable_test_plugin_t(QObject* parent): QObject(parent) {
    setObjectName("DummyLoadableTestPlugin");
}

#include "dummy_loadable_test_plugin.moc"
