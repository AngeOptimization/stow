#include "dummy_expired_test_plugin.h"

#include <stowplugininterface/pluginlicenseexception.h>
#include <QDate>

dummy_expired_test_plugin_t::dummy_expired_test_plugin_t(QObject* parent): QObject(parent) {
    setObjectName("DummyExpiredTestPlugin");
    QDate expiryDate(2013, 5, 21);
    if (QDate::currentDate() > expiryDate) {
        QString reason = QString("ÆØÅ Plugin license expired at %1").arg(expiryDate.toString("yyyy-MM-dd"));
        throw ange::angelstow::PluginLicenseException(reason);
    }
}

#include "dummy_expired_test_plugin.moc"
