#ifndef DUMMYEXPIREDTESTPLUGIN_H
#define DUMMYEXPIREDTESTPLUGIN_H
#include <QObject>

class dummy_expired_test_plugin_t : public QObject {
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "dk.ange.angelstow.plugin" FILE "metadata.json")
public:
    explicit dummy_expired_test_plugin_t(QObject* parent = 0);
};

#endif // DUMMYEXPIREDTESTPLUGIN_H
