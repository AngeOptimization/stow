#include "crashhandler.h"
#include "document.h"
#include "fileopener.h"
#include "mainwindow.h"
#include "pluginmanager.h"
#include "profiler.h"
#include "version.h"

#include <stowplugininterface/stowplugininterface_version.h>

#include <QApplication>
#include <QDateTime>
#include <QSettings>

static void copySettings(const QSettings &source, QSettings &desination) {
    Q_FOREACH (const QString& key, source.allKeys()) {
        desination.setValue(key, source.value(key));
    }
}

static void upgradeSettings() {
    int settingsVersion;
    QSettings settings;
    if (!settings.contains("settingsVersion")) {
        settingsVersion = 0;
    } else {
        settingsVersion = settings.value("settingsVersion").toInt();
    }

    switch (settingsVersion) { // switch used as a "goto"
        case 0: { // upgrade from version 0 to 1
            QSettings oldSettings("Ange Optimization Aps", "Angelstow");
            copySettings(oldSettings, settings);
            if (settings.contains("geometry")) {
                settings.setValue("layout/Startup/name", "Startup");
                settings.setValue("layout/Startup/mainWindow/geometry", settings.value("geometry"));
                settings.remove("geometry");
                settings.setValue("layout/Startup/mainWindow/state", settings.value("windowState"));
                settings.remove("windowState");
                settings.setValue("layout/Startup/allContainers/horizontalHeader/state",
                                  settings.value("all_containers_table_view_horizontal_header_state"));
                settings.remove("all_containers_table_view_horizontal_header_state");
                settings.setValue("layout/Startup/selectedContainers/horizontalHeader/state",
                                  settings.value("selected_containers_table_view_horizontal_header_state"));
                settings.remove("selected_containers_table_view_horizontal_header_state");
            }
            settings.setValue("settingsVersion", 1);
        } // fallthrough
        case 1: { // upgrade from version 1 to 2
            settings.beginGroup("recaps");
            QStringList recaps = settings.childGroups();
            settings.endGroup();
            Q_FOREACH (const QString& recapName, recaps) {
                QString oldKey = QString("recaps/%1/state").arg(recapName);
                settings.setValue(QString("layout/Startup/recaps/%1/name").arg(recapName), recapName);
                settings.setValue(QString("layout/Startup/recaps/%1/state").arg(recapName), settings.value(oldKey));
                settings.remove(oldKey);
            }
            settings.setValue("settingsVersion", 2);
        }
    }

    settings.setValue("angestowVersion", ANGESTOW_VERSION);
}

int main(int argc, char** argv) {
    CrashHandler::registerCrashHandler();

    Q_INIT_RESOURCE(data);
    Q_INIT_RESOURCE(icons);
    Q_INIT_RESOURCE(widgets);

    QApplication app(argc, argv);
    app.setOrganizationName("Ange Optimization ApS");
    app.setApplicationName("Angelstow");
    app.setOrganizationDomain("ange.dk");

    upgradeSettings();

    // Seeding for general randomness (used at least by the edifact writers)
    qsrand(QDateTime::currentMSecsSinceEpoch());

    PluginManager* pluginManager = new PluginManager(&app);
    if (!qEnvironmentVariableIsSet("ANGELSTOW_NO_PLUGINS")) {
        pluginManager->loadPluginsRecursive(app.applicationDirPath() + "/plugins");
        pluginManager->loadPluginsRecursive(STOW_PLUGIN_INTERFACE_DIR);
    }

    document_t* blankDocument = FileOpener::openBlankStowage().document;
    blankDocument->setPluginManager(pluginManager);
    MainWindow m(pluginManager, blankDocument);
    enableProfilerShortcut(&m);
    m.show();
    if (argc == 2) {
        m.stowage_open(QString(argv[1]));
    }

    return app.exec();
}
