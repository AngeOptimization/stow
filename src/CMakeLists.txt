if(MINGW)
   set(CMAKE_RC_COMPILER_INIT windres)
   enable_language(RC)
   set(CMAKE_RC_COMPILE_OBJECT "<CMAKE_RC_COMPILER> <FLAGS> <DEFINES> -o <OBJECT> <SOURCE>")
endif(MINGW)

# Needed because we include a lot of .h files from /src/ without declaring correct dependencies in CMakeLists.txt
include_directories(.)

configure_file(version.h.in "${CMAKE_CURRENT_BINARY_DIR}/version.h" @ONLY)

add_library(version INTERFACE)
target_include_directories(version INTERFACE ${CMAKE_CURRENT_BINARY_DIR})

set(application_src
    main.cpp
)
if(WIN32)
    add_executable(angelstow WIN32 ${application_src} angelstow.rc)
else(WIN32)
    add_executable(angelstow ${application_src})
endif(WIN32)

target_link_libraries(angelstow
    crashhandler profiler
    gui io document io edi Ange::StowPluginInterface resources version
    Qt5::Widgets Qt5::Gui
)

install(TARGETS angelstow RUNTIME DESTINATION "bin" LIBRARY DESTINATION "lib")

include(CMakePackageConfigHelpers)
configure_package_config_file(
    AngelstowConfig.cmake.in ${CMAKE_CURRENT_BINARY_DIR}/AngelstowConfig.cmake
    INSTALL_DESTINATION lib/cmake/Angelstow
)
write_basic_package_version_file(
    ${CMAKE_CURRENT_BINARY_DIR}/AngelstowConfigVersion.cmake
    VERSION ${ANGESTOW_VERSION}
    COMPATIBILITY SameMajorVersion
)
install(
    FILES
        ${CMAKE_CURRENT_BINARY_DIR}/AngelstowConfig.cmake
        ${CMAKE_CURRENT_BINARY_DIR}/AngelstowConfigVersion.cmake
    DESTINATION lib/cmake/Angelstow
)

add_subdirectory(crashhandler)
add_subdirectory(document)
add_subdirectory(edi)
add_subdirectory(gui)
add_subdirectory(io)
add_subdirectory(plugins)
add_subdirectory(profiler)
add_subdirectory(resources)
add_subdirectory(standardwidgets)
add_subdirectory(test)
