#include "document_interface.h"

#include "bayweightlimits.h"
#include "document.h"
#include "document/userconfiguration/userconfiguration.h"
#include "stabilityforcer.h"
#include "containers/container_list.h"
#include "problems/problem_model.h"
#include "stowage/stowage.h"
#include "stowage/stowagestack.h"
#include "undo/tank_state_command.h"
#include "stowage/container_move.h"
#include "stowage/container_leg.h"
#include "stowage/stability/stresses.h"
#include "stowage/stability/stability.h"
#include "stowplugininterface/macroplan.h"
#include "lashing/lashingpatternfactory.h"
#include "tanks/tankconditions.h"
#include "portwaterdensity/portwaterdensity.h"

#include <stowplugininterface/problem.h>

#include <ange/vessel/point3d.h>
#include <ange/vessel/vessel.h>
#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>

using namespace ange::angelstow;
using namespace ange::containers;
using namespace ange::schedule;
using namespace ange::units;
using namespace ange::vessel;

const ange::schedule::Schedule* document_interface_t::schedule() const{
  return m_document->schedule();
}

const ange::containers::Container* document_interface_t::getContainerById(int id) const {
  return m_document->containers()->get_container_by_id(id);
}

QList< ange::containers::Container* > document_interface_t::containers() {
  return m_document->containers()->list();
}

document_interface_t::document_interface_t(document_t* document): IDocument(), m_document(document) {
  connect(m_document->schedule(), &ange::schedule::Schedule::callAdded, this, &document_interface_t::callAdded);
  connect(m_document->schedule(), &ange::schedule::Schedule::callRemoved, this, &document_interface_t::callRemoved);
  connect(m_document->stability(), &Stability::stabilityChanged,
          this, &document_interface_t::centerOfMassChanged);
}

const ange::vessel::Slot* document_interface_t::containerPosition(const ange::containers::Container* container, const ange::schedule::Call* call) const {
  return m_document->stowage()->container_position(container, call);
}

const BayRowTier document_interface_t::containerNominalPosition(const Container* container, const Call* call) const {
    return m_document->stowage()->nominal_position(container, call);
}

const StowageStack* document_interface_t::stowageStackAtCall(const ange::schedule::Call* call, const ange::vessel::Stack* stack) const
{
  return m_document->stowage()->stowage_stack(call, stack);
}

const ange::vessel::Slot* document_interface_t::slot(const ange::containers::Container* container, const ange::schedule::Call* call) {
  return m_document->stowage()->container_position(container, call);
}

QUndoCommand* document_interface_t::createTankConditionTask(const ange::schedule::Call* call, const ange::vessel::VesselTank* tank, Mass content, QUndoCommand* parent
                                                              ) const {
  tank_state_command_t* tank_state = new tank_state_command_t(m_document, call, parent);
  tank_state->insert_tank_condition(tank, content);
  return tank_state;
}

void document_interface_t::doTask(QUndoCommand* task) {
  m_document->undostack()->push(task);
}

ange::vessel::BlockWeight document_interface_t::totalBlockWeight(const ange::schedule::Call* call) const {
  return m_document->stowage()->stowageAsBlockWeight(call);
}

QPair< Length, Length> document_interface_t::trimAndDraft(const ange::schedule::Call* call) const {
    Length trim = m_document->stabilityForcer()->trim(call);
    Length draft = m_document->stabilityForcer()->draft(call);
    return QPair<Length,Length>(trim, draft);
}

const QHash<const ange::vessel::BaySlice* , ange::vessel::BlockWeight> document_interface_t::cargoBlockWeights(const ange::schedule::Call* call) const {
  return m_document->stowage()->bay_block_weights(call);
}

const QList<ange::vessel::BlockWeight>& document_interface_t::buoyancyBlockWeights(const ange::schedule::Call* call) const {
  return m_document->stresses_bending()->buoyancyBlockWeights(call);
}

const QList<ange::vessel::BlockWeight>& document_interface_t::lightshipBlockWeights() const {
  return m_document->stresses_bending()->lightshipWeights();
}

const QList<ange::vessel::BlockWeight>& document_interface_t::tankBlockWeights(const ange::schedule::Call* call) const {
  return m_document->stresses_bending()->tankWeights(call);
}

const QList<ange::vessel::BlockWeight >& document_interface_t::constantBlockWeights() const {
  return m_document->vessel()->constantWeights();
}

QHash< const ange::vessel::VesselTank*, Mass > document_interface_t::tankConditions(const ange::schedule::Call* call) const {
  return m_document->tankConditions()->tankConditions(call);
}

const ange::vessel::Vessel* document_interface_t::vessel() const{
  return m_document->vessel();
}

double document_interface_t::maxBendingPct(const ange::schedule::Call* call) const{
  return m_document->stresses_bending()->maxBendingPct(call);
}

double document_interface_t::maxShearPct(const ange::schedule::Call* call) const{
  return m_document->stresses_bending()->maxShearPct(call);
}

double document_interface_t::maxTorsionPct(const ange::schedule::Call* call) const{
  return m_document->stresses_bending()->maxTorsionPct(call);
}

ange::units::Length document_interface_t::gm(const ange::schedule::Call* call) const {
    return m_document->stability()->data(call)->gm;
}

const ange::schedule::Call* document_interface_t::containerDestination(const ange::containers::Container* container) const {
  return m_document->stowage()->container_routes()->portOfDischarge(container);
}
const ange::schedule::Call* document_interface_t::containerSource(const ange::containers::Container* container) const {
  return m_document->stowage()->container_routes()->portOfLoad(container);
}

ange::angelstow::StowType document_interface_t::containerStowType(const ange::containers::Container* container) const {
  StowType rv = NonPlacedType;
  Q_FOREACH(const container_move_t* move, m_document->stowage()->moves(container)) {
    if (move->type() == MicroStowedType) {
      rv = MicroStowedType;
      break;
    } else if (rv != MicroStowedType && move->type() == MasterPlannedType) {
      rv = MasterPlannedType;
    }
  }
  return rv;
}

QVector<ange::angelstow::Move> document_interface_t::containerMoves(const ange::containers::Container* container) const {
  QVector<ange::angelstow::Move> rv;
  Q_FOREACH(const container_move_t* move, m_document->stowage()->moves(container)) {
    rv << move->move();
  }
  return rv;
}

ange::units::Mass document_interface_t::weightLimitForBay(const ange::schedule::Call* call,
                                                          const ange::vessel::BaySlice* bay_slice) const {
  return m_document->bayWeightLimits()->weightLimitForBay(call, bay_slice);
}

QHash<const BaySlice*, Mass> document_interface_t::weightLimitsForBays(const Call* call) const {
    QHash<const BaySlice*, Mass> limits;
    Q_FOREACH (const BaySlice* baySlice, m_document->vessel()->baySlices()) {
        Mass limit = m_document->bayWeightLimits()->weightLimitForBay(call, baySlice);
        if (!qIsInf(limit / kilogram)) {
            limits[baySlice] = m_document->bayWeightLimits()->weightLimitForBay(call, baySlice);
        }
    }
    return limits;
}

const ange::containers::Container* document_interface_t::slotContents(const ange::schedule::Call* call,
                                                                        const ange::vessel::Slot* slot) const {
  return m_document->stowage()->contents(slot, call).container();
}

ange::angelstow::LashingPatternInterface* document_interface_t::lashingPattern(const ange::schedule::Call* call) const {
    return LashingPatternFactory::newLashingPattern(this, call);
}

const ange::angelstow::MacroPlan* document_interface_t::macroStowage() const {
    return m_document->macro_stowage();
}
