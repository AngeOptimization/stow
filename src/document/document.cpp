#include "document.h"

#include "bayweightlimits.h"
#include "document/containers/editablecontainerlist.h"
#include "document/stabilityfigures.h"
#include "document/stowage/container_leg.h"
#include "document/stowage/stowage.h"
#include "document_interface.h"
#include "io/containerlist_reader.h"
#include "io/stowagereader.h"
#include "io/stowagewriter.h"
#include "fileopener.h"
#include "passivenotifications.h"
#include "pluginmanager.h"
#include "problems/problem_model.h"
#include "problems/problemkeeper.h"
#include "schedulemodel.h"
#include "stowage/container_move.h"
#include "stowage/crane_split.h"
#include "stowage/stability/stresses.h"
#include "stowage/stability/stability.h"
#include "stowage/visibility_line.h"
#include "stowplugininterface/macroplan.h"
#include "tanks/tankconditions.h"
#include "tanks/tankmodel.h"
#include "userconfiguration/userconfiguration.h"
#include "portwaterdensity/portwaterdensity.h"
#include "stabilityforcer.h"
#include "stowplugininterface/documentusinginterface.h"
#include "stowplugininterface/validatorplugin.h"

#include <ange/containers/container.h>
#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>
#include <ange/stowbase//schedulereader.h>
#include <ange/stowbase/schedulewriter.h>
#include <ange/vessel/bayslice.h>
#include <ange/vessel/slot.h>
#include <ange/vessel/stack.h>
#include <ange/vessel/vessel.h>
#include <ange/vessel/vesseltank.h>

#include <QApplication>
#include <QFile>
#include <QItemSelectionModel>
#include <QMap>
#include <QMessageBox>
#include <QScopedPointer>
#include <QUndoStack>
#include <QtGui>

using ange::angelstow::DocumentUsingInterface;
using ange::angelstow::MacroPlan;
using ange::containers::Container;
using ange::containers::IsoCode;
using ange::schedule::Call;
using ange::schedule::Schedule;
using ange::stowbase::ScheduleReader;
using ange::stowbase::ScheduleWriter;
using ange::vessel::BaySlice;
using ange::vessel::Stack;
using ange::vessel::Vessel;

document_t::document_t(Schedule* schedule,
                       ange::vessel::Vessel* vessel,
                       stowage_t* stowage,
                       QObject* parent):
    QObject(parent),
    m_undostack(new QUndoStack(this)),
    m_schedule(schedule),
    m_vessel(vessel),
    m_stowage(stowage),
    m_container_list(new EditableContainerList(this,this /*qobject parent*/)),
    m_macro_stowage(new MacroPlan(schedule, this)),
    m_user_configuration(new UserConfiguration(schedule, this)),
    m_schedule_model(new ScheduleModel(m_schedule.data(), m_user_configuration.data(), m_undostack)),
    m_problem_model(new problem_model_t(this)),
    m_tankConditions(new TankConditions(m_vessel, this)),
    m_tankTable(new TankModel(this, this /*qobject parent*/)),
    m_waterDensity(new PortWaterDensity(this)),
    m_stabilityForcer(new StabilityForcer(this)),
    m_bayWeightLimits(new BayWeightLimits(this)),
    m_passiveNotifications(new PassiveNotifications(this)),
    m_stability(new Stability(this)),
    m_stresses(new Stresses(this)),
    m_visibility_line(new visibility_line_t(this))
{
    setObjectName("Document");
  Q_ASSERT(m_schedule);
  Q_ASSERT(m_vessel);
  Q_ASSERT(m_stowage);
  vessel->setParent(this);
  stowage->set_document(this);

  connect(m_stowage, &stowage_t::container_changed_position, m_container_list, &container_list_t::slot_container_moved);
  connect(m_stowage, &stowage_t::stowageChangedAtCall, m_container_list, &container_list_t::update_container_positions);
  connect(m_schedule.data(), &Schedule::callAboutToBeRemoved, m_tankConditions, &TankConditions::removeCall);

  m_problemKeeper = new ProblemKeeper(m_problem_model);
  m_stability_figures = new StabilityFigures(this);
  m_bundledContainers.reset(new BundledContainers());
  m_bundledContainers->bundledContainers=new BundledContainerList(this);
  m_bundledContainers->bundledContainers->setSourceModel(m_container_list);
  m_bundledContainers->bundles = new ContainerBundleList(this);
  m_bundledContainers->bundles->setSourceModel(m_container_list);
  m_bundledContainers->filter = new ContainerBundleFilter(this);
  m_bundledContainers->filter->setSourceModel(m_container_list);
}

document_t::~document_t() {
    m_save_mutex.lock(); // ensure that async saving isn't running
    // ^^^ This is probably not the correct place to ensure that async saving is not running, if the saver can access
    //     the mutex then we can't really destroy the document_t, the fix would be to push the mutex a level up out of
    //     the document_t class
    m_save_mutex.unlock(); // Avoid warning: "QMutex: destroying locked mutex"
    delete m_vessel;
}

void document_t::save_stowage(const QString& file_name, const ange::schedule::Call* currentCall) const {
    QMutexLocker lock (&m_save_mutex);
    StowageWriter writer(this, currentCall);
    writer.write(file_name);
}

void document_t::start_save_stowage_async(const QString& filename, const ange::schedule::Call* currentCall) const {
    m_save_mutex.lock();
    StowageWriter* asyncWriter = new StowageWriter(this, currentCall);
    connect(asyncWriter, &StowageWriter::documentSaved, this, &document_t::save_stowage_async_ended);
    connect(asyncWriter, &StowageWriter::documentSaveFailed, this, &document_t::save_stowage_async_ended);
    asyncWriter->writeAsync(filename);
}

void document_t::save_stowage_async_ended() {
  if (StowageWriter* stowage_writer = qobject_cast<StowageWriter*>(sender())) {
    if (stowage_writer->error() == StowageWriter::NoError) {
      emit async_save_done();
    } else {
      emit async_save_error();
    }
  }
  m_save_mutex.unlock();
}

problem_model_t* document_t::problem_model() const {
  return m_problem_model;
}

ProblemKeeper* document_t::problemKeeper() const {
    return m_problemKeeper;
}

StabilityFigures* document_t::stability_figures() const {
  return m_stability_figures;
}

document_t* document_t::document(ange::vessel::Vessel* vessel) {
  document_t* rv = qobject_cast<document_t*>(vessel->parent());
  Q_ASSERT(rv);
  return rv;
}

QString document_t::stowage_filename() {
  return m_stowage_filename;
}

void document_t::stowage_filename(QString filename) {
  m_stowage_filename = filename;
  emit stowage_filename_changed(filename);
}

document_interface_t* document_t::documentInterface() {
  if (m_document_interface.isNull()) {
    m_document_interface.reset(new document_interface_t(this));
  }
  return m_document_interface.data();
}

const document_interface_t* document_t::documentInterface() const {
    return const_cast<document_t*>(this)->documentInterface();
}

QString document_t::autosave_filename() {
  if(m_stowage_filename.isEmpty()) {
    return QString();
  }
  return m_stowage_filename+QLatin1Char('~');
}

const BundledContainers* document_t::bundledContainers() const {
  return m_bundledContainers.data();
}

const Vessel* document_t::vessel() const {
    return m_vessel;
}

void document_t::setVessel(const Vessel* vessel) {
    m_vessel = vessel;
    m_stowage->set_vessel(m_vessel);
    m_tankConditions->setVessel(m_vessel);
    emit vessel_changed(const_cast<Vessel*>(m_vessel)); // TODO should change the signal, but also change all connect places
}

QByteArray document_t::vesselFileData() const {
    return m_vesselFileData;
}

void document_t::setVesselFileData(QByteArray vesselFileData) {
    Q_ASSERT(!vesselFileData.isEmpty());
    m_vesselFileData = vesselFileData;
}

TankModel* document_t::tankModel() {
    return m_tankTable;
}

const PortWaterDensity* document_t::portWaterDensity() const {
    return m_waterDensity;
}

PortWaterDensity* document_t::portWaterDensity() {
    return m_waterDensity;
}

const StabilityForcer* document_t::stabilityForcer() const {
    return m_stabilityForcer;
}

StabilityForcer* document_t::stabilityForcer() {
    return m_stabilityForcer;
}

const Stability* document_t::stability() const {
    return m_stability;
}

void document_t::setPluginManager(PluginManager* pluginManager) {
    // Stores the document_t's PluginManager in m_stowage
    if (m_stowage->pluginManager()) {
        m_stowage->pluginManager()->documentReset(0);
    }
    m_stowage->setPluginManager(pluginManager);
    pluginManager->documentReset(this);
}

const BayWeightLimits* document_t::bayWeightLimits() const {
    return m_bayWeightLimits;
}

BayWeightLimits* document_t::bayWeightLimits() {
    return m_bayWeightLimits;
}

PassiveNotifications* document_t::passiveNotifications() {
    return m_passiveNotifications;
}

#include "document.moc"
