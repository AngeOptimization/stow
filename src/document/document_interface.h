#ifndef DOCUMENT_INTERFACE_H
#define DOCUMENT_INTERFACE_H

#include "stowagestack.h"

#include <stowplugininterface/idocument.h>
#include <ange/units/units.h>

class QUndoCommand;
class StowageStack;
class document_t;

/**
 * Implementation for idocument_t.
 */
class document_interface_t : public ange::angelstow::IDocument {
public:
    document_interface_t(document_t* document);
    virtual const ange::schedule::Schedule* schedule() const;
    virtual const ange::containers::Container* getContainerById(int id) const;
    virtual QList< ange::containers::Container* > containers();
    virtual const ange::vessel::Slot* slot(const ange::containers::Container* container, const ange::schedule::Call* call);
    virtual const ange::vessel::Slot* containerPosition(const ange::containers::Container* container, const ange::schedule::Call* call) const;
    virtual const ange::vessel::BayRowTier containerNominalPosition(const ange::containers::Container* container, const ange::schedule::Call* call) const;
    virtual const StowageStack* stowageStackAtCall(const ange::schedule::Call* call, const ange::vessel::Stack* stack) const;
    virtual QUndoCommand* createTankConditionTask(const ange::schedule::Call* call, const ange::vessel::VesselTank* tank, ange::units::Mass content, QUndoCommand* parent = 0) const;
    virtual void doTask(QUndoCommand* task);
    virtual ange::vessel::BlockWeight totalBlockWeight(const ange::schedule::Call* call) const;
    virtual QPair<ange::units::Length, ange::units::Length> trimAndDraft(const ange::schedule::Call* call) const;
    const QHash< const ange::vessel::BaySlice*, ange::vessel::BlockWeight > cargoBlockWeights(const ange::schedule::Call* call) const;
    virtual const QList<ange::vessel::BlockWeight>& buoyancyBlockWeights(const ange::schedule::Call* call) const;
    virtual const QList< ange::vessel::BlockWeight >& lightshipBlockWeights() const;
    virtual const QList< ange::vessel::BlockWeight >& tankBlockWeights(const ange::schedule::Call* call) const;
    virtual const QList< ange::vessel::BlockWeight >& constantBlockWeights() const;
    virtual QHash<const ange::vessel::VesselTank*, ange::units::Mass> tankConditions(const ange::schedule::Call* call) const;
    virtual const ange::vessel::Vessel* vessel() const;
    virtual double maxBendingPct(const ange::schedule::Call* call) const;
    virtual double maxShearPct(const ange::schedule::Call* call) const;
    virtual double maxTorsionPct(const ange::schedule::Call* call) const;
    virtual ange::units::Length gm(const ange::schedule::Call* call) const;
    virtual const ange::schedule::Call* containerDestination(const ange::containers::Container* container) const;
    virtual const ange::schedule::Call* containerSource(const ange::containers::Container* container) const;
    virtual ange::angelstow::StowType containerStowType(const ange::containers::Container* container) const;
    QVector< ange::angelstow::Move > containerMoves(const ange::containers::Container* container) const;
    virtual QHash<const ange::vessel::BaySlice*, ange::units::Mass> weightLimitsForBays(const ange::schedule::Call* call) const;
    virtual ange::units::Mass weightLimitForBay(const ange::schedule::Call* call, const ange::vessel::BaySlice* bay_slice) const;
    const ange::containers::Container* slotContents(const ange::schedule::Call* call, const ange::vessel::Slot* slot) const;
    virtual ange::angelstow::LashingPatternInterface* lashingPattern(const ange::schedule::Call* call) const;
    virtual const ange::angelstow::MacroPlan* macroStowage() const;
private:
    document_t* m_document;
};

#endif // DOCUMENT_INTERFACE_H
