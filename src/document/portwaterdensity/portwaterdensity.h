#ifndef WATERDENSITY_H
#define WATERDENSITY_H

#include <QObject>
#include <ange/units/units.h>
#include <QHash>

class document_t;
namespace ange {
    namespace schedule {
        class Call;
    }
}

/**
 * Class to represent the water densities for various calls and abstract away the 'relevant'
 * water density and look up
 */
class PortWaterDensity : public QObject {
    Q_OBJECT

    public:
        explicit PortWaterDensity(document_t* document);
        ~PortWaterDensity();

        /**
         * \return the water density for \param call
         */
        ange::units::Density waterDensity(const ange::schedule::Call* call) const;

        /**
         * \return true if the water density for \param call is custom set
         */
        bool hasCustomWaterDensity(const ange::schedule::Call* call) const;

        /**
         * Sets the custom water density for \param call to \param newDensity
         */
        void setCustomWaterDensity(const ange::schedule::Call* call, ange::units::Density newDensity);

        /**
         * Restores the water density to the default density for \param call
         */
        void setDefaultWaterDensity(const ange::schedule::Call* call);
    Q_SIGNALS:
        void densityChanged(const ange::schedule::Call* call);
    private Q_SLOTS:
        void handleRemoveCall(const ange::schedule::Call* call);
    private:
        document_t* m_document;
        QHash<const ange::schedule::Call*, ange::units::Density> m_customDensities;
};

#endif // WATERDENSITY_H
