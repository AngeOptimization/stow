#include "portwaterdensity.h"

#include <document/document.h>
#include <document/userconfiguration/userconfiguration.h>
#include <ange/schedule/schedule.h>
#include <ange/schedule/call.h>

PortWaterDensity::PortWaterDensity(document_t* document)
  : QObject(document), m_document(document)
{
    setObjectName("PortWaterDensity");
    connect(m_document->schedule(), &ange::schedule::Schedule::callRemoved, this, &PortWaterDensity::handleRemoveCall);
}

PortWaterDensity::~PortWaterDensity() {
}

bool PortWaterDensity::hasCustomWaterDensity(const ange::schedule::Call* call) const {
    return m_customDensities.contains(call);
}


void PortWaterDensity::handleRemoveCall(const ange::schedule::Call* call) {
    m_customDensities.remove(call);
}

void PortWaterDensity::setCustomWaterDensity(const ange::schedule::Call* call, ange::units::Density newDensity) {
    m_customDensities[call] = newDensity;
    emit densityChanged(call);
}

void PortWaterDensity::setDefaultWaterDensity(const ange::schedule::Call* call) {
    if(m_customDensities.contains(call)) {
        m_customDensities.remove(call);
        emit densityChanged(call);
    }
}

ange::units::Density PortWaterDensity::waterDensity(const ange::schedule::Call* call) const {
    if(m_customDensities.contains(call)) {
        return m_customDensities.value(call);
    }
    return m_document->userconfiguration()->defaultWaterDensityByCall(call);
}


#include "portwaterdensity.moc"
