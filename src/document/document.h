#ifndef DOCUMENT_H
#define DOCUMENT_H

#include "containers/containerbundlefilter.h"
#include "io/ondiskdata.h"

#include <QObject>
#include <QMap>
#include <QVariant>
#include <QMutex>
#include <QVector>

class BayWeightLimits;
class PassiveNotifications;
class PluginManager;
class TankConditions;
class ScheduleModel;
class document_interface_t;
class problem_model_t;
class ProblemKeeper;
class stowage_t;
class UserConfiguration;
class StabilityFigures;
class PortWaterDensity;
class StabilityForcer;
class Stability;
class Stresses;
class visibility_line_t;
class container_list_t;
class TankModel;

namespace ange {
namespace angelstow {
class IDocument;
class Plugin;
class MacroPlan;
}
namespace vessel {
class Vessel;
}
namespace stowbase {
class Bundle;
class ReferenceObjectFactory;
}
namespace containers {
class Container;
}
namespace schedule {
class Schedule;
class Call;
}
}

class QUndoStack;

/**
 * \brief Struct holding data about bundled containers
 */
struct BundledContainers {
    /**
     * QAIM representing a container list without bundles, as in 'pure containers'
     */
    ContainerBundleFilter* filter;
    /**
     * QAIM with the bundles in the containerlist. The exact opposite as the 'filter'
     */
    ContainerBundleList* bundles;
    /**
     * List of containers in bundles
     */
    BundledContainerList* bundledContainers;
};


/**
 * Represents a current document (stowage) that the user has opened.
 *
 * Also contains other state that is not saved like problems created and
 * passive notifications, this data could be moved out to a state object
 * that could be bushed in between MainWindow and document_t.
 */
class document_t : public QObject {

    Q_OBJECT

public:

    /**
     * Create document from parts
     * Takes ownership of schedule, vessel, container elements and stowage.
     */
    document_t(ange::schedule::Schedule* schedule, ange::vessel::Vessel* vessel, stowage_t* stowage, QObject* parent = 0);

    /**
     * Destructor
     */
    ~document_t();

    /**
     * Sets the PluginManager for the document.
     * Resets the old and the new PluginManager.
     */
    void setPluginManager(PluginManager* pluginManager);

    /**
     * Save a stowage
     */
    void save_stowage(const QString& file_name, const ange::schedule::Call* currentCall) const;

    /**
     * Saves a stowage async.
     */
    void start_save_stowage_async(const QString& filename, const ange::schedule::Call* currentCall) const;

    /**
     * Undo stack, by which all changes must pass to support undo/redo
     */
    QUndoStack* undostack() {
      return m_undostack;
    }

    /**
     * @return schedule
     */
    const ange::schedule::Schedule* schedule() const {
      return m_schedule.data();
    }

    ange::schedule::Schedule* schedule() { return m_schedule.data();}

    /**
     * @return schedule as a model
     */
    const ScheduleModel* schedule_model() const {
      return m_schedule_model.data();
    }
    ScheduleModel* schedule_model() { return m_schedule_model.data();}

        /**
     * @return the list of containers.
     */
    container_list_t* containers() const {
      return m_container_list;
    }

    /**
     * @return User configuration, e.g. the colors of the ports. Later this could be other things as well.
     */
    UserConfiguration* userconfiguration() const {
      return m_user_configuration.data();
    }

    /**
     * Convience function to get document from vessel. Just gets the parent, and casts it appropriately.
     */
    static document_t* document(ange::vessel::Vessel* vessel);

    /**
     * Set stowage filename
     */
    void stowage_filename(QString filename);

    /**
     * Get stowage filename
     */
    QString stowage_filename();

    /**
     * Get filename for autosave. If no filename is set, the empty string is returned.
     * Currently, the implementation basically returns the filename with '~' appended.
     */
    QString autosave_filename();

    /**
     * @return stowage of ship (container positions and moves)
     */
    stowage_t* stowage() const {
      return m_stowage;
    }

    /**
     * @return a pointer to the problem model
     */
    problem_model_t* problem_model() const;

    /**
     * @return the problem keeper, this holds problems that should be deleted when the document is deleted
     */
    ProblemKeeper* problemKeeper() const;

    /**
     * @return a pointer to the key stability figures
     */
    StabilityFigures* stability_figures() const;

    /**
     * @return macro stowage for document
     */
    ange::angelstow::MacroPlan* macro_stowage() const {
      return m_macro_stowage;
    }

    /**
     * \return the tankconditions for this document
     */
    TankConditions* tankConditions() {
        return m_tankConditions;
    }

    const TankConditions* tankConditions() const {
        return m_tankConditions;
    }

    /**
     * @return the vessel
     */
    const ange::vessel::Vessel* vessel() const;

    /**
     * Sets the vessel, usually, you want the vessel_import_command_t instead.
     *
     * Requires that the full load condition of the document is empty, including stowage, macro stowage and tanks.
     * Takes ownership of the new vessel and loses ownership of the old.
     */
    void setVessel(const ange::vessel::Vessel* vessel);

    /**
     * Gets the vessel file data, the JSON representation of the vessel.
     */
    QByteArray vesselFileData() const;

    /**
     * Set vessel file data (JSON format).
     */
    void setVesselFileData(QByteArray vesselFileData);

    /**
     * @return external interface for document
     */
    document_interface_t* documentInterface();

    /**
     * @return external interface for document
     */
    const document_interface_t* documentInterface() const;

    /**
     * @return struct holding data about bundled containers
     */
    const BundledContainers* bundledContainers() const;

    /**
     * Returns the model/view tank representation
     */
    TankModel* tankModel();

    /**
     * @return water density object
     */
    PortWaterDensity* portWaterDensity();
    const PortWaterDensity* portWaterDensity() const;

    /**
     * @return draft survey object
     */
    StabilityForcer* stabilityForcer();

    /**
     * @return const draft survey object
     */
    const StabilityForcer* stabilityForcer() const;

    /**
     * @return bending data for stowage
     */
    const Stresses* stresses_bending() const {
        return m_stresses;
    }

    /**
     * @return visibiliy line calculation tool for stowage
     */
    visibility_line_t* visibility_line() {
        return m_visibility_line;
    }

    /**
     * Return all stability data
     */
    const Stability* stability() const;

    /**
     * Weights limits for bays, used by macro stowage
     */
    BayWeightLimits* bayWeightLimits();

    /**
     * Weights limits for bays, used by macro stowage
     */
    const BayWeightLimits* bayWeightLimits() const;

    /**
     * Current passive notifications
     */
    PassiveNotifications* passiveNotifications();

Q_SIGNALS:

    void stowage_filename_changed(QString stowage_filename);
    void vessel_changed(ange::vessel::Vessel* vessel);
    /**
     * emitted at the end of a async save, if succesful
     */
    void async_save_done();
    /**
     * emitted at the end of a async save if error
     */
    void async_save_error();

private Q_SLOTS:

    void save_stowage_async_ended();

private:

    mutable QMutex m_save_mutex;
    QUndoStack* m_undostack;
    QScopedPointer<ange::schedule::Schedule> m_schedule;
    const ange::vessel::Vessel* m_vessel;
    QByteArray m_vesselFileData;
    stowage_t* m_stowage;
    container_list_t* m_container_list;
    ProblemKeeper* m_problemKeeper;
    StabilityFigures* m_stability_figures;
    QString m_stowage_filename;
    ange::angelstow::MacroPlan* m_macro_stowage;
    QScopedPointer<UserConfiguration> m_user_configuration;
    QScopedPointer<document_interface_t> m_document_interface;
    QScopedPointer<ScheduleModel> m_schedule_model;
    problem_model_t* m_problem_model;
    QScopedPointer<BundledContainers> m_bundledContainers;
    TankConditions* m_tankConditions;
    TankModel* m_tankTable;
    PortWaterDensity* m_waterDensity;
    StabilityForcer* m_stabilityForcer;
    BayWeightLimits* m_bayWeightLimits;
    PassiveNotifications* m_passiveNotifications;

    /**
     * vessel physics related stuff
     */
    Stability* m_stability;
    Stresses* m_stresses;
    visibility_line_t* m_visibility_line;

};

#endif // DOCUMENT_H
