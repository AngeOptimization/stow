/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010

 Copyright: See COPYING file that comes with this distribution

*/

#ifndef SCHEDULE_MODEL_H
#define SCHEDULE_MODEL_H

#include <QAbstractItemModel>

class QUndoStack;
class QSignalMapper;
class UserConfiguration;
namespace ange {
namespace schedule {
class Call;
class Schedule;
}
}

class ScheduleModel : public QAbstractTableModel {
  Q_OBJECT

public:
    ScheduleModel(ange::schedule::Schedule* schedule, const UserConfiguration* user_configuration, QUndoStack* undostack, QObject* parent = 0);

    enum column_t {
      UNCODE_COL,
      VOYAGE_COL,
      CRANES_COL,
      HLIMIT_COL,
      CRANE_RULE_COL,
      TWINLIFT_COL,
      PRODUCTIVITY_COL,
      ETA_COL,
      ETD_COL,
      NO_COLUMNS
    };
    virtual QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const;
    virtual int columnCount(const QModelIndex& parent = QModelIndex()) const;
    virtual int rowCount(const QModelIndex& parent = QModelIndex()) const;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
    virtual Qt::ItemFlags flags(const QModelIndex& index) const;
    virtual bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole);

private Q_SLOTS:
    void slotCallAboutToBeRemoved(ange::schedule::Call* call);
    void slotCallAdded(ange::schedule::Call* call);
    void slotCallAboutToBeAdded(int position, ange::schedule::Call* call);
    bool setEditData(ange::schedule::Call* call, ScheduleModel::column_t column, QVariant value);
    void allDataChanged();
    void slotCallRemoved(ange::schedule::Call*);
    void slotCallChanged(int call_id);

    /**
     * @param fromPosition position of call before move
     * @param toPosition new position of call after it has been removed from fromPosition
     * WARNING! that the way toPosition is handled is bad because it not like QAbstractItemModel::beginMoveRows()
     */
    void rotationAboutToBeChanged(int fromPosition, int toPosition);
    /**
     * @param fromPosition position of call before move
     * @param toPosition new position of call after it has been removed from fromPosition
     * WARNING! that the way toPosition is handled is bad because it not like QAbstractItemModel::beginMoveRows()
     */
    void rotationChanged(int fromPosition, int toPosition);

private:
    QVariant displayData(const ange::schedule::Call* call, column_t column) const;
    QVariant editData(const ange::schedule::Call* call, column_t column) const;
    QColor backgroundColor(const ange::schedule::Call* call) const;

private:
    ange::schedule::Schedule* m_schedule;
    const UserConfiguration* m_userConfiguration;
    QUndoStack* m_undostack;
    QSignalMapper* m_callMapper;
    bool m_movingInProgress;

};

#endif // SCHEDULE_MODEL_H
