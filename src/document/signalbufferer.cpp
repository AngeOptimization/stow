#include "signalbufferer.h"
#include <ange/schedule/schedule.h>
#include <QTimer>

SignalBufferer::SignalBufferer(QObject* parent): QObject(parent), m_timer(new QTimer(this))
{
    setObjectName("SignalBufferer");
    m_timer->setSingleShot(true);
    m_timer->setInterval(50);
    connect(m_timer,&QTimer::timeout, this, &SignalBufferer::timeout);
}

void SignalBufferer::startBuffer() {
    m_timer->start();
}

SignalBuffererWithCall::SignalBuffererWithCall(const ange::schedule::Schedule* schedule,QObject* parent)
    : QObject(parent), m_bufferer(new SignalBufferer(this)), m_schedule(schedule) {
    connect(m_bufferer, &SignalBufferer::timeout, this, &SignalBuffererWithCall::emitSignals);
}

bool listContainsConstCall(const QList<ange::schedule::Call*>& list, const ange::schedule::Call* call) {
    Q_FOREACH(const ange::schedule::Call* c, list) {
        if(c == call) {
            return true;
        }
    }
    return false;
}

void SignalBuffererWithCall::emitSignals() {
    Q_FOREACH(const ange::schedule::Call* call, m_calls) {
        if(listContainsConstCall(m_schedule->calls(),call)) {
            timeout(call);
        }
    }
    m_calls.clear();
}

void SignalBuffererWithCall::startBuffer(const ange::schedule::Call* call) {
    m_calls << call;
    m_bufferer->startBuffer();
}




#include "signalbufferer.moc"
