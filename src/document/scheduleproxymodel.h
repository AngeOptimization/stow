#ifndef SCHEDULEPROXYMODEL_H
#define SCHEDULEPROXYMODEL_H

#include <QPointer>
#include <QIdentityProxyModel>

namespace ange {
namespace schedule {
class Call;
class Schedule;
}
}

/**
 * A class used to select calls between (not inclusive) the configured load and discharge calls
 */
class ScheduleProxyModel : public QIdentityProxyModel {
    Q_OBJECT

public:
    ScheduleProxyModel(QObject* parent = 0);

    /**
     * sets the load call to filter from. If null, it is assumed that all calls are valid discharge calls
     */
    void setLoadCall(const ange::schedule::Call* call);

    /**
     * sets the discharge call to filter from. If null, it is assumed that all calls are valid load calls
     */
    void setDischargeCall(const ange::schedule::Call* call);

    /**
     * sets the schedule to check against
     */
    void setSchedule(ange::schedule::Schedule* schedule);

protected:
    virtual QVariant data(const QModelIndex& proxyIndex, int role = Qt::DisplayRole) const;
    virtual Qt::ItemFlags flags(const QModelIndex& index) const;

private:
    bool filterAcceptsRow(int row) const;
    bool less(const ange::schedule::Call* call1, const ange::schedule::Call* call2) const;

private:
    QPointer<const ange::schedule::Call> m_loadCall;
    QPointer<const ange::schedule::Call> m_dischargeCall;
    QPointer<ange::schedule::Schedule> m_schedule;

};

#endif // SCHEDULEPROXYMODEL_H
