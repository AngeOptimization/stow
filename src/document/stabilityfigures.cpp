#include "stabilityfigures.h"

#include "document/document.h"
#include "stabilityforcer.h"
#include "document/userconfiguration/userconfiguration.h"
#include "document/utils/modelutils.h"
#include "gui/colortools.h"
#include "stowage/stowage.h"
#include "stowage/stability/stresses.h"
#include "stowage/stability/stability.h"
#include "portwaterdensity/portwaterdensity.h"
#include "undo/portwaterdensitycommand.h"
#include "undo/draftsurveycommand.h"
#include "forcegmcommand.h"

#include <ange/schedule/schedule.h>
#include <ange/schedule/call.h>
#include <ange/vessel/blockweight.h>
#include <ange/vessel/vessel.h>

using namespace ange::units;
using ange::schedule::Call;
using ange::schedule::Schedule;
using ange::vessel::BlockWeight;
using ange::vessel::Vessel;

StabilityFigures::StabilityFigures(document_t* document)
  : QAbstractTableModel(document), m_document(document)
{
    reset();
    connect(document->schedule(), &Schedule::scheduleChanged, this, &StabilityFigures::reset);
    connect(document->stresses_bending(), &Stresses::stresses_changed, this, &StabilityFigures::updateStresses);
    // Stresses::stresses_changed is triggered by:
    //   Stability::stabilityChanged that is triggered by:
    //     DraftSurvey::draftSurveyChanged that is triggered by:
    //       changes in DraftSurvey::Mode
    //       stowage_t::stowageChangedAtCall
    //       TankConditions::tankConditionChanged
    //       PortWaterDensity::densityChanged
    //       document_t::vessel_changed
    //       Schedule::callAdded
}

QString StabilityFigures::trim(const Call* call, const QString& nanString, const QString& unitString) const {
    double number = m_document->stability()->data(call)->trim / meter;
    return qIsNaN(number) ? nanString : QString::number(number, 'f', 2) + unitString;
}

QString StabilityFigures::draft(const Call* call, const QString& nanString, const QString& unitString) const {
    double number = m_document->stability()->data(call)->draft / meter;
    return qIsNaN(number) ? nanString : QString::number(number, 'f', 2) + unitString;
}

QString StabilityFigures::gm(const Call* call, const QString& nanString, const QString& unitString) const {
    double number = m_document->stability()->data(call)->gm / meter;
    return qIsNaN(number) ? nanString : QString::number(number, 'f', 2) + unitString;
}

QString StabilityFigures::waterDensity(const Call* call, const QString& nanString, const QString& unitString) const {
    double number = m_document->portWaterDensity()->waterDensity(call) / (kilogram/meter3);
    return qIsNaN(number) ? nanString : QString::number(number, 'f', 0) + unitString;
}

QString StabilityFigures::deadloadWeight(const Call* call, const QString& nanString, const QString& unitString) const {
    double number = m_document->stabilityForcer()->deadLoad(call).weight() / ton;
    return qIsNaN(number) ? nanString : QString::number(number, 'f', 0) + unitString;
}

QString StabilityFigures::deadloadLcg(const Call* call, const QString& nanString, const QString& unitString) const {
    double number = m_document->stabilityForcer()->deadLoad(call).lcg() / meter;
    return qIsNaN(number) ? nanString : QString::number(number, 'f', 2) + unitString;
}

QVariant StabilityFigures::data(const QModelIndex& index, int role) const {
    if (!index.isValid()) {
        return QVariant();
    }
    const Call* call = m_document->schedule()->at(index.row());

    if (role == Qt::EditRole) {
        switch (index.column()) {
            case TRIM:
                return trim(call, "", "");
            case DRAFT:
                return draft(call, "", "");
            case GM:
                return gm(call, "", "");
            case WATER_DENSITY:
                return waterDensity(call, "", "");
            case DEADLOAD_WEIGHT:
                return deadloadWeight(call, "", "");
            case DEADLOAD_LCG:
                return deadloadLcg(call, "", "");
            default:
                Q_ASSERT(false);
                return QVariant();
        }
    }

    if (role == Qt::DisplayRole) {
        switch (index.column()) {
            case UNCODE:
                return QVariant(call->uncode());
            case TRIM:
                return trim(call, "N/A", " m");
            case DRAFT:
                return draft(call, "N/A", " m");
            case DRAFT_AFT: {
                double number = (m_document->stability()->data(call)->draft + m_document->stability()->data(call)->trim/2)/ meter;
                return qIsNaN(number) ? "N/A" : QString::number(number, 'f', 2) + " m";
            }
            case DRAFT_FWD: {
                double number = (m_document->stability()->data(call)->draft - m_document->stability()->data(call)->trim/2)/ meter;
                return qIsNaN(number) ? "N/A" : QString::number(number, 'f', 2) + " m";
            }
            case BENDING: {
                double number = m_document->stresses_bending()->maxBendingPct(call);
                return qIsNaN(number) ? "N/A" : QString::number(number, 'f', 0) + " %";
            }
            case SHEAR: {
                double number = m_document->stresses_bending()->maxShearPct(call);
                return qIsNaN(number) ? "N/A" : QString::number(number, 'f', 0) + " %";
            }
            case TORSION: {
                double number = m_document->stresses_bending()->maxTorsionPct(call);
                return qIsNaN(number) ? "N/A" : QString::number(number, 'f', 0) + " %";
            }
            case GM:
                return gm(call, "N/A", " m");
            case DISPLACEMENT: {
                double number = m_document->stability()->data(call)->totalWeight / ton;
                return qIsNaN(number) ? "N/A" : QString::number(number, 'f', 0) + " t";
            }
            case BALLAST_WEIGHT: {
                double number = m_document->stability()->data(call)->ballastWeight / ton;
                return qIsNaN(number) ? "N/A" : QString::number(number, 'f', 0) + " t";
            }
            case CARGO_WEIGHT: {
                double number = m_document->stability()->data(call)->cargoWeight / ton;
                return qIsNaN(number) ? "N/A" : QString::number(number, 'f', 0) + " t";
            }
            case LIST: {
                double number = m_document->stability()->data(call)->list;
                return qIsNaN(number) ? "N/A" : QString::number(number, 'f', 1) + " \u00B0";
            }
            case WATER_DENSITY:
                return waterDensity(call, "N/A", " kg/m\u00B3");
            case DEADLOAD_WEIGHT:
                return deadloadWeight(call, "N/A", " t");
            case DEADLOAD_LCG:
                return deadloadLcg(call, "N/A", " m");
        }
        Q_ASSERT(false);
        return QVariant();
    }

    if (role == Qt::TextAlignmentRole) {
        if (index.column() == UNCODE) {
            return QVariant(Qt::AlignLeft | Qt::AlignVCenter);
        }
        return QVariant(Qt::AlignRight | Qt::AlignVCenter);
    }

    switch (index.column()) {
        case UNCODE:
            if (role == Qt::ForegroundRole) {
                return readableTextColor(m_document->userconfiguration()->get_color_by_call(call));
            }
            if (role == Qt::BackgroundRole) {
                return m_document->userconfiguration()->get_color_by_call(call);
            }
            break;
        case TRIM:
            if (m_document->stabilityForcer()->forcedDraftTrimIsActive(call)) {
                return ModelUtils::forcedEditCellStyle(role);
            }
            break;
        case DRAFT:
            if (m_document->stabilityForcer()->forcedDraftTrimIsActive(call)) {
                return ModelUtils::forcedEditCellStyle(role);
            }
            break;
        case BENDING:
            if (100 < m_document->stresses_bending()->maxBendingPct(call)) {
                return ModelUtils::limitBrokenCellStyle(role);
            }
            break;
        case SHEAR:
            if (100 < m_document->stresses_bending()->maxShearPct(call)) {
                return ModelUtils::limitBrokenCellStyle(role);
            }
            break;
        case TORSION:
            if (100 < m_document->stresses_bending()->maxTorsionPct(call)) {
                return ModelUtils::limitBrokenCellStyle(role);
            }
            break;
        case GM:
            if (m_document->stabilityForcer()->forcedGmIsActive(call)) {
                return ModelUtils::forcedEditCellStyle(role);
            }
            if (m_document->stability()->data(call)->gm < 0.6 * meter) { // TODO replace 0.6 with actual min GM for the vessel
                return ModelUtils::limitBrokenCellStyle(role);
            }
            break;
        case WATER_DENSITY:
            if (m_document->portWaterDensity()->hasCustomWaterDensity(call)) {
                return ModelUtils::forcedEditCellStyle(role);
            }
            break;
        case DEADLOAD_WEIGHT:
            if (m_document->stabilityForcer()->deadLoadIsActive(call)) {
                return ModelUtils::forcedEditCellStyle(role);
            }
            break;
        case DEADLOAD_LCG:
            if (m_document->stabilityForcer()->deadLoadIsActive(call)) {
                return ModelUtils::forcedEditCellStyle(role);
            }
            break;
    }

    return QVariant();
}

int StabilityFigures::columnCount(const QModelIndex& /*parent*/) const {
    return LAST_COLUMN;
}

int StabilityFigures::rowCount(const QModelIndex& /*parent*/) const {
    return m_document->schedule()->size();
}

QVariant StabilityFigures::headerData(int section, Qt::Orientation orientation, int role) const {
    if (orientation != Qt::Horizontal) {
        return QAbstractItemModel::headerData(section, orientation, role);
    }

    if (role == Qt::DisplayRole) {
        switch (section) {
            case UNCODE:
                return QVariant("Call");
            case TRIM:
                return QVariant("Trim");
            case DRAFT:
                return QVariant("Draft");
            case DRAFT_AFT:
                return QVariant("D. Aft");
            case DRAFT_FWD:
                return QVariant("D. Fwd");
            case BENDING:
                return QVariant("BM");
            case SHEAR:
                return QVariant("SF");
            case TORSION:
                return QVariant("TM");
            case GM:
                return QVariant("GM");
            case DISPLACEMENT:
                return QVariant("Displ.");
            case BALLAST_WEIGHT:
                return QVariant("Ballast");
            case CARGO_WEIGHT:
                return QVariant("Cargo");
            case LIST:
                return QVariant("List");
            case WATER_DENSITY:
                return QVariant("WD");
            case DEADLOAD_WEIGHT:
                return QVariant("DL. Weight");
            case DEADLOAD_LCG:
                return QVariant("DL. LCG ");
        }
    }

    if (role == Qt::ForegroundRole) {
        switch (section) {
            case BENDING:
                return QColor(Qt::blue);
            case SHEAR:
                return QColor(Qt::red);
            case TORSION:
                return QColor(Qt::black); // yellow is hard to read on the grey background
        }
    }

    switch (section) {
        case TRIM:
        case DRAFT:
        case GM:
        case WATER_DENSITY:
        case DEADLOAD_WEIGHT:
        case DEADLOAD_LCG:
            return ModelUtils::editableHeaderStyle(role);
        default:
            return ModelUtils::nonEditableHeaderStyle(role);
    }

    return QVariant();
}

Qt::ItemFlags StabilityFigures::flags(const QModelIndex& index) const {
    Qt::ItemFlags result = super::flags(index);
    switch (index.column()) {
        case TRIM:
        case DRAFT:
        case GM:
        case WATER_DENSITY:
        case DEADLOAD_WEIGHT:
        case DEADLOAD_LCG:
            result |= Qt::ItemIsEditable;
    }
    return result;
}

bool StabilityFigures::setData(const QModelIndex& index, const QVariant& value, int role) {
    if (role != Qt::EditRole) {
        Q_ASSERT(false);
        return false;
    }
    const Call* call = m_document->schedule()->at(index.row());
    switch (index.column()) {
        case TRIM: {
            bool conversionOk;
            double number = value.toDouble(&conversionOk);
            if (conversionOk) {
                m_document->undostack()->push(DraftSurveyCommand::forceTrim(m_document, call, number * meter));
            } else {
                m_document->undostack()->push(DraftSurveyCommand::clearSurvey(m_document, call));
            }
            return true;
        }
        case DRAFT: {
            bool conversionOk;
            double number = value.toDouble(&conversionOk);
            if (conversionOk) {
                m_document->undostack()->push(DraftSurveyCommand::forceDraft(m_document, call, number * meter));
            } else {
                m_document->undostack()->push(DraftSurveyCommand::clearSurvey(m_document, call));
            }
            return true;
        }
        case GM: {
            bool conversionOk;
            double number = value.toDouble(&conversionOk);
            if (conversionOk) {
                m_document->undostack()->push(new ForceGmCommand(m_document, call, number * meter));
            } else {
                m_document->undostack()->push(new ForceGmCommand(m_document, call, qQNaN() * meter)); // clears setting
            }
            return true;
        }
        case WATER_DENSITY: {
            bool conversionOk;
            double number = value.toDouble(&conversionOk);
            if (conversionOk) {
                m_document->undostack()->push(PortWaterDensityCommand::customDensity(m_document, call, number * kilogram/meter3));
            } else {
                m_document->undostack()->push(PortWaterDensityCommand::defaultDensity(m_document, call));
            }
            return true;
        }
        case DEADLOAD_WEIGHT: {
            bool conversionOk;
            double number = value.toDouble(&conversionOk);
            if (conversionOk) {
                m_document->undostack()->push(DraftSurveyCommand::setDeadLoadWeight(m_document, call, number * ton));
            } else {
                m_document->undostack()->push(DraftSurveyCommand::clearSurvey(m_document, call));
            }
            return true;
        }
        case DEADLOAD_LCG: {
            bool conversionOk;
            double number = value.toDouble(&conversionOk);
            if (conversionOk) {
                m_document->undostack()->push(DraftSurveyCommand::setDeadLoadLcg(m_document, call, number * meter));
            } else {
                m_document->undostack()->push(DraftSurveyCommand::clearSurvey(m_document, call));
            }
            return true;
        }
    }
    return false;
}

void StabilityFigures::updateStability(const Call* call) {
    int rowIndex = m_document->schedule()->indexOf(call);
    emit dataChanged(index(rowIndex, TRIM), index(rowIndex, DRAFT_FWD));
    emit dataChanged(index(rowIndex, GM), index(rowIndex, DEADLOAD_LCG));
}

void StabilityFigures::updateStresses(const Call* call) {
    int rowIndex = m_document->schedule()->indexOf(call);
    emit dataChanged(index(rowIndex, BENDING), index(rowIndex, TORSION));
}

void StabilityFigures::reset() {
    beginResetModel();
    Q_FOREACH (const Call * call, m_document->schedule()->calls()) {
        updateStability(call);
        updateStresses(call);
    }
    endResetModel();
}

#include "stabilityfigures.moc"
