#ifndef SIGNALBUFFERER_H

#define SIGNALBUFFERER_H
#include <QObject>
#include <QSet>

class QTimer;

namespace ange {
namespace schedule {
class Call;
class Schedule;
}
}


/**
 * \brief Simple class to buffer signals
 *
 * For 'frequent emitted signals' where you just want the last, you can put a SignalBufferer in between
 */
class SignalBufferer : public QObject {
    Q_OBJECT
    public:
        explicit SignalBufferer(QObject* parent = 0);
    Q_SIGNALS:
        void timeout();
    public Q_SLOTS:
        void startBuffer();
    private:
        QTimer* m_timer;
};

/**
 * \brief Simple class to buffer signals that conveys a call
 *
 * For 'frequent emitted signals' where you just want the last, you can put this SignalBufferer in between.
 * It also checks at emission time if the call still part of the schedule (and e.g. not removed). If that's
 * the case, the signal is not emitted.
 */
class SignalBuffererWithCall : public QObject {
    Q_OBJECT
    public:
        SignalBuffererWithCall(const ange::schedule::Schedule* schedule, QObject* parent = 0);
    private Q_SLOTS:
        void emitSignals();
    public Q_SLOTS:
        void startBuffer(const ange::schedule::Call* call);
    Q_SIGNALS:
        void timeout(const ange::schedule::Call* call);
    private:
        SignalBufferer* m_bufferer;
        QSet<const ange::schedule::Call*> m_calls;
        const ange::schedule::Schedule* m_schedule;
};

#endif // SIGNALBUFFERER_H
