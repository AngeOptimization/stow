#include "userconfiguration.h"

#include <QVector>
#include <QSettings>
#include <QFile>
#include <qfileinfo.h>
#include <ange/schedule/schedule.h>
#include <ange/schedule/call.h>
#include <ange/vessel/vessel.h>
#include <qpixmap.h>
#include <QPainter>
#include <QDesktopServices>
#include "gui/colortools.h"

using ange::schedule::Schedule;

UserConfiguration::UserConfiguration(const ange::schedule::Schedule* schedule, QObject* parent) :
    QObject(parent), m_schedule(schedule)
{
    setObjectName("UserConfiguration");

    Q_INIT_RESOURCE(defaultconfiguration); // these resources are part of user configuration and thus not
                                           // automatically init'ed on app start

    QSettings settings;
    m_usePivotPortBasedColoring = settings.value("view/use pivot port based coloring", true).toBool();

    if(m_colorsBeforePivot.isEmpty()) {
        m_colorsBeforePivot
             << QColor("#fdff4c") //pivot
             << QColor("#62ff00") //pivot - 1
             << QColor("#fd00ff") //pivot - 2
             << QColor("#9c77a0") //pivot - 3
             << QColor("#b31126") //pivot - 4
             << QColor("#2d51af") //pivot - 5
             << QColor("#edf096") //
             << QColor("#8d5a08") //
             << QColor("#4e46ff") //
             << QColor("#ffff14")
             << QColor("#00ffff")
             << QColor("#c79fef")
             << QColor("#d1b26f")
             << QColor("#050faa")
             << QColor("#659921")
             << QColor("#ff796c")
             << QColor("#6e750e")
             << QColor("#001164")
             << QColor("#ffb07c")
             << QColor("#53fca1")
             << QColor("#75bbfd")
             << QColor("#380282")
             << QColor("#6666CC")
             << QColor("#993333")
             << QColor("#339933")
             << QColor("#AAAAFF")
             << QColor("#999933")
             << QColor("#993399")
             << QColor("#339999")
             << QColor("#FFFF99")
             << QColor("#FF9933")
             << QColor("#FFCC66")
             ;
    }
    if(m_colorsAfterPivot.isEmpty()) {
        m_colorsAfterPivot
             << QColor("#e50000")
             << QColor("#1a552f") //pivot +1
             << QColor("#8c0d00") //pivot +2
             << QColor("#ff070f") //pivot +3
             << QColor("#039cff") //pivot +4
             << QColor("#f9a67c") //pivot +5
             << QColor("#69fff6") //pivot +6
             << QColor("#e7b839") //pivot +7
             << QColor("#6e750e") //
             << QColor("#19f7c1") //
             << QColor("#c20078")
             << QColor("#ffff14")
             << QColor("#00ffff")
             << QColor("#c79fef")
             << QColor("#d1b26f")
             << QColor("#050faa")
             << QColor("#659921")
             << QColor("#ff796c")
             << QColor("#6e750e")
             << QColor("#001164")
             << QColor("#ffb07c")
             << QColor("#53fca1")
             << QColor("#75bbfd")
             << QColor("#380282")
             << QColor("#6666CC")
             << QColor("#993333")
             << QColor("#339933")
             << QColor("#AAAAFF")
             << QColor("#999933")
             << QColor("#993399")
             << QColor("#339999")
             << QColor("#FFFF99")
             << QColor("#FF9933")
             << QColor("#FFCC66")
             ;
    }
    connect(m_schedule, &Schedule::scheduleChanged, this, &UserConfiguration::colorsChanged);
}

QColor UserConfiguration::get_color_by_call(const ange::schedule::Call* call) const {

    if (!call) {
        return Qt::red;
    }

    if(m_usePivotPortBasedColoring) {
        Q_ASSERT(m_schedule->pivotCall());
        if(call->distance(m_schedule->pivotCall()) >= 0){
            const int index = call->distance(m_schedule->pivotCall()) % (m_colorsBeforePivot.size());
            if (index < m_colorsBeforePivot.size()) {
                return m_colorsBeforePivot.at(index);
            }
        } else {
            const int index = m_schedule->pivotCall()->distance(call) % (m_colorsAfterPivot.size());
            if (index < m_colorsAfterPivot.size()) {
                return m_colorsAfterPivot.at(index);
            }
        }
        return Qt::black;
    }


    QHash<QString, QColor>::const_iterator it = m_customColorsCache.constFind(call->uncode());
    if(it != m_customColorsCache.constEnd()) {
        return it.value();
    }

    QString portColorsPath = findSettingsPath("portcolors.ini");
    QSettings portColors(portColorsPath, QSettings::IniFormat);
    portColors.beginGroup("portcolors");
    QColor color = portColors.value(call->uncode(),"black").toString();
    m_customColorsCache.insert(call->uncode(), color);
    return color;
}

QString UserConfiguration::findSettingsPath(const QString& iniFileName) {
    QSettings settings;
    if (settings.contains("customconfigurationdir")) {
        QString iniFilePath = settings.value("customconfigurationdir").toString() + "/" + iniFileName;
        if (QFileInfo(iniFilePath).isReadable()) {
            return iniFilePath;
        }
    }

    QString iniFilePath = QString(":/defaultconfiguration/defaultconfiguration/") + iniFileName;
    if (QFileInfo(iniFilePath).isReadable()) {
        return iniFilePath;
    }
    Q_ASSERT(false); // default configuration file path should always have been found here - otherwise build evironment is faulty
    return QString();
}

ange::units::Density UserConfiguration::defaultWaterDensityByCall(const ange::schedule::Call* call) const {
    if (!call) {
        return ange::vessel::Vessel::oceanWaterDensity;
    }
    QString portWaterDensitiesPath = findSettingsPath("portwaterdensities.ini");
    QSettings portWaterDensities(portWaterDensitiesPath, QSettings::IniFormat);
    portWaterDensities.beginGroup("portwaterdensities");
    if (portWaterDensities.contains(call->uncode())) {
        return portWaterDensities.value(call->uncode()).toDouble() * ange::units::kilogram_per_meter3;
    }
    return ange::vessel::Vessel::oceanWaterDensity;
}

void UserConfiguration::setUsePivotPortBasedColoring(bool enable) {
    m_usePivotPortBasedColoring = enable;
    emit colorsChanged();
}

QSettings* UserConfiguration::newLayoutSettings() {
    return new QSettings(findSettingsPath("layouts.ini"), QSettings::IniFormat);
}

#include "userconfiguration.moc"
