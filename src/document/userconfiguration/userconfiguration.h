#ifndef USER_CONFIGURATION_H
#define USER_CONFIGURATION_H

#include <QColor>
#include <QHash>
#include <qpixmap.h>
#include <ange/units/units.h>

class QSettings;
namespace ange {
namespace schedule {
class Call;
class Schedule;
}
}

class UserConfiguration : public QObject {
  Q_OBJECT
public:

    /**
     * The shared layout settings, caller own returned object
     */
    static QSettings* newLayoutSettings();

    UserConfiguration(const ange::schedule::Schedule* schedule, QObject* parent);
    /**
     * @return color connected to call, if call does not exist in colormap the function return null
     */
    QColor get_color_by_call(const ange::schedule::Call* call) const;
    /**
     * look-up of port water density at call as defined in ressource portwaterdensity,
     * should be in interval [990, 1030]. If call does not exist in portwaterdensity ressource,
     * the function returns the default Vessel::oceanWaterDensity
     *
     * @return port water density at call
     */
    ange::units::Density defaultWaterDensityByCall(const ange::schedule::Call* call) const;

Q_SIGNALS:
    void colorsChanged();
public Q_SLOTS:
    /**
     * sets wether to use pivot port based coloring for the ports or user-defined values per port
     */
    void setUsePivotPortBasedColoring(bool enable);

private:
    static QString findSettingsPath(const QString& iniFileName);

private:
    const ange::schedule::Schedule* m_schedule;
    QVector<QColor> m_colorsBeforePivot; // really a class constant
    QVector<QColor> m_colorsAfterPivot; // really a class constant
    bool m_usePivotPortBasedColoring;
    mutable QHash<QString, QColor> m_customColorsCache;

};

#endif // USER_CONFIGURATION_H
