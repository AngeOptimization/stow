QT5_ADD_RESOURCES(default_configuration_result resources/defaultconfiguration.qrc)

add_library(userconfiguration STATIC
    userconfiguration.cpp
    ${default_configuration_result}
)
target_link_libraries(userconfiguration Qt5::Gui Ange::Vessel Ange::Schedule)
