#ifndef STABILITY_FIGURES_H
#define STABILITY_FIGURES_H

#include <QAbstractItemModel>

#include <ange/units/units.h>

class document_t;
namespace ange {
namespace schedule {
class Call;
}
namespace vessel {
class Vessel;
}
}

class StabilityFigures : public QAbstractTableModel {
    typedef QAbstractTableModel super;

    Q_OBJECT

public:

    /**
     * Note: this enum defines the ordering of the columns in the table
     */
    enum columns {
        UNCODE,
        TRIM,
        DRAFT,
        DRAFT_AFT,
        DRAFT_FWD,
        BENDING,
        SHEAR,
        TORSION,
        GM,
        DISPLACEMENT,
        BALLAST_WEIGHT,
        CARGO_WEIGHT,
        LIST,
        WATER_DENSITY,
        DEADLOAD_WEIGHT,
        DEADLOAD_LCG,
        LAST_COLUMN // corresponds to the number of columns in the table
    };

    explicit StabilityFigures(document_t* document);

    // Inherited:
    virtual QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    virtual int columnCount(const QModelIndex& parent = QModelIndex()) const Q_DECL_OVERRIDE;
    virtual int rowCount(const QModelIndex& parent = QModelIndex()) const Q_DECL_OVERRIDE;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    virtual Qt::ItemFlags flags(const QModelIndex& index) const Q_DECL_OVERRIDE;
    virtual bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) Q_DECL_OVERRIDE;

private Q_SLOTS:

    /**
     * Updates the stability data for a given call
     */
    void updateStability(const ange::schedule::Call* call);

    /**
     * Updates the stress data for a given call
     */
    void updateStresses(const ange::schedule::Call* call);

    /**
     * Cleans and rebuilds the table
     */
    void reset();

private:

    QString trim(const ange::schedule::Call* call, const QString& nanString, const QString& unitString) const;
    QString draft(const ange::schedule::Call* call, const QString& nanString, const QString& unitString) const;
    QString gm(const ange::schedule::Call* call, const QString& nanString, const QString& unitString) const;
    QString waterDensity(const ange::schedule::Call* call, const QString& nanString, const QString& unitString) const;
    QString deadloadWeight(const ange::schedule::Call* call, const QString& nanString, const QString& unitString) const;
    QString deadloadLcg(const ange::schedule::Call* call, const QString& nanString, const QString& unitString) const;

private:

    document_t* m_document;

};

#endif // STABILITY_FIGURES_H
