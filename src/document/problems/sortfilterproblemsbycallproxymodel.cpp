#include "sortfilterproblemsbycallproxymodel.h"

#include "stowplugininterface/problem.h"

#include <ange/schedule/call.h>

#include <QSettings>

using ange::angelstow::Problem;

SortFilterProblemsByCallProxyModel::SortFilterProblemsByCallProxyModel(QObject* parent): QSortFilterProxyModel(parent), m_currentCall(0){
    QSettings settings;
    m_enable = settings.value("view/hide_nonactive").toBool();
}

void SortFilterProblemsByCallProxyModel::setCurrentCall(const ange::schedule::Call* call) {
    if (m_currentCall == call) {
        return;
    }
    m_currentCall = call;
    if (m_enable) {
        filterChanged();
    }
}

void SortFilterProblemsByCallProxyModel::setFilteringEnabled(bool enable) {
    if (m_enable == enable) {
        return;
    }
    m_enable = enable;
    filterChanged();
}

bool SortFilterProblemsByCallProxyModel::filterAcceptsRow(int source_row, const QModelIndex& source_parent) const {
    if(!m_enable) {
        return true;
    }
    QModelIndex idx = sourceModel()->index(source_row,0,source_parent);
    Problem* problem = idx.data(Qt::UserRole).value<Problem*>();

    if(problem->location().call()) {
        return problem->location().call() == m_currentCall;
    } else {
        return true;
    }
}

#include "sortfilterproblemsbycallproxymodel.moc"
