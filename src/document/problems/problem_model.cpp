#include "problem_model.h"

#include "stowplugininterface/problem.h"

#include <ange/containers/container.h>
#include <ange/schedule/call.h>
#include <ange/vessel/bayslice.h>
#include <ange/vessel/stack.h>
#include <ange/vessel/stacksupport.h>
#include <ange/vessel/vessel.h>

#include <QIcon>
#include <QTextStream>

using ange::angelstow::Problem;
using ange::angelstow::ProblemLocation;

using ange::containers::Container;
using ange::vessel::StackSupport;
using ange::schedule::Call;

inline static const QIcon & warningicon() {
  static QIcon icon(":/icons/icons/warning.svg");
  return icon;
}
inline static const QIcon & erroricon() {
  static QIcon icon(":/icons/icons/error.svg");
  return icon;
}
inline static const QIcon & noticeicon() {
  static QIcon icon(":/icons/icons/notice.svg");
  return icon;
}

problem_model_t::problem_model_t(QObject* parent) : QAbstractItemModel(parent) {
}

QVariant problem_model_t::data(const QModelIndex& index, int role) const {
  if(!index.isValid()) {
    return QVariant();
  }
  TreeNode* node = static_cast<TreeNode*>(index.internalPointer());
  Q_ASSERT(node);
  if(role==Qt::DisplayRole) {
    switch(index.column()) {
      case SEVERITY: {
        return Problem::severityToString(node->problem->severity());
      }
      case TYPE: {
          return node->problem->description();
      }
      case LOCATION: {
        ProblemLocation location = node->problem->location();
        if(const Container* container = location.container()) {
          return container->equipmentNumber();
        } else if(const Call* call = location.call()) {
          if(const StackSupport* stack_support = location.stackSupport()) {
            return call->assembledName() + " " + stack_support->bayName()+":"+stack_support->rowName();
          } else if (location.bayRowTier().placed()) {
            return call->assembledName() + " " +location.bayRowTier().toString();
          } else {
            return call->assembledName();
          }
        } else {
          return "unknown location";
        }
      }
      case DETAILS: {
        if(node->parent) {
          return node->problem->details();
        } else {
          return QString("%1 Occurrences").arg(node->children.count());
        }
      }
    }
  } else if(role==Qt::DecorationRole && !node->parent && index.column()==0) {
    switch(node->problem->severity()) {
      case Problem::Error: {
        return QVariant(erroricon());
      }
      case Problem::Warning: {
        return QVariant(warningicon());
      }
      case Problem::Notice: {
        return QVariant(noticeicon());
      }
    }
  } else if(role == SORT_ROLE){
    QString sort_string = QString::number(9-node->problem->severity()) //we want to reverse the order in the enum to have most important first, and least important is labelled 1
                                                                 //and most important is labelled 3
    + data(problem_model_t::index(index.row(), TYPE, index.parent()), Qt::DisplayRole).toString()
    + data(problem_model_t::index(index.row(), LOCATION, index.parent()), Qt::DisplayRole).toString();

    return sort_string;
  } else if (role == Qt::UserRole) {
      return QVariant::fromValue<Problem*>(node->problem);

  }
  return QVariant();
}

QVariant problem_model_t::headerData(int section, Qt::Orientation orientation, int role) const {
  if(orientation==Qt::Horizontal&&role==Qt::DisplayRole) {
    switch(section) {
      case SEVERITY: {
        return "Severity";
      }
      case TYPE: {
        return "Type";
      }
      case LOCATION: {
        return "Location";
      }
      case DETAILS: {
        return "Details";
      }
    }
  }
  return QVariant();
}



int problem_model_t::columnCount(const QModelIndex& parent) const {
  if(parent.isValid() && parent.column()>0)  {
    return 0;
  }
  return 4;
}

int problem_model_t::rowCount(const QModelIndex& parent) const {
  if(parent.column()>0) {
    return 0;
  }
  if(parent.isValid()) {
    TreeNode* problem = static_cast<TreeNode*>(parent.internalPointer());
    return problem->children.count();
  } else {
    return m_problems.count();
  }
}

QModelIndex problem_model_t::parent(const QModelIndex& child) const {
  if(!child.isValid()) {
    return QModelIndex();
  }
  TreeNode* childNode = static_cast<TreeNode*>(child.internalPointer());
  if(TreeNode* parentNode = childNode->parent) {
    Q_ASSERT(m_problems.contains(parentNode)); // we only have two levels;
    return createIndex(m_problems.indexOf(parentNode),0,parentNode);
  } else {
    return QModelIndex();
  }

}

QModelIndex problem_model_t::index(int row, int column, const QModelIndex& parent) const {
  if(!hasIndex(row,column,parent)) {
    return QModelIndex();
  }
  TreeNode* node = 0;
  if(parent.isValid()) {
    TreeNode* parentNode = static_cast<TreeNode*>(parent.internalPointer());
    if(parentNode->children.count() > row) {
      node = parentNode->children.at(row);
    }
  } else {
    if(m_problems.size() > row) {
      node = m_problems.at(row);
    }
  }
  if(node) {
    return createIndex(row,column,node);
  } else {
    return QModelIndex();
  }
}

bool problem_model_t::hasChildren(const QModelIndex& parent) const {
  if(parent.column()>0) {
    return false;
  }
  if(parent.isValid()) {
    TreeNode* node = static_cast<TreeNode*>(parent.internalPointer());
    return node->children.count()>0;
  } else {
    return m_problems.size()>0;
  }
}

QString calculateGroupId(Problem* problem) {
    QString id;
    QTextStream ss(&id);
    ss << problem->severity();
    ss << problem->description();
    if(problem->location().call()) {
        ss << problem->location().call()->id();
    }
    ss.flush();
    return id;
}


void problem_model_t::add_problem(Problem* problem) {
    QString id = calculateGroupId(problem);
    int row;
    TreeNode* parentNode;
    if(m_problem_groups.contains(id)) {
        parentNode = m_problem_groups.value(id);
        Q_ASSERT(parentNode);
        row = m_problems.indexOf(parentNode);
        Q_ASSERT(row>=0);
    } else {
        parentNode = new TreeNode;
        parentNode->problem = new Problem(problem->severity(),problem->location(),problem->description(),this);
        parentNode->problem->setDetails("HEADER");
        connect(parentNode->problem,&Problem::changed, this, &problem_model_t::problem_changed);
        beginInsertRows(QModelIndex(),m_problems.size(), m_problems.size());
        m_problems.append(parentNode);
        m_problem_groups[id]=parentNode;
        endInsertRows();
        row = m_problems.size()-1;
    }
    QModelIndex parentIndex = createIndex(row,0,parentNode);
    connect(problem,&Problem::changed, this, &problem_model_t::problem_changed);
    connect(problem,&Problem::destroyed, this, &problem_model_t::remove_problem);
    beginInsertRows(parentIndex,m_problem_groups[id]->children.count(),m_problem_groups[id]->children.count());
    TreeNode* childNode = new TreeNode;
    childNode->problem = problem;
    childNode->parent = parentNode;
    m_problem_groups[id]->children.append(childNode);
    endInsertRows();
    if(problem->location().stackSupport() && problem->location().call()) {
        CallStackSupportPair key(problem->location().call(), problem->location().stackSupport());
        m_problemsByCallAndStack[key] << problem;
        Q_ASSERT(m_problemsByCallAndStack[key].count(problem) == 1);
    }
}

problem_model_t::TreeNode* problem_model_t::findNode(Problem* problem) {
    QString id = calculateGroupId(problem);
    Q_ASSERT(m_problem_groups.contains(id));
    if(m_problem_groups.value(id)->problem == problem) {
        return m_problem_groups.value(id);
    } else {
        Q_FOREACH(TreeNode* possibleNode, m_problem_groups.value(id)->children) {
            if(possibleNode->problem == problem) {
                return possibleNode;
            }
        }
    }
    return 0;
}


void problem_model_t::remove_problem(Problem* problem ) {
//    Remove problem from model.
//    If last problem in group, the parent will be deleted
//    If parent, and no child, it will be removed
//    If parent with child, Q_ASSERT
    QString id = calculateGroupId(problem);
    Q_ASSERT(m_problem_groups.contains(id));
    TreeNode* node = findNode(problem);
    Q_ASSERT(node);
    if(node->parent) {
        if(problem->location().stackSupport() && problem->location().call()) {
            CallStackSupportPair key(problem->location().call(), problem->location().stackSupport());
            if(m_problemsByCallAndStack.contains(key)) {
                int count = m_problemsByCallAndStack[key].removeAll(problem);
                Q_UNUSED(count);
                Q_ASSERT(count == 1);
            }
        }
    }
    bool child_problem = node->parent != 0;
    if(child_problem) {
        TreeNode* parent = node->parent;
        int row = m_problems.indexOf(parent);
        QModelIndex parentIndex = createIndex(row,0,parent);
        int childnumber = parent->children.indexOf(node);
        beginRemoveRows(parentIndex,childnumber,childnumber);
        int count = parent->children.removeAll(node);
        Q_UNUSED(count);
        Q_ASSERT(count == 1);
        endRemoveRows();
        delete node;
        if(parent->children.isEmpty()) {
            remove_problem(parent->problem);
        }
    } else {
        if (node->children.isEmpty()) {
            Q_ASSERT(m_problems.contains(node));
            int row_to_remove = m_problems.indexOf(node);
            beginRemoveRows(QModelIndex(),row_to_remove, row_to_remove);
            m_problems.removeAt(row_to_remove);
            m_problem_groups.remove(id);
            endRemoveRows();
            delete node->parent; // this is our toplevel pseudo-problem
            delete node;
        } else {
            Q_ASSERT(false);
        }
    }
}

void problem_model_t::problem_changed(Problem* problem) {
  QString id = calculateGroupId(problem);
  TreeNode* node = findNode(problem);
  bool child_problem = node->parent != 0;
  Q_ASSERT(m_problem_groups.contains(id));
  if(child_problem) {
    TreeNode* parent = node->parent;
    int row = m_problems.indexOf(parent);
    Q_ASSERT(row >= 0);
    QModelIndex parentIndex = createIndex(row,0,parent);
    int childnumber = parent->children.indexOf(node);
    QModelIndex upper_left = createIndex(childnumber,0,node);
    QModelIndex lower_right = createIndex(childnumber,columnCount(parentIndex)-1,node);
    emit dataChanged(upper_left,lower_right);
  } else {
    int row = m_problems.indexOf(node);
    Q_ASSERT(row>=0);
    QModelIndex uppper_left = createIndex(row,0,node);
    QModelIndex lower_right = createIndex(row,columnCount(QModelIndex())-1, node);
    emit dataChanged(uppper_left,lower_right);
  }
}

QList< Problem* > problem_model_t::problemsForCallStackSupport(const Call* call, const ange::vessel::StackSupport* stack) const {
    return m_problemsByCallAndStack.value(CallStackSupportPair(call,stack));
}

#include "problem_model.moc"
