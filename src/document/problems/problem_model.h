#ifndef PROBLEM_MODEL_H
#define PROBLEM_MODEL_H

#include <QAbstractItemModel>
#include <QList>

#include <stowplugininterface/problemadderinterface.h>

namespace ange {
    namespace vessel {
        class StackSupport;
    } // namespace
    namespace schedule {
        class Call;
    } // namespace
    namespace angelstow {
        class Problem;
    }
} // namespace


class problem_model_t : public QAbstractItemModel, public ange::angelstow::ProblemAdderInterface {
  Q_OBJECT
  public:
    /**
     * enum describing the different columns of the problems in the model
     * Note: this also describes the order of the columns.
     */
    enum problem_column_type_t{
      SEVERITY,
      TYPE,
      LOCATION,
      DETAILS
    };

    enum role_t {
      SORT_ROLE = Qt::UserRole + 1
    };
    problem_column_type_t index2column_type(int index) const;
    /**
     * ctor.
     * @param parent as in QObject parent
     */
    problem_model_t(QObject* parent = 0);

    virtual QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const;
    virtual int columnCount(const QModelIndex& parent = QModelIndex()) const;
    virtual int rowCount(const QModelIndex& parent = QModelIndex()) const;
    virtual QModelIndex parent(const QModelIndex& child) const;
    virtual QModelIndex index(int row, int column, const QModelIndex& parent = QModelIndex()) const;
    virtual bool hasChildren(const QModelIndex& parent = QModelIndex()) const;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
    /**
     * Add a problem to the collection.
     * The only way to delete problems from the model is to delete the problem object.
     * @param problem to be added
     */
    void addProblem(ange::angelstow::Problem* problem) {
        add_problem(problem);
    }
    void add_problem(ange::angelstow::Problem* problem);

    QList<ange::angelstow::Problem*> problemsForCallStackSupport(const ange::schedule::Call* call, const ange::vessel::StackSupport* stacksupport) const;

    virtual ~problem_model_t() { }

  private Q_SLOTS:
    void remove_problem(ange::angelstow::Problem*);
    void problem_changed(ange::angelstow::Problem*);
  private:
      /* simple helper struct for the tree*/
    struct TreeNode {
        TreeNode() : parent(0), problem(0) { }
        TreeNode* parent;
        QList<TreeNode*> children;
        ange::angelstow::Problem* problem;
    };
    QList<TreeNode*> m_problems;
    QHash<QString,TreeNode*> m_problem_groups;;
    typedef QPair<const ange::schedule::Call*, const ange::vessel::StackSupport*> CallStackSupportPair;
    QHash<CallStackSupportPair, QList<ange::angelstow::Problem*> > m_problemsByCallAndStack;
    private:
        /**
         * Tries to find the node for a problem and return it.
         * Return 0 if the problem doesn't exist in the model.
         *
         * At a later point, it likely should be backed by a cache
         */
        TreeNode* findNode(ange::angelstow::Problem* problem);
};


#endif // PROBLEM_MODEL_H
