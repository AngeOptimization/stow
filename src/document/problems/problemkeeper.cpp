#include "problemkeeper.h"

#include "problem_model.h"

#include "stowplugininterface/problem.h"

using ange::angelstow::Problem;

ProblemKeeper::ProblemKeeper(problem_model_t* problemModel): QObject(problemModel), m_problemModel(problemModel) {
    setObjectName("ProblemKeeper");
}

ProblemKeeper::~ProblemKeeper() {
    qDeleteAll(QList<Problem*>(m_problems));  // Loop over copy of list as it is modified by signal/slot connections
}

void ProblemKeeper::addProblem(Problem* problem) {
    m_problems << problem;
    connect(problem, &Problem::destroyed, this, &ProblemKeeper::removeProblemFromKeeper);
    m_problemModel->add_problem(problem);
}

void ProblemKeeper::removeProblemFromKeeper(Problem* problem) {
    int removed = m_problems.removeAll(problem);
    Q_ASSERT(removed == 1);
    Q_UNUSED(removed);
}

#include "problemkeeper.moc"
