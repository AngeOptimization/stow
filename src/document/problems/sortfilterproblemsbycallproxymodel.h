#ifndef SORTFILTERPROBLEMSBYCALLPROXYMODEL_H
#define SORTFILTERPROBLEMSBYCALLPROXYMODEL_H

#include <QSortFilterProxyModel>

namespace ange {
namespace schedule {
class Call;
}
}

class SortFilterProblemsByCallProxyModel : public QSortFilterProxyModel {
    Q_OBJECT
    public:
        SortFilterProblemsByCallProxyModel(QObject* parent = 0);
    public Q_SLOTS:
        void setFilteringEnabled(bool enable);
        void setCurrentCall(const ange::schedule::Call* call);
    protected:
        virtual bool filterAcceptsRow(int source_row, const QModelIndex& source_parent) const;
    private:
        bool m_enable;
        const ange::schedule::Call* m_currentCall;
};

#endif // SORTFILTERPROBLEMSBYCALLPROXYMODEL_H
