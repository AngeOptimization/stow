#ifndef PROBLEMKEEPER_H
#define PROBLEMKEEPER_H

#include <QObject>

class problem_model_t;
namespace ange {
    namespace angelstow {
        class Problem;
    } // namespace angelstow
} // namespace ange

/**
 * Keeps problems and makes sure that they are deleted when this object is deleted.
 */
class ProblemKeeper : public QObject {
    Q_OBJECT

public:
    /**
     * @param problemModel the model that the problems will be fed into, will also be QObject parent
     */
    ProblemKeeper(problem_model_t* problemModel);

    /**
     * Will also delete all kept problems.
     */
    virtual ~ProblemKeeper();

    /**
     * Add a problem to the keeper and the companion model.
     *
     * If a problem that has been added is deleted it will also be dropped from the keeper.
     */
    void addProblem(ange::angelstow::Problem* problem);

private Q_SLOTS:
    void removeProblemFromKeeper(ange::angelstow::Problem* problem);

private:
    problem_model_t* m_problemModel;
    QList<ange::angelstow::Problem*> m_problems;

};

#endif // PROBLEMKEEPER_H
