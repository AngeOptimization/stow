#ifndef BAYWEIGHTLIMITS_H
#define BAYWEIGHTLIMITS_H

#include <ange/units/units.h>

#include <QHash>
#include <QObject>

namespace ange {
namespace schedule {
class Call;
}
namespace vessel {
class BaySlice;
}
}

/**
 * Weight limits for calls and bays, used bay macro stower
 */
class BayWeightLimits : public QObject {

    Q_OBJECT

public:

    BayWeightLimits(QObject* parent);
    ~BayWeightLimits();

    /**
     * @return the upper bound for weight on a bay. Will return infinity for no limit
     */
    ange::units::Mass weightLimitForBay(const ange::schedule::Call* call, const ange::vessel::BaySlice* baySlice) const;

    /**
     * @param limit the upper bound for weight on a bay in call. Use infinity for no limit
     */
    void setWeightLimitForBay(const ange::schedule::Call* call, const ange::vessel::BaySlice* baySlice,
                              ange::units::Mass value);

Q_SIGNALS:

    /**
     * A bay weight limit has changed
     */
    void bayWeightLimitChanged(const ange::schedule::Call* call, const ange::vessel::BaySlice* baySlice,
                               ange::units::Mass newValue);

private:

    QHash<const ange::schedule::Call*, QHash<const ange::vessel::BaySlice*, ange::units::Mass> > m_limits;

};

#endif // BAYWEIGHTLIMITS_H
