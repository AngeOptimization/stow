#ifndef SCHEDULE_WILDCARD_PROXY_MODEL_H
#define SCHEDULE_WILDCARD_PROXY_MODEL_H

#include <QAbstractProxyModel>

/**
 * A proxy model for the schedule model allowing us to place a wildcard "%" in the first row
 */
class ScheduleWildcardProxyModel : public QAbstractProxyModel
{
  Q_OBJECT
public:
    virtual Qt::ItemFlags flags(const QModelIndex& index) const;
    virtual bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole);
    virtual QVariant data(const QModelIndex & proxyIndex, int role = Qt::DisplayRole) const;
    virtual int rowCount(const QModelIndex& parent = QModelIndex()) const;
    virtual int columnCount(const QModelIndex& parent = QModelIndex()) const;
    virtual QModelIndex index(int row, int column, const QModelIndex& parent = QModelIndex()) const;
    virtual QModelIndex mapFromSource(const QModelIndex& sourceIndex) const;
    virtual QModelIndex mapToSource(const QModelIndex& proxyIndex) const;
    virtual QModelIndex parent(const QModelIndex& child) const;
    virtual QItemSelection mapSelectionToSource(const QItemSelection& selection) const;
    virtual QItemSelection mapSelectionFromSource(const QItemSelection& selection) const;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    virtual void setSourceModel(QAbstractItemModel* sourceModel);
};

#endif // SCHEDULE_WILDCARD_PROXY_MODEL_H
