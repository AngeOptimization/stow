#ifndef ABSTRACT_EXPORTER_BASE_H
#define ABSTRACT_EXPORTER_BASE_H

#include <QString>

class document_t;
class QIODevice;

namespace ange {
namespace containers {

class Container;
}

  namespace schedule {
    class Call;
  }
}

class abstract_exporter_base_t {
  public:
    /**
     * abstract base for similar exporters for baplie2.x, baplie1.5 and movins
     */
    abstract_exporter_base_t(const document_t* document, QString voyage_number, QString carrier_code, QString vessel_code, const ange::schedule::Call* current_call);

    /**
     * writes the output to the iodevice, restricted to the list of containers
     */
    virtual void write(QIODevice* device, QList<const ange::containers::Container*> containers) = 0;
    virtual ~abstract_exporter_base_t();
  protected:
    const document_t* document() const {
      return m_document;
    }
    QString voyage_number() const {
      return m_voyage_number;
    }
    QString carrier_code() const {
      return m_carrier_code;
    }
    QString vessel_code() const {
      return m_vessel_code;
    }
    const ange::schedule::Call* current_call() const {
      return m_current_call;
    }

  private:
    const document_t* m_document;
    QString m_voyage_number;
    QString m_carrier_code;
    QString m_vessel_code;
    const ange::schedule::Call* m_current_call;
};

#endif // ABSTRACT_EXPORTER_BASE_H
