#include "baplie15_exporter.h"
#include "edi/baplie15_writer.h"
#include "document/document.h"

#include <ange/containers/container.h>
#include <ange/schedule/schedule.h>

#include "document/stowage/stowage.h"
#include "document/stowage/container_leg.h"

baplie15_exporter_t::baplie15_exporter_t(const document_t* document, QString voyage_number, QString carrier_code, QString vessel_code, const ange::schedule::Call* current_call) :
                                         abstract_exporter_base_t(document,voyage_number,carrier_code,vessel_code,current_call)
                                        {
}

void baplie15_exporter_t::write(QIODevice* device, QList<const ange::containers::Container*> containers) {
  const ange::schedule::Call* next_call = document()->schedule()->next(current_call());
  if(!next_call) {
    return; //what to do here?
  }
  baplie15_writer_t writer(device,document()->vessel(),current_call(),next_call,
                           vessel_code(), voyage_number(),carrier_code());
  writer.start();
  Q_FOREACH(const ange::containers::Container* cont, containers) {
    ange::vessel::BayRowTier brt = document()->stowage()->nominal_position(cont,current_call());
    if (brt.placed()) {
      writer.add_container(cont,
                           document()->stowage()->container_routes()->portOfLoad(cont),
                           document()->stowage()->container_routes()->portOfDischarge(cont),
                           brt);
    }
  }
  writer.end();
}
