#ifndef COPRAR_EXPORTER_H
#define COPRAR_EXPORTER_H

#include "abstract_exporter_base.h"

namespace ange {
namespace containers {
class Container;
}
}


class coprar_exporter_t : public abstract_exporter_base_t {
  public:
    coprar_exporter_t(const document_t* document, QString voyage_number, QString carrier_code, const ange::schedule::Call* current_call);
    virtual void write(QIODevice* device);

    /**
     * writes only the given list of containers to iodevice. Note that only containers with source/origin = current_port() will actually be written.
     */
    virtual void write(QIODevice* device, QList<const ange::containers::Container*> containers);
};

#endif // COPRAR_EXPORTER_H
