#include <QtTest>
#include <document/document.h>
#include <baplie_exporter.h>
#include <movins_exporter.h>
#include <baplie15_exporter.h>
#include <coprar_exporter.h>
#include <document/containers/container_list.h>
#include <edi/test/testfilecontents.h>
#include <ange/schedule/schedule.h>
#include <ange/schedule/call.h>
#include <ange/vessel/vessel.h>
#include <ange/containers/container.h>
#include <QFile>
#include "test/util/extramacros.h"
#include <fileopener.h>

class LegoShipEdiExport : public QObject {
    Q_OBJECT
        document_t* m_document;
    private Q_SLOTS:
        void initTestCase();
        void testExporters_data();
        void testExporters();
};

void LegoShipEdiExport::initTestCase() {
    m_document = FileOpener::openStoFile(QFINDTESTDATA("lego-maersk-with-containers.sto")).document;
    QVERIFY(m_document);
}

struct StringCompareEquipmentNumber {
    bool operator()(const ange::containers::Container* container1, const ange::containers::Container* container2) const  {
        Q_ASSERT(container1 != 0);
        Q_ASSERT(container2 != 0);
        return container1->equipmentNumber() < container2->equipmentNumber();
    }
};

QList<const ange::containers::Container*> constifyAndSort(QList<ange::containers::Container*> inputlist) {
    QList<const ange::containers::Container*> output;
    Q_FOREACH(const ange::containers::Container* container, inputlist) {
        output << container;
    }
    qSort(output.begin(), output.end(), StringCompareEquipmentNumber());
    return output;
}

void LegoShipEdiExport::testExporters() {
    QFETCH(QString, expectedoutput);
    QFETCH(QString, outputtype);
    QFETCH(QString, callstring);
    QFETCH(int, headerlinecount);
    QFETCH(int, footerlinecount);

    const ange::schedule::Call* call = m_document->schedule()->getCallByUncodeAfterCall(callstring, m_document->schedule()->calls().front());
    QVERIFY(call);

    abstract_exporter_base_t* exporter = 0;
    if(outputtype == "BAPLIE") {
        exporter = new baplie_exporter_t(m_document, call->voyageCode(), "", m_document->vessel()->imoNumber(), call);
    } else if(outputtype == "MOVINS") {
        exporter = new movins_exporter_t(m_document, call->voyageCode(), "", m_document->vessel()->imoNumber(), call);
    } else if(outputtype == "BAPLIE15") {
        exporter = new baplie15_exporter_t(m_document, call->voyageCode(), "", m_document->vessel()->imoNumber(), call);
    } else if(outputtype == "COPRAR") {
        exporter = new coprar_exporter_t(m_document, call->voyageCode(), "", call);
    } else {
        QFAIL("Unknown output type");
    }

    QBuffer b;
    b.open(QIODevice::ReadWrite);
    exporter->write(&b, constifyAndSort(m_document->containers()->list()));
    b.reset();

    TestFileContents tf;

    QFile controldata(QFINDTESTDATA(expectedoutput));
    QVERIFY(controldata.exists());
    bool success = controldata.open(QIODevice::ReadOnly);
    QVERIFY(success);
    tf.setHeaderLineCount(headerlinecount);
    tf.setFooterLineCount(footerlinecount);
    ERUN(tf.setControlData(&controldata));
    ERUN(tf.setTestData(&b));
    ERUN(tf.compare());

    delete exporter;
}

void LegoShipEdiExport::testExporters_data() {
    QTest::addColumn<QString>("expectedoutput");
    QTest::addColumn<QString>("outputtype");
    QTest::addColumn<QString>("callstring");
    QTest::addColumn<int>("headerlinecount");
    QTest::addColumn<int>("footerlinecount");

    QTest::newRow("BAPLIE AAAAA") << "report-Lego Maersk Small-AAAAA-0001/Lego Maersk Small-AAAAA-0001.BAPLIE.edi" << "BAPLIE" << "AAAAA" << 8 << 2;
    QTest::newRow("BAPLIE BBBBB") << "report-Lego Maersk Small-BBBBB-0001/Lego Maersk Small-BBBBB-0001.BAPLIE.edi" << "BAPLIE" << "BBBBB" << 8 << 2;
    QTest::newRow("BAPLIE CCCCC") << "report-Lego Maersk Small-CCCCC-0001/Lego Maersk Small-CCCCC-0001.BAPLIE.edi" << "BAPLIE" << "CCCCC" << 8 << 2;
    QTest::newRow("BAPLIE DDDDD") << "report-Lego Maersk Small-DDDDD-0001/Lego Maersk Small-DDDDD-0001.BAPLIE.edi" << "BAPLIE" << "DDDDD" << 8 << 2;

    QTest::newRow("MOVINS AAAAA") << "report-Lego Maersk Small-AAAAA-0001/Lego Maersk Small-AAAAA-0001.MOVINS.edi" << "MOVINS" << "AAAAA" << 4 << 2;
    QTest::newRow("MOVINS BBBBB") << "report-Lego Maersk Small-BBBBB-0001/Lego Maersk Small-BBBBB-0001.MOVINS.edi" << "MOVINS" << "BBBBB" << 4 << 2;
    QTest::newRow("MOVINS CCCCC") << "report-Lego Maersk Small-CCCCC-0001/Lego Maersk Small-CCCCC-0001.MOVINS.edi" << "MOVINS" << "CCCCC" << 4 << 2;
    QTest::newRow("MOVINS DDDDD") << "report-Lego Maersk Small-DDDDD-0001/Lego Maersk Small-DDDDD-0001.MOVINS.edi" << "MOVINS" << "DDDDD" << 4 << 2;

    QTest::newRow("BAPLIE15 AAAAA") << "report-Lego Maersk Small-AAAAA-0001/Lego Maersk Small-AAAAA-0001.BAPLIE15.edi" << "BAPLIE15" << "AAAAA" << 8 << 2;
    QTest::newRow("BAPLIE15 BBBBB") << "report-Lego Maersk Small-BBBBB-0001/Lego Maersk Small-BBBBB-0001.BAPLIE15.edi" << "BAPLIE15" << "BBBBB" << 8 << 2;
    QTest::newRow("BAPLIE15 CCCCC") << "report-Lego Maersk Small-CCCCC-0001/Lego Maersk Small-CCCCC-0001.BAPLIE15.edi" << "BAPLIE15" << "CCCCC" << 8 << 2;
    QTest::newRow("BAPLIE15 DDDDD") << "report-Lego Maersk Small-DDDDD-0001/Lego Maersk Small-DDDDD-0001.BAPLIE15.edi" << "BAPLIE15" << "DDDDD" << 8 << 2;

    QTest::newRow("COPRAR AAAAA") << "report-Lego Maersk Small-AAAAA-0001/Lego Maersk Small-AAAAA-0001.COPRAR.edi" << "COPRAR" << "AAAAA" << 3 << 2;
    QTest::newRow("COPRAR BBBBB") << "report-Lego Maersk Small-BBBBB-0001/Lego Maersk Small-BBBBB-0001.COPRAR.edi" << "COPRAR" << "BBBBB" << 3 << 2;
    QTest::newRow("COPRAR CCCCC") << "report-Lego Maersk Small-CCCCC-0001/Lego Maersk Small-CCCCC-0001.COPRAR.edi" << "COPRAR" << "CCCCC" << 3 << 2;
    QTest::newRow("COPRAR DDDDD") << "report-Lego Maersk Small-DDDDD-0001/Lego Maersk Small-DDDDD-0001.COPRAR.edi" << "COPRAR" << "DDDDD" << 3 << 2;
}

QTEST_GUILESS_MAIN(LegoShipEdiExport);

#include "legoshipediexport.moc"
