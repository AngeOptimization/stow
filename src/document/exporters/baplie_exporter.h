#ifndef BAPLIE_EXPORTER_H
#define BAPLIE_EXPORTER_H
#include "abstract_exporter_base.h"

class baplie_exporter_t : public abstract_exporter_base_t {
  public:
    /**
    * export class for baplie. uses edi/baplie_writer for actual writing. This is just for wrapping and extracting the right
    * informations from the document.
    * Neither documnet nor call can be null.
    */
    baplie_exporter_t(const document_t* document, QString voyage_number, QString carrier_code, QString vessel_code, const ange::schedule::Call* current_call);
    /**
     * actually does the export. Assumes a opened QIODevice
     */
    virtual void write(QIODevice* device, QList< const ange::containers::Container* > containers);
};

#endif // BAPLIE_EXPORTER_H
