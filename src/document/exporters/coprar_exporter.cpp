#include "coprar_exporter.h"
#include "document/stowage/stowage.h"
#include "document/stowage/container_leg.h"
#include "document/document.h"
#include <edi/coprar_writer.h>
#include "document/containers/container_list.h"
#include <ange/vessel/vessel.h>

coprar_exporter_t::coprar_exporter_t(const document_t* document, QString voyage_number, QString carrier_code, const ange::schedule::Call* current_call)
 : abstract_exporter_base_t(document, voyage_number ,carrier_code, document->vessel()->vesselCode(), current_call) {

}


void coprar_exporter_t::write(QIODevice* device) {
  QList<const ange::containers::Container*> containers_sourced_at_current_port;

  Q_FOREACH(const ange::containers::Container* container, document()->containers()->list()) {
    if (document()->stowage()->container_routes()->portOfLoad(container) == current_call()) {
      containers_sourced_at_current_port << container;
    }
  }
  write(device, containers_sourced_at_current_port);
}

void coprar_exporter_t::write(QIODevice* device, QList< const ange::containers::Container* > containers)
{
  coprar_writer_t cw(device,document()->vessel(), voyage_number(),carrier_code(), current_call());
  cw.start();
  QList<const ange::containers::Container*> containers_sourced_at_current_port;

  Q_FOREACH(const ange::containers::Container* container, containers) {
    if (document()->stowage()->container_routes()->portOfLoad(container) == current_call()) {
      cw.add_container(container);
    }
  }
  cw.end();

}
