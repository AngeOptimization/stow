#include "abstract_exporter_base.h"
#include "document/document.h"
#include <ange/schedule/schedule.h>
#include <ange/schedule/call.h>
#include "document/utils/callutils.h"

abstract_exporter_base_t::abstract_exporter_base_t(const document_t* document, QString voyage_number, QString carrier_code, QString vessel_code, const ange::schedule::Call* current_call)
 : m_document(document),
   m_voyage_number(voyage_number),
   m_carrier_code(carrier_code),
   m_vessel_code(vessel_code),
   m_current_call(current_call)
{
  Q_ASSERT(m_document);
  Q_ASSERT(m_current_call);
}

abstract_exporter_base_t::~abstract_exporter_base_t() {

}

