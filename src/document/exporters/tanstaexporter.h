#ifndef TANSTAEXPORTER_H
#define TANSTAEXPORTER_H
#include <QString>

namespace ange {
    namespace schedule {
        class Call;
    } // namespace schedule
} // namespace ange

class document_t;
class QIODevice;
class TanstaExporter {
    public:
        TanstaExporter(document_t* document, QString voyage_number, QString carrier_code, const ange::schedule::Call* current_call);
        bool write(QIODevice* device);
    private:
        document_t* m_document;
        QString m_voyageNumber;
        QString m_carrierCode;
        QString m_vesselCode;
        const ange::schedule::Call* m_currentCall;
};

#endif // TANSTAEXPORTER_H
