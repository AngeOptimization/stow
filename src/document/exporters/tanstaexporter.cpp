#include "tanstaexporter.h"
#include "document/document.h"
#include "document/tanks/tankconditions.h"
#include <tanstawriter.h>
#include <ange/vessel/vessel.h>
#include <ange/schedule/schedule.h>

TanstaExporter::TanstaExporter(document_t* document, QString voyage_number,
                               QString carrier_code,
                               const ange::schedule::Call* current_call)
        : m_document(document), m_voyageNumber(voyage_number), m_carrierCode(carrier_code), m_currentCall(current_call) {
    Q_ASSERT(current_call);
    Q_ASSERT(document);
}

bool TanstaExporter::write(QIODevice* device) {
    const ange::schedule::Call* next_call = m_document->schedule()->next(m_currentCall);
    if(!next_call) {
        return false; //what to do here ?
    }
    TanstaWriter writer(device, m_document->vessel(), m_currentCall, next_call,
                                m_voyageNumber, m_carrierCode);
    writer.start();
    Q_FOREACH(const ange::vessel::VesselTank* tank, m_document->vessel()->tanks()) {
        ange::units::Mass content = m_document->tankConditions()->tankCondition(m_currentCall, tank);
        writer.addCondition(tank,content);
    }
    writer.end();
    return true;
}
