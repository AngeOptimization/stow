#include "movins_exporter.h"

#include <QIODevice>
#include <edi/movins_writer.h>

#include <ange/schedule/schedule.h>

#include "document/document.h"
#include "document/containers/container_list.h"

#include "document/stowage/stowage.h"
#include "document/stowage/container_leg.h"

movins_exporter_t::movins_exporter_t(const document_t* document, QString voyage_number, QString carrier_code, QString vessel_code, const ange::schedule::Call* current_call) :
                                     abstract_exporter_base_t(document,voyage_number,carrier_code,vessel_code,current_call) {
}


struct restow_to_load_t {
  ange::vessel::BayRowTier new_brt;
  const ange::containers::Container* container;
};


void movins_exporter_t::write(QIODevice* device, QList< const ange::containers::Container* > containers)
{
  const ange::schedule::Call* previous_call = document()->schedule()->previous(current_call());
  movins_writer_t mw(device,document()->vessel(),document()->schedule(),current_call(),
                        vessel_code(),voyage_number(),carrier_code());
  mw.start();

  Q_FOREACH(const ange::containers::Container* container, containers) {
    if(!previous_call) {
      continue; // erm. what to really do here? writing movins for Befor
    }
    if (document()->stowage()->container_routes()->portOfDischarge(container) == current_call()) {
      if (document()->stowage()->moves(container).empty()) {
        continue; // Do not write unplaced containers
      }

      ange::vessel::BayRowTier brt = document()->stowage()->nominal_position(container, previous_call);
      mw.disch_container(container,document()->stowage()->container_routes()->portOfLoad(container),brt);
    }
  }
  Q_FOREACH(const ange::containers::Container* container, containers) {
    if (document()->stowage()->container_routes()->portOfLoad(container) == current_call()) {
      if (document()->stowage()->moves(container).empty()) {
        continue; // Do not write unplaced containers
      }
      ange::vessel::BayRowTier brt = document()->stowage()->nominal_position(container, current_call());
      mw.load_container(container,document()->stowage()->container_routes()->portOfDischarge(container),brt);
    }
  }
  QList<restow_to_load_t> restows_to_load;
  Q_FOREACH(const ange::containers::Container* container, containers) {
    if(document()->stowage()->container_routes()->portOfLoad(container)!=current_call() &&
              document()->stowage()->container_routes()->portOfDischarge(container)!=current_call()) {
      if(document()->stowage()->container_moved_at_call(container, current_call())) {
        ange::vessel::BayRowTier old_brt = document()->stowage()->nominal_position(container,previous_call);
        ange::vessel::BayRowTier new_brt = document()->stowage()->nominal_position(container,current_call());
        mw.restow_container(container,document()->stowage()->container_routes()->portOfDischarge(container),old_brt,new_brt);
        restow_to_load_t tmp;
        tmp.new_brt = new_brt;
        tmp.container = container;
        restows_to_load << tmp;
      }
    }
  }
  Q_FOREACH(const restow_to_load_t& restow, restows_to_load) {
    mw.load_container(restow.container,document()->stowage()->container_routes()->portOfDischarge(restow.container),restow.new_brt);
  }
  mw.end();

}
