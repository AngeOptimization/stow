#include "schedule_wildcard_proxy_model.h"

#include <QItemSelectionModel>

QVariant ScheduleWildcardProxyModel::data(const QModelIndex& proxyIndex, int role) const {
    if(!proxyIndex.isValid()) {
        return QVariant();
    }
    if(proxyIndex.row()==0) {
        if(role == Qt::DisplayRole) {
            return QString("%");
        } else {
            return QVariant();
        }
    }
    return sourceModel()->data(mapToSource(proxyIndex), role);
}

int ScheduleWildcardProxyModel::rowCount(const QModelIndex& parent) const {
    if (!sourceModel()) {
        return 1;
    }
    int sourceRowCount = sourceModel()->rowCount(parent);
    return sourceRowCount + 1;
}

int ScheduleWildcardProxyModel::columnCount(const QModelIndex& parent) const {
    if (!sourceModel()) {
        return 0;
    }
    return sourceModel()->columnCount(parent);
}

QModelIndex ScheduleWildcardProxyModel::index(int row, int column, const QModelIndex& /*parent*/) const {
    if(!hasIndex(row,column)) {
        return QModelIndex();
    }
    QModelIndex idx = createIndex(row,column);
    return idx;
}

QModelIndex ScheduleWildcardProxyModel::parent(const QModelIndex& /*child*/) const {
    return QModelIndex();
}

QModelIndex ScheduleWildcardProxyModel::mapFromSource(const QModelIndex& sourceIndex) const {
    if(!sourceIndex.isValid()) {
        return QModelIndex();
    }
    return index(sourceIndex.row()+1,sourceIndex.column());
}

QModelIndex ScheduleWildcardProxyModel::mapToSource(const QModelIndex& proxyIndex) const {
    if(!proxyIndex.isValid()) {
        return QModelIndex();
    }
    if(proxyIndex.row() == 0) {
        return QModelIndex();
    }
    return sourceModel()->index(proxyIndex.row()-1,proxyIndex.column());
}

QVariant ScheduleWildcardProxyModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(orientation==Qt::Horizontal) {
        return sourceModel()->headerData(section, orientation, role);
    } else {
        if(section==0) {
            QVariant();
        }
        return sourceModel()->headerData(section-1, orientation, role);
    }
}

QItemSelection ScheduleWildcardProxyModel::mapSelectionFromSource(const QItemSelection& selection) const {
    QItemSelection newselection;
    Q_FOREACH(const QItemSelectionRange& range, selection) {
        newselection << QItemSelectionRange(mapFromSource(range.topLeft()), mapFromSource(range.bottomRight()));
    }
    return newselection;
}

QItemSelection ScheduleWildcardProxyModel::mapSelectionToSource(const QItemSelection& selection) const{
    QItemSelection newselection;
    Q_FOREACH(const QItemSelectionRange& range, selection) {
        newselection << QItemSelectionRange(mapToSource(range.topLeft()), mapToSource(range.bottomRight()));
    }
    return newselection;
}

Qt::ItemFlags ScheduleWildcardProxyModel::flags(const QModelIndex& index) const {
    if(index.row()==0) {
        return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
    } else {
        return sourceModel()->flags(mapToSource(index));
    }
}

bool ScheduleWildcardProxyModel::setData(const QModelIndex& index, const QVariant& value, int role) {
    if(index.row()==0) {
        return false;
    } else {
        return sourceModel()->setData(mapToSource(index), value, role);
    }
}

void ScheduleWildcardProxyModel::setSourceModel(QAbstractItemModel* sourceModel) {
    beginResetModel();
    QAbstractProxyModel::setSourceModel(sourceModel);
    endResetModel();
}

#include "schedule_wildcard_proxy_model.moc"
