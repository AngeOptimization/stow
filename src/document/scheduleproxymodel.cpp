#include "scheduleproxymodel.h"

#include <ange/schedule/schedule.h>
#include <ange/schedule/call.h>
#include <stdexcept>
#include <QApplication>
#include <QPalette>

using ange::schedule::Call;

ScheduleProxyModel::ScheduleProxyModel(QObject* parent)
    : QIdentityProxyModel(parent)
{
    // Do nothing
}

void ScheduleProxyModel::setSchedule(ange::schedule::Schedule* schedule) {
    m_schedule = schedule;
    resetInternalData();
}

void ScheduleProxyModel::setLoadCall(const Call* call) {
    if (m_loadCall.data() != call) {
        m_loadCall = call;
        resetInternalData();
    }
}

void ScheduleProxyModel::setDischargeCall(const Call* call) {
    if (m_dischargeCall.data() != call) {
        m_dischargeCall = call;
        resetInternalData();
    }
}

static Call* getSourceCall(int sourceRow, QAbstractItemModel* sourceModel) {
    return sourceModel->index(sourceRow, 0).data(Qt::UserRole).value<ange::schedule::Call*>();
}

bool ScheduleProxyModel::less(const Call* call1, const Call* call2) const {
    try {
        return m_schedule.data()->less(call1, call2);
    } catch (std::runtime_error&) {
        Q_ASSERT(false);  // call not in schedule
        return true;
    }
}

QVariant ScheduleProxyModel::data(const QModelIndex& index, int role) const {
    if (!index.isValid()) {
        return QVariant();
    }
    if (!filterAcceptsRow(index.row())) {
        switch (role) {
            case Qt::ForegroundRole:
                return QApplication::palette().color(QPalette::Disabled, QPalette::Foreground);
            case Qt::BackgroundRole:
                return QApplication::palette().color(QPalette::Disabled, QPalette::Background);
        }
    }
    return QAbstractProxyModel::data(index, role);
}

Qt::ItemFlags ScheduleProxyModel::flags(const QModelIndex& index) const {
    Qt::ItemFlags flags = QAbstractProxyModel::flags(index);
    if (!filterAcceptsRow(index.row())) {
        flags &= ~Qt::ItemIsEnabled;
    }
    return flags;
}


bool ScheduleProxyModel::filterAcceptsRow(int sourceRow) const {
    if (!m_schedule) {
        Q_ASSERT(false);
        return true;  // Show all calls when no schedule configured
    }
    if (m_loadCall || m_dischargeCall) {
        ange::schedule::Call* sourceCall = getSourceCall(sourceRow, sourceModel());
        return (!m_loadCall || less(m_loadCall.data(), sourceCall))
            && (!m_dischargeCall || less(sourceCall, m_dischargeCall.data()));
    } else {
        return true;
    }
}

#include "scheduleproxymodel.moc"
