#include "callformatterproxymodel.h"
#include <ange/schedule/call.h>

using ange::schedule::Call;

CallFormatterProxyModel::CallFormatterProxyModel(QObject* parent) : QIdentityProxyModel(parent) {
    // empty
}

Qt::ItemFlags CallFormatterProxyModel::flags(QModelIndex const& index) const {
    return super::flags(index) ^ Qt::ItemIsEditable;
}

QVariant CallFormatterProxyModel::data(const QModelIndex& proxyIndex, int role) const {
    if(!proxyIndex.isValid()) {
        return QVariant();
    }
    Q_ASSERT(!proxyIndex.parent().isValid()); // no trees supported
    if(role == Qt::DisplayRole && proxyIndex.column() == 0) {
        QModelIndex sourceIndex = sourceModel()->index(proxyIndex.row(), 0);
        const Call* call = sourceIndex.data(Qt::UserRole).value<Call*>();
        Q_ASSERT(call);
        if(!call) {
            return "No call found";
        }
        return call->assembledName();
    }
    return QAbstractProxyModel::data(proxyIndex, role);
}

#include "callformatterproxymodel.moc"
