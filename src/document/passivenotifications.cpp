#include "passivenotifications.h"

#include "document.h"

#include <QIcon>
#include <QUndoStack>

// PassiveNotification:

PassiveNotification::PassiveNotification(const QString& message, const QString& buttonText, const QIcon& buttonIcon,
                                         QObject* buttonReceiver, const char* buttonSlot)
  : message(message), buttonText(buttonText), buttonIcon(buttonIcon), buttonReceiver(buttonReceiver),
    buttonSlot(buttonSlot)
{
    // Empty
}

// PassiveNotifications:

PassiveNotifications::PassiveNotifications(document_t* document)
  : QObject(document), m_document(document)
{
    // Empty
}

PassiveNotifications::~PassiveNotifications() {
    // Empty
}

void PassiveNotifications::addWarning(const QString& message, const QString& buttonText, const QIcon& buttonIcon,
                                      QObject* buttonReceiver, const char* buttonSlot) {
    emit warningAdded(PassiveNotification(message, buttonText, buttonIcon, buttonReceiver, buttonSlot));
}

void PassiveNotifications::addWarningWithUndo(const QString& message) {
    static const QIcon undoIcon(":/icons/icons/edit-undo.png");
    emit warningAdded(PassiveNotification(message, "Undo", undoIcon, m_document->undostack(), SLOT(undo())));
}

#include "passivenotifications.moc"
