#include "../callformatterproxymodel.h"
#include <schedulemodel.h>
#include <QTest>
#include <ange/schedule/schedule.h>
#include <ange/schedule/call.h>

using ange::schedule::Schedule;
using ange::schedule::Call;

class TestCallFormatterProxyModel : public QObject {
    Q_OBJECT
    private Q_SLOTS:
        void testFormatters();
};

Schedule* buildSchedule() {
    QList<Call*> calls;
    calls << new Call("Call1","VOY1");
    calls << new Call("Call2","VOY1");
    calls << new Call("Call1","VOY2");
    calls << new Call("Call2","VOY2");
    return new Schedule(calls);
}

void TestCallFormatterProxyModel::testFormatters() {
    Schedule* schedule = buildSchedule();

    ScheduleModel* model = new ScheduleModel(schedule,0,0);

    CallFormatterProxyModel* proxy = new CallFormatterProxyModel();

    proxy->setSourceModel(model);

    QCOMPARE(proxy->rowCount(), 4);

    QCOMPARE(proxy->data(proxy->index(0,0), Qt::DisplayRole).toString(),QStringLiteral("Call1/VOY1"));
    QCOMPARE(proxy->data(proxy->index(1,0), Qt::DisplayRole).toString(),QStringLiteral("Call2/VOY1"));
    QCOMPARE(proxy->data(proxy->index(2,0), Qt::DisplayRole).toString(),QStringLiteral("Call1/VOY2"));
    QCOMPARE(proxy->data(proxy->index(3,0), Qt::DisplayRole).toString(),QStringLiteral("Call2/VOY2"));
    QVERIFY(!proxy->data(proxy->index(4,0), Qt::DisplayRole).isValid());

    delete proxy;
    delete model;
    delete schedule;
}

QTEST_GUILESS_MAIN(TestCallFormatterProxyModel);

#include "testcallformatterproxymodel.moc"
