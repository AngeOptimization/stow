#include "userconfigtest.h"
#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>
#include <document/userconfiguration/userconfiguration.h>
#include <QTest>
#include <QList>

using namespace ange::units;
using ange::schedule::Call;
using ange::schedule::Schedule;

void UserConfigTest::initTestCase(){
}

void UserConfigTest::testPortWaterDensities() {
    Call callNotDefined("ZZZZZ", "VOYAGE");
    Call callDeHam("DEHAM", "VOYAGE");

    QList< Call* > calls;
    Schedule schedule(calls, 0);
    UserConfiguration userconfiguration(&schedule, 0);

    QCOMPARE(userconfiguration.defaultWaterDensityByCall(&callNotDefined) / (kilogram/meter3), 1025.0);
    QCOMPARE(userconfiguration.defaultWaterDensityByCall(&callDeHam) / (kilogram/meter3), 1005.0);
}


UserConfigTest::~UserConfigTest(){
}

QTEST_GUILESS_MAIN(UserConfigTest);
#include "userconfigtest.moc"
