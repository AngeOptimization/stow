#include <QTest>
#include <QUndoStack>
#include <ange/schedule/schedule.h>
#include <ange/schedule/call.h>
#include <schedulemodel.h>
#include <userconfiguration.h>

using ange::schedule::Schedule;
using ange::schedule::Call;

class ScheduleModelTest : public QObject {
    Q_OBJECT
    public:
    private Q_SLOTS:
        void moveDown();
        void moveUp();
};

Schedule* buildSchedule() {
    Call* callA = new Call("A", "");
    Call* callB = new Call("B", "");
    Call* callC = new Call("C", "");
    Call* callD = new Call("D", "");
    Call* callE = new Call("E", "");
    Call* callF = new Call("F", "");
    Call* callG = new Call("G", "");
    Schedule* schedule = new Schedule(QList<Call*>() << callA << callB << callC << callD << callE << callF << callG);
    schedule->setPivotCall(schedule->size()-1); // needed for running schedulemodel with modeltest
    return schedule;
}


void ScheduleModelTest::moveDown() {
    Schedule* schedule = buildSchedule();
    UserConfiguration* userconf = new UserConfiguration(schedule,this);
    QUndoStack undostack;
    ScheduleModel* schedulemodel = new ScheduleModel(schedule, userconf, &undostack);

    schedule->changeOfRotation(2,3);

    delete schedulemodel;
    delete userconf;
    delete schedule;
}

void ScheduleModelTest::moveUp() {
    Schedule* schedule = buildSchedule();
    UserConfiguration* userconf = new UserConfiguration(schedule,this);
    QUndoStack undostack;
    ScheduleModel* schedulemodel = new ScheduleModel(schedule, userconf, &undostack);

    schedule->changeOfRotation(2,1);

    delete schedulemodel;
    delete userconf;
    delete schedule;
}

QTEST_GUILESS_MAIN(ScheduleModelTest);

#include "schedulemodeltest.moc"
