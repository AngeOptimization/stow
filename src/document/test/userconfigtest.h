#ifndef USERCONFIGTEST_H
#define USERCONFIGTEST_H
#include <QObject>

class UserConfigTest : public QObject {
    Q_OBJECT
public:
    virtual ~UserConfigTest();
private Q_SLOTS:
    void initTestCase();
    void testPortWaterDensities();
private:
};

#endif //USERCONFIGTEST_H
