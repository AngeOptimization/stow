#ifndef TANKCONDITIONS_H
#define TANKCONDITIONS_H

#include <QObject>
#include <QHash>

#include <ange/units/units.h>

namespace ange {
namespace schedule {
class Call;
}
namespace vessel {
class Vessel;
class VesselTank;
}
}

class TankConditions : public QObject {

    Q_OBJECT

public:

    TankConditions(const ange::vessel::Vessel* vessel, QObject* parent);

    ~TankConditions();

    void setVessel(const ange::vessel::Vessel* vessel);

    /**
     * @return tank condition for tank
     */
    ange::units::Mass tankCondition(const ange::schedule::Call* call, const ange::vessel::VesselTank* tank) const;

    /**
     * @return tank conditions for all tanks at call
     */
    QHash<const ange::vessel::VesselTank*, ange::units::Mass> tankConditions(const ange::schedule::Call* call) const;

    /**
     * Set tank condition for tank.
     */
    void changeTankCondition(const ange::schedule::Call* call, const ange::vessel::VesselTank* tank,
                             ange::units::Mass condition);

Q_SIGNALS:

    /**
     * A tank has changed it's level of fluid
     */
    void tankConditionChanged(const ange::schedule::Call* call, const ange::vessel::VesselTank* tank,
                              ange::units::Mass newValue);

public Q_SLOTS:

    void removeCall(ange::schedule::Call* call);

private:

    const ange::vessel::Vessel* m_vessel;

    QHash<const ange::schedule::Call*, QHash<const ange::vessel::VesselTank*, ange::units::Mass> > m_tankCondition;

};

#endif // TANKCONDITIONS_H
