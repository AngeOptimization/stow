#ifndef TANKMODEL_H
#define TANKMODEL_H

#include <QAbstractTableModel>
#include <ange/units/units.h>

namespace ange {
    namespace vessel {
        class Vessel;
        class VesselTank;
    } // namespace vessel
    namespace schedule {
        class Call;
    } // namespace schedule
} // namespace ange

class document_t;

/**
 * Table model on top of the tank conditions
 */
class TankModel : public QAbstractTableModel {
    typedef QAbstractTableModel super;

    Q_OBJECT

    public:
        TankModel(document_t* document, QObject* parent);
        /**
         * Accessor to the tank list
         */
        QList</*const // lol*/ ange::vessel::VesselTank*> vesselTanks() const;
        /**
         * sets the current viewed call to \param call
         */
        void setCurrentCall(const ange::schedule::Call* call);
        virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
        virtual int columnCount(const QModelIndex& parent = QModelIndex()) const Q_DECL_OVERRIDE;
        virtual QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
        virtual int rowCount(const QModelIndex& parent = QModelIndex()) const Q_DECL_OVERRIDE;
        virtual bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) Q_DECL_OVERRIDE;
        virtual Qt::ItemFlags flags(const QModelIndex& index) const Q_DECL_OVERRIDE;
        enum Columns {
            TankDescription,
            TankGroup,
            TankMass,
            TankDensity,
            TankCapacity
        };
    private:
        document_t* m_document;
        const ange::schedule::Call* m_currentCall;
    private Q_SLOTS:
        /**
         * Notifies the views on tank changes
         */
        void tankChanged(const ange::schedule::Call*,const ange::vessel::VesselTank*,ange::units::Mass);
        /**
         * Updates stuff when vessel changes in the document
         */
        void setVessel(const ange::vessel::Vessel* vessel);
};

#endif // TANKMODEL_H
