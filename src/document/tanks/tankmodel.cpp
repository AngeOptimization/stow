#include "tankmodel.h"

#include "tankconditions.h"
#include "document/document.h"
#include "document/undo/tank_state_command.h"
#include "document/utils/modelutils.h"

#include <ange/vessel/vessel.h>
#include <ange/schedule/call.h>
#include <ange/vessel/vesseltank.h>

#include <QColor>

using namespace ange::units;
using ange::vessel::VesselTank;

void TankModel::setVessel(const ange::vessel::Vessel* vessel) {
    Q_UNUSED(vessel);
    Q_ASSERT(vessel);
    beginResetModel();
    endResetModel();
}

TankModel::TankModel(document_t* document, QObject* parent): QAbstractTableModel(parent), m_document(document), m_currentCall(0) {
    connect(document, &document_t::vessel_changed, this, &TankModel::setVessel);
    connect(document->tankConditions(), &TankConditions::tankConditionChanged, this, &TankModel::tankChanged);
}

void TankModel::setCurrentCall(const ange::schedule::Call* call) {
    beginResetModel();
    m_currentCall = call;
    endResetModel();
}


int TankModel::columnCount(const QModelIndex& parent) const {
    if(!m_currentCall) {
        return 0;
    }
    Q_UNUSED(parent);
    return 5;
}

int TankModel::rowCount(const QModelIndex& parent) const {
    if(!m_currentCall) {
        return 0;
    }
    Q_UNUSED(parent);
    return m_document->vessel()->tanks().count();
}

QVariant TankModel::data(const QModelIndex& index, int role) const {
    if (!index.isValid()) {
        return QVariant();
    }
    VesselTank* tank = m_document->vessel()->tanks().at(index.row());
    switch (role) {
        case Qt::EditRole: {
            switch (index.column()) {
                case TankMass:
                    return QString("%1").arg(m_document->tankConditions()->tankCondition(m_currentCall, tank) / ton);
                default:
                    Q_ASSERT(false);
                    return QVariant();
            }
        }
        case Qt::DisplayRole: {
            switch (index.column()) {
                case TankDescription:
                    return tank->description();
                case TankGroup:
                    return tank->tankGroup();
                case TankMass:
                    return QString("%1 t").arg(m_document->tankConditions()->tankCondition(m_currentCall, tank) / ton);
                case TankDensity:
                    return QString("%1 kg/m³").arg(tank->density());
                case TankCapacity:
                    return QString("%1 t").arg(qRound(tank->capacity() / 1000)); // display in ton
                default:
                    Q_ASSERT(false);
                    return QVariant();
            }
        }
        case Qt::TextAlignmentRole: {
            switch (index.column()) {
                case TankMass:
                case TankDensity:
                case TankCapacity:
                    return QVariant(Qt::AlignRight | Qt::AlignVCenter);
                default:
                    return QVariant(Qt::AlignLeft | Qt::AlignVCenter);
            }
        }
        case Qt::UserRole: {
            return QVariant::fromValue<const ange::vessel::VesselTank*>(tank);
        }
        default: {
            switch (index.column()) {
                case TankMass: {
                    double content = m_document->tankConditions()->tankCondition(m_currentCall, tank) / kilogram;
                    if (content > tank->capacity()) {
                        return ModelUtils::limitBrokenCellStyle(role);
                    } else {
                        return QVariant();
                    }
                }
                default:
                    return QVariant();
            }
        }
    }
}

bool TankModel::setData(const QModelIndex& index, const QVariant& value, int role) {
    Q_ASSERT(role == Qt::EditRole);
    if (index.column() == TankMass) {
        if (!m_currentCall) {
            Q_ASSERT(false);
            return false;
        }
        bool ok;
        double d = value.toDouble(&ok);
        if (!ok || d < 0) {
            return false;
        }
        VesselTank* tank = m_document->vessel()->tanks().at(index.row());
        if (d * 1000 > tank->capacity() * 1.1) { // accept overfill up to 1.1 of tank capacity, then refuse more overfill
            d = qRound(tank->capacity() * 1.1 / 1000);
        }
        Mass newContent = d * ton;
        tank_state_command_t* command = new tank_state_command_t(m_document, m_currentCall);
        command->insert_tank_condition(m_document->vessel()->tanks().at(index.row()), newContent);
        m_document->undostack()->push(command);
        return true;
    } else {
        return super::setData(index, value, role);
    }
}

Qt::ItemFlags TankModel::flags(const QModelIndex& index) const {
    if(index.column() == TankMass ) {
        return super::flags(index) | Qt::ItemIsEditable;
    }
    return super::flags(index);
}

void TankModel::tankChanged(const ange::schedule::Call* call, const ange::vessel::VesselTank* tank, ange::units::Mass newMass) {
    Q_UNUSED(newMass);
    if(call != m_currentCall) {
        return;
    }
    int row = m_document->vessel()->tanks().indexOf(const_cast<ange::vessel::VesselTank*>(tank));
    dataChanged(index(row,TankMass), index(row,TankMass));
}

QVariant TankModel::headerData(int section, Qt::Orientation orientation, int role) const {
    if (orientation != Qt::Horizontal) {
        return QAbstractItemModel::headerData(section, orientation, role);
    }
    if (role == Qt::DisplayRole) {
        switch (static_cast<Columns>(section)) {
            case TankDensity:
                return QStringLiteral("Density");
            case TankDescription:
                return QStringLiteral("Name");
            case TankGroup:
                return QStringLiteral("Group");
            case TankMass:
                return QStringLiteral("Content");
            case TankCapacity:
                return QStringLiteral("Capacity");
            default:
                Q_ASSERT(false);
                return QStringLiteral("??");
        }
    }
    switch (section) {
        case TankMass:
            return ModelUtils::editableHeaderStyle(role);
        default:
            return ModelUtils::nonEditableHeaderStyle(role);
    }
}

QList<VesselTank*> TankModel::vesselTanks() const {
    return m_document->vessel()->tanks();
}

#include "tankmodel.moc"
