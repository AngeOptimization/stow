#include "tankconditions.h"

#include <ange/units/units.h>
#include <ange/vessel/vessel.h>

#include <QHash>
#include <QDebug>

using namespace ange::units;
using ange::schedule::Call;
using ange::vessel::Vessel;
using ange::vessel::VesselTank;

TankConditions::TankConditions(const Vessel* vessel, QObject* parent)
  : QObject(parent), m_vessel(vessel)
{
    setObjectName(QStringLiteral("TankConditions"));
}

TankConditions::~TankConditions() {
    // Empty
}

void TankConditions::changeTankCondition(const Call* call, const VesselTank* tank, Mass condition) {
    Q_ASSERT(call);
    Q_ASSERT(tank);
    if (!tank) {
        qWarning() << "tank is null";
        return;
    }
    if (!(condition >= 0.0 * ton)) {
        qWarning() << "Tank condition is negative";
        Q_ASSERT(false);
        return;
    }
    Mass& conditionReference = m_tankCondition[call][tank];
    if (conditionReference != condition) {
        conditionReference = condition;
        emit tankConditionChanged(call, tank, condition);
    }
}

void TankConditions::setVessel(const Vessel* vessel) {
    m_tankCondition.clear();
    m_vessel = vessel;
}

Mass TankConditions::tankCondition(const Call* call, const VesselTank* tank) const {
    Q_ASSERT(call);
    Q_ASSERT(tank);
    if (!tank) {
        qWarning() << "tank is null";
        return qQNaN() * ton;
    }
    return m_tankCondition.value(call).value(tank, 0.0 * ton); // Tanks are empty by deafult, NaN will break tests
}

void TankConditions::removeCall(Call* call) {
    m_tankCondition.remove(call);
}

QHash<const VesselTank*, Mass> TankConditions::tankConditions(const Call* call) const {
    return m_tankCondition.value(call);
}

#include "tankconditions.moc"
