#include "container_move.h"

#include <ange/vessel/slot.h>
#include <ange/schedule/call.h>
#include <ange/containers/container.h>

using namespace ange::angelstow;

container_move_t::container_move_t(const ange::containers::Container* container,
                                   const ange::vessel::Slot* target,
                                   const ange::schedule::Call* call,
                                   const StowType& type)
 :  m_container(container),
    m_move(target,call,type)
{
  Q_ASSERT(call);
  Q_ASSERT(container);
}

container_move_t::container_move_t(const ange::containers::Container* container, const ange::angelstow::Move& move)
 : m_container(container),
    m_move(move)
{
    // Empty
}

container_move_t::container_move_t(const container_move_t& other)
  : m_container(other.m_container), m_move(other.m_move)
{
    // Empty
}

QDebug operator<<(QDebug dbg, const container_move_t* move) {
  if (move) {
    dbg << *move;
  } else {
    dbg << "<null>";
  }
  return dbg;
}

QDebug operator<<(QDebug dbg, const container_move_t& move) {
  dbg << "(";
  if(move.container()) {
    dbg << move.container()->equipmentNumber() << ":" ;
  }
  if(move.slot()) {
    dbg << move.slot() << "/";
  }
  if(move.call()) {
    dbg << move.call()->uncode();
  }
  if(move.type()==MicroStowedType) {
  } else if(move.type()==MasterPlannedType) {
    dbg << "(m)";
  } else if(move.type()==NonPlacedType) {
      dbg << "(up)";
  } else {
    Q_ASSERT(false);
  }
  dbg << ")";
  return dbg;

}

bool container_move_t::operator<(const container_move_t& rhs) const {
  if (call() != rhs.call()) {
    return (call()->distance(rhs.call()) > 0);
  }
  if (m_container->id() != rhs.container()->id()) {
    return m_container->id() < rhs.container()->id();
  }
  if (slot() != rhs.slot()) {
    return rhs.slot(); //Discharge before load in same call
  }
  Q_ASSERT(type() == rhs.type()); // If we get this far, these moves should be identical
  return false;
}

void container_move_t::set_stow_type(ange::angelstow::StowType type)
{
  m_move.m_type = type;
}
