#ifndef ABSTRACTLASHINGPATTERN_H
#define ABSTRACTLASHINGPATTERN_H

#include <stowplugininterface/lashingpatterninterface.h>

#include <ange/vessel/direction.h>

#include <QMultiHash>

class document_interface_t;
namespace ange {
namespace angelstow {
class AftFore;
class StarboardPort;
}
namespace containers {
class Container;
}
namespace schedule {
class Call;
}
namespace vessel {
class LashingRod;
class Slot;
class StackSupport;
}
}

typedef QMultiHash<const ange::containers::Container*, const ange::vessel::LashingRod*> LashingsHash;

/**
 * Helper class for implementing LashingPatternInterface, it handles the lazy generation of lashing rods.
 */
class AbstractLashingPattern : public ange::angelstow::LashingPatternInterface {

public:
    AbstractLashingPattern(const document_interface_t* document, const ange::schedule::Call* call);

    virtual ~AbstractLashingPattern();

    virtual QList<const ange::vessel::LashingRod*> lashingRods(const ange::containers::Container* container) const;

    virtual bool hasCellGuides(ange::vessel::BayRowTier bayRowTier) const;

private:
    virtual void generateLashing() const;
    void generateLashing(const ange::vessel::StackSupport* support, ange::vessel::Direction::AftFore bayEnd) const;
    virtual void generateLashing(const ange::vessel::StackSupport* support, ange::vessel::Direction::AftFore bayEnd,
                                 const QList<const ange::containers::Container*>& containerList,
                                 ange::vessel::Direction::AftFore end, ange::vessel::Direction::StarboardPort side) const = 0;

protected:
    const document_interface_t* m_document;
    const ange::schedule::Call* m_call;
    mutable LashingsHash m_lashings;

private:
    mutable bool m_lashings_generated;

};

#endif // ABSTRACTLASHINGPATTERN_H
