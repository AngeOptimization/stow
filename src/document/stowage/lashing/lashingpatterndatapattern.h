#ifndef LASHINGPATTERNDATAPATTERN_H
#define LASHINGPATTERNDATAPATTERN_H

#include "abstractlashingpattern.h"

namespace ange {
namespace vessel {
class LashingPatternData;
}
}

/**
 * Lashing pattern that uses the lashing data from the profile
 */
class LashingPatternDataPattern : public AbstractLashingPattern {
    typedef AbstractLashingPattern super;

public:

    /**
     * Constructor
     */
    LashingPatternDataPattern(const document_interface_t* document, const ange::schedule::Call* call);

    // Inherited:
    virtual bool hasCellGuides(ange::vessel::BayRowTier bayRowTier) const;
    virtual QString name() const;
    virtual void generateLashing(const ange::vessel::StackSupport* support, ange::vessel::Direction::AftFore bayEnd,
                                 const QList<const ange::containers::Container*>& containerList,
                                 ange::vessel::Direction::AftFore end, ange::vessel::Direction::StarboardPort side) const;

private:
    const ange::vessel::LashingPatternData* m_lashingPatternData;

};

#endif // LASHINGPATTERNDATAPATTERN_H
