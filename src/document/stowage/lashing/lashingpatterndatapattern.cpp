#include "lashingpatterndatapattern.h"

#include "document_interface.h"

#include <stowplugininterface/istowagestack.h>

#include <ange/containers/container.h>
#include <ange/vessel/blockweight.h>
#include <ange/vessel/lashingpatterndata.h>
#include <ange/vessel/vessel.h>
#include <ange/vessel/slot.h>
#include <ange/vessel/stacksupport.h>

using namespace ange::units;
using namespace ange::vessel::Direction;
using ange::angelstow::IStowageStack;
using ange::containers::Container;
using ange::schedule::Call;
using ange::vessel::Above;
using ange::vessel::Below;
using ange::vessel::BayRowLevel;
using ange::vessel::BayRowTier;
using ange::vessel::BlockWeight;
using ange::vessel::LashingRod;
using ange::vessel::LashingStackData;
using ange::vessel::Point3D;
using ange::vessel::Slot;
using ange::vessel::StackSupport;

LashingPatternDataPattern::LashingPatternDataPattern(const document_interface_t* document, const Call* call)
  : super(document, call), m_lashingPatternData(m_document->vessel()->lashingPatternData())
{
    // Empty
}

QString LashingPatternDataPattern::name() const {
    return "Lashing pattern from vessel profile";
}

bool LashingPatternDataPattern::hasCellGuides(BayRowTier brt) const {
    BayRowLevel brl(brt.bay(), brt.row(), Above);
    return brt.tier() <= m_lashingPatternData->get(brl)->cellGuidesToTier;
}

// The calculations of the containerEnd and vesselEnd is documented in ticket #1473
static Point3D containerEnd(const Container* container, const IStowageStack* stowageStack,
                            AftFore end, StarboardPort side, TopBottom topBottom) {
    BlockWeight blockWeight = stowageStack->containerBlockWeight(container);
    Point3D containerEnd;
    // Longitudinal position is not important as it is just copied to the vesselEnd
    if (end == Fore) {
        containerEnd.setL(blockWeight.foreLimit());
    } else {
        containerEnd.setL(blockWeight.aftLimit());
    }
    // The querying of the ends of the container should be handled by IStowageStack
    double relativeVcg = 0.45; // How high up the VCG is relative to container height
    // The vertical position of the lashing point on the container is 7 cm in from top/bottom
    if (topBottom == Top) {
        Length top = blockWeight.vcg() + (1 - relativeVcg) * container->physicalHeight();
        containerEnd.setV(top - 7 * centimeter);
    } else {
        Length bottom = blockWeight.vcg() - relativeVcg * container->physicalHeight();
        containerEnd.setV(bottom + 7 * centimeter);
    }
    // The absolute transverse position of the lashing point is not really important as its partner is vesselEnd
    // is derived adding an offset to he number from containerEnd
    containerEnd.setT(blockWeight.tcg() +
                      (8 * feet / 2 - 7 * centimeter) * (side == Starboard ? +1 : -1));
    return containerEnd;
}

void LashingPatternDataPattern::generateLashing(const StackSupport* support, AftFore /*bayEnd*/,
                                                const QList<const Container*>& containerList,
                                                AftFore end, StarboardPort side) const {
    if (support->stackSupportLevel() == Below) {
        return;  // No lashing below deck
    }
    if (containerList.isEmpty()) {
        return;  // If there are no containers then there are no lashings
    }
    BayRowTier brt = m_document->containerNominalPosition(containerList.first(), m_call);
    BayRowLevel brl(brt.bay(), brt.row(), Above);
    const LashingStackData* lashingStackData = m_lashingPatternData->get(brl);
    const IStowageStack* stowageStack = m_document->stowageStackAtCall(m_call, support->stack());
    int numberOfContainersInStack = containerList.length();
    int containerNumber = 0;
    Length heightUnderLashing;
    Q_FOREACH (const Container* container, containerList) {
        containerNumber += 1;
        const LashingRod* rod;
        rod = lashingStackData->generateRod(end, side, Bottom, containerNumber, numberOfContainersInStack,
                                            heightUnderLashing, containerEnd(container, stowageStack, end, side, Bottom));
        if (rod != 0) {
            m_lashings.insert(container, rod);
        }
        heightUnderLashing += container->physicalHeight();
        rod = lashingStackData->generateRod(end, side, Top, containerNumber, numberOfContainersInStack,
                                            heightUnderLashing, containerEnd(container, stowageStack, end, side, Top));
        if (rod != 0) {
            m_lashings.insert(container, rod);
        }
    }
}
