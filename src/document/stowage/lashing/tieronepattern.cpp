#include "tieronepattern.h"

#include "lashingconstants.h"

#include <ange/vessel/lashingrod.h>

using namespace ange::vessel::Direction;
using namespace ange::units;
using ange::vessel::LashingRod;

TierOnePattern::TierOnePattern(const document_interface_t* document, const ange::schedule::Call* call)
    : AbstractLashingPattern(document, call) {
    // Empty
}

QString TierOnePattern::name() const {
    return "Tier one";
}


void TierOnePattern::generateLashing(const ange::vessel::StackSupport* support, ange::vessel::Direction::AftFore bayEnd,
                            const QList<const ange::containers::Container*>& containerList,
                            ange::vessel::Direction::AftFore end, ange::vessel::Direction::StarboardPort side) const {
    Q_UNUSED(support);
    Q_UNUSED(bayEnd);
    LashingRod templateRod;
    templateRod.setEModule(1.4e4 * kilonewton / centimeter2);
    templateRod.setDiameter(2.6 * centimeter);
    templateRod.setEnd(end);
    templateRod.setSide(side);
    if (containerList.size() >= 1) {
        templateRod.setLength(354 * centimeter);
        templateRod.setTransverseAngle(43 * degrees);
        templateRod.setMaxLoad(230 * kilonewton);
        templateRod.setTopBottom(Top);
        m_lashings.insert(containerList.at(0), new LashingRod(templateRod));
    }
    if (containerList.size() >= 2) {
        templateRod.setLength(365 * centimeter);
        templateRod.setTransverseAngle(41 * degrees);
        templateRod.setMaxLoad(230 * kilonewton);
        templateRod.setTopBottom(Bottom);
        m_lashings.insert(containerList.at(1), new LashingRod(templateRod));
    }
}
