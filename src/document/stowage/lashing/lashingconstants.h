#ifndef LASHINGCONSTANTS_H
#define LASHINGCONSTANTS_H

#include <ange/units/units.h>

static const double pi = 3.14159265358979323846;
// Use like this to get radians: 45 * degrees
static const double degrees = pi / 180;

static const ange::units::Acceleration gravityConstant = 9.81 * ange::units::meter / ange::units::second2;


#endif //LASHINGCONSTANTS_H