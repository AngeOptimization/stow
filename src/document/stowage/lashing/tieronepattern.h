#ifndef TIERONEPATTERN_H
#define TIERONEPATTERN_H

#include "abstractlashingpattern.h"
/**
 * The lashing pattern used in "lashing-test-case-realistic.sto"
 *
 * All:
 *   1 container: first layer top
 *   2 container: second layer bottom
 */
class TierOnePattern : public AbstractLashingPattern {

public:
    TierOnePattern(const document_interface_t* document, const ange::schedule::Call* call);

    virtual QString name() const;

private:
    virtual void generateLashing(const ange::vessel::StackSupport* support, ange::vessel::Direction::AftFore bayEnd,
                            const QList<const ange::containers::Container*>& containerList,
                            ange::vessel::Direction::AftFore end, ange::vessel::Direction::StarboardPort side) const;
};

#endif // TIERONEPATTERN_H
