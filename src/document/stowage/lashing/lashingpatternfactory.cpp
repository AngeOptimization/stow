#include "lashingpatternfactory.h"

#include "a13experimentalpattern.h"
#include "a13pattern.h"
#include "a7pattern.h"
#include "abstractlashingpattern.h"
#include "annexlpattern.h"
#include "document_interface.h"
#include "lashingpatterndatapattern.h"
#include "tieronepattern.h"

#include <ange/vessel/vessel.h>

#include <QMultiHash>
#include <QSet>

using ange::angelstow::LashingPatternInterface;
using ange::schedule::Call;

// static
LashingPatternInterface* LashingPatternFactory::newLashingPattern(const document_interface_t* document, const Call* call) {
    if (document->vessel()->lashingPatternData()) {
        return new LashingPatternDataPattern(document, call);
    }

    // UASC A13 vessels:
    static QSet<QString> a13ImoNumbers = QSet<QString>() << "9525857" << "9525869" << "9525883" << "9525895"
                                                         << "9525900" << "9525912" << "9525924" << "9525936"
                                                         << "GlLashingReducedTest-9525869";
    if (a13ImoNumbers.contains(document->vessel()->imoNumber())) { // A13 vessels
        return new A13Pattern(document, call);
    }
    // UASC A7 vessels:
    static QSet<QString> a7ImoNumbers = QSet<QString>() << "9349526" << "9349514" << "9349502" << "9349540"
                                                        << "9349538" << "9349564" << "9349497" << "9349552"
                                                        << "LrLashingTest-A7";
    if (a7ImoNumbers.contains(document->vessel()->imoNumber())) {
        return new A7Pattern(document, call);
    }
    if (document->vessel()->imoNumber() == "A13Experimental") {
        return new A13ExperimentalPattern(document, call);
    }
    // This is for creating the GL 2013 Annex L test:
    if (false) {
        return new AnnexLPattern(document, call);
    }
    // Default lashing pattern:
    return new TierOnePattern(document, call);
}
