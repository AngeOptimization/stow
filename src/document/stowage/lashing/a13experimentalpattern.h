#ifndef A13EXPERIMENTALPATTERN_H
#define A13EXPERIMENTALPATTERN_H

#include "abstractlashingpattern.h"

#include <ange/vessel/point3d.h>
#include <ange/units/units.h>

namespace ange {
namespace angelstow {
class IStowageStack;
}
}

/**
 * The lashing pattern used on the A13 vessels.
 *
 * 20' against lashing bridge:
 *   4 containers: 2 tiers up, second layer bottom
 * 20' middle of bay:
 *   1 container: first layer top
 *   2 containers: second layer bottom
 * 40' against lashing bridge:
 *   3 containers: 2 tiers up, first layer top
 *   4 containers: 2 tiers up, second layer bottom
 *
 * The two outer rows get special lashings in most rows (exceptions are bay 2, 6 and 66 aft).
 *
 * Lashing bridge before bay 2 is only one tier high, all the other lashing bridges are two tiers high.
 */
class A13ExperimentalPattern : public AbstractLashingPattern {

public:
    A13ExperimentalPattern(const document_interface_t* document, const ange::schedule::Call* call);

    virtual QString name() const;

    virtual bool hasCellGuides(ange::vessel::BayRowTier bayRowTier) const;

private:
    virtual void generateLashing(const ange::vessel::StackSupport* support, ange::vessel::Direction::AftFore bayEnd,
                                 const QList<const ange::containers::Container*>& containerList,
                                 ange::vessel::Direction::AftFore end, ange::vessel::Direction::StarboardPort side) const;

    const ange::vessel::LashingRod* rod(double angleInDegrees, double lengthInCentimeter,
                                           ange::vessel::Direction::TopBottom topBottom,
                                           double lashingBridgeDeformationInCentimeter,
                                           const ange::vessel::LashingRod& templateRod) const;

    void generateLashing20BridgeLow(const QList<const ange::containers::Container*>& containerList, const ange::vessel::LashingRod& templateRod) const;

    void generateLashing20Bridge(const QList<const ange::containers::Container*>& containerList, const ange::vessel::LashingRod& templateRod) const;

    // The calculations of the containerEnd and vesselEnd is documented in ticket #1473
    ange::vessel::Point3D containerEnd(const ange::containers::Container* container,
                                       const ange::angelstow::IStowageStack* stowageStack,
                                       const ange::vessel::LashingRod& templateRod) const;

    ange::vessel::Point3D vesselEnd(const ange::vessel::Point3D& containerEnd2, const ange::vessel::LashingRod& templateRod,
                                    ange::units::Length absoluteVertical, ange::units::Length relativeTransverse) const;

    void setRealLengthOnRod(ange::vessel::LashingRod* templateRod, const ange::vessel::StackSupport* support, const ange::containers::Container* container,
                            ange::units::Length absoluteVertical, ange::units::Length relativeTransverse) const;

    void generateLashing20BridgeOuter(const ange::vessel::StackSupport* support, const QList<const ange::containers::Container*>& containerList,
                                      ange::vessel::LashingRod templateRod) const;

    void generateLashing20Mid(const QList<const ange::containers::Container*>& containerList, const ange::vessel::LashingRod& templateRod) const;

    void generateLashing20MidVertical(const ange::vessel::StackSupport* support, const QList<const ange::containers::Container*>& containerList,
                                      ange::vessel::LashingRod templateRod) const;

    void generateLashing40Low(const QList<const ange::containers::Container*>& containerList, const ange::vessel::LashingRod& templateRod) const;

    void generateLashing40(const ange::vessel::StackSupport* support, const QList< const ange::containers::Container* >& containerList, ange::vessel::LashingRod templateRod) const;

    void generateLashing40Outer(const ange::vessel::StackSupport* support, const QList<const ange::containers::Container*>& containerList,
                                ange::vessel::LashingRod templateRod) const;

    void generateLashing40Bay70(const ange::vessel::StackSupport* support, QList<const ange::containers::Container*> containerList,
                                ange::vessel::LashingRod templateRod) const;

    void generateLashing40Bay70Outer(const ange::vessel::StackSupport* support, const QList< const ange::containers::Container* >& containerList, const ange::vessel::LashingRod& templateRod) const;

    void generateLashing40Bay86(QList<const ange::containers::Container*> containerList, const ange::vessel::LashingRod& templateRod) const;

};
#endif // A13EXPERIMENTALPATTERN_H
