#include "annexlpattern.h"

#include "lashingconstants.h"

#include <ange/vessel/lashingrod.h>

using namespace ange::vessel::Direction;
using namespace ange::units;
using ange::vessel::LashingRod;

AnnexLPattern::AnnexLPattern(const document_interface_t* document, const ange::schedule::Call* call)
    : AbstractLashingPattern(document, call) {
    // Empty
}

QString AnnexLPattern::name() const {
    return "GL2013 Annex L example";
}

void AnnexLPattern::generateLashing(const ange::vessel::StackSupport* support, ange::vessel::Direction::AftFore bayEnd,
                            const QList<const ange::containers::Container*>& containerList,
                            ange::vessel::Direction::AftFore end, ange::vessel::Direction::StarboardPort side) const {
    Q_UNUSED(support);
    Q_UNUSED(bayEnd);
    if (side != Starboard) {
        return; // Only make lashings in starboard side
    }
    int containerCount = containerList.size();
    LashingRod templateRod;
    templateRod.setDiameter(2.6 * centimeter);
    templateRod.setEnd(end);
    templateRod.setSide(side);
    if (containerCount >= 1) {
        templateRod.setEModule(1.4e4 * kilonewton / centimeter2);
        templateRod.setLength(354 * centimeter);
        templateRod.setTransverseAngle(43 * degrees);
        templateRod.setMaxLoad(230 * kilonewton);
        templateRod.setTopBottom(Top);
        m_lashings.insert(containerList.at(0), new LashingRod(templateRod));
    }
    if (containerCount >= 2) {
        templateRod.setEModule(1.4e4 * kilonewton / centimeter2);
        templateRod.setLength(365 * centimeter);
        templateRod.setTransverseAngle(41 * degrees);
        templateRod.setMaxLoad(230 * kilonewton);
        templateRod.setTopBottom(Bottom);
        m_lashings.insert(containerList.at(1), new LashingRod(templateRod));
    }
    if (containerCount >= 3) {
        templateRod.setEModule(1.75e4 * kilonewton / centimeter2);
        templateRod.setLength(575 * centimeter);
        templateRod.setTransverseAngle(22 * degrees);
        templateRod.setMaxLoad(270 * kilonewton);
        templateRod.setTopBottom(Bottom);
        m_lashings.insert(containerList.at(2), new LashingRod(templateRod));
    }
}
