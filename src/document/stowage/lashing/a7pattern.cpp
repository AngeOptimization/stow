#include "a7pattern.h"

#include "lashingconstants.h"

#include <ange/containers/container.h>
#include <ange/vessel/lashingrod.h>
#include <ange/vessel/stacksupport.h>

using namespace ange::vessel::Direction;
using namespace ange::units;
using ange::containers::Container;
using ange::vessel::LashingRod;
using ange::vessel::StackSupport;

A7Pattern::A7Pattern(const document_interface_t* document, const ange::schedule::Call* call)
  : AbstractLashingPattern(document, call)
{
    // Empty
}

QString A7Pattern::name() const {
    return "UASC A7";
}

namespace {

bool isOuterStack(const StackSupport* support) {
    if (support->bay() < 4) {
        return support->row() >= 13;
    } else {
        return support->row() >= 15;
    }
}

// True if this lashing rod will help against wind forces
bool isWindExposed(const StackSupport* support, StarboardPort side) {
    if (isOuterStack(support)) {
        // Even row numbers are to the Port
        StarboardPort stackSide = support->row() % 2 == 0 ? Port : Starboard;
        // The rod should be attached to the inside corner
        return stackSide != side;
    } else {
        return false;
    }
}

bool toLashingBridge(int bay, AftFore bayEnd, AftFore end) {
    if (bay % 2 == 0) {  // 40' stack
        if (bay < 10) {
            return false;
        } else if (bay == 10) {
            return end == Aft;
        } else {
            return true;
        }
    } else {  // 20' stack
        if (bayEnd == end) { // Towards the end of the 40' bay (and the possible bridge)
            return 10 < bay;
        } else { // Towards the middle of the 40' bay
            return false;
        }
    }
}

const LashingRod* rod(double angleInDegrees, double lengthInMeter, TopBottom topBottom, const LashingRod& templateRod) {
    LashingRod* lashingRod = new LashingRod(templateRod);
    lashingRod->setLength(lengthInMeter * meter);
    lashingRod->setTransverseAngle(angleInDegrees * degrees);
    lashingRod->setTopBottom(topBottom);
    return lashingRod;
}

const LashingRod* rodOneHigh(bool longRod, TopBottom topBottom, const LashingRod& templateRod) {
    if (longRod) {
        return rod(39.611, 3.764, topBottom, templateRod);
    } else {
        return rod(42.709, 3.538, topBottom, templateRod);
    }
}

const LashingRod* rodTwoHigh(bool longRod, TopBottom topBottom, const LashingRod& templateRod) {
    if (longRod) {
        return rod(22.479, 6.277, topBottom, templateRod);
    } else {
        return rod(24.775, 5.727, topBottom, templateRod);
    }
}

bool hasHcInContainers(const QList<const Container*>& containers, int containersToCheck) {
    Q_ASSERT(containersToCheck <= containers.size());
    for (int i = 0; i < containersToCheck; ++i) {
        if (containers.at(i)->physicalHeight() > 9 * feet) {
            return true;
        }
    }
    return false;
}

LashingsHash oneTop(int bridgeHeight, const QList<const Container*>& containers, const LashingRod& templateRod) {
    LashingsHash lashings;
    if (containers.size() >= bridgeHeight + 1) {
        bool longRod = hasHcInContainers(containers, bridgeHeight + 1);
        lashings.insert(containers.at(bridgeHeight + 0), rodOneHigh(longRod, Top, templateRod));
    }
    return lashings;
}

LashingsHash twoBottom(int bridgeHeight, const QList<const Container*>& containers, const LashingRod& templateRod) {
    LashingsHash lashings;
    if (containers.size() >= bridgeHeight + 2) {
        bool longRod = hasHcInContainers(containers, bridgeHeight + 1);
        lashings.insert(containers.at(bridgeHeight + 1), rodOneHigh(longRod, Bottom, templateRod));
    }
    return lashings;
}

LashingsHash threeBottom(int bridgeHeight, const QList<const Container*>& containers, const LashingRod& templateRod) {
    LashingsHash lashings;
    if (containers.size() >= bridgeHeight + 3) {
        bool longRod = hasHcInContainers(containers, bridgeHeight + 2);
        lashings.insert(containers.at(bridgeHeight + 2), rodTwoHigh(longRod, Bottom, templateRod));
    }
    return lashings;
}

} // anonymous namespace

void A7Pattern::generateLashing(const StackSupport* support, AftFore bayEnd, const QList<const Container*>& containers,
                                AftFore end, StarboardPort side) const {
    const int bay = support->bay();
    LashingRod templateRod;
    templateRod.setEModule(9.8 * ton / millimeter2 * gravityConstant);
    templateRod.setDiameter(26 * millimeter);
    templateRod.setMaxLoad(22.5 * ton * gravityConstant); // TODO should be between 22.5 and 30 depending on angle?
    templateRod.setEnd(end);
    templateRod.setSide(side);
    switch (support->stackSupportType()) {
        case StackSupport::Twenty: {
            if (toLashingBridge(bay, bayEnd, end)) {
                m_lashings += twoBottom(1, containers, templateRod);
            } else {
                m_lashings += twoBottom(0, containers, templateRod);
            }
            break;
        }
        case StackSupport::Forty: {
            if (bay == 70) {
                if (isOuterStack(support)) {
                    m_lashings += oneTop(1, containers, templateRod);
                    m_lashings += twoBottom(1, containers, templateRod);
                } else {
                    m_lashings += oneTop(2, containers, templateRod);
                    m_lashings += twoBottom(2, containers, templateRod);
                }
            } else if (toLashingBridge(bay, bayEnd, end)) {
                m_lashings += oneTop(1, containers, templateRod);
                m_lashings += twoBottom(1, containers, templateRod);
            } else {
                m_lashings += oneTop(0, containers, templateRod);
                m_lashings += twoBottom(0, containers, templateRod);
            }
            break;
        }
    }
    if (isWindExposed(support, side)) {
        if (toLashingBridge(bay, bayEnd, end)) {
            m_lashings += threeBottom(1, containers, templateRod);
        } else {
            m_lashings += threeBottom(0, containers, templateRod);
        }
    }
}
