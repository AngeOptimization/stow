#include "a13pattern.h"

#include "document_interface.h"
#include "lashingconstants.h"

#include <stowplugininterface/istowagestack.h>

#include <ange/containers/container.h>
#include <ange/vessel/bayrowtier.h>
#include <ange/vessel/blockweight.h>
#include <ange/vessel/lashingrod.h>
#include <ange/vessel/slot.h>
#include <ange/vessel/stacksupport.h>

using namespace ange::units;
using namespace ange::vessel::Direction;
using ange::angelstow::IStowageStack;
using ange::containers::Container;
using ange::vessel::BlockWeight;
using ange::vessel::LashingRod;
using ange::vessel::Point3D;
using ange::vessel::StackSupport;

A13Pattern::A13Pattern(const document_interface_t* document, const ange::schedule::Call* call) : AbstractLashingPattern(document, call) {
    // Empty
}

QString A13Pattern::name() const {
    return "UASC A13";
}

bool A13Pattern::hasCellGuides(ange::vessel::BayRowTier brt) const {
    if (69 <= brt.bay() && brt.bay() <= 71) {
        if (brt.row() >= 17) {  // Single outer row in each side
            return false;
        } else {  // Inner rows
            return brt.tier() <= 22;
        }
    } else {
        return false;
    }
}

void A13Pattern::generateLashing(const ange::vessel::StackSupport* support, ange::vessel::Direction::AftFore bayEnd,
                                const QList<const ange::containers::Container*>& containerList,
                                ange::vessel::Direction::AftFore end, ange::vessel::Direction::StarboardPort side) const {
    const int bay = support->bay();
    LashingRod templateRod;
    templateRod.setEModule(1.4e4 * kilonewton / centimeter2);
    templateRod.setDiameter(2.5 * centimeter);
    templateRod.setMaxLoad(230 * kilonewton);
    templateRod.setLashingBridgeDeformation(0 * centimeter);
    templateRod.setEnd(end);
    templateRod.setSide(side);
    switch (support->stackSupportType()) {
        case StackSupport::Twenty: {
            Q_ASSERT(bay % 2 == 1);
            if (bayEnd == end) {
                if (bay == 1 && end == Fore) {
                    generateLashing20BridgeLow(containerList, templateRod);
                } else if (bay <= 7 || (bay == 67 && end == Aft)) {
                    generateLashing20Bridge(containerList, templateRod);
                } else {
                    if (support->row() >= 15) {  // Two outer rows in each side
                        generateLashing20BridgeOuter(support, containerList, templateRod);
                    } else {  // Inner rows
                        generateLashing20Bridge(containerList, templateRod);
                    }
                }
            } else {
                generateLashing20Mid(containerList, templateRod);
                if (support->row() >= 17 && bay >= 9) {
                    StarboardPort supportSide = support->row() % 2 == 1 ? Starboard : Port;
                    if (supportSide == side) {
                        generateLashing20MidVertical(support, containerList, templateRod);
                    }
                }
            }
            break;
        }
        case StackSupport::Forty: {
            Q_ASSERT(bay % 2 == 0);
            if (bay == 2 && end == Fore) {
                generateLashing40Low(containerList, templateRod);
            } else if (bay <= 6 || (bay == 66 && end == Aft)) {
                generateLashing40(containerList, templateRod);
            } else if (bay == 70) {
                if (support->row() >= 17) {  // Single outer row in each side
                    generateLashing40Bay70Outer(containerList, templateRod);
                } else {  // Inner rows
                    generateLashing40Bay70(support, containerList, templateRod);
                }
            } else if (bay == 86) {
                if (support->row() >= 17) {  // Single outer row in each side
                    generateLashing40(containerList, templateRod);
                } else {  // Inner rows
                    generateLashing40Bay86(containerList, templateRod);
                }
            } else {
                if (support->row() >= 15) {  // Two outer rows in each side
                    generateLashing40Outer(support, containerList, templateRod);
                } else {  // Inner rows
                    generateLashing40(containerList, templateRod);
                }
            }
            break;
        }
    }
}

const LashingRod* A13Pattern::rod(double angleInDegrees, double lengthInCentimeter,
                                                   ange::vessel::Direction::TopBottom topBottom,
                                                   double lashingBridgeDeformationInCentimeter,
                                                   const LashingRod& templateRod) const {
    LashingRod* lashingRod = new LashingRod(templateRod);
    lashingRod->setLength(lengthInCentimeter * centimeter);
    lashingRod->setTransverseAngle(angleInDegrees * degrees);
    lashingRod->setLashingBridgeDeformation(lashingBridgeDeformationInCentimeter * centimeter);
    lashingRod->setTopBottom(topBottom);
    return lashingRod;
}

void A13Pattern::generateLashing20BridgeLow(const QList<const ange::containers::Container*>& containerList, const LashingRod& templateRod) const {
    if (containerList.size() >= 3) {
        m_lashings.insert(containerList.at(1 + 1), rod(41, 365, Bottom, 1.0, templateRod));
    }
}

void A13Pattern::generateLashing20Bridge(const QList<const ange::containers::Container*>& containerList, const LashingRod& templateRod) const {
    if (containerList.size() >= 4) {
        m_lashings.insert(containerList.at(2 + 1), rod(38, 389, Bottom, 2.5, templateRod));
    }
}

// The calculations of the containerEnd and vesselEnd is documented in ticket #1473
ange::vessel::Point3D A13Pattern::containerEnd(const ange::containers::Container* container,
                                    const ange::angelstow::IStowageStack* stowageStack,
                                    const LashingRod& templateRod) const {
    BlockWeight blockWeight = stowageStack->containerBlockWeight(container);
    Point3D containerEnd;
    // Longitudinal position is not important as it is just copied to the vesselEnd
    if (templateRod.end() == Fore) {
        containerEnd.setL(blockWeight.foreLimit());
    } else {
        containerEnd.setL(blockWeight.aftLimit());
    }
    // The querying of the ends of the container should be handled by IStowageStack
    double relativeVcg = 0.45; // How high up the VCG is relative to container height
    // The vertical position of the lashing point on the container is 7 cm in from top/bottom
    if (templateRod.topBottom() == Top) {
        Length top = blockWeight.vcg() + (1 - relativeVcg) * container->physicalHeight();
        containerEnd.setV(top - 7 * centimeter);
    } else {
        Length bottom = blockWeight.vcg() - relativeVcg * container->physicalHeight();
        containerEnd.setV(bottom + 7 * centimeter);
    }
    // The absolute transverse position of the lashing point is not really important as its partner is vesselEnd
    // is derived adding an offset to he number from containerEnd
    containerEnd.setT(blockWeight.tcg() +
                        (8 * feet / 2 - 7 * centimeter) * (templateRod.side() == Starboard ? +1 : -1));
    return containerEnd;
}

ange::vessel::Point3D A13Pattern::vesselEnd(const ange::vessel::Point3D& containerEnd2, const LashingRod& templateRod, ange::units::Length absoluteVertical,
                    ange::units::Length relativeTransverse) const {
    Point3D vesselEnd;
    // The calculations in MACS3 is pure 2D
    vesselEnd.setL(containerEnd2.l());
    vesselEnd.setV(absoluteVertical);
    vesselEnd.setT(containerEnd2.t() - relativeTransverse * (templateRod.side() == Starboard ? +1 : -1));
    return vesselEnd;
}

void A13Pattern::setRealLengthOnRod(LashingRod* templateRod, const ange::vessel::StackSupport* support, const ange::containers::Container* container,
                        ange::units::Length absoluteVertical, ange::units::Length relativeTransverse) const {
    const IStowageStack* stowageStack = m_document->stowageStackAtCall(m_call, support->stack());
    Point3D containerEnd2 = containerEnd(container, stowageStack, *templateRod);
    Point3D vesselEnd2 = vesselEnd(containerEnd2, *templateRod, absoluteVertical, relativeTransverse);
    templateRod->setLashingPoints(vesselEnd2, containerEnd2);
}

void A13Pattern::generateLashing20BridgeOuter(const ange::vessel::StackSupport* support, const QList<const ange::containers::Container*>& containerList,
                                    LashingRod templateRod) const {
    if (containerList.size() >= 4) {
        templateRod.setLashingBridgeDeformation(2.5 * centimeter);
        templateRod.setTopBottom(Bottom);
        const Container* container = containerList.at(2 + 1);
        setRealLengthOnRod(&templateRod, support, container, 38.42 * meter, 2.23 * meter);
        m_lashings.insert(container, new LashingRod(templateRod));
    }
}

void A13Pattern::generateLashing20Mid(const QList<const ange::containers::Container*>& containerList, const LashingRod& templateRod) const {
    if (containerList.size() >= 1) {
        m_lashings.insert(containerList.at(0), rod(43, 354, Top, 0, templateRod));
    }
    if (containerList.size() >= 2) {
        m_lashings.insert(containerList.at(1), rod(41, 365, Bottom, 0, templateRod));
    }
}

void A13Pattern::generateLashing20MidVertical(const ange::vessel::StackSupport* support, const QList<const ange::containers::Container*>& containerList,
                                    LashingRod templateRod) const {
    if (containerList.size() >= 2) {
        templateRod.setEModule(1.75e4 * kilonewton / centimeter2);
        templateRod.setTopBottom(Top);
        const Container* container = containerList.at(1);
        setRealLengthOnRod(&templateRod, support, container, 32.413 * meter, 0 * meter);
        m_lashings.insert(container, new LashingRod(templateRod));
    }
}

void A13Pattern::generateLashing40Low(const QList<const ange::containers::Container*>& containerList, const LashingRod& templateRod) const {
    if (containerList.size() >= 2) {
        m_lashings.insert(containerList.at(1 + 0), rod(40, 378, Top, 1.0, templateRod));
    }
    if (containerList.size() >= 3) {
        m_lashings.insert(containerList.at(1 + 1), rod(38, 389, Bottom, 1.0, templateRod));
    }
}

void A13Pattern::generateLashing40(const QList<const ange::containers::Container*>& containerList, const LashingRod& templateRod) const {
    if (containerList.size() >= 3) {
        m_lashings.insert(containerList.at(2 + 0), rod(40, 378, Top, 2.5, templateRod));
    }
    if (containerList.size() >= 4) {
        m_lashings.insert(containerList.at(2 + 1), rod(38, 389, Bottom, 2.5, templateRod));
    }
}

void A13Pattern::generateLashing40Outer(const ange::vessel::StackSupport* support, const QList<const ange::containers::Container*>& containerList,
                            LashingRod templateRod) const {
    if (containerList.size() >= 3) {
        templateRod.setLashingBridgeDeformation(2.5 * centimeter);
        templateRod.setTopBottom(Top);
        const Container* container = containerList.at(2 + 0);
        setRealLengthOnRod(&templateRod, support, container, 38.42 * meter, 2.23 * meter);
        m_lashings.insert(container, new LashingRod(templateRod));
    }
    if (containerList.size() >= 4) {
        templateRod.setTopBottom(Bottom);
        const Container* container = containerList.at(2 + 1);
        setRealLengthOnRod(&templateRod, support, container, 38.60 * meter, 2.23 * meter);
        m_lashings.insert(container, new LashingRod(templateRod));
    }
}

void A13Pattern::generateLashing40Bay70(const ange::vessel::StackSupport* support, QList<const ange::containers::Container*> containerList,
                            LashingRod templateRod) const {
    if (containerList.size() >= 6) {
        Length height;
        for (int i = 0; i < 6; ++i) {
            height += containerList.at(i)->physicalHeight();
        }
        if (height < (1 + 6 * 8.5) * feet - 15 * centimeter) {  // Low lashing pattern, 0 HC
            m_lashings.insert(containerList.at(5 + 0), rod(43, 354, Top, 0.0, templateRod));
            if (containerList.size() >= 7) {
                m_lashings.insert(containerList.at(5 + 1), rod(41, 365, Bottom, 0.0, templateRod));
            }
        } else if (height < (5 + 6 * 8.5) * feet - 15 * centimeter) {  // Medium lashing pattern, 1-4 HC
            m_lashings.insert(containerList.at(5 + 0), rod(40, 378, Top, 0.0, templateRod));
            if (containerList.size() >= 7) {
                m_lashings.insert(containerList.at(5 + 1), rod(38, 389, Bottom, 0.0, templateRod));
            }
        } else {  // High lashing pattern, 5-6 HC
            m_lashings.insert(containerList.at(5 + 0), rod(24, 560, Top, 0.0, templateRod));
            if (containerList.size() >= 7) {
                m_lashings.insert(containerList.at(5 + 1), rod(22, 575, Bottom, 0.0, templateRod));
            }
        }
    }
    if (containerList.size() >= 4) {
        templateRod.setTopBottom(Top);
        const Container* container = containerList.at(3);
        setRealLengthOnRod(&templateRod, support, container, 33.386 * meter, 0 * meter);
        m_lashings.insert(container, new LashingRod(templateRod));
    }
}

void A13Pattern::generateLashing40Bay70Outer(const QList<const ange::containers::Container*>& containerList, const LashingRod& templateRod) const {
    if (containerList.size() >= 3) {
        Length height;
        for (int i = 0; i < 3; ++i) {
            height += containerList.at(i)->physicalHeight();
        }
        if (height < (1 + 3 * 8.5) * feet - 15 * centimeter) {  // Low lashing pattern, 0 HC
            m_lashings.insert(containerList.at(2 + 0), rod(43, 354, Top, 2.5, templateRod));
            if (containerList.size() >= 4) {
                m_lashings.insert(containerList.at(2 + 1), rod(41, 365, Bottom, 2.5, templateRod));
            }
        } else {  // Normal lashing pattern, 1-3 HC
            generateLashing40(containerList, templateRod);
        }
    }
}

void A13Pattern::generateLashing40Bay86(QList<const ange::containers::Container*> containerList, const LashingRod& templateRod) const {
    if (containerList.size() >= 5) {
        Length height;
        for (int i = 0; i < 5; ++i) {
            height += containerList.at(i)->physicalHeight();
        }
        if (height < (5 + 5 * 8.5) * feet - 15 * centimeter) {  // Normal lashing pattern
            m_lashings.insert(containerList.at(4 + 0), rod(40, 378, Top, 2.5, templateRod));
            if (containerList.size() >= 6) {
                m_lashings.insert(containerList.at(4 + 1), rod(38, 389, Bottom, 2.5, templateRod));
            }
        } else {  // High lashing pattern, only used for 5 HC
            m_lashings.insert(containerList.at(4 + 0), rod(24, 560, Bottom, 2.5, templateRod));
            if (containerList.size() >= 6) {
                m_lashings.insert(containerList.at(4 + 1), rod(22, 575, Top, 2.5, templateRod));
            }
        }
    }
}
