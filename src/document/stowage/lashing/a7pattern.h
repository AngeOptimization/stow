#ifndef A7PATTERN_H
#define A7PATTERN_H

#include "abstractlashingpattern.h"

/**
 * The lashing pattern on UASC A7 vessels.
 *
 * There are no lashing bridges between the first 3 bays, this means that in bay 2 and bay 6 all lashing is to deck
 * and in back 10 the aft lashing is to bridge and the fore is to deck. In bay 70 the bay is sunken a single tier
 * except in the outer stacks.
 *
 * 20' to deck:
 *   2 containers: second layer bottom
 *
 * 20' to lashing bridge:
 *   3 containers: 1 tier up, second layer bottom
 *
 * 40' to deck:
 *   1 containers: first layer top
 *   2 containers: second layer bottom
 *
 * 40' to lashing bridge:
 *   2 containers: 1 tier up, first layer top
 *   3 containers: 1 tier up, second layer bottom
 *
 * In outer stacks to deck:
 *   3 containers: third layer bottom
 *
 * In outer stacks to lashing bridge:
 *   4 containers: 1 tier up, third layer bottom
 */
class A7Pattern : public AbstractLashingPattern {

public:

    /**
     * Construct pattern
     */
    A7Pattern(const document_interface_t* document, const ange::schedule::Call* call);

    // Override:
    virtual QString name() const;

private:

    // Override:
    virtual void generateLashing(const ange::vessel::StackSupport* support, ange::vessel::Direction::AftFore bayEnd,
                                 const QList<const ange::containers::Container*>& containers,
                                 ange::vessel::Direction::AftFore end, ange::vessel::Direction::StarboardPort side) const;

};

#endif // A7PATTERN_H
