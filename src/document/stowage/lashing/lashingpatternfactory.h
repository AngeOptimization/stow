#ifndef LASHINGPATTERNFACTORY_H
#define LASHINGPATTERNFACTORY_H

class document_interface_t;
namespace ange {
namespace angelstow {
class LashingPatternInterface;
}
namespace schedule {
class Call;
}
}

/**
 * Factory for generating the correct lashing pattern for vessel in the document
 */
class LashingPatternFactory {

public:

    /**
     * Factory method for generating lashing patterns, will generate the pattern based on the vessel found in
     * document and the loaded containers.
     * Caller owns the new object.
     */
    static ange::angelstow::LashingPatternInterface* newLashingPattern(const document_interface_t* document,
                                                                       const ange::schedule::Call* call);

};

#endif // LASHINGPATTERNFACTORY_H
