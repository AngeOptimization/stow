#ifndef ANNEXLPATTERN_H
#define ANNEXLPATTERN_H

#include "abstractlashingpattern.h"

/**
 * The lashing pattern used in example in GL2013 Annex L
 */
class AnnexLPattern : public AbstractLashingPattern {

public:
    AnnexLPattern(const document_interface_t* document, const ange::schedule::Call* call);

    virtual QString name() const;

private:
    virtual void generateLashing(const ange::vessel::StackSupport* support, ange::vessel::Direction::AftFore bayEnd,
                                const QList<const ange::containers::Container*>& containerList,
                                ange::vessel::Direction::AftFore end, ange::vessel::Direction::StarboardPort side) const;
};

#endif // ANNEXLPATTERN_H
