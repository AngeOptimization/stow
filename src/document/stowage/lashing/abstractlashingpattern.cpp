#include "abstractlashingpattern.h"

#include "document_interface.h"

#include <stowplugininterface/istowagestack.h>

#include <ange/vessel/vessel.h>
#include <ange/vessel/bayslice.h>
#include <ange/vessel/lashingrod.h>
#include <ange/vessel/stack.h>
#include <ange/vessel/stacksupport.h>

using namespace ange::vessel::Direction;
using ange::containers::Container;
using ange::vessel::LashingRod;
using ange::vessel::StackSupport;

AbstractLashingPattern::AbstractLashingPattern(const document_interface_t* document, const ange::schedule::Call* call)
    : m_document(document), m_call(call), m_lashings_generated(false)
{
    // Empty
}

AbstractLashingPattern::~AbstractLashingPattern() {
    qDeleteAll(m_lashings);
}

QList<const LashingRod*> AbstractLashingPattern::lashingRods(const Container* container) const {
    if (!m_lashings_generated) {
        generateLashing();
        m_lashings_generated = true;
    }
    return m_lashings.values(container);
}

bool AbstractLashingPattern::hasCellGuides(ange::vessel::BayRowTier) const {
    return false;  // Normal case is no cell guides on the vessel
}

void AbstractLashingPattern::generateLashing() const {
    Q_FOREACH (const ange::vessel::BaySlice* baySlice, m_document->vessel()->baySlices()) {
        Q_FOREACH (const ange::vessel::Stack* stack, baySlice->stacks()) {
            Q_ASSERT(!stack->stackSupport20Fore() || stack->stackSupport20Fore()->stackSupportType() == StackSupport::Twenty);
            generateLashing(stack->stackSupport20Fore(), Fore);
            Q_ASSERT(!stack->stackSupport40() || stack->stackSupport40()->stackSupportType() == StackSupport::Forty);
            generateLashing(stack->stackSupport40(), Fore);
            Q_ASSERT(!stack->stackSupport20Aft() || stack->stackSupport20Aft()->stackSupportType() == StackSupport::Twenty);
            generateLashing(stack->stackSupport20Aft(), Aft);
        }
    }
}

void AbstractLashingPattern::generateLashing(const StackSupport* support, AftFore bayEnd) const {
    if (!support) {
        return; // stack->stackSupports() returns a list with nullptrs!
    }
    QList<const Container*> containerList
        = m_document->stowageStackAtCall(m_call, support->stack())->containersOnSupport(support);
    Q_FOREACH (AftFore end, aftForeValues()) {
        Q_FOREACH (StarboardPort side, starboardPortValues()) {
            generateLashing(support, bayEnd, containerList, end, side);
        }
    }
}
