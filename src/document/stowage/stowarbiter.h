#ifndef STOW_ARBITER_H
#define STOW_ARBITER_H

#include <QFlag>
#include <QString>
#include <QStringList>
#include <QSet>

class StowageStack;
namespace ange {
namespace vessel {

class Slot;
}

namespace containers {

class Container;
}

namespace schedule {
class Call;
class Schedule;
}
}

class stowage_t;
/**
 * The purpose of this class is to determine if a single container is legal or not, and if not, for what reasons.
 *
 * Also, since there is two output possible for this functor, remember to have each check as a separate small function
 * (suitable for inlining) and call each one from both.
 */
class StowArbiter {
public:
  /**
   * Construct the arbiter. Again, keep this function as fast as possible.
   * @param stowage current stowage. Cannot be null
   * @param slot the slot under query. Note that containers might need additional slots to stowed, and the answer
   *    depends on these slots, too, and their contents.
   * @param from_call the call where the container is placed into the slot. The container is presumed to stay there
   *    until either just before discharge or the cutoff_call (if set), whichever comes first. Must be a valid call.
   */
  StowArbiter(const stowage_t* stowage, const ange::schedule::Schedule* schedule, const ange::vessel::Slot* slot, const ange::schedule::Call* from_call);

  /**
   * Sets the slot under query
   * @param slot the slot under query. Note that containers might need additional slots to stowed, and the answer
   *    depends on these slots, too, and their contents.
   */
  void setSlot(const ange::vessel::Slot* slot);

  /**
   * @param cutoff_call the slots are only check forward until this call, excluding this call. Pass null to signify no cutoff.
   */
  void setCutoff(const ange::schedule::Call* cutoff_call);

  /**
   * @param exception return results as if container "exception" was not stowed
   */
  void setException(const ange::containers::Container* exception);

  /**
   * @param on if on, ignore any slot contents not micro placed. Defaults to on.
   * Note: This configuration is currently unused. If it is still so by 0.21 or so,
   * feel free to nuke it.
   */
  void setMicroplacedOnly(bool on);

  /**
   * @param on if on, only check the barest neccessities to retain a valid stowage.
   * Stowing a container after checking with this on might well cause problem reports,
   * but should not cause crashes. Especially usefull for loaders and importers.
   */
  void setEssentialChecksOnly(bool on);

  /**
   * @return whether container can be placed under the conditions given during construction. Meant to be fast.
   */
  bool isLegal(const ange::containers::Container* container) const;

  /**
   * Possible reasons for disallowing stowage
   */
  enum reason_type_t {
    FROM_BEFORE_POL,
    FROM_AT_OR_AFTER_POD,
    TAKEN,
    SISTER_TAKEN,
    FORE_STACK_HEIGHT,
    AFT_STACK_HEIGHT,
    STACK_WEIGHT_20_AFT,
    STACK_WEIGHT_20_FORE,
    STACK_WEIGHT_40,
    TWENTY_OVER_FOURTY,
    FOURTY_OVER_FOURTY_NON_RUSSIAN,
    LENGTH_TWENTY,
    LENGTH_FOURTY,
    LENGTH_FOURTY_FIVE,
    REEFER,
    STACK_CONSTRAINT_UNKNOWN_REASON,
    SLOT_CONSTRAINT_UNKNOWN_REASON,
    OOG_IN_STACK_BELOW,
    CONTAINER_ABOVE,
    HANGING_CONTAINERS,
    BLOCKED_BY_OOG,
    FIRST_PLUGIN_ERROR // Must be the last
  };

  /**
   * @returns the reasons why a container cannot be stored under the conditions given during construction. Not meant to be extremely fast
   * See also the to_string and to_strings
   */
  QSet< int > whyNot(const ange::containers::Container* container) const;

  /**
   * @returns the reasons why a container cannot be stored under the conditions given during construction. Not meant to be extremely fast
   * This is just another variant of the above, meant to easy merging of the reasons for several containers
   */
  void whyNot(const ange::containers::Container* container, QSet< int >& reasons) const;

  /**
   * @return first port where the container is blocked, or cutoff / last port in schedule if not blocked.
   * Calling this is equivalent to calling stowage_t::slot_available_until() with appropriate arguments.
   */
  const ange::schedule::Call* slotAvailableUntil(const ange::containers::Container* container) const;

  /**
   * @return reason as a human readable string
   */
  QString toString(int reason) const;

  /**
   * @return reasons as a list of human readable strings
   */
  QStringList toStrings(QSet<int> reasons) const;

  /**
   * @return true if the container is hanging in the slot.
   */
  bool hanging(bool is20) const;

  /**
   * @return true iff slot does not support 40'+ containers
   */
  bool violates40capability(const ange::containers::Container* container) const;

private:

  /**
   * prepare arbiter for usage, setting m_dirty to false and initializing
   */
  void prepare() const;

  // RULES HERE

  bool fromBeforePol(const ange::containers::Container* container) const;

  bool fromAtOrAfterPod(const ange::containers::Container* container) const;

  /**
   * @return true if there is no support for a container of length type @param is20
   */
  bool noSupport(const bool is20) const;

  /*
   * Reefer in non-reefer slot
   */
  bool lacksPower(const ange::containers::Container* container, const bool is20) const;

  /**
   * slot is not free for the duration requested
   */
  bool blocked(const ange::containers::Container* container, const bool is20) const;

  /**
   * @return true iff 20' container violates stack height (assumes container really is 20')
   */
  bool tooTall20(const ange::containers::Container* container, const bool same_stack, const ange::containers::Container* Containero_replace) const;

  /**
   * @return true iff 40'+ container violates fore stack height (assumes container really is 40'+)
   */
  bool tooTall40Fore(const ange::containers::Container* container, const bool same_stack, const ange::containers::Container* Containero_replace_fore, const ange::containers::Container* Containero_replace_40) const;

  /**
   * @return true iff 40'+ container violates stack height (assumes container really is 40'+)
   */
  bool tooTall40aft(const ange::containers::Container* container, const bool same_stack, const ange::containers::Container* Containero_replace_aft, const ange::containers::Container* Containero_replace_40) const;

  /**
   * @return true iff 20' container violates stack weights (assumes container really is 20')
   */
  bool tooHeavy20(const ange::containers::Container* container, const bool same_stack, const ange::containers::Container* Containero_replace) const;

  /**
   * @return true iff 40'+ container violates stack weights (assumes container really is 40'+)
   */
  bool tooHeavy40(const ange::containers::Container* container, const bool same_stack, const ange::containers::Container* Containero_replace_fore, const ange::containers::Container* Containero_replace_aft, const ange::containers::Container* Containero_replace_40) const;

  /**
   * @return true iff 45 capability is satisfied
   */
  bool violates45capability(const ange::containers::Container* container) const;

  /**
   * @return true iff this would cause a 20' to be stowed over a 40'
   */
  bool places20over40(bool is20) const;

  /**
   * @return true iff this would cause a 40' to be stowed over a 20, and the stack is non-russion (i.e., that 20' stacks
   *   are not placed flushed against each other)
   */
  bool places40over20nonRussian(bool is20) const;

  /**
   * @return true iff there is a oog somewhere in the stack below, meaning you cannot place
   * a container on top of this.
   */
  bool oogInStackBelow(const ange::containers::Container* container) const;

  /**
   * @return true iff there is a container in the stack above current slot. This is normally only interesting if
   * there is oog, breakbulk or something like that where the 'air above' is blocked.
   */
  bool containerAbove(const ange::containers::Container* container) const;

private:
    const stowage_t* m_stowage;
    const ange::schedule::Schedule* m_schedule;
    const ange::vessel::Slot* m_slot;
    const ange::schedule::Call* m_from_call;
    const ange::schedule::Call* m_cutoff_call;
    const ange::containers::Container* m_exception_container;
    mutable const ange::schedule::Call* m_free_until_20;
    mutable const ange::schedule::Call* m_free_until_40;
    mutable const ange::schedule::Call* m_no_oog_until_20;
    mutable const ange::schedule::Call* m_no_oog_until_40;
    mutable const ange::schedule::Call* m_no_container_above_until_20;
    mutable const ange::schedule::Call* m_no_container_above_until_40;
    mutable const StowageStack* m_stowage_stack;
    mutable bool m_blocked_by_oog;
    bool m_microplaced_only;
    bool m_essential_checks_only;
    mutable bool m_dirty;
    mutable bool m_power_for_20;
    mutable bool m_power_for_40;

};

#endif // STOW_ARBITER_H
