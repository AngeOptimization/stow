#ifndef STOWER_IMPLEMENTATION_H
#define STOWER_IMPLEMENTATION_H

#include "stowplugininterface/istower.h"
// #include "stowtype.h"
#include <QSharedPointer>

class document_t;
class merger_command_t;
// class pool_t;
class stowage_signal_lock_t;
namespace ange {
namespace angelstow {
class Move;
}
namespace schedule {
// class Schedule;
}
}

class StowerImplementation : public ange::angelstow::IStower {

public:

    StowerImplementation(document_t* document);
    ~StowerImplementation();

    virtual void replaceStow(const ange::containers::Container* container, const QList<ange::angelstow::Move>& moves);

    virtual void setUndoText(const QString& undoText);

    virtual QUndoCommand* takeCommand();

private:
    StowerImplementation(const StowerImplementation&); // = delete;
    const StowerImplementation& operator=(const StowerImplementation&); // = delete

private:
    document_t* m_document;
    QScopedPointer<merger_command_t> m_command;
    QSharedPointer<stowage_signal_lock_t> m_stowage_signal_lock;

};

#endif
