#include "stowarbiter.h"

#include "container_leg.h"
#include "oogspacemanager.h"
#include "stowage.h"
#include "stowagestack.h"
#include <ange/containers/container.h>
#include <ange/containers/isocode.h>
#include <ange/containers/oog.h>
#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>
#include <ange/vessel/slot.h>
#include <ange/vessel/stack.h>
#include <ange/vessel/stacksupport.h>
#include <stowplugininterface/validatorplugin.h>
#include <document/pluginmanager.h>

using namespace ange::units;
using ange::containers::Container;
using ange::containers::IsoCode;
using ange::schedule::Call;
using ange::schedule::Schedule;
using ange::vessel::Slot;
using ange::vessel::Stack;
using ange::vessel::StackSupport;

StowArbiter::StowArbiter(const stowage_t* stowage, const Schedule* schedule , const Slot* slot, const Call* from_call)
    : m_stowage(stowage),
      m_schedule(schedule),
      m_slot(slot),
      m_from_call(from_call),
      m_cutoff_call(0L),
      m_exception_container(0L),
      m_microplaced_only(true),
      m_essential_checks_only(false),
      m_dirty(true) {
    Q_ASSERT(stowage);
    Q_ASSERT(from_call);
}

void StowArbiter::setCutoff(const Call* cutoff_call) {
    m_cutoff_call = cutoff_call;
    m_dirty = true;
}

void StowArbiter::setSlot(const Slot* slot) {
    m_slot = slot;
    m_dirty = true;
}

void StowArbiter::setException(const Container* exception) {
    m_exception_container = exception;
    m_dirty = true;
}

void StowArbiter::setMicroplacedOnly(bool on) {
    m_microplaced_only = on;
    m_dirty = true;
}

void StowArbiter::setEssentialChecksOnly(bool on) {
    m_essential_checks_only = on;
    m_dirty = true;
}

namespace {
/*
 * utility functions
 * calculate whether this slot has before for 40+
 * @param power_20 whether the slot has power itself
 * @param sister_slot the sister of the slot
 */
bool power_for_40(bool power_20, const Slot* sister_slot) {
    return power_20 || (sister_slot && sister_slot->hasCapability(ange::vessel::Slot::ReeferCapable));
}
}

void StowArbiter::prepare() const {
    Q_ASSERT(m_slot);
    m_free_until_20 = m_stowage->slot_available_until(m_slot, m_from_call, m_cutoff_call, m_exception_container, m_microplaced_only);
    m_free_until_40 = m_free_until_20;
    if (Slot* sister = m_slot->sister()) {
        const Call* cutoff_call = (m_cutoff_call && m_cutoff_call->distance(m_free_until_20) > 0) ? m_cutoff_call : m_free_until_20;
        m_free_until_40 = m_stowage->slot_available_until(sister, m_from_call, cutoff_call, m_exception_container, m_microplaced_only);
    }
    m_blocked_by_oog = m_stowage->oogSpaceManager()->allBlockedSlots(m_from_call).contains(m_slot);
    const Call* last_call = m_cutoff_call;
    if (!last_call) {
        last_call = m_schedule->at(m_schedule->size() - 1);
    }
    m_no_oog_until_20 = last_call;
    m_no_container_above_until_20 = last_call;
    if (!m_essential_checks_only) {
        QList<Call*> calls =  m_schedule->between(m_from_call, last_call);
        {  // scoped - the sister checks underneath likes to use same variable names for temporary stuff.
            bool slotIsAft = m_slot->isAft();
            QVarLengthArray<Slot*, 50> relevantSlots;
            Q_FOREACH(Slot * slot, m_slot->stack()->stackSlots()) {
                if (slot->isAft() == slotIsAft) {
                    relevantSlots << slot;
                }
            }
            Q_FOREACH(Call * call, calls) {
                QHash<const Slot*, slot_call_content_t> callContents = m_stowage->contentsAtCall(call);
                bool below = true;
                Q_FOREACH(Slot * slot, relevantSlots) {
                    if (slot == m_slot) {
                        below = false;
                        continue;
                    }
                    Q_ASSERT(slot->isAft() == slotIsAft);
                    slot_call_content_t slc = callContents.value(slot);
                    if (below) {
                        if (slc.container() && slc.container()->oog() && slc.container() != m_exception_container) {
                            if (m_no_oog_until_20 != last_call || m_no_oog_until_20->distance(call) < 0) {
                                m_no_oog_until_20 = call;
                            }
                        }
                    } else {
                        if (slc.container() && slc.container() != m_exception_container) {
                            if (m_no_container_above_until_20 != last_call || m_no_container_above_until_20->distance(call) < 0) {
                                m_no_container_above_until_20 = call;
                            }
                        }
                    }
                }
            }
            m_no_oog_until_40 = m_no_oog_until_20;
            m_no_container_above_until_40 = m_no_container_above_until_20;
        }
        if (Slot* sister = m_slot->sister()) {
            bool below = true;
            bool sisterIsAft = sister->isAft();
            QVarLengthArray<Slot*,50> relevantSlots;
            Q_FOREACH(Slot * slot, m_slot->stack()->stackSlots()) {
                if (slot->isAft() == sisterIsAft) {
                    relevantSlots << slot;
                }
            }
            Q_FOREACH(Call * call, calls) {
                QHash<const Slot*, slot_call_content_t> callContents = m_stowage->contentsAtCall(call);
                Q_FOREACH(Slot * slot, relevantSlots) {
                    if (slot == sister) {
                        below = false;
                        break;
                    }
                    Q_ASSERT(slot->isAft() == sisterIsAft);
                    slot_call_content_t slc = callContents.value(slot);
                    if (below) {
                        if (slc.container() && slc.container()->oog() && slc.container() != m_exception_container) {
                            if (m_no_oog_until_40->distance(call) < 0) {
                                m_no_oog_until_40 = call;
                            }
                        }
                    } else {
                        if (slc.container() && slc.container() != m_exception_container) {
                            if (m_no_container_above_until_40->distance(call) < 0) {
                                m_no_container_above_until_40 = call;
                            }
                        }
                    }
                }
            }
        }
        m_stowage_stack = m_stowage->stowage_stack(m_from_call, m_slot->stack());
        m_power_for_20 = m_slot->hasCapability(ange::vessel::Slot::ReeferCapable);
        m_power_for_40 = power_for_40(m_power_for_20, m_slot->sister());
    }
    m_dirty = false;
}

const Call* StowArbiter::slotAvailableUntil(const Container* container) const {
    return container->isoLength() == IsoCode::Twenty ? m_free_until_20 : m_free_until_40;
}

bool StowArbiter::lacksPower(const Container* container, const bool is20) const {
    const bool powered_reefer = container->live() == Container::Live;
    const bool lacks_power = powered_reefer && !(is20 ? m_power_for_20 : m_power_for_40);
    return lacks_power;

}

bool StowArbiter::blocked(const Container* container, const bool is20) const {
    const Call* free_until = is20 ? m_free_until_20 : m_free_until_40;
    if (free_until == m_cutoff_call) { // free_until is set to the cutoff if specified (to save cycles; no need to check further ahead)
        return false;
    }
    const Call* destination = m_stowage->container_routes()->portOfDischarge(container);
    return destination->distance(free_until) < 0;

}

bool StowArbiter::tooTall20(const Container* container, const bool same_stack, const Container* Containero_replace) const {
    if (same_stack) {
        return false; // Moving within one stack cannot violate height
    }
    ange::vessel::Stack* vessel_stack = m_slot->stack();
    StackSupport* stack_support = m_slot->support20();
    if (!stack_support) {
        return false; // no support
    }
    const Length heightToBeAccomodated = Containero_replace ?
                                          container->physicalHeight() - Containero_replace->physicalHeight()
                                          : container->physicalHeight();
    if (stack_support == vessel_stack->stackSupport20Fore()) {
        return !StowageStack::canAccomodateHeight(stack_support, m_stowage_stack->foreHeight(), heightToBeAccomodated);
    } else {
        return !StowageStack::canAccomodateHeight(stack_support, m_stowage_stack->aftHeight(), heightToBeAccomodated);
    }
}

bool StowArbiter::tooTall40Fore(const Container* container, const bool same_stack, const Container* Containero_replace_fore,
                                const Container* Containero_replace_40) const {
    if (same_stack) {
        return false; // Moving within one stack cannot violate height
    }
    //cannot replace both a 20' and a 40' at the same time
    Q_ASSERT(!(Containero_replace_40 && Containero_replace_fore));
    const StackSupport* const stackSupport40 = m_slot->stack()->stackSupport40();
    if (!stackSupport40) {
        return false; // no support
    }
    ange::vessel::Stack* vessel_stack = m_slot->stack();
    Length heightToBeReplaced = 0 * meter;
    if (Containero_replace_40 || Containero_replace_fore) {
        heightToBeReplaced = Containero_replace_fore ? Containero_replace_fore->physicalHeight()
                             : Containero_replace_40->physicalHeight();
    }
    const Length heightToBeAccomodated = container->physicalHeight() - heightToBeReplaced;
    if (const StackSupport*  stackSupport20Fore = vessel_stack->stackSupport20Fore()) {
        return !StowageStack::canAccomodateHeight(stackSupport20Fore, m_stowage_stack->foreHeight(), heightToBeAccomodated);
    } else {
        return !StowageStack::canAccomodateHeight(stackSupport40, m_stowage_stack->aftHeight(), heightToBeAccomodated);
    }
}

bool StowArbiter::tooTall40aft(const Container* container, const bool same_stack, const Container* Containero_replace_aft,
                               const Container* Containero_replace_40) const {
    if (same_stack) {
        return false; // Moving within one stack cannot violate height
    }
    //cannot replace both a 20' and a 40' at the same time
    Q_ASSERT(!(Containero_replace_40 && Containero_replace_aft));
    const StackSupport* const stackSupport40 = m_slot->stack()->stackSupport40();
    if (!stackSupport40) {
        return false; // no support
    }
    ange::vessel::Stack* vessel_stack = m_slot->stack();
    Length heightToBeReplaced = 0 * meter;
    if (Containero_replace_40 || Containero_replace_aft) {
        heightToBeReplaced = Containero_replace_aft ? Containero_replace_aft->physicalHeight()
                             : Containero_replace_40->physicalHeight();
    }
    const Length heightToBeAccomodated = container->physicalHeight() - heightToBeReplaced;
    if (const StackSupport*  stackSupport20Aft = vessel_stack->stackSupport20Fore()) {
        return !StowageStack::canAccomodateHeight(stackSupport20Aft, m_stowage_stack->foreHeight(), heightToBeAccomodated);
    } else {
        return !StowageStack::canAccomodateHeight(stackSupport40, m_stowage_stack->aftHeight(), heightToBeAccomodated);
    }
}

bool StowArbiter::tooHeavy20(const Container* container, const bool same_stack, const Container* Containero_replace) const {
    if (same_stack) {
        return false;
    }
    Mass weight_remaining_20 = 0.1 * kilogram - container->weight();
    const ange::vessel::Stack* vessel_stack = m_slot->stack();
    if (Containero_replace) {
        if (Containero_replace->isoLength() == IsoCode::Twenty) {
            weight_remaining_20 += Containero_replace->weight();
        }
    }
    if (StackSupport* stack_support20 = m_slot->support20()) {
        weight_remaining_20 -= (vessel_stack->stackSupport20Fore() == stack_support20 ? m_stowage_stack->weight20Fore() : m_stowage_stack->weight20Aft());
        weight_remaining_20 += stack_support20->maxWeight();
        if (weight_remaining_20 < 0.0 * ton) {
            return true;
        }
    } else {
        return false; // No support
    }
    return false;
}

bool StowArbiter::tooHeavy40(const Container* container, const bool same_stack, const Container* Containero_replace_fore,
                             const Container* Containero_replace_aft, const Container* Containero_replace_40) const {
    if (same_stack) {
        return false;
    }
    const StackSupport* stackSupport40 = m_slot->stack()->stackSupport40();
    if (!stackSupport40) {
        return false;
    }
    Mass remainingWeight = 0.1 * kilogram;
    if (Containero_replace_40) {
        remainingWeight += Containero_replace_40->weight();
    }
    if (Containero_replace_fore) {
        remainingWeight += Containero_replace_fore->weight();
    }
    if (Containero_replace_aft) {
        remainingWeight += Containero_replace_aft->weight();
    }
    remainingWeight -= container->weight();
    if (m_slot->isAft()) {
        remainingWeight  -= m_stowage_stack->weight40Aft();
    } else {
        remainingWeight  -= m_stowage_stack->weight40Fore();
    }
    remainingWeight += stackSupport40->maxWeight();
    return remainingWeight <= 0.1 * ton;
}

bool StowArbiter::places20over40(bool is20) const {
    const int tier = m_slot->brt().tier();
    if (is20) {
        if (m_stowage_stack->tier_of_lowest_40() < tier) {
            return true;
        }
    } else {
        if (m_stowage_stack->tier_of_highest_20() > tier) {
            return true;
        }
    }
    return false;
}

bool StowArbiter::places40over20nonRussian(bool is20) const {
    if (!m_slot->stack()->canRussian()) {
        const int tier = m_slot->brt().tier();
        if (is20) {
            return (tier < m_stowage_stack->tier_of_highest_40());
        } else {
            return (tier > m_stowage_stack->tier_of_lowest_20());
        }
    }
    return false;
}

bool StowArbiter::hanging(bool is20) const {
    if (is20) {
        if (m_slot->isFore()) {
            if (m_slot->brt().tier() > m_stowage_stack->tier_of_lowest_empty_slot_fore()) {
                return true;
            }
        } else {
            if (m_slot->brt().tier() > m_stowage_stack->tier_of_lowest_empty_slot_aft()) {
                return true;
            }
        }
    } else {
        if (m_slot->brt().tier() > std::min(m_stowage_stack->tier_of_lowest_empty_slot_aft(), m_stowage_stack->tier_of_lowest_empty_slot_fore())) {
            return true;
        }
    }
    return false;
}

bool StowArbiter::violates45capability(const Container* container) const {
    return container->isoLength() == IsoCode::FourtyFive && !m_slot->hasCapability(ange::vessel::Slot::FourtyFiveCapable);
}

bool StowArbiter::oogInStackBelow(const Container* container) const {
    const Call* no_oog_until = container->isoLength() == ange::containers::IsoCode::Twenty ? m_no_oog_until_20 : m_no_oog_until_40;
    if (no_oog_until == m_cutoff_call) { // free_until is set to the cutoff if specified (to save cycles; no need to check further ahead)
        return false;
    }
    const Call* destination = m_stowage->container_routes()->portOfDischarge(container);
    bool rv = destination->distance(no_oog_until) < 0;
    return rv;
}

bool StowArbiter::containerAbove(const Container* container) const {
    const Call* no_container_above_until = container->isoLength() == ange::containers::IsoCode::Twenty ? m_no_container_above_until_20 : m_no_container_above_until_40;
    if (no_container_above_until == m_cutoff_call) { // free_until is set to the cutoff if specified (to save cycles; no need to check further ahead)
        return false;
    }
    const Call* destination = m_stowage->container_routes()->portOfDischarge(container);
    bool rv = destination->distance(no_container_above_until) < 0;
    return rv;
}

bool StowArbiter::fromBeforePol(const Container* container) const {
    return *m_from_call < *m_stowage->container_routes()->portOfLoad(container);
}

bool StowArbiter::fromAtOrAfterPod(const Container* container) const {
    return *m_stowage->container_routes()->portOfDischarge(container) <= *m_from_call;
}

bool StowArbiter::noSupport(const bool is20) const {
    if (is20) {
        return !m_slot->support20();
    } else {
        return !m_slot->support40();
    }
}

bool StowArbiter::violates40capability(const Container* container) const {
    return container->isoLength() != IsoCode::Twenty && (!m_slot->sister() ||
            !m_slot->hasCapability(Slot::FourtyCapable));
}

bool StowArbiter::isLegal(const Container* container) const {
    if (m_dirty) {
        prepare();
    }
    if (fromBeforePol(container)) {
        return false;
    }
    if (fromAtOrAfterPod(container)) {
        return false;
    }
    // note that all checks happening before the check for m_essential_checks_only check are
    // completely invalid moves and will prevent putting the container there even when
    // reading in data from files that tells that the container *is* there
    const bool is20 = container->isoLength() == IsoCode::Twenty;
    if (noSupport(is20)) {
        return false;
    }
    if (blocked(container, is20)) {
        return false;
    }
    if (violates40capability(container)) {
        return false;
    }
    if (container->bundleParent()) {
        // containers that are part of a bundle are never allowed to be stowed
        return false;
    }
    if (m_essential_checks_only) {
        // If these checks passes we are good enough for essential checks
        return true;
    }
    if (m_blocked_by_oog) {
        return false;
    }
    if (lacksPower(container, is20)) {
        return false;
    }
    if (violates45capability(container)) {
        return false;
    }
    if (places20over40(is20)) {
        return false;
    }
    if (places40over20nonRussian(is20)) {
        return false;
    }
    if (oogInStackBelow(container)) {
        return false;
    }
    if (container->oog()) {
        if (containerAbove(container)) {
            return false;
        }
    }
    const ange::vessel::Slot* const old_slot = m_stowage->container_position(container, m_from_call);
    if (is20) {
        const bool same_stack = old_slot && (m_slot->stack() == old_slot->stack()) && (m_slot->isAft() == old_slot->isAft());
        const Container* Containero_replace = m_stowage->contents(m_slot, m_from_call).container();
        if (tooTall20(container, same_stack, Containero_replace)) {
            return false;
        }
        if (tooHeavy20(container, same_stack, Containero_replace)) {
            return false;
        }
        if (Containero_replace && Containero_replace->isoLength() == IsoCode::Twenty) {
            if (tooHeavy40(container, same_stack, Containero_replace, 0L, 0L)) {
                return false;
            }
        } else {
            // This will handle no container to replace as well as container to replace being a 40'
            if (tooHeavy40(container, same_stack, 0L, 0L, Containero_replace)) {
                return false;
            }
        }
    } else {
        const bool same_stack = old_slot && (m_slot->stack() == old_slot->stack());
        const Container* Containero_replace = m_stowage->contents(m_slot, m_from_call).container();
        if (Containero_replace) {
            if (Containero_replace->isoLength() != IsoCode::Twenty) {
                if (tooTall40Fore(container, same_stack, 0L, Containero_replace)) {
                    return false;
                }
                if (tooTall40aft(container, same_stack, 0L, Containero_replace)) {
                    return false;
                }
                if (tooHeavy40(container, same_stack, 0L, 0L, Containero_replace)) {
                    return false;
                }
            } else {
                const Container* sister_Containero_replace = m_slot->sister() ? m_stowage->contents(m_slot->sister(), m_from_call).container() : 0L;
                const bool fore = m_slot->stack()->stackSupport20Fore() == m_slot->support20();
                if (fore) {
                    if (tooTall40Fore(container, same_stack, Containero_replace, 0L)) {
                        return false;
                    }
                    if (tooTall40aft(container, same_stack, sister_Containero_replace, 0L)) {
                        return false;
                    }
                    if (tooHeavy40(container, same_stack, Containero_replace, sister_Containero_replace, 0L)) {
                        return false;
                    }
                } else {
                    if (tooTall40Fore(container, same_stack, sister_Containero_replace, 0L)) {
                        return false;
                    }
                    if (tooTall40aft(container, same_stack, Containero_replace, 0L)) {
                        return false;
                    }
                    if (tooHeavy40(container, same_stack, sister_Containero_replace, Containero_replace, 0L)) {
                        return false;
                    }
                }
            }
        } else {
            if (tooTall40Fore(container, same_stack, 0L, 0L)) {
                return false;
            }
            if (tooTall40aft(container, same_stack, 0L, 0L)) {
                return false;
            }
            if (tooHeavy40(container, same_stack, 0L, 0L, 0L)) {
                return false;
            }
        }
    }
    Q_FOREACH (ange::angelstow::ValidatorPlugin* plugin, m_stowage->pluginManager()->validatorPlugins()) {
        if (!plugin->legalAt(container, m_from_call, m_slot, old_slot)) {
            return false;
        }
    }
    return true;
}

QSet<int> StowArbiter::whyNot(const Container* container) const {
    QSet<int> rv;
    whyNot(container, rv);
    return rv;
}

void StowArbiter::whyNot(const Container* container, QSet< int >& reasons) const {
    if (m_dirty) {
        prepare();
    }
    if (fromBeforePol(container)) {
        reasons |= FROM_BEFORE_POL;
    }
    if (fromAtOrAfterPod(container)) {
        reasons |= FROM_AT_OR_AFTER_POD;
    }
    const bool is20 = container->isoLength() == IsoCode::Twenty;
    if (m_blocked_by_oog) {
        reasons |= BLOCKED_BY_OOG;
    }
    if (noSupport(is20)) {
        reasons |= LENGTH_TWENTY;
    }
    if (blocked(container, is20)) {
        reasons |= TAKEN;
    }
    if (violates40capability(container)) {
        reasons |= LENGTH_FOURTY;
    }
    if (m_essential_checks_only) {
        return;
    }
    if (lacksPower(container, is20)) {
        reasons |= REEFER;
    }
    if (violates45capability(container)) {
        reasons |= LENGTH_FOURTY_FIVE;
    }
    if (places20over40(is20)) {
        reasons |= TWENTY_OVER_FOURTY;
    }
    if (places40over20nonRussian(is20)) {
        reasons |= FOURTY_OVER_FOURTY_NON_RUSSIAN;
    }
    if (oogInStackBelow(container)) {
        reasons |= OOG_IN_STACK_BELOW;
    }
    if (container->oog()) {
        if (containerAbove(container)) {
            reasons |= CONTAINER_ABOVE;
        }
    }
    if (hanging(is20)) {
        reasons |= HANGING_CONTAINERS;
    }
    const ange::vessel::Slot* const old_slot = m_stowage->container_position(container, m_from_call);
    if (is20) {
        const bool same_stack = old_slot && (m_slot->stack() == old_slot->stack()) && (m_slot->isAft() == old_slot->isAft());
        const Container* Containero_replace = m_stowage->contents(m_slot, m_from_call).container();
        if (tooTall20(container, same_stack, Containero_replace)) {
            reasons |= (m_slot->isFore() ?  FORE_STACK_HEIGHT : AFT_STACK_HEIGHT);
        }
        if (tooHeavy20(container, same_stack, Containero_replace)) {
            reasons |= m_slot->isFore() ? STACK_WEIGHT_20_FORE : STACK_WEIGHT_20_AFT;
        }
        if (Containero_replace && Containero_replace->isoLength() == IsoCode::Twenty) {
            if (tooHeavy40(container, same_stack, Containero_replace, 0L, 0L)) {
                reasons |= STACK_WEIGHT_40;
            }
        } else {
            if (tooHeavy40(container, same_stack, 0L, 0L, Containero_replace)) {
                reasons |= STACK_WEIGHT_40;
            }
        }
    } else {
        const bool same_stack = old_slot && (m_slot->stack() == old_slot->stack());
        const Container* Containero_replace = m_stowage->contents(m_slot, m_from_call).container();
        if (Containero_replace) {
            if (Containero_replace->isoLength() != IsoCode::Twenty) {
                if (tooTall40Fore(container, same_stack, 0L, Containero_replace)) {
                    reasons |= FORE_STACK_HEIGHT;
                }
                if (tooTall40aft(container, same_stack, 0L, Containero_replace)) {
                    reasons |= AFT_STACK_HEIGHT;
                }
                if (tooHeavy40(container, same_stack, 0L, 0L, Containero_replace)) {
                    reasons |= STACK_WEIGHT_40;
                }
            } else {
                const Container* sister_Containero_replace = m_slot->sister() ? m_stowage->contents(m_slot->sister(), m_from_call).container() : 0L;
                if (m_slot->isFore()) {
                    if (tooTall40Fore(container, same_stack, Containero_replace, 0L)) {
                        reasons |= FORE_STACK_HEIGHT;
                    }
                    if (tooTall40aft(container, same_stack, sister_Containero_replace, 0L)) {
                        reasons |= AFT_STACK_HEIGHT;
                    }
                    if (tooHeavy40(container, same_stack, Containero_replace, sister_Containero_replace, 0L)) {
                        reasons |= STACK_WEIGHT_40;
                    }
                } else {
                    if (tooTall40Fore(container, same_stack, sister_Containero_replace, 0L)) {
                        reasons |= FORE_STACK_HEIGHT;
                    }
                    if (tooTall40aft(container, same_stack, Containero_replace, 0L)) {
                        reasons |= AFT_STACK_HEIGHT;
                    }
                    if (tooHeavy40(container, same_stack, sister_Containero_replace, Containero_replace, 0L)) {
                        reasons |= STACK_WEIGHT_40;
                    }
                }
            }
        } else {
            if (tooTall40Fore(container, same_stack, 0L, 0L)) {
                reasons |= FORE_STACK_HEIGHT;
            }
            if (tooTall40aft(container, same_stack, 0L, 0L)) {
                reasons |= AFT_STACK_HEIGHT;
            }
            if (tooHeavy40(container, same_stack, 0L, 0L, 0L)) {
                reasons |= STACK_WEIGHT_40;
            }
        }
    }
    Q_FOREACH (ange::angelstow::ValidatorPlugin* plugin, m_stowage->pluginManager()->validatorPlugins()) {
        if (int reason = plugin->whyNot(container, m_from_call, m_slot, old_slot)) {
            reasons |= reason;
        }
    }
}

QString StowArbiter::toString(int reason) const {
    if (m_dirty) {
        prepare();
    }
    switch (reason) {
        case FROM_BEFORE_POL:
            return QObject::tr("Attempted load call is before POL on container");
        case FROM_AT_OR_AFTER_POD:
            return QObject::tr("Attempted load call is at or after POD on container");
        case TAKEN:
            return QObject::tr("Slot taken from %1").arg(m_free_until_20->uncode());
        case SISTER_TAKEN:
            return QObject::tr("40' slot taken from %1").arg(m_free_until_40->uncode());
        case FORE_STACK_HEIGHT: {
            StackSupport* support = m_slot->stack()->stackSupport20Fore();
            if (!support) {
                support = m_slot->stack()->stackSupport40();
            }
            return QObject::tr("Remaining stack height limited to %1 (fore)")
                               .arg(support->maxHeight() / meter - m_stowage_stack->foreHeight() / meter, 0, 'f', 2);
        }
        case AFT_STACK_HEIGHT: {
            StackSupport* support = m_slot->stack()->stackSupport20Aft();
            if (!support) {
                support = m_slot->stack()->stackSupport40();
            }
            return QObject::tr("Remaining stack height limited to %1 (aft)")
                               .arg(support->maxHeight() / meter - m_stowage_stack->aftHeight() / meter, 0, 'f', 2);
        }
        case STACK_WEIGHT_20_AFT: {
            const Mass remaining_weight = m_slot->stack()->stackSupport20Aft()->maxWeight() - m_stowage_stack->weight20Aft();
            if (remaining_weight >= 0.0 * kilogram) {
                return QObject::tr("Remaining 20' stack weight (aft) limited to %1T").arg(remaining_weight / ton, 0, 'f', 1);
            } else {
                return QObject::tr("20' stack weight (aft) limit exceeded by %1T").arg((0.0 * ton - remaining_weight) / ton, 0, 'f', 1);
            }
        }
        case STACK_WEIGHT_20_FORE: {
            const Mass remaining_weight = m_slot->stack()->stackSupport20Fore()->maxWeight() - m_stowage_stack->weight20Fore();
            if (remaining_weight >= 0.0 * kilogram) {
                return QObject::tr("Remaining 20' stack weight (fore) limited to %1T").arg(remaining_weight / ton, 0, 'f', 1);
            } else {
                return QObject::tr("20' stack weight limit (fore) exceeded by to %1T").arg((0.0 * ton - remaining_weight) / ton, 0, 'f', 1);
            }
        }
        case STACK_WEIGHT_40: {
            const Mass weightCombined = m_slot->isAft() ? m_stowage_stack->weight40Aft() : m_stowage_stack->weight40Fore();
            const Mass remainingWeight = m_slot->stack()->stackSupport40()->maxWeight() - weightCombined;
            if (remainingWeight >= 0.0 * kilogram) {
                return QObject::tr("Remaining 40' stack weight limited to %1T").arg(remainingWeight / ton, 0, 'f', 1);
            } else {
                return QObject::tr("40' stack weight limit exceeded by %1T").arg((0.0 * ton - remainingWeight) / ton, 0, 'f', 1);
            }
        }
        case LENGTH_FOURTY_FIVE: {
            return QObject::tr("45' container length not allowed in slot");
        }
        case LENGTH_FOURTY: {
            return QObject::tr("40' container length not allowed in slot");
        }
        case LENGTH_TWENTY: {
            return QObject::tr("20' container length not allowed in slot");
        }
        case REEFER: {
            return QObject::tr("Slot cannot accept reefer");
        }
        case STACK_CONSTRAINT_UNKNOWN_REASON: {
            return QObject::tr("Stack constraint exceeded");
        }
        case SLOT_CONSTRAINT_UNKNOWN_REASON: {
            return QObject::tr("Slot constraints cannot accept container");
        }
        case TWENTY_OVER_FOURTY: {
            return QObject::tr("Stack cannot have 20' containers on top of 40' or longer containers");
        }
        case FOURTY_OVER_FOURTY_NON_RUSSIAN: {
            return QObject::tr("Stack cannot have 40' or longer containers on top of 20' when the stack is not russian");
        }
        case OOG_IN_STACK_BELOW: {
            return QObject::tr("There is a OOG in stack");
        }
        case CONTAINER_ABOVE: {
            return QObject::tr("There is a container above this location");
        }
        case HANGING_CONTAINERS: {
            return QObject::tr("The container is hanging, need underneath support");
        }
        case BLOCKED_BY_OOG: {
            return QObject::tr("The container is blocked by space taken by a OOG in a neighbour stack");
        }
        default: {
            if (ange::angelstow::ValidatorPlugin* plugin = m_stowage->pluginManager()->pluginForReason(reason)) {
                return plugin->reasonToString(reason);
            }
            Q_ASSERT(false);
            return QObject::tr("Unknown reason");
        }
    }
}

QStringList StowArbiter::toStrings(QSet< int > reasons) const {
    QStringList rv;
    Q_FOREACH (int reason, reasons) {
        rv << toString(reason);
    }
    return rv;
}
