#include "stowage.h"

#include "callutils.h"
#include "container_leg.h"
#include "container_list.h"
#include "container_move.h"
#include "crane_split.h"
#include "document.h"
#include "oogspacemanager.h"
#include "overstowcounter.h"
#include "pluginmanager.h"
#include "stabilityforcer.h"
#include "stowagestack.h"
#include "stowerimplementation.h"
#include "stresses.h"
#include "tankconditions.h"
#include "visibility_line.h"

#include <stowplugininterface/documentusinginterface.h>
#include <stowplugininterface/validatorplugin.h>

#include <ange/containers/container.h>
#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>
#include <ange/vessel/bayslice.h>
#include <ange/vessel/blockweight.h>
#include <ange/vessel/slot.h>
#include <ange/vessel/stack.h>
#include <ange/vessel/vessel.h>
#include <ange/vessel/vesseltank.h>

#include <QTimer>

#include <algorithm>

using namespace ange::angelstow;
using namespace ange::containers;
using namespace ange::schedule;
using namespace ange::units;
using namespace ange::vessel;

// stowage_signal_lock_t:

stowage_signal_lock_t::stowage_signal_lock_t(stowage_t* stowage)
    : m_stowage(stowage)
{
    m_stowage->blockSignals(true);
}

stowage_signal_lock_t::~stowage_signal_lock_t() {
    m_stowage->blockSignals(false);
    m_stowage->stowageChanged();
    Q_FOREACH (const Call* call, m_stowage->document()->schedule()->calls()) {
        m_stowage->stowageChangedAtCall(call);
    }
}

// stowage_t:

stowage_t::stowage_t(Schedule* schedule)
  : QObject(0),
    m_schedule(schedule),
    m_crane_split(new crane_split_t(0, this)),
    m_container_routes(new ContainerLeg(this)),
    m_oogSpaceManager(new OOGSpaceManager(0)),
    m_overstowCounter(new OverstowCounter(this, schedule)),
    m_pluginManager(new PluginManager(this))
{
    setObjectName("Stowage");
    Q_ASSERT(schedule);
    connect(m_schedule, SIGNAL(callAdded(ange::schedule::Call*)), SLOT(add_call(ange::schedule::Call*)));
    connect(m_schedule, SIGNAL(callAboutToBeRemoved(ange::schedule::Call*)), SLOT(remove_call(ange::schedule::Call*)));
    connect(m_schedule, SIGNAL(rotationChanged(int, int)), this, SLOT(changeRotation(int, int)));
    m_stowageChangedBuffer = new QTimer(this);
    m_stowageChangedBuffer->setSingleShot(true);
    m_stowageChangedBuffer->setInterval(200);
    connect(m_stowageChangedBuffer, SIGNAL(timeout()), this, SLOT(emitStowageChangedAtCall()));
}

stowage_t::~stowage_t() {
    Q_FOREACH (const QList<const container_move_t*>& moves, m_container_move_map) {
        qDeleteAll(moves);
    }
    typedef QHash<const Stack*, StowageStack*> StackMap;
    Q_FOREACH (const StackMap& stackMap, m_stowage_stacks) {
        qDeleteAll(stackMap);
    }
}

void stowage_t::set_document(document_t* document) {
    Q_ASSERT(document);
    Q_ASSERT(!parent()); // This method is called once, in document_t::document_t()
    setParent(document);
    Q_ASSERT(m_changed_buffer.isEmpty());
    m_crane_split->set_document(document);
    m_overstowCounter->setVessel(document->vessel());
    m_oogSpaceManager->setDocument(document);
    m_pluginManager->documentReset(document);
    Q_FOREACH (Call* call, m_schedule->calls()) {
        add_call(call);
    }
}

void stowage_t::set_vessel(const ange::vessel::Vessel* vessel) {
#ifndef QT_NO_DEBUG
    // When changing the vessel the stowage must be empty
    Q_FOREACH (const Call* call, m_schedule->calls()) {
        Q_ASSERT(m_call_slot_map[call].isEmpty());
    }
#endif
    Q_FOREACH (const Call* call, m_schedule->calls()) {
        resetCachesForCall(call);
    }
    m_overstowCounter->setVessel(vessel);
    m_crane_split->set_document(document());
    m_oogSpaceManager->setDocument(document());
    m_pluginManager->documentReset(document());
}

document_t* stowage_t::document() const {
    return qobject_cast<document_t*>(parent());
}

const OOGSpaceManager* stowage_t::oogSpaceManager() const {
    return m_oogSpaceManager;
}

QList<const Container* > stowage_t::containers_in_slot(const ange::vessel::Slot* slot) const {
    QList<const Container*> rv;
    Q_FOREACH (const Call* call, m_schedule->calls()) {
        const QHash<const Slot*, slot_call_content_t>& slotMap = m_call_slot_map.value(call);
        QHash<const Slot*, slot_call_content_t>::const_iterator it = slotMap.find(slot);
        if (it != slotMap.end()) {
            rv << it->container();
        }
    }
    return rv;
}

QList<const container_move_t*> stowage_t::moves(const Container* container) const {
    return m_container_move_map.value(container);
}

#ifndef QT_NO_DEBUG
/**
 * Only use this in Q_ASSERT() for checking that the returned list of all moves including
 * "pseudo-moves (the load/discharge port for container)" is correct
 */
static bool validate_moves_including_induced(const QList<container_move_t>& all_moves, const Container* container,
                                             ContainerLeg* container_routes) {
    Q_ASSERT(all_moves.length() >= 2);
    Q_ASSERT(all_moves.first().call() == container_routes->portOfLoad(container));
    Q_ASSERT(all_moves.last().call() == container_routes->portOfDischarge(container));
    const Slot* current_pos = 0;
    Q_FOREACH (const container_move_t& move, all_moves) {
        Q_ASSERT(move.slot() || current_pos); // Cannot discharge a container not on board
        current_pos = move.slot();
    }
    return true;
}
#endif // QT_NO_DEBUG

//FIXME: reword function name in English
QList<container_move_t> stowage_t::moves_inclusive_induced(const Container* container) const {
    QList<const container_move_t*> moves = m_container_move_map.value(container);
    QList<container_move_t> rv;
    if (moves.empty()) { // No moves planed, add source and destination
        container_move_t load_move(container, UnknownPosition, m_container_routes->portOfLoad(container), MicroStowedType);
        rv << load_move;
        container_move_t discharge_move(container, 0, m_container_routes->portOfDischarge(container), MicroStowedType);
        rv << discharge_move;
    } else {
        if (moves.front()->call() != m_container_routes->portOfLoad(container)) {
            container_move_t load_move(container, UnknownPosition, m_container_routes->portOfLoad(container), MicroStowedType);
            rv << load_move;
        }
        Q_FOREACH (const container_move_t* move, moves) {
            rv << *move;
        }
        if (moves.back()->call() != m_container_routes->portOfDischarge(container)) {
            if (!moves.back()->slot()) {
                // Container is shifted, but not reloaded yet. So generate an induced shift.
                container_move_t shift_move(container, UnknownPosition, moves.back()->call(), MicroStowedType);
                rv.pop_back();
                rv << shift_move;
            }
            container_move_t discharge_move(container, 0, m_container_routes->portOfDischarge(container), MicroStowedType);
            rv << discharge_move;
        }
    }
    Q_ASSERT(validate_moves_including_induced(rv, container, m_container_routes));
    return rv;
}

const ange::vessel::Slot* stowage_t::container_position(const Container* container, const Call* call) const {
    const container_move_t* move = moveForContainerInCall(container,call);
    const Slot* position = move ? move->slot() : 0L;
    return position;
}

ange::vessel::BayRowTier stowage_t::nominal_position(const ange::containers::Container* container, const Call* call) const {
    Q_ASSERT(call);
    const Slot* position = container_position(container, call);
    BayRowTier rv = position ? position->brt() : BayRowTier();
    if (rv.placed() && container->isoLength() != IsoCode::Twenty) {
        const int bay = position->stack()->baySlice()->joinedBay();
        rv = BayRowTier(bay, rv.row(), rv.tier());
    }
    return rv;
}

void stowage_t::set_stow_type(QList< ange::containers::Container* > containers, const StowType stow_type) {
    Q_FOREACH (const Container* container, containers) {
        QList<const container_move_t*> oldMoves = m_container_move_map.value(container);
        QList<const container_move_t*> newMoves;
        const Slot* current_slot = 0L;
        Q_FOREACH (const Call* call, m_schedule->calls()) {
            if (!oldMoves.empty() && oldMoves.front()->call() == call) {
                const container_move_t* oldMove = oldMoves.takeFirst();
                current_slot = oldMove->slot();
                container_move_t* newMove = new container_move_t(*oldMove);
                delete oldMove;
                newMove->set_stow_type(stow_type);
                newMoves << newMove;
            }
            if (current_slot) {
                Q_ASSERT(m_call_slot_map[call].contains(current_slot));
                m_call_slot_map[call][current_slot].set_type(stow_type);
                if (container->isoLength() != IsoCode::Twenty) {
                    if (const Slot* current_sister = current_slot->sister()) {
                        Q_ASSERT(m_call_slot_map[call].contains(current_sister));
                        m_call_slot_map[call][current_sister].set_type(stow_type);
                    }
                }
            }
        }
        m_container_move_map.insert(container, newMoves);
    }
}

StowageStack* stowage_t::stowage_stack(const Call* call, const Stack* stack) {
    StowageStack* stowageStack = m_stowage_stacks[call][stack];
    Q_ASSERT(stowageStack);
    return stowageStack;
}

const StowageStack* stowage_t::stowage_stack(const Call* call, const Stack* stack) const {
    const StowageStack* stowageStack = m_stowage_stacks[call][stack];
    Q_ASSERT(stowageStack);
    return stowageStack;
}

int stowage_t::discharge_count(const Call* call) const {
    return m_discharge_count.value(call, 0);
}

int stowage_t::load_count(const Call* call) const {
    return m_load_count.value(call, 0);
}

int stowage_t::restow_count(const Call* call) const {
    return m_restow_count.value(call, 0) + m_overstowCounter->overstowMoves(call);
}

static bool moveLessThan(const container_move_t* lhs, const container_move_t* rhs) {
    return *lhs < *rhs;
}

// A better interface would have been:
//   container: container to change moves for
//   removeFrom: remove this and all following moves, 0 if no moves should be removed
//   append: sorted list of moves to append
void stowage_t::change_moves(const Container* container,
                             QList<const container_move_t*> removed, QList<const container_move_t*> added) {
    Q_ASSERT(container);
    // Update container moves
    QList<const container_move_t*> movesOld = m_container_move_map.value(container);
    QList<const container_move_t*> movesNew = movesOld;
    // Remove moves
    Q_FOREACH (const container_move_t* removed_move, removed) {
        Q_ASSERT(removed_move->container() == container);
        Q_ASSERT(movesNew.count(removed_move) == 1);
        movesNew.removeOne(removed_move);
    }
    // Add moves
    Q_FOREACH (const container_move_t* added_move, added) {  // added is not sorted
        QList<const container_move_t*>::iterator it = std::lower_bound(movesNew.begin(), movesNew.end(), added_move, moveLessThan);
        movesNew.insert(it, added_move);
    }
    m_container_move_map.insert(container, movesNew);
    // Update container positions, emitted signals as we go
    int old_i = 0;
    int new_i = 0;
    const Slot* current_pos_old = 0;
    const Slot* current_pos_new = 0;
    StowType stow_type_new = NonPlacedType;
    StowType stow_type_old = NonPlacedType;
    Q_FOREACH (const Call* call, m_schedule->calls()) {
        if (old_i < movesOld.size() && movesOld.at(old_i)->call() == call) {
            const container_move_t* old_move = movesOld.at(old_i++);
            Q_ASSERT(old_move);
            if (old_move->slot()) {
                if (current_pos_old) {
                    m_crane_split->subtract_restow(call, current_pos_old, old_move->slot(), container->isoLength());
                    --m_restow_count[call];
                } else {
                    m_crane_split->subtract_load(call, old_move->slot(), container->isoLength());
                    --m_load_count[call];
                }
            } else {
                Q_ASSERT(current_pos_old);
                m_crane_split->subtract_discharge(call, current_pos_old, container->isoLength());
                --m_discharge_count[call];
            }
            current_pos_old = old_move->slot();
            stow_type_old = old_move->type();
        }
        if (new_i < movesNew.size() && movesNew.at(new_i)->call() == call) {
            const container_move_t* move = movesNew.at(new_i++);
            // Check that we are not receiving two moves in the same call
            while (new_i < movesNew.size() && movesNew.at(new_i)->call() == call) {
                qWarning("Multiple moves for container %s at %s",
                         container->equipmentNumber().toLocal8Bit().data(), call->uncode().toLocal8Bit().data());
                move = movesNew.at(new_i++); // Presumably, the last move is the important one
                Q_ASSERT(false);  // KIM 2015-05-06  Does this ever happen?
            }
            Q_ASSERT(move);
            if (move->slot()) {
                if (current_pos_new) {
                    m_crane_split->add_restow(call, current_pos_new, move->slot(), container->isoLength());
                    ++m_restow_count[call];
                } else {
                    m_crane_split->add_load(call, move->slot(), container->isoLength());
                    ++m_load_count[call];
                }
            } else {
                Q_ASSERT(current_pos_new);
                m_crane_split->add_discharge(call, current_pos_new, container->isoLength());
                ++m_discharge_count[call];
            }
            current_pos_new = move->slot();
            stow_type_new = move->type();
        }
        // current_pos_new == current_pos_old happens when both are 0, it can in theory also happen for a planned restow back to same slot
        if (current_pos_new != current_pos_old) {
            if (current_pos_old) {  // discharge
                Q_ASSERT(m_call_slot_map.value(call).value(current_pos_old).container() == container);  // remove correct container
                m_call_slot_map[call].remove(current_pos_old);
                if (container->isoLength() != IsoCode::Twenty) {
                    const Slot* sister =  current_pos_old->sister();
                    Q_ASSERT(sister);  // slot has sister
                    Q_ASSERT(m_call_slot_map.value(call).value(sister).container() == container);  // remove correct container
                    m_call_slot_map[call].remove(sister);
                }
                stowage_stack(call, current_pos_old->stack())->container_removed(container, current_pos_old);
                m_overstowCounter->movesChanged(current_pos_old);
            }
            if (current_pos_new) {  // load
                Q_ASSERT(!m_call_slot_map.value(call).contains(current_pos_new));  // slot is empty
                m_call_slot_map[call].insert(current_pos_new, slot_call_content_t(container, stow_type_new));
                if (container->isoLength() != IsoCode::Twenty) {
                    const Slot* sister =  current_pos_new->sister();
                    Q_ASSERT(sister);  // slot has sister
                    Q_ASSERT(!m_call_slot_map.value(call).contains(sister));  // slot is empty
                    m_call_slot_map[call].insert(sister, slot_call_content_t(container, stow_type_new));
                }
                stowage_stack(call, current_pos_new->stack())->container_added(container, current_pos_new);
                m_overstowCounter->movesChanged(current_pos_new);
            }
            m_baySlicesBlockWeightCacheIsDirty[call] = true;
            emit container_changed_position(container, current_pos_old, current_pos_new, call, stow_type_old, stow_type_new);
            stowageChangedAtCallBufferer(call); //note: the signal sent within this function will be buffered
            // Check with validators
            Q_FOREACH (ange::angelstow::ValidatorPlugin* plugin, m_pluginManager->validatorPlugins()) {
                plugin->containerMoved(container, call, current_pos_new, current_pos_old);
            }
        }
    }
    m_oogSpaceManager->refresh(container);
#ifndef NDEBUG
    // Sanity check stowage (for this container) after changing it
    {
        const Slot* current_pos = 0L;
        const Call* last_call = 0L;
        const Call* load = m_container_routes->portOfLoad(container);
        const Call* discharge = m_container_routes->portOfDischarge(container);
        bool ok = true;
        ok = ok && load;
        ok = ok && discharge;
        ok = ok && load->distance(discharge) > 0;
        Q_FOREACH (const container_move_t* move, m_container_move_map.value(container)) {
            ok = ok && (current_pos || move->slot());
            ok = ok && (move->call());
            ok = ok && (load->distance(move->call()) >= 0);
            ok = ok && (move->call()->distance(discharge) >= 0);
            ok = ok && (!last_call || last_call->distance(move->call()) > 0);
            current_pos = move->slot();
            last_call = move->call();
        }
        ok = ok && current_pos == 0L;
        if (!ok) {
            qDebug() << "Stowage corrupt: " << m_container_move_map.value(container) << " + " << added << " - "
                     << removed << " load=" << load << " discharge=" << discharge << " cont=" << container;
            Q_ASSERT(false);
        }
    }
#endif
    qDeleteAll(removed); // No longer needed
}

QSet<const ange::containers::Container* > stowage_t::get_containers_on_board(const Call* current_call) const {
    QSet<const ange::containers::Container*> rv;
    QHash<const Slot*, slot_call_content_t> call_stowage = m_call_slot_map.value(current_call);
    for (QHash<const Slot*, slot_call_content_t>::const_iterator it = call_stowage.begin(), end = call_stowage.end(); it != end; ++it) {
        if (it.value().container()) {
            rv << it->container();
        }
    }
    return rv;
}

const Call* stowage_t::load_call(const ange::containers::Container* container,
        const Call* call) const {
    const Call*  rv = 0L;
    Q_FOREACH (const container_move_t* move, m_container_move_map.value(container)) {
        if (move->slot()) {
            rv = move->call();
        }
        if (move->call()->distance(call) <= 0) {
            break;
        }
        if (!move->slot()) {
            rv = 0L; // Was discharged
        }
    }
    return rv;
}

void stowage_t::change_pol(const ange::containers::Container* container, const Call* newCall) {
    const Call* oldCall = m_container_routes->portOfLoad(container);
    m_container_routes->change_pol(container, newCall);
    m_crane_split->update_expected_with_new_source(container, oldCall);
}

void stowage_t::change_pod(const ange::containers::Container* container, const Call* newCall) {
    const Call* oldCall = m_container_routes->portOfDischarge(container);
    m_container_routes->change_pod(container, newCall);
    m_crane_split->update_expected_with_new_destination(container, oldCall);
}

ange::vessel::BlockWeight stowage_t::stowageAsBlockWeight(const Call* call) {
    ange::vessel::BlockWeight rv =  BlockWeight::totalBlockWeight(document()->vessel()->constantWeights()
                                    + document()->stresses_bending()->tankWeights(call)
                                    + document()->stresses_bending()->lightshipWeights()
                                    + bay_block_weights(call).values() << document()->stabilityForcer()->deadLoad(call));
    return rv;
}

void stowage_t::add_call(Call* call) {
    initCachesForCall(call);
    // A new call has no moves (yet)
#ifndef NDEBUG
    Q_FOREACH (const QList<const container_move_t*>& moves, m_container_move_map) {
        Q_FOREACH (const container_move_t* move, moves) {
            Q_ASSERT(move->call() != call);
        }
    }
    Q_ASSERT(!m_discharge_count.contains(call));
    Q_ASSERT(!m_load_count.contains(call));
    Q_ASSERT(!m_restow_count.contains(call));
#endif
    m_discharge_count.insert(call, 0);
    m_load_count.insert(call, 0);
    m_restow_count.insert(call, 0);
    m_crane_split->add_call(call);
    m_overstowCounter->reset();
    stowageChangedAtCallBufferer(call);
}

void stowage_t::remove_call(Call* call) {
    clearCachesForCall(call);
    Q_ASSERT(m_call_slot_map[call].isEmpty());
    m_call_slot_map.remove(call);
    // A removed call must not have any moves
#ifndef NDEBUG
    Q_FOREACH (const QList<const container_move_t*>& moves, m_container_move_map) {
        Q_FOREACH (const container_move_t* move, moves) {
            Q_ASSERT(move->call() != call);
        }
    }
    Q_ASSERT(m_discharge_count.contains(call));
    Q_ASSERT(m_load_count.contains(call));
    Q_ASSERT(m_restow_count.contains(call));
    Q_ASSERT(m_discharge_count.value(call) == 0);
    Q_ASSERT(m_load_count.value(call) == 0);
    Q_ASSERT(m_restow_count.value(call) == 0);
#endif
    m_discharge_count.remove(call);
    m_load_count.remove(call);
    m_restow_count.remove(call);
    m_crane_split->remove_call(call);
    m_overstowCounter->reset();
}

void stowage_t::changeRotation(int previousPosition, int currentPosition) {
    int minPosition = qMin(previousPosition, currentPosition);
    int maxPosition = qMax(previousPosition, currentPosition);
    for (int i = minPosition; i <= maxPosition; ++i) {
        resetCachesForCall(m_schedule->at(i));
    }
    // m_discharge_count, m_load_count and m_restow_count should not be reset as they only depend on planned moves
    m_overstowCounter->reset();
    // m_crane_split should not reset the planned moves, the induced overstow moves will be reset by m_overstowCounter
}

void stowage_t::initCachesForCall(const Call* call) {
    // Init m_stowage_stacks
    Q_ASSERT(!m_stowage_stacks.contains(call));
    Q_FOREACH (const BaySlice* baySlice, document()->vessel()->baySlices()) {
        Q_FOREACH (const Stack* stack, baySlice->stacks()) {
            m_stowage_stacks[call].insert(stack,
                new StowageStack(stack, call, document()->problem_model(), this, document()->visibility_line()));
        }
    }

    // Init m_call_slot_map
    Q_ASSERT(!m_call_slot_map.contains(call));
    QHash<const Slot*, slot_call_content_t>& slotMap = m_call_slot_map[call];
    Q_FOREACH (const ange::containers::Container* container, document()->containers()->list()) {
        const container_move_t* relevantMove = moveForContainerInCall(container, call);
        if (relevantMove) {  // Move found
            const Slot* slot = relevantMove->slot();
            if (slot) {  // Move is a load move
                Q_ASSERT(!slotMap.contains(slot));
                slot_call_content_t slotCallContent(container, relevantMove->type());
                slotMap.insert(slot, slotCallContent);
                StowageStack* current_stowage_stack = stowage_stack(call, slot->stack());
                current_stowage_stack->container_added(container, slot);
                if (container->isoLength() != IsoCode::Twenty) {
                    const Slot* sister = slot->sister();
                    Q_ASSERT(sister);
                    Q_ASSERT(!slotMap.contains(sister));
                    slotMap.insert(sister, slotCallContent);
                }
            }
        }
    }
}

void stowage_t::clearCachesForCall(const Call* call) {
    m_call_slot_map.remove(call);
    qDeleteAll(m_stowage_stacks[call]);
    m_stowage_stacks.remove(call);
}

void stowage_t::resetCachesForCall(const Call* call) {
    clearCachesForCall(call);
    initCachesForCall(call);
}

const container_move_t* stowage_t::container_moved_at_call(const ange::containers::Container* container, const Call* call) const {
    const container_move_t* rv = 0L;
    Q_FOREACH (const container_move_t* move, moves(container)) {
        if (move->call()->distance(call) < 0) {
            break;
        }
        if (move->call() == call) {
            rv = move;
            break;
        }
    }
    return rv;
}

bool stowage_t::hasMove(const Call* call, const Slot* slot) const {
    const Container* container = m_call_slot_map[call].value(slot).container();
    if (container) {
        return container_moved_at_call(container, call);
    } else {
        const Call* previous = m_schedule->previous(call);
        if (!previous) {
            return false;
        }
        return m_call_slot_map[previous].value(slot).container();
    }
}

QSharedPointer<stowage_signal_lock_t> stowage_t::prepare_for_major_stowage_change() {
    if (signalsBlocked()) {
        return QSharedPointer<stowage_signal_lock_t>(0L);
    } else {
        return QSharedPointer<stowage_signal_lock_t>(new stowage_signal_lock_t(this));
    }
}

QHash<const BaySlice*, BlockWeight> stowage_t::bay_block_weights(const Call* call) const {
    if (m_baySlicesBlockWeightCacheIsDirty.value(call, true)) {
        QHash<const ange::vessel::BaySlice*, ange::vessel::BlockWeight > baySliceAsBlockWeight;
        Q_FOREACH (const ange::vessel::BaySlice* bay, document()->vessel()->baySlices()) {
            baySliceAsBlockWeight[bay] = ange::vessel::BlockWeight();
            Q_FOREACH (const ange::vessel::Stack* stack, bay->stacks()) {
                baySliceAsBlockWeight[bay].extendWith(stowage_stack(call, stack)->block_weight_all());
            }
        }
        m_baySlicesBlockWeightCache[call] = baySliceAsBlockWeight;
        m_baySlicesBlockWeightCacheIsDirty[call] = false;
    }
    return m_baySlicesBlockWeightCache[call];
}

const container_move_t* stowage_t::get_move(const ange::vessel::Slot* slot, const Call* call) const {
    Q_ASSERT(call);
    Q_ASSERT(slot);
    const container_move_t* rv = 0L;
    if (const Container* container = contents(slot, call).container()) {
        rv = moveForContainerInCall(container, call);
    }
    return rv;
}

const container_move_t* stowage_t::moveForContainerInCall(const Container* container, const Call* call) const {
    const container_move_t* selectedMove = 0;
    Q_FOREACH (const container_move_t* move, m_container_move_map.value(container)) {
        if (move->call()->distance(call) < 0) {
            return selectedMove;
        }
        selectedMove = move;
    }
    return 0L;
}

const container_move_t* stowage_t::get_out_move(const ange::vessel::Slot* slot, const Call* call) const {
    Q_ASSERT(call);
    Q_ASSERT(slot);
    if (const Container* container = contents(slot, call).container()) {
        Q_FOREACH (const container_move_t* move, moves(container)) {
            if (move->call()->distance(call) < 0) {
                return move;
            }
        }
    }
    return 0L;
}

const container_move_t * stowage_t::loadMoveForDischargeMove(const container_move_t* move) const {
    Q_ASSERT(!move->slot());
    QList<const container_move_t*> container_moves = moves(move->container());
    const int move_index = container_moves.indexOf(move);
    if (move_index == -1 || move_index == 0) {
        qWarning("Stowage corrupt, container equipment_number=\"%s\", call=\"%s\", move index = %d",
                 move->container()->equipmentNumber().toLocal8Bit().data(), move->call()->uncode().toLocal8Bit().data(), move_index);
        Q_ASSERT(move_index != -1);
        return 0L; // Shouldn't happen, save what we can in this case
    }
    return container_moves.value(move_index - 1);
}

const Call* stowage_t::slot_available_until(const ange::vessel::Slot* slot, const Call* call,
        const Call* cutoff_call, const ange::containers::Container* exception, bool micro_only) const {
    const Call* rv = cutoff_call ? cutoff_call : m_schedule->calls().last();
    Q_FOREACH (const Call* c, m_schedule->between(call, rv)) {
        slot_call_content_t slot_contents = contents(slot, c);
        if (!micro_only || slot_contents.type() == MicroStowedType) {
            if (const Container* container = slot_contents.container()) {
                if (container != exception) {
                    rv = c;
                    break;
                }
            }
        }
    }
    return rv;
}

QList<const Call*> stowage_t::activeCalls(const Container* container) const {
    QList<const Call*> calls;
    const QList<const container_move_t*>& moves = m_container_move_map[container];
    const Call* loadPort = m_container_routes->portOfLoad(container);
    if (moves.isEmpty() || moves.first()->call() != loadPort) {
        calls << loadPort;
    }
    Q_FOREACH (const container_move_t* move, moves) {
        calls << move->call();
    }
    const Call* dischargePort = m_container_routes->portOfDischarge(container);
    if (moves.isEmpty() || moves.last()->call() != dischargePort) {
        calls << dischargePort;
    }
    Q_ASSERT(!calls.isEmpty());
    return calls;
}

QString stowage_t::changeOfRotationIsLegal(const Call* changedCall, int moveDelta) const {
    Q_ASSERT(!is_befor_or_after(changedCall));
    Q_ASSERT(moveDelta != 0);
    if (moveDelta < 0) {  // Move call up/back in schedule
        Q_ASSERT(m_schedule->indexOf(changedCall) + moveDelta >= 0);
        const Call* moveBefore = m_schedule->at(m_schedule->indexOf(changedCall) + moveDelta);
        // Check inverted container routes (discharge gets moved up before load port)
        Q_FOREACH (const Container* container, document()->containers()->get_containers_by_discharge_port(changedCall)) {
            if (*moveBefore <= *container_routes()->portOfLoad(container)) {
                return QString("Container %1 must be discharged before it's loaded").arg(container->equipmentNumber());
            }
        }
        // Check inverted moves
        Q_FOREACH (const Container* container, document()->containers()->list()) {
            QListIterator<const Call*> it(activeCalls(container));
            Q_ASSERT(it.hasNext());
            const Call* callA = it.next();
            Q_ASSERT(it.hasNext());
            while (it.hasNext()) {
                const Call* callB = it.next();
                if (callB == changedCall && *moveBefore <= *callA) {
                    return QStringLiteral("Container %1 gets invalid move with %2")
                        .arg(container->equipmentNumber()).arg(callA->assembledName());
                }
                callA = callB;
            }
        }
        // Check stowage collisions
        const Call* changedPrevious = m_schedule->previous(changedCall);
        Q_ASSERT(changedPrevious);  // There will be a call before changedCall when moving up
        const Call* moveBeforePrevious = m_schedule->previous(moveBefore);  // can be nullptr
        Q_FOREACH (const Slot* slot, m_call_slot_map[changedCall].keys()) {
            if (m_call_slot_map[changedPrevious].contains(slot)) {
                continue;  // Skip slots that has content in previous call, it is either same container and we have no
                           // move in this call or it is an other and both moves change without overlap
            }
            for (const Call* call = m_schedule->previous(changedPrevious);
                 call && (!moveBeforePrevious || *moveBeforePrevious <= *call); call = m_schedule->previous(call)) {
                if (m_call_slot_map[call].contains(slot)) {
                    const Container* container = m_call_slot_map[call][slot].container();
                    return QStringLiteral("Two containers in %1 other container is %2")
                        .arg(slot->brt().toString()).arg(container->equipmentNumber());
                }
            }

        }
    } else { // moveDelta > 0.  Move call down/forward in schedule
        Q_ASSERT(m_schedule->indexOf(changedCall) + moveDelta <= m_schedule->size() - 1);
        const Call* moveAfter = m_schedule->at(m_schedule->indexOf(changedCall) + moveDelta);
        // Check inverted container routes
        Q_FOREACH (const Container* container, document()->containers()->get_containers_by_load_port(changedCall)) {
            if (*container_routes()->portOfDischarge(container) <= *moveAfter) {
                return QStringLiteral("Container %1 must be loaded before it's discharged")
                    .arg(container->equipmentNumber());
            }
        }
        // Check inverted moves
        Q_FOREACH (const Container* container, document()->containers()->list()) {
            QListIterator<const Call*> it(activeCalls(container));
            Q_ASSERT(it.hasNext());
            const Call* callA = it.next();
            Q_ASSERT(it.hasNext());
            while (it.hasNext()) {
                const Call* callB = it.next();
                if (callA == changedCall && *callB <= *moveAfter) {
                    return QStringLiteral("Container %1 gets invalid move with %2")
                        .arg(container->equipmentNumber()).arg(callB->assembledName());
                }
                callA = callB;
            }
        }
        // Check stowage collisions
        const Call* changedPrevious = m_schedule->previous(changedCall);
        Q_ASSERT(changedPrevious);
        Q_FOREACH (const Slot* slot, m_call_slot_map[changedPrevious].keys()) {
            if (m_call_slot_map[changedCall].contains(slot)) {
                continue;  // Skip slots that has content in next call, it is either same container and we have no
                           // move in this call or it is an other and both moves change without overlap
            }
            for (const Call* call = m_schedule->next(changedCall);
                 call && (*call <= *moveAfter); call = m_schedule->next(call)) {
                if (m_call_slot_map[call].contains(slot)) {
                    const Container* container = m_call_slot_map[call].value(slot).container();
                    return QStringLiteral("Two containers in %1 other container is %2")
                        .arg(slot->brt().toString()).arg(container->equipmentNumber());
                }
            }

        }
    }
    return QString();
}

void stowage_t::stowageChangedAtCallBufferer(const Call* call) {
    m_changed_buffer.insert(call);
    m_stowageChangedBuffer->start();
}

void stowage_t::emitStowageChangedAtCall() {
    Q_FOREACH (const Call* call, m_changed_buffer) {
        emit stowageChangedAtCall(call);
    }
    m_changed_buffer.clear();
}

ange::angelstow::IStower* stowage_t::create_stowing() {
    return new StowerImplementation(document());
}

// Changeable plugins in the stowage causes unneeded complexity, it was created in ticket #1333
void stowage_t::setPluginManager(PluginManager* pluginManager) {
    m_pluginManager = pluginManager;
}

PluginManager* stowage_t::pluginManager() const {
    return m_pluginManager;
}

OverstowCounter* stowage_t::overstowCounter() {
    return m_overstowCounter.data();
}

const OverstowCounter* stowage_t::overstowCounter() const {
    return m_overstowCounter.data();
}

#include "stowage.moc"
