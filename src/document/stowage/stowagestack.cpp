#include "stowagestack.h"

#include "stowplugininterface/problem.h"
#include "document/document.h"
#include "document/stowage/stowage.h"
#include "document/problems/problem_model.h"
#include "visibility_line.h"

#include <ange/containers/container.h>
#include <ange/schedule/call.h>
#include <ange/vessel/slot.h>
#include <ange/vessel/stack.h>
#include <ange/vessel/bayslice.h>
#include <ange/vessel/stacksupport.h>

#include <cmath>
#include <limits>

using namespace ange::units;

using ange::vessel::Slot;
using ange::vessel::Stack;
using ange::containers::IsoCode;
using ange::vessel::StackSupport;
using ange::vessel::BayRowTier;
using ange::containers::Container;
using ange::vessel::BlockWeight;
using ange::angelstow::Problem;
using ange::angelstow::ProblemLocation;

// The per container VCG is normally chosen to be 45% of container height or 50%, this is depending on the
// classification society. Some use a combination of 45% for full and 50% for empty.
// Perhaps this should be moved to the vessel profile.
static const double perContainerVcgFraction = 0.45;

// Height of a twistlock. It is assumed that twistlocks only are used between containers on deck.
// Perhaps this should be moved to the vessel profile.
// 25.2 mm is the standard twistlock height from GL StowLash program.
static const Length onDeckTwistLockHeight = 2.52 * centimeter;

static QIcon overHeightIcon()  {
    static QIcon icon(":/slots/slot_overlays/over-height.svg");
    return icon;
}

static QIcon overWeightIcon()  {
    static QIcon icon(":/slots/slot_overlays/over-weight.svg");
    return icon;
}

StowageStack::StowageStack(const ange::vessel::Stack* stack, const ange::schedule::Call* call, problem_model_t* problem_model,
                           stowage_t* stowage, visibility_line_t* visibilityLine):
    IStowageStack(stowage),
    m_stowage(stowage),
    m_visibilityLine(visibilityLine),
    m_vessel_stack(stack),
    m_call(call),
    m_problemModel(problem_model),
    m_tier_of_highest_20_fore(0),
    m_tier_of_highest_20_aft(0),
    m_tier_of_highest_40(0),
    m_tier_of_lowest_20_fore(1000),
    m_tier_of_lowest_20_aft(1000),
    m_tier_of_lowest_40(1000),
    m_tier_of_lowest_void_fore(1000),
    m_tier_of_lowest_void_aft(1000),
    m_pending_signal(false),
    m_heightAft(0.0),
    m_heightFwd(0.0)
{
  m_stack_in_forward_view = visibilityLine->is_stack_in_forward_view(this->m_vessel_stack);
  Q_FOREACH(const ange::vessel::Slot* slot , stack->stackSlots()) {
    if(!slot->isAft()) {
      if(slot->brt().tier() < m_tier_of_lowest_void_fore) {
        m_tier_of_lowest_void_fore = slot->brt().tier();
      }
    } else {
      if(slot->brt().tier() < m_tier_of_lowest_void_aft) {
        m_tier_of_lowest_void_aft = slot->brt().tier();
      }
    }
  }
}

StowageStack::~StowageStack() {
    // needed for QScopedPointer
}


Problem* StowageStack::report_problem(Problem::Severity severity, const QString& description,
                                           const StackSupport* stack_support, const QString& details) {

    ProblemLocation loc;
    loc.setCall(m_call);
    loc.setStackSupport(stack_support);
    Problem* problem = new Problem(severity, loc, description, this);
    problem->setDetails(details);
    m_problemModel->add_problem(problem);
    return problem;
}

void StowageStack::check_20_40_mixing_add(const ange::containers::Container* container,
                                             const ange::vessel::Slot* vessel_slot) {
  const int vessel_tier = vessel_slot->brt().tier();
  ange::vessel::StackSupport* stack_support;
  if (container->isoLength() == IsoCode::Twenty)  {
    if(!vessel_slot->isAft()) {
      stack_support=vessel_stack()->stackSupport20Fore();
      if (vessel_tier > m_tier_of_highest_20_fore) {
        m_tier_of_highest_20_fore = vessel_tier;
      }
      if(vessel_tier < m_tier_of_lowest_20_fore) {
        m_tier_of_lowest_20_fore = vessel_tier;
      }
    } else {
      stack_support = vessel_stack()->stackSupport20Aft();
      if (vessel_tier > m_tier_of_highest_20_aft) {
        m_tier_of_highest_20_aft = vessel_tier;
      }
      if(vessel_tier < m_tier_of_lowest_20_aft) {
        m_tier_of_lowest_20_aft = vessel_tier;
      }
    }
  } else {
    // 40'+ placed
    stack_support=vessel_stack()->stackSupport40();
    if (vessel_tier < m_tier_of_lowest_40) {
      m_tier_of_lowest_40 = vessel_tier;
    }
    if(vessel_tier > m_tier_of_highest_40) {
      m_tier_of_highest_40 = vessel_tier;
    }
  }
  // We now scan for the lowest and highest void
  m_tier_of_lowest_void_aft = 1000;
  m_tier_of_lowest_void_fore = 1000;
  Q_FOREACH(const ange::vessel::Slot* slot, m_vessel_stack->stackSlots()) {
    if(!m_stowage->contents(slot, call()).container()){
      if(!slot->isAft()) {
        if(slot->brt().tier() <= m_tier_of_lowest_void_fore) {
          m_tier_of_lowest_void_fore = slot->brt().tier();
        }
      } else {
        if(slot->brt().tier() <= m_tier_of_lowest_void_aft) {
          m_tier_of_lowest_void_aft = slot->brt().tier();
        }
      }
    }
  }
  if (!m_20_over_40_problem) {
    if (m_tier_of_lowest_40 < std::max(m_tier_of_highest_20_fore, m_tier_of_highest_20_aft) )  {
      m_20_over_40_problem.reset(report_problem(Problem::Error, QStringLiteral("20' over 40' container"), vessel_stack()->stackSupport40(), QString()));
    }
  }
  if(!m_russian_stow_problem) {
    if (!vessel_slot->stack()->canRussian() && m_tier_of_highest_40 > std::min(m_tier_of_lowest_20_fore,m_tier_of_lowest_20_aft)) {
      m_russian_stow_problem.reset(report_problem(Problem::Error, QStringLiteral("40' conatiner over 20', but no russian stow supported"), vessel_stack()->stackSupport40(), QString()));
    }
  }
  if(m_tier_of_highest_20_aft > m_tier_of_lowest_void_aft
    || m_tier_of_highest_20_fore > m_tier_of_lowest_void_fore
    || m_tier_of_highest_40 > std::min(m_tier_of_lowest_void_aft,m_tier_of_lowest_void_fore) ) {
    m_hanging_container_problem.reset(report_problem(Problem::Error, "Hanging container", stack_support, "Container hanging in stack, need underneath support"));
  } else {
    m_hanging_container_problem.reset();
  }
}

void StowageStack::check_20_40_mixing_remove(const ange::containers::Container* container,
                                                const ange::vessel::Slot* vessel_slot) {
  const int vessel_tier = vessel_slot->brt().tier();
  ange::vessel::StackSupport* stack_support;
  if (container->isoLength() == IsoCode::Twenty)  {
    if(!vessel_slot->isAft()) {
      stack_support=vessel_stack()->stackSupport20Fore();
      if(vessel_tier < m_tier_of_lowest_void_fore) {
        m_tier_of_lowest_void_fore = vessel_tier;
      }
      if (vessel_tier == m_tier_of_highest_20_fore) {
        // Scan for new highest tier and lowest tier
        m_tier_of_highest_20_fore = 0;
        Q_FOREACH(const Slot* slot, vessel_stack()->stackSlots()) {
          if(!slot->isAft()) {
            if (slot == vessel_slot) {
              continue;
            }
            const int tier = slot->brt().tier();
            if ( tier > m_tier_of_highest_20_fore) {
              if (const Container* container = m_stowage->contents(slot, call()).container()) {
                if (container->isoLength() == IsoCode::Twenty) {
                  m_tier_of_highest_20_fore = tier;
                }
              }
            }
          }
        }
      }
      if(vessel_tier == m_tier_of_lowest_20_fore) {
        // Scan for new lowest tier
        m_tier_of_lowest_20_fore = 1000;
        Q_FOREACH(const Slot* slot, vessel_stack()->stackSlots()) {
          if(!slot->isAft()) {
            if (slot == vessel_slot) {
              continue;
            }
            const int tier = slot->brt().tier();
            if (tier < m_tier_of_lowest_20_fore) {
              if (const Container* container = m_stowage->contents(slot, call()).container()) {
                if (container->isoLength() == IsoCode::Twenty) {
                  m_tier_of_lowest_20_fore = tier;
                }
              }
            }
          }
        }
      }
    } else {
      stack_support=vessel_stack()->stackSupport20Aft();
      if(vessel_tier < m_tier_of_lowest_void_aft) {
        m_tier_of_lowest_void_aft = vessel_tier;
      }
      if (vessel_tier == m_tier_of_highest_20_aft) {
        // Scan for new highest tier
        m_tier_of_highest_20_aft = 0;
        Q_FOREACH(const Slot* slot, vessel_stack()->stackSlots()) {
          if(slot->isAft()) {
            if (slot == vessel_slot) {
              continue;
            }
            const int tier = slot->brt().tier();
            if (tier > m_tier_of_highest_20_aft) {
              if (const Container* container = m_stowage->contents(slot, call()).container()) {
                if (container->isoLength() == IsoCode::Twenty) {
                  m_tier_of_highest_20_aft = tier;
                }
              }
            }
          }
        }
      }
      if(vessel_tier == m_tier_of_lowest_20_aft) {
        // Scan for new lowest tier
        m_tier_of_lowest_20_aft = 1000;
        Q_FOREACH(const Slot* slot, vessel_stack()->stackSlots()) {
          if(slot->isAft()) {
            if (slot == vessel_slot) {
              continue;
            }
            const int tier = slot->brt().tier();
            if (tier < m_tier_of_lowest_20_aft) {
              if (const Container* container = m_stowage->contents(slot, call()).container()) {
                if (container->isoLength() == IsoCode::Twenty) {
                  m_tier_of_lowest_20_aft = tier;
                }
              }
            }
          }
        }
      }
    }
  } else {
    // 40'+ case.
      stack_support=vessel_stack()->stackSupport40();
      if(vessel_tier < m_tier_of_lowest_void_aft) {
        m_tier_of_lowest_void_aft = vessel_tier;
      }
      if(vessel_tier < m_tier_of_lowest_void_fore) {
        m_tier_of_lowest_void_fore = vessel_tier;
      }
    if (vessel_tier == m_tier_of_lowest_40) {
      // Scan for new lowest tier
      m_tier_of_lowest_40 = 1000;
      Q_FOREACH(const Slot* slot, vessel_stack()->stackSlots()) {
        if (slot == vessel_slot) {
          continue;
        }
        const int tier = slot->brt().tier();
        if (tier < m_tier_of_lowest_40) {
          if (const Container* container = m_stowage->contents(slot, call()).container()) {
            if (container->isoLength() != IsoCode::Twenty) {
              m_tier_of_lowest_40 = tier;
            }
          }
        }
      }
    }
    if (vessel_tier == m_tier_of_highest_40) {
      // Scan for new highest tier
      int new_highest_tier = 0;
      Q_FOREACH(const Slot* slot, vessel_stack()->stackSlots()) {
        if (slot == vessel_slot) {
          continue;
        }
        const int tier = slot->brt().tier();
        if (tier > new_highest_tier) {
          if (const Container* container = m_stowage->contents(slot, call()).container()) {
            if (container->isoLength() != IsoCode::Twenty) {
              new_highest_tier = tier;
            }
          }
        }
      }
      m_tier_of_highest_40 = new_highest_tier;
    }
  }
  if (std::max(m_tier_of_highest_20_fore,m_tier_of_highest_20_aft) < m_tier_of_lowest_40 && m_20_over_40_problem) {
    m_20_over_40_problem.reset();
  }
  if(std::min(m_tier_of_lowest_20_aft,m_tier_of_lowest_20_fore) > m_tier_of_highest_40 && m_russian_stow_problem) {
    m_russian_stow_problem.reset();
  }
  if(m_tier_of_highest_20_aft > m_tier_of_lowest_void_aft
    || m_tier_of_highest_20_fore > m_tier_of_lowest_void_fore
    || m_tier_of_highest_40 > std::min(m_tier_of_lowest_void_aft,m_tier_of_lowest_void_fore) ) {
    m_hanging_container_problem.reset(report_problem(Problem::Error, QStringLiteral("Hanging container"), stack_support, "Container hanging in stack, need underneath support"));
  } else {
    m_hanging_container_problem.reset();
  }
}

static Length stackTwistLockHeight(const Stack* stack) {
    return stack->level() == ange::vessel::Below ? 0.0*meter : onDeckTwistLockHeight;
}

/*
 * This method updates the cache of:
 *  - Distribution of containers in 20aft, 20fore, 40 stacks
 *  - Vertical position of containers
 *
 * The way the vertical position is calculated assumes a legal stacking with 20' below 40' and no hanging containers,
 * this leads to the following strange behaviour on illegal stowages (e.g. on a half finished stowage):
 *  - if the stowage has 20 over 40 the 20' containers will still get vcg below the 40' container
 *  - hanging containers will get a vcg as if they were standing on deck
 */
void StowageStack::updateCache() const {
    m_heightAft = 0.0*meter;
    m_heightFwd = 0.0*meter;
    m_blockWeight20Aft = BlockWeight();
    m_blockWeight20Fwd = BlockWeight();
    m_blockWeight40 = BlockWeight();
    m_containerList20Aft.clear();
    m_containerList20Fwd.clear();
    m_containerList40.clear();
    m_containerBlockWeight.clear();
    const Length containerLength20 = IsoCode::physicalLength(IsoCode::Twenty);
    const Length containerLength40 = IsoCode::physicalLength(IsoCode::Fourty);

    Length bottomPos20AftV = 0.0*meter;
    Length height20Aft = 0.0*meter;
    const StackSupport* supportAft = vessel_stack()->stackSupport20Aft();
    if (supportAft) {
        Q_FOREACH (Slot* slot, supportAft->stackSlots()) {
            if (const Container* container = m_stowage->contents(slot, call()).container()) {
                if (container->isoLength() == IsoCode::Twenty) {
                    m_containerList20Aft.insert(slot->brt().tier(), container);
                }
            }
        }
        bottomPos20AftV = supportAft->bottomPos().v();
        Length bottomPos20AftL = supportAft->bottomPos().l();
        Length bottomPos20AftT = supportAft->bottomPos().t();
        Length twistLockHeight = stackTwistLockHeight(vessel_stack());
        Q_FOREACH (const Container* container, containersOnSupport(supportAft)) {
            BlockWeight blockWeight(bottomPos20AftL, bottomPos20AftL - containerLength20 / 2, bottomPos20AftL + containerLength20 / 2,
                                    bottomPos20AftV + height20Aft + container->physicalHeight() * perContainerVcgFraction,
                                    bottomPos20AftT, container->weight());
            Q_ASSERT(!m_containerBlockWeight.contains(container));
            m_containerBlockWeight[container] = blockWeight;
            m_blockWeight20Aft.extendWith(blockWeight);
            height20Aft += container->physicalHeight() + twistLockHeight;
        }
    }

    Length bottomPos20FwdV = 0.0*meter;
    Length height20Fwd = 0.0*meter;
    const StackSupport* supportFore = vessel_stack()->stackSupport20Fore();
    if (supportFore) {
        Q_FOREACH (Slot* slot, supportFore->stackSlots()) {
            if (const Container* container = m_stowage->contents(slot, call()).container()) {
                if (container->isoLength() == IsoCode::Twenty) {
                    m_containerList20Fwd.insert(slot->brt().tier(), container);
                }
            }
        }
        bottomPos20FwdV = supportFore->bottomPos().v();
        Length bottomPos20FwdL = supportFore->bottomPos().l();
        Length bottomPos20FwdT = supportFore->bottomPos().t();
        Length twistLockHeight = stackTwistLockHeight(vessel_stack());
        Q_FOREACH (const Container* container, containersOnSupport(supportFore)) {
            BlockWeight blockWeight(bottomPos20FwdL, bottomPos20FwdL - containerLength20 / 2, bottomPos20FwdL + containerLength20 / 2,
                                    bottomPos20FwdV + height20Fwd + container->physicalHeight() * perContainerVcgFraction ,
                                    bottomPos20FwdT, container->weight());
            Q_ASSERT(!m_containerBlockWeight.contains(container));
            m_containerBlockWeight[container] = blockWeight;
            m_blockWeight20Fwd.extendWith(blockWeight);
            height20Fwd += container->physicalHeight() + twistLockHeight;
        }
    }

    Length height40 = 0.0*meter;
    const StackSupport* support40 = vessel_stack()->stackSupport40();
    if (support40) {
        Q_FOREACH (Slot* slot, support40->stackSlots()) {
            if (const Container* container = m_stowage->contents(slot, call()).container()) {
                if (container->isoLength() != IsoCode::Twenty) {
                    m_containerList40.insert(slot->brt().tier(), container);
                }
            }
        }
        Length bottomPos40V = qMax(support40->bottomPos().v(),
                                   qMax(height20Aft + bottomPos20AftV, height20Fwd + bottomPos20FwdV));
        Length bottomPos40L = support40->bottomPos().l();
        Length bottomPos40T = support40->bottomPos().t();
        Length twistLockHeight = stackTwistLockHeight(vessel_stack());
        Q_FOREACH (const Container* container, containersOnSupport(support40)) {
            BlockWeight blockWeight(bottomPos40L, bottomPos40L - containerLength40 / 2, bottomPos40L + containerLength40 / 2,
                                    bottomPos40V + height40 + container->physicalHeight() * perContainerVcgFraction,
                                    bottomPos40T, container->weight());
            Q_ASSERT(!m_containerBlockWeight.contains(container));
            m_containerBlockWeight[container] = blockWeight;
            m_blockWeight40.extendWith(blockWeight);
            height40 += container->physicalHeight() + twistLockHeight;
        }
    }

    m_heightAft = height40 + height20Aft;
    m_heightFwd = height40 + height20Fwd;
}

BlockWeight StowageStack::containerBlockWeight(const Container* container) const {
    Q_ASSERT(m_containerBlockWeight.contains(container));
    return m_containerBlockWeight.value(container);
}

bool StowageStack::canAccomodateHeight(const StackSupport* stackSupport, ange::units::Length currentHeight,
                                       Length heightToBeAccomodated) {
    const Length tolerance = 0.01*meter;
    return stackSupport->maxHeight() + tolerance >= currentHeight + heightToBeAccomodated;
}

void StowageStack::updateProblems() {
    if (const StackSupport* ss = m_vessel_stack->stackSupport20Aft()) {
        if (!m_heightProblemAft) {
            if (!canAccomodateHeight(ss, m_heightAft,  0*meter)) {
                m_heightProblemAft.reset(report_problem(Problem::Error, QStringLiteral("Height exceeded"),
                                            m_vessel_stack->stackSupport20Aft(), tr("Stack height exceeded")));
                m_heightProblemAft->setIcon(overHeightIcon());
            }
        } else {
            if (canAccomodateHeight(ss, m_heightAft, 0*meter)) {
                m_heightProblemAft.reset();
            }
        }
        if (!m_weightProblem20Aft) {
            if(m_blockWeight20Aft.weight() > ss->maxWeight()) {
                m_weightProblem20Aft.reset(report_problem(Problem::Error, QStringLiteral("Weight exceeded"),
                                    m_vessel_stack->stackSupport20Aft(), tr("20' stack weight limit exceeded")));
                m_weightProblem20Aft->setIcon(overWeightIcon());
            }
        } else {
            if(m_blockWeight20Aft.weight() <= ss->maxWeight()) {
                m_weightProblem20Aft.reset();
            }
         }
    }
    if (const StackSupport* ss = m_vessel_stack->stackSupport20Fore()) {
        if (!m_heightProblemFwd) {
            if (!canAccomodateHeight(ss, m_heightFwd, 0*meter)) {
                m_heightProblemFwd.reset(report_problem(Problem::Error, QStringLiteral("Height exceeded"),
                                                           ss, tr("Stack height exceeded")));
                m_heightProblemFwd->setIcon(overHeightIcon());
            }
        } else {
            if (canAccomodateHeight(ss, m_heightFwd, 0*meter)) {
                m_heightProblemFwd.reset();
            }
        }
        if (!m_weightProblem20Fwd) {
            if(m_blockWeight20Fwd.weight() > ss->maxWeight()) {
                m_weightProblem20Fwd.reset(report_problem(Problem::Error, QStringLiteral("Weight exceeded"),
                                    m_vessel_stack->stackSupport20Aft(), tr("20' stack weight limit exceeded")));
                m_weightProblem20Fwd->setIcon(overWeightIcon());
            }
        } else {
            if(m_blockWeight20Fwd.weight() <= ss->maxWeight()) {
                m_weightProblem20Fwd.reset();
            }
        }
    }
    if (const StackSupport* ss = m_vessel_stack->stackSupport40()) {
        Mass weight40 = qMax(m_blockWeight20Aft.weight(), m_blockWeight20Fwd.weight()) + m_blockWeight40.weight();
        if (!m_weightProblem40) {
            if (weight40 > ss->maxWeight()) {
                m_weightProblem40.reset(report_problem(Problem::Error, QStringLiteral("Weight exceeded"),
                                        m_vessel_stack->stackSupport40(), tr("40' stack weight limit exceeded")));
                m_weightProblem40->setIcon(overWeightIcon());
            }
        } else {
            if (weight40 <= m_vessel_stack->stackSupport40()->maxWeight()) {
                m_weightProblem40.reset();
            }
        }
    }
}

void StowageStack::container_added(const ange::containers::Container* container, const Slot* vessel_slot) {
  Q_ASSERT(container);
  Q_ASSERT(vessel_slot);
  connect(container, &Container::featureChanged, this, &StowageStack::updateCache);
  connect(container, &Container::featureChanged, this, &StowageStack::updateProblems);
  check_20_40_mixing_add(container, vessel_slot);
  updateCache();
  updateProblems();
  emit changed();
}

void StowageStack::container_removed(const ange::containers::Container* container, const Slot* vessel_slot) {
  Q_ASSERT(container);
  Q_ASSERT(vessel_slot);
  check_20_40_mixing_remove(container, vessel_slot);
  disconnect(container, &Container::featureChanged, this, &StowageStack::updateCache);
  disconnect(container, &Container::featureChanged, this, &StowageStack::updateProblems);
  updateCache();
  updateProblems();
  emit changed();
}

Mass StowageStack::weight40Aft() const {
    return weight20Aft() + m_blockWeight40.weight();
}

Mass StowageStack::weight40Fore() const {
    return weight20Fore() + m_blockWeight40.weight();
}

Mass StowageStack::weightTotal() const {
    return weight20Aft() + weight20Fore() + m_blockWeight40.weight();
}

void StowageStack::check_visibility_violations(const ange::schedule::Call* call) {
  if(call == m_call) {
    update_visibility();
    //TODO: clean up here: this signal seems only to be required for updating the visibility indicators in the UI
    //could as well be triggered by a centerOfMassChanged (perhaps indirectly)
    emit changed();
  }
}

void StowageStack::update_visibility() {
  if (m_fore_visibility_height_warning) {
    if (m_heightFwd <= get_visibility_maxheight()) {
      m_fore_visibility_height_warning.reset();
      m_visibilityLine->remove_stack_blocking_visibility(call(),vessel_stack()->baySlice()->joinedBay()*1000+vessel_stack()->row());
    }
  }
  if(m_aft_visibility_height_warning && m_heightAft <= get_visibility_maxheight()){
    m_aft_visibility_height_warning.reset();
    m_visibilityLine->remove_stack_blocking_visibility(call(),vessel_stack()->baySlice()->joinedBay()*1000+vessel_stack()->row());
  }
  if(!m_aft_visibility_height_warning) {
    if (const StackSupport* ss = m_vessel_stack->stackSupport20Aft()) {
      if(m_stack_in_forward_view){
        Length visibility_maxHeight = get_visibility_maxheight();
        if(m_heightAft>0.0*meter && visibility_maxHeight < ss->maxHeight() && m_heightAft > visibility_maxHeight){
          m_aft_visibility_height_warning.reset(report_problem(Problem::Notice, QStringLiteral("Visibility line exceeded"), ss, QString()));
          m_visibilityLine->add_stack_blocking_visibility(call(),vessel_stack()->baySlice()->joinedBay()*1000+vessel_stack()->row(), blocking_angles());
        }
      }
    }
  }
  if(!m_fore_visibility_height_warning){
    if (const StackSupport* ss = m_vessel_stack->stackSupport20Fore() ? m_vessel_stack->stackSupport20Fore() : m_vessel_stack->stackSupport40()) {
      if(m_stack_in_forward_view){
        Length visibility_maxHeight = get_visibility_maxheight();
        if(m_heightFwd>0.0*meter && visibility_maxHeight < ss->maxHeight() && m_heightFwd > visibility_maxHeight){
          m_fore_visibility_height_warning.reset(report_problem(Problem::Notice, QStringLiteral("Visibility line exceeded"), ss, QString()));
          m_visibilityLine->add_stack_blocking_visibility(call(),vessel_stack()->baySlice()->joinedBay()*1000+vessel_stack()->row(),blocking_angles());
        }
      }
    }
  }
  m_visibilityLine->update_visibility_line_violations(call());
  if (signalsBlocked()) {
    m_pending_signal = true;
  } else {
    emit changed();
  }
}

QPointF StowageStack::blocking_angles() const {
  QPointF rv;
  if(m_heightFwd > get_visibility_maxheight() + 0.001*meter ||
    m_heightAft > get_visibility_maxheight() + 0.001*meter) {
    rv = QPointF(m_view_blocking_angles.first,m_view_blocking_angles.second);
  }
  return rv;
}

int StowageStack::has_error(bool fore) {
    int rv = NO_ERROR;
    if(fore) {
        if(m_weightProblem20Fwd || m_weightProblem40) {
            rv |= OVERWEIGHT;
        }
        if(m_heightProblemFwd||(m_visibilityLine->has_error(call())&&m_fore_visibility_height_warning )) {
            rv |= OVERHEIGHT;
        }
    } else {
        if(m_weightProblem20Aft || m_weightProblem40) {
            rv |= OVERWEIGHT;
        }
        if(m_heightProblemAft||(m_visibilityLine->has_error(call())&&m_aft_visibility_height_warning )) {
            rv |= OVERHEIGHT;
        }
    }
    return rv;
}

bool StowageStack::has_warning(bool fore) {
  return fore ? false : false;
}

int StowageStack::slots_killed(bool fore) const {
  StackSupport* support = fore ? m_vessel_stack->stackSupport20Fore() : m_vessel_stack->stackSupport20Aft();
  if (!support) {
    support = m_vessel_stack->stackSupport40();
  }
  if (!support) {
    return 0;
  }
  const int nconts = fore ? fore_count() : aft_count();
  Length height = fore ? m_heightFwd : m_heightAft;
  const int max_dc_total = std::min(static_cast<int>(std::floor((support->maxHeight() + 1e-10*meter) / (8.5*ange::units::feet))), support->stackSlots().size());
  const int max_dc_remaining = std::max(std::floor((support->maxHeight() + 1e-10*meter - height) / (8.5*ange::units::feet)),0.0);
  return max_dc_total - max_dc_remaining - nconts;
}

namespace {
inline bool legal_weight(StackSupport* support, const Mass stack_weight, const Mass container_weight) {
  return support ? support->maxWeight()-(stack_weight+container_weight) >= -0.1*kilogram : true;
}

inline bool legal_height(StackSupport* support, double stackHeight, const Container* container, const Container* Containero_replace, bool same_stack) {
    Length heightDifference = same_stack ? 0.0*meter : container->physicalHeight() - (Containero_replace ? Containero_replace->physicalHeight() : 0.0*meter);
    return support ? support->maxHeight() - (stackHeight*meter + heightDifference) >= -0.01*meter : true;
}
}

ange::units::Length StowageStack::get_visibility_maxheight() const {
  Length rv = std::numeric_limits<double>::infinity()*meter;
  if(m_stack_in_forward_view){
    if(ange::vessel::StackSupport* ss = m_vessel_stack->stackSupport40()){
      rv = m_visibilityLine->get_visibility_maxheight(ss,m_call);
    } else {
      if(ange::vessel::StackSupport* ss = m_vessel_stack->stackSupport20Fore()){
        rv =m_visibilityLine->get_visibility_maxheight(ss,m_call);
      }
      if(ange::vessel::StackSupport* ss = m_vessel_stack->stackSupport20Aft()){
        rv = std::min(rv,m_visibilityLine->get_visibility_maxheight(ss,m_call));
      }
    }
  }
  return rv;
}

int StowageStack::fore_count() const {
    return m_containerList20Fwd.count() + m_containerList40.count();
}

int StowageStack::aft_count() const {
    return m_containerList20Aft.count() + m_containerList40.count();
}

QList<const Container*> StowageStack::containersOnSupport(const StackSupport* stackSupport) const {
    if(stackSupport == m_vessel_stack->stackSupport20Aft()) {
        return m_containerList20Aft.values();
    }
    if(stackSupport == m_vessel_stack->stackSupport20Fore()) {
        return m_containerList20Fwd.values();
    }
    if(stackSupport == m_vessel_stack->stackSupport40()) {
        return m_containerList40.values();
    }
    //we shouldn't ask for a StackSupport that is not in the stack.
    Q_ASSERT(false);
    return QList< const Container* >();
}

ange::vessel::BlockWeight StowageStack::block_weight_fore20() const {
    return m_blockWeight20Fwd;
}

ange::vessel::BlockWeight StowageStack::block_weight_aft20() const {
    return m_blockWeight20Aft;
}

ange::vessel::BlockWeight StowageStack::block_weight_40() const {
    return m_blockWeight40;
}

ange::vessel::BlockWeight StowageStack::block_weight_all() const {
    BlockWeight block_weight_all = block_weight_fore20();
    block_weight_all.extendWith(block_weight_aft20());
    block_weight_all.extendWith(block_weight_40());
    return block_weight_all;
}

bool StowageStack::violates_20_over_40(const ange::vessel::Slot* slot, const ange::containers::Container* container) const {
  bool rv = false;
  if (slot && container) {
    const int tier = slot->brt().tier();
    if (container->isoLength() == IsoCode::Twenty) {
      rv = (tier > m_tier_of_lowest_40);
    } else {
      rv = (tier < std::max(m_tier_of_highest_20_fore,m_tier_of_highest_20_aft));
    }
  }
  return rv;
}

#include "stowagestack.moc"
