#ifndef OVERSTOWCOUNTER_H
#define OVERSTOWCOUNTER_H

#include <ange/vessel/bayrowtier.h>

#include <QHash>
#include <QObject>
#include <QScopedPointer>
#include <QSet>

class OverstowCounterData;
class stowage_t;
namespace ange {
namespace containers {
class Container;
}
namespace schedule {
class Call;
class Schedule;
}
namespace vessel {
class HatchCover;
class Slot;
class Stack;
class StackSupport;
class Vessel;
}
}

/**
 * Counts the overstows
 */
class OverstowCounter : public QObject {
    Q_OBJECT

public:

    enum OverstowMoveType {
        /**
        * Container is not restowed or is doing a planned moved
        */
        None,
        /**
        * Container is restowed now because a container below it is moved
        */
        Now,
        /**
        * Container is restowed later
        */
        Later,
    };

public:

    /**
     * Constructor, no parent pointer
     */
    OverstowCounter(stowage_t* stowage, const ange::schedule::Schedule* schedule);
    ~OverstowCounter();

    /**
     * (Re)set the vessel
     */
    void setVessel(const ange::vessel::Vessel* vessel);

    /**
     * Mark that a slot has potentially changed overstow data.
     * @param slot nullptr not allowed
     */
    void movesChanged(const ange::vessel::Slot* slot) const;

    /**
     * Reset all of the cache, can e.g. be used when schedule is changed
     */
    void reset();

    /**
     * @returns type of overstow move this slot executes.
     * For long containers this will be None in aft slots
     * Not const as it might make changes in crane split.
     */
    OverstowMoveType overstowMove(const ange::schedule::Call* call, const ange::vessel::Slot* slot);

    /**
     * @returns total number of overstow moves one in this call.
     * This is the number of slots where overstowMove() returns Now.
     * Not const as it might make changes in crane split.
     */
    int overstowMoves(const ange::schedule::Call* call);

public Q_SLOTS:
    void updateAllCaches();

private:
    void updateCaches(const ange::vessel::Stack* stack);

    void updateLidCache(const ange::schedule::Call* call, const ange::vessel::HatchCover* lid) const;
    void resetMoveBelowLidCache(const ange::schedule::Call* call, const ange::vessel::HatchCover* lid) const;
    void invalidateLidCache(const ange::vessel::Stack* stack) const;

    void updateMoveCache(const ange::vessel::Stack* stack);
    void resetNowMoveCache(const ange::schedule::Call* call, const ange::vessel::Stack* stack);
    void resetLaterMoveCache(const ange::vessel::Stack* stack) const;
    void invalidateMoveCache(const ange::vessel::Stack* stack) const;

private:
    stowage_t* m_stowage;
    const ange::schedule::Schedule* m_schedule;
    const ange::vessel::Vessel* m_vessel;

    QScopedPointer<OverstowCounterData> d; // Using private data pointer in order to hide implementation of Restow

    mutable bool m_allCachesAreClean;

    mutable QHash<const ange::schedule::Call*, QSet<const ange::vessel::HatchCover*> > m_lidCacheIsClean;
    mutable QHash<const ange::schedule::Call*, QSet<const ange::vessel::HatchCover*> > m_moveBelowLidCache;

    mutable QSet<const ange::vessel::Stack*> m_moveCacheIsClean;
    mutable QHash<const ange::schedule::Call*, QSet<const ange::vessel::Slot*> > m_nowMoveCache;
    mutable QHash<const ange::schedule::Call*, QSet<const ange::vessel::Slot*> > m_laterMoveCache;

};

#endif // OVERSTOWCOUNTER_H
