#ifndef SLOT_CALL_CONTENT_H
#define SLOT_CALL_CONTENT_H

#include <stowplugininterface/stowtype.h>

namespace ange {
namespace containers {
class Container;
}
}

class slot_call_content_t {

public:

    /**
     * Needed for QHash, will return empty content
     */
    slot_call_content_t();

    slot_call_content_t(const ange::containers::Container* container, ange::angelstow::StowType type);

    const ange::containers::Container* container() const {
        return m_container;
    }

    ange::angelstow::StowType type() const {
        return m_type;
    }

    void set_type(ange::angelstow::StowType type) {
        m_type = type;
    }

private:
    const ange::containers::Container* m_container;
    ange::angelstow::StowType m_type;

    friend QDebug operator<<(QDebug dbg, const slot_call_content_t& contents);

};

QDebug operator<<(QDebug dbg, const slot_call_content_t& contents);

#endif // SLOT_CALL_CONTENT_H
