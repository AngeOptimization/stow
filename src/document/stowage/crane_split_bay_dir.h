/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010

 Copyright: See COPYING file that comes with this distribution

*/

#ifndef CRANE_SPLIT_BAY_DIR_H
#define CRANE_SPLIT_BAY_DIR_H
#include <QPair>

class crane_split_bay_dir_t {
    public:
        struct BelowAbove {
            int below;
            int above;
            BelowAbove() : below(0), above(0) {}
        };

        crane_split_bay_dir_t();

        /**
         * 20 forwards
         */
        BelowAbove& twentyFore();

        /**
         * 20 afthwards
         */
        BelowAbove& twentyAft();

        /**
         * 40, 45
         */
        BelowAbove& fourty();

        /**
         * 20 forwards
         */
        const BelowAbove& twentyFore() const;

        /**
         * 20 afthwards
         */
        const BelowAbove& twentyAft() const;

        /**
         * 40, 45
         */
        const BelowAbove& fourty() const;

        /**
         * @return joined bay
         */
        int joined_bay() const {
            return m_joined_bay;
        }
    private:
        int m_joined_bay;
        BelowAbove m_twenty_fore;
        BelowAbove m_twenty_aft;
        BelowAbove m_fourty;
};

#endif // CRANE_SPLIT_BAY_DIR_H
