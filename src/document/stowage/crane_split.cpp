#include "crane_split.h"

#include "callutils.h"
#include "container_leg.h"
#include "container_list.h"
#include "document.h"
#include "problem_model.h"
#include "stowage.h"

#include <stowplugininterface/problem.h>

#include <ange/containers/container.h>
#include <ange/schedule/call.h>
#include <ange/schedule/cranerules.h>
#include <ange/schedule/schedule.h>
#include <ange/vessel/bayslice.h>
#include <ange/vessel/decklevel.h>
#include <ange/vessel/slot.h>
#include <ange/vessel/stack.h>
#include <ange/vessel/stacksupport.h>
#include <ange/vessel/vessel.h>

#include <QSignalMapper>
#include <QTimer>

#include <cmath>
#include <limits>

using namespace ange::units;
using namespace ange::angelstow;
using namespace ange::containers;
using namespace ange::schedule;
using namespace ange::vessel;

crane_split_t::crane_split_t(document_t* document, QObject* parent)
  : QObject(parent), m_document(document), m_call_mapper(new QSignalMapper(this))
{
    setObjectName("CraneSplit");
    if (document) {
        set_document(document);
    }
    m_buffer_timer = new QTimer(this);
    m_buffer_timer->setSingleShot(true);
    m_buffer_timer->setInterval(100);
    connect(m_buffer_timer, &QTimer::timeout, this, &crane_split_t::do_buffered_calculations);
    connect(m_call_mapper, static_cast<void(QSignalMapper::*)(int)>(&QSignalMapper::mapped), this, &crane_split_t::update_call_by_id);
    connect(this, &crane_split_t::bay_changed, this, &crane_split_t::update_call);
}

void crane_split_t::set_document(document_t* document) {
    if (m_document) {
        m_document->schedule()->disconnect(this);
        m_document->containers()->disconnect(this);
    }
    m_document = document;
    // Signals &Schedule::callAdded and ::callRemoved is triggered through stowage_t
    connect(m_document->schedule(), &Schedule::callAboutToBeRemoved, this, &crane_split_t::slotCallAboutToBeRemoved);
    connect(m_document->containers(), &container_list_t::rowsInserted, this, &crane_split_t::containersAdded);
    connect(m_document->containers(), &container_list_t::rowsAboutToBeRemoved, this, &crane_split_t::containersRemoved);
    // Reset:
    m_changed_calls.clear();
    typedef QHash<int, crane_split_bay_t*> bays_t;
    Q_FOREACH (bays_t bays, m_bays) {
        qDeleteAll(bays);
    }
    m_bays.clear();
    m_calls_data.clear();
    Q_FOREACH (QList<crane_split_bin_t*> bins, m_bins) {
        qDeleteAll(bins);
    }
    m_bins.clear();
    Q_FOREACH (Call* call, m_document->schedule()->calls()) {
        call->disconnect(m_call_mapper);
        add_call(call);
    }
}

void crane_split_t::add_call(ange::schedule::Call* call) {
    QHash<int, crane_split_bay_t*> call_bay;
    Q_FOREACH (BaySlice* slice, m_document->vessel()->baySlices()) {
        // TODO: right now the crane split calulation of total time does not support twinlifting on to be set as a parameter on stack level,
        // so we go through the stacks and if all support twinlifting the bay support twinlifting.
        bool twinlifting_above = true;
        bool twinlifting_below = true;
        Q_FOREACH (ange::vessel::Stack* stack, slice->stacks()) {
            if (!stack->isTwinliftSupported()) {
                if (stack->level() == ange::vessel::Above) {
                    twinlifting_above = false;
                } else {
                    twinlifting_below = false;
                }
            }
        }
        crane_split_bay_t* bay = new crane_split_bay_t(slice->joinedBay(), call, this, twinlifting_above, twinlifting_below);
        call_bay.insert(slice->joinedBay(), bay);
    }
    m_bays.insert(call, call_bay);

    // Calculate initial expected
    call_data_t cs(0.8, 1.0, 0.0, 0.0);
    ContainerLeg* routes = m_document->stowage()->container_routes();
    Q_FOREACH (Container* container, m_document->containers()->list()) {
        const bool istwinliftable = container->isoLength() == IsoCode::Twenty;
        if (routes->portOfDischarge(container) == call || routes->portOfLoad(container) == call) {
            cs.m_expectedTotalTimeNormalized += istwinliftable ? cs.m_twinliftDurationNormalized : cs.m_singleliftDurationNormalized;
        }
    }
    m_calls_data.insert(call, cs);
    calculate_bins_for_call(call);
    m_call_mapper->setMapping(call, call->id());
    connect(call, &Call::cranesChanged, m_call_mapper, static_cast<void(QSignalMapper::*)()>(&QSignalMapper::map));
    connect(call, &Call::craneRuleChanged, m_call_mapper, static_cast<void(QSignalMapper::*)()>(&QSignalMapper::map));
    connect(call, &Call::twinLiftingChanged, m_call_mapper, static_cast<void(QSignalMapper::*)()>(&QSignalMapper::map));
    connect(call, &Call::craneProductivityChanged, m_call_mapper, static_cast<void(QSignalMapper::*)()>(&QSignalMapper::map));
}

void crane_split_t::calculate_bins_for_call(const ange::schedule::Call* call) {
    if (m_bins.contains(call)) {
        qDeleteAll(m_bins[call]);
        m_bins.remove(call);
    }
    // this is an attempt to make a better bin distribution, the crane rules are implemented for the 40', and 80' rule, and for living quaters.
    QHash<int, crane_split_bay_t*> call_bay = m_bays[call];
    QList<BaySlice*> baySlices = m_document->vessel()->baySlices();
    QPair<int, int> living_quater_surrounding_bays = living_quater_pseudo_bay();
    int aft_bay_number = 0;
    if (!baySlices.isEmpty()) {
        aft_bay_number = baySlices.last()->bays().last();
    }
    ange::schedule::CraneRules::Types crane_rule = call->craneRule();
    int baySlice_number = 0;
    Q_FOREACH (BaySlice* baySlice, baySlices) {
        int bay_count = 0;
        Q_FOREACH (int bay_number, baySlice->bays()) {
            ++bay_count;
            int bay_span;
            if (crane_rule == ange::schedule::CraneRules::CraneRuleEighty) {
                if (bay_number % 2) {
                    bay_span = 7;
                } else {
                    bay_span = 8;
                }
            } else {
                bay_span = 7;
            }
            if ((living_quater_surrounding_bays.second >= bay_number
                    && bay_number + bay_span > living_quater_surrounding_bays.second
                    && bay_number + bay_span - 4 < living_quater_surrounding_bays.first) ||
                    (living_quater_surrounding_bays.first <= bay_number && bay_number + bay_span > aft_bay_number)) {
                break;
            } else {
                bool spanning_living_quater = false;
                if (bay_number + bay_span > living_quater_surrounding_bays.second && living_quater_surrounding_bays.second >= bay_number) {
                    spanning_living_quater = true;
                }
                typedef QPair<crane_split_bay_t*, crane_split_bin_include_t::crane_split_bin_includes_t> crane_split_bay_include_t;
                QList< crane_split_bay_include_t > bin_slices;
                if (baySlice->bays().size() == 1) {
                    if (bay_number % 2) {
                        bin_slices << crane_split_bay_include_t(call_bay.value(baySlice->joinedBay()), crane_split_bin_include_t::PURE_TWENTY_BAY);
                    } else {
                        bin_slices << crane_split_bay_include_t(call_bay.value(baySlice->joinedBay()), crane_split_bin_include_t::PURE_FOURTY_BAY);
                    }
                } else if (baySlice->bays().size() == 2) {
                    if (bay_count == 1) {
                        bin_slices << crane_split_bay_include_t(call_bay.value(baySlice->joinedBay()), crane_split_bin_include_t::FORE20_AFT20_BAYS);
                    } else {
                        bin_slices << crane_split_bay_include_t(call_bay.value(baySlice->joinedBay()), crane_split_bin_include_t::AFT20_BAY);
                    }
                } else if (baySlice->bays().size() == 3) {
                    if (bay_count == 1) {
                        bin_slices << crane_split_bay_include_t(call_bay.value(baySlice->joinedBay()), crane_split_bin_include_t::FORE20_FOURTY_AFT20_BAYS);
                    } else if (bay_count == 2) {
                        bin_slices << crane_split_bay_include_t(call_bay.value(baySlice->joinedBay()), crane_split_bin_include_t::FOURTY_AFT20_BAYS);
                    } else {
                        bin_slices << crane_split_bay_include_t(call_bay.value(baySlice->joinedBay()), crane_split_bin_include_t::AFT20_BAY);
                    }
                } else {
                    Q_ASSERT(false);
                }
                for (int i = baySlice_number + 1; i < baySlices.size(); ++i) {
                    int bay_span_next = spanning_living_quater ? bay_span - 4 : bay_span;
                    ange::vessel::BaySlice* next_baySlice = baySlices.at(i);
                    if (next_baySlice->bays().first() > bay_number + bay_span_next) {
                        break;
                    }
                    if (next_baySlice->bays().size() == 1) {
                        if (next_baySlice->bays().first() % 2) {
                            bin_slices << crane_split_bay_include_t(call_bay.value(next_baySlice->joinedBay()), crane_split_bin_include_t::PURE_TWENTY_BAY);
                        } else {
                            bin_slices << crane_split_bay_include_t(call_bay.value(next_baySlice->joinedBay()), crane_split_bin_include_t::PURE_FOURTY_BAY);
                        }
                    } else if (next_baySlice->bays().size() == 2) {
                        if (next_baySlice->bays().last() <= bay_number + bay_span_next) {
                            bin_slices << crane_split_bay_include_t(call_bay.value(next_baySlice->joinedBay()), crane_split_bin_include_t::FORE20_AFT20_BAYS);
                        } else {
                            bin_slices << crane_split_bay_include_t(call_bay.value(next_baySlice->joinedBay()), crane_split_bin_include_t::FORE20_BAY);
                        }
                    } else if (next_baySlice->bays().size() == 3) {
                        if (next_baySlice->bays().last() <= bay_number + bay_span_next) {
                            bin_slices << crane_split_bay_include_t(call_bay.value(next_baySlice->joinedBay()), crane_split_bin_include_t::FORE20_FOURTY_AFT20_BAYS);
                        } else if (next_baySlice->bays().first() == bay_number + bay_span_next) {
                            bin_slices << crane_split_bay_include_t(call_bay.value(next_baySlice->joinedBay()), crane_split_bin_include_t::FORE20_BAY);
                        } else {
                            bin_slices << crane_split_bay_include_t(call_bay.value(next_baySlice->joinedBay()), crane_split_bin_include_t::FORE20_FORTY_BAYS);
                        }
                    } else {
                        Q_ASSERT(false);
                    }
                }
                // Here we have created the list of bayslices and how much of them is included.
                crane_split_bin_t* bin = new crane_split_bin_t(this, bin_slices, 1);
                // qDebug() << "bin created: (" << bin_slices.first().first->joinedBay() << "," << bin_slices.first().second << ") to (" << bin_slices.last().first->joinedBay() << "," << bin_slices.last().second << ")";
                m_bins[call] << bin;
            }
        }
        ++baySlice_number;
    }
    // Now we want to do the same to find the super bins, but only when we are using the crane rule 40'+
    if (crane_rule == ange::schedule::CraneRules::CraneRuleFourtySuperBins) {
        int baySlice_number = 0;
        Q_FOREACH (BaySlice* baySlice, baySlices) {
            int bay_count = 0;
            Q_FOREACH (int bay_number, baySlice->bays()) {
                ++bay_count;
                int bay_span = 19;
                if ((living_quater_surrounding_bays.second >= bay_number
                        && bay_number + bay_span > living_quater_surrounding_bays.second
                        && bay_number + bay_span - 4 < living_quater_surrounding_bays.first) ||
                        (living_quater_surrounding_bays.first <= bay_number && bay_number + bay_span > aft_bay_number)) {
                    break;
                } else {
                    bool spanning_living_quater = false;
                    if (bay_number + bay_span > living_quater_surrounding_bays.second && living_quater_surrounding_bays.second >= bay_number) {
                        spanning_living_quater = true;
                    }
                    typedef QPair<crane_split_bay_t*, crane_split_bin_include_t::crane_split_bin_includes_t> crane_split_bay_include_t;
                    QList< crane_split_bay_include_t > bin_slices;
                    if (baySlice->bays().size() == 1) {
                        if (bay_number % 2) {
                            bin_slices << crane_split_bay_include_t(call_bay.value(baySlice->joinedBay()), crane_split_bin_include_t::PURE_TWENTY_BAY);
                        } else {
                            bin_slices << crane_split_bay_include_t(call_bay.value(baySlice->joinedBay()), crane_split_bin_include_t::PURE_FOURTY_BAY);
                        }
                    } else if (baySlice->bays().size() == 2) {
                        if (bay_count == 1) {
                            bin_slices << crane_split_bay_include_t(call_bay.value(baySlice->joinedBay()), crane_split_bin_include_t::FORE20_AFT20_BAYS);
                        } else {
                            bin_slices << crane_split_bay_include_t(call_bay.value(baySlice->joinedBay()), crane_split_bin_include_t::AFT20_BAY);
                        }
                    } else if (baySlice->bays().size() == 3) {
                        if (bay_count == 1) {
                            bin_slices << crane_split_bay_include_t(call_bay.value(baySlice->joinedBay()), crane_split_bin_include_t::FORE20_FOURTY_AFT20_BAYS);
                        } else if (bay_count == 2) {
                            bin_slices << crane_split_bay_include_t(call_bay.value(baySlice->joinedBay()), crane_split_bin_include_t::FOURTY_AFT20_BAYS);
                        } else {
                            bin_slices << crane_split_bay_include_t(call_bay.value(baySlice->joinedBay()), crane_split_bin_include_t::AFT20_BAY);
                        }
                    } else {
                        Q_ASSERT(false);
                    }
                    for (int i = baySlice_number + 1; i < baySlices.size(); ++i) {
                        int bay_span_next = spanning_living_quater ? bay_span - 4 : bay_span;
                        ange::vessel::BaySlice* next_baySlice = baySlices.at(i);
                        if (next_baySlice->bays().first() > bay_number + bay_span_next) {
                            break;
                        }
                        if (next_baySlice->bays().size() == 1) {
                            if (next_baySlice->bays().first() % 2) {
                                bin_slices << crane_split_bay_include_t(call_bay.value(next_baySlice->joinedBay()), crane_split_bin_include_t::PURE_TWENTY_BAY);
                            } else {
                                bin_slices << crane_split_bay_include_t(call_bay.value(next_baySlice->joinedBay()), crane_split_bin_include_t::PURE_FOURTY_BAY);
                            }
                        } else if (next_baySlice->bays().size() == 2) {
                            if (next_baySlice->bays().last() <= bay_number + bay_span_next) {
                                bin_slices << crane_split_bay_include_t(call_bay.value(next_baySlice->joinedBay()), crane_split_bin_include_t::FORE20_AFT20_BAYS);
                            } else {
                                bin_slices << crane_split_bay_include_t(call_bay.value(next_baySlice->joinedBay()), crane_split_bin_include_t::FORE20_BAY);
                            }
                        } else if (next_baySlice->bays().size() == 3) {
                            if (next_baySlice->bays().last() <= bay_number + bay_span_next) {
                                bin_slices << crane_split_bay_include_t(call_bay.value(next_baySlice->joinedBay()), crane_split_bin_include_t::FORE20_FOURTY_AFT20_BAYS);
                            } else if (next_baySlice->bays().first() == bay_number + bay_span_next) {
                                bin_slices << crane_split_bay_include_t(call_bay.value(next_baySlice->joinedBay()), crane_split_bin_include_t::FORE20_BAY);
                            } else {
                                bin_slices << crane_split_bay_include_t(call_bay.value(next_baySlice->joinedBay()), crane_split_bin_include_t::FORE20_FORTY_BAYS);
                            }
                        } else {
                            Q_ASSERT(false);
                        }
                    }
                    // Here we have created the list of bayslices and how much of them is included.
                    crane_split_bin_t* bin = new crane_split_bin_t(this, bin_slices, 2, true);
                    // qDebug() << "bin created: (" << bin_slices.first().first->joinedBay() << "," << bin_slices.first().second << ") to (" << bin_slices.last().first->joinedBay() << "," << bin_slices.last().second << ")";
                    m_bins[call] << bin;
                }
            }
            ++baySlice_number;
        }
    }
}

void crane_split_t::remove_call(ange::schedule::Call* call) {
    QList<crane_split_bin_t*> bins = m_bins.take(call);
    delete(m_problems.take(call));
    QHash<int, crane_split_bay_t*> call_bay = m_bays.take(call);
    Q_FOREACH(int i, call_bay.keys()) {
        delete call_bay[i];
    }
    qDeleteAll(bins);
}

QPair<int, int> crane_split_t::living_quater_pseudo_bay() {
    if (std::isnan(m_document->vessel()->observerLcg() / meter)) {
        int last_bay;
        if (!m_document->vessel()->baySlices().isEmpty()) {
            last_bay = m_document->vessel()->baySlices().last()->bays().last();
        } else {
            last_bay = 0;
        }
        return QPair<int, int>(last_bay, last_bay);
    }
    Length living_quater = m_document->vessel()->observerLcg();
    int living_quater_fore_bay = 1;
    Length living_quater_fore_lcg = std::numeric_limits<double>::infinity() * meter;
    int living_quater_aft_bay = -1;
    Length living_quater_aft_lcg = - std::numeric_limits<double>::infinity() * meter;
    Q_FOREACH(BaySlice * baySlice, m_document->vessel()->baySlices()) {
        Length bay_lcg_min = std::numeric_limits<double>::infinity() * meter;
        Length bay_lcg_max = - std::numeric_limits<double>::infinity() * meter;
        if (baySlice->stacks().first()->stackSupport20Aft()) {
            Length lcg = baySlice->stacks().first()->stackSupport20Aft()->bottomPos().l();
            bay_lcg_min = lcg <= bay_lcg_min ? lcg : bay_lcg_min;
            bay_lcg_max = lcg >= bay_lcg_max ? lcg : bay_lcg_max;
        }
        if (baySlice->stacks().first()->stackSupport20Fore()) {
            Length lcg = baySlice->stacks().first()->stackSupport20Fore()->bottomPos().l();
            bay_lcg_min = lcg <= bay_lcg_min ? lcg : bay_lcg_min;
            bay_lcg_max = lcg >= bay_lcg_max ? lcg : bay_lcg_max;
        }
        if (baySlice->stacks().first()->stackSupport40()) {
            Length lcg = baySlice->stacks().first()->stackSupport40()->bottomPos().l();
            bay_lcg_min = lcg <= bay_lcg_min ? lcg : bay_lcg_min;
            bay_lcg_max = lcg >= bay_lcg_max ? lcg : bay_lcg_max;
        }
        Q_ASSERT(!baySlice->bays().empty());
        if (living_quater > bay_lcg_max && living_quater_aft_lcg < bay_lcg_max) {
            living_quater_aft_bay = baySlice->bays().first();
            living_quater_aft_lcg = bay_lcg_max;
        }
        if (living_quater < bay_lcg_min && living_quater_fore_lcg > bay_lcg_min) {
            living_quater_fore_bay = baySlice->bays().last();
            living_quater_fore_lcg = bay_lcg_min;
        }
    }
    // the bridge is behind the container bays
    if (living_quater < living_quater_aft_lcg) {
        return QPair<int, int>(living_quater_fore_bay, living_quater_fore_bay);
        // the bridge is in front of the container bays (new type of vessel)
    } else if (living_quater > living_quater_fore_lcg) {
        return QPair<int, int>(living_quater_aft_bay, living_quater_aft_bay);
        // the bridge is somewhere in the middle of the vessel
    } else if (living_quater > living_quater_aft_lcg && living_quater < living_quater_fore_lcg) {
        return QPair<int, int>(living_quater_aft_bay, living_quater_fore_bay);
    }
    Q_ASSERT(false);
    return QPair<int, int>(-1, -1);
}

void crane_split_t::modify_move(crane_split_bay_dir_t& data, const int direction, const bool restow,
                                const Call* call, const Slot* slot, const IsoCode::IsoLength isoLength) {
    if (is_befor_or_after(call)) {
        return; // ignore before/after
    }
    call_data_t& call_data = m_calls_data[call];
    double twinlift_prod_above = call_data.m_singleliftDurationNormalized;
    double twinlift_prod_below = call_data.m_singleliftDurationNormalized;
    crane_split_bay_t* bay = m_bays.value(call).value(slot->stack()->baySlice()->joinedBay());
    if (call->twinLifting() == ange::schedule::TwinLiftRules::TwinliftRuleAll ||
            call->twinLifting() == ange::schedule::TwinLiftRules::TwinliftRuleUnknown) {
        if (bay->vessel_twinlifting_above()) {
            twinlift_prod_above = call_data.m_twinliftDurationNormalized;
        }
        if (bay->vessel_twinlifting_below()) {
            twinlift_prod_below = call_data.m_twinliftDurationNormalized;
        }
    }
    if (call->twinLifting() == ange::schedule::TwinLiftRules::TwinliftRuleBelow) {
        if (bay->vessel_twinlifting_below()) {
            twinlift_prod_below = call_data.m_twinliftDurationNormalized;
        }
    }
    if (isoLength == IsoCode::Twenty) {
        if (bay->joinedBay() >= slot->brt().bay()) {
            if (slot->stack()->level() == ange::vessel::Above) {
                data.twentyFore().above += direction;
                call_data.m_actualTotalTimeNormalized += direction * twinlift_prod_above;
                if (restow) {
                    call_data.m_expectedTotalTimeNormalized += direction * twinlift_prod_above;
                }
            } else {
                data.twentyFore().below += direction;
                call_data.m_actualTotalTimeNormalized += direction * twinlift_prod_below;
                if (restow) {
                    call_data.m_expectedTotalTimeNormalized += direction * twinlift_prod_below;
                }
            }
        } else {
            if (slot->stack()->level() == ange::vessel::Above) {
                data.twentyAft().above += direction;
                call_data.m_actualTotalTimeNormalized += direction * twinlift_prod_above;
                if (restow) {
                    call_data.m_expectedTotalTimeNormalized += direction * twinlift_prod_above;
                }
            } else {
                data.twentyAft().below += direction;
                call_data.m_actualTotalTimeNormalized += direction * twinlift_prod_below;
                if (restow) {
                    call_data.m_expectedTotalTimeNormalized += direction * twinlift_prod_below;
                }
            }
        }
    } else {
        if (slot->stack()->level() == ange::vessel::Above) {
            data.fourty().above += direction;
        } else {
            data.fourty().below += direction;
        }
        call_data.m_actualTotalTimeNormalized += direction * call_data.m_singleliftDurationNormalized;
        if (restow) {
            call_data.m_expectedTotalTimeNormalized += direction * twinlift_prod_below;
        }
    }
    emit bay_changed(call, bay->joinedBay());
}

void crane_split_t::add_discharge(const Call* call, const Slot* slot, const IsoCode::IsoLength isoLength) {
    if (is_befor_or_after(call)) {
        return;
    }
    Q_ASSERT(slot);
    crane_split_bay_t* bay = m_bays.value(call).value(slot->stack()->baySlice()->joinedBay());
    Q_ASSERT(bay);
    if (bay) {
        modify_move(bay->discharge(), 1, false, call, slot, isoLength);
    }
}

void crane_split_t::subtract_discharge(const Call* call, const Slot* slot, const IsoCode::IsoLength isoLength) {
    if (is_befor_or_after(call)) {
        return;
    }
    crane_split_bay_t* bay = m_bays.value(call).value(slot->stack()->baySlice()->joinedBay());
    Q_ASSERT(bay);
    if (bay) {
        modify_move(bay->discharge(), -1, false, call, slot, isoLength);
    }
}

void crane_split_t::add_load(const Call* call, const Slot* slot, const IsoCode::IsoLength isoLength) {
    if (is_befor_or_after(call)) {
        return;
    }
    Q_ASSERT(slot);
    crane_split_bay_t* bay = m_bays.value(call).value(slot->stack()->baySlice()->joinedBay());
    Q_ASSERT(bay);
    if (bay) {
        modify_move(bay->load(), 1, false, call, slot, isoLength);
    }
}

void crane_split_t::subtract_load(const Call* call, const Slot* slot, const IsoCode::IsoLength isoLength) {
    if (is_befor_or_after(call)) {
        return;
    }
    crane_split_bay_t* bay = m_bays.value(call).value(slot->stack()->baySlice()->joinedBay());
    Q_ASSERT(bay);
    if (bay) {
        modify_move(bay->load(), -1, false, call, slot, isoLength);
    }
}

void crane_split_t::add_restow(const Call* call, const Slot* origin, const Slot* destination, const IsoCode::IsoLength isoLength) {
    if (is_befor_or_after(call)) {
        return;
    }
    Q_ASSERT(origin);
    Q_ASSERT(destination);
    crane_split_bay_t* bay = m_bays.value(call).value(origin->stack()->baySlice()->joinedBay());
    Q_ASSERT(bay);
    if (bay) {
        modify_move(bay->restow(), 1, true, call, origin, isoLength);
    }
    bay = m_bays.value(call).value(destination->stack()->baySlice()->joinedBay());
    Q_ASSERT(bay);
    if (bay) {
        modify_move(bay->restow(), 1, true, call, destination, isoLength);
    }
}

void crane_split_t::subtract_restow(const Call* call, const Slot* origin, const Slot* destination, const IsoCode::IsoLength isoLength) {
    if (is_befor_or_after(call)) {
        return;
    }
    Q_ASSERT(origin);
    Q_ASSERT(destination);
    crane_split_bay_t* bay = m_bays.value(call).value(origin->stack()->baySlice()->joinedBay());
    Q_ASSERT(bay);
    if (bay) {
        modify_move(bay->restow(), -1, true, call, origin, isoLength);
    }
    bay = m_bays.value(call).value(destination->stack()->baySlice()->joinedBay());
    Q_ASSERT(bay);
    if (bay) {
        modify_move(bay->restow(), -1, true, call, destination, isoLength);
    }
}
double crane_split_t::no_cranes(const ange::schedule::Call* call) const {
    const double cranes = call->cranes();
    return std::isnan(cranes) ? 3.0 : cranes;

}
double crane_split_t::singlelift_productivity(const ange::schedule::Call* call) const {
    return m_calls_data.value(call).m_singleliftDurationNormalized / call->craneProductivity();
}
double crane_split_t::actual_total_crane_time(const ange::schedule::Call* call) const {
    return m_calls_data.value(call).m_actualTotalTimeNormalized / call->craneProductivity();
}
double crane_split_t::expected_total_crane_time(const ange::schedule::Call* call) const {
    return m_calls_data.value(call).m_expectedTotalTimeNormalized / call->craneProductivity();
}

double crane_split_t::twinlift_productivity(const ange::schedule::Call* call) const {
    return m_calls_data.value(call).m_twinliftDurationNormalized / call->craneProductivity();
}

QList< crane_split_bin_t* > crane_split_t::bins(const ange::schedule::Call* call) const {
    return m_bins.value(call);
}

void crane_split_t::update_call_now(const ange::schedule::Call* call) {
    double ncranes = no_cranes(call);
    if (ncranes <= 1.0) {
        ncranes = 1.0;
    }
    const double total_time = expected_total_crane_time(call);
    const double max_ideal_time_per_crane  = total_time / ncranes;
    bool has_warnings = false;
    bool has_notices = false;
    Q_FOREACH(crane_split_bin_t * bin, m_bins[call]) {
        const qreal crane_time = bin->moveStatistics().time / bin->max_cranes();
        if (crane_time > max_ideal_time_per_crane * 1.05) {
            has_warnings = true;
            break; // No need to check the rest, the problem is as bad as it can be
        } else if (crane_time > max_ideal_time_per_crane * 0.95) {
            has_notices = true;
        }
    }
    Problem*& problem = m_problems[call];
    if (problem && !has_warnings && !has_notices) {
        delete problem;
        problem = 0;
    } else {
        if (has_warnings) {
            if (!problem || problem->severity() != Problem::Warning) {
                delete problem;
                ProblemLocation loc;
                loc.setCall(call);
                problem = new Problem(Problem::Warning, loc, QStringLiteral("Crane split problem"));
                problem->setDetails("Crane split limit violated in " + call->name());
                m_document->problem_model()->add_problem(problem);
            }
        } else if (has_notices) {
            if (!problem || problem->severity() != Problem::Notice) {
                delete problem;
                ProblemLocation loc;
                loc.setCall(call);
                problem = new Problem(Problem::Notice, loc, QStringLiteral("Crane split problem"));
                problem->setDetails("Crane split limit almost reached in " + call->name());
                m_problems.insert(call, problem);
                m_document->problem_model()->add_problem(problem);
            }
        }
    }
    emit calculations_done(call);
}

void crane_split_t::update_call(const ange::schedule::Call* call) {
    if (is_befor_or_after(call)) {
        return;
    }
    m_changed_calls << call;
    if (!m_buffer_timer->isActive()) {
        m_buffer_timer->start();
    }
}

void crane_split_t::update_call_by_id(int call_id) {
    if (const Call* call = m_document->schedule()->getCallById(call_id)) {
        calculate_bins_for_call(call);
        update_call(call);
    }
}

void crane_split_t::do_buffered_calculations() {
    QSet<const Call*> calls = m_changed_calls;
    m_changed_calls.clear();
    Q_FOREACH(const ange::schedule::Call * call, calls) {
        update_call_now(call);
    }
}

crane_split_t::~crane_split_t() {
    qDeleteAll(m_problems);
    Q_FOREACH(const ange::schedule::Call * call, m_bins.keys()) {
        qDeleteAll(m_bins.value(call));
    }
}

void crane_split_t::slotCallAboutToBeRemoved(ange::schedule::Call* call) {
    m_changed_calls.remove(call);
}

void crane_split_t::update_expected_with_new_destination(const ange::containers::Container* container, const ange::schedule::Call* old_call) {
    const bool is_twin_liftable = container->isoLength() == IsoCode::Twenty;
    if (old_call) {
        call_data_t& cs = m_calls_data[old_call];
        cs.m_expectedTotalTimeNormalized -= is_twin_liftable ? cs.m_twinliftDurationNormalized : cs.m_singleliftDurationNormalized;
    }
    if (const ange::schedule::Call* new_call = m_document->stowage()->container_routes()->portOfDischarge(container)) {
        call_data_t& cs = m_calls_data[new_call];
        cs.m_expectedTotalTimeNormalized += is_twin_liftable ? cs.m_twinliftDurationNormalized : cs.m_singleliftDurationNormalized;
    }
}

void crane_split_t::update_expected_with_new_source(const ange::containers::Container* container, const ange::schedule::Call* old_call) {
    const bool is_twin_liftable = container->isoLength() == IsoCode::Twenty;
    if (old_call && !is_befor_or_after(old_call)) {
        call_data_t& cs = m_calls_data[old_call];
        cs.m_expectedTotalTimeNormalized -= is_twin_liftable ? cs.m_twinliftDurationNormalized : cs.m_singleliftDurationNormalized;
    }
    if (const ange::schedule::Call* new_call = m_document->stowage()->container_routes()->portOfLoad(container)) {
        if (!is_befor_or_after(new_call)) {
            call_data_t& cs = m_calls_data[new_call];
            cs.m_expectedTotalTimeNormalized += is_twin_liftable ? cs.m_twinliftDurationNormalized : cs.m_singleliftDurationNormalized;
        }
    }
}

void crane_split_t::containersAdded(const QModelIndex& parent, int start, int end) {
    for (int i = start ; i <= end ; i++) {
        QModelIndex idx = m_document->containers()->index(i, 0, parent);
        Container* container = idx.data(Qt::UserRole).value<Container*>();
        if (container) {
            update_expected_with_new_source(container, 0);
            update_expected_with_new_destination(container, 0);
        }
    }
}

void crane_split_t::containersRemoved(const QModelIndex& parent, int start, int end) {
    for (int i = start ; i <= end ; i++) {
        QModelIndex idx = m_document->containers()->index(i, 0, parent);
        Container* container = idx.data(Qt::UserRole).value<Container*>();
        Q_ASSERT(container);
        if (!container) {
            continue;
        }
        const bool is_twin_liftable = container->isoLength() == IsoCode::Twenty;
        const Call* source_call = m_document->stowage()->container_routes()->portOfLoad(container);
        if (source_call && !is_befor_or_after(source_call)) {
            call_data_t& cs = m_calls_data[source_call];
            cs.m_expectedTotalTimeNormalized -= is_twin_liftable ? cs.m_twinliftDurationNormalized : cs.m_singleliftDurationNormalized;
        }
        const Call* dest_call = m_document->stowage()->container_routes()->portOfDischarge(container);
        if (dest_call) {
            call_data_t& cs = m_calls_data[dest_call];
            cs.m_expectedTotalTimeNormalized -= is_twin_liftable ? cs.m_twinliftDurationNormalized : cs.m_singleliftDurationNormalized;
        }
    }
}

#include "crane_split.moc"
