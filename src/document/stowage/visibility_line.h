#ifndef VISIBILITY_LINE_H
#define VISIBILITY_LINE_H

#include "stowplugininterface/problem.h"

#include <ange/units/units.h>

#include <QObject>
#include <QPair>
#include <QPointF>

class document_t;
namespace ange {
namespace schedule {
class Call;
}
namespace vessel {
class BaySlice;
class Stack;
class StackSupport;
}
}

/**
 * Helper class to calculate the visibility line.
 */
class visibility_line_t : public QObject {

    Q_OBJECT

public:

    explicit visibility_line_t(document_t* document);

    void set_document(document_t* document);

    ange::units::Length get_visibility_maxheight(const ange::vessel::StackSupport* stack_support, const ange::schedule::Call* call);

    ange::units::Length get_visibility_maxheight_coordinate(const ange::schedule::Call* call, const ange::vessel::BaySlice* bayslice);

    QPair<double, double> get_anglespan(const ange::vessel::StackSupport* someStackSupport);

    bool is_stack_in_forward_view(const ange::vessel::Stack* stack);

    /**
     * update the visibility line violations. This has to be done when center of mass has changed.
     */
    void update_visibility_line_violations(const ange::schedule::Call* call);

    /**
     * report if a stack is blocking the visibility
     */
    void add_stack_blocking_visibility(const ange::schedule::Call* call, int id, QPointF blocking);

    /**
     * report if a stack is no longer blocking the visibility
     */
    void remove_stack_blocking_visibility(const ange::schedule::Call* call, int id);

    /**
     * @return true if the stacks block to much of the visibility.
     */
    bool has_error(const ange::schedule::Call* call);

Q_SIGNALS:

    void repaint_stowage_stack_labels();

private Q_SLOTS:
    void invalidate_call(const ange::schedule::Call* call);
    void remove_call(ange::schedule::Call* call);
    void update_cached_visibilityheights();
    void update_cached_visibilityheights(const ange::schedule::Call* call);
    void resetVessel();

private:
    QList< QPointF > get_rel_corners(const ange::vessel::StackSupport* stack_support);
    void update_mass_and_center_and_trim(const ange::schedule::Call* call);
    void calculate_visibility_line_violations(const ange::schedule::Call* call);
    ange::angelstow::Problem* report_problem(ange::angelstow::Problem::Severity severity, const QString& description,
                                             const ange::schedule::Call* call, const QString& details);

private:
    document_t* m_document;
    QHash< const ange::schedule::Call*, ange::units::Length > m_trim;
    QHash< const ange::schedule::Call*, ange::units::Length > m_draft;
    QPointF m_observation_point;
    bool m_observer_set;
    QHash<const ange::schedule::Call*, QHash<int,QPointF> > m_blocking_angles;
    QHash<const ange::schedule::Call*, bool > m_blocking_angles_dirty;
    QHash<const ange::schedule::Call*, QHash< const ange::vessel::BaySlice*, ange::units::Length> > m_visibility_maxheights;
    QHash<const ange::schedule::Call*,  bool > m_visibility_maxheights_dirty;
    QHash<const ange::schedule::Call*, ange::angelstow::Problem* > m_blocked_visibility_problem;

private:
    bool m_visibilityLineEnabled;

private:
    friend class DraftSurveyTest; // unit testing only

};

#endif // VISIBILITY_LINE_H
