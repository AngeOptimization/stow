#include "slot_call_content.h"

#include <ange/containers/container.h>

using namespace ange::angelstow;

slot_call_content_t::slot_call_content_t()
  : m_container(0), m_type(NonPlacedType)
{
    // Empty
}

slot_call_content_t::slot_call_content_t(const ange::containers::Container* container, StowType type)
  : m_container(container), m_type(type)
{
    Q_ASSERT(m_container);
    Q_ASSERT(m_type != NonPlacedType);
}

QDebug operator<<(QDebug dbg, const slot_call_content_t& contents) {
    dbg.nospace() << contents.container();
    if (contents.type() == MasterPlannedType) {
        dbg << " (m)";
    } else if (contents.type() == NonPlacedType) {
        dbg << " (up)";
    }
    return dbg.maybeSpace();
}
