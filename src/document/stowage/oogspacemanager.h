#ifndef OOGSPACEMANAGER_H
#define OOGSPACEMANAGER_H

#include <QObject>
#include <QHash>

class QTimer;
namespace ange {
namespace vessel {

class Slot;
}

namespace schedule {

class Call;
}

namespace containers {
class Container;
}
namespace angelstow {
class Problem;
}
}

/**
 * \brief class to manage and track oog space and problems related
 *
 */

class document_t;
class OOGSpaceManager : public QObject {
    Q_OBJECT

    public:
        ~OOGSpaceManager();
        OOGSpaceManager(document_t* document);

        void setDocument(document_t* document);

        /**
         * refreshes the data for this container. Queries stowage for position information.
         * Also triggers a update of problems
         * \param container that has had moves
         */
        void refresh(const ange::containers::Container* container);
        /**
         * \return all slots blocked by OOGs this call
         */
        QSet< const ange::vessel::Slot* > allBlockedSlots(const ange::schedule::Call* call) const;

    private Q_SLOTS:
        void checkOogViolations();
    private:
        const document_t* m_document;
        QHash<const ange::containers::Container* ,QHash<const ange::schedule::Call*, QList<const ange::vessel::Slot*> > > m_blockedByOog;
        void buildFullList(const ange::schedule::Call* call);
        QHash<const ange::schedule::Call*, QSet<const ange::vessel::Slot*> > m_allBlockedSlots;
        void containerPlacedAtCall(const ange::containers::Container* container, const ange::schedule::Call* call, const ange::vessel::Slot* slot);
        QTimer* m_delayedViolationsCheck;
        struct SlotCall {
            SlotCall(const ange::vessel::Slot* slot, const ange::schedule::Call* call) : m_slot(slot), m_call(call) {}
            const ange::vessel::Slot * m_slot;
            const ange::schedule::Call * m_call;
            bool operator==(const SlotCall& other) const  {
                return m_slot == other.m_slot && m_call == other.m_call;
            }
        };
        friend uint qHash(const SlotCall& slotCall);
        QHash<SlotCall, ange::angelstow::Problem*> m_slotCall_problems;
};

#endif // OOGSPACEMANAGER_H
