#ifndef CONTAINER_MOVE_H
#define CONTAINER_MOVE_H

#include <stowplugininterface/move.h>
#include <stowplugininterface/stowtype.h>

#include <QDebug>

namespace ange {
namespace schedule {
class Call;
}
namespace vessel {
class Slot;
}
namespace containers {
class Container;
}
}

/**
 * Class describing a container move from or to the vessel (to or from the yard).
 *
 * The class is a little more complex than a move_t, in particular it contains a
 * pointer to the container it is moving.
 */
class container_move_t {

public:
    /**
     * container moved from source to target in call.
     * @param container container moved
     * @param slot spot the container is moved to. If null, it is from the vessel to the yard
     * @param call when (during the schedule) the container is moved.
     * @param type the stowage type (non-placed, macro-placed, micro-placed)
     */
    container_move_t(const ange::containers::Container* container,
                     const ange::vessel::Slot* slot,
                     const ange::schedule::Call* call,
                     const ange::angelstow::StowType& type);

    /**
     * construct from move_t
     */
    container_move_t(const ange::containers::Container* container,
                     const ange::angelstow::Move& move);

    /**
     * Copy constructor
     */
    container_move_t(const container_move_t& other);

    /**
     * @return the interface part of the move
     */
    ange::angelstow::Move move() const {
      return m_move;
    }

    /**
     * @return the container that is moved
     */
    const ange::containers::Container* container() const {
      return m_container;
    }

    bool operator<(const container_move_t& rhs) const;

    bool operator==(const container_move_t& rhs) const {
      return m_container == rhs.m_container && m_move == rhs.m_move;
    }

    /**
     * Convert macro to micro stow (fixate move)
     */
    void set_stow_type(ange::angelstow::StowType type = ange::angelstow::MicroStowedType);

    /**
     * @return call of move
     */
    const ange::schedule::Call* call() const {
      return m_move.call();
    }

    /**
     * @return copy of the move in the given call
     */
    container_move_t inOtherCall(const ange::schedule::Call* call) const {
        return container_move_t(m_container, m_move.inOtherCall(call));
    }

    /**
    * @return the place the container was moved to (null = Quay)
    */
    const ange::vessel::Slot* slot() const {
      return m_move.slot();
    }

    /**
    * @return the stow type
    */
    ange::angelstow::StowType type() const {
      return m_move.type();
    }

private:
    const ange::containers::Container* m_container;
    ange::angelstow::Move m_move;

};

QDebug operator<<(QDebug dbg, const container_move_t* move);
QDebug operator<<(QDebug dbg, const container_move_t& move);

#endif // CONTAINER_MOVE_H
