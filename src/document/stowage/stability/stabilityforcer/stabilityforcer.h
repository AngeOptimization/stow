#ifndef STABILITYFORCER_H
#define STABILITYFORCER_H

#include <ange/vessel/blockweight.h>
#include <ange/units/units.h>

#include <QObject>
#include <QHash>

class document_t;
namespace ange {
namespace schedule {
class Call;
}
}

/**
 * Handles trim/draft calculations and the generation of a deadload that forces a given trim/draft.
 *
 * Also handles forced GM. If GM was placed somewhere else aggregating the signals from stowage and tanks
 * would be complicated.
 */
class StabilityForcer : public QObject {

    Q_OBJECT

public:

    explicit StabilityForcer(document_t* document);

    ~StabilityForcer();

    // Force trim and draft using deadload: (also called DraftSurvey)

    /**
     * \return the observed draft for \param call , or the natural computed draft if forced draft has not been set
     */
    ange::units::Length draft(const ange::schedule::Call* call) const;

    /**
     * Sets the observed draft for \param call to \param draft, freezes trim if not forced before
     */
    void setForcedDraft(const ange::schedule::Call* call, const ange::units::Length draft);

    /**
     * \return the observed trim for \param call , or the natural computed trim if forced trim has not been set
     */
    ange::units::Length trim(const ange::schedule::Call* call) const;

    /**
     * Sets the observed trim for \param call to \param trim, freezes draft if not forced before
     */
    void setForcedTrim(const ange::schedule::Call* call, const ange::units::Length trim);

    /**
     * \return true if draft and/or trim for \param call is forced
     */
    bool forcedDraftTrimIsActive(const ange::schedule::Call* call) const;

    /**
     * \return the extra deadload for \param call, or zero deadload if not set
     */
    ange::vessel::BlockWeight deadLoad(const ange::schedule::Call* call) const;

    /**
     * \return true if deadload for \param call is custom set
     * TODO: return a DraftSurvey::Mode instead and merge with deadLoadIsActive
     */
    bool deadLoadIsActive(const ange::schedule::Call* call) const;

    /**
     * Sets the extra deadload for \param call to \param draft
     */
    void setDeadLoad(const ange::schedule::Call* call, const ange::vessel::BlockWeight deadLoad);

    /**
     * clear observed draft, trim and deadload for \param call
     */
    void clearSurvey(const ange::schedule::Call* call);

    // Force GM, not using anything just be inconsistent

    /**
     * Get current GM, can be forced or real
     */
    ange::units::Length gm(const ange::schedule::Call* call, ange::units::Length realGm) const;

    /**
     * Force the GM
     */
    void forceGm(const ange::schedule::Call* call, ange::units::Length gm);

    /**
     * Clear forcing of GM
     */
    void clearForcedGm(const ange::schedule::Call* call);

    /**
     * @return true if GM is currently forced
     */
    bool forcedGmIsActive(const ange::schedule::Call* call) const;


Q_SIGNALS:

    /**
     * Data (trim, draft, "forceness") changed in draft survey
     */
    void stabilityForcerChanged(const ange::schedule::Call* call);

public Q_SLOTS:

    /**
     * Remove call from draft survey
     */
    void removeCall(const ange::schedule::Call* call);

    /**
     * Re-calculate draft survey
     */
    void recalculateDraftSurvey(const ange::schedule::Call* call);

    /**
     * Re-calculate draft survey for all calls
     */
    void recalculateDraftSurveyAllCalls();

private:

    enum Mode {
        DEADLOAD_ACTIVE, // default value
        DRAFTTRIM_ACTIVE
    };

private:

    Mode mode(const ange::schedule::Call* call) const;

    void clearCallData(const ange::schedule::Call* call);

    void setDeadLoadFromTrimOrDraft(const ange::schedule::Call* call);

    ange::units::Length draftFromStowage(const ange::schedule::Call* call) const;

    ange::units::Length trimFromStowage(const ange::schedule::Call* call) const;

    ange::units::Mass displacementFromDraft(const ange::units::Length draftForced,
                                            const ange::schedule::Call* call) const;

    ange::units::Length lcgFromDisplacementAndTrim(const ange::units::Mass displacementForced,
                                                   const ange::units::Length trimForced,
                                                   const ange::schedule::Call* call) const;

private:
    document_t* m_document;

    // Variables for "draft survey" (force trim+draft):
    QHash<const ange::schedule::Call*, Mode> m_mode;
    QHash<const ange::schedule::Call*, ange::units::Length> m_forcedDraft;
    QHash<const ange::schedule::Call*, ange::units::Length> m_forcedTrim;
    QHash<const ange::schedule::Call*, ange::vessel::BlockWeight> m_deadLoad;

    // Variables for force GM:
    QHash<const ange::schedule::Call*, ange::units::Length> m_forceGm;

private:
    friend class DraftSurveyTest; // unit testing only

};

#endif // STABILITYFORCER_H
