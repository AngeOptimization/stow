#include "stabilityforcer.h"

#include "document/document.h"
#include "document/portwaterdensity/portwaterdensity.h"
#include "document/tanks/tankconditions.h"
#include "document/stowage/stowage.h"

#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>
#include <ange/vessel/bilinearinterpolator.h>
#include <ange/vessel/blockweight.h>
#include <ange/vessel/vessel.h>
#include <ange/vessel/vesseltank.h>

using namespace ange::units;
using ange::schedule::Call;
using ange::schedule::Schedule;
using ange::vessel::BilinearInterpolator;
using ange::vessel::BlockWeight;
using ange::vessel::Vessel;

StabilityForcer::StabilityForcer(document_t* document)
  : QObject(document), m_document(document)
{
    setObjectName("StabilityForcer");
    connect(m_document->stowage(), &stowage_t::stowageChangedAtCall, this, &StabilityForcer::recalculateDraftSurvey);
    connect(m_document->portWaterDensity(), &PortWaterDensity::densityChanged, this, &StabilityForcer::recalculateDraftSurvey);
    connect(m_document->tankConditions(), &TankConditions::tankConditionChanged, this, &StabilityForcer::recalculateDraftSurvey);
    connect(m_document->schedule(), &Schedule::callAdded, this, &StabilityForcer::recalculateDraftSurvey);
    connect(m_document, &document_t::vessel_changed, this, &StabilityForcer::recalculateDraftSurveyAllCalls);
    connect(m_document->schedule(), &Schedule::callRemoved, this, &StabilityForcer::removeCall);
}

StabilityForcer::~StabilityForcer() {
    // Empty
}

Length StabilityForcer::draft(const Call* call) const {
    if (m_forcedDraft.contains(call)) {
        Q_ASSERT(qFuzzyCompare(m_forcedDraft.value(call) / meter, draftFromStowage(call) / meter));
        return m_forcedDraft.value(call);
    }
    return draftFromStowage(call);
}

Length StabilityForcer::draftFromStowage(const Call* call) const {
    BlockWeight stowageBlock = m_document->stowage()->stowageAsBlockWeight(call);
    Density waterDensity = m_document->portWaterDensity()->waterDensity(call);
    return m_document->vessel()->draft(stowageBlock.weight(), stowageBlock.lcg(), waterDensity);
}

Length StabilityForcer::trim(const Call* call) const {
    if (m_forcedTrim.contains(call)) {
        Q_ASSERT(qFuzzyCompare(m_forcedTrim.value(call) / meter, trimFromStowage(call) / meter));
        return m_forcedTrim.value(call);
    }
    return trimFromStowage(call);
}

Length StabilityForcer::trimFromStowage(const Call* call) const {
    BlockWeight stowageBlock = m_document->stowage()->stowageAsBlockWeight(call);
    Density waterDensity = m_document->portWaterDensity()->waterDensity(call);
    return m_document->vessel()->trim(stowageBlock.weight(), stowageBlock.lcg(), waterDensity);
}

void StabilityForcer::setForcedDraft(const Call* call, const Length draft) {
    m_forcedDraft[call] = draft;
    if (!m_forcedTrim.contains(call)) { // freeze both trim and draft at same time
        m_forcedTrim[call] = trimFromStowage(call);
    }
    setDeadLoadFromTrimOrDraft(call);
    m_mode[call] = DRAFTTRIM_ACTIVE;
    emit stabilityForcerChanged(call);
}

void StabilityForcer::setForcedTrim(const Call* call, const Length trim) {
    m_forcedTrim[call] = trim;
    if (!m_forcedDraft.contains(call)) { // freeze both trim and draft at same time
        m_forcedDraft[call] = draftFromStowage(call);
    }
    setDeadLoadFromTrimOrDraft(call);
    m_mode[call] = DRAFTTRIM_ACTIVE;
    emit stabilityForcerChanged(call);
}

StabilityForcer::Mode StabilityForcer::mode(const Call* call) const {
    return m_mode.value(call, DEADLOAD_ACTIVE);
}

bool StabilityForcer::forcedDraftTrimIsActive(const Call* call) const {
    return mode(call) == DRAFTTRIM_ACTIVE;
}

ange::vessel::BlockWeight StabilityForcer::deadLoad(const Call* call) const {
    return m_deadLoad.value(call);
}

bool StabilityForcer::deadLoadIsActive(const Call* call) const {
    return mode(call) == DEADLOAD_ACTIVE;
}

void StabilityForcer::setDeadLoad(const Call* call, const ange::vessel::BlockWeight deadLoad) {
    clearCallData(call);
    m_deadLoad[call] = deadLoad;
    // Done in clearCallData(), m_mode[call] = DEADLOAD_ACTIVE;
    emit stabilityForcerChanged(call);
}

void StabilityForcer::clearSurvey(const Call* call) {
    clearCallData(call);
    emit stabilityForcerChanged(call);
}

void StabilityForcer::removeCall(const Call* call) {
    clearCallData(call);
    // don't emit a draftSurveyChanged signal for a call that no longer exists
}

void StabilityForcer::recalculateDraftSurvey(const Call* call) {
    setDeadLoadFromTrimOrDraft(call);
    emit stabilityForcerChanged(call);
}

void StabilityForcer::recalculateDraftSurveyAllCalls() {
    Q_FOREACH (const Call* call, m_document->schedule()->calls()) {
        recalculateDraftSurvey(call);
    }
}

void StabilityForcer::clearCallData(const Call* call) {
    m_forcedDraft.remove(call);
    m_forcedTrim.remove(call);
    m_deadLoad.remove(call);
    m_mode.remove(call);
}

void StabilityForcer::setDeadLoadFromTrimOrDraft(const Call* call) {
    const Vessel* vessel = m_document->vessel();

    if (!vessel->draftData().isValid() || !vessel->trimData().isValid()) {
        // qDebug() << "no inverse hydrostatic lookup possible";
        return;
    }

    if (!m_forcedDraft.contains(call) && !m_forcedTrim.contains(call)) {
        // qDebug() << "no forced draft or trim";
        return;
    }

    Q_ASSERT(m_forcedDraft.contains(call));
    Q_ASSERT(m_forcedTrim.contains(call));

    const Length draftForced = m_forcedDraft.value(call);
    const Length trimForced = m_forcedTrim.value(call);

    // draft table is one-dimensional piecewise linear in (displacement, 0) -> draft, y values are always -1.0 and 1.0
    // trim table is piecewise bi-linear in (displacement, lcg) -> trim, largest variation in lcg
    // therefore it makes sense to compute the inverse displacement lookup first, and use this to compute the inverse lcg/moment
    const Mass displacementForced = displacementFromDraft(draftForced, call);
    const Length lcgForced = lcgFromDisplacementAndTrim(displacementForced, trimForced, call);
    BlockWeight blockForced(lcgForced, vessel->aftOverAll(), vessel->foreOverAll(), vessel->hullVcg(), 0 * meter, displacementForced);

    const BlockWeight totalBlockWeight = m_document->stowage()->stowageAsBlockWeight(call); // include exisiting deadLoad
    BlockWeight deadLoad(blockForced);
    deadLoad.extendWith(totalBlockWeight.oppositeWeight());
    deadLoad.extendWith(m_deadLoad.value(call));

    deadLoad.setDescription("Automatic deadload weight adjustment");

    m_deadLoad[call] = deadLoad;

    Q_ASSERT(qFuzzyCompare(m_forcedTrim.value(call) / meter, trimFromStowage(call) / meter));
    Q_ASSERT(qFuzzyCompare(m_forcedDraft.value(call) / meter, draftFromStowage(call) / meter));

    emit stabilityForcerChanged(call);
}

Mass  StabilityForcer::displacementFromDraft(const Length draftForced, const Call* call) const {
    const BilinearInterpolator draftData = m_document->vessel()->draftData();
    if (!draftData.isValid()) {
        return 0.0 * ton;
    }

    // draft table is one-dimensional piecewise linear in (displacement, 0) -> draft, y values are always -1.0 and 1.0
    // draftData.valueAt((displacement*Vessel::oceanWaterDensity)/(kilogram*waterDensity), lcg/meter)*meter;
    // X = (displacement*Vessel::oceanWaterDensity)/(kilogram*waterDensity)
    // Y = lcg/meter ( == 0.0 for draft table)

    // find piecewise linear segment in strictly increasing displacement->draft data
    int xSegmentStart = 0;
    for (int i = 0; i < draftData.xCoordinates().size(); ++i) {
        const qreal x = draftData.xCoordinates()[i];
        const qreal value = draftData.valueAt(x, 0.0);
        if (value * meter < draftForced) { // increasing values
            xSegmentStart = i;
        } else {
            break;
        }
        //qDebug() << "DRAFT TABLE" <<  i << x << 0.0 << value << xSegmentStart << draftForced/meter;
    }

    if (!(xSegmentStart + 1 < draftData.xCoordinates().size())) {
        return 0.0 * ton;
    }

    // linear inverse draft->displacement interpolation
    // displacement = (X*kilogram*waterDensity)/Vessel::oceanWaterDensity
    const Density waterDensity = m_document->portWaterDensity()->waterDensity(call);
    const Length draftSegmentStart = draftData.valueAt(draftData.xCoordinates()[xSegmentStart], 0.0) * meter;
    const Length draftSegmentEnd = draftData.valueAt(draftData.xCoordinates()[xSegmentStart + 1], 0.0) * meter;
    // qDebug() << "DRAFT TABLE segment draft start/forced/end" << draftSegmentStart/meter << draftForced/meter << draftSegmentEnd/meter;
    Q_ASSERT(draftSegmentStart < draftSegmentEnd); // increasing values
    Q_ASSERT(draftSegmentStart <= draftForced);
    Q_ASSERT(draftForced <= draftSegmentEnd);
    const Mass displacementSegmentStart = (draftData.xCoordinates()[xSegmentStart] * kilogram * waterDensity) / Vessel::oceanWaterDensity;
    const Mass displacementSegmentEnd = (draftData.xCoordinates()[xSegmentStart + 1] * kilogram * waterDensity) / Vessel::oceanWaterDensity;
    Q_ASSERT(displacementSegmentStart < displacementSegmentEnd);
    const Mass displacementForced
        = (displacementSegmentStart * (draftSegmentEnd - draftForced) + displacementSegmentEnd * (draftForced - draftSegmentStart)) / (draftSegmentEnd - draftSegmentStart);
    // qDebug() << "DRAFT TABLE segment displacement start/forced/end" << displacementSegmentStart/ton << displacementForced/ton << displacementSegmentEnd/ton;
    Q_ASSERT(displacementSegmentStart <= displacementForced);
    Q_ASSERT(displacementForced <= displacementSegmentEnd);

#if 0 // debug only
    {
        const BlockWeight totalBlock = m_document->stowage()->stowageAsBlockWeight(call);
        const Density waterDensity = m_document->portWaterDensity()->waterDensity(call);
        qDebug() << "DRAFT TABLE draft forced: " << draftForced / meter
                 << " recomputed by inverse displacement lookup: " << m_document->vessel()->draft(displacementForced, totalBlock.lcg(), waterDensity) / meter;
    }
#endif
    return displacementForced;
}

Length StabilityForcer::lcgFromDisplacementAndTrim(Mass displacementForced, Length trimForced, const Call* call) const {
    const BilinearInterpolator trimData = m_document->vessel()->trimData();
    if (!trimData.isValid()) {
        return 0.0 * meter;
    }

    // trim table is piecewise bi-linear in (displacement, lcg) -> trim, largest variation in lcg
    //trimData.valueAt((displacement*Vessel::oceanWaterDensity)/(kilogram*waterDensity), lcg/meter)*meter;
    // X = (displacement*Vessel::oceanWaterDensity)/(kilogram*waterDensity)
    // Y = lcg/meter
    const Density waterDensity = m_document->portWaterDensity()->waterDensity(call);
    const qreal forcedX = displacementForced * Vessel::oceanWaterDensity / (kilogram * waterDensity);

    // find piecewise linear segment in strictly decreasing lcg->trim data for fixed displacement
    int ySegmentStart = 0;
    for (int j = 0; j < trimData.yCoordinates().size(); ++j) {
        const qreal y = trimData.yCoordinates()[j];
        const qreal value = trimData.valueAt(forcedX, y);
        if (trimForced < value * meter) { // decreasing values
            ySegmentStart = j;
        } else {
            break;
        }
        //qDebug() << "TRIM TABLE" <<  j << forcedX << y << trimData.valueAt(forcedX, y);
    }

    if (!(ySegmentStart + 1 < trimData.yCoordinates().size())) {
        return 0.0 * meter;
    }

    // linear inverse trim->lcg interpolation for fixed displacement
    const Length trimSegmentStart = trimData.valueAt(forcedX, trimData.yCoordinates()[ySegmentStart]) * meter;
    const Length trimSegmentEnd = trimData.valueAt(forcedX, trimData.yCoordinates()[ySegmentStart + 1]) * meter;
    //qDebug() << "TRIM TABLE segment trim start/forced/end" << trimSegmentStart/meter << trimForced/meter << trimSegmentEnd/meter;
    Q_ASSERT(trimSegmentEnd < trimSegmentStart);  // decreasing values
    Q_ASSERT(trimSegmentEnd <= trimForced);
    Q_ASSERT(trimForced <= trimSegmentStart);
    const Length lcgSegmentStart = trimData.yCoordinates()[ySegmentStart] * meter;
    const Length lcgSegmentEnd = trimData.yCoordinates()[ySegmentStart + 1] * meter;
    Q_ASSERT(lcgSegmentStart < lcgSegmentEnd);
    const Length lcgForced = (lcgSegmentStart * (trimSegmentEnd - trimForced) + lcgSegmentEnd * (trimForced - trimSegmentStart)) / (trimSegmentEnd - trimSegmentStart);
    //qDebug() << "TRIM TABLE segment lcg  start/forced/end" << lcgSegmentStart/meter << lcgForced/meter << lcgSegmentEnd/meter;

#if 0 // debug only
    {
        const Density waterDensity = m_document->portWaterDensity()->waterDensity(call);
        qDebug() << "TRIM TABLE trim forced: " << trimForced / meter
                 << " recomputed by inverse lcg lookup: " << m_document->vessel()->trim(displacementForced, lcgForced, waterDensity) / meter;
    }
#endif
    return lcgForced;
}

Length StabilityForcer::gm(const Call* call, Length realGm) const {
    if (m_forceGm.contains(call)) {
        return m_forceGm.value(call);
    } else {
        return realGm;
    }
}

void StabilityForcer::forceGm(const Call* call, Length gm) {
    m_forceGm[call] = gm;
    emit stabilityForcerChanged(call);
}

void StabilityForcer::clearForcedGm(const Call* call) {
    m_forceGm.remove(call);
    emit stabilityForcerChanged(call);
}

bool StabilityForcer::forcedGmIsActive(const Call* call) const {
    return m_forceGm.contains(call);
}

#include "stabilityforcer.moc"
