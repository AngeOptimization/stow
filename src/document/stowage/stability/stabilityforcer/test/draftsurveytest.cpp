#include <QTest>

#include "stabilityforcer.h"

#include "document.h"
#include "document_interface.h"
#include "fileopener.h"
#include "portwaterdensity.h"
#include "stability.h"
#include "stowage.h"
#include "visibility_line.h"

#include <stowplugininterface/idocument.h>

#include <ange/schedule/schedule.h>
#include <ange/vessel/vessel.h>
#include <ange/units/units.h>

#include <QObject>

using namespace ange::units;
using ange::schedule::Call;
using ange::vessel::BlockWeight;
using ange::vessel::Vessel;

/**
 * Test intended to assure that setting forced drafts and trims do not mess up on other
 * calculations than the intended ones. For the moment beeing, this is only visibility line.
 */
class DraftSurveyTest : public QObject {
    Q_OBJECT

private Q_SLOTS:
    void testInverseDraftAndTrimLookup();
    void testForcedTrim();
    void testForcedDraft();
    void testDeadLoad();

private:

    template <class T>
    bool compare(T actual, T expected, T margin) {
        Q_ASSERT(0.0 * margin < margin); // only strictly positive units are allowed as margin
        // qDebug() << actual / T(1) << expected / T(1) << margin / T(1) << (expected - actual) / T(1)
        //          << abs(expected - actual) / T(1) << (abs(expected - actual) <= margin);
        return abs(expected - actual) <= margin;
    }

    Length draftTableRoundtrip(document_t*, const Call* call, Length draft);
    Length trimTableRoundtrip(document_t* document, const Call* call, Mass displacement, Length trim);
};


Length DraftSurveyTest::draftTableRoundtrip(document_t* document, const Call* call, Length draft)
{
    const Mass displacement =  document->stabilityForcer()->displacementFromDraft(draft, call);
    const BlockWeight totalBlock = document->stowage()->stowageAsBlockWeight(call);
    const Density waterDensity = document->portWaterDensity()->waterDensity(call);
    return document->vessel()->draft(displacement, totalBlock.lcg(), waterDensity);
}

Length DraftSurveyTest::trimTableRoundtrip(document_t* document, const Call* call, Mass displacement, Length trim) {
    const Length lcg =  document->stabilityForcer()->lcgFromDisplacementAndTrim(displacement, trim, call);
    const Density waterDensity = document->portWaterDensity()->waterDensity(call);
    return document->vessel()->trim(displacement, lcg, waterDensity);
}

void DraftSurveyTest::testInverseDraftAndTrimLookup() {
    document_t* document = FileOpener::openStoFile(QFINDTESTDATA("test-data/draft_survey_AL_MANAMAH_AEKLF_1416E.sto")).document;
    QCOMPARE(document->schedule()->calls().size(), 3);
    const Call* call = document->schedule()->calls().at(1);

    {
        // draft table goes from
        // 2.708e+07 kg 4.5 meter to
        // 1.23098e+08 kg 15.55 meter
        // choosing some arbitrary values and testing
        // draft -> displacement -> draft roundtrip lookup
        QVERIFY(compare(draftTableRoundtrip(document, call, 4.500*meter), 4.500*meter, 1.0*millimeter));
        QVERIFY(compare(draftTableRoundtrip(document, call, 4.501*meter), 4.501*meter, 1.0*millimeter));
        QVERIFY(compare(draftTableRoundtrip(document, call, 6.0123*meter), 6.0123*meter, 1.0*millimeter));
        QVERIFY(compare(draftTableRoundtrip(document, call, 9.485*meter), 9.485*meter, 1.0*millimeter));
        QVERIFY(compare(draftTableRoundtrip(document, call, 11.432*meter), 11.432*meter, 1.0*millimeter));
        QVERIFY(compare(draftTableRoundtrip(document, call, 14.345*meter), 14.345*meter, 1.0*millimeter));
        QVERIFY(compare(draftTableRoundtrip(document, call, 15.549*meter), 15.549*meter, 1.0*millimeter));
        QVERIFY(compare(draftTableRoundtrip(document, call, 15.550*meter), 15.550*meter, 1.0*millimeter));
    }

    {
        // trim table goes from
        // lcg -107.355 meter to lcg 98.062 meter and
        // 2.708e+07 kg to 1.23098e+08 kg and
        // trim 40.6698 meter to trim -57.133 meter - but then the ship sinks ..
        // choosing some arbitrary values and testing
        // trim -> lcg -> trim roundtrip lookup for fixed displacement
        QVERIFY(compare(trimTableRoundtrip(document, call, 60000*ton, 3.0*meter), 3.0*meter, 1.0*millimeter));
        QVERIFY(compare(trimTableRoundtrip(document, call, 60000*ton, 2.0*meter), 2.0*meter, 1.0*millimeter));
        QVERIFY(compare(trimTableRoundtrip(document, call, 60000*ton, 1.0*meter), 1.0*meter, 1.0*millimeter));
        QVERIFY(compare(trimTableRoundtrip(document, call, 60000*ton, 0.0*meter), 0.0*meter, 1.0*millimeter));
        QVERIFY(compare(trimTableRoundtrip(document, call, 60000*ton, -1.0*meter), -1.0*meter, 1.0*millimeter));
        QVERIFY(compare(trimTableRoundtrip(document, call, 60000*ton, -2.0*meter), -2.0*meter, 1.0*millimeter));
        QVERIFY(compare(trimTableRoundtrip(document, call, 60000*ton, -3.0*meter), -3.0*meter, 1.0*millimeter));

        QVERIFY(compare(trimTableRoundtrip(document, call, 120000*ton, 3.0*meter), 3.0*meter, 1.0*millimeter));
        QVERIFY(compare(trimTableRoundtrip(document, call, 120000*ton, 2.0*meter), 2.0*meter, 1.0*millimeter));
        QVERIFY(compare(trimTableRoundtrip(document, call, 120000*ton, 1.0*meter), 1.0*meter, 1.0*millimeter));
        QVERIFY(compare(trimTableRoundtrip(document, call, 120000*ton, 0.0*meter), 0.0*meter, 1.0*millimeter));
        QVERIFY(compare(trimTableRoundtrip(document, call, 120000*ton, -1.0*meter), -1.0*meter, 1.0*millimeter));
        QVERIFY(compare(trimTableRoundtrip(document, call, 120000*ton, -2.0*meter), -2.0*meter, 1.0*millimeter));
        QVERIFY(compare(trimTableRoundtrip(document, call, 120000*ton, -3.0*meter), -3.0*meter, 1.0*millimeter));
    }
}

void DraftSurveyTest::testForcedTrim() {
    document_t* document = FileOpener::openStoFile(QFINDTESTDATA("test-data/draft_survey_AL_MANAMAH_AEKLF_1416E.sto")).document;
    QCOMPARE(document->schedule()->calls().size(), 3);
    const Call* call = document->schedule()->calls().at(1);
    const ange::vessel::BaySlice* bayslice = document->vessel()->baySliceContaining(2);
    visibility_line_t* visibilityLine = document->visibility_line();

    // trim -0.219342 m from sto file content
    const Length trim = -0.219342*meter;
    QCOMPARE(document->stabilityForcer()->deadLoadIsActive(call), true);
    QCOMPARE(document->stabilityForcer()->forcedDraftTrimIsActive(call), false);
    QVERIFY(compare(document->stabilityForcer()->trim(call), trim, 1.0*centimeter));

    // visibility height 42.5251
    const Length visibilityHeight_42_5251 = 42.5251*meter;
    visibilityLine->update_cached_visibilityheights(call);
    QVERIFY(compare(visibilityLine->get_visibility_maxheight_coordinate(call, bayslice), visibilityHeight_42_5251, 1.0*centimeter));

    // trim -0.903 m forced
    const Length trimForced = -0.903*meter;
    document->stabilityForcer()->setForcedTrim(call, trimForced);
    QCOMPARE(document->stabilityForcer()->forcedDraftTrimIsActive(call), true);
    QVERIFY(compare(document->stabilityForcer()->trim(call), trimForced, 1.0*centimeter));
    QVERIFY(compare(document->documentInterface()->trimAndDraft(call).first, trimForced, 1.0*centimeter));

    // forced trim builds also forced deadload of weight 0*ton and unknown LCG set to inf, but it is not active in UI
    QCOMPARE(document->stabilityForcer()->deadLoadIsActive(call), false);
    QVERIFY(compare(document->stabilityForcer()->deadLoad(call).weight(), 0.0*ton, 1.0*kilogram));
    QVERIFY(qIsInf(document->stabilityForcer()->deadLoad(call).lcg() / meter)); // it would be better if this was nan!

    // forced trim and the trim computed from total displacement are expected equal
    ange::vessel::BlockWeight displacementBlockWeight = document->stowage()->stowageAsBlockWeight(call);
    QVERIFY(compare(document->vessel()->trim(displacementBlockWeight.weight(), displacementBlockWeight.lcg(), Vessel::oceanWaterDensity),
                    trimForced, 1.0*centimeter));

    // visibility height 42.8742 due to forced trim
    const Length visibilityHeightForced = 42.8742*meter;
    visibilityLine->update_cached_visibilityheights(call);
    QVERIFY(compare(visibilityLine->get_visibility_maxheight_coordinate(call, bayslice), visibilityHeightForced, 1.0*centimeter));

    // forced trim released, additional deadload is removed too
    document->stabilityForcer()->clearSurvey(call);
    QCOMPARE(document->stabilityForcer()->deadLoadIsActive(call), true);
    QVERIFY(compare(document->stabilityForcer()->deadLoad(call).weight(), 0.0*ton, 1.0*kilogram));
    QCOMPARE(document->stabilityForcer()->forcedDraftTrimIsActive(call), false);
    QVERIFY(compare(document->stabilityForcer()->trim(call), trim, 1.0*centimeter));
}

void DraftSurveyTest::testForcedDraft() {
    document_t* document = FileOpener::openStoFile(QFINDTESTDATA("test-data/draft_survey_AL_MANAMAH_AEKLF_1416E.sto")).document;
    QCOMPARE(document->schedule()->calls().size(), 3);
    const Call* call = document->schedule()->calls().at(1);
    const ange::vessel::BaySlice* bayslice = document->vessel()->baySliceContaining(2);
    visibility_line_t* visibilityLine = document->visibility_line();

    // draft 10.2279 m from sto file content
    const Length draft = 10.2279*meter;
    QCOMPARE(document->stabilityForcer()->deadLoadIsActive(call), true);
    QCOMPARE(document->stabilityForcer()->forcedDraftTrimIsActive(call), false);
    QVERIFY(compare(document->stabilityForcer()->draft(call), draft, 1.0*centimeter));

    // visibility height 42.5251
    const Length visibilityHeight = 42.5251*meter;
    visibilityLine->update_cached_visibilityheights(call);
    QVERIFY(compare(visibilityLine->get_visibility_maxheight_coordinate(call, bayslice), visibilityHeight, 1.0*centimeter));

    // draft 11.5 m forced
    const Length forcedDraft = 11.5*meter;
    document->stabilityForcer()->setForcedDraft(call, forcedDraft);
    QCOMPARE(document->stabilityForcer()->forcedDraftTrimIsActive(call), true);
    QVERIFY(compare(document->stabilityForcer()->draft(call), forcedDraft, 1.0*centimeter));

    // forced draft builds also forced deadload, but it is not active in UI
    QCOMPARE(document->stabilityForcer()->deadLoadIsActive(call), false);
    QVERIFY(compare(document->stabilityForcer()->deadLoad(call).weight(), 11248.9*ton, 1.0*ton));
    QVERIFY(compare(document->stabilityForcer()->deadLoad(call).lcg(), -7.39802*meter, 1.0*centimeter));

    // forced draft and the draft computed from total displacement are expected equal
    ange::vessel::BlockWeight displacementBlockWeight = document->stowage()->stowageAsBlockWeight(call);
    QVERIFY(compare(document->vessel()->draft(displacementBlockWeight.weight(), displacementBlockWeight.lcg(), Vessel::oceanWaterDensity),
                    forcedDraft, 1.0*centimeter));

    // visibility height 42.8187
    const Length forcedVisibilityHeight = 42.8187*meter;
    visibilityLine->update_cached_visibilityheights(call);
    QVERIFY(compare(visibilityLine->get_visibility_maxheight_coordinate(call, bayslice), forcedVisibilityHeight, 1.0*centimeter));

    // forced draft released, additional deadload is removed too
    document->stabilityForcer()->clearSurvey(call);
    QCOMPARE(document->stabilityForcer()->deadLoadIsActive(call), true);
    QVERIFY(compare(document->stabilityForcer()->deadLoad(call).weight(), 0.0*ton, 1.0*kilogram));
    QCOMPARE(document->stabilityForcer()->forcedDraftTrimIsActive(call), false);
    QVERIFY(compare(document->stabilityForcer()->draft(call), draft, 1.0*centimeter));
}

void DraftSurveyTest::testDeadLoad() {
    document_t* document = FileOpener::openStoFile(QFINDTESTDATA("test-data/draft_survey_AL_MANAMAH_AEKLF_1416E.sto")).document;
    QCOMPARE(document->schedule()->calls().size(), 3);
    const Call* call = document->schedule()->calls().at(1);

    // and no deadload adjustment yet
    {
        QCOMPARE(document->stabilityForcer()->deadLoadIsActive(call), true);
        QCOMPARE(document->stabilityForcer()->deadLoad(call).weight(), 0.0*ton);
        QVERIFY(qIsNaN(document->stabilityForcer()->deadLoad(call).lcg() / meter));

        // draft 10.2279 m and trim -0.219342 m from sto file content
        QCOMPARE(document->stabilityForcer()->forcedDraftTrimIsActive(call), false);
        QVERIFY(compare(document->stabilityForcer()->draft(call), 10.2279*meter, 1.0*centimeter));
        QCOMPARE(document->stabilityForcer()->forcedDraftTrimIsActive(call), false);
        QVERIFY(compare(document->stabilityForcer()->trim(call), -0.219342*meter, 1.0*centimeter));

        // weight, lcg, tcg, vcg from from sto file content
        // WEIGHT   72576.1
        // LCG -1.95073
        // TCG 0.00856998
        // VCG 16.9332
        const StabilityData* stabilityData = document->stability()->data(call);
        QVERIFY(compare(stabilityData->totalWeight, 72576.1*ton, 1.0*ton));
        QVERIFY(compare(stabilityData->totalLcg, -1.95073*meter, 1.0*centimeter));
        QVERIFY(compare(stabilityData->totalTcg, 0.00856998*meter, 1.0*centimeter));
        QVERIFY(compare(stabilityData->totalVcg, 16.9332*meter, 1.0*centimeter));
    }

    // deadload adjustment added - set fore, aft, vcg and lcg same as existing cargo block weight, and set new weight and lcg
    {
        BlockWeight cargoBlock = BlockWeight::totalBlockWeight(document->stowage()->bay_block_weights(call).values());
        BlockWeight deadLoad(50.5*meter , cargoBlock.aftLimit(), cargoBlock.foreLimit(),
                            cargoBlock.vcg(), cargoBlock.tcg(), 1000.0*ton);
        document->stabilityForcer()->setDeadLoad(call, deadLoad);

        QCOMPARE(document->stabilityForcer()->deadLoadIsActive(call), true);
        QVERIFY(compare(document->stabilityForcer()->deadLoad(call).weight(), 1000.0*ton, 1.0*kilogram));
        QVERIFY(compare(document->stabilityForcer()->deadLoad(call).lcg(), 50.5*meter, 1.0*centimeter));

        // draft must increase to 10.3432 m and trim must decrease to -0.655472 (as LCG 50.5 m is aft direction)
        QCOMPARE(document->stabilityForcer()->forcedDraftTrimIsActive(call), false);
        QVERIFY(compare(document->stabilityForcer()->draft(call), 10.3432*meter, 1.0*centimeter));
        QCOMPARE(document->stabilityForcer()->forcedDraftTrimIsActive(call), false);
        QVERIFY(compare(document->stabilityForcer()->trim(call), -0.655472*meter, 1.0*centimeter));

        // weight, lcg, tcg, vcg changed due to deadload adjustment
        // WEIGHT   73576.1  must increase with 1000 t
        // LCG -1.23786      must be more to the positve direction as deadload LCG is positive
        // TCG 0.0103777     must be increasing in same direction as TCG of cargo has been used for deadload
        // VCG 16.9837       must be increasing in same direction as VCG of cargo has been used for deadload
        const StabilityData* stabilityData = document->stability()->data(call);
        QVERIFY(compare(stabilityData->totalWeight, 73576.1*ton, 1.0*ton));
        QVERIFY(compare(stabilityData->totalLcg, -1.23786*meter, 1.0*centimeter));
        QVERIFY(compare(stabilityData->totalTcg, 0.0103777*meter, 1.0*centimeter));
        QVERIFY(compare(stabilityData->totalVcg, 16.9837*meter, 1.0*centimeter));
    }

    // delete added deadload again
    {
        document->stabilityForcer()->clearSurvey(call);
        QCOMPARE(document->stabilityForcer()->deadLoadIsActive(call), true);
        QCOMPARE(document->stabilityForcer()->deadLoad(call).weight(), 0.0*ton);
        QVERIFY(qIsNaN(document->stabilityForcer()->deadLoad(call).lcg() / meter));

        // draft 10.2279 m and trim -0.219342 m from sto file content
        QCOMPARE(document->stabilityForcer()->forcedDraftTrimIsActive(call), false);
        QVERIFY(compare(document->stabilityForcer()->draft(call), 10.2279*meter, 1.0*centimeter));
        QCOMPARE(document->stabilityForcer()->forcedDraftTrimIsActive(call), false);
        QVERIFY(compare(document->stabilityForcer()->trim(call), -0.219342*meter, 1.0*centimeter));

        // weight, lcg, tcg, vcg from from sto file content
        // WEIGHT   72576.1
        // LCG -1.95073
        // TCG 0.00856998
        // VCG 16.9332
        const StabilityData* stabilityData = document->stability()->data(call);
        QVERIFY(compare(stabilityData->totalWeight, 72576.1*ton, 1.0*ton));
        QVERIFY(compare(stabilityData->totalLcg, -1.95073*meter, 1.0*centimeter));
        QVERIFY(compare(stabilityData->totalTcg, 0.00856998*meter, 1.0*centimeter));
        QVERIFY(compare(stabilityData->totalVcg, 16.9332*meter, 1.0*centimeter));
    }
}

QTEST_GUILESS_MAIN(DraftSurveyTest);

#include "draftsurveytest.moc"
