#include <QTest>

#include <document/document.h>
#include <document/stowage/stability/stresses.h>
#include <stability.h>
#include <document/stowage/stowage.h>
#include <fileopener.h>

#include <ange/schedule/schedule.h>
#include <ange/vessel/vessel.h>

using namespace ange::units;
using namespace std;  // abs()
using ange::vessel::Vessel;


/**
 * Test of a stowage where the stress calculation is espesially sensitive.
 * See ticket #1630.
 */
class SensitiveStressesTest : public QObject {
    Q_OBJECT
private Q_SLOTS:
    void initTestCase();
    void testStressCurves();
    void testLcg();
    void testWeights();
    void testTrim();
    void testDraft();
    void testStability();
private:
    document_t* m_document;
    stowage_t* m_stowage;
    ange::schedule::Call* m_call;
};


void SensitiveStressesTest::initTestCase() {
    m_document = FileOpener::openStoFile(QFINDTESTDATA("test-data/sensitivestresses.sto")).document;
    m_stowage = m_document->stowage();
    m_call = m_document->schedule()->calls().at(1);
}

void SensitiveStressesTest::testStressCurves() {
    // These numbers were off by up to 200% in the problem seen in ticket #1630
    QVERIFY(abs(40.0 - m_document->stresses_bending()->maxBendingPct(m_call)) < 2.0);
    QVERIFY(abs(50.0 - m_document->stresses_bending()->maxShearPct(m_call)) < 2.0);
    QVERIFY(abs(20.0 - m_document->stresses_bending()->maxTorsionPct(m_call)) < 2.0);
}

void SensitiveStressesTest::testLcg() {
    QVERIFY(abs(98.52 - m_stowage->stowageAsBlockWeight(m_call).lcg() / meter) < 0.01);
}

void SensitiveStressesTest::testWeights() {
    QVERIFY(abs(37130.0 - m_stowage->stowageAsBlockWeight(m_call).weight() / ton) < 1.0);
}

void SensitiveStressesTest::testTrim() {
    ange::vessel::BlockWeight p = m_document->stowage()->stowageAsBlockWeight(m_call);
    Length trim = m_document->vessel()->trim(p.weight(), p.lcg(), Vessel::oceanWaterDensity);
    QVERIFY(abs(2.17 - trim / meter) < 0.01);
}

void SensitiveStressesTest::testDraft() {
    ange::vessel::BlockWeight p = m_document->stowage()->stowageAsBlockWeight(m_call);
    Length  draft = m_document->vessel()->draft(p.weight(), p.lcg(), Vessel::oceanWaterDensity);
    QVERIFY(abs(9.39 - draft / meter) < 0.01);
}

void SensitiveStressesTest::testStability() {
    ange::units::Length gm = m_document->stability()->data(m_call)->gm;
    QVERIFY(abs(1.25 - gm / meter) < 0.01);
}

QTEST_GUILESS_MAIN(SensitiveStressesTest);

#include "sensitivestressestest.moc"
