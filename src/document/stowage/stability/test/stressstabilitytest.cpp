#include <QTest>

#include <document/document.h>
#include <document/stowage/stability/stresses.h>
#include <stability.h>
#include <document/stowage/stowage.h>
#include <fileopener.h>

#include <ange/schedule/schedule.h>
#include <ange/vessel/vessel.h>

using namespace ange::units;
using ange::vessel::Vessel;


/**
 * Test intended to assure that the stress/stability calculations don't get
 * too messed up. Depending on the chosen calculation method, some of the values
 * can have quite significant deviations (e.g. 20%).
 * The idea here is to test if the values get far off (e.g. > 500%), and ranges have
 * been chosen from what made visually sense to the users at the time of writing.
 */
class StressStabilityTest : public QObject {
    Q_OBJECT
private Q_SLOTS:
    void initTestCase();
    void testStressCurves();
    void testLcg();
    void testWeights();
    void testTrim();
    void testDraft();
    void testStability();
private:
    document_t* m_document;
    stowage_t* m_stowage;
    ange::schedule::Call* m_call;
};


void StressStabilityTest::initTestCase() {
    m_document = FileOpener::openStoFile(QFINDTESTDATA("test-data/real-empty-vessel.sto")).document;
    m_stowage = m_document->stowage();
    m_call = m_document->schedule()->calls().at(0);
}

void StressStabilityTest::testStressCurves() {
    // stress curve span taken based on values that at the time of writing
    // visually made sense to the users. The idea is to test if the values get
    // far off e.g. > 500%
    QVERIFY(m_document->stresses_bending()->maxBendingPct(m_call) < 102.0);
    QVERIFY(m_document->stresses_bending()->maxBendingPct(m_call) > 92.0);
    QVERIFY(m_document->stresses_bending()->maxShearPct(m_call) < 100.0);
    QVERIFY(m_document->stresses_bending()->maxShearPct(m_call) > 80.0);
    QCOMPARE(m_document->stresses_bending()->maxTorsionPct(m_call), 0.0);
}

void StressStabilityTest::testLcg() {
    QVERIFY(m_stowage->stowageAsBlockWeight(m_call).lcg() / meter < -23.0);
    QVERIFY(m_stowage->stowageAsBlockWeight(m_call).lcg() / meter > -24.0);
}

void StressStabilityTest::testWeights() {
    QVERIFY(m_stowage->stowageAsBlockWeight(m_call).weight() / ton > 19000);
    QVERIFY(m_stowage->stowageAsBlockWeight(m_call).weight() / ton < 20000);
}

void StressStabilityTest::testTrim() {
    ange::vessel::BlockWeight p = m_document->stowage()->stowageAsBlockWeight(m_call);
    Length trim = m_document->vessel()->trim(p.weight(), p.lcg(), Vessel::oceanWaterDensity);
    QVERIFY(trim < 6.60 * meter);
    QVERIFY(trim > 6.50 * meter);
}

void StressStabilityTest::testDraft() {
    ange::vessel::BlockWeight p = m_document->stowage()->stowageAsBlockWeight(m_call);
    Length  draft = m_document->vessel()->draft(p.weight(), p.lcg(), Vessel::oceanWaterDensity);
    QVERIFY(draft < 4.80 * meter);
    QVERIFY(draft > 4.70 * meter);
}

void StressStabilityTest::testStability() {
    ange::units::Length gm = m_document->stability()->data(m_call)->gm;
    QVERIFY(gm < 5.5 * meter);
    QVERIFY(gm > 4.5 * meter);
}

QTEST_GUILESS_MAIN(StressStabilityTest);

#include "stressstabilitytest.moc"
