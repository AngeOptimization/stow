#include "stresses.h"

#include "stability.h"
#include "document/document.h"
#include "document/portwaterdensity/portwaterdensity.h"
#include "document/signalbufferer.h"
#include "document/stowage/stowage.h"
#include "document/stowage/stowagestack.h"
#include "document/tanks/tankconditions.h"
#include "document/userconfiguration/userconfiguration.h"

#include <ange/containers/container.h>
#include <ange/schedule/schedule.h>
#include <ange/schedule/call.h>
#include <ange/vessel/bayslice.h>
#include <ange/vessel/blockweight.h>
#include <ange/vessel/bilinearinterpolator.h>
#include <ange/vessel/point3d.h>
#include <ange/vessel/slot.h>
#include <ange/vessel/stack.h>
#include <ange/vessel/stacksupport.h>
#include <ange/vessel/vesseltank.h>
#include <ange/vessel/vessel.h>

#include <cmath>
#include <limits>

using namespace ange::containers;
using namespace ange::schedule;
using namespace ange::units;
using namespace ange::vessel;

static const Acceleration gravityConstant = 9.81 * meter / second2;

Stresses::Stresses(document_t* document)
  : QObject(document), m_document(0), m_bufferer(0)
{
    Q_ASSERT(document);
    setObjectName("Stresses");
    setDocument(document);
}

Stresses::~Stresses() {
    // Empty
}

void Stresses::setDocument(document_t* document) {
    Q_ASSERT(!m_document);
    Q_ASSERT(!m_bufferer);
    m_document = document;
    m_bufferer = new SignalBuffererWithCall(m_document->schedule(), this);
    connect(m_bufferer, &SignalBuffererWithCall::timeout, this, &Stresses::recalculateReal);
    connect(m_document->schedule(), &Schedule::callAdded, this, &Stresses::add_call);
    connect(m_document->schedule(), &Schedule::callAboutToBeRemoved, this, &Stresses::remove_call);
    connect(document->stability(), &Stability::stabilityChanged, this, &Stresses::recalculate);
    // Stability::stabilityChanged is triggered by:
    //   DraftSurvey::draftSurveyChanged that is triggered by:
    //     stowage_t::stowageChangedAtCall
    //     TankConditions::tankConditionChanged
    //     PortWaterDensity::densityChanged
    //     document_t::vessel_changed
    //     Schedule::callAdded
    connect(document, &document_t::vessel_changed, this, &Stresses::reset_vessel_data);
    reset_vessel_data();
}

void Stresses::reset_vessel_data() {
  // we want to get the vessel length and data resolution from the bonjean data.
  m_lightship_weight_slices = m_document->vessel()->lightshipWeights();
  const ange::vessel::BilinearInterpolator& max_bending_limits_data = m_document->vessel()->maxBendingLimitsData();
  //FIXME: move this to Vessel
  BlockWeight vessel_length_description = BlockWeight::totalBlockWeight(m_document->vessel()->lightshipWeights()); //this is overkill, just get aft and fwd LPP in there
  BlockWeight light_ship_as_block_weight = BlockWeight(m_document->vessel()->hullLcg(),
                                                       vessel_length_description.aftLimit() - 0.01*centimeter, // Silence warning about "Longitudinal extent of 0.0m"
                                                       vessel_length_description.foreLimit() + 0.01*centimeter,
                                                       m_document->vessel()->hullVcg(),
                                                       0.0*meter,
                                                       m_document->vessel()->lightshipWeight());
  if (m_document->vessel()->hasInfo(Vessel::HullShape)) {
    // We also want to make sure that the hullweight data matches the given hullweight information.
    Length lcg_max = - std::numeric_limits< double >::infinity()*meter;
    Length lcg_min = std::numeric_limits< double >::infinity()*meter;
    m_stationlistStresses = max_bending_limits_data.xCoordinates();
    Q_FOREACH (ange::vessel::BlockWeight block, m_lightship_weight_slices) {
      if(block.aftLimit() <= lcg_min) {
        lcg_min = block.aftLimit();
      }
      if(block.foreLimit() >= lcg_max) {
        lcg_max = block.foreLimit();
      }
    }
    m_lcg_max = lcg_max;
    m_lcg_min = lcg_min;
    m_stationlistBonjean = m_document->vessel()->bonjeanCurves().xCoordinates();

    //FIXME: move this to Vessel
    m_lightship_weight_slices = adjust_block_data(m_lightship_weight_slices, light_ship_as_block_weight);

    Q_FOREACH(const ange::schedule::Call* call , m_document->schedule()->calls()) {
      m_bendingcurves_dirty[call] = true;
      m_shearcurves_dirty[call] = true;
      m_torsioncurves_dirty[call] = true;
      m_buoyancy_dirty[call] = true;
      m_tanks_dirty[call] = true;
    }
  } else {
    m_lightship_weight_slices.clear();
    m_lightship_weight_slices << light_ship_as_block_weight;
  }
}

void Stresses::add_call(ange::schedule::Call* call) {
  Q_ASSERT(call);
  Q_ASSERT(m_document);
  if (!call || !m_document) {
    return;
  }
  m_bendingcurves_dirty[call] = true;
  m_shearcurves_dirty[call] = true;
  m_torsioncurves_dirty[call] = true;
  m_buoyancy_dirty[call] = true;
  m_tanks_dirty[call] = true;
  emit stresses_changed(call);
}

void Stresses::remove_call(ange::schedule::Call* call) {
  m_bending_curves.remove(call);
  m_shear_curves.remove(call);
  m_torsion_curves.remove(call);
  m_bendingcurves_dirty.remove(call);
  m_shearcurves_dirty.remove(call);
  m_torsioncurves_dirty.remove(call);
  m_buoyancy_dirty.remove(call);
  m_buoyancy.remove(call);
  m_tanks_stresses.remove(call);
  m_ballastTanksWeights.remove(call);
  m_tanks_dirty.remove(call);
}

void Stresses::recalculateReal(const ange::schedule::Call* call) {
  emit stresses_changed(call);
}

void Stresses::recalculate(const ange::schedule::Call* call) {
  m_bendingcurves_dirty[call] = true;
  m_shearcurves_dirty[call] = true;
  m_torsioncurves_dirty[call] = true;
  m_buoyancy_dirty[call] = true;
  m_tanks_dirty[call] = true;
  m_bufferer->startBuffer(call);
}

QList<double> Stresses::get_stresscurve_measurepoints() const {
  QList<double> measurepoints;
  measurepoints = m_stationlistStresses;
  if (measurepoints.first() > m_lcg_min/meter) {
    measurepoints.push_front(m_lcg_min/meter);
  }
  if (measurepoints.back() < m_lcg_max/meter) {
    measurepoints.push_back(m_lcg_max/meter);
  }
  return measurepoints;
}

void Stresses::calculate_bending_curve(const Call* call) const {
  QVector< QPointF> rv;
  if (m_document->vessel()->hasInfo(Vessel::HullShape)) {
    QList<double> measurepoints = get_stresscurve_measurepoints();  // FIXME should be ange::units::Length
    calculate_buoyancy_data(call);
    calculate_tanks_stresses(call);
    Q_FOREACH(double current_lcg, measurepoints) {
      // I have split the torque in a left and a right component for debugging purpose.
      // It is important that the left and the right contribution is always equal, otherwise the vessel is not in equilibrium.
      Torque current_torque_right;
      Torque current_torque_left;
      // First we include the contribution from the submerged water.
      Q_FOREACH(ange::vessel::BlockWeight buoyancy_slice, m_buoyancy[call]) { //FIXME: don't duplicate all this code
        current_torque_left += - gravityConstant * buoyancy_slice.longitudinalMomentAftOf(current_lcg*meter);
        current_torque_right += - gravityConstant * buoyancy_slice.longitudinalMomentForeOf(current_lcg*meter);
      }
      // We then include the contribution from the hull
      Q_FOREACH(const ange::vessel::BlockWeight hull_weight_slice, m_lightship_weight_slices) {
        current_torque_left +=  gravityConstant * hull_weight_slice.longitudinalMomentAftOf(current_lcg*meter);
        current_torque_right +=  gravityConstant * hull_weight_slice.longitudinalMomentForeOf(current_lcg*meter);
      }
      // We then include the contribution from constant weights
      Q_FOREACH(ange::vessel::BlockWeight constant_weight_slice, m_document->vessel()->constantWeights()) {
        current_torque_left +=  gravityConstant * constant_weight_slice.longitudinalMomentAftOf(current_lcg*meter);
        current_torque_right +=  gravityConstant * constant_weight_slice.longitudinalMomentForeOf(current_lcg*meter);
      }
      // Then we include the contribution from the containers
      Q_FOREACH(const BlockWeight bay_slice_cargo, m_document->stowage()->bay_block_weights(call)) {
        current_torque_left+= gravityConstant * (bay_slice_cargo.longitudinalMomentAftOf(current_lcg*meter));
        current_torque_right+= gravityConstant * (bay_slice_cargo.longitudinalMomentForeOf(current_lcg*meter));
      }
      // We also have to include the torque from the tanks.
      Q_FOREACH(ange::vessel::BlockWeight slice, m_tanks_stresses[call]){
        current_torque_left +=  gravityConstant * slice.longitudinalMomentAftOf(current_lcg*meter);
        current_torque_right +=  gravityConstant * slice.longitudinalMomentForeOf(current_lcg*meter);
      }
      //The calculations of the torque at the extremities of the vessel are highly sensitive as the
      //arm in the torque gets large. In consequence we choose the torque calculation based on the
      //the shortest arm
      Torque current_torque = current_lcg*meter < m_lcg_min + .5*(m_lcg_max - m_lcg_min)?
                                                   current_torque_left : current_torque_right;
      rv << QPointF(current_lcg, current_torque/newtonmeter);
    }
  }
  for(int i = 0; i < rv.size(); ++i) {
    double limit = m_document->vessel()->maxBendingLimit(rv.at(i).x()*meter);
    if(qFuzzyIsNull(limit)){
      limit = 1.0;
    }
    rv[i].setY(rv.at(i).y() / limit * 100);
  }
  m_bending_curves[call] = rv;
  m_max_bending_pct[call] = calculate_max_stress_pct(rv);;
  m_bendingcurves_dirty[call] = false;
}

QVector<QPointF> Stresses::relativeBendingCurve(const Call* call) const {
  if (m_document->vessel()->hasInfo(Vessel::HullShape)) {
    if (m_bendingcurves_dirty[call] || m_bending_curves[call].size() == 0) {
      calculate_bending_curve(call);
    }
    return m_bending_curves[call];
  } else {
    return QVector< QPointF >();
  }
}

void Stresses::calculate_shear_curve(const Call* call) const {
  QVector< QPointF> rv;
  m_shear_curves.remove(call);
  if (m_document->vessel()->hasInfo(Vessel::HullShape)) {
    QList<double> measurepoints = get_stresscurve_measurepoints();  // FIXME should be ange::units::Length
    calculate_buoyancy_data(call);
    calculate_tanks_stresses(call);
    Q_FOREACH(double current_lcg, measurepoints) {
      Force left_force;
      Force right_force;
      // First we include the contribution from the submerged water.
      Q_FOREACH(ange::vessel::BlockWeight bs, m_buoyancy[call]) {
        left_force += - gravityConstant*bs.weightAftOf(current_lcg*meter);
        right_force += - gravityConstant *bs.weightForeOf(current_lcg*meter);
      }
      // We then include the contribution from the hull
      Q_FOREACH(ange::vessel::BlockWeight hs, m_lightship_weight_slices) {
        left_force += gravityConstant * hs.weightAftOf(current_lcg*meter);
        right_force += gravityConstant * hs.weightForeOf(current_lcg*meter);
      }
      //We then include the contribution from the constant weights
      Q_FOREACH(ange::vessel::BlockWeight cs, m_document->vessel()->constantWeights()) {
        left_force += gravityConstant * cs.weightAftOf(current_lcg*meter);
        right_force += gravityConstant * cs.weightForeOf(current_lcg*meter);
      }
      // Then we include the contribution from the containers
      Q_FOREACH(const BlockWeight bsm, m_document->stowage()->bay_block_weights(call)) {
        left_force += gravityConstant * (bsm.weightAftOf(current_lcg*meter));
        right_force += gravityConstant * (bsm.weightForeOf(current_lcg*meter));
      }
      // We also have to include the weight from the tanks.
      Q_FOREACH(ange::vessel::BlockWeight slice, m_tanks_stresses[call]){
        left_force +=  gravityConstant * slice.weightAftOf(current_lcg*meter);
        right_force +=  gravityConstant * slice.weightForeOf(current_lcg*meter);
      }
      rv << QPointF(current_lcg, left_force/newton);
    }
  }
  for(int i = 0; i < rv.size(); ++i) {
    double limit;
    if(rv.at(i).y()>=0) {
        limit = m_document->vessel()->maxShearForceLimit(rv.at(i).x()*meter);
    } else {
        limit = - m_document->vessel()->minShearForceLimit(rv.at(i).x()*meter);
    }
    if(qFuzzyIsNull(limit)){
      limit = 1.0;
    }
    rv[i].setY(rv.at(i).y() / limit * 100);
  }
  m_shear_curves[call] = rv;
  m_shearcurves_dirty[call] = false;
  m_max_shearforce_pct[call] = calculate_max_stress_pct(rv);;
}

void Stresses::calculate_tanks_stresses(const Call* call) const {
    if(m_tanks_dirty[call]){
        m_tanks_stresses[call].clear();
        m_ballastTanksWeights[call].clear();
        Q_FOREACH(const ange::vessel::VesselTank* tank , m_document->vessel()->tanks()) {
            const Mass tank_condition = m_document->tankConditions()->tankCondition(call, tank);
            BlockWeight tankSlice = BlockWeight(tank->lcg(tank_condition)*meter, tank->aftEnd()*meter, tank->foreEnd()*meter, tank->vcg(tank_condition)*meter, tank->tcg(tank_condition)*meter, tank_condition);
            m_tanks_stresses[call].append(tankSlice);
            if(tank->tankGroup() == m_document->vessel()->ballastTankGroup()) {
                m_ballastTanksWeights[call].append(tankSlice);
            }
        }
    }
    m_tanks_dirty[call] = false;
}

QVector<QPointF> Stresses::relativeShearForceCurve(const Call* call) const {
  if (m_document->vessel()->hasInfo(Vessel::HullShape)) {
    if (m_shearcurves_dirty[call] || m_shear_curves[call].size() == 0) {
      calculate_shear_curve(call);
    }
    return m_shear_curves[call];
  } else {
    return QVector< QPointF >();
  }
}

void Stresses::calculate_torsion_curve(const Call* call) const {
  QVector< QPointF> rv;
  m_torsion_curves.remove(call);
  calculate_tanks_stresses(call);
  if (m_document->vessel()->hasInfo(Vessel::HullShape)) {
    QList<double> measurepoints = get_stresscurve_measurepoints();
    Q_FOREACH(double current_lcg, measurepoints) {
      Torque left_transverseCMtorque;
      Torque right_transverseCMtorque;
      // We do not include the contribution from the submerged water as we believe this transverse torsional moment is always zero.
      // We now include the contribution from the hull
      Q_FOREACH(ange::vessel::BlockWeight hs, m_lightship_weight_slices) {
        if(!std::isnan(hs.tcg()/meter)) {
          left_transverseCMtorque += gravityConstant * hs.weightAftOf(current_lcg*meter) * hs.tcg();
          right_transverseCMtorque += gravityConstant * hs.weightForeOf(current_lcg*meter) * hs.tcg();
        }
      }
      // We then include the contribution from the constant weights
      Q_FOREACH(ange::vessel::BlockWeight cs, m_document->vessel()->constantWeights()) {
        if(!std::isnan(cs.tcg()/meter)) {
          left_transverseCMtorque += gravityConstant * cs.weightAftOf(current_lcg*meter) * cs.tcg();
          right_transverseCMtorque += gravityConstant * cs.weightForeOf(current_lcg*meter) * cs.tcg();
        }
      }
      // Then we include the contribution from the containers
      Q_FOREACH(const BlockWeight bsm, m_document->stowage()->bay_block_weights(call)) {
        left_transverseCMtorque += gravityConstant * (bsm.weightAftOf(current_lcg*meter)) * bsm.tcg();
        right_transverseCMtorque += gravityConstant * (bsm.weightForeOf(current_lcg*meter)) * bsm.tcg();
      }
      // We also have to include the contribution from the tanks.
        Q_FOREACH(const BlockWeight tank, m_tanks_stresses[call]){
        if (tank.weight() > 0.0*ton) {
          left_transverseCMtorque += gravityConstant * tank.weightAftOf(current_lcg*meter) * tank.tcg();
          right_transverseCMtorque += gravityConstant * tank.weightForeOf(current_lcg*meter) * tank.tcg();
        }
      }
      // We then have to adjust the torsional moment, because it might not be balanced. (Assumption is that the vessel lies still in the water and with transverse trim 0.)
      Torque left_torque = left_transverseCMtorque - (left_transverseCMtorque + right_transverseCMtorque) / (measurepoints.last() - measurepoints.first()) * (current_lcg - measurepoints.first());
      rv << QPointF(current_lcg, left_torque / newtonmeter);
    }
  }
  for(int i = 0; i < rv.size(); ++i) {
      double limit = m_document->vessel()->maxTorsionLimit(rv.at(i).x()*meter);
      if(qFuzzyIsNull(limit)){
        limit = 1.0;
      }
      rv[i].setY(rv.at(i).y() / limit * 100);
  }
  m_torsion_curves[call] = rv;
  m_torsioncurves_dirty[call] = false;
  m_max_torsion_pct[call] = calculate_max_stress_pct(rv);;
}

QVector<QPointF> Stresses::relativeTorsionCurve(const Call* call) const {
  if (m_document->vessel()->hasInfo(Vessel::HullShape)) {
    if (m_torsioncurves_dirty[call] || m_torsion_curves[call].size() == 0) {
      calculate_torsion_curve(call);
    }
    return m_torsion_curves[call];
  } else {
    return QVector< QPointF >();
  }
}

void Stresses::calculate_buoyancy_data(const Call* call) const {
  if(m_buoyancy_dirty[call]){
    // this is the data we trust, and hence adjust the bending data lookup to these.
    Length draft = m_document->stability()->data(call)->draft;
    Length trim = m_document->stability()->data(call)->trim;
    Length vessel_length = m_lcg_max - m_lcg_min;
    Length midt_ship = 0.5 * (m_lcg_max + m_lcg_min);
    double a = - trim / vessel_length;
    Length b = - a * midt_ship;
    Density waterDensity = m_document->portWaterDensity()->waterDensity(call);
    m_buoyancy[call].clear();
    for (int i = 0; i < m_stationlistBonjean.size() - 1; ++i) {
      Length start_x = m_stationlistBonjean[i]*meter;
      Length end_x = m_stationlistBonjean[i+1]*meter;
      Length start_localdraft = draft + a * start_x + b;
      Length end_localdraft = draft + a * end_x + b;
      double start_y = m_document->vessel()->bonjean(start_x, start_localdraft) * waterDensity / (kilogram/meter); //FIXME:should be longitudinal_density_t
      double end_y = m_document->vessel()->bonjean(end_x, end_localdraft) * waterDensity / (kilogram/meter); // FIXME: correct units, ticket #1088
      m_buoyancy[call].insert(i, ange::vessel::BlockWeight(start_x, start_y*kilogram_per_meter, end_x, end_y*kilogram_per_meter));
    }
    BlockWeight total = BlockWeight::totalBlockWeight(m_document->vessel()->constantWeights() + lightshipWeights()
    + m_document->stowage()->bay_block_weights(call).values() + tankWeights(call));
    m_buoyancy[call] = adjust_block_data(m_buoyancy[call], total);
    m_buoyancy_dirty[call] = false;
  }
}

QList< BlockWeight > Stresses::adjust_block_data(QList< BlockWeight > block_weights, const BlockWeight& target) {
    BlockWeight actual_total = BlockWeight::totalBlockWeight(block_weights);
    double weight_ratio = target.weight()/actual_total.weight();
    QList<ange::vessel::BlockWeight> new_block_weights;
    Q_FOREACH(BlockWeight block_weight, block_weights) {
        BlockWeight corrected_block_weight = block_weight;
        corrected_block_weight.setWeight(block_weight.weight()*weight_ratio);
        new_block_weights.append(corrected_block_weight);
    }
    BlockWeight new_total = BlockWeight::totalBlockWeight(new_block_weights);
    BlockWeight correction = target;
    correction.extendWith(new_total.oppositeWeight());
    new_block_weights.append(correction);
    return new_block_weights;
}

double Stresses::maxBendingPct(const Call* call) const {
  double rv = 0.0;
  if (m_document->vessel()->hasInfo(Vessel::HullShape)) {
    if (m_bendingcurves_dirty[call]) {
      calculate_bending_curve(call);
    }
    rv = m_max_bending_pct.value(call);
  }
  return rv;
}

qreal Stresses::calculate_max_stress_pct(const QVector<QPointF>& stress_curve) const {
    qreal rv = 0.0;
    Q_FOREACH(QPointF p, stress_curve){
        if(qAbs(p.y()) > rv){
            rv = qAbs(p.y());
        }
    }
    return rv;
}

double Stresses::maxShearPct(const Call* call) const {
  double rv = 0.0;
  if (m_document->vessel()->hasInfo(Vessel::HullShape)) {
    if (m_shearcurves_dirty[call]) {
      calculate_shear_curve(call);
    }
    rv = m_max_shearforce_pct.value(call);
  }
  return rv;
}

double Stresses::maxTorsionPct(const Call* call) const {
  double rv = 0.0;
  if (m_document->vessel()->hasInfo(Vessel::HullShape)) {
    if (m_torsioncurves_dirty[call]) {
      calculate_torsion_curve(call);
    }
    rv = m_max_torsion_pct.value(call);
  }
  return rv;
}

const QList<ange::vessel::BlockWeight>& Stresses::buoyancyBlockWeights(const Call* call) const {
  if(m_buoyancy_dirty[call]){
    calculate_buoyancy_data(call);
  }
  return m_buoyancy[call];
}

const QList<BlockWeight>& Stresses::tankWeights(const Call* call) const {
  if(m_tanks_dirty[call]){
    calculate_tanks_stresses(call);
  }
  return m_tanks_stresses[call];
}

const QList<BlockWeight>& Stresses::ballastTankWeights(const Call* call) const {
  if(m_tanks_dirty[call]){
    calculate_tanks_stresses(call);
  }
  return m_ballastTanksWeights[call];
}


const QList<ange::vessel::BlockWeight>& Stresses::lightshipWeights() const {
  return m_lightship_weight_slices;
}

#include "stresses.moc"
