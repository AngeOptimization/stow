#ifndef STABILITY_H
#define STABILITY_H

#include <ange/units/units.h>
#include <ange/vessel/blockweight.h>
#include <QHash>

class document_t;
namespace ange {
namespace schedule {
class Call;
}
namespace vessel {
class VesselTank;
}
}

struct TankData {
    /**
     * The tank in the vessel model
     */
    const ange::vessel::VesselTank* tank;
    /**
     * The free surface moment contribution from the tank
     */
    ange::units::MassMoment freeSurfaceMoment;
};

/**
 * @brief All the calculated stability data.
 *
 * If some data is impossible to calculate because of bad vessel data it will be NaN.
 */
class StabilityData {

public:
    StabilityData();

public:
    /**
     * Stability contributions for the individual tanks
     */
    QList<TankData> tanksData;
    /**
     * Total mass of vessel including all (lightship + deadweight = displacement)
     */
    ange::units::Mass totalWeight;
    /**
     * Longitudinal moment of total mass
     */
    ange::units::MassMoment totalLmom;
    /**
     * Longitudinal center of gravity for the total mass
     */
    ange::units::Length totalLcg;
    /**
     * Height of center of gravity for the total mass, alco called KG
     */
    ange::units::Length totalVcg;
    /**
     * Transverse center of gravity for the total mass
     */
    ange::units::Length totalTcg;
    /**
     * The draft
     */
    ange::units::Length draft;
    /**
     * The trim
     */
    ange::units::Length trim;
    /**
     * Free surface correction to gm from tanks
     */
    ange::units::Length freeSurfaceCorrection;
    /**
     * Height of metacenter
     */
    ange::units::Length km;
    /**
     * Distance from metacenter to KG correct for free surface
     */
    ange::units::Length gm;
    /**
     * List in radians, positive means starboard. List is how much the vessel leans to the side.
     */
    qreal list;
    /**
     * Weight of ballast
     */
    ange::units::Mass ballastWeight;
    /**
     * Weight of cargo
     */
    ange::units::Mass cargoWeight;

private:
    bool dirty;

    friend class Stability;
};

/**
 * A class handling the calculation of all the stability data on the vessel
 */
class Stability : public QObject {

    Q_OBJECT

public:

    explicit Stability(document_t* document);

    virtual ~Stability();

    /**
     * Get the stability data for the call
     */
    const StabilityData* data(const ange::schedule::Call* call) const;

Q_SIGNALS:

    void stabilityChanged(const ange::schedule::Call* call);

private Q_SLOTS:

    /**
     * Recalculates the stability data for the given call. Caching needed for acceptable
     * performance on larger instances
     */
    void changeStability(const ange::schedule::Call* call);

    /**
     * Remove stability data for a call that has been deleted
     */
    void removeCall(const ange::schedule::Call* call);

private:

    /**
     * In place update of the stability data
     */
    void updateStabilityData(StabilityData* data, const ange::schedule::Call* call) const;

private:
    const document_t* m_document;
    mutable QHash<const ange::schedule::Call*, StabilityData> m_stabilityData;

};

#endif // STABILITY_H
