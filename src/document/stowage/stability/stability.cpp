#include "stability.h"

#include "document/document.h"
#include "document/stowage/stowage.h"
#include "document/tanks/tankconditions.h"
#include "document/portwaterdensity/portwaterdensity.h"
#include "stabilityforcer.h"

#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>
#include <ange/vessel/blockweight.h>
#include <ange/vessel/vessel.h>
#include <ange/vessel/vesseltank.h>

using namespace ange::units;
using ange::schedule::Call;
using ange::schedule::Schedule;
using ange::vessel::BlockWeight;
using ange::vessel::Vessel;
using ange::vessel::VesselTank;

StabilityData::StabilityData()
  : dirty(true)
{
    // Empty
}

Stability::Stability(document_t* document)
  : QObject(document), m_document(document)
{
    Q_ASSERT(m_document);
    setObjectName("Stability");
    connect(m_document->stabilityForcer(), &StabilityForcer::stabilityForcerChanged, this, &Stability::changeStability);
    // DraftSurvey::draftSurveyChanged is triggered by:
    //   stowage_t::stowageChangedAtCall
    //   TankConditions::tankConditionChanged
    //   PortWaterDensity::densityChanged
    //   document_t::vessel_changed
    //   Schedule::callAdded
    connect(m_document->schedule(), &Schedule::callRemoved, this, &Stability::removeCall);
}

Stability::~Stability() {
    // Empty
}

const StabilityData* Stability::data(const Call* call) const {
    StabilityData& stabilityData = m_stabilityData[call];
    if (stabilityData.dirty) {
        updateStabilityData(&stabilityData, call);
        stabilityData.dirty = false;
    }
    return &stabilityData;
}

void Stability::changeStability(const Call* call) {
    m_stabilityData[call].dirty = true;
    emit stabilityChanged(call);
}

void Stability::removeCall(const Call* call) {
    m_stabilityData.remove(call);
}

void Stability::updateStabilityData(StabilityData* data, const Call* call) const {
    if (m_document->vessel()->hasInfo(Vessel::Hydrostatics)) {
        BlockWeight totalWeight = m_document->stowage()->stowageAsBlockWeight(call);
        data->totalWeight = totalWeight.weight();
        data->totalLmom = totalWeight.longitudinalMoment();
        data->totalLcg = totalWeight.lcg();
        data->totalTcg = totalWeight.tcg();
        data->totalVcg = totalWeight.vcg();
        data->draft = m_document->stabilityForcer()->draft(call);
        data->trim = m_document->stabilityForcer()->trim(call);
        data->ballastWeight = 0 * kilogram;
        data->tanksData.clear();
        MassMoment totalFreeSurfaceMoment = 0.0 * kilogrammeter;
        Q_FOREACH (const VesselTank* tank, m_document->vessel()->tanks()) {
            TankData tankData;
            tankData.tank = tank;
            Mass tankCondition = m_document->tankConditions()->tankCondition(call, tank);
            tankData.freeSurfaceMoment = tank->fsm(tankCondition) * tank->density() * kilogrammeter;
            data->tanksData << tankData;
            totalFreeSurfaceMoment += tankData.freeSurfaceMoment;
            if (tank->isBallast()) {
                data->ballastWeight += tankCondition;
            }
        }
        data->freeSurfaceCorrection = totalFreeSurfaceMoment / data->totalWeight;
        data->km = m_document->vessel()->metacenter(data->trim, data->draft);
        data->gm = m_document->stabilityForcer()->gm(call, data->km - data->totalVcg - data->freeSurfaceCorrection);
        data->list = atan2(data->totalTcg/meter, data->gm/meter);
        data->cargoWeight = 0 * kilogram;
        Q_FOREACH (const BlockWeight& bayWeight, m_document->stowage()->bay_block_weights(call)) {
            data->cargoWeight += bayWeight.weight();
        }
    } else {
        double nan = std::numeric_limits<double>::quiet_NaN();
        data->totalWeight= nan * kilogram;
        data->totalLmom = nan * kilogrammeter;
        data->totalLcg = nan * meter;
        data->totalVcg = nan * meter;
        data->totalTcg = nan * meter;
        data->draft = nan * meter;
        data->trim = nan * meter;
        data->tanksData.clear();
        data->ballastWeight = nan * kilogram;
        data->freeSurfaceCorrection = nan * meter;
        data->km = nan * meter;
        data->gm = nan * meter;
        data->list = nan;
        data->cargoWeight = nan * kilogram;
    }
}

#include "stability.moc"
