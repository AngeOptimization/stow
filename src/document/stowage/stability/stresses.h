#ifndef STRESSES_H
#define STRESSES_H

#include <stowplugininterface/stowtype.h>
#include <ange/units/units.h>
#include <ange/vessel/point3d.h>

#include <QObject>
#include <QPair>
#include <QVector>

class SignalBuffererWithCall;
class QPointF;
class document_t;
namespace ange {
namespace schedule {
class Call;
}
namespace containers {
class Container;
}
namespace vessel {
class Slot;
class VesselTank;
class BlockWeight;
class BaySlice;
}
}

/**
 * Class handling the calculation of the stresses of the vessel
 */
class Stresses: public QObject {

    Q_OBJECT

public:

    Stresses(document_t* document);

    virtual ~Stresses();

    /**
     *@return the calculated bending curve for the vessel for the given call. Units are SI with meters (m) in the first
     * coordinate and torque (Nm) in the second.
     */
    QVector<QPointF> relativeBendingCurve(const ange::schedule::Call* call) const;
    /**
     *@return the calculated shear force curve for the vessel for the given call. Units are SI with meters (m) in
     * first coordinate and Newton (N) in the second
     */
    QVector<QPointF> relativeShearForceCurve(const ange::schedule::Call* call) const;

    /**
     *@return the calculated torsion force curve for the vessel for the given call. Units are SI with meters (m) in
     * first coordinate and torque (Nm) in the second
     */
    QVector<QPointF> relativeTorsionCurve(const ange::schedule::Call* call) const;

    ange::units::Length rearPerpendicular() const {
      return m_lcg_min;
    }
    ange::units::Length forePerpendicular() const {
      return m_lcg_max;
    }

    double maxBendingPct(const ange::schedule::Call* call) const;

    double maxShearPct(const ange::schedule::Call* call) const;

    double maxTorsionPct(const ange::schedule::Call* call) const;

    /**
    * @returns a map of stations and buoyancy slice stresses for a specific call
    */
    const QList<ange::vessel::BlockWeight>& buoyancyBlockWeights(const ange::schedule::Call* call) const;

    /**
    * @return the vessel's lightship weights, adjusted to fit with total lightship weight and lcg
    */
    const QList< ange::vessel::BlockWeight >& lightshipWeights() const;

    /**
    * @return slice_stress information of all tanks for a specific call
    */
    const QList<ange::vessel::BlockWeight>& tankWeights(const ange::schedule::Call* call) const;

    /**
     * \return blockweights of all tanks in the ballast tanks tank group for a specific call
     */
    const QList<ange::vessel::BlockWeight>& ballastTankWeights(const ange::schedule::Call* call) const;

Q_SIGNALS:

    /**
     * The bending curve changed in a given port.
     * Note that it might make sense to do some buffering in the slots.
     */
    void stresses_changed(const ange::schedule::Call* call);


public Q_SLOTS:

    /**
     * Delete call from calculation. Note that it will be resurrected with wrong data if requested.
     */
    void remove_call(ange::schedule::Call* call);

    /**
     * Add call to calculation. initially, values will be copied from the following call
     *  --- that is, it is assumed that this call initially have no loads or discharges
     */
    void add_call(ange::schedule::Call* call);

    /**
     * If the state of the tanks has changed it is necessary to recalculate the stresses and stability.
     */
    void recalculate(const ange::schedule::Call* call);

private Q_SLOTS:

    void recalculateReal(const ange::schedule::Call* call);

private:

    void setDocument(document_t* document);
    /**
     * adjust the @param block_weights such that their total lcg and mass match
     * @param lcg and @param mass respectively
     */
    static QList< ange::vessel::BlockWeight> adjust_block_data(QList< ange::vessel::BlockWeight > block_weights, const ange::vessel::BlockWeight& target);
    void reset_vessel_data();

    void calculate_bending_curve(const ange::schedule::Call* call) const;
    void calculate_shear_curve(const ange::schedule::Call* call) const;
    void calculate_torsion_curve(const ange::schedule::Call* call) const;
    qreal calculate_max_stress_pct(const QVector<QPointF>& stress_curve) const;
    /**
    * generates the buoyancy slice stresses for all stations in a specific call
    */
    void calculate_buoyancy_data(const ange::schedule::Call* call) const;
    /**
    * calculates tanks stresses for a specific call
    */
    void calculate_tanks_stresses(const ange::schedule::Call* call) const;
    QList<double> get_stresscurve_measurepoints() const;

private:

    document_t* m_document;
    ange::units::Length m_lcg_min;
    ange::units::Length m_lcg_max;
    QList<ange::vessel::BlockWeight >m_lightship_weight_slices;
    mutable QHash<const ange::schedule::Call*, double> m_max_bending_pct;
    mutable QHash<const ange::schedule::Call*, double> m_max_shearforce_pct;
    mutable QHash<const ange::schedule::Call*, double> m_max_torsion_pct;
    mutable QHash<const ange::schedule::Call*, QVector<QPointF> > m_bending_curves;
    mutable QHash<const ange::schedule::Call*, QVector<QPointF> > m_shear_curves;
    mutable QHash<const ange::schedule::Call*, QVector<QPointF> > m_torsion_curves;
    mutable QHash<const ange::schedule::Call*, bool> m_bendingcurves_dirty;
    mutable QHash<const ange::schedule::Call*, bool> m_shearcurves_dirty;
    mutable QHash<const ange::schedule::Call*, bool> m_torsioncurves_dirty;
    mutable QHash<const ange::schedule::Call*, bool> m_buoyancy_dirty;
    mutable QHash<const ange::schedule::Call*, bool> m_tanks_dirty;
    QList<double> m_stationlistStresses;  // FIXME should be ange::units::Length
    QList<double> m_stationlistBonjean;  // FIXME should be ange::units::Length
    typedef QHash<const ange::schedule::Call*, QList<ange::vessel::BlockWeight> > buoyancy_stresses_t;
    mutable buoyancy_stresses_t m_buoyancy;
    typedef QHash<const ange::schedule::Call*, QList<ange::vessel::BlockWeight> > tank_stresses_t;
    mutable tank_stresses_t m_tanks_stresses;
    mutable tank_stresses_t m_ballastTanksWeights;
    QHash<const ange::schedule::Call*, ange::vessel::BlockWeight> m_cargo_as_block_weight;
    SignalBuffererWithCall* m_bufferer;

};
#endif // STRESSES_BENDING_H
