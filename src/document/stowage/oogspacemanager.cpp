#include "oogspacemanager.h"

#include "container_move.h"
#include "document.h"
#include "problem_model.h"
#include "stowage.h"

#include <stowplugininterface/problem.h>

#include <ange/vessel/slot.h>
#include <ange/vessel/stack.h>
#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>
#include <ange/containers/container.h>
#include <ange/containers/oog.h>

#include <QHash>
#include <QTimer>

using ange::angelstow::Problem;
using ange::angelstow::ProblemLocation;
using ange::containers::Container;
using ange::containers::IsoCode;
using ange::containers::Oog;
using ange::schedule::Call;
using ange::units::Length;
using ange::units::centimeter;
using ange::vessel::BayRowTier;
using ange::vessel::Slot;
using ange::vessel::Stack;

static const Length allowedOog = 6 * centimeter;

OOGSpaceManager::OOGSpaceManager(document_t* document)
  : QObject(document), m_document(document), m_delayedViolationsCheck(new QTimer(this))
{
    setObjectName("OOGSpaceManager");
    m_delayedViolationsCheck->setSingleShot(true);
    m_delayedViolationsCheck->setInterval(500);
    connect(m_delayedViolationsCheck,SIGNAL(timeout()),SLOT(checkOogViolations()));
}

OOGSpaceManager::~OOGSpaceManager() {
    // Empty
}

bool oogLessThanSixCm(const Oog& oog) {
    if (oog.back() > allowedOog) {
        return false;
    }
    if (oog.front() > allowedOog) {
        return false;
    }
    if (oog.left() > allowedOog) {
        return false;
    }
    if (oog.right() > allowedOog) {
        return false;
    }
    if (oog.top() > allowedOog) {
        return false;
    }
    return true;
}

void OOGSpaceManager::refresh(const Container* container) {
    m_blockedByOog.remove(container);
    if(!container->oog()) {
        m_delayedViolationsCheck->start();
        return;
    }
    if (oogLessThanSixCm(container->oog())) {
        m_delayedViolationsCheck->start();
        return;
    }
    QList<const container_move_t*> container_moves = m_document->stowage()->moves(container);
    if (container_moves.isEmpty()) {
        Q_FOREACH(const Call* call, m_document->schedule()->calls()) {
            buildFullList(call);
        }
        m_delayedViolationsCheck->start();
        return;
    }
    Q_FOREACH(const Call* call, m_document->schedule()->calls()) {
        const Slot* slot = m_document->stowage()->container_position(container,call);
        if(slot) {
            containerPlacedAtCall(container,call,slot);
            if (container->isoLength() != IsoCode::Twenty) {
                containerPlacedAtCall(container, call, slot->sister());
            }
        }
        buildFullList(call);
    }
    m_delayedViolationsCheck->start();
}

QSet<const ange::vessel::Slot* > OOGSpaceManager::allBlockedSlots(const Call* call) const {
    return m_allBlockedSlots.value(call);
}

void OOGSpaceManager::buildFullList(const ange::schedule::Call* call) {
    m_allBlockedSlots[call].clear();
    typedef QHash<const Call*,QList<const Slot*> > CallSlots;
    Q_FOREACH (CallSlots cs, m_blockedByOog.values()) {
        m_allBlockedSlots[call].unite(QSet<const Slot*>::fromList(cs.value(call)));
    }
}

void OOGSpaceManager::setDocument(document_t* document) {
    if(document != m_document) {
        setParent(document);
        m_document = document;
    }
}

void OOGSpaceManager::containerPlacedAtCall(const Container* container,const Call* call, const Slot* slot) {
    if (container->oog().left() > allowedOog) {
        Stack* port_stack = slot->stack()->portNeighbour();
        if(port_stack) {
            Q_FOREACH(Slot* portslot, port_stack->stackSlots()) {
                if(portslot->brt().tier() >= slot->brt().tier() && slot->isAft() == portslot->isAft()) {
                    m_blockedByOog[container][call] << portslot;
                }
            }
        }
    }
    if (container->oog().right() > allowedOog) {
        Stack* starboard_stack = slot->stack()->starboardNeighbour();
        if(starboard_stack) {
            Q_FOREACH(Slot* starboardslot, starboard_stack->stackSlots()) {
                if(starboardslot->brt().tier() >= slot->brt().tier() && slot->isAft() == starboardslot->isAft()) {
                    m_blockedByOog[container][call] << starboardslot;
                }
            }
        }
    }
    buildFullList(call);
}

void OOGSpaceManager::checkOogViolations() {
    QList<SlotCall> old_problems = m_slotCall_problems.keys();
    for(QHash<const ange::schedule::Call*, QSet<const ange::vessel::Slot*> >::iterator it = m_allBlockedSlots.begin(), end = m_allBlockedSlots.end(); it!= end; ++it) {
        const Call* call = it.key();
        Q_FOREACH(const Slot* slot, it.value()) {
            bool slotEmptyAtCall = m_document->stowage()->contents(slot,call).container() == 0;
            SlotCall sc(slot,call);
            old_problems.removeAll(sc);
            if(!slotEmptyAtCall) {
                if(!m_slotCall_problems.contains(sc)) {
                    ProblemLocation location;
                    location.setCall(call);
                    location.setBayRowTier(slot->brt());
                    Problem* problem = new Problem(Problem::Warning,location,QStringLiteral("Container occupies space taken up by OOG container"));
                    m_document->problem_model()->add_problem(problem);
                    m_slotCall_problems[sc] = problem;
                }
            }
        }
    }
    Q_FOREACH(const SlotCall& slotcall, old_problems) {
        delete m_slotCall_problems[slotcall];
        m_slotCall_problems.remove(slotcall);
    }
}


uint qHash(const OOGSpaceManager::SlotCall& slotCall) {
    return reinterpret_cast<size_t>(slotCall.m_call) + reinterpret_cast<size_t>(slotCall.m_slot);
}


#include "oogspacemanager.moc"
