#include "visibility_line.h"

#include "document/document.h"
#include "document/problems/problem_model.h"
#include "document/stowage/stowage.h"
#include "document/stowage/stowagestack.h"
#include "document/userconfiguration/userconfiguration.h"
#include "stability/stability.h"

#include <ange/schedule/schedule.h>
#include <ange/schedule/call.h>
#include <ange/vessel/bayslice.h>
#include <ange/vessel/point3d.h>
#include <ange/vessel/stacksupport.h>
#include <ange/vessel/stack.h>
#include <ange/vessel/vessel.h>

#include <cmath>

static double PI = std::acos(-1.);
static double FOOT = 0.3048;
// TODO: The panamafactor and panama view angle could probably better be set in some kind of configuration parameter.
static double PANAMA_VIEW_ANGLE =  PI/ 18;
static double PANAMA_TOTAL_BLOCKING_ANGLE = PI / 12;
static double PANAMA_MAX_SINGLE_BLOCK = PI / 36;
static double PANAMA_MIN_OPEN_VIEW = PI / 36;

using namespace ange::units;
using ange::angelstow::Problem;
using ange::angelstow::ProblemLocation;
using ange::schedule::Schedule;
using ange::vessel::Vessel;

visibility_line_t::visibility_line_t(document_t* document)
  : QObject(document), m_document(document), m_observer_set(false)
{
    Q_ASSERT(document);
    setObjectName("VisibilityLine");
    connect(m_document->schedule(), &Schedule::callAboutToBeRemoved, this, &visibility_line_t::remove_call);
    connect(m_document->stability(), &Stability::stabilityChanged, this, &visibility_line_t::invalidate_call);
    resetVessel();
}

void visibility_line_t::resetVessel() {
    m_visibilityLineEnabled = !qIsNaN(m_document->vessel()->observerLcg() / meter)
                              && !qIsNaN(m_document->vessel()->observerVcg() / meter)
                              && !qIsNaN(m_document->vessel()->lpp() / meter);
    if (m_visibilityLineEnabled) {
        m_observation_point = QPointF(m_document->vessel()->observerLcg() / meter, m_document->vessel()->observerVcg() / meter);
        m_observer_set = true;
    } else {
        m_observation_point = QPointF();
        m_observer_set = false;
    }
}

ange::units::Length visibility_line_t::get_visibility_maxheight(const ange::vessel::StackSupport* stack_support, const ange::schedule::Call* call)
{

    Length rv = std::numeric_limits<double>::infinity()*meter;
    if(!m_observer_set ) {
        return rv;
    }
    //Note: might not be appropriate for hatch-less vessels
    if(stack_support->stack()->level() != ange::vessel::Above) {
        return rv;
    }
    if(!m_trim.contains(call) || !m_draft.contains(call)){
        update_mass_and_center_and_trim(call);
    }
    if(!m_visibility_maxheights_dirty.contains(call) || m_visibility_maxheights_dirty[call]) {
        update_cached_visibilityheights(call);
    }
    rv = m_visibility_maxheights[call][m_document->vessel()->baySliceContaining(stack_support->bay())] - stack_support->bottomPos().v();
    return rv;
}

ange::units::Length visibility_line_t::get_visibility_maxheight_coordinate(const ange::schedule::Call* call, const ange::vessel::BaySlice* bayslice)
{
  if(m_visibility_maxheights.contains(call) && m_visibility_maxheights[call].contains(bayslice)) {
    return m_visibility_maxheights[call][bayslice];
  }
  return std::numeric_limits<double>::infinity()*meter;
}

void visibility_line_t::update_cached_visibilityheights()
{
  if(m_observer_set) {
    Q_FOREACH(const ange::schedule::Call* call, m_blocking_angles_dirty.keys()) {
      update_cached_visibilityheights(call);
    }
  }
}

void visibility_line_t::update_cached_visibilityheights(const ange::schedule::Call* call)
{
  if(m_observer_set) {
    if(!m_visibility_maxheights_dirty.contains(call) || m_visibility_maxheights_dirty[call]) {
        update_mass_and_center_and_trim(call);
    // TODO move things not dependent on bay out of loop
      Q_FOREACH(const ange::vessel::BaySlice* bayslice, m_document->vessel()->baySlices()){
        Length bayslice_lcg = bayslice->stacks().first()->bottom().l();
        if( bayslice_lcg > m_observation_point.x()*meter) {
          Length vessel_length = m_document->vessel()->lpp();
          //trim measured relative to mid-ship position
          Length mid_ship = vessel_length / 2.0;
          //trim angle, measured at the stern? FIXME
          double trim_angle = atan( m_trim[call] / vessel_length);
          //observer height relative to sea surface
          Length rel_observerheight = m_observation_point.y()*meter - m_draft[call]- ( mid_ship - m_observation_point.x()*meter)*( m_trim[call] / vessel_length);
          //distance from stern to the intersection of visibility line and sea surface (IMO rule)
          Length forward_viewlength = std::min(500.0*meter, 2*m_document->vessel()->lpp());
          //distance from the stack to the intersection point
          Length rel_stack_to_limit =  forward_viewlength + mid_ship - (bayslice_lcg - mid_ship) / cos(trim_angle);
          //distance from observer to intersection point
          Length rel_observer_to_limit = forward_viewlength + mid_ship + (mid_ship - m_observation_point.x()*meter) / cos(trim_angle);
          //max allowable stack height, relative to the sea level
          Length rel_stackheight = rel_observerheight * (rel_stack_to_limit / rel_observer_to_limit) - (m_trim[call] / vessel_length)*(bayslice_lcg - mid_ship);
          m_visibility_maxheights[call][bayslice] = rel_stackheight + m_draft[call];
          m_visibility_maxheights_dirty[call]=false;
          emit repaint_stowage_stack_labels();
        }
      }
    }
  }
}


void visibility_line_t::update_mass_and_center_and_trim(const ange::schedule::Call* call) {
    if(m_document->stowage()){
        m_draft[call] = m_document->stability()->data(call)->draft;
        m_trim[call]  = m_document->stability()->data(call)->trim;
    }
}

void visibility_line_t::invalidate_call(const ange::schedule::Call* call) {
    if (!m_visibilityLineEnabled) {
        return;
    }
  m_trim.remove(call);
  m_draft.remove(call);
  m_blocking_angles_dirty[call]=true;
  m_visibility_maxheights_dirty[call]=true;
  update_cached_visibilityheights();
}

bool visibility_line_t::is_stack_in_forward_view(const ange::vessel::Stack* stack)
{
    if (!m_observer_set) {
        return false;
    }
    ange::vessel::StackSupport* someStackSupport = 0L;
    if (stack->stackSupport20Aft()) {
        someStackSupport = stack->stackSupport20Aft();
    } else if (stack->stackSupport20Fore()){
        someStackSupport = stack->stackSupport20Fore();
    } else if (stack->stackSupport40()){
        someStackSupport = stack->stackSupport40();
    }
    Q_ASSERT(someStackSupport);
  if(someStackSupport->bottomPos().l() < m_observation_point.x()*meter){
    return false;
  } else {
    QPair< double,double > angle_span = get_anglespan(someStackSupport);
    if( (angle_span.first >= -1*PANAMA_VIEW_ANGLE && angle_span.first <= PANAMA_VIEW_ANGLE ) ||
      (angle_span.second >= -1*PANAMA_VIEW_ANGLE && angle_span.second <= PANAMA_VIEW_ANGLE ) ||
      (angle_span.first <= -1*PANAMA_VIEW_ANGLE && angle_span.second >= PANAMA_VIEW_ANGLE) ) {
      return true;
    } else {
      return false;
    }
  }
}

QList< QPointF > visibility_line_t::get_rel_corners(const ange::vessel::StackSupport* stack_support)
{
  QList< QPointF > rv;
  double slotwidth = 8.0*FOOT;
  double slotlenght = (stack_support->stackSupportType()==ange::vessel::StackSupport::Twenty?20.0*FOOT:40.0*FOOT);
  //FIXME: use a ange::vessel::Point3D
  QPointF rel_slotcentre = QPointF(stack_support->bottomPos().l()/meter-m_observation_point.x(),stack_support->bottomPos().t()/meter);
  rv.append(QPointF(rel_slotcentre.x()+slotlenght/2.,rel_slotcentre.y()+slotwidth/2.));
  rv.append(QPointF(rel_slotcentre.x()-slotlenght/2.,rel_slotcentre.y()+slotwidth/2.));
  rv.append(QPointF(rel_slotcentre.x()-slotlenght/2.,rel_slotcentre.y()-slotwidth/2.));
  rv.append(QPointF(rel_slotcentre.x()+slotlenght/2.,rel_slotcentre.y()-slotwidth/2.));
  return rv;
}

QPair< double, double > visibility_line_t::get_anglespan(const ange::vessel::StackSupport* someStackSupport) {
    QList<double> angles;
    QList< QPointF > corners = get_rel_corners(someStackSupport);
    Q_FOREACH( QPointF corner, corners) {
      double angle = (corner.y()<0?-std::atan(std::abs(corner.y())/corner.x()):std::atan(std::abs(corner.y())/corner.x()));
      angles.append(angle);
    }
    double min_angle = std::numeric_limits<double>::infinity();
    double max_angle = -std::numeric_limits<double>::infinity();
    Q_FOREACH( double angle, angles) {
      if( angle < min_angle ) {
        min_angle = angle;
      }
      if (angle > max_angle ) {
        max_angle = angle;
      }
    }
    return QPair< double, double>(min_angle, max_angle);
}


void visibility_line_t::add_stack_blocking_visibility(const ange::schedule::Call* call, int id, QPointF blocking)
{
  if(m_observer_set) {
    m_blocking_angles[call][id] = blocking;
    m_blocking_angles_dirty[call]=true;
  }
}

void visibility_line_t::remove_stack_blocking_visibility(const ange::schedule::Call* call, int id)
{
  if(m_observer_set) {
    if(!m_blocking_angles[call].isEmpty() && m_blocking_angles[call].contains(id)) {
      m_blocking_angles[call].remove(id);
    }
    m_blocking_angles_dirty[call] =true;
  }
}

void visibility_line_t::calculate_visibility_line_violations(const ange::schedule::Call* call)
{
  QList< QPointF > raw_blockings = m_blocking_angles[call].values() ;
  if(!raw_blockings.isEmpty()){
    // We then have to sort the entries.
    QMap<double,QPointF> sorting;
    Q_FOREACH(QPointF point, raw_blockings){
      sorting.insert(point.x(),point);
    }
    raw_blockings = sorting.values();
    sorting.clear();
    // We now combine blocks.
    QList<QPointF> blockings;
    QPointF start = raw_blockings.takeFirst();
    blockings << start;
    while(!raw_blockings.isEmpty()) {
      QPointF span = raw_blockings.takeFirst();
      bool appended=false;
      for(int i=0;i<blockings.size();++i){
        if(span.x()<=blockings[i].y()+0.001) {
          if(span.y() > blockings[i].y()){
            blockings[i].ry()=span.y();
          }
          appended = true;
        }
      }
      if(!appended){
        blockings << span;
      }
    }
    double min_clear_sector = std::numeric_limits<double>::infinity();
    double max_blocked_sector = -std::numeric_limits<double>::infinity();
    double total_blocked_sector = 0.0;
    for(int i=0;i<blockings.size();++i) {
      max_blocked_sector=std::max(max_blocked_sector,blockings[i].y()-blockings[i].x());
      total_blocked_sector += blockings[i].y()-blockings[i].x();
      for(int j=0;j<blockings.size();++j) {
        if(i!=j) {
          if(blockings[i].x() < blockings[j].x()) {
            min_clear_sector=std::min(min_clear_sector,blockings[j].x()-blockings[i].y());
          } else {
            min_clear_sector=std::min(min_clear_sector,blockings[i].x()-blockings[j].y());
          }
        }
      }
    }
    if(total_blocked_sector > PANAMA_TOTAL_BLOCKING_ANGLE || max_blocked_sector > PANAMA_MAX_SINGLE_BLOCK || min_clear_sector < PANAMA_MIN_OPEN_VIEW){
      if(!m_blocked_visibility_problem.contains(call)) {
        Problem* problem = report_problem(Problem::Error, QStringLiteral("Visibility blocked"),call,tr("Too many stacks exceed the visibility line and block the minimum allowed forward view"));
        m_blocked_visibility_problem.insert(call,problem);
        emit repaint_stowage_stack_labels();
      }
    } else {
      if(m_blocked_visibility_problem.contains(call)) {
        delete m_blocked_visibility_problem[call];
        m_blocked_visibility_problem.remove(call);
        emit repaint_stowage_stack_labels();
      }
    }
  }
  m_blocking_angles_dirty[call] = false;
}

bool visibility_line_t::has_error(const ange::schedule::Call* call)
{
  return m_blocked_visibility_problem.contains(call);
}


void visibility_line_t::update_visibility_line_violations(const ange::schedule::Call* call)
{
  if(!m_blocking_angles_dirty.contains(call) || m_blocking_angles_dirty[call]) {
    calculate_visibility_line_violations(call);
  }
}

Problem* visibility_line_t::report_problem(Problem::Severity severity, const QString& description, const ange::schedule::Call* call, const QString& details)
{
    ProblemLocation loc;
    loc.setCall(call);
    Problem* problem = new Problem(severity, loc, description, this);
    problem->setDetails(details);
    m_document->problem_model()->add_problem(problem);
    return problem;
}

void visibility_line_t::remove_call(ange::schedule::Call* call)
{
  if(m_blocked_visibility_problem.contains(call)) {
    delete m_blocked_visibility_problem[call];
    m_blocked_visibility_problem.remove(call);
  }
}

#include "visibility_line.moc"
