/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010

 Copyright: See COPYING file that comes with this distribution

*/

#include "crane_split_bin.h"
#include "crane_split_bay.h"
typedef QPair<crane_split_bay_t*,crane_split_bin_include_t::crane_split_bin_includes_t> crane_split_bay_include_t;

crane_split_bin_t::crane_split_bin_t(const crane_split_t* crane_split, QList< crane_split_bay_include_t > bay_slices, int max_cranes, bool superbin) :
    m_crane_split_bays(bay_slices),
    m_max_cranes(max_cranes),
    m_crane_split(crane_split),
    m_superbin(superbin)
{

}

int crane_split_bin_t::max_cranes() const {
  return m_max_cranes;
}

crane_split_bay_t::MoveStatistics crane_split_bin_t::moveStatistics() const {
    crane_split_bay_t::MoveStatistics moveStats;
    Q_FOREACH(crane_split_bay_include_t crane_split_bay_include, bays()) {
        crane_split_bay_t::MoveStatistics stats = crane_split_bay_include.first->moveStatistics(crane_split_bay_include.second);
        moveStats += stats;
    }
    return moveStats;
}
