#include "stowerimplementation.h"

#include "container_move.h"
#include "container_stow_command.h"
#include "document.h"
#include "merger_command.h"
#include "stowage.h"

#include <ange/schedule/call.h>

#include <QUndoCommand>

using ange::angelstow::Move;

StowerImplementation::StowerImplementation(document_t* document)
  : IStower(),
    m_document(document),
    m_command(new merger_command_t("StowerImplementation")),
    m_stowage_signal_lock(m_document->stowage()->prepare_for_major_stowage_change())
{
    // Empty
}

StowerImplementation::~StowerImplementation() {
    // Empty
}

void StowerImplementation::replaceStow(const ange::containers::Container* container, const QList<Move>& moves) {
    QList<container_move_t> addedMoves;
#ifndef NDEBUG
    const ange::schedule::Call* lastCall = 0;
    const ange::vessel::Slot* currentPos = 0;
#endif
    Q_FOREACH (const Move& move, moves) {
#ifndef NDEBUG
        if (!move.call()) {
            Q_ASSERT("All moves must have calls in ");
        }
        if (lastCall && !currentPos) {
            Q_ASSERT("Only the last move can be with null slot in ");
        }
        if (lastCall && lastCall->distance(move.call()) < 0) {
            Q_ASSERT("moves must be sorted according to schedule in ");
        }
        lastCall = move.call();
        currentPos = move.slot();
#endif
        addedMoves << container_move_t(container, move);
    }
    container_stow_command_t* stow = new container_stow_command_t(m_document);
    stow->change_moves(container, m_document->stowage()->moves(container), addedMoves);
    m_command->push(stow);
}

void StowerImplementation::setUndoText(const QString& undo_text) {
    m_command->setText(undo_text);
}

QUndoCommand* StowerImplementation::takeCommand() {
    return m_command.take();
}
