/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010

 Copyright: See COPYING file that comes with this distribution

*/

#ifndef CRANE_SPLIT_BAY_H
#define CRANE_SPLIT_BAY_H

#include <QObject>

#include "crane_split_bay_dir.h"
#include "crane_split_bin_include.h"
#include <ange/vessel/decklevel.h>

class crane_split_t;
namespace ange {
namespace schedule {
class Call;
}
}

/**
 *
 */
class crane_split_bay_t  : public QObject {
  Q_OBJECT
  public:
    crane_split_bay_t(int joinedBay, const ange::schedule::Call* call, crane_split_t* crane_split,
                      bool vessel_twinlifting_above, bool vessel_twinlifting_below);

    /**
     * @return load part
     */
    crane_split_bay_dir_t& load() {
      return m_load;
    }

    /**
     * @return load part
     */
    const crane_split_bay_dir_t& load() const {
      return m_load;
    }

    /**
     * @return discharge part
     */
    crane_split_bay_dir_t& discharge() {
      return m_discharge;
    }

    /**
     * @return discharge part
     */
    const crane_split_bay_dir_t& discharge() const {
      return m_discharge;
    }

    /**
     * @return restow part
     */
    crane_split_bay_dir_t& restow() {
      return m_restow;
    }

    /**
     * @return restow part
     */
    const crane_split_bay_dir_t& restow() const {
      return m_restow;
    }

    /**
     * @return joined bay
     */
    int joinedBay() const {
      return m_joinedBay;
    }

    /**
     *  @return crane_split
     */
    crane_split_t* crane_split() const {
      return m_crane_split;
    }
    /**
     * @return call
     */
    const ange::schedule::Call* call() const {
      return m_call;
    }


    /**
     * Return total time spent in bay
     */
    double total_time() const;

    /**
     * \brief move statistics
     *
     * total is taking twinlifting into account
     */
    struct MoveStatistics {
        int fourty;
        int twenty;
        double total;
        double time;
        int twinlifts;
        double totalffe;
        MoveStatistics() : fourty(0), twenty(0), total(0), time(0), twinlifts(0), totalffe(0) {}
        /**
         * adds by adding all members.
         */
        MoveStatistics& operator+=(const MoveStatistics& rhs) {
            fourty+=rhs.fourty;
            twenty+=rhs.twenty;
            total+=rhs.total;
            time+=rhs.time;
            twinlifts+=rhs.twinlifts;
            totalffe += rhs.totalffe;
            return *this;
        }
    };
    /**
     * calculates and returns the move count for fourties, twenties and a total involving twinlifting and
     * the time used for moving all based on which stacks contribute
     */
    MoveStatistics moveStatistics(crane_split_bin_include_t::crane_split_bin_includes_t include_state) const;

    /**
     * @return true if this bay support twinlifting above as given by the vessel.
     */
    bool vessel_twinlifting_above() const {
      return m_vessel_twinlifting_above;
    }

    /**
     * @return true if this bay support twinlifting below as given by the vessel.
     */
    bool vessel_twinlifting_below() const {
      return m_vessel_twinlifting_below;
    }

private:
    int m_joinedBay;
    crane_split_t* m_crane_split;
    const ange::schedule::Call* m_call;

    crane_split_bay_dir_t m_load;
    crane_split_bay_dir_t m_discharge;
    crane_split_bay_dir_t m_restow;

    bool m_vessel_twinlifting_above;
    bool m_vessel_twinlifting_below;

    int fourtyMoves() const;
    int twentyAboveForeMoves() const;
    int twentyAboveAftMoves() const;
    int twentyBelowForeMoves() const;
    int twentyBelowAftMoves() const;
};
inline QDebug operator <<(QDebug debug, const crane_split_bay_t::MoveStatistics& ms) {
    debug << ms.fourty << "f " << ms.twenty << "t " << ms.total << "to " << ms.time << "ti ";
    return debug;
}

#endif // CRANE_SPLIT_BAY_H
