#include "overstowcounter.h"

#include "container_move.h"
#include "crane_split.h"
#include "stowage.h"

#include <ange/containers/container.h>
#include <ange/schedule/schedule.h>
#include <ange/vessel/bayslice.h>
#include <ange/vessel/direction.h>
#include <ange/vessel/hatchcover.h>
#include <ange/vessel/slot.h>
#include <ange/vessel/stack.h>
#include <ange/vessel/stacksupport.h>
#include <ange/vessel/vessel.h>

#include <QListIterator>
#include <QTimer>

using namespace ange::containers;
using namespace ange::schedule;
using namespace ange::vessel;
using namespace ange::vessel::Direction;

// Restow:

struct Restow {
    const ange::vessel::Slot* slot;
    const ange::containers::IsoCode::IsoLength isoLength;
    Restow() : slot(0), isoLength(IsoCode::UnknownLength) { Q_ASSERT(false); }
    Restow(const ange::vessel::Slot* slot, const ange::containers::IsoCode::IsoLength isoLength)
      : slot(slot), isoLength(isoLength) {}
    bool operator==(const Restow& rhs) const {
        return this->slot == rhs.slot && this->isoLength == rhs.isoLength;
    }
};

inline uint qHash(const Restow& restow) {
    uint hash = 0;
    hash = 37 * hash + qHash(restow.slot);
    hash = 37 * hash + qHash(restow.isoLength);
    return hash;
}

QDebug &operator<<(QDebug debug, const Restow& restow) {
    return debug << "Restow(" << *restow.slot << restow.isoLength << ")";
}

// OverstowCounterData:

struct OverstowCounterData {
    mutable QHash<const ange::schedule::Call*, QHash<const ange::vessel::Stack*, QSet<Restow> > > craneSplitRestows;
    QTimer timer;
};

// OverstowCounter:

OverstowCounter::OverstowCounter(stowage_t* stowage, const Schedule* schedule)
  : QObject(), m_stowage(stowage), m_schedule(schedule), m_vessel(0), d(new OverstowCounterData),
    m_allCachesAreClean(false)
{
    d->timer.setSingleShot(true);
    d->timer.setInterval(10);
    QObject::connect(&d->timer, &QTimer::timeout, this, &OverstowCounter::updateAllCaches);
}

OverstowCounter::~OverstowCounter() {
    // Empty
}

void OverstowCounter::setVessel(const Vessel* vessel) {
    if (m_vessel) {
        reset();
    }
    m_vessel = vessel;
}

void OverstowCounter::movesChanged(const Slot* slot) const {
    Q_ASSERT(slot);
    Q_ASSERT(m_vessel);
    invalidateLidCache(slot->stack());
    invalidateMoveCache(slot->stack());
    d->timer.start();
    m_allCachesAreClean = false;
}

void OverstowCounter::reset() {
    Q_ASSERT(m_vessel);
    Q_FOREACH (const Call* call, d->craneSplitRestows.keys()) {
        Q_FOREACH (const QSet<Restow>& restows, d->craneSplitRestows[call]) {
            Q_FOREACH (const Restow& restow, restows) {
                m_stowage->crane_split()->subtract_restow(call, restow.slot, restow.slot, restow.isoLength);
            }
        }
    }
    d->craneSplitRestows.clear();

    m_lidCacheIsClean.clear();
    m_moveBelowLidCache.clear();

    m_moveCacheIsClean.clear();
    m_nowMoveCache.clear();
    m_laterMoveCache.clear();

    m_allCachesAreClean = false;
}

void OverstowCounter::updateAllCaches() {
    Q_ASSERT(m_vessel);
    if (m_allCachesAreClean) {
        return;
    }
    Q_FOREACH (const BaySlice* baySlice, m_vessel->baySlices()) {
        Q_FOREACH (const Stack* stack, baySlice->stacks()) {
            updateCaches(stack);
        }
    }
    m_allCachesAreClean = true;
}

void OverstowCounter::updateCaches(const Stack* stack) {
    Q_FOREACH (const HatchCover* lidBelow, stack->lidsBelow()) {
        Q_FOREACH (const Call* call, m_schedule->calls()) {
            updateLidCache(call, lidBelow);
        }
    }
    updateMoveCache(stack);
}

OverstowCounter::OverstowMoveType OverstowCounter::overstowMove(const Call* call, const Slot* slot) {
    Q_ASSERT(m_vessel);
    updateCaches(slot->stack());
    if (m_nowMoveCache[call].contains(slot)) {
        return Now;
    }
    if (m_laterMoveCache[call].contains(slot)) {
        return Later;
    }
    return None;
}

int OverstowCounter::overstowMoves(const Call* call) {
    updateAllCaches();
    return m_nowMoveCache[call].count();
}

void OverstowCounter::updateLidCache(const Call* call, const HatchCover* lid) const {
    if (m_lidCacheIsClean[call].contains(lid)) {
        return;
    }
    resetMoveBelowLidCache(call, lid);
    m_lidCacheIsClean[call].insert(lid);
}

static bool checkMoveBelowLid(const stowage_t* stowage, const Call* call, const HatchCover* lid) {
    Q_FOREACH (const Stack* stack, lid->stacksBelow()) {
        Q_FOREACH (const Slot* slot, stack->stackSlots()) {
            if (stowage->hasMove(call, slot)) {
                return true;
            }
        }
    }
    return false;
}

void OverstowCounter::resetMoveBelowLidCache(const Call* call, const HatchCover* lid) const {
    bool oldMoveBelowLid = m_moveBelowLidCache[call].contains(lid);
    bool newMoveBelowLid = checkMoveBelowLid(m_stowage, call, lid);

    if (newMoveBelowLid != oldMoveBelowLid) {
        if (newMoveBelowLid) {
            m_moveBelowLidCache[call].insert(lid);
        } else {
            m_moveBelowLidCache[call].remove(lid);
        }
        // Invalidate other cache
        Q_FOREACH (const Stack* stackAbove, lid->stacksAbove()) {
            invalidateMoveCache(stackAbove);
        }
    }
}

void OverstowCounter::invalidateLidCache(const Stack* stack) const {
    Q_FOREACH (const HatchCover* lidAbove, stack->lidsAbove()) {
        Q_FOREACH (const Call* call, m_schedule->calls()) {
            m_lidCacheIsClean[call].remove(lidAbove);
        }
    }
}

void OverstowCounter::updateMoveCache(const Stack* stack) {
    if (!m_moveCacheIsClean.contains(stack)) {
        Q_FOREACH (const Call* call, m_schedule->calls()) {
            resetNowMoveCache(call, stack);
        }
        resetLaterMoveCache(stack);
        m_moveCacheIsClean.insert(stack);
    }
}

void OverstowCounter::resetNowMoveCache(const Call* call, const Stack* stack) {
    QSet<const Slot*>& cacheByCall = m_nowMoveCache[call];

    // Clear old data in cache
    Q_FOREACH (const Slot* slot, stack->stackSlots()) {
        cacheByCall.remove(slot);
    }

    bool moveBelowLid = false;
    Q_FOREACH (const HatchCover* lidBelow, stack->lidsBelow()) {
        if (m_moveBelowLidCache[call].contains(lidBelow)) {
            moveBelowLid = true;
            break;
        }
    }

    QSet<Restow> newRestows;

    Q_FOREACH (AftFore end, aftForeValues()) {
        QListIterator<Slot*> slotsIterator(stack->stackSlots());
        // Spool slotsIterator to just after the first moved slot in this call
        while (!moveBelowLid && slotsIterator.hasNext()) {
            const Slot* slot = slotsIterator.next();
            if (slot->isAft() != (end == Aft)) {
                continue;
            }
            if (m_stowage->hasMove(call, slot)) {
                break; // Done spooling
            }
        }

        // Mark all the other slots above it that has containers without moves as restows
        // In the case of long containers (non-20') mark only the fore slot
        while (slotsIterator.hasNext()) {
            const Slot* slot = slotsIterator.next();
            if (slot->isAft() != (end == Aft)) {
                continue;
            }
            const Container* container = m_stowage->contents(slot, call).container();
            if (container && !m_stowage->container_moved_at_call(container, call)) {
                if (container->isoLength() != IsoCode::Twenty) {
                    slot = slot->isFore() ? slot : slot->sister();
                    Q_ASSERT(slot->isFore());
                }
                cacheByCall.insert(slot);
                newRestows << Restow(slot, container->isoLength());
            }
        }
    }

    // Synchronize restows registered in crane_split_t and here
    QSet<Restow> oldRestows = d->craneSplitRestows[call][stack];
    d->craneSplitRestows[call][stack] = newRestows;
    QSet<Restow> sharedRestows(oldRestows);
    sharedRestows.intersect(newRestows);
    oldRestows.subtract(sharedRestows); // oldRestows now only contains Restows to be removed
    newRestows.subtract(sharedRestows); // newRestows now only contains Restows to be added
    Q_FOREACH (const Restow& restow, newRestows) {
        m_stowage->crane_split()->add_restow(call, restow.slot, restow.slot, restow.isoLength);
    }
    Q_FOREACH (const Restow& restow, oldRestows) {
        m_stowage->crane_split()->subtract_restow(call, restow.slot, restow.slot, restow.isoLength);
    }
}

void OverstowCounter::resetLaterMoveCache(const Stack* stack) const {
    // Clear old data in cache
    Q_FOREACH (const Call* call, m_schedule->calls()) {
        QSet<const Slot*>& cacheByCall = m_laterMoveCache[call];
        Q_FOREACH (const Slot* slot, stack->stackSlots()) {
            cacheByCall.remove(slot);
        }
    }

    Q_FOREACH (const Slot* slot, stack->stackSlots()) {
        QListIterator<Call*> callsIterator(m_schedule->calls());
        callsIterator.toBack();
    outerLoop:
        while (callsIterator.hasPrevious()) {
            const Call* call = callsIterator.previous();
            if (!m_nowMoveCache[call].contains(slot)) {
                continue; // Skip call if not overstow moved now
            }
            const Call* loadCall = m_stowage->get_move(slot, call)->call();
            Q_ASSERT(loadCall); // There must be a load move as there is an overstow move now
            while (true) {
                m_laterMoveCache[call].insert(slot);
                if (call == loadCall || !callsIterator.hasPrevious()) {
                    goto outerLoop; // Use goto to break out of two loops
                }
                Q_ASSERT(callsIterator.hasPrevious());
                call = callsIterator.previous();
            }
        }
    }
}

void OverstowCounter::invalidateMoveCache(const Stack* stack) const {
    m_moveCacheIsClean.remove(stack);
}

#include <overstowcounter.moc>
