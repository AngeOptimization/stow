/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010

 Copyright: See COPYING file that comes with this distribution

*/

#include "crane_split_bay_dir.h"

crane_split_bay_dir_t::crane_split_bay_dir_t() :
        m_twenty_fore(),
        m_twenty_aft(),
        m_fourty() {

}


crane_split_bay_dir_t::BelowAbove& crane_split_bay_dir_t::fourty() {
    return m_fourty;
}

const crane_split_bay_dir_t::BelowAbove& crane_split_bay_dir_t::fourty() const {
    return m_fourty;
}

crane_split_bay_dir_t::BelowAbove& crane_split_bay_dir_t::twentyAft() {
    return m_twenty_aft;
}

const crane_split_bay_dir_t::BelowAbove& crane_split_bay_dir_t::twentyAft() const {
    return m_twenty_aft;
}

crane_split_bay_dir_t::BelowAbove& crane_split_bay_dir_t::twentyFore() {
    return m_twenty_fore;
}

const crane_split_bay_dir_t::BelowAbove& crane_split_bay_dir_t::twentyFore() const {
    return m_twenty_fore;
}
