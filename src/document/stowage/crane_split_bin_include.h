#ifndef CRANE_SPLIT_BIN_INCLUDE_H
#define CRANE_SPLIT_BIN_INCLUDE_H

struct crane_split_bin_include_t
{
  /**
  * enums describing the how much of a given bay is to be included in a given crane split bin.
  */
  enum crane_split_bin_includes_t {
      FORE20_BAY = 0,
      FORE20_FORTY_BAYS = 1,
      FORE20_FOURTY_AFT20_BAYS = 2,
      FOURTY_AFT20_BAYS = 3,
      AFT20_BAY = 4,
      PURE_FOURTY_BAY = 5,
      PURE_TWENTY_BAY = 6,
      FORE20_AFT20_BAYS = 7
    };
};

#endif // CRANE_SPLIT_BIN_INCLUDE_H
