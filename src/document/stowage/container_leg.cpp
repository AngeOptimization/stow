#include "container_leg.h"

#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>

#include <QtCore>

using ange::containers::Container;
using ange::schedule::Call;

ContainerLeg::ContainerLeg(QObject* parent): QObject(parent)
{
    setObjectName("ContainerLeg");
}

void ContainerLeg::add_container(const ange::containers::Container* container, const ange::schedule::Call* source, const ange::schedule::Call* destination) {
    Q_ASSERT(container);
    Q_ASSERT(source);
    Q_ASSERT(destination);
    Q_ASSERT(*source < *destination);
    Q_ASSERT(!m_containerPOD.contains(container));
    Q_ASSERT(!m_containerPOL.contains(container));
    m_containerPOD[container] = destination;
    m_containerPOL[container] = source;
    m_containersCountPOD[destination] += 1;
    m_containersCountPOL[source] += 1;
    validate();
}

static void decrementCountMap(QHash<const Call*, int>* map, const Call* call) {
    (*map)[call] -= 1;
    if (map->value(call) == 0) {
        map->remove(call);
    }
}

void ContainerLeg::remove_container(const ange::containers::Container* container) {
    Q_ASSERT(container);
    Q_ASSERT(m_containerPOD.contains(container));
    Q_ASSERT(m_containerPOL.contains(container));
    decrementCountMap(&m_containersCountPOD, m_containerPOD[container]);
    decrementCountMap(&m_containersCountPOL, m_containerPOL[container]);
    m_containerPOD.remove(container);
    m_containerPOL.remove(container);
    validate();
}

const ange::schedule::Call* ContainerLeg::portOfDischarge(const ange::containers::Container* container) const {
    Q_ASSERT(container);
    Q_ASSERT(m_containerPOD.contains(container));
    return m_containerPOD.value(container);
}

const ange::schedule::Call* ContainerLeg::portOfLoad(const ange::containers::Container* container) const {
    Q_ASSERT(container);
    Q_ASSERT(m_containerPOL.contains(container));
    const Call* call = m_containerPOL.value(container);
    return call;
}

int ContainerLeg::destination_count(const ange::schedule::Call* call) const {
    return m_containersCountPOD.value(call, 0);
}

int ContainerLeg::source_count(const ange::schedule::Call* call) const {
    return m_containersCountPOL.value(call, 0);
}

void ContainerLeg::change_pod(const ange::containers::Container* container, const ange::schedule::Call* new_pod) {
    Q_ASSERT(new_pod);
    Q_ASSERT(m_containerPOD.contains(container));
    Q_ASSERT(m_containerPOL.contains(container));
    Q_ASSERT(*m_containerPOL.value(container) < *new_pod);
    Call const*& call = m_containerPOD[container];
    const Call* old_call = call;
    call = new_pod;
    decrementCountMap(&m_containersCountPOD, old_call);
    m_containersCountPOD[new_pod] += 1;
    validate();
    emit containerChangedPOD(container);
}

void ContainerLeg::change_pol(const ange::containers::Container* container, const ange::schedule::Call* new_pol) {
    Q_ASSERT(new_pol);
    Q_ASSERT(m_containerPOL.contains(container));
    Q_ASSERT(m_containerPOD.contains(container));
    Q_ASSERT(*new_pol < *m_containerPOD.value(container));
    Call const*& call = m_containerPOL[container];
    const Call* old_call = call;
    call = new_pol;
    decrementCountMap(&m_containersCountPOL, old_call);
    m_containersCountPOL[new_pol] += 1;
    validate();
    emit containerChangedPOL(container);
}

void ContainerLeg::validate(ange::schedule::Schedule* schedule) const {
#ifndef QT_NO_DEBUG
    if (m_containerPOD.size() % 50 > 1) {  // False on size 0, 1, 50, 51, 100, 101, ...
        return;  // Only validate about once every 25th change, otherwise document load will be very slow
    }
    Q_FOREACH (const Call* call, m_containerPOL.values() + m_containerPOD.values()
                                 + m_containersCountPOL.keys() + m_containersCountPOD.keys()) {
        Q_ASSERT(call->parent() == schedule);
    }
#else
    Q_UNUSED(schedule);
#endif
}

void ContainerLeg::validate() const {
    if (m_containerPOL.isEmpty()) {
        Q_ASSERT(m_containerPOD.isEmpty());
        Q_ASSERT(m_containersCountPOL.isEmpty());
        Q_ASSERT(m_containersCountPOD.isEmpty());
        return;
    }
    QObject* parent = m_containerPOL.begin().value()->parent();
    validate(qobject_cast<ange::schedule::Schedule*>(parent));
}

#include "container_leg.moc"
