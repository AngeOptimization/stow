#ifndef CRANE_SPLIT_H
#define CRANE_SPLIT_H

#include "crane_split_bay.h"
#include "crane_split_bin.h"
#include "crane_split_bin_include.h"

#include <ange/containers/isocode.h>

#include <QObject>
#include <QHash>
#include <QList>

class document_t;
class QModelIndex;
class QSignalMapper;
class QTimer;
namespace ange {
namespace angelstow {
class Problem;
}
namespace containers {
class Container;
}
namespace schedule {
class Call;
}
namespace vessel {
class Slot;
class Vessel;
}
}

class crane_split_t : public QObject {

    Q_OBJECT

public:

    /**
     * @return crane_split details for bay, call
     */
    crane_split_bay_t* details(const ange::schedule::Call* call, int bay) const {
        return m_bays.value(call).value(bay);
    }

    /**
     * (re)set document. Ownership is not claimed.
     */
    void set_document(document_t* document);

    /**
     * @return number of cranes for call
     */
    double no_cranes(const ange::schedule::Call* call) const;

    /**
     * @return total time for call (in hours)
     */
    double actual_total_crane_time(const ange::schedule::Call* call) const;

    /**
     * @return actual + a guestimate of the crane time needed for the containers on yard.
     **/
    double expected_total_crane_time(const ange::schedule::Call* call) const;

    /**
     * @return twinlift productivity;
     */
    double twinlift_productivity(const ange::schedule::Call* call) const;

    /**
     * @return singlelift productivity;
     */
    double singlelift_productivity(const ange::schedule::Call* call) const;

    /**
     * @return all bins for call
     */
    QList<crane_split_bin_t*> bins(const ange::schedule::Call* call) const;

Q_SIGNALS:
    void bay_changed(const ange::schedule::Call*, int bay);
    void calculations_done(const ange::schedule::Call* call);

private Q_SLOTS:
    void update_call(const ange::schedule::Call* call);
    void update_call_by_id(int call_id);
    void do_buffered_calculations();
    void add_call(ange::schedule::Call* call);
    void remove_call(ange::schedule::Call* call);
    void slotCallAboutToBeRemoved(ange::schedule::Call* call);
    void containersAdded(const QModelIndex& parent, int start, int end);
    void containersRemoved(const QModelIndex& parent, int start, int end);

private:

    /**
     * Construct empty cranesplit
     */
    crane_split_t(document_t* document, QObject* parent = 0L);

    /**
     * Destructor
     */
    ~crane_split_t();

    /**
     * Add a load
     */
    void add_load(const ange::schedule::Call* call, const ange::vessel::Slot* slot, const ange::containers::IsoCode::IsoLength isoLength);

    /**
     * Substract load
     */
    void subtract_load(const ange::schedule::Call* call, const ange::vessel::Slot* slot, const ange::containers::IsoCode::IsoLength isoLength);

    /**
     * Add a discharge
     */
    void add_discharge(const ange::schedule::Call* call, const ange::vessel::Slot* slot, const ange::containers::IsoCode::IsoLength isoLength);

    /**
     * Substract load
     */
    void subtract_discharge(const ange::schedule::Call* call, const ange::vessel::Slot* slot, const ange::containers::IsoCode::IsoLength isoLength);

    /**
     * Add a restow
     */
    void add_restow(const ange::schedule::Call* call, const ange::vessel::Slot* origin,
                    const ange::vessel::Slot* destination, const ange::containers::IsoCode::IsoLength isoLength);

    /**
     * Subtract a restow
     */
    void subtract_restow(const ange::schedule::Call* call, const ange::vessel::Slot* origin,
                         const ange::vessel::Slot* destination, const ange::containers::IsoCode::IsoLength isoLength);
    /**
     * @param data : the "move" to be modified
     * @param direction : whether to increase or decrease the "move"
     * @param restow : whether the "move" is a restow
     * @param call : the call in which the move is taking place
     * @param slot : the slot involved in the move - needed to know the bay and level of the slot
     * @param isoLength : container length of moved container
     */
    void modify_move(crane_split_bay_dir_t& data, const int direction, const bool restow, const ange::schedule::Call* call,
                     const ange::vessel::Slot* slot, const ange::containers::IsoCode::IsoLength isoLength);

    /**
     * Updates the container with new destination
     * \param container that changed
     * \param call old call
     */
    void update_expected_with_new_destination(const ange::containers::Container* container, const ange::schedule::Call* old_call);
    /**
     * Updates the container with new source
     * \param container that changed
     * \param call old call
     */
    void update_expected_with_new_source(const ange::containers::Container* container, const ange::schedule::Call* old_call);

    void update_call_now(const ange::schedule::Call* call);
    QPair<int, int> living_quater_pseudo_bay();
    void calculate_bins_for_call(const ange::schedule::Call* call);

private:
    /**
     * A struct to describe the time used per move / in port, normalized with
     * crane productivity, i.e. divide by the call's crane productivity to
     * get the actual times.
     */
    struct call_data_t {
        double m_twinliftDurationNormalized;
        double m_singleliftDurationNormalized;
        double m_actualTotalTimeNormalized;
        double m_expectedTotalTimeNormalized;
        call_data_t() {};
        call_data_t(double m_twinliftDurationNormalized, double singleliftDurationNormalized,
                    double actualTotalTime, double expectedTotalTime) :
            m_twinliftDurationNormalized(m_twinliftDurationNormalized),
            m_singleliftDurationNormalized(singleliftDurationNormalized),
            m_actualTotalTimeNormalized(actualTotalTime),
            m_expectedTotalTimeNormalized(expectedTotalTime) {};
    };

private:
    QHash<const ange::schedule::Call*, QHash<int, crane_split_bay_t*> > m_bays;
    QHash<const ange::schedule::Call*, call_data_t> m_calls_data;
    document_t* m_document;
    QHash<const ange::schedule::Call*, QList<crane_split_bin_t*> > m_bins;
    typedef QHash<const ange::schedule::Call*, ange::angelstow::Problem*> problems_t;
    problems_t m_problems;
    QSet<const ange::schedule::Call*> m_changed_calls;
    QTimer* m_buffer_timer;
    QSignalMapper* m_call_mapper;

private:
    friend class stowage_t;
    friend class OverstowCounter;

};

#endif // CRANE_SPLIT_H
