#ifndef STOWAGE_H
#define STOWAGE_H

#include "slot_call_content.h"

#include <ange/units/units.h>
#include <ange/vessel/bayrowtier.h>
#include <ange/vessel/blockweight.h>

#include <QHash>
#include <QObject>
#include <QSharedPointer>

class ContainerLeg;
class OOGSpaceManager;
class OverstowCounter;
class PluginManager;
class QTimer;
class StowageStack;
class container_move_t;
class crane_split_t;
class document_t;
class stowage_t;
namespace ange {
namespace angelstow {
class IStower;
class ValidatorPlugin;
}
namespace containers {
class Container;
}
namespace schedule {
class Call;
class Schedule;
}
namespace vessel {
class BaySlice;
class HatchCover;
class Slot;
class Stack;
class Vessel;
}
}

class stowage_signal_lock_t {
    friend class stowage_t;
    stowage_signal_lock_t(stowage_t* stowage) ;
    stowage_signal_lock_t(const stowage_signal_lock_t&); // no copying
    const stowage_signal_lock_t& operator=(const stowage_signal_lock_t&); // no assignment
    stowage_t* m_stowage;
public:
    ~stowage_signal_lock_t();
};

/**
 * The position of the containers in all calls.
 * Is responsible for managing the "moves".
 * Caches some counts that represents the current moves state.
 */
class stowage_t : public QObject {
    Q_OBJECT

public:
    /**
     * Construct empty stowage
     * @param schedule schedule, does not take ownership of this
     */
    stowage_t(ange::schedule::Schedule* schedule);
    ~stowage_t();

    /**
     * Change the moves for this container
     * Moves in "removed" will be deleted in this function and as such it will take ownership of the added moves
     */
    void change_moves(const ange::containers::Container* container, QList<const container_move_t*> removed,
                      QList<const container_move_t*> added);

    /**
     * Change port of discharge of a container
     */
    void change_pod(const ange::containers::Container* container, const ange::schedule::Call* newPOD);

    /**
     * Change port of load of a container
     */
    void change_pol(const ange::containers::Container* container, const ange::schedule::Call* call);

    /**
     * @return all containers that is in this slot (at some point in time)
     */
    QList<const ange::containers::Container*> containers_in_slot(const ange::vessel::Slot* slot) const;

    /**
     * @return move for container at call, or 0L if no move at call.
     * Overstow moves are NOT considered as they don't have a container_move_t.
     */
    const container_move_t* container_moved_at_call(const ange::containers::Container* container, const ange::schedule::Call* call) const;

    /**
     * @return true if the slot has a move (load, discharge or planned restow) in the given call.
     * Overstow moves are NOT considered.
     */
    bool hasMove(const ange::schedule::Call* call, const ange::vessel::Slot* slot) const;

    /**
     * @return all movements for one container
     * The pointers returned are owned by the stowage
     *
     * The moves list has the following format:
     *  - it can be empty if there is no planned moves for the container
     *  - there is a load move before every discharge move
     *  - a planned restow is represented by having two load moves next to each other
     *  - the last move must be a discharge move
     *  - there is only one move per call
     *  - it is ordered by call
     *
     * Overstow moves are NOT considered as they don't have a container_move_t.
     */
    QList<const container_move_t*> moves(const ange::containers::Container* container) const;

    /**
     * @return all movements for one container inclusive the moves induced by the source and destination.
     * Overstow moves are NOT induced.
     */
    QList<container_move_t> moves_inclusive_induced(const ange::containers::Container* container) const;

    /**
     * @return position of container. If container spans 2 slots, master is returned
     */
    const ange::vessel::Slot* container_position(const ange::containers::Container* container, const ange::schedule::Call* call) const;

    /**
     * @return nomiel position, e.g. a 40-foot container would have an even bay number
     */
    ange::vessel::BayRowTier nominal_position(const ange::containers::Container* container, const ange::schedule::Call* call) const;

    /**
     * @return current load or shift call for a container (0 if container is not loaded at call)
     * in other words, returns when this container was moved into it's current position provided it is onboard at call
     */
    const ange::schedule::Call* load_call(const ange::containers::Container* container, const ange::schedule::Call* call) const;

    /**
     * @return contents of a slot in a call
     * This is header-only and is called in several tight loops
     */
    slot_call_content_t contents(const ange::vessel::Slot* slot, const ange::schedule::Call* call) const {
        return m_call_slot_map[call].value(slot);
    }

    /**
     * @return all the contents in a given call
     * This is header-only and called in tight loops
     */
    QHash<const ange::vessel::Slot*, slot_call_content_t> contentsAtCall(const ange::schedule::Call* call) const {
        return m_call_slot_map[call];
    }

    /**
     * @returns a pointer to the oog space manager for this stowage
     */
    const OOGSpaceManager* oogSpaceManager() const;

    /**
     * Get the corresponding crane split
     */
    crane_split_t* crane_split() {
        return m_crane_split;
    }

    /**
     * Get the corresponding crane split
     */
    const crane_split_t* crane_split() const {
        return m_crane_split;
    }

    /**
     * (re)set document
     */
    void set_document(document_t* document);

    /**
     * @return stowage stack info for stack and call
     */
    StowageStack* stowage_stack(const ange::schedule::Call* call, const ange::vessel::Stack* stack);

    /**
     * @return stowage stack info for stack and call
     */
    const StowageStack* stowage_stack(const ange::schedule::Call* call, const ange::vessel::Stack* stack) const;

    /**
     * @return load count of call
     */
    int load_count(const ange::schedule::Call* call) const;

    /**
     * @return discharge count of call
     */
    int discharge_count(const ange::schedule::Call* call) const;

    /**
     * @return restow count of call, sum of planned restows and overstow moves
     */
    int restow_count(const ange::schedule::Call* call) const;

    /**
     * Return containers routes (which contains (mapped) POL/POD for containers)
     */
    ContainerLeg* container_routes() {
        return m_container_routes;
    }

    /**
     * Return containers routes (which contains (mapped) POL/POD for containers)
     */
    const ContainerLeg* container_routes() const {
        return m_container_routes;
    }

    /**
     * @return containers on board when departing call current_call
     */
    QSet<const ange::containers::Container*> get_containers_on_board(const ange::schedule::Call* current_call) const;

    /**
     * Fixate (convert from macro to micro) or unfixate containers
     */
    void set_stow_type(QList< ange::containers::Container* > containers, const ange::angelstow::StowType stow_type);

    /**
     * returns the center of mass and the mass of the stowage and ship at the given call.
     * Note that the Y and the Z coordinate in center of mass does only include the contributions
     * from the stowage, not anything related to the hull nor the tanks.
     * TODO Move out of stowage_t, perhaps to Stability?
     */
    ange::vessel::BlockWeight stowageAsBlockWeight(const ange::schedule::Call* call);

    /**
     * @return the bays' content (i.e. cargo) as BlockWeights
     */
    QHash<const ange::vessel::BaySlice*, ange::vessel::BlockWeight> bay_block_weights(const ange::schedule::Call* call) const;

    /**
     * Call before major changes of the stowage. This will prevent signals from the stowage, until the returned
     * lock is destroyed.  If signals are already blocked a NULL pointer is returned.
     */
    QSharedPointer<stowage_signal_lock_t> prepare_for_major_stowage_change();

    /**
     * Set a (new) vessel. Stowage is required to be empty (no container stowed in any port).
     * bay weight limits are cleared.
     */
    void set_vessel(const ange::vessel::Vessel* vessel);

    /**
     * @return the container move that filled the slot at call.
     * If slot is empty, null is returned.
     * Overstow moves are NOT considered as they don't have a container_move_t.
     */
    const container_move_t* get_move(const ange::vessel::Slot* slot, const ange::schedule::Call* call) const;

    /**
     * @return the container move that will empty the slot at call.
     * If slot is empty, null is returned.
     * Overstow moves are NOT considered as they don't have a container_move_t.
     */
    const container_move_t* get_out_move(const ange::vessel::Slot* slot, const ange::schedule::Call* call) const;

    /**
     * @return a newly created stowage manipulation interface for a plugin.
     * Note that this stowage should not be manipulated while this class exists.
     * @param stow_type all moves are created as this stow_type
     */
    ange::angelstow::IStower* create_stowing();

    /**
    * @returns the the first call where the slot is no longer available
    *    If the slot is never available, returns call
    *    If the slot is always available, return "After"
    *   this is so that the container can be discharged as appropriate. Especially not the After behaviour.
    * @param exception If non-null, this particular container will be ignored for the purpose of determening
    * availability
    * @param micro-stow ignore non-microstowed containers for the purpose of this function
    */
    const ange::schedule::Call* slot_available_until(const ange::vessel::Slot* slot, const ange::schedule::Call* call,
            const ange::schedule::Call* cutoff_call = 0L, const ange::containers::Container* exception = 0L, bool micro_only = true) const;

    /**
     * @returns true if changing the @param call @param moveDelta is legal, i.e.
     * doesn't cause cargo to have impossible load-discharge combinations and doesn't cause duplicate
     * containers in slots.
     * Otherwise false
     */
    QString changeOfRotationIsLegal(const ange::schedule::Call* call, int moveDelta) const;

    /**
     * @returns the corresponding load move for a discharge move (it might actually be a shift/restow move).
     *       If called with a load move, will throw std::invalid_argument
     * NOTE this function *should* always return a non-null pointer,
     * but if the stowage is corrupt it can return a null pointer unless
     * running in debug mode, where it will crash. It will always issue a warning.
     * Overstow moves are NOT considered as they don't have a container_move_t.
     */
    const container_move_t* loadMoveForDischargeMove(const container_move_t* move) const;

    /**
     * Sets the plugins used for validation of the stowage.
     * Should probably only be called by the owing document_t
     */
    void setPluginManager(PluginManager* pluginManager);

    /**
     * Returns the plugins used for validation of the stowage
     */
    // FIXME this const is wrong!
    PluginManager* pluginManager() const;

    /**
     * @return the overstow counter
     */
    OverstowCounter* overstowCounter();

    /**
     * @return the overstow counter
     */
    const OverstowCounter* overstowCounter() const;

Q_SIGNALS:

    /**
     * stowage changed.
     * This is the big reset hammer and only emitted when a major stowage change has been performed.
     * If you can just use @ref stowageChangedAtCall or @ref container_changed_position,
     * consider doing that
     */
    void stowageChanged();

    /**
     * Stowage is changed at @param call. This signal is buffered.
     */
    void stowageChangedAtCall(const ange::schedule::Call* call);

    /**
     * container moved from source to target in call.
     * Note that listeners for this slot should also listen to stowageChanged, which
     * indicates that a large number of containers have moved
     * @param container container moved
     * @param source spot container is moved from. If null, it is moved from outside vessel
     * @param target spot container is moved to. If null, it is moved to outside vessel
     * @param call when the container is moved.
     */
    void container_changed_position(const ange::containers::Container* container, const ange::vessel::Slot* previous,
                                    const ange::vessel::Slot* present, const ange::schedule::Call* call,
                                    ange::angelstow::StowType old_type, ange::angelstow::StowType new_type);

private Q_SLOTS:

    void add_call(ange::schedule::Call* call);
    void remove_call(ange::schedule::Call* call);

    /**
     * Update stowage after schedule has changed
     * @param previousPosition position of moved call in old schedule
     * @param currentPosition position of moved call in current schedule
     */
    void changeRotation(int previousPosition, int currentPosition);

    void emitStowageChangedAtCall();

private: // functions

    /**
     * Initializes m_call_slot_map and m_stowage_stacks for the given call.
     * Creates new StowageStack objects for all stacks on vessel.
     */
    void initCachesForCall(const ange::schedule::Call* call);
    /**
     * Clears m_call_slot_map and m_stowage_stacks for the given call.
     * Destroys all StowageStack objects for the given call.
     */
    void clearCachesForCall(const ange::schedule::Call* call);
    /**
     * Calls init and clear for the given call
     */
    void resetCachesForCall(const ange::schedule::Call* call);

    /**
     * @return document for stowage
     */
    document_t* document() const;

    void stowageChangedAtCallBufferer(const ange::schedule::Call* call);

    /**
     * @return the move that caused the container to be moved before call
     */
    const container_move_t* moveForContainerInCall(const ange::containers::Container* container, const ange::schedule::Call* call) const;

    /**
     * @return list of calls where the container is moved plus source and destination
     */
    QList<const ange::schedule::Call*> activeCalls(const ange::containers::Container* container) const;

private: // variables

    ange::schedule::Schedule* m_schedule;

    crane_split_t* m_crane_split;

    ContainerLeg* m_container_routes;

    OOGSpaceManager* m_oogSpaceManager;

    QScopedPointer<OverstowCounter> m_overstowCounter;

    /**
     * Master data for stowage - mapping from containers to list of container moves
     */
    QHash<const ange::containers::Container*, QList<const container_move_t*> > m_container_move_map;

    /**
     * Lookup from (call, slot) -> (container, stow_type) - secondary data, but kept in sync all times
     * This hash map is called in several tight loops via the contents() and contentsAtCall() member functions, and must be kept efficient.
     * When there is no container in a (call,slot), make sure it is removed, we use .contains() to check for containers.
     */
    QHash<const ange::schedule::Call*, QHash<const ange::vessel::Slot*, slot_call_content_t> > m_call_slot_map;

    /**
     * Lookup from (call, stack) -> (*StowageStack) - secondary data, but kept in sync all times
     */
    QHash<const ange::schedule::Call*, QHash<const ange::vessel::Stack*, StowageStack*> > m_stowage_stacks;

    /**
     * Cache for the calculation of bay slices as block weights
     */
    mutable QHash<const ange::schedule::Call*,
            QHash<const ange::vessel::BaySlice*, ange::vessel::BlockWeight> > m_baySlicesBlockWeightCache;
    mutable QHash<const ange::schedule::Call*, bool> m_baySlicesBlockWeightCacheIsDirty;

    // The following three hashes could be merged into one, see ticket #1559
    /**
     * Number of discharge moves in call.
     * Only read from main_statistics_widget_t::update_stats() via discharge_count().
     */
    QHash<const ange::schedule::Call*, int> m_discharge_count;
    /**
     * Number of load moves in call.
     * Only read from main_statistics_widget_t::update_stats() via load_count().
     */
    QHash<const ange::schedule::Call*, int> m_load_count;
    /**
     * Number of planned restow moves in call. (Overstow restows are counted by m_overstowCounter)
     * Only read from main_statistics_widget_t::update_stats() via restow_count().
     */
    QHash<const ange::schedule::Call*, int> m_restow_count;

    PluginManager* m_pluginManager;

    QTimer* m_stowageChangedBuffer;
    QSet<const ange::schedule::Call*> m_changed_buffer;

private:

    friend class stowage_signal_lock_t;

};

#endif // STOWAGE_H
