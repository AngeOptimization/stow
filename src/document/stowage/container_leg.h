#ifndef CONTAINER_LEG_H
#define CONTAINER_LEG_H

#include <QObject>
#include <QHash>

namespace ange {
namespace containers {
class Container;
}
namespace schedule {
class Call;
class Schedule;
}
}

class stowage_t;
/**
 * A class that keeps track of the port of load call and port of discharge destination call of each container
 * (but not intermediate positions)together with sums per source and per destination
 */
class ContainerLeg : public QObject {

    Q_OBJECT

public:
    ContainerLeg(QObject* parent=0);
    /**
     *@return the source call of @param container
     */
    const ange::schedule::Call* portOfLoad(const ange::containers::Container*) const;
    /**
     *@return the destination call of @param container
     */
    const ange::schedule::Call* portOfDischarge(const ange::containers::Container* container) const;
    /**
     * Adds @param container with @param source and @param destination to the internal "list"
     */
    void add_container(const ange::containers::Container* container,const  ange::schedule::Call* source, const ange::schedule::Call* destination);
    /**
     * Remove @param container from the list
     */
    void remove_container(const ange::containers::Container* container);
    /**
     * @return the number of containers with source @param source
     */
    int source_count(const ange::schedule::Call* call) const;
    /**
     * @return the number of containers with destination @param destination
     */
    int destination_count(const ange::schedule::Call* call) const;

Q_SIGNALS:
    /**
     * A container has changed point of load / source. the new position can be queried from this object.
     * In general, you should not connect to this signal, but instead follow dataChanged from container list.
     */
    void containerChangedPOL(const ange::containers::Container* container);

    /**
     * A container has change point of discharge / destination. the new position can be queried from this object.
     * In general, you should not connect to this signal, but instead follow dataChanged from container list.
     */
    void containerChangedPOD(const ange::containers::Container* container);

private:
    /**
     * Validate object against @param schedule
     */
    void validate(ange::schedule::Schedule* schedule) const;

    /**
     * Validate internal state of object
     */
    void validate() const;

    void change_pod(const ange::containers::Container* container, const ange::schedule::Call* new_pod);
    void change_pol(const ange::containers::Container* container, const ange::schedule::Call* new_pol);

private:
    QHash<const ange::containers::Container*,const ange::schedule::Call*> m_containerPOL;
    QHash<const ange::containers::Container*,const ange::schedule::Call*> m_containerPOD;
    QHash<const ange::schedule::Call*,int> m_containersCountPOL;
    QHash<const ange::schedule::Call*,int> m_containersCountPOD;

    friend class stowage_t; // to be able to reach the change_pod and change_pol functions

};

#endif // CONTAINER_LEG_H
