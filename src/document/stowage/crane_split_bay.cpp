#include "crane_split_bay.h"

#include "crane_split.h"
#include <ange/schedule/call.h>

crane_split_bay_t::crane_split_bay_t( int joined_bay, const ange::schedule::Call* call, crane_split_t* crane_split,
                                      bool vessel_twinlifting_above, bool vessel_twinlifting_below) :
    QObject(crane_split),
    m_joinedBay(joined_bay),
    m_crane_split(crane_split),
    m_call(call),
    m_vessel_twinlifting_above(vessel_twinlifting_above),
    m_vessel_twinlifting_below(vessel_twinlifting_below)
{
    setObjectName("CraneSplitBay");
}

double crane_split_bay_t::total_time() const {
  double twinlift_prod_below = m_crane_split->singlelift_productivity(call());
  double twinlift_prod_above = m_crane_split->singlelift_productivity(call());
  if( m_call->twinLifting() == ange::schedule::TwinLiftRules::TwinliftRuleAll ||
    m_call->twinLifting() == ange::schedule::TwinLiftRules::TwinliftRuleUnknown) {
    if(m_vessel_twinlifting_above) {
      twinlift_prod_above =m_crane_split->twinlift_productivity(call());
    }
    if(m_vessel_twinlifting_below) {
      twinlift_prod_below = m_crane_split->twinlift_productivity(call());
    }
  }
  if(m_call->twinLifting() == ange::schedule::TwinLiftRules::TwinliftRuleBelow) {
    if(m_vessel_twinlifting_below) {
      twinlift_prod_below = m_crane_split->twinlift_productivity(call());
    }
  }
  double singlelift_prod = m_crane_split->singlelift_productivity(call());
  double total = 0.0;
  const crane_split_bay_dir_t& di = discharge();
  const crane_split_bay_dir_t& lo = load();
  const crane_split_bay_dir_t& rs = restow();
  total += (di.fourty().below+di.fourty().above) * singlelift_prod +
      (di.twentyAft().above + di.twentyFore().above) * twinlift_prod_above
      +(di.twentyFore().below +di.twentyAft().below) * twinlift_prod_below;
  total += (lo.fourty().above+lo.fourty().below) * singlelift_prod +
    (lo.twentyAft().above+lo.twentyFore().above)* twinlift_prod_above+
    (lo.twentyAft().below +lo.twentyFore().below) * twinlift_prod_below;
  total += (rs.fourty().above+rs.fourty().below) * singlelift_prod +
    (rs.twentyAft().above+rs.twentyFore().above)*twinlift_prod_above+
    (rs.twentyAft().below +rs.twentyFore().below) * twinlift_prod_below;
  return total;
}

int crane_split_bay_t::fourtyMoves() const {
    return m_discharge.fourty().above + m_discharge.fourty().below
            + m_restow.fourty().above + m_restow.fourty().below
            + m_load.fourty().above + m_load.fourty().below;
}
int crane_split_bay_t::twentyAboveAftMoves() const {
    return m_discharge.twentyAft().above + m_load.twentyAft().above + m_restow.twentyAft().above;
}
int crane_split_bay_t::twentyAboveForeMoves() const {
    return m_discharge.twentyFore().above + m_load.twentyFore().above + m_restow.twentyFore().above;
}
int crane_split_bay_t::twentyBelowAftMoves() const {
    return m_discharge.twentyAft().below + m_load.twentyAft().below + m_restow.twentyAft().below;
}
int crane_split_bay_t::twentyBelowForeMoves() const {
    return m_discharge.twentyFore().below + m_load.twentyFore().below + m_restow.twentyFore().below;
}

crane_split_bay_t::MoveStatistics crane_split_bay_t::moveStatistics(crane_split_bin_include_t::crane_split_bin_includes_t include_state) const {
    double singlelift_prod = m_crane_split->singlelift_productivity(call());
    double twinlift_prod_below = m_crane_split->singlelift_productivity(call());
    double twinlift_prod_above = m_crane_split->singlelift_productivity(call());
    bool can_twinlift_above = false;
    bool can_twinlift_below = false;
    double twinlift_move_count_above = 1.0;
    double twinlift_move_count_below = 1.0;
    if(m_call->twinLifting() == ange::schedule::TwinLiftRules::TwinliftRuleAll ||
        m_call->twinLifting() == ange::schedule::TwinLiftRules::TwinliftRuleUnknown){
        if(m_vessel_twinlifting_above) {
            twinlift_prod_above = m_crane_split->twinlift_productivity(call());
            can_twinlift_above = true;
            twinlift_move_count_above = 0.8;
        }
        if(m_vessel_twinlifting_below){
            twinlift_prod_below = m_crane_split->twinlift_productivity(call());
            can_twinlift_below = true;
            twinlift_move_count_below = 0.8;
        }
        } else if (m_call->twinLifting() == ange::schedule::TwinLiftRules::TwinliftRuleBelow) {
            if(m_vessel_twinlifting_below) {
                twinlift_prod_below = m_crane_split->twinlift_productivity(call());
                can_twinlift_below = true;
                twinlift_move_count_below = 0.8;
            }
        }

    int fourty = 0;
    int twentyBelow = 0;
    int twentyAbove = 0;
    int twinliftcount = 0;
    double totalffe = 0.0;
    double time = 0.0;
    switch (include_state) {
        case crane_split_bin_include_t::FORE20_FOURTY_AFT20_BAYS : {
            fourty += fourtyMoves();
            twentyAbove += (twentyAboveAftMoves() + twentyAboveForeMoves());
            twentyBelow += (twentyBelowAftMoves() + twentyBelowForeMoves());
            time += fourtyMoves() * singlelift_prod +
                (twentyAboveAftMoves() + twentyAboveForeMoves()) * twinlift_prod_above +
                (twentyBelowAftMoves() + twentyBelowForeMoves()) * twinlift_prod_below;
            totalffe += fourtyMoves() +
                (twentyAboveAftMoves() + twentyAboveForeMoves()) * twinlift_move_count_above +
                (twentyBelowAftMoves() + twentyBelowForeMoves()) * twinlift_move_count_below;
            if(can_twinlift_above) {
                twinliftcount += (twentyAboveAftMoves() + twentyAboveForeMoves())/2;
            }
            if(can_twinlift_below) {
                twinliftcount += (twentyBelowAftMoves() + twentyBelowForeMoves())/2;
            }
            break;
        }
        case crane_split_bin_include_t::FORE20_BAY : {
            if(!can_twinlift_above) {
                twentyAbove += twentyAboveForeMoves();
                time += twentyAboveForeMoves() * singlelift_prod;
                totalffe += twentyAboveForeMoves();
            }
            if(!can_twinlift_below) {
                twentyBelow += twentyBelowForeMoves();
                time += twentyBelowForeMoves() * singlelift_prod;
                totalffe += twentyBelowForeMoves();
            }
            break;
        }
        case crane_split_bin_include_t::FORE20_FORTY_BAYS : {
            fourty += fourtyMoves();
            time += fourtyMoves() * singlelift_prod;
            totalffe += totalffe;
            if(can_twinlift_above){
                twentyAbove += (twentyAboveForeMoves() + twentyAboveAftMoves() );
                time += (twentyAboveForeMoves() + twentyAboveAftMoves() )* twinlift_prod_above;
                totalffe += (twentyAboveForeMoves() + twentyAboveAftMoves() )* twinlift_move_count_above;
                twinliftcount += (twentyAboveAftMoves() + twentyAboveForeMoves())/2;
            } else {
                twentyAbove += twentyAboveForeMoves() ;
                time += twentyAboveForeMoves() * singlelift_prod;
                totalffe += twentyAboveForeMoves();
            }
            if(can_twinlift_below) {
                twentyBelow += (twentyBelowForeMoves() + twentyBelowAftMoves());
                time += (twentyBelowForeMoves() + twentyBelowAftMoves()) * twinlift_prod_below;
                totalffe += (twentyBelowForeMoves() + twentyBelowAftMoves()) * twinlift_move_count_below;
                twinliftcount += (twentyBelowAftMoves() + twentyBelowForeMoves())/2;
            } else {
                twentyBelow += twentyBelowForeMoves();
                time += twentyBelowForeMoves() * singlelift_prod;
                totalffe += twentyBelowForeMoves();
            }
            break;
        }
        case crane_split_bin_include_t::FOURTY_AFT20_BAYS : {
            fourty += fourtyMoves();
            totalffe += fourtyMoves();
            time += fourtyMoves() * singlelift_prod;
            if(can_twinlift_above){
                twentyAbove += (twentyAboveForeMoves() + twentyAboveAftMoves() );
                time += (twentyAboveForeMoves() + twentyAboveAftMoves() ) * twinlift_prod_above;
                totalffe += (twentyAboveForeMoves() + twentyAboveAftMoves() ) * twinlift_move_count_above;
                twinliftcount += (twentyAboveAftMoves() + twentyAboveForeMoves())/2;
            } else {
                twentyAbove += twentyAboveAftMoves();
                time += twentyAboveAftMoves() * singlelift_prod;
                totalffe += twentyAboveAftMoves();
            }
            if(can_twinlift_below) {
                twentyBelow += (twentyBelowForeMoves() + twentyBelowAftMoves());
                time += (twentyBelowForeMoves() + twentyBelowAftMoves()) * twinlift_prod_below;
                totalffe += (twentyBelowForeMoves() + twentyBelowAftMoves()) * twinlift_move_count_below;
                twinliftcount += (twentyBelowAftMoves() + twentyBelowForeMoves())/2;
            } else {
                twentyBelow += twentyBelowAftMoves();
                time += twentyBelowAftMoves() * singlelift_prod;
                totalffe += twentyBelowAftMoves();
            }
            break;
        }
        case crane_split_bin_include_t::AFT20_BAY : {
            if(!can_twinlift_above) {
                twentyAbove += twentyAboveAftMoves();
                time += twentyAboveAftMoves() * singlelift_prod;
                totalffe += twentyAboveAftMoves();
            }
            if(!can_twinlift_below) {
                twentyBelow += twentyBelowAftMoves();
                time += twentyBelowAftMoves() * singlelift_prod;
                totalffe += twentyBelowAftMoves();
            }
            break;
        }
        case crane_split_bin_include_t::PURE_FOURTY_BAY : {
            fourty += fourtyMoves();
            time += fourtyMoves() * singlelift_prod;
            totalffe += fourtyMoves();
            break;
        }
        case crane_split_bin_include_t::PURE_TWENTY_BAY : {
            twentyAbove += twentyAboveAftMoves();
            twentyBelow += twentyAboveForeMoves();
            twentyAbove += twentyBelowAftMoves();
            twentyBelow += twentyBelowForeMoves();
            time += twentyAboveAftMoves() * singlelift_prod;
            time += twentyBelowAftMoves() * singlelift_prod;
            time += twentyAboveForeMoves() * singlelift_prod;
            time += twentyBelowForeMoves() * singlelift_prod;
            totalffe += twentyAboveAftMoves();
            totalffe += twentyBelowAftMoves();
            totalffe += twentyAboveForeMoves();
            totalffe += twentyBelowForeMoves();
            break;
        }
        case crane_split_bin_include_t::FORE20_AFT20_BAYS : {
            twentyAbove += twentyAboveAftMoves();
            twentyBelow += twentyAboveForeMoves();
            twentyAbove += twentyBelowAftMoves();
            twentyBelow += twentyBelowForeMoves();
            totalffe += twentyAboveAftMoves() *  twinlift_move_count_above;
            totalffe += twentyAboveForeMoves() * twinlift_move_count_above;
            totalffe += twentyBelowAftMoves()  * twinlift_move_count_below;
            totalffe += twentyBelowForeMoves() * twinlift_move_count_below;
            time += twentyAboveAftMoves()*  twinlift_prod_above;
            time += twentyAboveForeMoves() * twinlift_prod_above;
            time += twentyBelowAftMoves()  * twinlift_prod_below;
            time += twentyBelowForeMoves() * twinlift_prod_below;
            twinliftcount += (twentyBelowAftMoves() + twentyBelowForeMoves())/2;
            twinliftcount += (twentyAboveAftMoves() + twentyAboveForeMoves())/2;
            break;
        }
    }
    MoveStatistics ms;
    ms.fourty = fourty;
    ms.twenty = twentyBelow + twentyAbove;
    ms.total = ms.twenty + ms.fourty;
    ms.time = time;
    ms.totalffe = totalffe;
    ms.twinlifts = twinliftcount;
    return ms;
}
#include "crane_split_bay.moc"
