#ifndef STOWAGE_STACK_H
#define STOWAGE_STACK_H

#include <stowplugininterface/istowagestack.h>
#include <stowplugininterface/problem.h>
#include <stowplugininterface/stowtype.h>

#include <ange/units/units.h>
#include <ange/vessel/blockweight.h>
#include <ange/vessel/point3d.h>

#include <QObject>

class visibility_line_t;
class problem_model_t;
class stowage_t;
class QTimer;
namespace ange {
namespace vessel {
class Stack;
class Slot;
class StackSupport;
class VesselTank;
}
namespace containers {
class Container;
}
namespace schedule {
class Call;
}
namespace angelstow {
class Problem;
}
}

class StowageStack : public ange::angelstow::IStowageStack {
  Q_OBJECT
  public:
      StowageStack(const ange::vessel::Stack* stack, const ange::schedule::Call* call, problem_model_t* problem_model,
                   stowage_t* stowage, visibility_line_t* visibilityLine);
    virtual ~StowageStack();

    /**
     * Add container to stack (update height, weights)
     */
    void container_added(const ange::containers::Container* latestContainer, const ange::vessel::Slot* vessel_slot);

    /**
     * Remove container from stack (update height, weights)
     */
    void container_removed(const ange::containers::Container* container, const ange::vessel::Slot* vessel_slot);

    /**
     * @return height of fore stack
     */
    ange::units::Length foreHeight() const {
      return m_heightFwd;
    }

    /**
     * @return height of aft stack
     */
    ange::units::Length aftHeight() const {
      return m_heightAft;
    }

    //FIXME: delete weight* and use block_weights instead
    /**
     * @return weight in fore 20' stack
     */
    ange::units::Mass weight20Fore() const {
      return block_weight_fore20().weight();
    }

    /**
     * @return weight in aft 20' stack
     */
    ange::units::Mass weight20Aft() const {
      return block_weight_aft20().weight();
    }

    /**
     * @return the combined weight of 20' and 40' containers in a stack
     * Used e.g. to determine if the max weight limit on the 40' shoes is exceeded
     */
    ange::units::Mass weight40Aft() const;

    /**
     * @return the combined weight of 20' and 40' containers in a stack
     * Used e.g. to determine if the max weight limit on the 40' shoes is exceeded
     */
    ange::units::Mass weight40Fore() const;

    /**
     * @return the total weight
     */
    ange::units::Mass weightTotal() const;

    /**
     * @return number of slots killed by HC in stack
     */
    int slots_killed(bool fore) const;

    /**
     * @return vessel stack
     */
    const ange::vessel::Stack* vessel_stack() const {
      return m_vessel_stack;
    }
    /**
     * The different types of errors potentially affecting a StowageStack
     */
    enum StowageStackError {
        NO_ERROR = 0,
        OVERHEIGHT = 1,
        OVERWEIGHT = 2,
    };

    /**
     * @return the types of errors affecting the stack if any
     */
    int has_error(bool fore);

    /**
     * @return true if this stack has a warning
     */
    bool has_warning(bool fore);

    /**
     * @return the list of the containers positioned on @param stackSupport
     * Note: stackSupport has to be a valid stack support in the stack.
     */
    QList< const ange::containers::Container* > containersOnSupport(const ange::vessel::StackSupport* stackSupport) const;

    /**
     * @return number of containers in (fore) stack
     **/
    int fore_count() const;

    /**
    * @return number of containers in aft stack
    **/
    int aft_count() const;

    /**
     * @return true if stack is blocking forward view
     **/
    bool is_stack_in_forward_view() const {
      return m_stack_in_forward_view;
    }

    /**
     * @return the max height allowed from visibility line
     */
    ange::units::Length get_visibility_maxheight() const;

    /**
     * @return the blocking angles, i the stack is not blocking this function return null
     */
    QPointF blocking_angles() const;

    /**
     *@return the call for the stowage stack
     */
    const ange::schedule::Call* call() const {
      return m_call;
    }

    /**
    * @return true if container would violate 20-over-40 rule in this position
    */
    bool violates_20_over_40(const ange::vessel::Slot* slot, const ange::containers::Container* container) const;

    /**
     * @return the lowest tier container a 40'+
     */
    int tier_of_lowest_40() const {
      return m_tier_of_lowest_40;
    }

    /**
    * @return the highest tier container a 20
    */
    int tier_of_highest_20() const {
      return std::max(m_tier_of_highest_20_fore,m_tier_of_highest_20_aft);
    }

    /**
    * @return the highest tier container a 20 in fore stack
    */
    int tier_of_highest_20_fore() const {
      return m_tier_of_highest_20_fore;
    }

   /**
    * @return the highest tier container a 20 in aft stack
    */
    int tier_of_highest_20_aft() const {
      return m_tier_of_highest_20_aft;
    }

    /**
    * @return the lowest tier container a 20'
    */
    int tier_of_lowest_20() const {
      return std::min(m_tier_of_lowest_20_fore,m_tier_of_lowest_20_aft);
    }

    /**
    * @return the highest tier container a 40'+
    */
    int tier_of_highest_40() const {
      return m_tier_of_highest_40;
    }

    /**
     * @return the tier of the lowest empty slot in the fore stack
     */
    int tier_of_lowest_empty_slot_fore() const {
      return m_tier_of_lowest_void_fore;
    }

    /**
     * @return the tier of the lowest empty slot in the aft stack
     */
    int tier_of_lowest_empty_slot_aft() const {
      return m_tier_of_lowest_void_aft;
    }

    /**
     * @return the center of mass for the 20 aft stack
     */
    ange::vessel::BlockWeight block_weight_fore20() const;

    /**
     * @return the center of mass for the 20 fore stack
     */
    ange::vessel::BlockWeight block_weight_aft20() const;

    /**
     * @return the center of mass for the 20 aft stack1
     */
    ange::vessel::BlockWeight block_weight_40() const;

    /**
     * @return the combined block weight of all 3 stacks
     */
    ange::vessel::BlockWeight block_weight_all() const;

    /**
     * @return whether @param stackSupport with @param currentHeight can accomodate @param heightToBeAccomodated.
     * If heightToBeAccomodated is 0, the function returns whether its current configuration is legal
     */
    static bool canAccomodateHeight(const ange::vessel::StackSupport* stackSupport, ange::units::Length currentHeight,
                                    ange::units::Length heightToBeAccomodated);

    /**
     * @return the block weight for a given container
     */
    ange::vessel::BlockWeight containerBlockWeight(const ange::containers::Container* container) const;

  public Q_SLOTS:
    void check_visibility_violations(const ange::schedule::Call* call);
  private Q_SLOTS:
    void update_visibility();
  Q_SIGNALS:
    void changed();
  private:
    const stowage_t* m_stowage;
    visibility_line_t* m_visibilityLine;
    const ange::vessel::Stack* m_vessel_stack;
    const ange::schedule::Call* m_call;
    problem_model_t* m_problemModel;
    int m_tier_of_highest_20_fore;
    int m_tier_of_highest_20_aft;
    int m_tier_of_highest_40;
    int m_tier_of_lowest_20_fore;
    int m_tier_of_lowest_20_aft;
    int m_tier_of_lowest_40;
    int m_tier_of_lowest_void_fore;
    int m_tier_of_lowest_void_aft;
    bool m_pending_signal;
    QScopedPointer<ange::angelstow::Problem> m_weightProblem20Aft;
    QScopedPointer<ange::angelstow::Problem> m_weightProblem20Fwd;
    QScopedPointer<ange::angelstow::Problem> m_weightProblem40;
    QScopedPointer<ange::angelstow::Problem> m_heightProblemAft;
    QScopedPointer<ange::angelstow::Problem> m_heightProblemFwd;
    QScopedPointer<ange::angelstow::Problem> m_20_over_40_problem;
    QScopedPointer<ange::angelstow::Problem> m_russian_stow_problem;
    QScopedPointer<ange::angelstow::Problem> m_fore_ineffective_height;
    QScopedPointer<ange::angelstow::Problem> m_aft_ineffective_height;
    QScopedPointer<ange::angelstow::Problem> m_fore_visibility_height_warning;
    QScopedPointer<ange::angelstow::Problem> m_aft_visibility_height_warning;
    QScopedPointer<ange::angelstow::Problem> m_hanging_container_problem;
    QTimer* m_stowage_stack_timer;

    bool m_stack_in_forward_view;
    typedef QPair<double, double> span_t;
    span_t m_view_blocking_angles;

    ange::angelstow::Problem* report_problem(ange::angelstow::Problem::Severity severity, const QString& description, const ange::vessel::StackSupport* stack_support, const QString& details);

    /**
     * Verify whether the container added causes 20-over-40 problems, or causes russian stowage problems.
     */
    void check_20_40_mixing_add(const ange::containers::Container* container, const ange::vessel::Slot* slot);

    /**
     * Verify whether the container added removes 20-over-40 problems, or causes russian stowage problems
     */
    void check_20_40_mixing_remove(const ange::containers::Container* container, const ange::vessel::Slot* vessel_slot);

    void updateProblems();
    /**
     * all calculations done here. The cache is simply recalculated when containers are added or removed as we need the
     * results immediately after the insertion/removal.
     */
    void updateCache() const;
    mutable ange::units::Length m_heightAft;
    mutable ange::units::Length m_heightFwd;
    mutable ange::vessel::BlockWeight m_blockWeight20Aft;
    mutable ange::vessel::BlockWeight m_blockWeight20Fwd;
    mutable ange::vessel::BlockWeight m_blockWeight40;
    mutable QMap< int, const ange::containers::Container* > m_containerList20Aft;
    mutable QMap< int, const ange::containers::Container* > m_containerList20Fwd;
    mutable QMap< int, const ange::containers::Container* > m_containerList40;
    mutable QMap<const ange::containers::Container*, ange::vessel::BlockWeight> m_containerBlockWeight;

};

#endif // STOWAGE_STACK_H
