/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010

 Copyright: See COPYING file that comes with this distribution

*/

#ifndef CRANE_SPLIT_BIN_H
#define CRANE_SPLIT_BIN_H
#include <QList>
#include <QHash>
#include <document/stowage/crane_split_bin_include.h>
#include "crane_split_bay.h"
class crane_split_bay_t;
class crane_split_t;
namespace ange {
namespace vessel {
class BaySlice;
}
namespace schedule {
class Call;
}
}

class crane_split_bin_t {
  public:
    crane_split_bin_t(const crane_split_t* crane_split,
                  QList< QPair<crane_split_bay_t*,crane_split_bin_include_t::crane_split_bin_includes_t> > bay_slices,
                  int max_cranes = 1, bool superbin = false);

    /**
     * @return number of cranes permitted in this bin (at once) (Usually 1)
     */
    int max_cranes() const;

    /**
     * @return the aggregated move count for the bin
     */
    crane_split_bay_t::MoveStatistics moveStatistics() const;

    /**
     * @return components of this bin.
     */
    QList< QPair<crane_split_bay_t*,crane_split_bin_include_t::crane_split_bin_includes_t> > bays() const {
      return m_crane_split_bays;
    }

    /**
     * @return true if the bin is a super bin
     */
    bool superbin() const {
      return m_superbin;
    }

  private:
    QList< QPair<crane_split_bay_t*,crane_split_bin_include_t::crane_split_bin_includes_t> > m_crane_split_bays;
    int m_max_cranes;
    const crane_split_t* m_crane_split;
    bool m_superbin;
};

#endif // CRANE_SPLIT_BIN_H
