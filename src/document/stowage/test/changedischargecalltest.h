#ifndef CHANGEDISCHARGECALLTEST_H
#define CHANGEDISCHARGECALLTEST_H

#include <QObject>

/**
 * Testing change of disharge port
 */
class ChangeDischargeCallTest : public QObject {
    Q_OBJECT

private Q_SLOTS:
    void movePodInSchedule();
    void shortenTripOfUnplacedContainer();
    void shortenTripOfContainerWithShortStow();
    void overlappingExtensionOfTrip();
    void partialExtensionNotDone();

};

#endif // CHANGEDISCHARGECALLTEST_H
