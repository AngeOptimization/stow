#include "changedischargecalltest.h"

#include "test/util/vesseltestutils.h"

#include "document/document.h"
#include "document/undo/schedule_change_command.h"
#include <document/undo/containerchanger.h>
#include <document/undo/container_list_change_command.h>
#include <document/undo/container_stow_command.h>
#include <document/stowage/stowage.h>
#include <ange/containers/container.h>
#include <ange/vessel/vessel.h>
#include <QTest>
#include <ange/schedule/schedule.h>
#include <document/stowage/container_move.h>
#include <document/stowage/container_leg.h>
#include <document/containers/container_list.h>
#include <problemkeeper.h>
#include <problem_model.h>
#include <fileopener.h>
#include <ange/vessel/slot.h>

using namespace ange::vessel;
using namespace ange::containers;
using namespace ange::schedule;
using namespace ange::units;

void ChangeDischargeCallTest::movePodInSchedule() {
    document_t* document = FileOpener::openStoFile(QFINDTESTDATA("test-data/tiny-test.sto")).document;
    stowage_t* stowage = document->stowage();
    Schedule* schedule = document->schedule();
    const Slot* slot = document->vessel()->slotAt(BayRowTier(3,0,2));
    const Container* container = stowage->containers_in_slot(slot).at(0);

    const Call* call2 = schedule->calls().at(2);
    const Call* callAfter = schedule->calls().last();
    QVERIFY(call2 != callAfter);
    {
        QVERIFY(stowage->container_routes()->portOfDischarge(container) == call2);
        const container_move_t* moveIn2 = stowage->container_moved_at_call(container, call2);
        QVERIFY(moveIn2);
        QVERIFY(!moveIn2->slot());
        QVERIFY(stowage->container_routes()->portOfDischarge(container) != callAfter);
        const container_move_t* moveInAfter = stowage->container_moved_at_call(container, callAfter);
        QVERIFY(!moveInAfter);
    }
    {
        // Move discharge call to later (call After)
        QSharedPointer<ChangerResult> result = ContainerChanger::changeDischargeCall(document, container, callAfter);
        QVERIFY(result->problemsGenerated() == 0);
        document->undostack()->push(result->takeCommand());

        QVERIFY(stowage->container_routes()->portOfDischarge(container) != call2);
        const container_move_t* moveIn2 = stowage->container_moved_at_call(container, call2);
        QVERIFY(!moveIn2);
        QVERIFY(stowage->container_routes()->portOfDischarge(container) == callAfter);
        const container_move_t* moveInAfter = stowage->container_moved_at_call(container, callAfter);
        QVERIFY(moveInAfter);
        QVERIFY(!moveInAfter->slot());
    }
    {
        // Move discharge call to earlier (call 2)
        QSharedPointer<ChangerResult> result = ContainerChanger::changeDischargeCall(document, container, call2);
        QVERIFY(result->problemsGenerated() == 0);
        document->undostack()->push(result->takeCommand());

        QVERIFY(stowage->container_routes()->portOfDischarge(container) == call2);
        const container_move_t* moveIn2 = stowage->container_moved_at_call(container, call2);
        QVERIFY(moveIn2);
        QVERIFY(!moveIn2->slot());
        QVERIFY(stowage->container_routes()->portOfDischarge(container) != callAfter);
        const container_move_t* moveInAfter = stowage->container_moved_at_call(container, callAfter);
        QVERIFY(!moveInAfter);
    }

    delete document;
}

void ChangeDischargeCallTest::shortenTripOfUnplacedContainer() {
    document_t* document = FileOpener::openStoFile(QFINDTESTDATA("test-data/tiny-test.sto")).document;
    stowage_t* stowage = document->stowage();
    Schedule* schedule = document->schedule();

    int containerId;
    {
        Container container(EquipmentNumber(""), 0 * kilogram, ange::containers::IsoCode("4200"));
        containerId = container.id();
        container_list_change_command_t* command1 = new container_list_change_command_t(document);
        QCOMPARE(schedule->size(), 4);
        command1->add_container(&container, schedule->at(0), schedule->at(3));
        document->undostack()->push(command1);
    }

    Container* container = document->containers()->get_container_by_id(containerId);

    QCOMPARE(stowage->container_routes()->portOfDischarge(container), schedule->at(3));
    QCOMPARE(stowage->moves(container).size(), 0);

    QSharedPointer<ChangerResult> result = ContainerChanger::changeDischargeCall(document, container, schedule->at(2));
    QCOMPARE(result->problemsGenerated(), 0);
    document->undostack()->push(result->takeCommand());

    QCOMPARE(stowage->container_routes()->portOfDischarge(container), schedule->at(2));
    QCOMPARE(stowage->moves(container).size(), 0);

    delete document;
}

void ChangeDischargeCallTest::shortenTripOfContainerWithShortStow() {
    document_t* document = FileOpener::openStoFile(QFINDTESTDATA("test-data/tiny-test.sto")).document;
    stowage_t* stowage = document->stowage();
    Schedule* schedule = document->schedule();
    const Slot* slot = document->vessel()->slotAt(BayRowTier(3,0,2));

    const Container* oldContainer = stowage->containers_in_slot(slot).at(0);
    document->undostack()->push((new container_stow_command_t(document))->clearMoves(oldContainer));
    QCOMPARE(stowage->moves(oldContainer).size(), 0);

    int containerId;
    {
        Container container(EquipmentNumber(""), 0 * kilogram, ange::containers::IsoCode("2200"));
        containerId = container.id();
        container_list_change_command_t* command = new container_list_change_command_t(document);
        QCOMPARE(schedule->size(), 4);
        command->add_container(&container, schedule->at(0), schedule->at(3));
        document->undostack()->push(command);
    }

    Container* container = document->containers()->get_container_by_id(containerId);

    document->undostack()->push((new container_stow_command_t(document))->add_load(
        container, slot, schedule->at(0), ange::angelstow::MicroStowedType)->add_discharge(
            container, schedule->at(1), ange::angelstow::MicroStowedType));

    QCOMPARE(stowage->container_routes()->portOfDischarge(container), schedule->at(3));
    QCOMPARE(stowage->moves(container).size(), 2);
    QCOMPARE(stowage->container_moved_at_call(container, schedule->at(0))->slot(), slot);
    QCOMPARE(stowage->container_moved_at_call(container, schedule->at(1))->slot(), (void*)0);

    QSharedPointer<ChangerResult> result = ContainerChanger::changeDischargeCall(document, container, schedule->at(2));
    QCOMPARE(result->problemsGenerated(), 0);
    document->undostack()->push(result->takeCommand());

    QCOMPARE(stowage->container_routes()->portOfDischarge(container), schedule->at(2));
    QCOMPARE(stowage->moves(container).size(), 2);
    QCOMPARE(stowage->container_moved_at_call(container, schedule->at(0))->slot(), slot);
    QVERIFY(stowage->container_moved_at_call(container, schedule->at(1)));
    QCOMPARE(stowage->container_moved_at_call(container, schedule->at(1))->slot(), (void*)0);

    delete document;
}

void ChangeDischargeCallTest::overlappingExtensionOfTrip() {
    Vessel* vessel = loadVessel(QFINDTESTDATA("test-data/single-stack.json"));
    Call* callA = new Call("A", "");
    Call* callB = new Call("B", "");
    Call* callC = new Call("C", "");
    Call* callD = new Call("D", "");
    Schedule* schedule = new Schedule(QList<Call*>() << callA << callB << callC << callD);
    schedule->setPivotCall(schedule->size()-1);  // needed for running schedulemodel with modeltest
    stowage_t* stowage = new stowage_t(schedule);
    document_t* document = new document_t(schedule, vessel, stowage);

    const Slot* slot = vessel->slotAt(BayRowTier(2, 0, 80));
    QVERIFY(slot);

    Container* container1 = createContainerOnboard(document, callA, callB, slot);
    Container* container2 = createContainerOnboard(document, callC, callD, slot);
    Q_UNUSED(container2);
    QVERIFY(stowage->container_moved_at_call(container1, callB));
    QCOMPARE(stowage->container_moved_at_call(container1, callB)->slot(), (void*)0);
    QVERIFY(!stowage->container_moved_at_call(container1, callC));
    QCOMPARE(document->problem_model()->rowCount(), 0);

    {
        QSharedPointer<ChangerResult> result = ContainerChanger::changeDischargeCall(document, container1, callC);
        QCOMPARE(result.data()->problemsGenerated(), 0);
        document->undostack()->push(result.data()->takeCommand());
    }
    QVERIFY(!stowage->container_moved_at_call(container1, callB));
    QVERIFY(stowage->container_moved_at_call(container1, callC));
    QCOMPARE(stowage->container_moved_at_call(container1, callC)->slot(), (void*)0);
    QCOMPARE(document->problem_model()->rowCount(), 0);

    {
        QSharedPointer<ChangerResult> result = ContainerChanger::changeDischargeCall(document, container1, callD);
        QCOMPARE(result.data()->problemsGenerated(), 1);
        document->undostack()->push(result.data()->takeCommand());
    }
    QVERIFY(!stowage->container_moved_at_call(container1, callB));
    QVERIFY(stowage->container_moved_at_call(container1, callC));
    QCOMPARE(stowage->container_moved_at_call(container1, callC)->slot(), (void*)0);
    QCOMPARE(document->problem_model()->rowCount(), 1);
    QModelIndex parentIndex = document->problem_model()->index(0, 0);
    QModelIndex problemDetailsIndex = document->problem_model()->index(0, problem_model_t::DETAILS, parentIndex);
    QCOMPARE(document->problem_model()->data(problemDetailsIndex).toString(),
             QString("Could not extend trip from C to D"));

    delete document;
}

/*
 * This tests that the extension of trip A->B is not half done as A->C when trying to extend to A->D
 * with C->D overlapped.
 */
void ChangeDischargeCallTest::partialExtensionNotDone() {
    Vessel* vessel = loadVessel(QFINDTESTDATA("test-data/single-stack.json"));
    Call* callA = new Call("A", "");
    Call* callB = new Call("B", "");
    Call* callC = new Call("C", "");
    Call* callD = new Call("D", "");
    Schedule* schedule = new Schedule(QList<Call*>() << callA << callB << callC << callD);
    schedule->setPivotCall(schedule->size()-1); // needed for running schedulemodel with modeltest
    stowage_t* stowage = new stowage_t(schedule);
    document_t* document = new document_t(schedule, vessel, stowage);

    const Slot* slot = vessel->slotAt(BayRowTier(2, 0, 80));
    QVERIFY(slot);

    Container* container1 = createContainerOnboard(document, callA, callB, slot);
    Container* container2 = createContainerOnboard(document, callC, callD, slot);
    Q_UNUSED(container2);
    QVERIFY(stowage->container_moved_at_call(container1, callB));
    QCOMPARE(stowage->container_moved_at_call(container1, callB)->slot(), (void*)0);
    QVERIFY(!stowage->container_moved_at_call(container1, callC));
    QCOMPARE(document->problem_model()->rowCount(), 0);

    {
        QSharedPointer<ChangerResult> result = ContainerChanger::changeDischargeCall(document, container1, callD);
        QCOMPARE(result.data()->problemsGenerated(), 1);
        document->undostack()->push(result.data()->takeCommand());
    }
    QVERIFY(stowage->container_moved_at_call(container1, callB));
    QCOMPARE(stowage->container_moved_at_call(container1, callB)->slot(), (void*)0);
    QVERIFY(!stowage->container_moved_at_call(container1, callC));
    QCOMPARE(document->problem_model()->rowCount(), 1);
    QModelIndex parentIndex = document->problem_model()->index(0, 0);
    QModelIndex problemDetailsIndex = document->problem_model()->index(0, problem_model_t::DETAILS, parentIndex);
    QCOMPARE(document->problem_model()->data(problemDetailsIndex).toString(),
             QString("Could not extend trip from B to D"));

    delete document;
}

QTEST_GUILESS_MAIN(ChangeDischargeCallTest);

#include "changedischargecalltest.moc"
