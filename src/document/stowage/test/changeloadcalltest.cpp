#include "changeloadcalltest.h"

#include "test/util/vesseltestutils.h"

#include <QTest>
#include <stowage.h>
#include <container_move.h>
#include <document.h>
#include <container_stow_command.h>
#include <container_list_change_command.h>
#include <containerchanger.h>
#include <problem_model.h>
#include <container_list.h>
#include <ange/units/units.h>
#include <ange/containers/equipmentnumber.h>
#include <ange/containers/isocode.h>
#include <ange/vessel/bayrowtier.h>
#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>
#include <ange/containers/container.h>
#include <ange/vessel/vessel.h>

using namespace ange::vessel;
using namespace ange::containers;
using namespace ange::schedule;
using namespace ange::units;

void ChangeLoadCallTest::shortenTrip() {
    Vessel* vessel = loadVessel(QFINDTESTDATA("test-data/single-stack.json"));
    Call* callA = new Call("A", "");
    Call* callB = new Call("B", "");
    Call* callC = new Call("C", "");
    Call* callD = new Call("D", "");
    Call* callE = new Call("E", "");
    Call* callF = new Call("F", "");
    Call* callG = new Call("G", "");
    Schedule* schedule = new Schedule(QList<Call*>() << callA << callB << callC << callD << callE << callF << callG);
    schedule->setPivotCall(schedule->size()-1); // needed for running schedulemodel with modeltest
    stowage_t* stowage = new stowage_t(schedule);
    document_t* document = new document_t(schedule, vessel, stowage);

    const Slot* slot1 = vessel->slotAt(BayRowTier(2, 0, 80));
    QVERIFY(slot1);
    const Slot* slot2 = vessel->slotAt(BayRowTier(2, 0, 82));
    QVERIFY(slot2);

    Container* container = createContainerOnboard(document, callA, callG, slot1);
    QVERIFY(stowage->container_moved_at_call(container, callA));
    QVERIFY(stowage->container_moved_at_call(container, callG));

    document->undostack()->push((new container_stow_command_t(document))->clearMoves(container));
    QVERIFY(!stowage->container_moved_at_call(container, callA));
    QVERIFY(!stowage->container_moved_at_call(container, callG));

    document->undostack()->push((new container_stow_command_t(document))->addMoves(container, QList<container_move_t>()
        << container_move_t(container, slot1, callB, ange::angelstow::MicroStowedType)
        << container_move_t(container, slot2, callD, ange::angelstow::MicroStowedType)
        << container_move_t(container, 0, callF, ange::angelstow::MicroStowedType)));
    QVERIFY(!stowage->container_moved_at_call(container, callA));
    QVERIFY(!stowage->container_moved_at_call(container, callG));
    QVERIFY(stowage->container_moved_at_call(container, callB));
    QCOMPARE(stowage->container_moved_at_call(container, callB)->slot(), slot1);
    QVERIFY(stowage->container_moved_at_call(container, callD));
    QCOMPARE(stowage->container_moved_at_call(container, callD)->slot(), slot2);
    QVERIFY(stowage->container_moved_at_call(container, callF));
    QCOMPARE(stowage->container_moved_at_call(container, callF)->slot(), (void*)0);

    {
        QSharedPointer<ChangerResult> result = ContainerChanger::changeLoadCall(document, container, callB);
        QCOMPARE(result.data()->problemsGenerated(), 0);
        document->undostack()->push(result->takeCommand());
    }
    QVERIFY(stowage->container_moved_at_call(container, callB));
    QCOMPARE(stowage->container_moved_at_call(container, callB)->slot(), slot1);

    {
        QSharedPointer<ChangerResult> result = ContainerChanger::changeLoadCall(document, container, callC);
        QCOMPARE(result.data()->problemsGenerated(), 0);
        document->undostack()->push(result->takeCommand());
    }
    QVERIFY(stowage->container_moved_at_call(container, callC));
    QCOMPARE(stowage->container_moved_at_call(container, callC)->slot(), slot1);

    {
        QSharedPointer<ChangerResult> result = ContainerChanger::changeLoadCall(document, container, callD);
        QCOMPARE(result.data()->problemsGenerated(), 0);
        document->undostack()->push(result->takeCommand());
    }
    QVERIFY(stowage->container_moved_at_call(container, callD));
    QCOMPARE(stowage->container_moved_at_call(container, callD)->slot(), slot2);

    {
        QSharedPointer<ChangerResult> result = ContainerChanger::changeLoadCall(document, container, callE);
        QCOMPARE(result.data()->problemsGenerated(), 0);
        document->undostack()->push(result->takeCommand());
    }
    QVERIFY(stowage->container_moved_at_call(container, callE));
    QCOMPARE(stowage->container_moved_at_call(container, callE)->slot(), slot2);

    {
        QSharedPointer<ChangerResult> result = ContainerChanger::changeLoadCall(document, container, callF);
        QCOMPARE(result.data()->problemsGenerated(), 0);
        document->undostack()->push(result->takeCommand());
    }
    QVERIFY(!stowage->container_moved_at_call(container, callF));
}

void ChangeLoadCallTest::overlappingExtensionOfTrip() {
    Vessel* vessel = loadVessel(QFINDTESTDATA("test-data/single-stack.json"));
    Call* callA = new Call("A", "");
    Call* callB = new Call("B", "");
    Call* callC = new Call("C", "");
    Call* callD = new Call("D", "");
    Schedule* schedule = new Schedule(QList<Call*>() << callA << callB << callC << callD);
    schedule->setPivotCall(schedule->size()-1); // needed for when running ModelTest with schedule model
    stowage_t* stowage = new stowage_t(schedule);
    document_t* document = new document_t(schedule, vessel, stowage);

    const Slot* slot = vessel->slotAt(BayRowTier(2, 0, 80));
    QVERIFY(slot);

    Container* container1 = createContainerOnboard(document, callA, callB, slot);
    Q_UNUSED(container1);
    Container* container2 = createContainerOnboard(document, callC, callD, slot);
    QVERIFY(!stowage->container_moved_at_call(container2, callB));
    QVERIFY(stowage->container_moved_at_call(container2, callC));
    QCOMPARE(stowage->container_moved_at_call(container2, callC)->slot(), slot);
    QCOMPARE(document->problem_model()->rowCount(), 0);

    {
        QSharedPointer<ChangerResult> result = ContainerChanger::changeLoadCall(document, container2, callB);
        QCOMPARE(result.data()->problemsGenerated(), 0);
        document->undostack()->push(result->takeCommand());
    }
    QVERIFY(stowage->container_moved_at_call(container2, callB));
    QCOMPARE(stowage->container_moved_at_call(container2, callB)->slot(), slot);
    QVERIFY(!stowage->container_moved_at_call(container2, callC));
    QCOMPARE(document->problem_model()->rowCount(), 0);

    {
        QSharedPointer<ChangerResult> result = ContainerChanger::changeLoadCall(document, container2, callA);
        QCOMPARE(result.data()->problemsGenerated(), 1);
        document->undostack()->push(result.data()->takeCommand());
    }
    QVERIFY(stowage->container_moved_at_call(container2, callB));
    QCOMPARE(stowage->container_moved_at_call(container2, callB)->slot(), slot);
    QVERIFY(!stowage->container_moved_at_call(container2, callC));
    QCOMPARE(document->problem_model()->rowCount(), 1);
    QModelIndex parentIndex = document->problem_model()->index(0, 0);
    QModelIndex problemDetailsIndex = document->problem_model()->index(0, problem_model_t::DETAILS, parentIndex);
    QCOMPARE(document->problem_model()->data(problemDetailsIndex).toString(),
             QString("Could not forward extend trip from B to A"));

    delete document;
}

QTEST_GUILESS_MAIN(ChangeLoadCallTest);

#include "changeloadcalltest.moc"
