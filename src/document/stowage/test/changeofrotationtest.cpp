#include <QTest>

#include "container_move.h"
#include "document.h"
#include "fileopener.h"
#include "schedulechangerotationcommand.h"
#include "stowage.h"

#include <ange/containers/container.h>
#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>
#include <ange/vessel/vessel.h>

using namespace ange::containers;
using namespace ange::schedule;
using namespace ange::vessel;

class ChangeOfRotationTest : public QObject {
    Q_OBJECT
private Q_SLOTS:
    void changeOfRotationTest();
    void changeOfRotationTest2();
    void changeOfRotationTest3();
};

QTEST_GUILESS_MAIN(ChangeOfRotationTest);

void ChangeOfRotationTest::changeOfRotationTest() {
    // schedule is befor, debrv, nlrot, after
    // slot 1 contains a container from before => debrv, slot 2 contains a container from nlrot->after
    // debrv and nlrot gets swapped
    // schedule is now befor, nlrot, debrv, after
    // slot1 is now expected to have container1 in befor and nlrot
    // slot2 is expected to have container2 in nlrot and debrv
    document_t* document = FileOpener::openStoFile(QFINDTESTDATA("test-data/cor-test.sto")).document;
    Schedule* schedule = document->schedule();
    const Slot* slot1 = document->vessel()->slotAt(BayRowTier(2,2,80));
    const Slot* slot2 = document->vessel()->slotAt(BayRowTier(2,1,80));
    QVERIFY(slot1);
    QVERIFY(slot2);
    const Call* befor = schedule->at(0);
    QCOMPARE(befor->uncode(), QStringLiteral("Befor"));
    const Call* debrv = schedule->at(1);
    QCOMPARE(debrv->uncode(), QStringLiteral("DEBRV"));
    const Call* nlrot = schedule->at(2);
    QCOMPARE(nlrot->uncode(), QStringLiteral("NLROT"));
    const Call* after = schedule->at(3);
    QCOMPARE(after->uncode(), QStringLiteral("After"));
    const Container* container1 = document->stowage()->contents(slot1, befor).container();
    const Container* container2 = document->stowage()->contents(slot2, nlrot).container();
    QVERIFY(container1);
    QVERIFY(container2);
    QCOMPARE(QString(container1->equipmentNumber()), QString("ZZZU5698360"));
    QCOMPARE(QString(container2->equipmentNumber()), QString("ZZZU5698361"));

    QCOMPARE(document->stowage()->container_position(container1, befor), slot1);
    QCOMPARE(document->stowage()->container_position(container1, debrv), (Slot*)0);
    QCOMPARE(document->stowage()->container_position(container1, nlrot), (Slot*)0);
    QCOMPARE(document->stowage()->container_position(container1, after), (Slot*)0);
    QCOMPARE(document->stowage()->container_position(container2, befor), (Slot*)0);
    QCOMPARE(document->stowage()->container_position(container2, debrv), (Slot*)0);
    QCOMPARE(document->stowage()->container_position(container2, nlrot), slot2);
    QCOMPARE(document->stowage()->container_position(container2, after), (Slot*)0);

    QCOMPARE(document->stowage()->contents(slot1, befor).container(), container1);
    QCOMPARE(document->stowage()->contents(slot1, debrv).container(), (Container*)0);
    QCOMPARE(document->stowage()->contents(slot1, nlrot).container(), (Container*)0);
    QCOMPARE(document->stowage()->contents(slot1, after).container(), (Container*)0);
    QCOMPARE(document->stowage()->contents(slot2, befor).container(), (Container*)0);
    QCOMPARE(document->stowage()->contents(slot2, debrv).container(), (Container*)0);
    QCOMPARE(document->stowage()->contents(slot2, nlrot).container(), container2);
    QCOMPARE(document->stowage()->contents(slot2, after).container(), (Container*)0);

    QCOMPARE(document->stowage()->changeOfRotationIsLegal(debrv, -1), QString("Container ZZZU5698360 must be discharged before it's loaded"));
    QCOMPARE(document->stowage()->changeOfRotationIsLegal(debrv, +1), QString());
    QCOMPARE(document->stowage()->changeOfRotationIsLegal(debrv, +2), QString());

    QCOMPARE(document->stowage()->changeOfRotationIsLegal(nlrot, -2), QString());
    QCOMPARE(document->stowage()->changeOfRotationIsLegal(nlrot, -1), QString());
    QCOMPARE(document->stowage()->changeOfRotationIsLegal(nlrot, +1), QString("Container ZZZU5698361 must be loaded before it's discharged"));

    QCOMPARE(document->schedule()->at(1), debrv);
    QCOMPARE(document->schedule()->at(2), nlrot);
    ScheduleChangeRotationCommand* corCommand = ScheduleChangeRotationCommand::changeOfRotation(schedule, 1, 2);
    document->undostack()->push(corCommand);
    QCOMPARE(document->schedule()->calls().at(1)->uncode(), QString("NLROT"));
    QCOMPARE(document->schedule()->calls().at(1), nlrot);
    QCOMPARE(document->schedule()->calls().at(2)->uncode(), QString("DEBRV"));
    QCOMPARE(document->schedule()->calls().at(2), debrv);

    QCOMPARE(document->stowage()->container_position(container1, befor), slot1);
    QCOMPARE(document->stowage()->container_position(container1, nlrot), slot1);
    QCOMPARE(document->stowage()->container_position(container1, debrv), (Slot*)0);
    QCOMPARE(document->stowage()->container_position(container1, after), (Slot*)0);
    QCOMPARE(document->stowage()->container_position(container2, befor), (Slot*)0);
    QCOMPARE(document->stowage()->container_position(container2, nlrot), slot2);
    QCOMPARE(document->stowage()->container_position(container2, debrv), slot2);
    QCOMPARE(document->stowage()->container_position(container2, after), (Slot*)0);

    QCOMPARE(document->stowage()->contents(slot1, befor).container(), container1);
    QCOMPARE(document->stowage()->contents(slot1, nlrot).container(), container1);
    QCOMPARE(document->stowage()->contents(slot1, debrv).container(), (Container*)0);
    QCOMPARE(document->stowage()->contents(slot1, after).container(), (Container*)0);
    QCOMPARE(document->stowage()->contents(slot2, befor).container(), (Container*)0);
    QCOMPARE(document->stowage()->contents(slot2, nlrot).container(), container2);
    QCOMPARE(document->stowage()->contents(slot2, debrv).container(), container2);
    QCOMPARE(document->stowage()->contents(slot2, after).container(), (Container*)0);

    QCOMPARE(document->stowage()->contents(slot1, nlrot).container()->equipmentNumber(), EquipmentNumber("ZZZU5698360"));
    QCOMPARE(document->stowage()->contents(slot2, nlrot).container()->equipmentNumber(), EquipmentNumber("ZZZU5698361"));

    document->undostack()->undo();

    // check that everything is like before

    QCOMPARE(document->schedule()->at(1), debrv);
    QCOMPARE(document->schedule()->at(2), nlrot);
    QCOMPARE(document->schedule()->calls().at(1)->uncode(), QString("DEBRV"));
    QCOMPARE(document->schedule()->calls().at(2)->uncode(), QString("NLROT"));

    QCOMPARE(document->stowage()->contents(slot1, befor).container(), container1);
    QCOMPARE(document->stowage()->contents(slot1, debrv).container(), (Container*)0);
    QCOMPARE(document->stowage()->contents(slot1, nlrot).container(), (Container*)0);
    QCOMPARE(document->stowage()->contents(slot1, after).container(), (Container*)0);
    QCOMPARE(document->stowage()->contents(slot2, befor).container(), (Container*)0);
    QCOMPARE(document->stowage()->contents(slot2, debrv).container(), (Container*)0);
    QCOMPARE(document->stowage()->contents(slot2, nlrot).container(), container2);
    QCOMPARE(document->stowage()->contents(slot2, after).container(), (Container*)0);

    QCOMPARE(document->stowage()->container_position(container1, befor), slot1);
    QCOMPARE(document->stowage()->container_position(container1, debrv), (Slot*)0);
    QCOMPARE(document->stowage()->container_position(container1, nlrot), (Slot*)0);
    QCOMPARE(document->stowage()->container_position(container1, after), (Slot*)0);
    QCOMPARE(document->stowage()->container_position(container2, befor), (Slot*)0);
    QCOMPARE(document->stowage()->container_position(container2, debrv), (Slot*)0);
    QCOMPARE(document->stowage()->container_position(container2, nlrot), slot2);
    QCOMPARE(document->stowage()->container_position(container2, after), (Slot*)0);

    delete document;
}

void ChangeOfRotationTest::changeOfRotationTest2() {
    document_t* document = FileOpener::openStoFile(QFINDTESTDATA("test-data/cor-test-2.sto")).document;
    Schedule* schedule = document->schedule();
    const Call* callA = schedule->at(1);
    QCOMPARE(callA->uncode(), QStringLiteral("AAAAA"));
    const Call* callB = schedule->at(2);
    QCOMPARE(callB->uncode(), QStringLiteral("BBBBB"));
    const Call* callC = schedule->at(3);
    QCOMPARE(callC->uncode(), QStringLiteral("CCCCC"));
    const Call* callD = schedule->at(4);
    QCOMPARE(callD->uncode(), QStringLiteral("DDDDD"));
    const Call* callE = schedule->at(5);
    QCOMPARE(callE->uncode(), QStringLiteral("EEEEE"));

    QCOMPARE(document->stowage()->changeOfRotationIsLegal(callA, +1), QString("Container ZZZU5698360 gets invalid move with BBBBB/0000"));
    QCOMPARE(document->stowage()->changeOfRotationIsLegal(callB, -1), QString("Container ZZZU5698360 gets invalid move with AAAAA/0000"));

    QCOMPARE(document->stowage()->changeOfRotationIsLegal(callB, +1), QString());
    QCOMPARE(document->stowage()->changeOfRotationIsLegal(callC, -1), QString());

    QCOMPARE(document->stowage()->changeOfRotationIsLegal(callC, +1), QString());
    QCOMPARE(document->stowage()->changeOfRotationIsLegal(callD, -1), QString());

    QCOMPARE(document->stowage()->changeOfRotationIsLegal(callD, +1), QString("Container ZZZU5698360 gets invalid move with EEEEE/0000"));
    QCOMPARE(document->stowage()->changeOfRotationIsLegal(callE, -1), QString("Container ZZZU5698360 gets invalid move with DDDDD/0000"));

    delete document;
}

void ChangeOfRotationTest::changeOfRotationTest3() {
    document_t* document = FileOpener::openStoFile(QFINDTESTDATA("test-data/cor-test-3.sto")).document;
    Schedule* schedule = document->schedule();
    const Call* callA = schedule->at(1);
    QCOMPARE(callA->uncode(), QStringLiteral("AAAAA"));
    const Call* callB = schedule->at(2);
    QCOMPARE(callB->uncode(), QStringLiteral("BBBBB"));
    const Call* callC = schedule->at(3);
    QCOMPARE(callC->uncode(), QStringLiteral("CCCCC"));
    const Call* callD = schedule->at(4);
    QCOMPARE(callD->uncode(), QStringLiteral("DDDDD"));
    const Call* callE = schedule->at(5);
    QCOMPARE(callE->uncode(), QStringLiteral("EEEEE"));
    const Call* callF = schedule->at(6);
    QCOMPARE(callF->uncode(), QStringLiteral("FFFFF"));
    const Call* callG = schedule->at(7);
    QCOMPARE(callG->uncode(), QStringLiteral("GGGGG"));
    const Call* callH = schedule->at(8);
    QCOMPARE(callH->uncode(), QStringLiteral("HHHHH"));

    QCOMPARE(document->stowage()->changeOfRotationIsLegal(callA, +1), QString());
    QCOMPARE(document->stowage()->changeOfRotationIsLegal(callB, -1), QString());

    QCOMPARE(document->stowage()->changeOfRotationIsLegal(callB, +1), QString());
    QCOMPARE(document->stowage()->changeOfRotationIsLegal(callC, -1), QString());

    QCOMPARE(document->stowage()->changeOfRotationIsLegal(callC, +1), QString("Two containers in 1,1,80 other container is ZZZU0131965"));
    QCOMPARE(document->stowage()->changeOfRotationIsLegal(callD, -1), QString("Two containers in 1,1,80 other container is ZZZU0131964"));

    QCOMPARE(document->stowage()->changeOfRotationIsLegal(callD, +1), QString());
    QCOMPARE(document->stowage()->changeOfRotationIsLegal(callE, -1), QString());

    QCOMPARE(document->stowage()->changeOfRotationIsLegal(callE, +1), QString());
    QCOMPARE(document->stowage()->changeOfRotationIsLegal(callF, -1), QString());

    QCOMPARE(document->stowage()->changeOfRotationIsLegal(callF, +1), QString());
    QCOMPARE(document->stowage()->changeOfRotationIsLegal(callG, -1), QString());

    QCOMPARE(document->stowage()->changeOfRotationIsLegal(callG, +1), QString());
    QCOMPARE(document->stowage()->changeOfRotationIsLegal(callH, -1), QString());

    delete document;
}

#include "changeofrotationtest.moc"
