#include <QTest>

#include "frontcallpopper.h"
#include "macrocompartmentcommand.h"
#include "stowplugininterface/macroplan.h"
#include "test/util/vesseltestutils.h"

#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>

using ange::schedule::Schedule;
using ange::schedule::Call;
using ange::vessel::Vessel;
using ange::angelstow::MacroPlan;

class MacroStowageTest : public QObject {

    Q_OBJECT

private Q_SLOTS:
    void addMacroStowage();
    void addTwoMacroStowageAfter();
    void addTwoMacroStowageBefore();
    void clearMacroStowage();

    // Reacting to schedule changes:
    // TODO void addCall();
    // TODO void removeCall();
    void testCanChangeRotationSwapping();
    void testCanChangeRotationOverlap();

    // React to new vessel:
    // TODO void vesselChanging();

    void testRemoveCallUpdateLastModified();
};

#if 0
static void dumpMacrostowageCompartment(MacroStowage* macroStowage, const ange::vessel::Compartment* compartment) {
    qDebug() << "begin" << Q_FUNC_INFO;
    MacroStowage::MacroStowageSequence sequence = macroStowage->sequenceForCompartment(compartment);
    Q_FOREACH(const MacroStowage::MacroStowageElement element, sequence) {
        qDebug() << "Load" << element.load << "disch" << element.discharge;
    }
    qDebug() << "end" << Q_FUNC_INFO;
}
#endif

// ownership responsibility of caller
static Schedule* buildSchedule() {
    Call* callA = new Call("A", "");
    Call* callB = new Call("B", "");
    Call* callC = new Call("C", "");
    Call* callD = new Call("D", "");
    Call* callE = new Call("E", "");
    Call* callF = new Call("F", "");
    Call* callG = new Call("G", "");
    Schedule* schedule = new Schedule(QList<Call*>() << callA << callB << callC << callD << callE << callF << callG);
    schedule->setPivotCall(schedule->size() - 1); // needed for running schedulemodel with modeltest
    return schedule;
}

void MacroStowageTest::addMacroStowage() {
    Schedule* schedule = buildSchedule();
    Vessel* vessel = loadVessel(QFINDTESTDATA("test-data/single-stack.json"));
    stowage_t* stowage = new stowage_t(schedule);
    document_t* document = new document_t(schedule, vessel, stowage);

    MacroPlan* macroStowage = document->macro_stowage();

    const ange::vessel::Compartment* compartment = vessel->compartmentContaining(ange::vessel::BayRowTier(1, 0, 80));
    QVERIFY(compartment);

    Call* callA = schedule->at(0); // before load call
    QCOMPARE(callA->uncode(), QStringLiteral("A"));
    Call* callB = schedule->at(1); // load call
    QCOMPARE(callB->uncode(), QStringLiteral("B"));
    Call* callD = schedule->at(3); // call after load call
    QCOMPARE(callD->uncode(), QStringLiteral("D"));
    Call* callE = schedule->at(4); // discharge call
    QCOMPARE(callE->uncode(), QStringLiteral("E"));
    Call* callF = schedule->at(5); // call after discharge call
    QCOMPARE(callF->uncode(), QStringLiteral("F"));

    // verify empty
    QCOMPARE(macroStowage->dischargeCall(compartment, callB), (Call*)0);
    QCOMPARE(macroStowage->dischargeCall(compartment, callD), (Call*)0);
    QCOMPARE(macroStowage->dischargeCall(compartment, callF), (Call*)0);
    QCOMPARE(macroStowage->loadCall(compartment, callE), (Call*)0);
    QVERIFY(macroStowage->macroStowageEmpty());

    QCOMPARE(macroStowage->conflicts(compartment, callB, callE), MacroPlan::NoConflicts);
    MacroCompartmentCommand* command = MacroCompartmentCommand::insert(document, compartment, callB, callE);
    document->undostack()->push(command);

    QVERIFY(!macroStowage->macroStowageEmpty());

    QCOMPARE(macroStowage->conflicts(compartment, callB, callF), MacroPlan::SameLoadOtherDischarge);
    QCOMPARE(macroStowage->conflicts(compartment, callD, callE), MacroPlan::OtherConflict);
    QCOMPARE(macroStowage->conflicts(compartment, callB, callE), MacroPlan::SameAllocation);
    QCOMPARE(macroStowage->dischargeCall(compartment, callD), callE);
    QCOMPARE(macroStowage->dischargeCall(compartment, callB), callE);

    QCOMPARE(macroStowage->dischargeCall(compartment, callF), (Call*)0);
    QCOMPARE(macroStowage->dischargeCall(compartment, callA), (Call*)0);

    QCOMPARE(macroStowage->loadCall(compartment, callA), (Call*)0);
    QCOMPARE(macroStowage->loadCall(compartment, callB), callB);
    QCOMPARE(macroStowage->loadCall(compartment, callD), callB);
    QCOMPARE(macroStowage->loadCall(compartment, callE), (Call*)0);
    QCOMPARE(macroStowage->loadCall(compartment, callF), (Call*)0);

    QCOMPARE(macroStowage->allAllocations().size(), 1);
    QCOMPARE(macroStowage->allAllocations().first().compartment, compartment);
    QCOMPARE(macroStowage->allAllocations().first().load, callB);
    QCOMPARE(macroStowage->allAllocations().first().discharge, callE);

    document->undostack()->undo();

    // verify empty
    QCOMPARE((Call*)0, macroStowage->dischargeCall(compartment, callB));
    QCOMPARE((Call*)0, macroStowage->dischargeCall(compartment, callD));
    QCOMPARE((Call*)0, macroStowage->dischargeCall(compartment, callF));

    QCOMPARE((Call*)0, macroStowage->loadCall(compartment, callE));

    QVERIFY(macroStowage->macroStowageEmpty());
    QVERIFY(macroStowage->allAllocations().isEmpty());

    delete document;
}

void MacroStowageTest::clearMacroStowage() {
    Schedule* schedule = buildSchedule();
    Vessel* vessel = loadVessel(QFINDTESTDATA("test-data/single-stack.json"));
    stowage_t* stowage = new stowage_t(schedule);
    document_t* document = new document_t(schedule, vessel, stowage);

    MacroPlan* macroStowage = document->macro_stowage();

    const ange::vessel::Compartment* compartment = vessel->compartmentContaining(ange::vessel::BayRowTier(1, 0, 80));
    QVERIFY(compartment);

    Call* callB = schedule->at(1); // load call
    QCOMPARE(callB->uncode(), QStringLiteral("B"));
    Call* callC = schedule->at(2); // load call
    QCOMPARE(callC->uncode(), QStringLiteral("C"));
    Call* callD = schedule->at(3); // call after load call
    QCOMPARE(callD->uncode(), QStringLiteral("D"));
    Call* callE = schedule->at(4); // discharge call
    QCOMPARE(callE->uncode(), QStringLiteral("E"));
    Call* callF = schedule->at(5); // call after discharge call
    QCOMPARE(callF->uncode(), QStringLiteral("F"));

    // verify empty
    QCOMPARE(macroStowage->dischargeCall(compartment, callB), (Call*)0);
    QCOMPARE(macroStowage->dischargeCall(compartment, callD), (Call*)0);
    QCOMPARE(macroStowage->dischargeCall(compartment, callF), (Call*)0);
    QCOMPARE(macroStowage->loadCall(compartment, callE), (Call*)0);

    QVERIFY(macroStowage->macroStowageEmpty());
    {
        QCOMPARE(macroStowage->conflicts(compartment, callB, callE), MacroPlan::NoConflicts);
        MacroCompartmentCommand* command = MacroCompartmentCommand::insert(document, compartment, callB, callE);
        document->undostack()->push(command);

        QCOMPARE(macroStowage->dischargeCall(compartment, callB), callE);
        QCOMPARE(macroStowage->dischargeCall(compartment, callD), callE);

        QCOMPARE(macroStowage->dischargeCall(compartment, callF), (Call*)0);

        QCOMPARE(macroStowage->loadCall(compartment, callD), callB);
    }
    QVERIFY(!macroStowage->macroStowageEmpty());
    {
        MacroCompartmentCommand* command = MacroCompartmentCommand::remove(document, compartment, callB, callE);
        document->undostack()->push(command);

        QCOMPARE(macroStowage->dischargeCall(compartment, callB), (Call*)0);
        QCOMPARE(macroStowage->dischargeCall(compartment, callD), (Call*)0);

        QCOMPARE(macroStowage->dischargeCall(compartment, callF), (Call*)0);
    }
    QVERIFY(macroStowage->macroStowageEmpty());

    document->undostack()->undo();
    QCOMPARE(macroStowage->dischargeCall(compartment, callB), callE);
    QCOMPARE(macroStowage->dischargeCall(compartment, callD), callE);
    QVERIFY(!macroStowage->macroStowageEmpty());

    document->undostack()->undo();

    // verify empty
    QCOMPARE((Call*)0, macroStowage->dischargeCall(compartment, callB));
    QCOMPARE((Call*)0, macroStowage->dischargeCall(compartment, callD));
    QCOMPARE((Call*)0, macroStowage->dischargeCall(compartment, callF));
    QCOMPARE((Call*)0, macroStowage->loadCall(compartment, callE));
    QVERIFY(macroStowage->macroStowageEmpty());

    delete document;
}

void MacroStowageTest::addTwoMacroStowageAfter() {
    // adds two macro stowages in order
    Schedule* schedule = buildSchedule();
    Vessel* vessel = loadVessel(QFINDTESTDATA("test-data/single-stack.json"));
    stowage_t* stowage = new stowage_t(schedule);
    document_t* document = new document_t(schedule, vessel, stowage);

    MacroPlan* macroStowage = document->macro_stowage();

    const ange::vessel::Compartment* compartment = vessel->compartmentContaining(ange::vessel::BayRowTier(1, 0, 80));
    QVERIFY(compartment);

    Call* callB = schedule->at(1); // first load call
    QCOMPARE(callB->uncode(), QStringLiteral("B"));
    Call* callC = schedule->at(2); // first discharge
    QCOMPARE(callC->uncode(), QStringLiteral("C"));
    Call* callD = schedule->at(3); // second load
    QCOMPARE(callD->uncode(), QStringLiteral("D"));
    Call* callE = schedule->at(4); // in second trip
    QCOMPARE(callE->uncode(), QStringLiteral("E"));
    Call* callF = schedule->at(5); // second discharge
    QCOMPARE(callF->uncode(), QStringLiteral("F"));

    QVERIFY(macroStowage->macroStowageEmpty());
    {
        QCOMPARE(macroStowage->conflicts(compartment, callB, callC), MacroPlan::NoConflicts);
        MacroCompartmentCommand* command = MacroCompartmentCommand::insert(document, compartment, callB, callC);
        document->undostack()->push(command);
    }
    QVERIFY(!macroStowage->macroStowageEmpty());
    {
        QCOMPARE(macroStowage->conflicts(compartment, callD, callF), MacroPlan::NoConflicts);
        MacroCompartmentCommand* command = MacroCompartmentCommand::insert(document, compartment, callD, callF);
        document->undostack()->push(command);
    }
    QVERIFY(!macroStowage->macroStowageEmpty());

    QCOMPARE(macroStowage->sequenceForCompartment(compartment).size(), 2);
    QCOMPARE(macroStowage->sequenceForCompartment(compartment).at(0).load, callB);
    QCOMPARE(macroStowage->sequenceForCompartment(compartment).at(0).discharge, callC);
    QCOMPARE(macroStowage->sequenceForCompartment(compartment).at(1).load, callD);
    QCOMPARE(macroStowage->sequenceForCompartment(compartment).at(1).discharge, callF);

    QCOMPARE(macroStowage->allAllocations().size(),2);
    QCOMPARE(macroStowage->allAllocations().first().compartment, compartment);
    QCOMPARE(macroStowage->allAllocations().first().load, callB);
    QCOMPARE(macroStowage->allAllocations().first().discharge, callC);
    QCOMPARE(macroStowage->allAllocations().last().compartment, compartment);
    QCOMPARE(macroStowage->allAllocations().last().load, callD);
    QCOMPARE(macroStowage->allAllocations().last().discharge, callF);

    QCOMPARE(macroStowage->loadCall(compartment, callB), callB);
    QCOMPARE(macroStowage->loadCall(compartment, callC), (Call*)0);
    QCOMPARE(macroStowage->loadCall(compartment, callD), callD);
    QCOMPARE(macroStowage->loadCall(compartment, callE), callD);
    QCOMPARE(macroStowage->loadCall(compartment, callF), (Call*)0);

    QCOMPARE(macroStowage->dischargeCall(compartment, callB), callC);
    QCOMPARE(macroStowage->dischargeCall(compartment, callC), (Call*)0);
    QCOMPARE(macroStowage->dischargeCall(compartment, callD), callF);
    QCOMPARE(macroStowage->dischargeCall(compartment, callE), callF);
    QCOMPARE(macroStowage->dischargeCall(compartment, callF), (Call*)0);

    delete document;
}

void MacroStowageTest::addTwoMacroStowageBefore() {
    // adds two macro stowages in opposite order
    Schedule* schedule = buildSchedule();
    Vessel* vessel = loadVessel(QFINDTESTDATA("test-data/single-stack.json"));
    stowage_t* stowage = new stowage_t(schedule);
    document_t* document = new document_t(schedule, vessel, stowage);

    MacroPlan* macroStowage = document->macro_stowage();

    const ange::vessel::Compartment* compartment = vessel->compartmentContaining(ange::vessel::BayRowTier(1, 0, 80));
    QVERIFY(compartment);

    Call* callB = schedule->at(1); // first load call
    QCOMPARE(callB->uncode(), QStringLiteral("B"));
    Call* callC = schedule->at(2); // first discharge
    QCOMPARE(callC->uncode(), QStringLiteral("C"));
    Call* callD = schedule->at(3); // second load
    QCOMPARE(callD->uncode(), QStringLiteral("D"));
    Call* callE = schedule->at(4); // second discharge
    QCOMPARE(callE->uncode(), QStringLiteral("E"));

    QVERIFY(macroStowage->macroStowageEmpty());
    {
        QCOMPARE(macroStowage->conflicts(compartment, callD, callE), MacroPlan::NoConflicts);
        MacroCompartmentCommand* command = MacroCompartmentCommand::insert(document, compartment, callD, callE);
        document->undostack()->push(command);
    }
    QVERIFY(!macroStowage->macroStowageEmpty());
    {
        QCOMPARE(macroStowage->conflicts(compartment, callB, callC), MacroPlan::NoConflicts);
        MacroCompartmentCommand* command = MacroCompartmentCommand::insert(document, compartment, callB, callC);
        document->undostack()->push(command);
    }
    QVERIFY(!macroStowage->macroStowageEmpty());

    QCOMPARE(macroStowage->sequenceForCompartment(compartment).size(), 2);
    QCOMPARE(macroStowage->sequenceForCompartment(compartment).at(0).load, callB);
    QCOMPARE(macroStowage->sequenceForCompartment(compartment).at(0).discharge, callC);
    QCOMPARE(macroStowage->sequenceForCompartment(compartment).at(1).load, callD);
    QCOMPARE(macroStowage->sequenceForCompartment(compartment).at(1).discharge, callE);

    delete document;
}

void MacroStowageTest::testCanChangeRotationSwapping() {
    // tests primarily rotation up if there is a macro stowage from B->C and a request to swap B and C.
    Schedule* schedule = buildSchedule();
    Vessel* vessel = loadVessel(QFINDTESTDATA("test-data/single-stack.json"));
    stowage_t* stowage = new stowage_t(schedule);

    document_t* document = new document_t(schedule, vessel, stowage);
    MacroPlan* macroStowage = document->macro_stowage();

    const ange::vessel::Compartment* compartment = vessel->compartmentContaining(ange::vessel::BayRowTier(1, 0, 80));
    QVERIFY(compartment);

    Call* callA = schedule->at(0);
    QCOMPARE(callA->uncode(), QStringLiteral("A"));
    Call* callB = schedule->at(1);
    QCOMPARE(callB->uncode(), QStringLiteral("B"));
    Call* callC = schedule->at(2);
    QCOMPARE(callC->uncode(), QStringLiteral("C"));
    Call* callD = schedule->at(3);
    QCOMPARE(callD->uncode(), QStringLiteral("D"));

    QVERIFY(macroStowage->callSwapIsLegal(callA, callB).isNull());
    QVERIFY(macroStowage->callSwapIsLegal(callB, callC).isNull());
    QVERIFY(macroStowage->callSwapIsLegal(callC, callD).isNull());
    {
        QCOMPARE(macroStowage->conflicts(compartment, callB, callC), MacroPlan::NoConflicts);
        MacroCompartmentCommand* command = MacroCompartmentCommand::insert(document, compartment, callB, callC);
        QVERIFY(macroStowage->macroStowageEmpty());
        document->undostack()->push(command);
        QVERIFY(!macroStowage->macroStowageEmpty());

        QVERIFY(macroStowage->callSwapIsLegal(callA, callB).isNull());
        QCOMPARE(macroStowage->callSwapIsLegal(callB, callC), QStringLiteral("Master planning reversed in 2A0-0"));
        QVERIFY(macroStowage->callSwapIsLegal(callC, callD).isNull());
    }

    delete document;
}

void MacroStowageTest::testCanChangeRotationOverlap() {
    // tests primarily a situation with a macro stowage from B->C and D->E in same compartment and a request to swap C and D.
    Schedule* schedule = buildSchedule();
    Vessel* vessel = loadVessel(QFINDTESTDATA("test-data/single-stack.json"));
    stowage_t* stowage = new stowage_t(schedule);
    document_t* document = new document_t(schedule, vessel, stowage);

    MacroPlan* macroStowage = document->macro_stowage();

    const ange::vessel::Compartment* compartment = vessel->compartmentContaining(ange::vessel::BayRowTier(1, 0, 80));
    QVERIFY(compartment);

    Call* callB = schedule->at(1); // first load call
    QCOMPARE(callB->uncode(), QStringLiteral("B"));
    Call* callC = schedule->at(2); // first discharge
    QCOMPARE(callC->uncode(), QStringLiteral("C"));
    Call* callD = schedule->at(3); // second load
    QCOMPARE(callD->uncode(), QStringLiteral("D"));
    Call* callE = schedule->at(4); // second discharge
    QCOMPARE(callE->uncode(), QStringLiteral("E"));

    QVERIFY(macroStowage->macroStowageEmpty());
    {
        QCOMPARE(macroStowage->conflicts(compartment, callB, callC), MacroPlan::NoConflicts);
        MacroCompartmentCommand* command = MacroCompartmentCommand::insert(document, compartment, callB, callC);
        document->undostack()->push(command);
    }
    QVERIFY(!macroStowage->macroStowageEmpty());
    {
        QCOMPARE(macroStowage->conflicts(compartment, callD, callE), MacroPlan::NoConflicts);
        MacroCompartmentCommand* command = MacroCompartmentCommand::insert(document, compartment, callD, callE);
        document->undostack()->push(command);
    }
    QVERIFY(!macroStowage->macroStowageEmpty());
    QCOMPARE(macroStowage->callSwapIsLegal(callC, callD), QStringLiteral("Master planning overlap in 2A0-0"));

    delete document;
}

void MacroStowageTest::testRemoveCallUpdateLastModified() {
    Schedule* schedule = buildSchedule();
    schedule->insert(0, new Call("Befor","XXXX")); // front call popper needs the first call to be Befor

    Vessel* vessel = loadVessel(QFINDTESTDATA("test-data/single-stack.json"));
    stowage_t* stowage = new stowage_t(schedule);
    document_t* document = new document_t(schedule, vessel, stowage);

    MacroPlan* macroStowage = document->macro_stowage();
    macroStowage->setFirstModifiedCall(document->schedule()->calls().last()); // executing the macro stowage functionality does this

    const ange::vessel::Compartment* compartment = vessel->compartmentContaining(ange::vessel::BayRowTier(1, 0, 80));
    QVERIFY(compartment);

    Call* callB = schedule->at(2);
    QCOMPARE(callB->uncode(), QStringLiteral("B"));
    Call* callC = schedule->at(3);
    QCOMPARE(callC->uncode(), QStringLiteral("C"));
    Call* callD = schedule->at(4);
    QCOMPARE(callD->uncode(), QStringLiteral("D"));

    {
        MacroCompartmentCommand* command = MacroCompartmentCommand::insert(document, compartment, callB, callC);
        document->undostack()->push(command);
    }
    {
        MacroCompartmentCommand* command = MacroCompartmentCommand::insert(document, compartment, callC, callD);
        document->undostack()->push(command);
    }

    FrontCallPopper* popper = new FrontCallPopper(document, 0);
    popper->pop();
    popper->pop();
    popper->pop();

    // should not segfault.
}



QTEST_GUILESS_MAIN(MacroStowageTest);

#include "macrostowagetest.moc"
