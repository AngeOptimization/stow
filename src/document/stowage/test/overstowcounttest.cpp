#include <QTest>

#include "callutils.h"
#include "document.h"
#include "fileopener.h"
#include "frontcallpopper.h"

#include <ange/schedule/schedule.h>

/**
 * Test the overstow count
 *
 * Used to crash, see ticket #1384
 */
class OverstowCountTest : public QObject {
    Q_OBJECT
private Q_SLOTS:
    void popEverything1();
    void popEverything2();
};

void OverstowCountTest::popEverything1() {
    document_t* doc = FileOpener::openStoFile(QFINDTESTDATA("test-data/overstow.sto")).document;
    while (!is_after(doc->schedule()->at(1))) {
        FrontCallPopper popper(doc, 0);
        popper.pop();
    }
    delete doc;
}

void OverstowCountTest::popEverything2() {
    document_t* doc = FileOpener::openStoFile(QFINDTESTDATA("test-data/lego_overstow_2containers_4calls.sto")).document;
    while (!is_after(doc->schedule()->at(1))) {
        FrontCallPopper popper(doc, 0);
        popper.pop();
    }
    delete doc;
}

QTEST_GUILESS_MAIN(OverstowCountTest);

#include "overstowcounttest.moc"
