#ifndef CHANGELOADCALLTEST_H
#define CHANGELOADCALLTEST_H

#include <QObject>

/**
 * Testing change of load port
 */
class ChangeLoadCallTest : public QObject {
    Q_OBJECT

private Q_SLOTS:
    void shortenTrip();
    void overlappingExtensionOfTrip();

};

#endif // CHANGELOADCALLTEST_H
