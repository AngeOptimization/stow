#include <QTest>

#include "changecontainercommand.h"
#include "container_list.h"
#include "document.h"
#include "fileopener.h"
#include "schedule_change_command.h"
#include "stowage.h"
#include "stowagestack.h"

#include <ange/containers/container.h>
#include <ange/schedule/schedule.h>
#include <ange/vessel/bayslice.h>
#include <ange/vessel/blockweight.h>
#include <ange/vessel/vessel.h>

#include <QObject>

class StowageStack;
namespace ange {
namespace containers {
class Container;
}
}

using namespace ange::angelstow;
using namespace ange::containers;
using namespace ange::schedule;
using namespace ange::units;
using namespace ange::vessel;

class StowageTest : public QObject {
    Q_OBJECT
private Q_SLOTS:
    void initTestCase();
    void testCount();
    void testHeight();
    void testWeight();
private:
    const ange::containers::Container* m_container1;
    const ange::containers::Container* m_container2;
    const StowageStack* m_stowageStack;
};

void StowageTest::initTestCase(){
    document_t* doc = FileOpener::openStoFile(QFINDTESTDATA("test-data/tiny-test.sto")).document;
    const stowage_t* stowage = doc->stowage();
    Schedule* schedule = doc->schedule();
    QCOMPARE(BlockWeight::totalBlockWeight(stowage->bay_block_weights(
                                                        schedule->calls().at(1)).values()).weight()/ton, 45.0);
    const Slot* slot1 = doc->vessel()->slotAt(BayRowTier(3,0,2));
    m_container1 = doc->stowage()->containers_in_slot(slot1).at(0);
    const Slot* slot2 = doc->vessel()->slotAt(BayRowTier(2,0,6));
    m_container2 = doc->stowage()->containers_in_slot(slot2).at(0);
    m_stowageStack = stowage->stowage_stack(schedule->calls().at(1),
                                                                 doc->vessel()->baySlices().at(0)->stacks().at(0));
}

void StowageTest::testCount(){
    QCOMPARE(m_stowageStack->aft_count(), 2);
    QCOMPARE(m_stowageStack->fore_count(), 1);
}
void StowageTest::testHeight() {
    QCOMPARE(m_stowageStack->aftHeight(), m_container1->physicalHeight() + m_container2->physicalHeight());
    QCOMPARE(m_stowageStack->foreHeight(), m_container2->physicalHeight());
}

void StowageTest::testWeight() {
    QCOMPARE(m_stowageStack->weight20Aft(), m_container1->weight());
    QCOMPARE(m_stowageStack->weight20Fore(), 0.0*ton);
    QCOMPARE(m_stowageStack->weight40Aft(), m_container2->weight() + m_container1->weight());
    QCOMPARE(m_stowageStack->weight40Fore(), m_container2->weight());
    QCOMPARE(m_stowageStack->weightTotal(), m_container2->weight() + m_container1->weight());
}


QTEST_GUILESS_MAIN(StowageTest);
#include "stowagetest.moc"
