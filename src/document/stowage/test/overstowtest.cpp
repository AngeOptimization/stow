#include <QTest>

#include "container_stow_command.h"
#include "containerchanger.h"
#include "document.h"
#include "fileopener.h"
#include "overstowcounter.h"
#include "schedulechangerotationcommand.h"
#include "slot_call_content.h"
#include "stowage.h"

#include <ange/containers/container.h>
#include <ange/schedule/schedule.h>
#include <ange/schedule/call.h>
#include <ange/vessel/slot.h>
#include <ange/vessel/vessel.h>

#include <QObject>

using namespace ange::containers;
using ange::schedule::Call;
using ange::vessel::Slot;
using ange::vessel::BayRowTier;

class OverstowTest : public QObject {
    Q_OBJECT
public:

private Q_SLOTS:
    void tinyNone();
    void tinyOne();
    void tinyHanging();
    void tinySupported();
    void hansaLidOverstows();
    void scheduleSwapTest();
    void flushDeletedContainersFromCache();
};

void OverstowTest::tinyNone(){
    const document_t* document = FileOpener::openStoFile(QFINDTESTDATA("test-data/tiny_overstow_none.sto")).document;
    stowage_t* stowage = document->stowage();
    QCOMPARE(document->schedule()->calls().size(), 6);

    const Call* callBefore = document->schedule()->calls().at(0);
    const Call* callA = document->schedule()->calls().at(1);
    const Call* callB = document->schedule()->calls().at(2);
    const Call* callC = document->schedule()->calls().at(3);
    const Call* callD = document->schedule()->calls().at(4);
    const Call* callAfter = document->schedule()->calls().at(5);

    // containers loaded are:
    // Equipment No  Type    POL     POD     Location  Moves        Comments
    // ZZZU8395658   22G0    BBBBB   CCCCC   3,0,4     BBBBB:CCCCC  top - no overstow
    // ZZZU8459933   22G0    AAAAA   DDDDD   3,0,2     AAAAA:DDDDD  bottom

    const Slot* slot302 = document->vessel()->slotAt(BayRowTier(3,0,2));
    const Slot* slot304 = document->vessel()->slotAt(BayRowTier(3,0,4));

    // checking that we have the right loads/restows/discharges before testing overstows

    QCOMPARE(stowage->load_count(callBefore), 0);
    QCOMPARE(stowage->restow_count(callBefore), 0);
    QCOMPARE(stowage->discharge_count(callBefore), 0);

    QCOMPARE(stowage->load_count(callA), 1);
    QCOMPARE(stowage->restow_count(callA), 0);
    QCOMPARE(stowage->discharge_count(callA), 0);

    QCOMPARE(stowage->load_count(callB), 1);
    QCOMPARE(stowage->restow_count(callB), 0);
    QCOMPARE(stowage->discharge_count(callB), 0);

    QCOMPARE(stowage->load_count(callC), 0);
    QCOMPARE(stowage->restow_count(callC), 0);
    QCOMPARE(stowage->discharge_count(callC), 1);

    QCOMPARE(stowage->load_count(callD), 0);
    QCOMPARE(stowage->restow_count(callD), 0);
    QCOMPARE(stowage->discharge_count(callD), 1);

    QCOMPARE(stowage->load_count(callAfter), 0);
    QCOMPARE(stowage->restow_count(callAfter), 0);
    QCOMPARE(stowage->discharge_count(callAfter), 0);

    QCOMPARE(stowage->overstowCounter()->overstowMove(callBefore, slot302), OverstowCounter::None);
    QCOMPARE(stowage->overstowCounter()->overstowMove(callA, slot302), OverstowCounter::None);
    QCOMPARE(stowage->overstowCounter()->overstowMove(callB, slot302), OverstowCounter::None);
    QCOMPARE(stowage->overstowCounter()->overstowMove(callC, slot302), OverstowCounter::None);
    QCOMPARE(stowage->overstowCounter()->overstowMove(callD, slot302), OverstowCounter::None);
    QCOMPARE(stowage->overstowCounter()->overstowMove(callAfter, slot302), OverstowCounter::None);

    QCOMPARE(stowage->overstowCounter()->overstowMove(callBefore, slot304), OverstowCounter::None);
    QCOMPARE(stowage->overstowCounter()->overstowMove(callA, slot304), OverstowCounter::None);
    QCOMPARE(stowage->overstowCounter()->overstowMove(callB, slot304), OverstowCounter::None);
    QCOMPARE(stowage->overstowCounter()->overstowMove(callC, slot304), OverstowCounter::None);
    QCOMPARE(stowage->overstowCounter()->overstowMove(callD, slot304), OverstowCounter::None);
    QCOMPARE(stowage->overstowCounter()->overstowMove(callAfter, slot304), OverstowCounter::None);

    delete document;
}

void OverstowTest::tinyOne(){
    document_t* document = FileOpener::openStoFile(QFINDTESTDATA("test-data/tiny_overstow_one.sto")).document;
    stowage_t* stowage = document->stowage();
    QCOMPARE(document->schedule()->calls().size(), 8);

    const Call* callBefore = document->schedule()->calls().at(0);
    const Call* callA = document->schedule()->calls().at(1);
    const Call* callB = document->schedule()->calls().at(2);
    const Call* callC = document->schedule()->calls().at(3);
    const Call* callD = document->schedule()->calls().at(4);
    const Call* callE = document->schedule()->calls().at(5);
    const Call* callF = document->schedule()->calls().at(6);
    const Call* callAfter = document->schedule()->calls().at(7);

    // containers loaded are:
    // Equipment No  Type    POL     POD     Location  Moves        Comments
    // ZZZU8459933   22G0    AAAAA   FFFFF   3,0,4     AAAAA:FFFFF  hanging in AAAAA and DDDDD-FFFFF
    // ZZZU8395658   22G0    BBBBB   DDDDD   3,0,2     BBBBB:DDDDD  understow in BBBBB and DDDDD

    const Slot* slot302 = document->vessel()->slotAt(BayRowTier(3,0,2));
    const Slot* slot304 = document->vessel()->slotAt(BayRowTier(3,0,4));

    // checking that we have the right loads/restows/discharges before testing overstows

    QCOMPARE(stowage->load_count(callBefore), 0);
    QCOMPARE(stowage->restow_count(callBefore), 0);
    QCOMPARE(stowage->discharge_count(callBefore), 0);

    QCOMPARE(stowage->load_count(callA), 1);
    QCOMPARE(stowage->restow_count(callA), 0);
    QCOMPARE(stowage->discharge_count(callA), 0);

    QCOMPARE(stowage->load_count(callB), 1);
    QCOMPARE(stowage->restow_count(callB), 1);
    QCOMPARE(stowage->discharge_count(callB), 0);

    QCOMPARE(stowage->load_count(callC), 0);
    QCOMPARE(stowage->restow_count(callC), 0);
    QCOMPARE(stowage->discharge_count(callC), 0);

    QCOMPARE(stowage->load_count(callD), 0);
    QCOMPARE(stowage->restow_count(callD), 1);
    QCOMPARE(stowage->discharge_count(callD), 1);

    QCOMPARE(stowage->load_count(callE), 0);
    QCOMPARE(stowage->restow_count(callE), 0);
    QCOMPARE(stowage->discharge_count(callE), 0);

    QCOMPARE(stowage->load_count(callF), 0);
    QCOMPARE(stowage->restow_count(callF), 0);
    QCOMPARE(stowage->discharge_count(callF), 1);

    QCOMPARE(stowage->load_count(callAfter), 0);
    QCOMPARE(stowage->restow_count(callAfter), 0);
    QCOMPARE(stowage->discharge_count(callAfter), 0);

    QCOMPARE(stowage->overstowCounter()->overstowMove(callBefore, slot302), OverstowCounter::None);
    QCOMPARE(stowage->overstowCounter()->overstowMove(callA, slot302), OverstowCounter::None);
    QCOMPARE(stowage->overstowCounter()->overstowMove(callB, slot302), OverstowCounter::None);
    QCOMPARE(stowage->overstowCounter()->overstowMove(callC, slot302), OverstowCounter::None);
    QCOMPARE(stowage->overstowCounter()->overstowMove(callD, slot302), OverstowCounter::None);
    QCOMPARE(stowage->overstowCounter()->overstowMove(callE, slot302), OverstowCounter::None);
    QCOMPARE(stowage->overstowCounter()->overstowMove(callF, slot302), OverstowCounter::None);
    QCOMPARE(stowage->overstowCounter()->overstowMove(callAfter, slot302), OverstowCounter::None);

    QCOMPARE(stowage->overstowCounter()->overstowMove(callBefore, slot304), OverstowCounter::None);
    QCOMPARE(stowage->overstowCounter()->overstowMove(callA, slot304), OverstowCounter::Later);
    QCOMPARE(stowage->overstowCounter()->overstowMove(callB, slot304), OverstowCounter::Now);
    QCOMPARE(stowage->overstowCounter()->overstowMove(callC, slot304), OverstowCounter::Later);
    QCOMPARE(stowage->overstowCounter()->overstowMove(callD, slot304), OverstowCounter::Now);
    QCOMPARE(stowage->overstowCounter()->overstowMove(callE, slot304), OverstowCounter::None);
    QCOMPARE(stowage->overstowCounter()->overstowMove(callF, slot304), OverstowCounter::None);
    QCOMPARE(stowage->overstowCounter()->overstowMove(callAfter, slot304), OverstowCounter::None);

    { // Change POL or POD for ZZZU8459933
        // This does not need special code in stowage_t as moves are changed by ContainerChanger
        const Container* containerZZZU8459933 = stowage->contents(slot304, callA).container();
        QCOMPARE(QString(containerZZZU8459933->equipmentNumber()), QString("ZZZU8459933"));

        QCOMPARE(stowage->overstowCounter()->overstowMove(callBefore, slot304), OverstowCounter::None);
        QSharedPointer<ChangerResult> polChange =  ContainerChanger::changeLoadCall(document, containerZZZU8459933, callBefore);
        QCOMPARE(polChange->problemsGenerated(), 0);
        document->undostack()->push(polChange->takeCommand());
        QCOMPARE(stowage->overstowCounter()->overstowMove(callBefore, slot304), OverstowCounter::Later);

        QCOMPARE(stowage->overstowCounter()->overstowMove(callD, slot304), OverstowCounter::Now);
        QSharedPointer<ChangerResult> podChange =  ContainerChanger::changeDischargeCall(document, containerZZZU8459933, callD);
        QCOMPARE(podChange->problemsGenerated(), 0);
        document->undostack()->push(podChange->takeCommand());
        QCOMPARE(stowage->overstowCounter()->overstowMove(callD, slot304), OverstowCounter::None);
    }

    delete document;
}

void OverstowTest::tinyHanging(){
    const document_t* document = FileOpener::openStoFile(QFINDTESTDATA("test-data/tiny_overstow_hanging.sto")).document;
    stowage_t* stowage = document->stowage();
    QCOMPARE(document->schedule()->calls().size(), 6);

    const Call* callBefore = document->schedule()->calls().at(0);
    const Call* callA = document->schedule()->calls().at(1);
    const Call* callB = document->schedule()->calls().at(2);
    const Call* callC = document->schedule()->calls().at(3);
    const Call* callD = document->schedule()->calls().at(4);
    const Call* callAfter = document->schedule()->calls().at(5);

    // containers loaded are:
    // Equipment No  Type    POL     POD     Location  Moves        Comments
    // ZZZU8459933   22G0    AAAAA   DDDDD   3,0,4     AAAAA:DDDDD  hanging in AAAAA and DDDD
    // ZZZU8395658   22G0    BBBBB   CCCCC   3,0,2     BBBBB:CCCCC  understow in BBBBB + CCCCC

    const Slot* slot302 = document->vessel()->slotAt(BayRowTier(3,0,2));
    const Slot* slot304 = document->vessel()->slotAt(BayRowTier(3,0,4));

    // checking that we have the right loads/restows/discharges before testing overstows

    QCOMPARE(stowage->load_count(callBefore), 0);
    QCOMPARE(stowage->restow_count(callBefore), 0);
    QCOMPARE(stowage->discharge_count(callBefore), 0);

    QCOMPARE(stowage->load_count(callA), 1);
    QCOMPARE(stowage->restow_count(callA), 0);
    QCOMPARE(stowage->discharge_count(callA), 0);

    QCOMPARE(stowage->load_count(callB), 1);
    QCOMPARE(stowage->restow_count(callB), 1);  // 1 restow = 1 load + 1 disccharge
    QCOMPARE(stowage->discharge_count(callB), 0);

    QCOMPARE(stowage->load_count(callC), 0);
    QCOMPARE(stowage->restow_count(callC), 1);  // 1 restow = 1 load + 1 disccharge
    QCOMPARE(stowage->discharge_count(callC), 1);

    QCOMPARE(stowage->load_count(callD), 0);
    QCOMPARE(stowage->restow_count(callD), 0);
    QCOMPARE(stowage->discharge_count(callD), 1);

    QCOMPARE(stowage->load_count(callAfter), 0);
    QCOMPARE(stowage->restow_count(callAfter), 0);
    QCOMPARE(stowage->discharge_count(callAfter), 0);

    QCOMPARE(stowage->overstowCounter()->overstowMove(callBefore, slot302), OverstowCounter::None);
    QCOMPARE(stowage->overstowCounter()->overstowMove(callA, slot302), OverstowCounter::None);
    QCOMPARE(stowage->overstowCounter()->overstowMove(callB, slot302), OverstowCounter::None);
    QCOMPARE(stowage->overstowCounter()->overstowMove(callC, slot302), OverstowCounter::None);
    QCOMPARE(stowage->overstowCounter()->overstowMove(callD, slot302), OverstowCounter::None);
    QCOMPARE(stowage->overstowCounter()->overstowMove(callAfter, slot302), OverstowCounter::None);

    QCOMPARE(stowage->overstowCounter()->overstowMove(callBefore, slot304), OverstowCounter::None);
    QCOMPARE(stowage->overstowCounter()->overstowMove(callA, slot304), OverstowCounter::Later);
    QCOMPARE(stowage->overstowCounter()->overstowMove(callB, slot304), OverstowCounter::Now);
    QCOMPARE(stowage->overstowCounter()->overstowMove(callC, slot304), OverstowCounter::Now); // overstow because top container goes to DDDDD
    QCOMPARE(stowage->overstowCounter()->overstowMove(callD, slot304), OverstowCounter::None);
    QCOMPARE(stowage->overstowCounter()->overstowMove(callAfter, slot304), OverstowCounter::None);

    delete document;
}

void OverstowTest::tinySupported(){
    const document_t* document = FileOpener::openStoFile(QFINDTESTDATA("test-data/tiny_overstow_supported.sto")).document;
    stowage_t* stowage = document->stowage();
    QCOMPARE(document->schedule()->calls().size(), 6);

    const Call* callBefore = document->schedule()->calls().at(0);
    const Call* callA = document->schedule()->calls().at(1);
    const Call* callB = document->schedule()->calls().at(2);
    const Call* callC = document->schedule()->calls().at(3);
    const Call* callD = document->schedule()->calls().at(4);
    const Call* callAfter = document->schedule()->calls().at(5);

    // containers loaded are:
    // Equipment No  Type    POL     POD     Location  Moves        Comments
    // ZZZU8459933 22G0      AAAAA   DDDDD   3,0,4     AAAAA:DDDDD  supported in AAAAA and DDDD
    // ZZZU8395658 22G0      BBBBB   CCCCC   3,0,2     BBBBB:CCCCC  understow in BBBBB + CCCCC
    // ZZZU5714297 22G0      AAAAA   BBBBB   3,0,2     AAAAA:BBBBB  supporting container
    // ZZZU1254843 22G0      CCCCC   DDDDD   3,0,2     CCCCC:DDDDD  supporting container

    const Slot* slot302 = document->vessel()->slotAt(BayRowTier(3,0,2));
    const Slot* slot304 = document->vessel()->slotAt(BayRowTier(3,0,4));

    // checking that we have the right loads/restows/discharges before testing overstows

    QCOMPARE(stowage->load_count(callBefore), 0);
    QCOMPARE(stowage->restow_count(callBefore), 0);
    QCOMPARE(stowage->discharge_count(callBefore), 0);

    QCOMPARE(stowage->load_count(callA), 2);
    QCOMPARE(stowage->restow_count(callA), 0);
    QCOMPARE(stowage->discharge_count(callA), 0);

    QCOMPARE(stowage->load_count(callB), 1);
    QCOMPARE(stowage->restow_count(callB), 1);  // 1 restow = 1 load + 1 disccharge
    QCOMPARE(stowage->discharge_count(callB), 1);

    QCOMPARE(stowage->load_count(callC), 1);
    QCOMPARE(stowage->restow_count(callC), 1);  // 1 restow = 1 load + 1 disccharge
    QCOMPARE(stowage->discharge_count(callC), 1);

    QCOMPARE(stowage->load_count(callD), 0);
    QCOMPARE(stowage->restow_count(callD), 0);
    QCOMPARE(stowage->discharge_count(callD), 2);

    QCOMPARE(stowage->load_count(callAfter), 0);
    QCOMPARE(stowage->restow_count(callAfter), 0);
    QCOMPARE(stowage->discharge_count(callAfter), 0);

    QCOMPARE(stowage->overstowCounter()->overstowMove(callBefore, slot302), OverstowCounter::None);
    QCOMPARE(stowage->overstowCounter()->overstowMove(callA, slot302), OverstowCounter::None);
    QCOMPARE(stowage->overstowCounter()->overstowMove(callB, slot302), OverstowCounter::None);
    QCOMPARE(stowage->overstowCounter()->overstowMove(callC, slot302), OverstowCounter::None);
    QCOMPARE(stowage->overstowCounter()->overstowMove(callD, slot302), OverstowCounter::None);
    QCOMPARE(stowage->overstowCounter()->overstowMove(callAfter, slot302), OverstowCounter::None);

    QCOMPARE(stowage->overstowCounter()->overstowMove(callBefore, slot304), OverstowCounter::None);
    QCOMPARE(stowage->overstowCounter()->overstowMove(callA, slot304), OverstowCounter::Later);
    QCOMPARE(stowage->overstowCounter()->overstowMove(callB, slot304), OverstowCounter::Now);
    QCOMPARE(stowage->overstowCounter()->overstowMove(callC, slot304), OverstowCounter::Now); // overstow because top container goes to DDDDD
    QCOMPARE(stowage->overstowCounter()->overstowMove(callD, slot304), OverstowCounter::None);
    QCOMPARE(stowage->overstowCounter()->overstowMove(callAfter, slot304), OverstowCounter::None);

    delete document;
}

void OverstowTest::hansaLidOverstows() {
    const document_t* document = FileOpener::openStoFile(QFINDTESTDATA("test-data/1330W_HANSA_ALG_OVERSTOW.sto")).document;
    stowage_t* stowage = document->stowage();
    QCOMPARE(document->schedule()->calls().size(), 7);

    const Call* callBefore = document->schedule()->calls().at(0);
    const Call* call1ESALG = document->schedule()->calls().at(1);
    const Call* call2NGTIN = document->schedule()->calls().at(2);
    const Call* call3BJCOO = document->schedule()->calls().at(3);
    const Call* call4TGLFW = document->schedule()->calls().at(4);
    const Call* call5ESALG = document->schedule()->calls().at(5);
    const Call* callAfter = document->schedule()->calls().at(6);

    /*
     *    Equipment No    Type    POL POD Location    Moves
     *
     *    ZZZU7284157 42G0    ESALG   NGTIN   14,0,82 ESALG:NGTIN
     *    ZZZU7284149 42G0    ESALG   NGTIN   22,5,2  ESALG:NGTIN
     *    ZZZU7284148 42G0    ESALG   NGTIN   26,5,82 ESALG:NGTIN
     *    ZZZU7284138 22G0    ESALG   NGTIN   13,2,82 ESALG:NGTIN
     *    ZZZU7284137 22G0    ESALG   NGTIN   21,9,4  ESALG:NGTIN
     *    ZZZU7284136 22G0    ESALG   NGTIN   23,9,4  ESALG:NGTIN
     *    ZZZU7284135 22G0    ESALG   NGTIN   15,2,82 ESALG:NGTIN
     *    ZZZU7284134 22G0    ESALG   NGTIN   27,6,82 ESALG:NGTIN
     *    ZZZU7284128 22G0    ESALG   NGTIN   25,6,82 ESALG:NGTIN
     *
     *
     *
     *    Equipment No    Type    POL POD Location    Moves
     *    ZZZU6652890 42G0    ESALG   TGLFW   14,3,82 ESALG:TGLFW
     *    ZZZU6652882 42G0    ESALG   TGLFW   22,7,82 ESALG:TGLFW
     *    ZZZU6652881 42G0    ESALG   TGLFW   26,5,84 ESALG:TGLFW
     *    ZZZU6652871 22G0    ESALG   TGLFW   13,1,82 ESALG:TGLFW
     *    ZZZU6652870 22G0    ESALG   TGLFW   23,6,82 ESALG:TGLFW
     *    ZZZU6652869 22G0    ESALG   TGLFW   21,6,82 ESALG:TGLFW
     *    ZZZU6652868 22G0    ESALG   TGLFW   15,1,82 ESALG:TGLFW
     *    ZZZU6652867 22G0    ESALG   TGLFW   25,6,84 ESALG:TGLFW
     *    ZZZU6652861 22G0    ESALG   TGLFW   27,6,84 ESALG:TGLFW
     *
     *    Equipment No    Type    POL POD Location    Moves
     *    ZZZU6652922 42G0    NGTIN   BJCOO   26,5,82 NGTIN:BJCOO
     *    ZZZU6652914 22G0    NGTIN   BJCOO   27,6,82 NGTIN:BJCOO
     *    ZZZU6652901 22G0    NGTIN   BJCOO   25,6,82 NGTIN:BJCOO
     *
     *    Equipment No    Type    POL POD Location    Moves
     *    ZZZU7284197 42G0    BJCOO   ESALG   14,4,82 BJCOO:ESALG
     *    ZZZU7284188 42G0    BJCOO   ESALG   26,5,82 BJCOO:ESALG
     *    ZZZU7284177 22G0    BJCOO   ESALG   13,6,82 BJCOO:ESALG
     *    ZZZU7284176 22G0    BJCOO   ESALG   15,6,82 BJCOO:ESALG
     *    ZZZU7284174 22G0    BJCOO   ESALG   27,6,82 BJCOO:ESALG
     *    ZZZU7284168 22G0    BJCOO   ESALG   25,6,82 BJCOO:ESALG
     */

    // checking that we have the right loads/restows/discharges before testing overstows

    QCOMPARE(stowage->discharge_count(callBefore), 0);
    QCOMPARE(stowage->restow_count(callBefore), 0);
    QCOMPARE(stowage->load_count(callBefore), 0);

    QCOMPARE(stowage->discharge_count(call1ESALG), 0);
    QCOMPARE(stowage->restow_count(call1ESALG), 0);
    QCOMPARE(stowage->load_count(call1ESALG), 28);

    QCOMPARE(stowage->discharge_count(call2NGTIN), 13);
    QCOMPARE(stowage->restow_count(call2NGTIN), 7);
    QCOMPARE(stowage->load_count(call2NGTIN), 6);

    QCOMPARE(stowage->discharge_count(call3BJCOO), 6);
    QCOMPARE(stowage->restow_count(call3BJCOO), 6);
    QCOMPARE(stowage->load_count(call3BJCOO), 9);

    QCOMPARE(stowage->discharge_count(call4TGLFW), 15);
    QCOMPARE(stowage->restow_count(call4TGLFW), 2);
    QCOMPARE(stowage->load_count(call4TGLFW), 0);

    QCOMPARE(stowage->discharge_count(call5ESALG), 9);
    QCOMPARE(stowage->restow_count(call5ESALG), 0);
    QCOMPARE(stowage->load_count(call5ESALG), 0);

    QCOMPARE(stowage->discharge_count(callAfter), 0);
    QCOMPARE(stowage->restow_count(callAfter), 0);
    QCOMPARE(stowage->load_count(callAfter), 0);

    {
        // Bays 13-14-15 no overstows
        const Slot* slotNo130182 = document->vessel()->slotAt(BayRowTier(13,1,82));
        const Slot* slotNo130282 = document->vessel()->slotAt(BayRowTier(13,2,82));
        const Slot* slotNo130682 = document->vessel()->slotAt(BayRowTier(13,6,82));
        const Slot* slotNo140082 = document->vessel()->slotAt(BayRowTier(14,0,82));
        const Slot* slotNo140382 = document->vessel()->slotAt(BayRowTier(14,3,82));
        const Slot* slotNo140482 = document->vessel()->slotAt(BayRowTier(14,4,82));
        const Slot* slotNo150182 = document->vessel()->slotAt(BayRowTier(15,1,82));
        const Slot* slotNo150282 = document->vessel()->slotAt(BayRowTier(15,2,82));
        const Slot* slotNo150682 = document->vessel()->slotAt(BayRowTier(15,6,82));

        QCOMPARE(stowage->overstowCounter()->overstowMove(callBefore, slotNo130182), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call1ESALG, slotNo130182), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call2NGTIN, slotNo130182), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call3BJCOO, slotNo130182), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call4TGLFW, slotNo130182), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call5ESALG, slotNo130182), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(callAfter, slotNo130182), OverstowCounter::None);

        QCOMPARE(stowage->overstowCounter()->overstowMove(callBefore, slotNo130282), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call1ESALG, slotNo130282), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call2NGTIN, slotNo130282), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call3BJCOO, slotNo130282), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call4TGLFW, slotNo130282), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call5ESALG, slotNo130282), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(callAfter, slotNo130282), OverstowCounter::None);

        QCOMPARE(stowage->overstowCounter()->overstowMove(callBefore, slotNo130682), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call1ESALG, slotNo130682), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call2NGTIN, slotNo130682), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call3BJCOO, slotNo130682), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call4TGLFW, slotNo130682), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call5ESALG, slotNo130682), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(callAfter, slotNo130682), OverstowCounter::None);

        QCOMPARE(stowage->overstowCounter()->overstowMove(callBefore, slotNo140082), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call1ESALG, slotNo140082), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call2NGTIN, slotNo140082), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call3BJCOO, slotNo140082), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call4TGLFW, slotNo140082), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call5ESALG, slotNo140082), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(callAfter, slotNo140082), OverstowCounter::None);

        QCOMPARE(stowage->overstowCounter()->overstowMove(callBefore, slotNo140382), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call1ESALG, slotNo140382), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call2NGTIN, slotNo140382), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call3BJCOO, slotNo140382), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call4TGLFW, slotNo140382), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call5ESALG, slotNo140382), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(callAfter, slotNo140382), OverstowCounter::None);

        QCOMPARE(stowage->overstowCounter()->overstowMove(callBefore, slotNo140482), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call1ESALG, slotNo140482), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call2NGTIN, slotNo140482), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call3BJCOO, slotNo140482), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call4TGLFW, slotNo140482), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call5ESALG, slotNo140482), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(callAfter, slotNo140482), OverstowCounter::None);

        QCOMPARE(stowage->overstowCounter()->overstowMove(callBefore, slotNo150182), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call1ESALG, slotNo150182), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call2NGTIN, slotNo150182), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call3BJCOO, slotNo150182), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call4TGLFW, slotNo150182), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call5ESALG, slotNo150182), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(callAfter, slotNo150182), OverstowCounter::None);

        QCOMPARE(stowage->overstowCounter()->overstowMove(callBefore, slotNo150282), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call1ESALG, slotNo150282), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call2NGTIN, slotNo150282), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call3BJCOO, slotNo150282), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call4TGLFW, slotNo150282), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call5ESALG, slotNo150282), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(callAfter, slotNo150282), OverstowCounter::None);

        QCOMPARE(stowage->overstowCounter()->overstowMove(callBefore, slotNo150682), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call1ESALG, slotNo150682), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call2NGTIN, slotNo150682), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call3BJCOO, slotNo150682), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call4TGLFW, slotNo150682), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call5ESALG, slotNo150682), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(callAfter, slotNo150682), OverstowCounter::None);
    }

    {
        // Bays 17-18-19 mixed overstows (lid+container)
        const Slot* slot170482 = document->vessel()->slotAt(BayRowTier(17,4,82));
        const Slot* slot170484 = document->vessel()->slotAt(BayRowTier(17,4,84));
        const Slot* slot170602 = document->vessel()->slotAt(BayRowTier(17,6,2));
        const Slot* slot180302 = document->vessel()->slotAt(BayRowTier(18,3,2));
        const Slot* slot180382 = document->vessel()->slotAt(BayRowTier(18,3,82));
        const Slot* slot180384 = document->vessel()->slotAt(BayRowTier(18,3,84));
        const Slot* slot190482 = document->vessel()->slotAt(BayRowTier(19,4,82));
        const Slot* slot190484 = document->vessel()->slotAt(BayRowTier(19,4,84));
        const Slot* slot190602 = document->vessel()->slotAt(BayRowTier(19,6,2));

        /*
        *    Equipment No    Type    POL POD Location    Moves
        *    ZZZU7284139 22G0    ESALG   NGTIN   17,4,82 ESALG:NGTIN
        *    ZZZU7284158 42G0    ESALG   NGTIN   18,3,82 ESALG:NGTIN
        *    ZZZU7284129 22G0    ESALG   NGTIN   19,4,82 ESALG:NGTIN
        *    ZZZU6652891 42G0    ESALG   TGLFW   18,3,84 ESALG:TGLFW
        *    ZZZU6652889 42G0    ESALG   TGLFW   18,3,2  ESALG:TGLFW
        *    ZZZU6652873 22G0    ESALG   TGLFW   19,6,2  ESALG:TGLFW
        *    ZZZU6652872 22G0    ESALG   TGLFW   17,4,84 ESALG:TGLFW
        *    ZZZU6652863 22G0    ESALG   TGLFW   17,6,2  ESALG:TGLFW
        *    ZZZU6652862 22G0    ESALG   TGLFW   19,4,84 ESALG:TGLFW
        *    ZZZU6652921 42G0    NGTIN   BJCOO   18,3,82 NGTIN:BJCOO
        *    ZZZU6652915 22G0    NGTIN   BJCOO   19,4,82 NGTIN:BJCOO
        *    ZZZU6652913 22G0    NGTIN   BJCOO   17,4,82 NGTIN:BJCOO
        *    ZZZU7284189 42G0    BJCOO   ESALG   18,3,82 BJCOO:ESALG
        *    ZZZU7284178 22G0    BJCOO   ESALG   17,4,82 BJCOO:ESALG
        *    ZZZU7284175 22G0    BJCOO   ESALG   19,4,82 BJCOO:ESALG
        */

        QCOMPARE(stowage->overstowCounter()->overstowMove(callBefore, slot170482), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call1ESALG, slot170482), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call2NGTIN, slot170482), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call3BJCOO, slot170482), OverstowCounter::Later);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call4TGLFW, slot170482), OverstowCounter::Now);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call5ESALG, slot170482), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(callAfter, slot170482), OverstowCounter::None);

        QCOMPARE(stowage->overstowCounter()->overstowMove(callBefore, slot170484), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call1ESALG, slot170484), OverstowCounter::Later);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call2NGTIN, slot170484), OverstowCounter::Now);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call3BJCOO, slot170484), OverstowCounter::Now);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call4TGLFW, slot170484), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call5ESALG, slot170484), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(callAfter, slot170484), OverstowCounter::None);

        QCOMPARE(stowage->overstowCounter()->overstowMove(callBefore, slot170602), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call1ESALG, slot170602), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call2NGTIN, slot170602), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call3BJCOO, slot170602), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call4TGLFW, slot170602), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call5ESALG, slot170602), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(callAfter, slot170602), OverstowCounter::None);

        QCOMPARE(stowage->overstowCounter()->overstowMove(callBefore, slot180302), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call1ESALG, slot180302), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call2NGTIN, slot180302), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call3BJCOO, slot180302), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call4TGLFW, slot180302), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call5ESALG, slot180302), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(callAfter, slot180302), OverstowCounter::None);

        QCOMPARE(stowage->overstowCounter()->overstowMove(callBefore, slot180382), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call1ESALG, slot180382), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call2NGTIN, slot180382), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call3BJCOO, slot180382), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call4TGLFW, slot180382), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call5ESALG, slot180382), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(callAfter, slot180382), OverstowCounter::None);

        QCOMPARE(stowage->overstowCounter()->overstowMove(callBefore, slot180384), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call1ESALG, slot180384), OverstowCounter::Later);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call2NGTIN, slot180384), OverstowCounter::Now);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call3BJCOO, slot180384), OverstowCounter::Now);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call4TGLFW, slot180384), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call5ESALG, slot180384), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(callAfter, slot180384), OverstowCounter::None);

        QCOMPARE(stowage->overstowCounter()->overstowMove(callBefore, slot190482), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call1ESALG, slot190482), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call2NGTIN, slot190482), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call3BJCOO, slot190482), OverstowCounter::Later);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call4TGLFW, slot190482), OverstowCounter::Now);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call5ESALG, slot190482), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(callAfter, slot190482), OverstowCounter::None);

        QCOMPARE(stowage->overstowCounter()->overstowMove(callBefore, slot190484), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call1ESALG, slot190484), OverstowCounter::Later);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call2NGTIN, slot190484), OverstowCounter::Now);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call3BJCOO, slot190484), OverstowCounter::Now);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call4TGLFW, slot190484), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call5ESALG, slot190484), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(callAfter, slot190484), OverstowCounter::None);

        QCOMPARE(stowage->overstowCounter()->overstowMove(callBefore, slot190602), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call1ESALG, slot190602), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call2NGTIN, slot190602), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call3BJCOO, slot190602), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call4TGLFW, slot190602), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call5ESALG, slot190602), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(callAfter, slot190602), OverstowCounter::None);
    }

    {
        // Bays 21-22-23 lid overstows
        const Slot* slot210682 = document->vessel()->slotAt(BayRowTier(21,6,82));
        const Slot* slot210904 = document->vessel()->slotAt(BayRowTier(21,9,4));
        const Slot* slot220082 = document->vessel()->slotAt(BayRowTier(22,0,82));
        const Slot* slot220582 = document->vessel()->slotAt(BayRowTier(22,5,82));
        const Slot* slot220502 = document->vessel()->slotAt(BayRowTier(22,5,2));
        const Slot* slot230682 = document->vessel()->slotAt(BayRowTier(23,6,82));
        const Slot* slot230904  = document->vessel()->slotAt(BayRowTier(23,9,4 ));

        QCOMPARE(stowage->overstowCounter()->overstowMove(callBefore, slot210682), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call1ESALG, slot210682), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call2NGTIN, slot210682), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call3BJCOO, slot210682), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call4TGLFW, slot210682), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call5ESALG, slot210682), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(callAfter, slot210682), OverstowCounter::None);

        QCOMPARE(stowage->overstowCounter()->overstowMove(callBefore, slot210904), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call1ESALG, slot210904), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call2NGTIN, slot210904), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call3BJCOO, slot210904), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call4TGLFW, slot210904), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call5ESALG, slot210904), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(callAfter, slot210904), OverstowCounter::None);

        QCOMPARE(stowage->overstowCounter()->overstowMove(callBefore, slot220082), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call1ESALG, slot220082), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call2NGTIN, slot220082), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call3BJCOO, slot220082), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call4TGLFW, slot220082), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call5ESALG, slot220082), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(callAfter, slot220082), OverstowCounter::None);

        QCOMPARE(stowage->overstowCounter()->overstowMove(callBefore, slot220582), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call1ESALG, slot220582), OverstowCounter::Later);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call2NGTIN, slot220582), OverstowCounter::Now);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call3BJCOO, slot220582), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call4TGLFW, slot220582), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call5ESALG, slot220582), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(callAfter, slot220582), OverstowCounter::None);

        QCOMPARE(stowage->overstowCounter()->overstowMove(callBefore, slot220502), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call1ESALG, slot220502), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call2NGTIN, slot220502), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call3BJCOO, slot220502), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call4TGLFW, slot220502), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call5ESALG, slot220502), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(callAfter, slot220502), OverstowCounter::None);

        QCOMPARE(stowage->overstowCounter()->overstowMove(callBefore, slot230682), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call1ESALG, slot230682), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call2NGTIN, slot230682), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call3BJCOO, slot230682), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call4TGLFW, slot230682), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call5ESALG, slot230682), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(callAfter, slot230682), OverstowCounter::None);

        QCOMPARE(stowage->overstowCounter()->overstowMove(callBefore, slot230904), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call1ESALG, slot230904), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call2NGTIN, slot230904), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call3BJCOO, slot230904), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call4TGLFW, slot230904), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call5ESALG, slot230904), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(callAfter, slot230904), OverstowCounter::None);
    }

    {
        // Bays 25-26-27 container overstows
        const Slot* slot250682 = document->vessel()->slotAt(BayRowTier(25,6,82));
        const Slot* slot250684 = document->vessel()->slotAt(BayRowTier(25,6,84));
        const Slot* slot260582 = document->vessel()->slotAt(BayRowTier(26,5,82));
        const Slot* slot260584 = document->vessel()->slotAt(BayRowTier(26,5,84));
        const Slot* slot270682 = document->vessel()->slotAt(BayRowTier(27,6,82));
        const Slot* slot270684 = document->vessel()->slotAt(BayRowTier(27,6,84));

        QCOMPARE(stowage->overstowCounter()->overstowMove(callBefore, slot250682), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call1ESALG, slot250682), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call2NGTIN, slot250682), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call3BJCOO, slot250682), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call4TGLFW, slot250682), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call5ESALG, slot250682), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(callAfter, slot250682), OverstowCounter::None);

        QCOMPARE(stowage->overstowCounter()->overstowMove(callBefore, slot250684), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call1ESALG, slot250684), OverstowCounter::Later);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call2NGTIN, slot250684), OverstowCounter::Now);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call3BJCOO, slot250684), OverstowCounter::Now);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call4TGLFW, slot250684), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call5ESALG, slot250684), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(callAfter, slot250684), OverstowCounter::None);

        QCOMPARE(stowage->overstowCounter()->overstowMove(callBefore, slot260582), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call1ESALG, slot260582), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call2NGTIN, slot260582), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call3BJCOO, slot260582), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call4TGLFW, slot260582), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call5ESALG, slot260582), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(callAfter, slot260582), OverstowCounter::None);

        QCOMPARE(stowage->overstowCounter()->overstowMove(callBefore, slot260584), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call1ESALG, slot260584), OverstowCounter::Later);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call2NGTIN, slot260584), OverstowCounter::Now);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call3BJCOO, slot260584), OverstowCounter::Now);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call4TGLFW, slot260584), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call5ESALG, slot260584), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(callAfter, slot260584), OverstowCounter::None);

        QCOMPARE(stowage->overstowCounter()->overstowMove(callBefore, slot270682), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call1ESALG, slot270682), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call2NGTIN, slot270682), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call3BJCOO, slot270682), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call4TGLFW, slot270682), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call5ESALG, slot270682), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(callAfter, slot270682), OverstowCounter::None);

        QCOMPARE(stowage->overstowCounter()->overstowMove(callBefore, slot270684), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call1ESALG, slot270684), OverstowCounter::Later);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call2NGTIN, slot270684), OverstowCounter::Now);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call3BJCOO, slot270684), OverstowCounter::Now);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call4TGLFW, slot270684), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(call5ESALG, slot270684), OverstowCounter::None);
        QCOMPARE(stowage->overstowCounter()->overstowMove(callAfter, slot270684), OverstowCounter::None);
    }

    delete document;
}

void OverstowTest::scheduleSwapTest() {
    document_t* document = FileOpener::openStoFile(QFINDTESTDATA("test-data/1330W_HANSA_ALG_OVERSTOW.sto")).document;
    stowage_t* stowage = document->stowage();
    ange::schedule::Schedule* schedule = document->schedule();
    const ange::vessel::Vessel* vessel = document->vessel();
    const Call* callNGTIN = schedule->at(2);
    QCOMPARE(callNGTIN->uncode(), QString("NGTIN"));
    const Call* callBJCOO = schedule->at(3);
    QCOMPARE(callBJCOO->uncode(), QString("BJCOO"));

    { // Get overstowMove for ZZZU7284175 into cache
        const Slot* slot190482 = vessel->slotAt(BayRowTier(19, 4, 82));
        const Container* containerZZZU7284175 = stowage->contents(slot190482, callBJCOO).container();
        QCOMPARE(QString(containerZZZU7284175->equipmentNumber()), QString("ZZZU7284175"));
        QCOMPARE(stowage->overstowCounter()->overstowMove(callBJCOO, slot190482), OverstowCounter::Later);
    }

    // Move call TWLFW up
    document->undostack()->push(ScheduleChangeRotationCommand::changeOfRotation(schedule, 4, 3));

    { // Check that overstowMove for ZZZU7284175 is cleared from cache
        const Slot* slot190482 = vessel->slotAt(BayRowTier(19, 4, 82));
        const Container* containerZZZU7284175 = stowage->contents(slot190482, callBJCOO).container();
        QCOMPARE(QString(containerZZZU7284175->equipmentNumber()), QString("ZZZU7284175"));
        // Returned OverstowCounter::Later before fix with OverstowCounter::reset()
        QCOMPARE(stowage->overstowCounter()->overstowMove(callBJCOO, slot190482), OverstowCounter::None);
    }

    { // Unstow container ZZZU6652921, causes problems in stowage_t::unmark_overstow with finding overstow_move and slot_call_content_t
        const Slot* slot180382 = vessel->slotAt(BayRowTier(18, 3, 82));
        const Container* containerZZZU6652921 = stowage->contents(slot180382, callNGTIN).container();
        QCOMPARE(QString(containerZZZU6652921->equipmentNumber()), QString("ZZZU6652921"));
        container_stow_command_t* unstowCommand1 = new container_stow_command_t(document);
        unstowCommand1->clearMoves(containerZZZU6652921);
        document->undostack()->push(unstowCommand1);
    }

    { // Unstow containers ZZZU6652863 and ZZZU6652873, causes problems in stowage_t::remove_old_overstows with bad call order
        const Slot* slot170602 = vessel->slotAt(BayRowTier(17, 6, 2));
        const Container* containerZZZU6652863 = stowage->contents(slot170602, callNGTIN).container();
        QCOMPARE(QString(containerZZZU6652863->equipmentNumber()), QString("ZZZU6652863"));
        const Slot* slot190602 = vessel->slotAt(BayRowTier(19, 6, 2));
        const Container* containerZZZU6652873 = stowage->contents(slot190602, callNGTIN).container();
        QCOMPARE(QString(containerZZZU6652873->equipmentNumber()), QString("ZZZU6652873"));
        container_stow_command_t* unstowCommand2 = new container_stow_command_t(document);
        unstowCommand2->clearMoves(containerZZZU6652863);
        unstowCommand2->clearMoves(containerZZZU6652873);
        document->undostack()->push(unstowCommand2);
    }

    { // Unstow container ZZZU7284189, causes problems in stowage_t::unmark_overstow with finding Container
        const Slot* slot180382 = vessel->slotAt(BayRowTier(18, 3, 82));
        const Container* containerZZZU7284189 = stowage->contents(slot180382, callBJCOO).container();
        QCOMPARE(QString(containerZZZU7284189->equipmentNumber()), QString("ZZZU7284189"));
        container_stow_command_t* unstowCommand3 = new container_stow_command_t(document);
        unstowCommand3->clearMoves(containerZZZU7284189);
        document->undostack()->push(unstowCommand3);
    }

    delete document;
}

/*
 * This test never failed consistently. The crash we expirienced first was when crane_split_t::modify_move()
 * called Container::isoLength(), this test has only succeded in failing by adding a call to
 * Container::equipmentNumber() in crane_split_t::modify_move().
 * Now we don't store Container* in OverstowCounter any more, this test is saved in order to document how to test this
 * problem an other time.
 */
void OverstowTest::flushDeletedContainersFromCache() {
    document_t* document = FileOpener::openStoFile(QFINDTESTDATA("test-data/tiny_overstow_one.sto")).document;
    stowage_t* stowage = document->stowage();

    // Update caches, this will store a pointer to container ZZZU8459933
    stowage->overstowCounter()->updateAllCaches();

    // Unstow and delete ZZZU8459933
    const Slot* slot304 = document->vessel()->slotAt(BayRowTier(3,0,4));
    const Call* callA = document->schedule()->calls().at(1);
    const Container* containerZZZU8459933 = stowage->contents(slot304, callA).container();
    QCOMPARE(QString(containerZZZU8459933->equipmentNumber()), QString("ZZZU8459933"));
    container_stow_command_t* unstowCommand = new container_stow_command_t(document);
    unstowCommand->clearMoves(containerZZZU8459933);
    document->undostack()->push(unstowCommand);
    delete containerZZZU8459933;

    // Update caches again
    // This can cause problems if the deleted container is still referenced from the cache
    stowage->overstowCounter()->updateAllCaches();

    delete document;
}

QTEST_GUILESS_MAIN(OverstowTest);

#include "overstowtest.moc"
