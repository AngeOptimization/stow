#include <QtTest>

#include "container_list.h"
#include "document.h"
#include "fileopener.h"
#include "stowage.h"
#include "stowarbiter.h"

#include <ange/schedule/schedule.h>
#include <ange/vessel/slot.h>
#include <ange/vessel/vessel.h>

#include <QObject>

using namespace ange::containers;
using namespace ange::schedule;
using namespace ange::vessel;

class StowArbiterTest : public QObject {
    Q_OBJECT
private Q_SLOTS:
    void testCalls();
};

QTEST_GUILESS_MAIN(StowArbiterTest);

void StowArbiterTest::testCalls() {
    document_t* document = FileOpener::openStoFile(QFINDTESTDATA("test-data/simple-oog.sto")).document;
    stowage_t* stowage = document->stowage();
    Schedule* schedule = document->schedule();
    QCOMPARE(schedule->size(), 4);
    const Slot* slot = document->vessel()->slotAt(BayRowTier(2, 1, 80));
    QVERIFY(slot);
    const Container* container = document->containers()->get_container_by_equipment_number("ZZZU6934218");
    QVERIFY(container);
    QVERIFY(stowage->moves(container).isEmpty());

    // From is before POL
    StowArbiter arbiter0(stowage, schedule, slot, schedule->at(0));
    QCOMPARE(arbiter0.isLegal(container), false);
    QSet<int> whyNot0 = arbiter0.whyNot(container);
    QCOMPARE(whyNot0.size(), 1);
    QCOMPARE(*whyNot0.begin(), int(StowArbiter::FROM_BEFORE_POL)); // First element of set
    QCOMPARE(arbiter0.toString(StowArbiter::FROM_BEFORE_POL), QString("Attempted load call is before POL on container"));

    // From is at POL
    StowArbiter arbiter1(stowage, schedule, slot, schedule->at(1));
    QCOMPARE(arbiter1.isLegal(container), true);

    // From is at POD
    StowArbiter arbiter2(stowage, schedule, slot, schedule->at(2));
    QCOMPARE(arbiter2.isLegal(container), false);
    QSet<int> whyNot2 = arbiter2.whyNot(container);
    QCOMPARE(whyNot2.size(), 1);
    QCOMPARE(*whyNot2.begin(), int(StowArbiter::FROM_AT_OR_AFTER_POD)); // First element of set
    QCOMPARE(arbiter2.toString(StowArbiter::FROM_AT_OR_AFTER_POD), QString("Attempted load call is at or after POD on container"));

    // From is after POD
    StowArbiter arbiter3(stowage, schedule, slot, schedule->at(3));
    QCOMPARE(arbiter3.isLegal(container), false);
    QSet<int> whyNot3 = arbiter3.whyNot(container);
    QCOMPARE(whyNot3.size(), 1);
    QCOMPARE(*whyNot3.begin(), int(StowArbiter::FROM_AT_OR_AFTER_POD)); // First element of set
    QCOMPARE(arbiter3.toString(StowArbiter::FROM_AT_OR_AFTER_POD), QString("Attempted load call is at or after POD on container"));

    delete document;
}

#include "stowarbitertest.moc"
