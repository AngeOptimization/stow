#include <QTest>

#include "fileopener.h"
#include "stowagestatusaggregator.h"
#include "stowarbiter.h"

#include "test/util/vesseltestutils.h"

#include <ange/containers/oog.h>

using namespace ange::containers;
using namespace ange::schedule;
using namespace ange::vessel;

/**
 * A class to test that OOGs forbid neighbouring slots the way they should.
 */
class OogTest : public QObject {
    Q_OBJECT
private Q_SLOTS:
    void testOogLegality();
};

void OogTest::testOogLegality() {
    document_t* document = FileOpener::openStoFile(QFINDTESTDATA("test-data/simple-oog.sto")).document;
    const Slot* slot1 = document->vessel()->slotAt(BayRowTier(2, 1, 80));
    const Slot* slot2 = document->vessel()->slotAt(BayRowTier(2, 2, 80));
    const Container* container40DC = document->containers()->get_container_by_equipment_number("ZZZU6934218");
    const Container* containerOOG = document->containers()->get_container_by_equipment_number("ZZZU1422448");
    QVERIFY(slot1);
    QVERIFY(slot2);
    QVERIFY(container40DC);
    QVERIFY(containerOOG);
    QVERIFY(containerOOG->oog().left() > 10.0 * centimeter);
    QVERIFY(containerOOG->oog().right() > 10.0 * centimeter);
    const Schedule* schedule = document->schedule();
    container_stow_command_t* oogStowCommand = new container_stow_command_t(document);
    oogStowCommand->add_stow(containerOOG, slot1, schedule->at(1), schedule->at(2), ange::angelstow::MicroStowedType);
    document->undostack()->push(oogStowCommand);
    StowArbiter arbiter(document->stowage(), schedule, slot2, schedule->at(1));
    QVERIFY(!arbiter.isLegal(container40DC));
    container_stow_command_t* oogUnStowCommand = new container_stow_command_t(document);
    oogUnStowCommand->add_discharge(containerOOG, schedule->at(1), ange::angelstow::MicroStowedType);
    document->undostack()->push(oogUnStowCommand);
    StowArbiter arbiter2(document->stowage(), schedule, slot2, schedule->at(1));
    QVERIFY2(arbiter2.isLegal(container40DC), "After unstowing the OOG, the 40DC should now be placeable next to its former position.");
    delete document;
}

QTEST_GUILESS_MAIN(OogTest);

#include "oogtest.moc"
