#ifndef PASSIVENOTIFICATIONS_H
#define PASSIVENOTIFICATIONS_H

#include <QObject>

class document_t;
class QIcon;

class PassiveNotification {
public:
    PassiveNotification(const QString& message, const QString& buttonText, const QIcon& buttonIcon,
                        QObject* buttonReceiver, const char* buttonSlot);
public:
    const QString& message;
    const QString& buttonText;
    const QIcon& buttonIcon;
    QObject* buttonReceiver;
    const char* buttonSlot;
};

/**
 * The passive notifications for the document
 */
class PassiveNotifications : public QObject {
    Q_OBJECT

public:

    /**
     * @param document is parent
     */
    PassiveNotifications(document_t* document);
    ~PassiveNotifications();

    /**
     * Add a passive warning, with a button to activate some action.
     * Example: If the offered action is "undo", dismiss when the undo stack has changed
     */
    void addWarning(const QString& message, const QString& buttonText, const QIcon& buttonIcon,
                    QObject* buttonReceiver, const char* buttonSlot);

    /**
     * Add a passive warning with an undo button.
     */
    void addWarningWithUndo(const QString& message);

Q_SIGNALS:

    void warningAdded(PassiveNotification warning);

private:
    document_t* m_document;

};

#endif // PASSIVENOTIFICATIONS_H
