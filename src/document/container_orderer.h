#ifndef CONTAINER_ORDERER_H
#define CONTAINER_ORDERER_H

#include <QString>

class container_orderer_t {
  public:
    enum container_placement_order {
      WEIGHT,
      SELECTION_ORDER,
    };
    static container_placement_order string2enum(const QString& value) {
      if(value == "Weight") {
        return WEIGHT;
      }
      if(value == "Selection order") {
        return SELECTION_ORDER;
      }
      Q_ASSERT(false);
      return WEIGHT; // to do something sane-ish in release mode
    };
};

#endif // CONTAINER_ORDERER_H
