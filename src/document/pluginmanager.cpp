#include "pluginmanager.h"

#include "document.h"
#include "document_interface.h"
#include "problem_model.h"
#include "stowarbiter.h"
#include "version.h"

#include <stowplugininterface/documentusinginterface.h>
#include <stowplugininterface/guiusinginterface.h>
#include <stowplugininterface/masterplanningreporterinterface.h>
#include <stowplugininterface/pluginlicenseexception.h>
#include <stowplugininterface/problemaddinginterface.h>
#include <stowplugininterface/stabilityreporterinterface.h>
#include <stowplugininterface/masterplanningplugin.h>
#include <stowplugininterface/stowplugininterface_version.h>
#include <stowplugininterface/validatorplugin.h>

#include <QDebug>
#include <QPluginLoader>

using ange::angelstow::DocumentUsingInterface;
using ange::angelstow::ProblemAddingInterface;
using ange::angelstow::GuiUsingInterface;
using ange::angelstow::ballast_tanks_plugin_t;
using ange::angelstow::MasterPlanningPlugin;
using ange::angelstow::ValidatorPlugin;
using ange::angelstow::StabilityReporterInterface;
using ange::angelstow::MasterPlanningReporterInterface;

PluginManager::PluginManager(QObject* parent): QObject(parent), m_nextReservedReason(StowArbiter::FIRST_PLUGIN_ERROR)
{
    setObjectName("PluginManager");
}

PluginManager::~PluginManager() {
    Q_FOREACH (const PluginHandle& pluginHandle, m_pluginHandles) {
        if (pluginHandle.plugin) {
            delete pluginHandle.plugin;
        }
    }
}

static QList<QString> thisDirAndAllSubDirs(const QString& dir) {
    QList<QString> list;
    list << dir;
    Q_FOREACH (const QFileInfo& fileinfo, QDir(dir).entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot, QDir::Name)) {
        list << thisDirAndAllSubDirs(fileinfo.absoluteFilePath());
    }
    return list;
}

void PluginManager::loadPluginsRecursive(const QString& directoryPath) {
    Q_FOREACH (const QString& subDirectoryPath, thisDirAndAllSubDirs(directoryPath)) {
        m_loadedDirectories << subDirectoryPath;
        Q_FOREACH (QFileInfo fileInfo, QDir(subDirectoryPath).entryInfoList(QStringList() << "*" PLUGIN_SUFFIX)) {
            loadPlugin(fileInfo.absoluteFilePath());
        }
    }
}

void PluginManager::loadPlugin(const QString& filePath) {
    QPluginLoader pluginLoader(filePath);
    m_pluginHandles << PluginHandle(filePath, &pluginLoader);
    PluginHandle& handle = m_pluginHandles.last(); // Use reference to make early return easy

    int expectedInterfaceVersion = STOW_PLUGIN_INTERFACE_SOVERSION;
    if (handle.angelstowInterfaceVersion == -1) {
        handle.status = "Plugin too old, misses version info";
        return;
    }
    if (handle.angelstowInterfaceVersion != expectedInterfaceVersion) {
        handle.status = QString("Plugin with wrong interface version, had %1 expected %2")
                                .arg(handle.angelstowInterfaceVersion).arg(expectedInterfaceVersion);
        return;
    }
    if (pluginWithNameIsLoaded(handle.name)) {
        handle.status = QString("Plugin with same name already loaded");
        return;
    }
    try {
        // Loads plugin here
        QObject* plugin =  pluginLoader.instance();
        if (!pluginLoader.isLoaded()) {
            handle.status = pluginLoader.errorString();
            qWarning() << "Error when loading plugin " << filePath << " : " << handle.status;
            return;
        }
        // Success!
        handle.plugin = plugin;
        handle.status = "Successfully loaded";
    } catch (const ange::angelstow::PluginLicenseException& e) {
        handle.status = "Plugin with license issues: " + e.reason();
        return;
    }

    ValidatorPlugin* validatorPlugin = qobject_cast<ValidatorPlugin*>(handle.plugin);
    if (validatorPlugin) {
        validatorPlugin->initializeReasonReserver(this);
        m_validatorPlugins << validatorPlugin;
    }
}

bool PluginManager::pluginWithNameIsLoaded(QString pluginName) {
    Q_FOREACH (const PluginHandle& pluginHandle, m_pluginHandles) {
        if (pluginHandle.plugin && pluginHandle.name == pluginName) {
            return true;
        }
    }
    return false;
}

QList<PluginHandle> PluginManager::pluginHandles() {
    return m_pluginHandles;
}

MasterPlanningPlugin* PluginManager::masterPlanningPlugin() {
    Q_FOREACH (const PluginHandle& pluginHandle, m_pluginHandles) {
        MasterPlanningPlugin* castPlugin = qobject_cast<MasterPlanningPlugin*>(pluginHandle.plugin);
        if (castPlugin) {
            return castPlugin;
        }
    }
    return 0;
}

QList<ValidatorPlugin*> PluginManager::validatorPlugins() {
    return m_validatorPlugins;
}

QList<StabilityReporterInterface*> PluginManager::stabilityReporterPlugins() {
    QList<StabilityReporterInterface*> list;
    Q_FOREACH (const PluginHandle& pluginHandle, m_pluginHandles) {
        StabilityReporterInterface* castPlugin = qobject_cast<StabilityReporterInterface*>(pluginHandle.plugin);
        if (castPlugin) {
            list << castPlugin;
        }
    }
    return list;
}

QList<MasterPlanningReporterInterface*> PluginManager::masterPlanningReporterPlugins() {
    QList<MasterPlanningReporterInterface*> reporterInterfacelist;
    Q_FOREACH (const PluginHandle& pluginHandle, m_pluginHandles) {
        MasterPlanningReporterInterface* castPlugin = qobject_cast<MasterPlanningReporterInterface*>(pluginHandle.plugin);
        if (castPlugin) {
            reporterInterfacelist << castPlugin;
        }
    }
    return reporterInterfacelist;
}

void PluginManager::documentReset(document_t* document) {
    Q_FOREACH (const PluginHandle& pluginHandle, m_pluginHandles) {
        if(ProblemAddingInterface* problemadding = qobject_cast<ProblemAddingInterface*>(pluginHandle.plugin)) {
            problemadding->setProblemAdder(document->problem_model());
        }
        DocumentUsingInterface* castPlugin = qobject_cast<DocumentUsingInterface*>(pluginHandle.plugin);
        if (castPlugin) {
            castPlugin->documentReset(document->documentInterface());
        }
    }
}

void PluginManager::initializeGui(ange::angelstow::IGui* gui) {
    Q_FOREACH (const PluginHandle& pluginHandle, m_pluginHandles) {
        GuiUsingInterface* castPlugin = qobject_cast<GuiUsingInterface*>(pluginHandle.plugin);
        if (castPlugin) {
            castPlugin->initializeGui(gui);
        }
    }
}

int PluginManager::reserveReason(ange::angelstow::ValidatorPlugin* validatorPlugin) {
    m_pluginForReason.insert(m_nextReservedReason, validatorPlugin);
    return m_nextReservedReason++;
}

ange::angelstow::ValidatorPlugin* PluginManager::pluginForReason(int reason) {
    return m_pluginForReason.value(reason, 0);
}

PluginHandle::PluginHandle()
    : filePath("Unknown file"), status("Unknown error"), angelstowInterfaceVersion(-2), plugin(0L)
{
    // Empty
}

PluginHandle::PluginHandle(const QString& filePath, const QPluginLoader* pluginLoader)
    : filePath(filePath), plugin(0L)
{
    const QJsonObject userData = pluginLoader->metaData().value("MetaData").toObject();
    author = userData.value("AuthorInfo").toObject().value("Author").toString();
    authorLink = userData.value("AuthorInfo").toObject().value("AuthorLink").toString();
    licenseType = userData.value("LicenseInfo").toObject().value("LicenseType").toString();
    licenseText = userData.value("LicenseInfo").toObject().value("LicenseText").toString();
    name = userData.value("PluginName").toString();
    pluginVersion = userData.value("PluginVersion").toString();
    angelstowInterfaceVersion = userData.value("AngelstowInterfaceVersion").toInt(-1);
}

#include "pluginmanager.moc"
