#ifndef PLUGINMANAGER_H
#define PLUGINMANAGER_H

#include <QObject>
#include "stowplugininterface/reasonreserverinterface.h"

#include <QDir>
#include <QHash>
#include <QList>

class document_t;

class QPluginLoader;
namespace ange {
namespace angelstow {
class DocumentUsingInterface;
class IGui;
class ValidatorPlugin;
class ballast_tanks_plugin_t;
class MasterPlanningPlugin;
class StabilityReporterInterface;
class MasterPlanningReporterInterface;
}
}

/**
 * The knowledge we have about a plugin
 */
class PluginHandle {

public:

    /**
     * Used by QHash<*, PluginHandle>, not for general use
     */
    PluginHandle();

    /**
     * Used for pre filling the handle based on meta data from QPluginLoader
     */
    PluginHandle(const QString& filePath, const QPluginLoader* pluginLoader);

public:
    // Where we have found the plugin file:
    QString filePath;
    // Data from the JSON file:
    QString name;
    QString author;
    QString licenseType;
    QString licenseText;
    QString status;
    QString authorLink;
    QString pluginVersion;
    int angelstowInterfaceVersion;

    // The loaded plugin, will be nullprt if the load failed
    QObject* plugin;
    // TODO make plugin a member var and add getter, this will allow a usefull const PluginHandle

};

/**
 * Manages the plugins
 */
class PluginManager : public QObject, public ange::angelstow::ReasonReserverInterface {

    Q_OBJECT
    Q_INTERFACES(ange::angelstow::ReasonReserverInterface)

public:
    explicit PluginManager(QObject* parent = 0);

    virtual ~PluginManager();

    /**
     * Loads all plugins from the given directory and all its subdirectories using the loadPlugin() method.
     */
    void loadPluginsRecursive(const QString& directoryPath);

    /**
     * Load a single plugin. If a plugin with same name in the JSON data exists the plugin will not be loaded.
     */
    void loadPlugin(const QString& filePath);

    // Lists of plugins:

    /**
     * Get information about all plugins, both successfully loaded and failures
     */
    QList<PluginHandle> pluginHandles();

    /**
     * The single stowage plugin, will return nullprt if there is no stowage plugins.
     * TODO We don't handle multiple stowage plugins in a sensible way, we just expose the first and forget the rest.
     */
    ange::angelstow::MasterPlanningPlugin* masterPlanningPlugin();

    /**
     * List of all validator plugins
     */
    QList<ange::angelstow::ValidatorPlugin*> validatorPlugins();

    /**
     * List of all plugins that has contributions to the stability report
     */
    QList<ange::angelstow::StabilityReporterInterface*> stabilityReporterPlugins();

    /**
     * List of all plugins that has contributions to the master planning report
     */
    QList<ange::angelstow::MasterPlanningReporterInterface*> masterPlanningReporterPlugins();

    // Actions on plugins:

    /**
     * Calls documentReset(document) on all plugins implementing DocumentUsingInterface
     */
    void documentReset(document_t* document);

    /**
     * Calls initializeGui(gui) on all plugins implementing GuiUsingInterface
     */
    void initializeGui(ange::angelstow::IGui* gui);

    /**
     * Reserve a reason
     */
    int reserveReason(ange::angelstow::ValidatorPlugin* validatorPlugin);

    /**
     * @return validator plugin for reason
     */
    ange::angelstow::ValidatorPlugin* pluginForReason(int reason);

private:
    /**
     * Returns whether a plugin with the given name is already loaded
     */
    bool pluginWithNameIsLoaded(QString pluginName);

private:
    QList<QString> m_loadedDirectories;
    QList<PluginHandle> m_pluginHandles;
    QList<ange::angelstow::ValidatorPlugin*> m_validatorPlugins;

    QHash<int, ange::angelstow::ValidatorPlugin*> m_pluginForReason;
    int m_nextReservedReason;
};

#endif // PLUGINMANAGER_H
