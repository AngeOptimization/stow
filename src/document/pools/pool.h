#ifndef POOL_H
#define POOL_H

#include <QItemSelectionModel>
#include <QList>
#include <QObject>
#include <QSet>

class QTimer;
class QAbstractProxyModel;
class container_list_t;

namespace ange {
namespace containers {
class Container;
}
}

class pool_t : public QObject {
    Q_OBJECT

public:

    pool_t(container_list_t* container_list);

    /**
     * Get included rows in pool
     */
    QList<int> included_rows() const {
      return m_included_rows;
    }

    /**
     * @return true iff pool is empty
     */
    bool empty() const {
      return m_included_rows.empty();
    }

    /**
     * Restore pool to state
     */
    void reset_included_rows(QList<int> included_rows);

    /**
     * @return currently included containers
     */
    QList<const ange::containers::Container*> containers() const;

    /**
     * Synchronized list with selection model (to container_list_t* or to a proxy thereof)
     */
    void synchronize(QItemSelectionModel* selection_model);

public Q_SLOTS:

    /**
     * Include or exclude container
     * @param append if true, appends to bottom, otherwise inserts at top
     */
    void include(const ange::containers::Container* container, bool append = true, bool include = true);

    /**
     * Include group of containers
     */
    void include(QList<const ange::containers::Container*> containers);

    /**
     * Include group of containers
     */
    void include(QList<ange::containers::Container*> containers);

    /**
     * Include group of containers
     */
    void include(QList<int> rows);

    /**
     * Include group of containers
     */
    void include(QSet<const ange::containers::Container*> containers);

    /**
     * Exclude or include container
     */
    void exclude(const ange::containers::Container* container, bool exclude = true);

    /**
     * Exclude rows
     */
    void exclude(QList< int > rows);

    /**
     * Exclude group of containers
     */
    void exclude(QSet<const ange::containers::Container*> containers);

    /**
     * Remove all containers
     */
    void clear();

    /**
     * When synchronized, we need to deselect and adjust remaining indexes
     * when containers are deleted/removed from model to avoid
     * being unsynchronized
     */
    void slot_model_rows_removed(const QModelIndex& parent, int first, int last);

    /**
     * When synchronized, we need to adjust remaining indexes
     * when containers are deleted/removed from model to avoid
     * being unsynchronized
     */
    void slot_model_rows_inserted(const QModelIndex& parent, int first, int last);

private Q_SLOTS:
    /**
     * Meant to be connect to master container_list, if sync. to this is desired.
     */
    void slot_selection_changed(QItemSelection idx_selected, QItemSelection idx_deselected);

    /**
     * Called when the data in the undelying model changes
     */
    void dataChanged(const QModelIndex & topLeft, const QModelIndex & bottomRight);

    /**
     * Sync to selection_model
     */
    void sync_to_model();

Q_SIGNALS:
    /**
     * emitted if the selection has changed
     */
    void changed();
    /**
     * emitted if the selection's container count has changed to/from 0
     */
    void empty_changed(bool isempty);
    /**
     * emitted if the selection has been extended
     */
    void rows_added(QList<int> rows);
    /**
     * emitted if the selection has been reduced
     */
    void rows_removed(QList<int> rows);

private:
    void include_rows(const QList< int >& rowlist);

    container_list_t* m_container_list;
    QList<int> m_included_rows;
    QItemSelectionModel* m_synchronized_selection_model;
    QTimer* m_synchronize_timer;

    bool m_ignore_selection;

    mutable QList<const ange::containers::Container*> m_cachedIncludedContainers;
    mutable bool m_cachedIncludedContainersDirty;

private:
    // const violation, but needed for cache rebuilds
    void rebuildContainerCache() const;

};

#endif // POOL_H
