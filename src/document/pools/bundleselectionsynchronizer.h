#ifndef BUNDLESELECTIONSYNCHRONIZER_H
#define BUNDLESELECTIONSYNCHRONIZER_H

#include <QObject>
#include "pool.h"

/**
 * \brief helps synchronizing selecting and deselecting of bundles
 */

class container_list_t;
class BundleSelectionSynchronizer : public QObject {
    Q_OBJECT

    public:
        /**
         * constructor. Takes a seleciton pool to synchronize with.
         */
        BundleSelectionSynchronizer(container_list_t* containerList, pool_t* pool);
    private Q_SLOTS:
        void synchronizeRowsIncluded(QList<int> rows_included);
        void synchronizeRowsExcluded(QList<int> rows_excluded);
    private:
        container_list_t* m_containerList;
        pool_t* m_pool;
        bool m_synchronizing;
        /**
         * \return the list of rows with containers that are related to the containers in the list
         * \param rows containers to look for
         */
        QList< int > findRelatedContainerRow(QList< int > rows);
        /**
         * \return list of containers in a bundle with the container. including the container itself
         * \param container to check for
         */
        QList< const ange::containers::Container* > containersInBundles(const ange::containers::Container* container);
};

#endif // BUNDLESELECTIONSYNCHRONIZER_H
