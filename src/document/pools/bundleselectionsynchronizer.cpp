#include "bundleselectionsynchronizer.h"
#include <document/containers/container_list.h>
#include <ange/containers/container.h>

using ange::containers::Container;

BundleSelectionSynchronizer::BundleSelectionSynchronizer(container_list_t* containerList, pool_t* pool)
  : QObject(pool), m_containerList(containerList), m_pool(pool), m_synchronizing(false)
{
    setObjectName("BundleSelectionSynchronizer");
    Q_ASSERT(pool);
    connect(m_pool,SIGNAL(rows_added(QList<int>)),SLOT(synchronizeRowsIncluded(QList<int>)));
    connect(m_pool,SIGNAL(rows_removed(QList<int>)),SLOT(synchronizeRowsExcluded(QList<int>)));
}

QList<const Container*> BundleSelectionSynchronizer::containersInBundles(const Container* container) {
    if(container->bundleParent()) {
        return containersInBundles(container->bundleParent());
    }
    if(!container->bundleChildren().isEmpty()) {
        QList<const Container*> rv;
        rv << container;
        Q_FOREACH(Container* c, container->bundleChildren()) {
            rv << c;
        }
        return rv;
    }
    return QList<const Container*>();
}


QList<int> BundleSelectionSynchronizer::findRelatedContainerRow(QList< int > rows) {

    QList<int> related;
    Q_FOREACH(int row, rows) {
        const Container* container = m_containerList->get_container_at(row);
        QList<const Container*> relatedContainers = containersInBundles(container);
        Q_FOREACH(const Container* c, relatedContainers) {
            related << m_containerList->find_container(c);
        }
    }
    return related;
}


void BundleSelectionSynchronizer::synchronizeRowsExcluded(QList< int > rows_excluded) {
    if(m_synchronizing) {
        return;
    }
    m_synchronizing=true;

    QList<int> excludeAlso = findRelatedContainerRow(rows_excluded);

    m_pool->exclude(excludeAlso);

    m_synchronizing=false;
}

void BundleSelectionSynchronizer::synchronizeRowsIncluded(QList< int > rows_included) {
    if(m_synchronizing) {
        return;
    }
    m_synchronizing=true;

    QList<int> includeAlso = findRelatedContainerRow(rows_included);

    m_pool->include(includeAlso);

    m_synchronizing=false;

}



#include "bundleselectionsynchronizer.moc"
