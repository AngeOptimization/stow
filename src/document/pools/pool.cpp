#include "pool.h"

#include "bundleselectionsynchronizer.h"
#include <QAbstractProxyModel>
#include "document/containers/container_list.h"
#include <QDate>
#include <algorithm>
#include <QTimer>

using ange::containers::Container;

pool_t::pool_t(container_list_t* container_list):
    QObject(container_list),
    m_container_list(container_list),
    m_synchronized_selection_model(0L),
    m_synchronize_timer(0L),
    m_ignore_selection(false),
    m_cachedIncludedContainersDirty(false)
{
    setObjectName("Pool");
    connect(m_container_list, &container_list_t::rowsAboutToBeInserted, this, &pool_t::slot_model_rows_inserted);
    connect(m_container_list, &container_list_t::rowsAboutToBeRemoved,this, &pool_t::slot_model_rows_removed);
    new BundleSelectionSynchronizer(container_list, this);
    connect(m_container_list, &container_list_t::dataChanged, this, &pool_t::dataChanged);
}

void pool_t::slot_selection_changed(QItemSelection idx_selected, QItemSelection idx_deselected) {
    if (m_ignore_selection) {
        return;
    }
    QList<int> new_includes;
    const QAbstractProxyModel* proxy = 0L;
    if (!idx_selected.indexes().isEmpty()) {
        QModelIndex a_index = idx_selected.indexes().first();
        proxy = qobject_cast<const QAbstractProxyModel*>(a_index.model());
        Q_ASSERT(!proxy || proxy->sourceModel() == m_container_list);
    }
    if (!proxy && !idx_deselected.indexes().isEmpty()) {
        QModelIndex a_index = idx_deselected.indexes().first();
        proxy = qobject_cast<const QAbstractProxyModel*>(a_index.model());
        Q_ASSERT(!proxy || proxy->sourceModel() == m_container_list);
    }
    const bool was_empty = m_included_rows.empty();
    Q_FOREACH(QModelIndex index, idx_selected.indexes()) {
        int row = proxy ? proxy->mapToSource(index).row() : index.row();
        if (!m_included_rows.contains(row)) {
        m_included_rows << row;
        new_includes << row;
        }
    }
    QList<int> removed_rows;
    Q_FOREACH(QModelIndex index, idx_deselected.indexes()) {
        int row = proxy ? proxy->mapToSource(index).row() : index.row();
        if (m_included_rows.removeOne(row)) {
        removed_rows << row;
        }
    }
    if (was_empty != m_included_rows.isEmpty()) {
        emit empty_changed(m_included_rows.isEmpty());
    }
    m_cachedIncludedContainersDirty = true;
    emit changed();
    if (!new_includes.empty()) {
        m_ignore_selection = true;
        emit rows_added(new_includes);
        m_ignore_selection = false;
    }
    if (!removed_rows.empty()) {
        m_ignore_selection = true;
        emit rows_removed(removed_rows);
        m_ignore_selection = false;
    }
}

void pool_t::dataChanged(const QModelIndex& topLeft, const QModelIndex& bottomRight) {
    for (int row = topLeft.row(); row <= bottomRight.row(); ++row) {
        if (m_included_rows.contains(row)) {
            m_cachedIncludedContainersDirty = true;
            emit changed();
            return;
        }
    }
}

void pool_t::exclude(const ange::containers::Container* container, bool exclude) {
    include(container, true, !exclude);
}

void pool_t::exclude(QList< int > rows) {
    if (rows.empty()) {
        return;
    }
    const bool was_empty = m_included_rows.isEmpty();
    QList<int> removed_rows;
    Q_FOREACH(int row, rows) {
        if (m_included_rows.removeOne(row)) {
        removed_rows << row;
        }
    }
    if (m_included_rows.isEmpty() && !was_empty) {
        emit empty_changed(true);
    }
    if (!removed_rows.isEmpty()) {
        m_ignore_selection = true;
        emit rows_removed(removed_rows);
        m_ignore_selection = false;
    }
    m_cachedIncludedContainersDirty = true;
    emit changed();
    if (m_synchronize_timer) {
        m_synchronize_timer->start();
    }
}

void pool_t::exclude(QSet<const Container*> containers) {
    QList<int> rows;
    Q_FOREACH(const Container* container, containers) {
        rows << m_container_list->find_container(container);
    }
    exclude(rows);
}

void pool_t::clear() {
    exclude(m_included_rows);
}

void pool_t::reset_included_rows(QList< int > included_rows) {
    qSort(included_rows);
    QList<int> currently_included_rows(m_included_rows);
    qSort(currently_included_rows);
    QList<int> rows;
    // Calculate excluded rows
    std::set_difference(currently_included_rows.begin(), currently_included_rows.end(), included_rows.begin(), included_rows.end(), std::back_inserter(rows) );
    exclude(rows);
    rows.clear();
    std::set_difference(included_rows.begin(), included_rows.end(), currently_included_rows.begin(), currently_included_rows.end(), std::back_inserter(rows) );
    include(rows);
}

void pool_t::include(QList< int > rows) {
    include_rows(rows);
}

void pool_t::include_rows(const QList< int >& rowlist) {
    const bool was_empty = m_included_rows.isEmpty();
    QList<int> new_rows;
    Q_FOREACH(int row, rowlist) {
        if (!m_included_rows.contains(row)) {
        m_included_rows << row;
        new_rows << row;
        }
    }
    if (was_empty && !m_included_rows.empty()) {
        emit empty_changed(false);
    }
    if (!new_rows.isEmpty()) {
        m_ignore_selection = true;
        emit rows_added(new_rows);
        m_ignore_selection = false;
    }
    m_cachedIncludedContainersDirty = true;
    emit changed();
    if (m_synchronize_timer) {
        m_synchronize_timer->start();
    }
}

void pool_t::include(const ange::containers::Container* container, bool append, bool include) {
    int row = m_container_list->find_container(container);
    if (row == -1) {
        return;
    }
    if (include) {
        QList<int> new_rows;
        if (!m_included_rows.contains(row)) {
        new_rows << row;
        const bool was_empty = m_included_rows.isEmpty();
        if (append) {
            m_included_rows << row;
        } else {
            m_included_rows.prepend(row);
        }
        if (was_empty) {
            emit empty_changed(false);
        }
        }
        if (!new_rows.isEmpty()) {
        m_ignore_selection = true;
        emit rows_added(new_rows);
        m_ignore_selection = false;
        }
    } else {
        bool removed = m_included_rows.removeOne(row);
        if (removed) {
        m_ignore_selection = true;
        emit rows_removed(QList<int>() << row);
        m_ignore_selection = false;
        }
        if (m_included_rows.isEmpty()) {
        emit empty_changed(true);
        }
    }
    m_cachedIncludedContainersDirty = true;
    emit changed();
    if (m_synchronize_timer) {
        m_synchronize_timer->start();
    }
}

void pool_t::include(QSet<const Container*> containers) {
    QList<int> rows;
    Q_FOREACH(const Container* container, containers) {
        rows << m_container_list->find_container(container);
    }
    include_rows(rows);
}

void pool_t::include(QList<const ange::containers::Container* > containers) {
    QList<int> rows;
    QSet<int> rows_set;
    Q_FOREACH(const ange::containers::Container* c, containers) {
        const int row = m_container_list->find_container(c);
        if (!rows_set.contains(row)) {
        rows <<  row;
        rows_set << row;
        }
    }
    include_rows(rows);
}

void pool_t::include(QList< ange::containers::Container* > containers) {
    QList<int> rows;
    QSet<int> rows_set;
    Q_FOREACH(const ange::containers::Container* c, containers) {
        const int row = m_container_list->find_container(c);
        if (!rows_set.contains(row)) {
        rows <<  row;
        rows_set << row;
        }
    }
    include_rows(rows);
}

void pool_t::synchronize(QItemSelectionModel* selection_model) {
    if (m_synchronized_selection_model) {
        m_synchronized_selection_model->disconnect(this);
        delete m_synchronize_timer;
        m_synchronize_timer = 0L;
    }
    m_synchronized_selection_model = selection_model;
    connect(m_synchronized_selection_model, &QItemSelectionModel::selectionChanged,
                                        this, &pool_t::slot_selection_changed);
    m_synchronize_timer = new QTimer(this);
    m_synchronize_timer->setSingleShot(true);
    m_synchronize_timer->setInterval(0L);
    connect(m_synchronize_timer, &QTimer::timeout, this, &pool_t::sync_to_model);
}

QList< const Container* > pool_t::containers() const {
    rebuildContainerCache();
    return m_cachedIncludedContainers;
}

void pool_t::slot_model_rows_removed(const QModelIndex& parent, int first, int last) {
    if (parent.isValid()) {
        return;
    }
    int n_removed = last-first+1;
    for (int i=0; i<m_included_rows.size(); ++i) {
        int& row = m_included_rows[i];
        if (row>=first) {
        if (row <=last) {
            // This row was deleted. Remove
            m_included_rows.removeAt(i);
            --i;
            continue;
        } else {
            row -= n_removed;
        }
        }
    }
}

void pool_t::slot_model_rows_inserted(const QModelIndex& parent, int first, int last) {
    if (parent.isValid()) {
        return;
    }
    int n_inserted = last-first+1;
    for (int i=0; i<m_included_rows.size(); ++i) {
        int& row = m_included_rows[i];
        if (row>=first) {
        row += n_inserted;
        }
    }
}

void pool_t::sync_to_model() {
    QItemSelection selection;
    const QAbstractProxyModel* proxy = qobject_cast<const QAbstractProxyModel*>(m_synchronized_selection_model->model());
    Q_FOREACH(int row, m_included_rows) {
        QModelIndex index = m_container_list->index(row, 0);
        if (proxy) {
        index = proxy->mapFromSource(index);
        }
        selection << QItemSelectionRange(m_synchronized_selection_model->model()->index(index.row(), 0),m_synchronized_selection_model->model()->index(index.row(), proxy->columnCount()-1));
    }

    m_ignore_selection = true;
    m_synchronized_selection_model->select(selection, QItemSelectionModel::ClearAndSelect);
    m_ignore_selection = false;
}

void pool_t::rebuildContainerCache() const {
    if(m_cachedIncludedContainersDirty) {
        m_cachedIncludedContainers.clear();
        m_cachedIncludedContainers.reserve(m_included_rows.count());
        Q_FOREACH(int row, m_included_rows) {
            m_cachedIncludedContainers << m_container_list->at(row);
        }
        m_cachedIncludedContainersDirty = false;
    }
}

#include "pool.moc"
