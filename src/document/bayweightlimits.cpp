#include "bayweightlimits.h"

using namespace ange::schedule;
using namespace ange::vessel;
using namespace ange::units;

BayWeightLimits::BayWeightLimits(QObject* parent)
  : QObject(parent)
{
    // Empty
}

BayWeightLimits::~BayWeightLimits() {
    // Empty
}

Mass BayWeightLimits::weightLimitForBay(const Call* call, const BaySlice* baySlice) const {
    return m_limits[call].value(baySlice, qInf() * kilogram);
}

void BayWeightLimits::setWeightLimitForBay(const Call* call, const BaySlice* baySlice, Mass value) {
    m_limits[call][baySlice] = value;
    emit bayWeightLimitChanged(call, baySlice, value);
}

#include "bayweightlimits.moc"
