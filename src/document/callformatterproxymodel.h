#ifndef CALLFORMATTERPROXYMODEL_H
#define CALLFORMATTERPROXYMODEL_H

#include <QIdentityProxyModel>

class CallFormatterProxyModel : public QIdentityProxyModel {
    typedef QIdentityProxyModel super;
    Q_OBJECT
    public:
        CallFormatterProxyModel(QObject* parent = 0);
        virtual Qt::ItemFlags flags(const QModelIndex& index ) const Q_DECL_OVERRIDE;
        virtual QVariant data(const QModelIndex& proxyIndex, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    private:
};

#endif // CALLFORMATTERPROXYMODEL_H
