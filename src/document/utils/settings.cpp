#include "settings.h"

#include <QSettings>
#include <QStandardPaths>

// Keys:

static const QString stowageDirectoryKey = "stowageDirectory";
static const QString fileImportDirectoryKey = "fileImportDirectory";

// Settings:

// TODO Finish this class as described in ticket #1762

QString Settings::stowageDirectory() {
    QSettings qSettings;
    QString documentsLocation = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
    return qSettings.value(stowageDirectoryKey, documentsLocation).toString();
}

void Settings::setStowageDirectory(const QString& directory) {
    QSettings qSettings;
    qSettings.setValue(stowageDirectoryKey, directory);
}

QString Settings::fileImportDirectory() {
    QSettings qSettings;
    QString documentsLocation = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
    return qSettings.value(fileImportDirectoryKey, documentsLocation).toString();
}

void Settings::setFileImportDirectory(const QString& directory) {
    QSettings qSettings;
    qSettings.setValue(fileImportDirectoryKey, directory);
}
