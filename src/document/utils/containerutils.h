#ifndef CONTAINERUTILS_H
#define CONTAINERUTILS_H
#include <ange/containers/container.h>
#include <QStringList>

namespace ContainerUtils {

inline QString dangerousGoodsCodesAsParsableString(const QList<ange::containers::DangerousGoodsCode>& dangerousGoodsCodes) {
    QStringList codeStrings;
    Q_FOREACH(ange::containers::DangerousGoodsCode dgCode, dangerousGoodsCodes) {
        codeStrings << dgCode.unNumber() + (dgCode.isLimitedQuantity()? "L":"");
    }
    return codeStrings.join(" ");
}

inline QList<ange::containers::DangerousGoodsCode> parseDangerousGoodsString(const QString& dgString) {
    QList<ange::containers::DangerousGoodsCode> dgCodes;
    QStringList un_numbers_as_strings = dgString.split(" ");
    QRegExp un_number_rx("(\\d{4}[lL]?)");
    Q_FOREACH(const QString un_number, un_numbers_as_strings){
        if(un_number_rx.indexIn(un_number) != -1) {
            dgCodes << ange::containers::DangerousGoodsCode(un_number_rx.cap(1));
        }
    }
    return dgCodes;
}

inline QString handlingCodesAsParsableString(const QList<ange::containers::HandlingCode>& handlingCodes) {
    QStringList handlingCodesAsStrings;
    Q_FOREACH(const ange::containers::HandlingCode& handlingCode, handlingCodes) {
        handlingCodesAsStrings << handlingCode.code();
    }
    return handlingCodesAsStrings.join(" ");
}

inline QList<ange::containers::HandlingCode> parseHandlingCodesString(const QString& handlingCodesString) {
    QList<ange::containers::HandlingCode> handlingCodes;
    Q_FOREACH(const QString& code, handlingCodesString.split(" ")) {
        handlingCodes << ange::containers::HandlingCode(code);
    }
    return handlingCodes;
}
};


#endif // CONTAINERUTILS_H
