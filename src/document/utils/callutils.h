#ifndef CALLUTILS_H
#define CALLUTILS_H

#include <ange/schedule/call.h>

inline bool is_befor(const ange::schedule::Call* call) {
  return call->uncode()==QLatin1String("Befor");
}

inline bool is_after(const ange::schedule::Call* call) {
  return call->uncode()==QLatin1String("After");
}

inline bool is_befor_or_after(const ange::schedule::Call* call) {
  return is_befor(call) || is_after(call);
}

inline QString three_letter_call_code(const ange::schedule::Call* call) {
  if (call) {
    if (is_befor_or_after(call)) {
      return call->uncode().left(3);
    } else {
      return call->uncode().right(3);
    }
  }
  Q_ASSERT(call);
  return QString();
}

#endif //CALLUTILS_H