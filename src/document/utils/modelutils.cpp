#include "modelutils.h"

#include <QColor>
#include <QFont>

namespace ModelUtils {

QVariant editableHeaderStyle(int role) {
    switch (role) {
        case Qt::FontRole: {
            QFont font;
            font.setItalic(true);
            return font;
        }
        // Changing background is too aggressive and difficult to make work with styles that also changes background
        // case Qt::BackgroundRole:
        //     return QColor(0xFF, 0xFF, 0xCC); // Light yellow
        case Qt::DisplayRole:
            Q_ASSERT(false); // Only use this function for styling
            return QString("??");
        default:
            return QVariant();
    }
}

QVariant nonEditableHeaderStyle(int role) {
    switch (role) {
        case Qt::ForegroundRole:
            return QColor::fromHsv(0x00, 0x00, 0x66); // Light grey. 0x33 too close to black, 0xCC too close to white
        case Qt::DisplayRole:
            Q_ASSERT(false); // Only use this function for styling
            return QString("??");
        default:
            return QVariant();
    }
}

QVariant limitBrokenCellStyle(int role) {
    switch (role) {
        case Qt::ForegroundRole:
            return QColor(Qt::red);
        case Qt::DisplayRole:
            Q_ASSERT(false); // Only use this function for styling
            return QString("??");
        default:
            return QVariant();
    }
}

QVariant forcedEditCellStyle(int role) {
    switch (role) {
        case Qt::ForegroundRole:
            return QColor(Qt::blue);
        case Qt::DisplayRole:
            Q_ASSERT(false); // Only use this function for styling
            return QString("??");
        default:
            return QVariant();
    }
}

} // namespace ModelUtils
