#ifndef SETTINGS_H
#define SETTINGS_H

#include <QString>

/**
 * Settings for the application.
 * A wrapper around QSettings that documents the keys used and the default values.
 */
namespace Settings {

    /**
     * Directory used in "File->Open..." dialog
     * Default if not defined in QSettings is QStandardPaths::DocumentsLocation.
     */
    QString stowageDirectory();
    void setStowageDirectory(const QString& directory);

    /**
     * Directory used in "File->Import file..." dialog
     * Default if not defined in QSettings is QStandardPaths::DocumentsLocation.
     */
    QString fileImportDirectory();
    void setFileImportDirectory(const QString& directory);

};

#endif // SETTINGS_H
