#ifndef MODELUTILS_H
#define MODELUTILS_H

#include <QVariant>

namespace ModelUtils {

/**
 * Style data for the header of a column that is editable
 */
QVariant editableHeaderStyle(int role);

/**
 * Style data for the header of a column that is not editable
 */
QVariant nonEditableHeaderStyle(int role);

/**
 * Style data for a cell that has broken a limit
 */
QVariant limitBrokenCellStyle(int role);

/**
 * Style data for a cell that has forced and edited content
 */
QVariant forcedEditCellStyle(int role);

} // namespace ModelUtils

#endif // MODELUTILS_H
