#include "createproblemcommand.h"

#include "document.h"
#include "container_list.h"
#include "problemkeeper.h"

#include <ange/containers/container.h>
#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>

using ange::angelstow::Problem;
using ange::angelstow::ProblemLocation;
using ange::containers::Container;

CreateProblemCommand::CreateProblemCommand(document_t* document, Problem::Severity severity,
                                           const QString& description, const QString& details)
  : QUndoCommand(), m_document(document), m_severity(severity), m_description(description), m_details(details),
    m_call_id(-1), m_container_id(-1), m_problem(0)
{
    // Do nothing
}

void CreateProblemCommand::setCall(const ange::schedule::Call* call) {
    if (call) {
        m_call_id = call->id();
    } else {
        m_call_id = -1;
    }
}


void CreateProblemCommand::setContainer(const Container* container) {
    if (container) {
        m_container_id = container->id();
    } else {
        m_container_id = -1;
    }
}

void CreateProblemCommand::redo() {
    Q_ASSERT(!m_problem);
    QUndoCommand::redo();
    ProblemLocation location;
    location.setCall(m_document->schedule()->getCallById(m_call_id));
    location.setContainer(m_document->containers()->get_container_by_id(m_container_id));
    m_problem = new Problem(m_severity, location, m_description);
    m_problem->setDetails(m_details);
    m_document->problemKeeper()->addProblem(m_problem);
    Q_ASSERT(m_problem);
}

void CreateProblemCommand::undo() {
    Q_ASSERT(m_problem);
    delete m_problem;
    m_problem = 0;
    QUndoCommand::undo();
    Q_ASSERT(!m_problem);
}
