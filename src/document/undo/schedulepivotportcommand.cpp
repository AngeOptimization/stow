#include "schedulepivotportcommand.h"
#include <ange/schedule/schedule.h>
#include <ange/schedule/call.h>

SchedulePivotPortCommand* SchedulePivotPortCommand::setPivot(ange::schedule::Schedule* schedule, int newPivotIndex, QUndoCommand* parent) {
    Q_ASSERT(schedule);
    SchedulePivotPortCommand* cmd =  new SchedulePivotPortCommand(schedule, newPivotIndex, parent);
    cmd->setText(QString("Set pivot port to %1").arg(schedule->at(newPivotIndex)->uncode()));
    return cmd;
}

SchedulePivotPortCommand::SchedulePivotPortCommand(ange::schedule::Schedule* schedule, int newPivotIndex, QUndoCommand* parent)
    : QUndoCommand(parent), m_oldPivotIndex(schedule->indexOf(schedule->pivotCall())), m_newPivotIndex(newPivotIndex), m_schedule(schedule) {
    Q_ASSERT(schedule); // we would have been crashed here anyways due to the initializers, but for documentation's sake.
}

void SchedulePivotPortCommand::undo() {
    m_schedule->setPivotCall(m_oldPivotIndex);
}

void SchedulePivotPortCommand::redo() {
    m_schedule->setPivotCall(m_newPivotIndex);
}
