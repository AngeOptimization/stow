#include "macrocompartmentcommand.h"
#include "stowplugininterface/macroplan.h"
#include "document/document.h"
#include <ange/vessel/compartment.h>
#include <ange/vessel/bayslice.h>
#include <ange/vessel/stack.h>
#include <ange/vessel/slot.h>
#include <ange/vessel/vessel.h>
#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>

using ange::schedule::Call;
using ange::vessel::Compartment;

MacroCompartmentCommand* MacroCompartmentCommand::insert(document_t* document, const Compartment* compartment,
                                                         const Call* load, const Call* discharge, QUndoCommand* parent) {
    Q_ASSERT(compartment);
    MacroCompartmentCommand* command = new MacroCompartmentCommand(document, compartment, load, discharge, parent, true);
    command->setText(QString("Allocate %1 in master plan from %2 to %3")
                     .arg(compartment->objectName()).arg(load->uncode()).arg(discharge->uncode()));
    return command;
}

MacroCompartmentCommand* MacroCompartmentCommand::remove(document_t* document, const Compartment* compartment,
                                                         const Call* load, const Call* discharge, QUndoCommand* parent) {
    Q_ASSERT(compartment);
    MacroCompartmentCommand* command = new MacroCompartmentCommand(document, compartment, load, discharge, parent, false);
    command->setText(QString("Remove allocation of %1 in master plan from %2 to %3")
                     .arg(compartment->objectName()).arg(load->uncode()).arg(discharge->uncode()));
    return command;
}

MacroCompartmentCommand::MacroCompartmentCommand(document_t* document, const Compartment* compartment, const Call* load,
                                                 const Call* discharge, QUndoCommand* parent, bool insert)
    : super(parent), m_document(document), m_brtInCompartment(compartment->stacks().first()->stackSlots().first()->brt()),
      m_loadId(load->id()), m_dischargeId(discharge->id()), m_insert(insert)
{
    // Empty
}

void MacroCompartmentCommand::undo() {
    action(false);
    super::undo();
}

void MacroCompartmentCommand::redo() {
    super::redo();
    action(true);
}

void MacroCompartmentCommand::action(bool redo) {
    const Call* load = m_document->schedule()->getCallById(m_loadId);
    Q_ASSERT(load);
    const Call* discharge = m_document->schedule()->getCallById(m_dischargeId);
    Q_ASSERT(discharge);
    const Compartment* compartment = m_document->vessel()->compartmentContaining(m_brtInCompartment);
    Q_ASSERT(compartment);
    if (m_insert == redo) { // insert.redo or remove.undo
        m_document->macro_stowage()->insertAllocation(compartment, load, discharge);
    } else {
        m_document->macro_stowage()->removeAllocation(compartment, load, discharge);
    }
}
