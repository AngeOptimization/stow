#ifndef CONTAINER_STOW_COMMAND_H
#define CONTAINER_STOW_COMMAND_H

#include <QUndoCommand>
#include <QHash>

#include <stowplugininterface/stowtype.h>

class container_move_t;
class document_t;
class pool_t;
namespace ange {
namespace containers {
class Container;
}
namespace schedule {
class Call;
class Schedule;
}
namespace vessel {
class Slot;
}
}

class container_stow_command_t : public QUndoCommand {

public:

    /**
     * @return a container_stow_command_t needed for stowing one container.
     * Note: relies on the current stowage to determine the container's moves
     * @param container to stow
     * @param slot where to stow
     * @param load_call when to stow
     * @param discharge_call when to discharge again
     * @param add_to_selection if true, moves will be added to selection after redo. Otherwise, they will be removed.
     *        (undo will always simply restore selection)
     * @param ignore_selection_for_first_redo For performance reasons, it is sometimes desirable not to have the selection
     * changed for the very first time (instead, the push'er would do this manually).
     *
     */
    static container_stow_command_t* createStowageDependentStowCommand(document_t* document,
                             pool_t* pool,
                             const ange::containers::Container* container,
                             const ange::vessel::Slot* slot, const ange::schedule::Call* load_call,
                             const ange::schedule::Call* discharge_call,
                             ange::angelstow::StowType type,
                             bool add_to_selection,
                             bool ignore_selection_for_first_redo = false,
                             QUndoCommand* parent = 0L);

    /**
     * Create empty stow command
     * @param add_to_selection if true, moves will be added to selection after redo. Otherwise, they will be removed.
     *        (undo will always simply restore selection)
     */
    container_stow_command_t(document_t* document, pool_t* pool, bool add_to_selection, QUndoCommand* parent = 0L);

    /**
     * Create empty stow command, does not connect to a selection pool
     */
    container_stow_command_t(document_t* document, QUndoCommand* parent = 0L);

    /**
     * Add a stow to command
     * @param container to stow
     * @param slot where to stow
     * @param load_call when to stow
     * @param discharge_call when to discharge again
     * @return the command for chaining calls
     */
    container_stow_command_t* add_stow(const ange::containers::Container* container, const ange::vessel::Slot* slot, const ange::schedule::Call* load_call, const ange::schedule::Call* discharge_call, ange::angelstow::StowType type);

    /**
     * Add a load (with no discharge) to command.
     * @param container to stow
     * @param slot where to stow
     * @param call when to load
     * @return the command for chaining calls
     */
    container_stow_command_t* add_load(const ange::containers::Container* container, const ange::vessel::Slot* slot, const ange::schedule::Call* call, ange::angelstow::StowType type);

    /**
     * Add a discharge (with no discharge) to command.
     * @param container to stow
     * @param slot where container was stowed
     * @param call when to discharge
     * @return the command for chaining calls
     */
    container_stow_command_t* add_discharge(const ange::containers::Container* container, const ange::schedule::Call* call, ange::angelstow::StowType type);

    /**
     * Add moves to command. This is a "raw" function, meaning that the moves have to make sense. Please be careful
     * @return the command for chaining calls
     */
    container_stow_command_t* change_moves(const ange::containers::Container* container,
                                           QList<const container_move_t*> removed, QList<container_move_t> appended);

    /**
     * Clear all moves for this container, must be the first time this container is added to the command
     * @return the command for chaining calls
     */
    container_stow_command_t* clearMoves(const ange::containers::Container* container);

    /**
     * Add moves to command. This is a "raw" function, meaning that the moves have to make sense. Please be careful
     * @return the command for chaining calls
     */
    container_stow_command_t* addMoves(const ange::containers::Container* container, QList<container_move_t> moves);

    /**
     * @override
     */
    virtual void undo();

    /**
     * @override
     */
    virtual void redo();

    /**
     * @override
     */
    virtual int id() const {
      return 328; // This is not a guaranteed unique number, but mergeWith() handles if other QUndoCommands have same id
    }

private:

    struct move_t {
      move_t(const ange::vessel::Slot* slot, int call_id, ange::angelstow::StowType type) : slot(slot), call_id(call_id), type(type) {}
      const ange::vessel::Slot* slot;
      int call_id;
      ange::angelstow::StowType type;
      bool operator==(const move_t& rhs) const {
        return slot==rhs.slot && call_id == rhs.call_id && type == rhs.type;
      }
    };
    friend QDebug operator<<(QDebug dbg, const move_t& move);

    struct container_moves_t {
      QList<move_t> removed;
      QList<move_t> appended;
    };
    typedef QHash<int, container_moves_t> moves_t;
    moves_t m_moves;
    QList<int> m_list;
    document_t* m_document;
    bool m_add_to_selection;
    bool m_dont_change_selection_in_first_redo;
    void set_text();
    QList<int> m_selected;
    pool_t* m_pool;
    QList<const container_move_t*> generate_added_moves(const ange::containers::Container* container, QList<move_t> moves);
    QList<const container_move_t*> generate_removed_moves(const ange::containers::Container* container, QList<move_t> moves);

    /**
     * Remove moves for container until call, and return the position at this point
     * Also checks that container is not moved multiple times in same command
     */
    const ange::vessel::Slot* prepare(const ange::containers::Container* container, const ange::schedule::Call* call);

};

#endif // CONTAINER_STOW_COMMAND_H
