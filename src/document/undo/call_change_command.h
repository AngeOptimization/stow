/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010

 Copyright: See COPYING file that comes with this distribution

*/

#ifndef CALL_CHANGE_COMMAND_H
#define CALL_CHANGE_COMMAND_H

#include <QUndoStack>
#include <QVariant>

namespace ange {
namespace schedule {
class Call;
class Schedule;
}
}

class call_change_command_t : public QUndoCommand {
  public:
    /**
     * Change value in for
     * @param call
     * and column
     * @param column from schedule_model::column_t
     * to
     * @param new_value
     */
    call_change_command_t(ange::schedule::Schedule* schedule, ange::schedule::Call* call, int column, QVariant new_value, QUndoCommand* parent = 0);
    virtual void undo();
    virtual void redo();
  private:
    ange::schedule::Schedule* m_schedule;
    int m_column;
    int m_call_id;
    QVariant m_oldvalue;
    QVariant m_newvalue;

    void set_column_to_value(const QVariant& value);
};

#endif // CALL_CHANGE_COMMAND_H
