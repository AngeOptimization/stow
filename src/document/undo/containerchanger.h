#ifndef CONTAINERCHANGER_H
#define CONTAINERCHANGER_H

#include <ange/containers/dangerousgoodscode.h>
#include <ange/containers/isocode.h>

#include <QScopedPointer>

class QUndoCommand;
class document_t;
class stowage_t;
namespace ange {
namespace containers {
class Container;
}
namespace schedule {
class Call;
}
}

/**
 * The result of changing some features on a container, this is the QUndoCommand that did the change and the number of
 * problems in the ProblemKeeper that it generated.
 */
class ChangerResult {

public:

    /**
     * Create result
     */
    ChangerResult(QUndoCommand* command, int problemsGenerated);

    /**
     * The command that executes the change, the caller gets ownership of it
     */
    QUndoCommand* takeCommand();

    /**
     * The number of problems generated
     */
    int problemsGenerated();

private:
    QScopedPointer<QUndoCommand> m_command;
    int m_problemsGenerated;

};


namespace ContainerChanger {

/**
 * Creates the QUndoCommand that changes the length of a single container, when changing the length the
 * container will be unstowed and then restowed into the same position if possible. If it is not possible the
 * container will be left unstowed from the first problematic position and a problem generated.
 */
QSharedPointer<ChangerResult> changeLength(document_t* document, const ange::containers::Container* container,
                                           ange::containers::IsoCode::IsoLength length);

/**
 * Creates the QUndoCommand that changes the discharge call of a single container.
 * When making the trip shorter the moves invalidated will be removed or shortened.
 * When making the trip longer the last onboard leg will be extended to the new discharge call if the container was
 * scheduled onboard to the old discharge call, if the extension is impossible because of an onboard container
 * the extension will not be done and a warning will be generated.
 */
QSharedPointer<ChangerResult> changeDischargeCall(document_t* document, const ange::containers::Container* container,
                                                  const ange::schedule::Call* call);

/**
 * Creates the QUndoCommand that changes the load call of a single container.
 * When making the trip shorter the moves invalidated will be removed or shortened.
 * When making the trip longer the first onboard leg will be extended to the new load call if the container was
 * scheduled onboard from the old load call, if the extension is impossible because of an onboard container
 * the extension will not be done and a warning will be generated.
 */
QSharedPointer<ChangerResult> changeLoadCall(document_t* document, const ange::containers::Container* container,
                                             const ange::schedule::Call* call);

/**
 * Creates the QUndoCommand that changes both the load call and the discharge call of a single container.
 * It uses changeLoadCall() and changeDischargeCall() to do the actual work, but calls them in an order that
 * avoids illegal state in cases where both load and discharge is changed and moved out of the previous
 * load and discharge calls.
 * The call parameters can be nullprt, in that case the repective call is not changed.
 */
QSharedPointer<ChangerResult> changeCalls(document_t* document, const ange::containers::Container* container,
                                          const ange::schedule::Call* loadCall, const ange::schedule::Call* dischargeCall);

/**
 * Change dangerous goods for a container.
 * This function is used from tests in the DG module.
 */
QSharedPointer<ChangerResult> changeDg(document_t* document, const ange::containers::Container* container,
                                       const QList<ange::containers::DangerousGoodsCode>& dangerousGoodsCodes);

}

#endif // CONTAINERCHANGER_H
