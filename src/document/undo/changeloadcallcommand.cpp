/*
 * Copyright (C) 2013 Ange Optimization ApS <contact@ange.dk>
 * See COPYING file that comes with this distribution
 */
#include "changeloadcallcommand.h"

#include <document/document.h>
#include <document/stowage/stowage.h>
#include <document/stowage/container_leg.h>
#include <document/containers/container_list.h>
#include <ange/containers/container.h>
#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>

using ange::containers::Container;
using ange::schedule::Call;

ChangeLoadCallCommand::ChangeLoadCallCommand(document_t* document, const Container* container,
                                                       const Call* newCall, QUndoCommand* parent)
: QUndoCommand(parent), m_document(document), m_containerId(container->id()), m_newCallId(newCall->id())
, m_oldCallId(document->stowage()->container_routes()->portOfLoad(container)->id())
{
    // Do nothing
}

void ChangeLoadCallCommand::redo() {
    const Container* container = m_document->containers()->get_container_by_id(m_containerId);
    const Call* newCall = m_document->schedule()->getCallById(m_newCallId);
    m_document->stowage()->change_pol(container, newCall);
    QUndoCommand::redo();
}

void ChangeLoadCallCommand::undo() {
    QUndoCommand::undo();
    const Container* container = m_document->containers()->get_container_by_id(m_containerId);
    const Call* oldCall = m_document->schedule()->getCallById(m_oldCallId);
    m_document->stowage()->change_pol(container, oldCall);
}
