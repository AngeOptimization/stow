#ifndef CREATEPROBLEMCOMMAND_H
#define CREATEPROBLEMCOMMAND_H

#include <QUndoCommand>

#include "stowplugininterface/problem.h"

class ProblemKeeper;
class document_t;
namespace ange {
namespace containers {
class Container;
}
}

/**
 * Command for creating problems in the problem view. It is only for the problems that will be owner less in the sense
 * that they are created as the result of an action. The created problems are added to the ProblemKeeper.
 */
class CreateProblemCommand : public QUndoCommand {

    // Incomplete implementation... Right now the class only supports creating problems for containers, when there is
    // need for creating problems for other locations add that functionality.

public:

    /**
     * Create a command that will create a problem. Remember to set location.
     */
    CreateProblemCommand(document_t* document, ange::angelstow::Problem::Severity severity,
                         const QString& description, const QString& details);

    /**
     * Set call for location
     */
    void setCall(const ange::schedule::Call* call);

    /**
     * Set container for location
     */
    void setContainer(const ange::containers::Container* container);

    // overrides from QUndoCommand:
    virtual void redo();
    virtual void undo();

private:
    document_t* m_document;
    const ange::angelstow::Problem::Severity m_severity;
    const QString m_description;
    const QString m_details;

    int m_call_id;
    int m_container_id;

    ange::angelstow::Problem* m_problem;

};

#endif // CREATEPROBLEMCOMMAND_H
