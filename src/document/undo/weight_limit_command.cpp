#include "weight_limit_command.h"

#include "bayweightlimits.h"
#include "document.h"
#include "stowage.h"

#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>
#include <ange/vessel/bayslice.h>

#include <limits>

weight_limit_command_t* weight_limit_command_t::add(document_t* document, const ange::vessel::BaySlice* bay_slice, const ange::schedule::Call* call, double initial_value, QUndoCommand* parent) {
  weight_limit_command_t* rv = new weight_limit_command_t(ADD_TYPE, document, bay_slice, call, initial_value, parent);
  rv->setText(document->tr("Add limit for weight in bay %1 in call %2").arg(bay_slice->joinedBay()).arg(call->uncode()));
  return rv;
}

weight_limit_command_t* weight_limit_command_t::remove(document_t* document, const ange::vessel::BaySlice* bay_slice, const ange::schedule::Call* call, QUndoCommand* parent) {
  weight_limit_command_t* rv = new weight_limit_command_t(REMOVE_TYPE, document, bay_slice, call, std::numeric_limits<double>::infinity(), parent);
  rv->setText(document->tr("Remove limit for weight in bay %1 in call %2").arg(bay_slice->joinedBay()).arg(call->uncode()));
  return rv;
}

weight_limit_command_t* weight_limit_command_t::set(document_t* document, const ange::vessel::BaySlice* bay_slice, const ange::schedule::Call* call, double new_value, QUndoCommand* parent) {
  weight_limit_command_t* rv = new weight_limit_command_t(ADD_TYPE, document, bay_slice, call, new_value, parent);
  rv->setText(document->tr("Change limit for weight in bay %1 in call %2").arg(bay_slice->joinedBay()).arg(call->uncode()));
  return rv;
}


void weight_limit_command_t::undo() {
  const ange::schedule::Call* call = m_document->schedule()->getCallById(m_call_id);
  const ange::units::Mass old_value = m_document->bayWeightLimits()->weightLimitForBay(call, m_bay_slice);
  m_document->bayWeightLimits()->setWeightLimitForBay(call, m_bay_slice, m_value);
  m_value = old_value;
  QUndoCommand::undo();
}

void weight_limit_command_t::redo() {
  QUndoCommand::redo();
  const ange::schedule::Call* call = m_document->schedule()->getCallById(m_call_id);
  const ange::units::Mass old_value = m_document->bayWeightLimits()->weightLimitForBay(call, m_bay_slice);
  m_document->bayWeightLimits()->setWeightLimitForBay(call, m_bay_slice, m_value);
  m_value = old_value;
}

weight_limit_command_t::weight_limit_command_t(weight_limit_command_t::type_t type,
                                               document_t* document,
                                               const ange::vessel::BaySlice* bay_slice,
                                               const ange::schedule::Call* call,
                                               double value,
                                               QUndoCommand* parent)
  : QUndoCommand(parent),
  m_document(document),
  m_type(type),
  m_bay_slice(bay_slice),
  m_call_id(call->id()),
  m_value(value)
{
}
