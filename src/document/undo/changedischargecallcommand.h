/*
 * Copyright (C) 2013 Ange Optimization ApS <contact@ange.dk>
 * See COPYING file that comes with this distribution
 */
#ifndef CHANGEDISCHARGECALLCOMMAND_H
#define CHANGEDISCHARGECALLCOMMAND_H

#include <QUndoStack>

class document_t;
namespace ange {
namespace containers {
class Container;
}
namespace schedule {
class Call;
}
}

/**
 * Change the Discharge Call of a container
 */
class ChangeDischargeCallCommand : public QUndoCommand {
public:
    explicit ChangeDischargeCallCommand(document_t* document, const ange::containers::Container* container,
                                        const ange::schedule::Call* newCall, QUndoCommand* parent = 0);
    virtual void redo();
    virtual void undo();
private:
    document_t* m_document;
    int m_containerId;
    int m_newCallId;
    int m_oldCallId;
};

#endif // CHANGEDISCHARGECALLCOMMAND_H
