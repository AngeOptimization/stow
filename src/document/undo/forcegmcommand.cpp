#include "forcegmcommand.h"

#include "document.h"
#include "stabilityforcer.h"

#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>

using namespace ange::schedule;
using namespace ange::units;

ForceGmCommand::ForceGmCommand(document_t* document, const Call* call, Length gm)
  : QUndoCommand(), m_document(document), m_callId(call->id()), m_gm(gm)
{
    if (qIsNaN(m_gm / meter)) {
        setText("Clear forced GM");
    } else {
        setText(QStringLiteral("Force GM to %1 m").arg(m_gm/meter, 0, 'f', 2));
    }
}

void ForceGmCommand::redo() {
    const Call* call = m_document->schedule()->getCallById(m_callId);
    StabilityForcer* stabilityForcer = m_document->stabilityForcer();
    Length oldGm = stabilityForcer->gm(call, qQNaN() * meter);
    if (qIsNaN(m_gm / meter)) {
        stabilityForcer->clearForcedGm(call);
    } else {
        stabilityForcer->forceGm(call, m_gm);
    }
    m_gm = oldGm;
}

void ForceGmCommand::undo() {
    redo();  // As redo() swaps old setting into m_gm, it can also be used as undo
}
