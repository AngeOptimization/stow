/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010

 Copyright: See COPYING file that comes with this distribution

*/

#ifndef WEIGHT_LIMIT_COMMAND_H
#define WEIGHT_LIMIT_COMMAND_H

#include <QUndoStack>
#include <ange/units/units.h>

class document_t;
namespace ange {
namespace schedule {

class Call;
}

namespace vessel {
class BaySlice;
}
}

class weight_limit_command_t : public QUndoCommand {
  public:
    static weight_limit_command_t* add(document_t* document, const ange::vessel::BaySlice* bay_slice, const ange::schedule::Call* call, double initial_value, QUndoCommand* parent = 0);
    static weight_limit_command_t* remove(document_t* document, const ange::vessel::BaySlice* bay_slice, const ange::schedule::Call* call, QUndoCommand* parent = 0);
    static weight_limit_command_t* set(document_t* document, const ange::vessel::BaySlice* bay_slice, const ange::schedule::Call* call, double new_value, QUndoCommand* parent = 0);
    virtual void undo();
    virtual void redo();
  private:
    enum type_t {
      ADD_TYPE,
      REMOVE_TYPE,
      SET_TYPE
    };
    weight_limit_command_t(type_t type, document_t* document, const ange::vessel::BaySlice* bay_slice, const ange::schedule::Call* call, double value, QUndoCommand* parent = 0);
    document_t* m_document;
    type_t m_type;
    const ange::vessel::BaySlice* m_bay_slice;
    int m_call_id;
    ange::units::Mass m_value;
};

#endif // WEIGHT_LIMIT_COMMAND_H
