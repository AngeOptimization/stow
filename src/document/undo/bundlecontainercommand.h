#ifndef BUNDLECONTAINERCOMMAND_H
#define BUNDLECONTAINERCOMMAND_H

#include <QUndoCommand>

class document_t;
namespace ange {
namespace containers {
class Container;
}
}

/**
 * \brief undo command for adding/removing containers for bundles
 */

class BundleContainerCommand : public QUndoCommand {
    public:
        enum Type {
            Add,
            Remove
        };
        /**
         * \param t Type of command (Add or Remove)
         * \param document The document to act upon
         * \param bottom container in bundle to add to
         * \param parent optional undo command
         */
        BundleContainerCommand(Type t,document_t* document, ange::containers::Container* parentContainer, QUndoCommand* parent=0);

        virtual void undo();
        virtual void redo();

        /**
         * put one more container in this command, either for adding or for removal.
         * \param container to add to bundle given in constructor
         */
        void addContainer(const ange::containers::Container* container);

    private:
        Type m_type;
        document_t* m_document;
        int m_bottom_container_id;
        QList<int> m_container_ids;
        void bundleContainers();
        void unbundleContainers();

};

#endif // BUNDLECONTAINERCOMMAND_H
