#ifndef TANK_STATE_COMMAND_H
#define TANK_STATE_COMMAND_H

#include <QUndoStack>
#include <QHash>
#include <ange/units/units.h>

class document_t;
namespace ange {
namespace schedule {

class Call;
}

namespace vessel {
class VesselTank;
}
}

/**
 * Undo command to set tank content in a port and all following ports.
 */


class tank_state_command_t : public QUndoCommand
{

  public:
    tank_state_command_t(document_t* document,const ange::schedule::Call* call, QUndoCommand* parent = 0);
    virtual void undo();
    virtual void redo();
    void insert_tank_condition(const ange::vessel::VesselTank* tank, ange::units::Mass condition);
  private:
    document_t* m_document;
    int m_call_id;
    // The following are callid,tankid,content
    QHash<int, QHash<int, ange::units::Mass> > m_old_conditions;
    QHash<int, QHash<int, ange::units::Mass> > m_new_conditions;
};

#endif // TANK_STATE_COMMAND_H
