#include "schedule_change_command.h"

#include <ange/schedule/schedule.h>
#include <ange/vessel/vesseltank.h>
#include <ange/vessel/vessel.h>

#include "document/document.h"
#include "document/stowage/stowage.h"
#include "document/utils/callutils.h"
#include <document/tanks/tankconditions.h>

schedule_change_command_t* schedule_change_command_t::insert_call(int index,
                                                                  const QString& uncode,
                                                                  const QString& voyage_code,
                                                                  const QString& port_name,
                                                                  const QDateTime& eta,
                                                                  const QDateTime& etd,
                                                                  double cranes,
                                                                  double height_limit,
                                                                  const ange::schedule::CraneRules::Types cranerule,
                                                                  const ange::schedule::TwinLiftRules::Types twinlifting,
                                                                  double craneProductivity,
                                                                  document_t* document,
                                                                  QHash<const ange::vessel::VesselTank*, ange::units::Mass> tank_conditions,
                                                                  QUndoCommand* parent) {
    schedule_change_command_t* cmd = new schedule_change_command_t(APPEND,document,parent);
    Q_ASSERT(index>=0);
    Q_ASSERT(index<=document->schedule()->calls().size());
    cmd->m_uncode=uncode;
    cmd->m_voyage_code=voyage_code;
    cmd->m_port_name=port_name;
    cmd->m_eta=eta;
    cmd->m_etd=etd;
    cmd->m_cranes=cranes;
    cmd->m_height_limit=height_limit;
    cmd->m_cranerule=cranerule;
    cmd->m_twinlifting = twinlifting;
    cmd->m_craneProductivity = craneProductivity;
    cmd->m_index=index;
    Q_FOREACH(const ange::vessel::VesselTank* tank, tank_conditions.keys()){
        cmd->m_tank_conditions_for_call[tank->id()] = tank_conditions.value(tank);
    }
    if (index == document->schedule()->calls().size() || is_after(document->schedule()->calls().at(index))) {
        cmd->setText(QObject::tr("Append call %1").arg(uncode));
    } else {
        cmd->setText(QObject::tr("Insert call %1 before %2").arg(uncode).arg(document->schedule()->calls().at(index)->uncode()));
    }
    return cmd;
}

schedule_change_command_t* schedule_change_command_t::remove_call(document_t* document, int index, QUndoCommand* parent) {
    const ange::schedule::Call* call = document->schedule()->at(index);
    schedule_change_command_t* cmd = new schedule_change_command_t(REMOVE,document,parent);
    cmd->m_uncode = call->uncode();
    cmd->m_voyage_code = call->voyageCode();
    cmd->m_port_name = call->name();
    cmd->m_eta = call->eta();
    cmd->m_etd = call->etd();
    cmd->m_id = call->id();
    cmd->m_cranes = call->cranes();
    cmd->m_height_limit=call->heightLimit();
    cmd->m_cranerule = call->craneRule();
    cmd->m_twinlifting = call->twinLifting();
    cmd->m_craneProductivity = call->craneProductivity();
    cmd->m_index = index;
    Q_FOREACH(ange::vessel::VesselTank* tank, cmd->m_document->vessel()->tanks()) {
        cmd->m_tank_conditions_for_call[tank->id()] = cmd->m_document->tankConditions()->tankCondition(call,tank);
    }
    cmd->setText(QObject::tr("Remove call %1").arg(cmd->m_uncode));
    return cmd;
}
schedule_change_command_t::schedule_change_command_t(schedule_change_command_t::type_t type,document_t* document, QUndoCommand* parent): QUndoCommand(parent),m_type(type), m_document(document),m_id(-1) {
}

void schedule_change_command_t::undo(){
    switch(m_type) {
        case APPEND: {
            m_document->schedule()->removeAt(m_index);
            break;
        }
        case REMOVE: {
            ange::schedule::Call* call = new ange::schedule::Call(m_uncode, m_voyage_code, m_port_name, m_eta, m_etd, 0,m_id);
            call->setCranes(m_cranes);
            call->setHeightLimit(m_height_limit);
            call->setCraneRule(m_cranerule);
            call->setTwinlifting(m_twinlifting);
            call->setCraneProductivity(m_craneProductivity);
            //We need to block the signals here, as changing the tank condition and inserting
            //a call will cause a recalculation of the stability figures, which require both
            //to be available
            QSharedPointer<stowage_signal_lock_t> signal_lock = m_document->stowage()->prepare_for_major_stowage_change();
            m_document->schedule()->insert(m_index,call);
            Q_FOREACH(const ange::vessel::VesselTank* tank, m_document->vessel()->tanks()) {
                m_document->tankConditions()->changeTankCondition(call,tank,m_tank_conditions_for_call[tank->id()]);
            }
            signal_lock.clear();
            break;
        }
    }
    QUndoCommand::undo();
}

void schedule_change_command_t::redo() {
    QUndoCommand::redo();
    switch(m_type) {
        case APPEND: {
            ange::schedule::Call* call = new ange::schedule::Call(m_uncode, m_voyage_code, m_port_name, m_eta, m_etd, 0, m_id);
            call->setCranes(m_cranes);
            call->setCraneRule(m_cranerule);
            call->setTwinlifting(m_twinlifting);
            call->setCraneProductivity(m_craneProductivity);
            if(m_id==-1) {
                m_id=call->id();
            }
            QSharedPointer<stowage_signal_lock_t> signal_lock = m_document->stowage()->prepare_for_major_stowage_change();
            m_document->schedule()->insert(m_index,call);
            Q_FOREACH(const ange::vessel::VesselTank* tank, m_document->vessel()->tanks()) {
                m_document->tankConditions()->changeTankCondition(call,tank,m_tank_conditions_for_call[tank->id()]);
            }
            signal_lock.clear();
            break;
        }
        case REMOVE: {
            m_document->schedule()->removeAt(m_index);
            break;
        }
    }
}

