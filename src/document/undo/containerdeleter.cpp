#include "containerdeleter.h"
#include "merger_command.h"
#include "bundlecontainercommand.h"
#include "container_stow_command.h"
#include "container_list_change_command.h"
#include <document.h>
#include <stowage.h>
#include <ange/containers/container.h>

using ange::containers::Container;

QUndoCommand* ContainerDeleter::deleteContainers(document_t* document, pool_t* selected_pool, QList< Container* > containersToDelete) {

#ifndef QT_NO_DEBUG
    { // scoping to not 'leak' variables to outside the debug statement
        QSet<Container*> containerSet = containersToDelete.toSet();
        Q_FOREACH(Container* container, containersToDelete) {
            if(container->bundleParent()) {
                Q_ASSERT(containerSet.contains(container->bundleParent()));
            }
            if(!container->bundleChildren().isEmpty()) {
                Q_FOREACH(Container* bundlechild, container->bundleChildren()) {
                    Q_ASSERT(containerSet.contains(bundlechild));
                }
            }
        }
    }
#endif

    merger_command_t* merger = new merger_command_t(QString("Delete %1 containers").arg(containersToDelete.size()));

    // first ensure that everything is unbundled.
    // We need to do stuff in two steps because we don't know the order of containers, and treating
    // the bundle parents before the children is better
    Q_FOREACH(Container* container, containersToDelete) {
        if(!container->bundleChildren().isEmpty()) {
            BundleContainerCommand* bundleCommand = new BundleContainerCommand(BundleContainerCommand::Remove, document, container);
            Q_FOREACH(const Container* bundleChild, container->bundleChildren()) {
                bundleCommand->addContainer(bundleChild);
            }
            merger->push(bundleCommand);
        }
    }
    container_stow_command_t* unstow_cmd = new container_stow_command_t(document, selected_pool, false);
    container_list_change_command_t* del_cmd = new container_list_change_command_t(document);
    Q_FOREACH(const Container* container, containersToDelete) {
        unstow_cmd->clearMoves(container);
        del_cmd->remove_container(container);
    }
    merger->push(unstow_cmd);
    merger->push(del_cmd);
    return merger;

}

