
#ifndef MACROCOMPARTMENTCOMMAND_H
#define MACROCOMPARTMENTCOMMAND_H

#include <QUndoStack>
#include <ange/vessel/bayrowtier.h>

class document_t;
namespace ange {
namespace schedule {
class Call;
class Schedule;
}
namespace vessel {
class Compartment;
}
namespace angelstow {
class MacroPlan;
}
}

class MacroCompartmentCommand : public QUndoCommand {
    typedef QUndoCommand super;

public:
    /**
     * Insert into macro stowage
     */
    static MacroCompartmentCommand* insert(document_t* document, const ange::vessel::Compartment* compartment,
                                           const ange::schedule::Call* load, const ange::schedule::Call* discharge,
                                           QUndoCommand* parent = 0L);
    /**
     * Remove from macro stowage
     */
    static MacroCompartmentCommand* remove(document_t* document, const ange::vessel::Compartment* compartment,
                                           const ange::schedule::Call* load, const ange::schedule::Call* discharge,
                                           QUndoCommand* parent = 0L);

    // Inherited:
    virtual void undo();
    virtual void redo();

private:
    MacroCompartmentCommand(document_t* document, const ange::vessel::Compartment* compartment,
                            const ange::schedule::Call* load, const ange::schedule::Call* discharge,
                            QUndoCommand* parent, bool insert);

    void action(bool redo);

private:
    const document_t* m_document;
    const ange::vessel::BayRowTier m_brtInCompartment;
    const int m_loadId;
    const int m_dischargeId;
    const bool m_insert;

};

#endif // MACROCOMPARTMENTCOMMAND_H
