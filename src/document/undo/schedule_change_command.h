#ifndef SCHEDULE_CHANGE_COMMAND_H
#define SCHEDULE_CHANGE_COMMAND_H

#include <QUndoCommand>
#include <QDateTime>
#include <QHash>
#include <ange/schedule/call.h>
#include <ange/schedule/cranerules.h>
#include <ange/units/units.h>

class document_t;
namespace ange {
namespace vessel {
class VesselTank;
}

namespace schedule {
class Call;
}
}

class schedule_change_command_t : public QUndoCommand
{
  public:
    /**
     * insert call at index. If index=size(), append call.
     */
    static schedule_change_command_t* insert_call(int index, const QString& uncode, const QString& voyage_code, const QString& port_name,
                                                  const QDateTime& eta, const QDateTime& etd, double cranes, double height_limit,
                                                  const ange::schedule::CraneRules::Types cranerule,
                                                  const ange::schedule::TwinLiftRules::Types twinlifting,
                                                  double craneProductivity,
                                                  document_t* document,
                                                  QHash<const ange::vessel::VesselTank*, ange::units::Mass> tank_condition,
                                                  QUndoCommand* parent = 0 );

    /**
     * removes first call. if first call is 'Befor' it  removes the second call, unless second call is
     * After, in which this command does nothing and this function returns null (!)
     */
    static schedule_change_command_t* remove_call(document_t* document, int index, QUndoCommand* parent = 0);
    virtual void undo();
    virtual void redo();
  private:
    enum type_t {
      APPEND,
      REMOVE
    };
    schedule_change_command_t(schedule_change_command_t::type_t type, document_t* document, QUndoCommand* parent = 0);
    QString m_uncode;
    QString m_voyage_code;
    QString m_port_name;
    double m_cranes;
    double m_height_limit;
    ange::schedule::CraneRules::Types m_cranerule;
    ange::schedule::TwinLiftRules::Types m_twinlifting;
    double m_craneProductivity;
    QDateTime m_eta;
    QDateTime m_etd;
    type_t m_type;
    document_t* m_document;
    int m_index;
    int m_id;
    QHash<const int, ange::units::Mass> m_tank_conditions_for_call;
};

#endif // SCHEDULE_CHANGE_COMMAND_H
