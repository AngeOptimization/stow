#ifndef CHANGE_CONTAINER_COMMAND_H
#define CHANGE_CONTAINER_COMMAND_H

#include <QUndoCommand>
#include <QVariant>

class container_list_t;
namespace ange {
namespace containers {
class Container;
}
}

/**
 * A command to change different types of features of a container.
 * Note: the changes should not impact the rest of the stowage, e.g. the length
 * of on-board containers cannot be changed here.
 */
class ChangeContainerCommand : public QUndoCommand {

public:

    enum Feature {
        Dg,
        Weight,
        Carrier,
        Empty,
        Height,
        Length,
        EquipmentNo,
        BookingNumber,
        PlaceOfDelivery,
        Temperature,
        Reefer,
        Live,
        OogFeature,
        HandlingCodes,
    };

    static ChangeContainerCommand* change_feature(
        container_list_t* containerList, const ange::containers::Container* container,
        Feature feature_index, const QVariant& feature_value);

    // Inherited:
    virtual void undo();
    virtual void redo();

private:

    explicit ChangeContainerCommand(container_list_t* containerList, const ange::containers::Container* container);

    void swapUndoRedoData();

private:

    container_list_t* m_containerList;
    int m_container;
    Feature m_featureType;
    QVariant m_undoData;

};

#endif // CHANGE_CONTAINER_COMMAND_H
