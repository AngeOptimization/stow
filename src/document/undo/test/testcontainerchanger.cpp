#include <QtTest>

#include "containerchanger.h"

#include "test/util/vesseltestutils.h"

#include <ange/containers/container.h>
#include <ange/schedule/schedule.h>
#include <ange/vessel/vessel.h>

using namespace ange::containers;
using namespace ange::schedule;
using namespace ange::vessel;

class TestContainerChanger : public QObject {
    Q_OBJECT
private Q_SLOTS:
    void testChangeCalls();
    void testChangeCalls_data();
};
QTEST_GUILESS_MAIN(TestContainerChanger);

void TestContainerChanger::testChangeCalls_data() {
    QTest::addColumn<int>("loadIndex");
    QTest::addColumn<int>("dischargeIndex");

    QTest::newRow("0,1") << 0 << 1;
    QTest::newRow("0,2") << 0 << 2;
    QTest::newRow("0,3") << 0 << 3;
    QTest::newRow("0,4") << 0 << 4;
    QTest::newRow("0,5") << 0 << 5;
    QTest::newRow("0,6") << 0 << 6;

    QTest::newRow("1,2") << 1 << 2;
    QTest::newRow("1,3") << 1 << 3;
    QTest::newRow("1,4") << 1 << 4;
    QTest::newRow("1,5") << 1 << 5;
    QTest::newRow("1,6") << 1 << 6;

    QTest::newRow("2,3") << 2 << 3;
    QTest::newRow("2,4") << 2 << 4;
    QTest::newRow("2,5") << 2 << 5;
    QTest::newRow("2,6") << 2 << 6;

    QTest::newRow("3,4") << 3 << 4;
    QTest::newRow("3,5") << 3 << 5;
    QTest::newRow("3,6") << 3 << 6;

    QTest::newRow("4,5") << 4 << 5;
    QTest::newRow("4,6") << 4 << 6;

    QTest::newRow("5,6") << 5 << 6;

    QTest::newRow("-,3") << -1 << 3;
    QTest::newRow("-,4") << -1 << 4;
    QTest::newRow("-,5") << -1 << 5;
    QTest::newRow("-,6") << -1 << 6;

    QTest::newRow("0,-") << 0 << -1;
    QTest::newRow("1,-") << 1 << -1;
    QTest::newRow("2,-") << 2 << -1;
    QTest::newRow("3,-") << 3 << -1;

    QTest::newRow("-,-") << -1 << -1;
}

void TestContainerChanger::testChangeCalls() {
    QFETCH(int, loadIndex);
    QFETCH(int, dischargeIndex);

    Schedule* schedule = buildSchedule(7);
    Vessel* vessel = new Vessel(QList<BaySlice*>(), QList<HatchCover*>(), QList<VesselTank*>());
    stowage_t* stowage = new stowage_t(schedule);
    document_t* document = new document_t(schedule, vessel, stowage);

    Container* container = new Container(EquipmentNumber("KIMU1234567"), 10*ton, IsoCode("42G1"));
    container_list_change_command_t* command = new container_list_change_command_t(document);
    const Call* origLoad = schedule->at(2);
    const Call* origDischarge = schedule->at(4);
    command->add_container(container, origLoad, origDischarge);
    document->undostack()->push(command);
    delete container;
    container = document->containers()->get_container_at(0);

    QCOMPARE(document->stowage()->container_routes()->portOfLoad(container), origLoad);
    QCOMPARE(document->stowage()->container_routes()->portOfDischarge(container), origDischarge);

    QSharedPointer<ChangerResult> result = ContainerChanger::changeCalls(document, container,
        loadIndex == -1 ? 0 : schedule->at(loadIndex),
        dischargeIndex == -1 ? 0 : schedule->at(dischargeIndex));
    QCOMPARE(result->problemsGenerated(), 0);

    document->undostack()->push(result->takeCommand());

    QCOMPARE(document->stowage()->container_routes()->portOfLoad(container),
             loadIndex == -1 ? origLoad : schedule->at(loadIndex));
    QCOMPARE(document->stowage()->container_routes()->portOfDischarge(container),
             dischargeIndex == -1 ? origDischarge : schedule->at(dischargeIndex));

    document->undostack()->undo();

    QCOMPARE(document->stowage()->container_routes()->portOfLoad(container), origLoad);
    QCOMPARE(document->stowage()->container_routes()->portOfDischarge(container), origDischarge);

    document->undostack()->redo();

    QCOMPARE(document->stowage()->container_routes()->portOfLoad(container),
             loadIndex == -1 ? origLoad : schedule->at(loadIndex));
    QCOMPARE(document->stowage()->container_routes()->portOfDischarge(container),
             dischargeIndex == -1 ? origDischarge : schedule->at(dischargeIndex));

    delete document;
}

#include "testcontainerchanger.moc"
