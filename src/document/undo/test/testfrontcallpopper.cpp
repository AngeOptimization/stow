#include <QtTest>

#include "frontcallpopper.h"

#include "test/util/vesseltestutils.h"

#include <ange/containers/container.h>
#include <ange/schedule/schedule.h>
#include <ange/schedule/call.h>
#include <ange/vessel/vessel.h>

using ange::containers::Container;
using ange::schedule::Schedule;
using ange::schedule::Call;
using ange::vessel::BayRowTier;
using ange::vessel::Vessel;

/**
 * Test the use of a front call popper
 */
class TestFrontCallPopper : public QObject {
    Q_OBJECT
private Q_SLOTS:
    void testPopOneOnEmpty();
    void testPopAllOnEmpty();
    void testPopInnerCall();
    void testPopDischargeCall();
    void testPopLoadCall();
    void testPopRestow();
    void testPopDischargeRestow();
};

static document_t* newDocument() {
    Schedule* schedule = buildSchedule(5);
    Vessel* vessel = loadVessel(QFINDTESTDATA("test-data/single-stack.json"));
    stowage_t* stowage = new stowage_t(schedule);
    document_t* document = new document_t(schedule, vessel, stowage);
    return document;
}

void TestFrontCallPopper::testPopOneOnEmpty() {
    document_t* document = newDocument();
    QCOMPARE(5, document->schedule()->size());
    FrontCallPopper(document, 0).pop(); // Action under test
    QCOMPARE(4, document->schedule()->size());
    delete document;
}

void TestFrontCallPopper::testPopAllOnEmpty() {
    document_t* document = newDocument();
    QCOMPARE(5, document->schedule()->size());
    while (document->schedule()->size() > 2) {
        FrontCallPopper(document, 0).pop(); // Action under test
    }
    QCOMPARE(2, document->schedule()->size());
    delete document;
}

void TestFrontCallPopper::testPopInnerCall() {
    document_t* document = newDocument();
    const Schedule* schedule = document->schedule();
    QCOMPARE(QStringLiteral("CALL1"), schedule->at(1)->uncode());
    const Vessel* vessel = document->vessel();
    const Container* container = createContainerOnboard(document, schedule->at(0), schedule->at(2), vessel->slotAt(BayRowTier(2, 0, 80)));
    QCOMPARE(schedule->at(2), document->stowage()->container_routes()->portOfDischarge(container));
    FrontCallPopper(document, 0).pop(); // Action under test
    QCOMPARE(QStringLiteral("CALL2"), schedule->at(1)->uncode());
    QCOMPARE(schedule->at(1), document->stowage()->container_routes()->portOfDischarge(container));
    delete document;
}

void TestFrontCallPopper::testPopDischargeCall() {
    document_t* document = newDocument();
    const Schedule* schedule = document->schedule();
    const Vessel* vessel = document->vessel();
    createContainerOnboard(document, schedule->at(0), schedule->at(1), vessel->slotAt(BayRowTier(2, 0, 80)));
    QCOMPARE(1, document->containers()->size());
    QCOMPARE(QStringLiteral("CALL1"), schedule->at(1)->uncode());
    FrontCallPopper(document, 0).pop(); // Action under test
    QCOMPARE(0, document->containers()->size());
    QCOMPARE(QStringLiteral("CALL2"), schedule->at(1)->uncode());
    delete document;
}

void TestFrontCallPopper::testPopLoadCall() {
    document_t* document = newDocument();
    const Schedule* schedule = document->schedule();
    const Vessel* vessel = document->vessel();
    const Container* container = createContainerOnboard(document, schedule->at(1), schedule->at(2), vessel->slotAt(BayRowTier(2, 0, 80)));
    QCOMPARE(2, document->stowage()->moves(container).size());
    QCOMPARE(schedule->at(1), document->stowage()->container_routes()->portOfLoad(container));
    QCOMPARE(schedule->at(2), document->stowage()->container_routes()->portOfDischarge(container));
    FrontCallPopper(document, 0).pop(); // Action under test
    QCOMPARE(schedule->at(0), document->stowage()->container_routes()->portOfLoad(container));
    QCOMPARE(schedule->at(1), document->stowage()->container_routes()->portOfDischarge(container));
    delete document;
}

void TestFrontCallPopper::testPopRestow() {
    document_t* document = newDocument();
    const Schedule* schedule = document->schedule();
    const Vessel* vessel = document->vessel();
    const Container* container = createContainerOnboard(document, schedule->at(0), schedule->at(2), vessel->slotAt(BayRowTier(2, 0, 80)));
    container_stow_command_t* command = new container_stow_command_t(document);
    command->add_load(container, vessel->slotAt(BayRowTier(2, 0, 82)), schedule->at(1), ange::angelstow::MasterPlannedType);
    command->add_discharge(container, schedule->at(2), ange::angelstow::MasterPlannedType);
    document->undostack()->push(command);
    QCOMPARE(3, document->stowage()->moves(container).size());
    QCOMPARE(ange::angelstow::MicroStowedType, document->stowage()->moves(container).at(0)->type());
    FrontCallPopper(document, 0).pop(); // Action under test
    QCOMPARE(2, document->stowage()->moves(container).size());
    QCOMPARE(ange::angelstow::MasterPlannedType, document->stowage()->moves(container).at(0)->type());
    delete document;
}

void TestFrontCallPopper::testPopDischargeRestow() {
    document_t* document = newDocument();
    const Schedule* schedule = document->schedule();
    const Vessel* vessel = document->vessel();
    const Container* container = createContainerOnboard(document, schedule->at(0), schedule->at(2), vessel->slotAt(BayRowTier(2, 0, 80)));
    container_stow_command_t* command = new container_stow_command_t(document);
    command->add_discharge(container, schedule->at(1), ange::angelstow::MasterPlannedType);
    document->undostack()->push(command);
    QCOMPARE(2, document->stowage()->moves(container).size());
    FrontCallPopper(document, 0).pop(); // Action under test
    QCOMPARE(1, document->containers()->size());
    QCOMPARE(0, document->stowage()->moves(container).size());
    delete document;
}

QTEST_GUILESS_MAIN(TestFrontCallPopper);

#include "testfrontcallpopper.moc"
