#include <QtTest>
#include <test/util/vesseltestutils.h>
#include "../schedulepivotportcommand.h"

class TestSchedulePivotPortChangeCommand : public QObject {
    Q_OBJECT
    private Q_SLOTS:
        void testPivotPortCommand();
};

void TestSchedulePivotPortChangeCommand::testPivotPortCommand() {
    ange::schedule::Schedule* schedule1 = buildSchedule(5);

    ange::schedule::Schedule* schedule2 = buildSchedule(5);
    QUndoStack* undostack = new QUndoStack();

    QCOMPARE(schedule1->pivotCall(), schedule2->pivotCall());

    QUndoCommand* command = SchedulePivotPortCommand::setPivot(schedule1, 3);

    undostack->push(command);

    QCOMPARE(QStringLiteral("CALL3"), schedule1->pivotCall()->uncode());

    undostack->undo();

    QCOMPARE(schedule1->pivotCall(), schedule2->pivotCall());
    
    delete undostack;
    delete schedule2;
    delete schedule1;
}

QTEST_GUILESS_MAIN(TestSchedulePivotPortChangeCommand);

#include "testschedulepivotportcommand.moc"
