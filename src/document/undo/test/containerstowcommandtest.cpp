#include <QtTest>

#include "container_list.h"
#include "container_stow_command.h"
#include "document.h"
#include "fileopener.h"
#include "stowage.h"

#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>
#include <ange/vessel/slot.h>
#include <ange/vessel/vessel.h>

#include <QObject>

using namespace ange::angelstow;
using namespace ange::containers;
using namespace ange::schedule;
using namespace ange::vessel;

class ContainerStowCommandTest : public QObject {
    Q_OBJECT
private Q_SLOTS:
    void testRolStow();
};

QTEST_GUILESS_MAIN(ContainerStowCommandTest);

void ContainerStowCommandTest::testRolStow() {
    document_t* document = FileOpener::openStoFile(QFINDTESTDATA("test-data/stow-rol.sto")).document;
    stowage_t* stowage = document->stowage();
    Schedule* schedule = document->schedule();
    QCOMPARE(schedule->size(), 5);
    const Call* callAAAAA = schedule->at(1);
    QCOMPARE(callAAAAA->uncode(), QString("AAAAA"));
    const Call* callBBBBB = schedule->at(2);
    QCOMPARE(callBBBBB->uncode(), QString("BBBBB"));
    const Call* callCCCCC = schedule->at(3);
    QCOMPARE(callCCCCC->uncode(), QString("CCCCC"));
    const Call* callAfter = schedule->at(4);
    QCOMPARE(callAfter->uncode(), QString("After"));
    const Slot* slot = document->vessel()->slotAt(BayRowTier(2, 2, 80));
    QVERIFY(slot);
    const Container* container = document->containers()->get_container_by_equipment_number("ZZZU4115674");
    QVERIFY(container);

    QCOMPARE(stowage->moves(container).size(), 0);

    // Stow in call AAAAA
    container_stow_command_t* commandA = container_stow_command_t::createStowageDependentStowCommand(
        document, 0, container, slot, callAAAAA, callAfter, MicroStowedType, true);
    document->undostack()->push(commandA);
    QCOMPARE(stowage->moves(container).size(), 2);
    QCOMPARE(stowage->container_position(container, callAAAAA), slot);
    QCOMPARE(stowage->container_position(container, callBBBBB), slot);
    QCOMPARE(stowage->container_position(container, callCCCCC), slot);

    // Unstow in call BBBBB
    container_stow_command_t* commandB = new container_stow_command_t(document, 0, true);
    commandB->add_discharge(container, callBBBBB, MicroStowedType);
    document->undostack()->push(commandB);
    QCOMPARE(stowage->moves(container).size(), 2);
    QCOMPARE(stowage->container_position(container, callAAAAA), slot);
    QCOMPARE(stowage->container_position(container, callBBBBB), (Slot*)0);
    QCOMPARE(stowage->container_position(container, callCCCCC), (Slot*)0);

    // Stow in call CCCCC
    container_stow_command_t* commandC = container_stow_command_t::createStowageDependentStowCommand(
        document, 0, container, slot, callCCCCC, callAfter, MicroStowedType, true);
    document->undostack()->push(commandC);
    QCOMPARE(stowage->moves(container).size(), 4);
    QCOMPARE(stowage->container_position(container, callAAAAA), slot);
    QCOMPARE(stowage->container_position(container, callBBBBB), (Slot*)0);
    QCOMPARE(stowage->container_position(container, callCCCCC), slot);

    delete document;
}

#include "containerstowcommandtest.moc"
