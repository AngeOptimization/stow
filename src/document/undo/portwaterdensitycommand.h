#ifndef PORTWATERDENSITYCOMMAND_H
#define PORTWATERDENSITYCOMMAND_H

#include <QUndoCommand>
#include <ange/units/units.h>

class document_t;
namespace ange {
namespace schedule {
class Call;
}
}

class PortWaterDensityCommand : public QUndoCommand {

public:
    static PortWaterDensityCommand* customDensity(document_t* document, const ange::schedule::Call* call,
                                                  ange::units::Density density);
    static PortWaterDensityCommand* defaultDensity(document_t* document, const ange::schedule::Call* call);

    void undo() Q_DECL_OVERRIDE;
    void redo() Q_DECL_OVERRIDE;

private:
    PortWaterDensityCommand(document_t* document, const ange::schedule::Call* call, bool newCustom,
                            ange::units::Density newDensity);

private:
    document_t* m_document;
    const int m_callId;
    bool m_newCustom;
    ange::units::Density m_newDensity;
    bool m_oldCustom;
    ange::units::Density m_oldDensity;

};

#endif // PORTWATERDENSITYCOMMAND_H
