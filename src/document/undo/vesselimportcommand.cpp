#include "vesselimportcommand.h"

#include "bayweightlimits.h"
#include "document/document.h"
#include "document/containers/container_list.h"
#include "document/stowage/stowage.h"
#include "document/stowage/container_move.h"
#include "document/stowage/stowarbiter.h"
#include "stowplugininterface/macroplan.h"
#include "tankconditions.h"

#include <ange/containers/container.h>
#include <ange/vessel/vessel.h>
#include <ange/vessel/slot.h>
#include <ange/schedule/schedule.h>
#include <ange/vessel/bayslice.h>
#include <ange/schedule/call.h>
#include <ange/vessel/stack.h>
#include <ange/vessel/vesseltank.h>
#include <ange/vessel/compartment.h>

#include <cmath>
#include <limits>

using namespace ange::angelstow;
using namespace ange::units;
using ange::containers::Container;
using ange::vessel::Vessel;
using ange::vessel::Slot;
using ange::schedule::Call;
using ange::containers::IsoCode;
using ange::angelstow::MacroPlan;

VesselImportCommand::VesselImportCommand(const Vessel* vessel, const QByteArray& vesselFileData, document_t* document)
    : QUndoCommand(), m_vessel(vessel), m_vesselFileData(vesselFileData), m_document(document)
{
    Q_ASSERT(!m_vessel->parent());
    Q_ASSERT(!m_vesselFileData.isEmpty());
    setText(m_document->tr("Import vessel"));

    Q_FOREACH(const ange::schedule::Call* call,m_document->schedule()->calls()) {
        Q_FOREACH(const ange::vessel::VesselTank* tank, m_document->vessel()->tanks()) {
            m_tankConditions[call->id()][tank->description()] = m_document->tankConditions()->tankCondition(call,tank);
        }
    }
    Q_FOREACH(const MacroPlan::MacroStowageAllocation allocation, m_document->macro_stowage()->allAllocations()) {
        MacroStowageComponent component;
        component.loadId = allocation.load->id();
        component.dischargeId = allocation.discharge->id();
        component.brtInCompartment = allocation.compartment->stacks().first()->stackSlots().first()->brt();
        m_macroStowage << component;
    }
}

void VesselImportCommand::undo() {
  // Remove translated moves
  stowage_t* stowage = m_document->stowage();
  Q_FOREACH(const Container* container, m_document->containers()->list()) {
    stowage->change_moves(container, stowage->moves(container), QList<const container_move_t*>());
  }

    // clear the macro stowage on "new" vessel
    Q_FOREACH(const MacroPlan::MacroStowageAllocation allocation, m_document->macro_stowage()->allAllocations()) {
        m_document->macro_stowage()->removeAllocation(allocation.compartment,allocation.load, allocation.discharge);
    }
    Q_ASSERT(m_document->macro_stowage()->macroStowageEmpty());


  // Swap back old vessel
  const Vessel* new_vessel = m_document->vessel();
  m_document->setVessel(m_vessel.take());
  m_vessel.reset(new_vessel);

  QByteArray newVesselFileData = m_document->vesselFileData();
  m_document->setVesselFileData(m_vesselFileData);
  m_vesselFileData = newVesselFileData;
  Q_ASSERT(!m_vesselFileData.isEmpty());

  // Restore old moves
  m_containers_left_on_quay.clear();
  for (old_moves_t::const_iterator it = m_old_moves.begin(), iend = m_old_moves.end(); it != iend; ++it) {
    QList<const container_move_t*> restored_moves;
    const Container* container = m_document->containers()->get_container_by_id(it.key());
    Q_ASSERT(container);
    Q_FOREACH(const move_t& move, it.value()) {
      Call* call = m_document->schedule()->getCallById(move.m_call_id);
      restored_moves << new container_move_t(container, move.m_target, call, move.m_type);
    }
    stowage->change_moves(container, QList<const container_move_t*>(), restored_moves);
  }
  m_old_moves.clear();

  // Clear bay weight limits'
  Q_FOREACH(Call* call, m_document->schedule()->calls()) {
    Q_FOREACH(ange::vessel::BaySlice* bay_slice, new_vessel->baySlices()) {
      const Mass limit = m_document->bayWeightLimits()->weightLimitForBay(call, bay_slice);
      if (!std::isinf(limit / kilogram)) {
        m_document->bayWeightLimits()->setWeightLimitForBay(call, bay_slice, std::numeric_limits<double>::infinity() * kilogram);
      }
    }
  }

  // Restore old bay weight limits
  for (old_bay_weight_limits_t::const_iterator it = m_old_bay_weight_limits.constBegin(), iend = m_old_bay_weight_limits.constEnd(); it != iend; ++it) {
    Call* call = m_document->schedule()->getCallById(it.key());
    for (QHash<ange::vessel::BaySlice*, Mass>::const_iterator jit = it.value().constBegin(), jend = it.value().end(); jit != jend; ++jit) {
      m_document->bayWeightLimits()->setWeightLimitForBay(call, jit.key(), jit.value());
    }
  }
  setTankConditions();

    Q_ASSERT(m_document->macro_stowage()->macroStowageEmpty());
    Q_FOREACH(const MacroStowageComponent& component, m_macroStowage) {
        const Call* load = m_document->schedule()->getCallById(component.loadId);
        Q_ASSERT(load);
        const Call* discharge = m_document->schedule()->getCallById(component.dischargeId);
        Q_ASSERT(discharge);
        const ange::vessel::Compartment* compartment = m_document->vessel()->compartmentContaining(component.brtInCompartment);
        Q_ASSERT(compartment);
        Q_ASSERT(m_document->macro_stowage()->conflicts(compartment, load, discharge) == MacroPlan::NoConflicts);
        m_document->macro_stowage()->insertAllocation(compartment,load, discharge);
    }

  QUndoCommand::undo();
}

QDebug operator<<(QDebug dbg, const VesselImportCommand::move_t& move)
{
  dbg.nospace() << move.m_target << "@" << move.m_call_id;
  if (move.m_type == MasterPlannedType) {
    dbg << "(m)";
  } else if (move.m_type == NonPlacedType) {
    dbg << "(up)";
  }
  return dbg.maybeSpace();

}

void VesselImportCommand::redo() {
  QUndoCommand::redo();

  // body
  stowage_t* stowage = m_document->stowage();

  // store and remove old container moves
  Q_FOREACH(const Container* container, m_document->containers()->list()) {
    QList<move_t> moves;
    Q_FOREACH(const container_move_t* move, stowage->moves(container)) {
      moves << move_t(move->slot(), move->call()->id(), move->type());
    }
    m_old_moves.insert(container->id(), moves);
    stowage->change_moves(container, stowage->moves(container), QList<const container_move_t*>());
  }
    // clear the macro stowage on "new" vessel
    Q_FOREACH(const MacroPlan::MacroStowageAllocation allocation, m_document->macro_stowage()->allAllocations()) {
        m_document->macro_stowage()->removeAllocation(allocation.compartment,allocation.load, allocation.discharge);
    }
    Q_ASSERT(m_document->macro_stowage()->macroStowageEmpty());

  // Swap in the vessel
  const Vessel* old_vessel = m_document->vessel();
  const Vessel* new_vessel = m_vessel.take();
  m_document->setVessel(new_vessel);
  m_vessel.reset(old_vessel);

  QByteArray newVesselFileData = m_vesselFileData;
  m_vesselFileData = m_document->vesselFileData();
  Q_ASSERT(!m_vesselFileData.isEmpty());
  m_document->setVesselFileData(newVesselFileData);

  // translate moves to new vessel
  Q_FOREACH(const Container* container, m_document->containers()->list()) {
    QList<const container_move_t*> translated_moves;
    QList<move_t> old_moves = m_old_moves.value(container->id());
    for (int i=1; i<old_moves.size(); ++i) {
      const move_t& move_a = old_moves.at(i-1);
      const move_t& move_b = old_moves.at(i);
      const Slot* old_slot = move_a.m_target;
      ange::vessel::BayRowTier old_brt = old_slot->brt();
      if ( container->isoLength() != IsoCode::Twenty) {
        const int bay = old_slot->stack()->baySlice()->joinedBay();
        old_brt = ange::vessel::BayRowTier(bay,old_brt.row(),old_brt.tier());
      }
      const Slot* new_slot = new_vessel->slotAt(old_brt);
      const Call* from_call = m_document->schedule()->getCallById(move_a.m_call_id);
      const Call* to_call = m_document->schedule()->getCallById(move_b.m_call_id);
      StowArbiter arbiter(stowage, m_document->schedule(), new_slot, from_call);
      arbiter.setCutoff(to_call);
      arbiter.setEssentialChecksOnly(true);
      arbiter.setMicroplacedOnly(false);
      if (new_slot == 0L || !arbiter.isLegal(container)) {
        // Unable to translate slot. So don't stow container at all
        qDebug() << "Failed to translate slot " << move_a.m_target << " for container " << container;
        if (!new_slot) {
          qDebug() << "Reason: Failed to translate slot from new to old vessel";
        } else {
          qDebug() << "Reason: New load move to " << new_slot << from_call << "->" << to_call << " illegal " << arbiter.whyNot(container);
        }
        qDeleteAll(translated_moves);
        translated_moves.clear();
        m_containers_left_on_quay << container->id();
        break;
      }
      const Call* call = m_document->schedule()->getCallById(move_a.m_call_id);
      translated_moves << new container_move_t(container, new_slot, call, move_a.m_type);
    }
    if (!old_moves.empty() && !translated_moves.empty()) {
      const move_t& last_move = old_moves.last();
      const Call* call = m_document->schedule()->getCallById(last_move.m_call_id);
      Q_ASSERT(last_move.m_target == 0L);
      translated_moves << new container_move_t(container, 0L, call, last_move.m_type);
    }
    if (!translated_moves.isEmpty()) {
      stowage->change_moves(container, QList<const container_move_t*>(), translated_moves);
    }
  }

  // store and translate bay weight limits
  m_old_bay_weight_limits.clear();
  Q_FOREACH(Call* call, m_document->schedule()->calls()) {
    Q_FOREACH(ange::vessel::BaySlice* bay_slice, old_vessel->baySlices()) {
      const Mass limit = m_document->bayWeightLimits()->weightLimitForBay(call, bay_slice);
      if (!std::isinf(limit / kilogram)) {
        m_old_bay_weight_limits[call->id()].insert(bay_slice, limit);
        m_document->bayWeightLimits()->setWeightLimitForBay(call, bay_slice, std::numeric_limits<double>::infinity() * kilogram);
        if (const ange::vessel::BaySlice* new_bay_slice = new_vessel->baySliceContaining(bay_slice->joinedBay())) {
          m_document->bayWeightLimits()->setWeightLimitForBay(call, new_bay_slice, limit);
        } else {
          qDebug() << "Failed to set weight limit for " << call << " and " << bay_slice;
        }
      }
    }
  }
    Q_ASSERT(m_document->macro_stowage()->macroStowageEmpty());
    // clear the macro stowage on "new" vessel
    Q_FOREACH(const MacroStowageComponent& component, m_macroStowage) {
        const Call* load = m_document->schedule()->getCallById(component.loadId);
        Q_ASSERT(load);
        const Call* discharge = m_document->schedule()->getCallById(component.dischargeId);
        Q_ASSERT(discharge);
        const ange::vessel::Compartment* compartment = m_document->vessel()->compartmentContaining(component.brtInCompartment);
        if(compartment) { // the replaced vessel might actually be different
            if(m_document->macro_stowage()->conflicts(compartment, load, discharge) == MacroPlan::NoConflicts) {
                m_document->macro_stowage()->insertAllocation(compartment,load, discharge);
            } else {
                qDebug() << "conflict" << compartment << load << discharge << m_document->macro_stowage()->conflicts(compartment, load, discharge);
            }
        }
    }
  setTankConditions();
}

QList< int > VesselImportCommand::container_ids_left_on_quay() const {
  return m_containers_left_on_quay;
}

void VesselImportCommand::setTankConditions() {
    Q_FOREACH(int callId, m_tankConditions.keys()) {
        const ange::schedule::Call* call = m_document->schedule()->getCallById(callId);
        QHash<QString, Mass> conditions = m_tankConditions.value(callId);
        Q_FOREACH(QString tankId, conditions.keys()) {
            const ange::vessel::VesselTank* tank = m_document->vessel()->tankByDescription(tankId);
            if(tank) {
                Mass condition = conditions.value(tankId);
                m_document->tankConditions()->changeTankCondition(call, tank, condition);
            }
        }
    }
}

