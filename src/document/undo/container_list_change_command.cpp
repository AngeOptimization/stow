#include "container_list_change_command.h"

#include <algorithm>

#include <ange/containers/container.h>
#include <ange/schedule/schedule.h>
#include <ange/schedule/call.h>

#include "document/document.h"
#include "document/containers/container_list.h"
#include "document/stowage/stowage.h"
#include "document/stowage/container_leg.h"
#include <ange/containers/oog.h>
#include <ange/containers/dangerousgoodscode.h>

using ange::schedule::Schedule;
using ange::schedule::Call;

using ange::containers::Container;
using ange::units::Mass;

ListContainerUndoData::ListContainerUndoData(const int id, const int source_call_id, const int destination_source_id, ange::containers::ContainerUndoBlob* undo_blob, const ListContainerUndoData::type_t add_or_remove, const int idx ): ContainerUndoData(id, source_call_id, destination_source_id, undo_blob),
m_type(add_or_remove),
m_index(idx)
{}


bool ListContainerUndoData::operator<(const ListContainerUndoData& rhs) const
{
    return m_index < rhs.m_index;
}

container_list_change_command_t::container_list_change_command_t(document_t* document, QUndoCommand* parent):
    QUndoCommand(parent),
    m_document(document),
    m_adds(0),
    m_removes(0)
{}

container_list_change_command_t* container_list_change_command_t::remove_container(const ange::containers::Container* container) {
    Q_ASSERT(!container->bundleParent());
    Q_ASSERT(container->bundleChildren().isEmpty());
    ListContainerUndoData data(container->id(),
                             m_document->stowage()->container_routes()->portOfLoad(container)->id(),
                             m_document->stowage()->container_routes()->portOfDischarge(container)->id(),
                             container->undoData(),
                             ListContainerUndoData::REMOVE,
                             m_document->containers()->find_container(container));
    m_undo_data << data;
    m_removes++;
    set_text();
    return this;
}

void container_list_change_command_t::set_text() {
  if (m_adds == 0) {
    setText(QString("Remove %1 containers").arg(m_removes));
  } else if (m_removes==0) {
    setText(QString("Add %1 new containers").arg(m_adds));
  } else {
    setText(QString("Add %1 new containers and remove %2 containers").arg(m_adds).arg(m_removes));
  }
}

void container_list_change_command_t::add_container(const ange::containers::Container* container, const Call* load_call, const Call* discharge_call) {
    ListContainerUndoData data(container->id(),
                             load_call->id(),
                             discharge_call->id(),
                             container->undoData(),
                             ListContainerUndoData::ADD,
                             -1);
    m_undo_data << data;
    m_adds++;
    set_text();
}

void container_list_change_command_t::undo() {
  for (int i=m_undo_data.size()-1; i>=0; --i) {
    ListContainerUndoData& data = m_undo_data[i];
    switch (data.m_type) {
        case ListContainerUndoData::REMOVE: {
        add_container_implementation(data, i>0);
        break;
      }
      case ListContainerUndoData::ADD: {
        remove_container_implementation(data);
        break;
      }
    }
  }
  QUndoCommand::undo();
}

void container_list_change_command_t::redo()
{
  QUndoCommand::redo();
  for (UndoDataList::iterator it = m_undo_data.begin(), iend=m_undo_data.end(); it!=iend;++it) {
    ListContainerUndoData& data = *it;
    switch (data.m_type) {
        case ListContainerUndoData::ADD: {
        add_container_implementation(data,it+1!=iend);
        break;
      }
      case ListContainerUndoData::REMOVE: {
        remove_container_implementation(data);
        break;
      }
    }
  }
}

void container_list_change_command_t::add_container_implementation(ListContainerUndoData& data, bool more)
{
  container_list_t* container_list = m_document->containers();
  ContainerLeg* container_routes =  m_document->stowage()->container_routes();
  Schedule* schedule = m_document->schedule();
  Container* container = Container::createFromUndoData(data.m_undo_blob,
                                            data.m_id);
  data.m_id = container->id();
  const Call* source = schedule->getCallById(data.m_source_call_id);
  Q_ASSERT(source);
  const Call* destination = schedule->getCallById(data.m_destination_source_id);
  Q_ASSERT(destination);
  container_routes->add_container(container, source, destination );
  container_list->add_container(container, data.m_index, more);
  if (data.m_index == -1) {
    data.m_index = container_list->size()-1;
  }
}

void container_list_change_command_t::remove_container_implementation(ListContainerUndoData& data) {
  container_list_t* container_list = m_document->containers();
  ContainerLeg* container_routes =  m_document->stowage()->container_routes();
  Container* container = container_list->get_container_by_id(data.m_id);
  data.m_index = container_list->find_container(container); // TODO: Possible speed trap
  container_list->remove_container(container);
  container_routes->remove_container(container);
}

container_list_change_command_t::~container_list_change_command_t()
{
    Q_FOREACH(const ListContainerUndoData& data, m_undo_data){
        Container::deleteUndoData(data.m_undo_blob);
    }
}
