#ifndef FORCEGMCOMMAND_H
#define FORCEGMCOMMAND_H

#include <QUndoCommand>

#include <ange/units/units.h>

class document_t;
namespace ange {
namespace schedule {
class Call;
}
}

class ForceGmCommand : public QUndoCommand {

public:

    /**
     * Create command that forces the GM or clears it if gm is NaN
     */
    ForceGmCommand(document_t* document, const ange::schedule::Call* call, ange::units::Length gm);

    // Inherited:
    virtual void redo();
    virtual void undo();

private:
    document_t* m_document;
    const int m_callId;
    ange::units::Length m_gm;

};

#endif // FORCEGMCOMMAND_H
