#ifndef CONTAINER_UNDO_DATA_H
#define CONTAINER_UNDO_DATA_H
namespace ange {
namespace containers {
class Container;
class ContainerUndoBlob;
}
}
/**
 * Base class containing the information needed to define a container,
 * in a way suitable for undoing/redoing (i.e. where pointers
 * have been replaced by appropriate ids (call ids in particular)
 */
class ContainerUndoData
{
public:
    ContainerUndoData(const int id, const int source_call_id, const int destination_source_id, ange::containers::ContainerUndoBlob* undo_blob);
    int m_id;
    int m_source_call_id;
    int m_destination_source_id;
    ange::containers::ContainerUndoBlob* m_undo_blob;
};

#endif // CONTAINER_UNDO_DATA_H
