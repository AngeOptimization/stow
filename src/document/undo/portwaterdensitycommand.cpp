#include "portwaterdensitycommand.h"

#include "document/document.h"
#include "document/portwaterdensity/portwaterdensity.h"

#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>

using namespace ange::units;
using ange::schedule::Call;

PortWaterDensityCommand::PortWaterDensityCommand(document_t* document, const Call* call, bool newCustom, Density newDensity)
  : QUndoCommand(), m_document(document), m_callId(call->id()), m_newCustom(newCustom), m_newDensity(newDensity)
{
    m_oldCustom = document->portWaterDensity()->hasCustomWaterDensity(call);
    m_oldDensity = document->portWaterDensity()->waterDensity(call);
}

PortWaterDensityCommand* PortWaterDensityCommand::customDensity(document_t* document, const Call* call, Density density) {
    PortWaterDensityCommand* cmd = new PortWaterDensityCommand(document, call, true, density);
    cmd->setText(QString("Set custom water density for %1").arg(call->assembledName()));
    return cmd;
}

PortWaterDensityCommand* PortWaterDensityCommand::defaultDensity(document_t* document, const Call* call) {
    PortWaterDensityCommand* cmd = new PortWaterDensityCommand(document, call, false, qQNaN() * kilogram/meter3);
    cmd->setText(QString("Set default water density for %1").arg(call->assembledName()));
    return cmd;
}

void PortWaterDensityCommand::redo() {
    const Call* call = m_document->schedule()->getCallById(m_callId);
    Q_ASSERT(call);
    if (m_newCustom) {
        m_document->portWaterDensity()->setCustomWaterDensity(call, m_newDensity);
    } else {
        m_document->portWaterDensity()->setDefaultWaterDensity(call);
    }
}

void PortWaterDensityCommand::undo() {
    const Call* call = m_document->schedule()->getCallById(m_callId);
    Q_ASSERT(call);
    if (m_oldCustom) {
        m_document->portWaterDensity()->setCustomWaterDensity(call, m_oldDensity);
    } else {
        m_document->portWaterDensity()->setDefaultWaterDensity(call);
    }
}
