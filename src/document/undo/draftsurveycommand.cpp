#include "draftsurveycommand.h"

#include "document/document.h"
#include "stabilityforcer.h"

#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>
#include <ange/vessel/vessel.h>

using namespace ange::units;
using ange::schedule::Call;
using ange::vessel::BlockWeight;
using ange::vessel::Vessel;

DraftSurveyCommand::DraftSurveyCommand(document_t* document, const Call* call,
                                       bool newDeadLoadCustom, BlockWeight newDeadLoad,
                                       bool newDraftTrimCustom, Length newDraft, Length newTrim)
  : QUndoCommand(),
    m_document(document),
    m_callId(call->id()),

    m_newDeadLoadCustom(newDeadLoadCustom),
    m_newDeadLoad(newDeadLoad),
    m_newDraftTrimCustom(newDraftTrimCustom),
    m_newDraft(newDraft),
    m_newTrim(newTrim),

    m_oldDeadLoadCustom(document->stabilityForcer()->deadLoadIsActive(call)),
    m_oldDeadLoad(document->stabilityForcer()->deadLoad(call)),
    m_oldDraftTrimCustom(document->stabilityForcer()->forcedDraftTrimIsActive(call)),
    m_oldDraft(document->stabilityForcer()->draft(call)),
    m_oldTrim(document->stabilityForcer()->trim(call))
{
    // Empty
}

DraftSurveyCommand* DraftSurveyCommand::clearSurvey(document_t* document, const Call* call) {
    DraftSurveyCommand* command = new DraftSurveyCommand(document, call, false, BlockWeight(),
                                                         false, qQNaN() * meter, qQNaN() * meter);
    command->setText(QString("Clear draft survey for %1").arg(call->assembledName()));
    return command;
}

DraftSurveyCommand* DraftSurveyCommand::setDeadLoad(document_t* document, const Call* call, BlockWeight deadLoad) {
    DraftSurveyCommand* command = new DraftSurveyCommand(document, call, true, deadLoad,
                                                         false, qQNaN() * meter, qQNaN() * meter);
    command->setText(QString("Set deadload for %1").arg(call->assembledName()));
    return command;
}

static BlockWeight deadLoadBlock(const Vessel* vessel, Mass weight, Length lcg) {
    BlockWeight deadLoad(lcg, vessel->aftOverAll(), vessel->foreOverAll(), vessel->hullVcg(), 0 * meter, weight);
    deadLoad.setDescription("Deadload weight adjustment");
    return deadLoad;
}

DraftSurveyCommand* DraftSurveyCommand::setDeadLoadLcg(document_t* document, const Call* call, Length lcg) {
    BlockWeight deadLoad = deadLoadBlock(document->vessel(), document->stabilityForcer()->deadLoad(call).weight(), lcg);
    DraftSurveyCommand* command = new DraftSurveyCommand(document, call, true, deadLoad,
                                                         false, qQNaN() * meter, qQNaN() * meter);
    command->setText(QString("Set deadload lcg for %1").arg(call->assembledName()));
    return command;
}

DraftSurveyCommand* DraftSurveyCommand::setDeadLoadWeight(document_t* document, const Call* call, Mass weight) {
    BlockWeight deadLoad = deadLoadBlock(document->vessel(), weight, document->stabilityForcer()->deadLoad(call).lcg());
    DraftSurveyCommand* command = new DraftSurveyCommand(document, call, true, deadLoad,
                                                         false, qQNaN() * meter, qQNaN() * meter);
    command->setText(QString("Set deadload weight for %1").arg(call->assembledName()));
    return command;
}

DraftSurveyCommand* DraftSurveyCommand::forceTrim(document_t* document, const Call* call, Length trim) {
    DraftSurveyCommand* command = new DraftSurveyCommand(document, call, false, BlockWeight(),
                                                         true, document->stabilityForcer()->draft(call), trim);
    command->setText(QString("Set forced trim for %1").arg(call->assembledName()));
    return command;
}

DraftSurveyCommand* DraftSurveyCommand::forceDraft(document_t* document, const Call* call, Length draft) {
    DraftSurveyCommand* command = new DraftSurveyCommand(document, call, false, BlockWeight(),
                                                         true, draft, document->stabilityForcer()->trim(call));
    command->setText(QString("Set forced draft for %1").arg(call->assembledName()));
    return command;
}

void DraftSurveyCommand::redo() {
    const Call* call = m_document->schedule()->getCallById(m_callId);
    Q_ASSERT(call);
    if (m_newDeadLoadCustom) {
        m_document->stabilityForcer()->setDeadLoad(call, m_newDeadLoad);
    } else if (m_newDraftTrimCustom) {
        m_document->stabilityForcer()->setForcedDraft(call, m_newDraft);
        m_document->stabilityForcer()->setForcedTrim(call, m_newTrim);
    } else {
        m_document->stabilityForcer()->clearSurvey(call);
    }
}

void DraftSurveyCommand::undo() {
    const Call* call = m_document->schedule()->getCallById(m_callId);
    Q_ASSERT(call);
    if (m_oldDeadLoadCustom) {
        m_document->stabilityForcer()->setDeadLoad(call, m_oldDeadLoad);
    } else if (m_oldDraftTrimCustom) {
        m_document->stabilityForcer()->setForcedDraft(call, m_oldDraft);
        m_document->stabilityForcer()->setForcedTrim(call, m_oldTrim);
    } else {
        m_document->stabilityForcer()->clearSurvey(call);
    }
}
