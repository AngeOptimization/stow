#include "schedulechangerotationcommand.h"
#include <ange/schedule/schedule.h>

void ScheduleChangeRotationCommand::undo() {
    m_schedule->changeOfRotation(m_newIndex, m_index);
    QUndoCommand::undo();
}

void ScheduleChangeRotationCommand::redo() {
    QUndoCommand::redo();
    m_schedule->changeOfRotation(m_index, m_newIndex);
}

ScheduleChangeRotationCommand* ScheduleChangeRotationCommand::changeOfRotation(ange::schedule::Schedule* schedule, int index, int newIndex, QUndoCommand* parent) {
    ScheduleChangeRotationCommand* cmd = new ScheduleChangeRotationCommand(parent);
    cmd->m_index = index;
    cmd->m_schedule = schedule;
    cmd->m_newIndex = newIndex;
    cmd->setText(QObject::tr("Change Of Rotation"));
    return cmd;
}

ScheduleChangeRotationCommand::ScheduleChangeRotationCommand(QUndoCommand* parent): QUndoCommand(parent) {
    // empty
}

