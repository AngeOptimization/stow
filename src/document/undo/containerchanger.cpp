#include "containerchanger.h"

#include "changecontainercommand.h"
#include "changedischargecallcommand.h"
#include "changeloadcallcommand.h"
#include "container_leg.h"
#include "container_move.h"
#include "container_stow_command.h"
#include "createproblemcommand.h"
#include "document.h"
#include "merger_command.h"
#include "stowage.h"
#include "stowarbiter.h"

#include <ange/containers/container.h>
#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>

using namespace ange::angelstow;
using namespace ange::containers;
using namespace ange::schedule;

ChangerResult::ChangerResult(QUndoCommand* command, int problemsGenerated)
    : m_command(command), m_problemsGenerated(problemsGenerated)
{
    // Do nothing
}

QUndoCommand* ChangerResult::takeCommand() {
    return m_command.take();
}

int ChangerResult::problemsGenerated() {
    return m_problemsGenerated;
}

template <class T>
static void deleteRest(QMutableListIterator<T> it) {
    while (it.hasNext()) {
        it.next();
        it.remove();
    }
}

QSharedPointer<ChangerResult> ContainerChanger::changeLength(document_t* document, const Container* container,
                                                             IsoCode::IsoLength length) {
    merger_command_t* merger = new merger_command_t("Change length of a container");
    int problemsGenerated = 0;

    QList<container_move_t> moves;
    Q_FOREACH (const container_move_t* move, document->stowage()->moves(container)) {
        moves << *move;
    }

    merger->push((new container_stow_command_t(document))->clearMoves(container));
    merger->push(ChangeContainerCommand::change_feature(document->containers(), container, ChangeContainerCommand::Length,
        qVariantFromValue<IsoCode::IsoLength>(length)));

    QMutableListIterator<container_move_t> it(moves);
    while (it.hasNext()) {
        const container_move_t& move = it.next();
        if (move.slot() == 0) {
            continue;  // Just continue on discharge moves
        }
        StowArbiter arbiter(document->stowage(), document->schedule(), move.slot(), move.call());
        arbiter.setEssentialChecksOnly(true);
        arbiter.setException(move.container());
        if (it.hasNext()) {
            arbiter.setCutoff(it.peekNext().call());
        }
        if (!arbiter.isLegal(container)) {
            // TODO Code should be rewritten to use multiple sequential while loops like in changeDischargeCall() below
            Move simple_move = move.move();
            QString message = QString("Length change of %1 caused it to be unstowed from %2")
                .arg(container->equipmentNumber()).arg(move.call()->uncode());
            CreateProblemCommand* command;
            command = new CreateProblemCommand(document, Problem::Notice, "Container unstowed by program", message);
            command->setContainer(container);
            merger->push(command);
            ++problemsGenerated;
            it.remove();
            deleteRest(it);
            // Create fake discharge if needed
            if (!moves.isEmpty() && moves.last().slot()) {  // if last move is load
                moves << container_move_t(container, 0, simple_move.call(), simple_move.type());
            }
        }
    }
    merger->push((new container_stow_command_t(document))->addMoves(container, moves));

    return QSharedPointer<ChangerResult>(new ChangerResult(merger, problemsGenerated));
}

QSharedPointer< ChangerResult > ContainerChanger::changeLoadCall(document_t* document, const Container* container,
                                                                 const Call* newCall) {
    Q_ASSERT(newCall);
    merger_command_t* merger = new merger_command_t("Change load call of a container");
    int problemsGenerated = 0;
    const ange::schedule::Call* oldCall = document->stowage()->container_routes()->portOfLoad(container);
    Q_ASSERT(oldCall);
    if (document->schedule()->less(oldCall, newCall)) {  // shorter trip
        QList<container_move_t> moves;
        Q_FOREACH (const container_move_t* move, document->stowage()->moves(container)) {
            moves << *move;
        }
        QMutableListIterator<container_move_t> it(moves);
        it.toBack();
        // Spool just past the move at or before the new call:
        while (it.hasPrevious()) {
            if (!document->schedule()->less(newCall, it.peekPrevious().call())) {
                break;
            }
            it.previous();
        }
        // Handle the move we might want to modify
        if (it.hasPrevious()) {
            const container_move_t& move = it.previous();
            if (move.slot() == 0) {  // Just delete discharge move
                it.next();  // Move will be deleted later
            } else {  // Shorten discharge move
                container_move_t newMove(move.container(), move.slot(), newCall, move.type());
                it.setValue(newMove);
            }
        }
        while (it.hasPrevious()) {
            it.previous();
            it.remove();
        }
        // Create commands
        // Optimization? This could be done more effectively with a single complex change_moves()
        merger->push((new container_stow_command_t(document))->clearMoves(container));
        merger->push((new container_stow_command_t(document))->addMoves(container, moves));
        merger->push(new ChangeLoadCallCommand(document, container, newCall));
    } else if (document->schedule()->less(newCall, oldCall)) {  // longer trip
        merger->push(new ChangeLoadCallCommand(document, container, newCall));
        QList<const container_move_t*> moves = document->stowage()->moves(container);
        if (!moves.isEmpty()) {
            const container_move_t* firstMove = moves.first();
            Q_ASSERT(firstMove->slot());
            if (firstMove->call() == oldCall) {
                StowArbiter arbiter(document->stowage(), document->schedule(), firstMove->slot(), newCall);
                arbiter.setEssentialChecksOnly(true);
                arbiter.setCutoff(firstMove->call());
                if (arbiter.isLegal(container)) {
                    container_move_t newMove = container_move_t(container, firstMove->slot(), newCall, firstMove->type());
                    merger->push((new container_stow_command_t(document))->change_moves(
                        container, QList<const container_move_t*>() << firstMove, QList<container_move_t>() << newMove));
                } else {
                    QString message = QString("Could not forward extend trip from %1 to %2").arg(oldCall->uncode()).arg(newCall->uncode());
                    CreateProblemCommand* command = new CreateProblemCommand(document, Problem::Notice, "Container left behind", message);
                    command->setContainer(container);
                    merger->push(command);
                    problemsGenerated += 1;
                }
            }
        }
    } else {  // no change
        // Do nothing
    }
    return QSharedPointer<ChangerResult>(new ChangerResult(merger, problemsGenerated));
}

QSharedPointer<ChangerResult> ContainerChanger::changeDischargeCall(document_t* document, const Container* container,
                                                                    const ange::schedule::Call* newCall) {
    Q_ASSERT(newCall);
    merger_command_t* merger = new merger_command_t("Change discharge call of a container");
    int problemsGenerated = 0;
    const ange::schedule::Call* oldCall = document->stowage()->container_routes()->portOfDischarge(container);
    Q_ASSERT(oldCall);
    if (document->schedule()->less(newCall, oldCall)) {  // shorter trip
        QList<container_move_t> moves;
        Q_FOREACH (const container_move_t* move, document->stowage()->moves(container)) {
            moves << *move;
        }
        QMutableListIterator<container_move_t> it(moves);
        // Spool just past the move at or after the new call:
        while (it.hasNext()) {
            if (!document->schedule()->less(it.next().call(), newCall)) {
                break;
            }
        }
        // Handle the move we might want to modify
        if (it.hasPrevious()) {
            const container_move_t& move = it.peekPrevious();
            if (move.slot() != 0) {  // Just delete load move
                it.previous();  // Move will be deleted later
            } else if (move.call() != oldCall) {  // Last discharge was not in oldCall, leave it alone
                // Do nothing
            } else {  // Shorten discharge move
                container_move_t newMove(move.container(), move.slot(), newCall, move.type());
                it.setValue(newMove);
            }
        }
        deleteRest(it);
        // Create commands
        // Optimization? This could be done more effectively with a single complex change_moves()
        merger->push((new container_stow_command_t(document))->clearMoves(container));
        merger->push((new container_stow_command_t(document))->addMoves(container, moves));
        merger->push(new ChangeDischargeCallCommand(document, container, newCall));
    } else if (document->schedule()->less(oldCall, newCall)) {  // longer trip
        merger->push(new ChangeDischargeCallCommand(document, container, newCall));
        QList<const container_move_t*> moves = document->stowage()->moves(container);
        if (!moves.isEmpty()) {
            const container_move_t* lastMove = moves.last();
            Q_ASSERT(!lastMove->slot());
            if (lastMove->call() == oldCall) {
                StowArbiter arbiter(document->stowage(), document->schedule(),
                    document->stowage()->loadMoveForDischargeMove(lastMove)->slot(), lastMove->call());
                arbiter.setEssentialChecksOnly(true);
                if (arbiter.isLegal(container)) {
                    container_move_t newMove = container_move_t(container, lastMove->slot(), newCall, lastMove->type());
                    merger->push((new container_stow_command_t(document))->change_moves(
                        container, QList<const container_move_t*>() << lastMove, QList<container_move_t>() << newMove));
                } else {
                    QString details = QString("Could not extend trip from %1 to %2").arg(oldCall->uncode()).arg(newCall->uncode());
                    CreateProblemCommand* command;
                    command = new CreateProblemCommand(document, Problem::Notice, "Container left behind", details);
                    command->setContainer(container);
                    merger->push(command);
                    problemsGenerated += 1;
                }
            }
        }
    } else {  // no change
        // Do nothing
    }
    return QSharedPointer<ChangerResult>(new ChangerResult(merger, problemsGenerated));
}

static void changeLoadCall(merger_command_t* merger, int* problemsGenerated, document_t* document,
                           const Container* container, const Call* loadCall) {
    QSharedPointer<ChangerResult> result = ContainerChanger::changeLoadCall(document, container, loadCall);
    *problemsGenerated += result->problemsGenerated();
    merger->push(result->takeCommand());
}

static void changeDischargeCall(merger_command_t* merger, int* problemsGenerated, document_t* document,
                                const Container* container, const Call* dischargeCall) {
    QSharedPointer<ChangerResult> result = ContainerChanger::changeDischargeCall(document, container, dischargeCall);
    *problemsGenerated += result->problemsGenerated();
    merger->push(result->takeCommand());
}

QSharedPointer<ChangerResult> ContainerChanger::changeCalls(document_t* document, const Container* container,
                                                            const Call* loadCall, const Call* dischargeCall) {
    merger_command_t* merger = new merger_command_t("Change calls of a container");
    int problemsGenerated = 0;
    if (loadCall) {
        if (dischargeCall) {
            // When we change both load and discharge we have to do it in correct order or we will get illegal
            // state in ContainerLeg.
            // The simple solution where we reuse changeLoadCall and changeDischargeCall will create some
            // unnecessary problems when a stowed container moves past a conflicting stowed container, but
            // that case will probably never happen in real use and if it does it will just be anoying to the
            // user, not crash the program
            if (*loadCall < *document->stowage()->container_routes()->portOfDischarge(container)) {
                changeLoadCall(merger, &problemsGenerated, document, container, loadCall);
                changeDischargeCall(merger, &problemsGenerated, document, container, dischargeCall);
            } else {
                changeDischargeCall(merger, &problemsGenerated, document, container, dischargeCall);
                changeLoadCall(merger, &problemsGenerated, document, container, loadCall);
            }
        } else {
            changeLoadCall(merger, &problemsGenerated, document, container, loadCall);
        }
    } else {
        if (dischargeCall) {
            changeDischargeCall(merger, &problemsGenerated, document, container, dischargeCall);
        }
    }
    return QSharedPointer<ChangerResult>(new ChangerResult(merger, problemsGenerated));
}

// A better interface would have been just to return the QUndoCommand*, this can not generate problems
QSharedPointer<ChangerResult> ContainerChanger::changeDg(document_t* document, const Container* container,
                                                         const QList<DangerousGoodsCode>& dangerousGoodsCodes) {
    ChangeContainerCommand* command =
        ChangeContainerCommand::change_feature(document->containers(), container, ChangeContainerCommand::Dg,
                                               qVariantFromValue<QList<DangerousGoodsCode> >(dangerousGoodsCodes));
    return QSharedPointer<ChangerResult>(new ChangerResult(command, 0));
}
