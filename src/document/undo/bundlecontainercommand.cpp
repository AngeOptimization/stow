#include "bundlecontainercommand.h"
#include <document/document.h>
#include <document/containers/container_list.h>
#include <ange/containers/container.h>


using ange::containers::Container;

BundleContainerCommand::BundleContainerCommand(BundleContainerCommand::Type t, document_t* document, Container* parentContainer, QUndoCommand* parent)
    : QUndoCommand(parent)
    , m_type(t)
    , m_document(document)
    , m_bottom_container_id(parentContainer->id())
    {

}


void BundleContainerCommand::undo() {
    switch(m_type) {
        case Add:
            unbundleContainers();
            break;
        case Remove:
            bundleContainers();
            break;
    }
}

void BundleContainerCommand::redo() {
    switch(m_type) {
        case Add:
            bundleContainers();
            break;
        case Remove:
            unbundleContainers();
            break;
    }
}

void BundleContainerCommand::addContainer(const ange::containers::Container* container) {
    m_container_ids << container->id();
}

void BundleContainerCommand::bundleContainers() {
    Container* bottom_container = m_document->containers()->get_container_by_id(m_bottom_container_id);

    QList<Container*> containers = m_document->containers()->get_containers_by_id(m_container_ids);
    Q_FOREACH(Container* container, containers) {
        bottom_container->bundleContainer(container);
    }
}

void BundleContainerCommand::unbundleContainers() {
    Container* bottom_container = m_document->containers()->get_container_by_id(m_bottom_container_id);

    QList<Container*> containers = m_document->containers()->get_containers_by_id(m_container_ids);
    Q_FOREACH(Container* container, containers) {
        bottom_container->unbundleContainer(container);
    }
}


