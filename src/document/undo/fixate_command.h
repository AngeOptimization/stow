/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010

 Copyright: See COPYING file that comes with this distribution

*/

#ifndef FIXATE_COMMAND_H
#define FIXATE_COMMAND_H

#include <QUndoStack>

class document_t;
namespace ange {
namespace containers {
class Container;
}
}

/**
 * Fixate, unfixate macro stowage command
 */
class fixate_command_t : public QUndoCommand {
  public:
    /**
     * @return fixate list command
     */
    static fixate_command_t* fixate_list(document_t* document, QList<const ange::containers::Container*> containers, QUndoCommand* parent = 0L);

    /**
     * @return unfixate list command
     */
    static fixate_command_t* unfixate_list(document_t* document, QList<const ange::containers::Container*> containers, QUndoCommand* parent = 0L);

    /**
     * @override
     */
    virtual void redo();

    /**
     * @override
     */
    virtual void undo();
  private:
    fixate_command_t(document_t* document, QList<const ange::containers::Container*> containers,  bool fixate, QUndoCommand* parent);
    fixate_command_t(document_t* document, QList<ange::containers::Container*> containers,  bool fixate, QUndoCommand* parent);
    bool m_fixate;
    QList<int> m_container_ids;
    document_t* m_document;

};

#endif // FIXATE_COMMAND_H
