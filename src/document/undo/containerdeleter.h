#ifndef CONTAINERDELETER_H
#define CONTAINERDELETER_H

#include <QList>

class pool_t;
namespace ange {
namespace containers {
class Container;
}
}
class document_t;
class QUndoCommand;
/**
 * Simple helper to ensure containers are properly unstowed, unbundled and deleted
 */
class ContainerDeleter {
    public:
        /**
         * \param document
         * \param selected_pool selection pool. can be null
         * \param containersToDelete containers to delete
         * \return undo command to push for container deletion. Note that the containers might actually
         * be deleted at command return, so ensure to either push or delete the command immediately
         */
        static QUndoCommand* deleteContainers(document_t* document, pool_t* selected_pool, QList<ange::containers::Container* > containersToDelete);
};

#endif // CONTAINERDELETER_H
