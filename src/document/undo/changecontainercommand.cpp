#include "changecontainercommand.h"

#include "container_list.h"
#include "document.h"

#include <ange/containers/container.h>
#include <ange/containers/dangerousgoodscode.h>
#include <ange/containers/oog.h>

using namespace ange::units;
using namespace ange::containers;

ChangeContainerCommand::ChangeContainerCommand(container_list_t* containerList, const Container* container):
    QUndoCommand(),
    m_containerList(containerList),
    m_container(container->id())
{
    setText("Modify container");
}

ChangeContainerCommand* ChangeContainerCommand::change_feature(container_list_t* containerList, const Container* container,
                                                               Feature featureIndex, const QVariant& featureValue) {
    ChangeContainerCommand* command = new ChangeContainerCommand(containerList, container);
    command->m_featureType = featureIndex;
    command->m_undoData = featureValue;
    return command;
}

void ChangeContainerCommand::swapUndoRedoData() {
    Container* container = m_containerList->get_container_by_id(m_container);
    switch (m_featureType) {
        case Dg: {
            QList<DangerousGoodsCode> oldDgList(container->dangerousGoodsCodes());
            container->setDangerousGoodsCodes(qvariant_cast<QList<DangerousGoodsCode> >(m_undoData));
            m_undoData = qVariantFromValue<QList<DangerousGoodsCode> >(oldDgList);
            break;
        }
        case Weight: {
            Mass oldWeight = container->weight();
            container->setWeight(qvariant_cast<Mass>(m_undoData));
            m_undoData = qVariantFromValue<Mass>(oldWeight);
            break;
        }
        case Carrier: {
            QString oldCarrierCode = container->carrierCode();
            container->setCarrierCode(qvariant_cast<QString>(m_undoData));
            m_undoData = qVariantFromValue<QString>(oldCarrierCode);
            break;
        }
        case Empty: {
            bool oldEmpty = container->empty();
            container->setEmpty(qvariant_cast<bool>(m_undoData));
            m_undoData = qVariantFromValue<bool>(oldEmpty);
            break;
        }
        case Height: {
            IsoCode::IsoHeight oldIsoHeight = container->isoCode().height();
            container->setIsoHeight(qvariant_cast<IsoCode::IsoHeight>(m_undoData));
            m_undoData = qVariantFromValue<IsoCode::IsoHeight>(oldIsoHeight);
            // FIXME will not restore non-iso height on undo
            break;
        }
        case Length: {
            IsoCode::IsoLength oldIsoLength = container->isoCode().length();
            container->setIsoLength(qvariant_cast<IsoCode::IsoLength>(m_undoData));
            m_undoData = qVariantFromValue<IsoCode::IsoLength>(oldIsoLength);
            break;
        }
        case EquipmentNo: {
            QString oldEqNo = container->equipmentNumber();
            container->setEquipmentNumber(qvariant_cast<EquipmentNumber>(m_undoData));
            m_undoData = qVariantFromValue<EquipmentNumber>(oldEqNo);
            break;
        }
        case BookingNumber: {
            const QString oldBookingNo = container->bookingNumber();
            container->setBookingNumber(qvariant_cast<QString>(m_undoData));
            m_undoData = qVariantFromValue<QString>(oldBookingNo);
            break;
        }
        case PlaceOfDelivery: {
            const QString oldPlaceOfDelivery = container->placeOfDelivery();
            container->setPlaceOfDelivery(qvariant_cast<QString>(m_undoData));
            m_undoData = qVariantFromValue<QString>(oldPlaceOfDelivery);
            break;
        }
        case Temperature: {
            double oldTemperature = container->temperature();
            container->setTemperature(qvariant_cast<double>(m_undoData));
            m_undoData = qVariantFromValue<double>(oldTemperature);
            break;
        }
        case Reefer: {
            bool newReeferStatus = qvariant_cast<bool>(m_undoData);
            m_undoData = qVariantFromValue<bool>(container->isoCode().reefer());
            IsoCode isoCode(container->isoCode());
            isoCode.setReefer(newReeferStatus);
            container->setIsoCode(isoCode);
            container->setLive(newReeferStatus ? Container::Live : Container::NonLive); // FIXME will not restore this data on undo
            break;
        }
        case Live: {
            const bool oldLive = container->live() == Container::Live;
            container->setLive(qvariant_cast<bool>(m_undoData) ? Container::Live : Container::NonLive);
            m_undoData = qVariantFromValue<bool>(oldLive);
            break;
        }
        case OogFeature: {
            Oog oldOog = container->oog();
            container->setOog(qvariant_cast<Oog>(m_undoData));
            m_undoData = qVariantFromValue<Oog>(oldOog);
            break;
        }
        case HandlingCodes: {
            QList<HandlingCode> oldHandlingCodes(container->handlingCodes());
            container->setHandlingCodes(qvariant_cast<QList<HandlingCode> >(m_undoData));
            m_undoData = qVariantFromValue<QList<HandlingCode> >(oldHandlingCodes);
            break;
        }
        default: {
            qDebug() << "unexpected container feature. doing nothing: " << m_featureType;
            Q_ASSERT(false);
        }
    }
}

void ChangeContainerCommand::undo() {
    swapUndoRedoData();
    QUndoCommand::undo();
}

void ChangeContainerCommand::redo() {
    QUndoCommand::redo();
    swapUndoRedoData();
}
