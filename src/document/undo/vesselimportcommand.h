#ifndef VESSEL_IMPORT_COMMAND_H
#define VESSEL_IMPORT_COMMAND_H

#include <stowplugininterface/stowtype.h>
#include <ange/units/units.h>
#include <ange/vessel/bayrowtier.h>

#include <QByteArray>
#include <QHash>
#include <QUndoStack>

class container_move_t;
class document_t;

namespace ange {
namespace vessel {
class Vessel;
class BaySlice;
class Slot;
}
namespace schedule {
class Call;
}
}

class VesselImportCommand : public QUndoCommand {

public:
    /**
     * Import a vessel into current document
     * Takes ownership of the given vessel
     */
    VesselImportCommand(const ange::vessel::Vessel* vessel, const QByteArray& vesselFileData, document_t* document);
    virtual void undo();
    virtual void redo();
    QList<int> container_ids_left_on_quay() const;

private:
    void setTankConditions();

private:
    QScopedPointer<const ange::vessel::Vessel> m_vessel;
    QByteArray m_vesselFileData;
    document_t* m_document;

    struct move_t {
      move_t(const ange::vessel::Slot* target, int call_id, ange::angelstow::StowType type) : m_target(target), m_call_id(call_id), m_type(type) {}
      const ange::vessel::Slot* m_target;
      int m_call_id;
      ange::angelstow::StowType m_type;
    };
    friend QDebug operator<<(QDebug dbg, const move_t& move);

    typedef QHash<int, QList<move_t> >old_moves_t;
    old_moves_t m_old_moves;
    typedef QHash<int, QHash<ange::vessel::BaySlice*, ange::units::Mass> > old_bay_weight_limits_t;
    old_bay_weight_limits_t m_old_bay_weight_limits;
    QList<int> m_containers_left_on_quay;
    QHash<int, QHash<QString, ange::units::Mass> > m_tankConditions;
    struct MacroStowageComponent {
        ange::vessel::BayRowTier brtInCompartment;
        int loadId;
        int dischargeId;
    };
    QList<MacroStowageComponent> m_macroStowage;

};

#endif // VESSEL_IMPORT_COMMAND_H
