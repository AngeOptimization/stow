#ifndef CONTAINER_LIST_CHANGE_COMMAND_H
#define CONTAINER_LIST_CHANGE_COMMAND_H

#include <QUndoCommand>
#include <ange/containers/isocode.h>
#include <ange/units/units.h>
#include "container_undo_data.h"

namespace ange {
namespace schedule {
class Call;
}

namespace containers {
class Container;
//class ContainerUndoBlob;
}
}
class document_t;

/**
 * Class used to store the container-related information.
 * Also stores whether the container is removed or added to the list.
 * We might want to move this to the implementation, e.g. through
 * a d-pointer scheme.
 */
class ListContainerUndoData : public ContainerUndoData
{
public:
    enum type_t {
      ADD,
      REMOVE
    };
    ListContainerUndoData(const int id, const int source_call_id, const int destination_source_id,
                          ange::containers::ContainerUndoBlob* undo_blob, type_t type, const int index);
    bool operator<(const ListContainerUndoData& rhs) const;
    type_t m_type;
    int m_index;
};

class container_list_change_command_t : public QUndoCommand {
  public:
    container_list_change_command_t(document_t* document, QUndoCommand* parent = 0);
    ~container_list_change_command_t();
    /**
     * At redo, will remove the given @param container, extracting and storing its data
     * before.
     * The function is used when building up the list_change command
     * Consider use ContainerDeleter in the common case 
     * @returns self for chaining
     */
    container_list_change_command_t* remove_container(const ange::containers::Container* container);

    /**
     * At (re)do, will add container with the indicated properties
     * @param container: container from which data is extracted (copied
     * as a blob, pointers being handled by explicit indices).
     * The function is used when building up the list_change command
     */
    void add_container(const ange::containers::Container* container,
                       const ange::schedule::Call* load_call,
                       const ange::schedule::Call* discharge_call);
    virtual void undo();
    virtual void redo();
  private:
    document_t* m_document;
    typedef QList<ListContainerUndoData> UndoDataList;
    /**
     * List of containers to be added/removed
     */
    UndoDataList m_undo_data;
    /**
     * Number of added containers
     */
    int m_adds;
    /**
     * Number of removed containers
     */
    int m_removes;
    void set_text();
    void add_container_implementation(ListContainerUndoData& data, bool more);
    void remove_container_implementation(ListContainerUndoData& data);
};

#endif // CONTAINER_LIST_CHANGE_COMMAND_H
