#include "fixate_command.h"

#include "document/stowage/stowage.h"
#include "document/document.h"
#include "document/containers/container_list.h"

#include <ange/containers/container.h>

using ange::angelstow::MasterPlannedType;
using ange::angelstow::MicroStowedType;
using ange::containers::Container;

fixate_command_t* fixate_command_t::fixate_list(document_t* document, QList< const ange::containers::Container* > containers, QUndoCommand* parent) {
  fixate_command_t* command = new fixate_command_t(document, containers, true, parent);
  command->setText(command->m_document->tr("Fixate %1 macro-stowed containers").arg(containers.size()));
  return command;
}

fixate_command_t* fixate_command_t::unfixate_list(document_t* document, QList< const ange::containers::Container* > containers, QUndoCommand* parent) {
  fixate_command_t* command = new fixate_command_t(document, containers, false, parent);
  command->setText(command->m_document->tr("Unfixate %1 containers").arg(containers.size()));
  return command;

}

fixate_command_t::fixate_command_t(document_t* document,
                                   QList< const ange::containers::Container* > containers,
                                   bool fixate,
                                   QUndoCommand* parent) :
    QUndoCommand(parent),
    m_fixate(fixate),
    m_container_ids(),
    m_document(document)
{
  Q_FOREACH(const Container* container, containers) {
    m_container_ids << container->id();
  }

}

fixate_command_t::fixate_command_t(document_t* document,
                                   QList< ange::containers::Container* > containers,
                                   bool fixate,
                                   QUndoCommand* parent) :
    QUndoCommand(parent),
    m_fixate(fixate),
    m_container_ids(),
    m_document(document)
{
  Q_FOREACH(const Container* container, containers) {
    m_container_ids << container->id();
  }

}

void fixate_command_t::redo()
{
  QUndoCommand::redo();
  QList<Container*> containers = m_document->containers()->get_containers_by_id(m_container_ids);
  QSharedPointer<stowage_signal_lock_t> lock;
  if (containers.size()>100) {
    lock = m_document->stowage()->prepare_for_major_stowage_change();
  }
  m_document->stowage()->set_stow_type(containers, m_fixate ? MicroStowedType : MasterPlannedType);
}

void fixate_command_t::undo()
{
  QList<Container*> containers = m_document->containers()->get_containers_by_id(m_container_ids);
  QSharedPointer<stowage_signal_lock_t> lock;
  if (containers.size()>100) {
    lock = m_document->stowage()->prepare_for_major_stowage_change();
  }
  m_document->stowage()->set_stow_type(containers, m_fixate ? MasterPlannedType : MicroStowedType);
  QUndoCommand::undo();
}
