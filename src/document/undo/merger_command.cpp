#include "merger_command.h"

merger_command_t::merger_command_t(const QString& text, QUndoCommand* parent)
 : QUndoCommand(text, parent),
   m_pushed_to_stack(false)
{}

merger_command_t::merger_command_t(QUndoCommand* parent)
 : QUndoCommand(parent),
   m_pushed_to_stack(false)
{}

void merger_command_t::push(QUndoCommand* command) {
  if (!command) {
    return;
  }
  command->redo();
  if (command->id() != -1 && !m_subcommands.empty() &&
    m_subcommands.back()->id() == command->id() &&
    m_subcommands.back()->mergeWith(command))
  {
    delete command;
  } else {
    m_subcommands << command;
  }
}

void merger_command_t::undo() {
  QUndoCommand::undo();
  for (int i=m_subcommands.size()-1; i>=0; --i) {
    m_subcommands.at(i)->undo();
  }
}

void merger_command_t::redo() {
  if (m_pushed_to_stack) {
    Q_FOREACH(QUndoCommand* command, m_subcommands) {
      command->redo();
    }
  } else {
    m_pushed_to_stack = true;
  }
  QUndoCommand::redo();
}

merger_command_t::~merger_command_t()
{
  if (!m_pushed_to_stack) {
    undo();
  }
  qDeleteAll(m_subcommands);
}
