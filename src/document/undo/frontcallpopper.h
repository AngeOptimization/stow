#ifndef FRONTCALLPOPPER_H
#define FRONTCALLPOPPER_H

class pool_t;
class document_t;

/**
 * A class handling the deletion of the first call after Befor
 */
class FrontCallPopper {

public:

    /**
     * Create popper
     */
    FrontCallPopper(document_t* document, pool_t* selected_pool);

    /**
     * Pop call 1 (second call in schedule)
     */
    void pop();

private:

    document_t* m_document;
    pool_t* m_selected_pool;

};

#endif // FRONTCALLPOPPER_H
