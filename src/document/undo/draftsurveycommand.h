#ifndef DRAFTSURVEYCOMMAND_H
#define DRAFTSURVEYCOMMAND_H

#include <QUndoCommand>
#include <ange/vessel/blockweight.h>
#include <ange/units/units.h>

class document_t;
namespace ange {
namespace schedule {
class Call;
}
}

class DraftSurveyCommand : public QUndoCommand {

public:

    /**
     * Create new undo command that clears draft survey.
     * Ownership is transfered to caller
     */
    static DraftSurveyCommand* clearSurvey(document_t* document, const ange::schedule::Call* call);

    /**
     * Create new undo command that forces a static dead load in draft survey.
     * Ownership is transfered to caller
     */
    static DraftSurveyCommand* setDeadLoad(document_t* document, const ange::schedule::Call* call,
                                           ange::vessel::BlockWeight deadLoad);

    /**
     * Create new undo command that forces a static dead load in draft survey.
     * Ownership is transfered to caller
     */
    static DraftSurveyCommand* setDeadLoadWeight(document_t* document, const ange::schedule::Call* call,
                                                 ange::units::Mass weight);

    /**
     * Create new undo command that forces a static dead load in draft survey.
     * Ownership is transfered to caller
     */
    static DraftSurveyCommand* setDeadLoadLcg(document_t* document, const ange::schedule::Call* call,
                                              ange::units::Length lcg);

    /**
     * Create new undo command that forces trim in draft survey.
     * Ownership is transfered to caller
     */
    static DraftSurveyCommand* forceTrim(document_t* document, const ange::schedule::Call* call,
                                         ange::units::Length trim);

    /**
     * Create new undo command that forces draft in draft survey.
     * Ownership is transfered to caller
     */
    static DraftSurveyCommand* forceDraft(document_t* document, const ange::schedule::Call* call,
                                          ange::units::Length draft);

    void undo() Q_DECL_OVERRIDE;
    void redo() Q_DECL_OVERRIDE;

private:
    DraftSurveyCommand(document_t* document, const ange::schedule::Call* call,
                       bool newDeadLoadCustom, ange::vessel::BlockWeight newDeadLoad,
                       bool newDraftTrimCustom, ange::units::Length newDraft, ange::units::Length newTrim);

private:

    document_t* m_document;
    const int m_callId;

    bool m_newDeadLoadCustom;
    ange::vessel::BlockWeight m_newDeadLoad;
    bool m_newDraftTrimCustom;
    ange::units::Length m_newDraft;
    ange::units::Length m_newTrim;

    bool m_oldDeadLoadCustom;
    ange::vessel::BlockWeight m_oldDeadLoad;
    bool m_oldDraftTrimCustom;
    ange::units::Length m_oldDraft;
    ange::units::Length m_oldTrim;

};

#endif // DRAFTSURVEYCOMMAND_H
