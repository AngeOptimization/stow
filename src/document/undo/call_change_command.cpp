/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010

 Copyright: See COPYING file that comes with this distribution

*/

#include "call_change_command.h"

#include <stdexcept>

#include <ange/schedule/call.h>
#include <ange/schedule/cranerules.h>
#include <ange/schedule/schedule.h>

#include "document/schedulemodel.h"

using ange::schedule::Call;

call_change_command_t::call_change_command_t(ange::schedule::Schedule* schedule, ange::schedule::Call* call, int column, QVariant new_value, QUndoCommand* parent):
    QUndoCommand(parent),
    m_schedule(schedule),
    m_column(column),
    m_call_id(call->id()),
    m_oldvalue(),
    m_newvalue(new_value) {
  if (column<=0 || column>=ScheduleModel::NO_COLUMNS) {
    throw std::invalid_argument(QString("Column must be between 0 and %1, but was %2").arg(ScheduleModel::NO_COLUMNS-1).arg(column).toStdString());
  }
  Q_ASSERT(call);
  Q_ASSERT(schedule);
  switch(static_cast<ScheduleModel::column_t>(column)) {
    case ScheduleModel::UNCODE_COL:
      throw std::invalid_argument("Changing code not supported");
    case ScheduleModel::ETA_COL:
      setText(QString("Change ETA of call %1 to %2").arg(call->uncode()).arg(new_value.toDateTime().toString(Qt::DefaultLocaleShortDate)));
      m_oldvalue=call->eta();
      break;
    case ScheduleModel::ETD_COL:
      setText(QString("Change ETD of call %1 to %2").arg(call->uncode()).arg(new_value.toDateTime().toString(Qt::DefaultLocaleShortDate)));
      m_oldvalue=call->etd();
      break;
    case ScheduleModel::CRANES_COL:{
      setText(QString("Change number of cranes in %1 to %2").arg(call->uncode()).arg(new_value.toDouble()));
      m_oldvalue = call->cranes();
      break;
    }
    case ScheduleModel::HLIMIT_COL:{
      setText(QString("Change height limit stacks overdeck in %1 to %2").arg(call->uncode()).arg(new_value.toDouble()));
      m_oldvalue = call->heightLimit();
      break;
    }
    case ScheduleModel::CRANE_RULE_COL: {
      ange::schedule::CraneRules::Types new_rule = new_value.value<ange::schedule::CraneRules::Types>();
      QString new_rule_string = ange::schedule::CraneRules::toString(new_rule);
      setText(QString("Change crane bin size in %1 to %2").arg(call->uncode()).arg(new_rule_string));
      m_oldvalue = QVariant::fromValue<ange::schedule::CraneRules::Types>(call->craneRule());
      break;
    }
    case ScheduleModel::TWINLIFT_COL: {
      setText(QString("Change twinlift status in %1 to %2").arg(call->uncode()).arg(new_value.toBool()?"TRUE":"FALSE"));
      m_oldvalue = QVariant::fromValue<ange::schedule::TwinLiftRules::Types>( call->twinLifting());
      break;
    }
    case ScheduleModel::PRODUCTIVITY_COL: {
      setText(QString("Change crane productivity in %1 to %2").arg(call->uncode()).arg(new_value.toDouble()));
      m_oldvalue = call->craneProductivity();
      break;
    }
    case ScheduleModel::VOYAGE_COL:
      setText(QString("Change voyage code in %1 to %2").arg(call->uncode()).arg(new_value.toString()));
      m_oldvalue = call->voyageCode();
      break;
    case ScheduleModel::NO_COLUMNS:
      Q_ASSERT(false);
  }
}

void call_change_command_t::set_column_to_value(const QVariant& value) {
  switch(static_cast<ScheduleModel::column_t>(m_column)) {
    case ScheduleModel::UNCODE_COL:
      throw std::runtime_error("Changing code not supported");
    case ScheduleModel::ETA_COL: {
      Call* call = m_schedule->getCallById(m_call_id);
      Q_ASSERT(call);
      if(call){
        call->setEta(value.toDateTime());
      }
      break;
    }
    case ScheduleModel::ETD_COL: {
      Call* call = m_schedule->getCallById(m_call_id);
      Q_ASSERT(call);
      if(call){
        call->setEtd(value.toDateTime());
      }
      break;
    }
    case ScheduleModel::CRANES_COL: {
      Call* call = m_schedule->getCallById(m_call_id);
      Q_ASSERT(call);
      if (call) {
        call->setCranes(value.toDouble());
      }
      break;
    }
    case ScheduleModel::HLIMIT_COL: {
      Call* call = m_schedule->getCallById(m_call_id);
      Q_ASSERT(call);
      if (call) {
        call->setHeightLimit(value.toDouble());
      }
      break;
    }
    case ScheduleModel::CRANE_RULE_COL: {
      Call* call = m_schedule->getCallById(m_call_id);
      Q_ASSERT(call);
      if (call) {
        ange::schedule::CraneRules::Types crane_rule = value.value<ange::schedule::CraneRules::Types>();
        call->setCraneRule(crane_rule);
      }
      break;
    }
    case ScheduleModel::TWINLIFT_COL: {
      Call* call = m_schedule->getCallById(m_call_id);
      Q_ASSERT(call);
      if (call) {
        ange::schedule::TwinLiftRules::Types twinlifting_rule = value.value<ange::schedule::TwinLiftRules::Types>();
        call->setTwinlifting(twinlifting_rule);
      }
      break;
    }
    case ScheduleModel::PRODUCTIVITY_COL: {
      Call* call = m_schedule->getCallById(m_call_id);
      Q_ASSERT(call);
      if (call) {
        call->setCraneProductivity(value.toDouble());
      }
      break;
    }
    case ScheduleModel::VOYAGE_COL: {
      Call* call = m_schedule->getCallById(m_call_id);
      Q_ASSERT(call);
      if (call) {
        call->setVoyageCode(value.toString());
      }
      break;
    }
    case ScheduleModel::NO_COLUMNS:
      Q_ASSERT(false);
  }

}

void call_change_command_t::undo() {
  set_column_to_value(m_oldvalue);
  QUndoCommand::undo();
}

void call_change_command_t::redo() {
  set_column_to_value(m_newvalue);
  QUndoCommand::redo();
}
