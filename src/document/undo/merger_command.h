#ifndef MERGER_COMMAND_H
#define MERGER_COMMAND_H

#include <QUndoStack>


/**
 * This class works around the somewhat limited macro facility of undo stack
 * When a command is pushed to merge_command_t, it is immediately executed (with redo).
 * If the command that is pushed to the stack (that is, merge_command_t::redo is called),
 *    nothing except setting a flag is done. Calling redo again will cause all the pushed
 *    commands to be redone. undo will always undo.
 * Should merge_command_t be destroyed without being pushed to the stack, the push'ed commands
 * will be undone.
 */
class merger_command_t : public QUndoCommand {
  public:
    explicit merger_command_t(const QString& text, QUndoCommand* parent = 0);
    explicit merger_command_t(QUndoCommand* parent = 0);
    virtual ~merger_command_t();
    void push(QUndoCommand* command);
    virtual void undo();
    virtual void redo();
  private:
    QList<QUndoCommand*> m_subcommands;
    bool m_pushed_to_stack;

};

#endif // MERGER_COMMAND_H
