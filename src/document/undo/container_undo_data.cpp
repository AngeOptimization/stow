#include "container_undo_data.h"

ContainerUndoData::ContainerUndoData(const int id, const int source_call_id, const int destination_source_id, ange::containers::ContainerUndoBlob* undo_blob ) :
m_id(id),
m_source_call_id(source_call_id),
m_destination_source_id(destination_source_id),
m_undo_blob(undo_blob)
{}
