#include "frontcallpopper.h"

#include "callutils.h"
#include "changeloadcallcommand.h"
#include "container_leg.h"
#include "container_list.h"
#include "container_list_change_command.h"
#include "container_move.h"
#include "container_stow_command.h"
#include "containerchanger.h"
#include "containerdeleter.h"
#include "document.h"
#include "macrocompartmentcommand.h"
#include "stowplugininterface/macroplan.h"
#include "merger_command.h"
#include "pool.h"
#include "schedule_change_command.h"
#include "stowage.h"
#include <ange/containers/container.h>
#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>
#include <ange/vessel/bayslice.h>
#include <ange/vessel/compartment.h>
#include <ange/vessel/vessel.h>

using ange::containers::Container;
using ange::schedule::Call;
using ange::angelstow::MacroPlan;

FrontCallPopper::FrontCallPopper(document_t* document, pool_t* selected_pool) : m_document(document), m_selected_pool(selected_pool) {
    // Empty
}

void FrontCallPopper::pop() {
    const ange::schedule::Schedule* schedule = m_document->schedule();
    if (not (schedule->calls().size() > 2)) {
        Q_ASSERT(false);
        return;
    }
    const Call* befor = schedule->calls().at(0);
    Q_ASSERT(is_befor(befor));
    const Call* call = schedule->calls().at(1);
    Q_ASSERT(!is_after(call));
    const stowage_t* stowage = m_document->stowage();
    const ContainerLeg* routes = stowage->container_routes();

    // Kill the selection
    if (m_selected_pool) {
        m_selected_pool->clear();
    }

    merger_command_t* merger = new merger_command_t(QString("Remove call %1").arg(call->uncode()));

    // Delete macro stowage
    MacroPlan* macro_stowage = m_document->macro_stowage();
    Q_FOREACH (const ange::vessel::BaySlice* bay_slice, m_document->vessel()->baySlices()) {
        Q_FOREACH (const ange::vessel::Compartment* compartment, bay_slice->compartments()) {
            if (const Call* discharge_call = macro_stowage->dischargeCall(compartment, call)) {
                merger->push(MacroCompartmentCommand::remove(m_document, compartment, call, discharge_call));
            }
        }
    }

    // Delete all containers with route befor->call
    QList<Container*> containersToRemove;
    Q_FOREACH (const Container* container, m_document->containers()->list()) {
        if (routes->portOfLoad(container) == befor && routes->portOfDischarge(container) == call) {
            containersToRemove << m_document->containers()->get_container_by_id(container->id());
        }
    }
    merger->push(ContainerDeleter::deleteContainers(m_document, m_selected_pool, containersToRemove));

    // Delete load moves in befor for restows in call
    Q_FOREACH (const Container* container, m_document->containers()->list()) {
        const container_move_t* load;
        const container_move_t* restow;
        if ((load = stowage->container_moved_at_call(container, befor))
                && (restow = stowage->container_moved_at_call(container, call))) {
            QList<const container_move_t*> removedMoves;
            removedMoves << load;
            if (!restow->slot()) {
                removedMoves << restow; // also delete the discharge move for discharge restows
            }
            merger->push((new container_stow_command_t(m_document))->change_moves(container, removedMoves, QList<container_move_t>()));
        }
    }

    // Change POL for all containers with route source=call to befor
    Q_FOREACH (const Container* container, m_document->containers()->list()) {
        if (routes->portOfLoad(container) == call) {
            merger->push(new ChangeLoadCallCommand(m_document, container, befor));
        }
    }

    // Change load moves in call to be executed in befor
    Q_FOREACH (const Container* container, m_document->containers()->list()) {
        const container_move_t* load;
        if ((load = stowage->container_moved_at_call(container, call))) {
            Q_ASSERT(load->slot());
            merger->push((new container_stow_command_t(m_document))->change_moves(
                container, QList<const container_move_t*>() << load, QList<container_move_t>() << load->inOtherCall(befor)));
            }
    }

    // Delete the call
    merger->push(schedule_change_command_t::remove_call(m_document, 1));

    m_document->undostack()->push(merger);
}
