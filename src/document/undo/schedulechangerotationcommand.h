#ifndef SCHEDULECHANGEROTATIONCOMMAND_H
#define SCHEDULECHANGEROTATIONCOMMAND_H

#include <QUndoCommand>

namespace ange {
namespace schedule {
class Schedule;
}
}

/**
 * undo class for change of rotation for moving up or down
 *
 * It is the callers responsibility that the stowage is sane. This only
 * handles the modification of the schedule itself.
 */

class ScheduleChangeRotationCommand : public QUndoCommand {
    public:
        /**
         * Change Of Rotation (COR): moves the call with index @param index to position newIndex
         */
        static ScheduleChangeRotationCommand* changeOfRotation(ange::schedule::Schedule* schedule, int index, int newIndex, QUndoCommand* parent = 0);
        virtual void undo();
        virtual void redo();
    private:
        ScheduleChangeRotationCommand(QUndoCommand* parent);
        int m_index;
        int m_newIndex;
        ange::schedule::Schedule* m_schedule;
};

#endif // SCHEDULECHANGEROTATIONCOMMAND_H
