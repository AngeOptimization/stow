#include "container_stow_command.h"

#include "document/stowage/stowage.h"
#include "document/stowage/container_move.h"
#include <document/stowage/container_leg.h>
#include "document/document.h"
#include "document/containers/container_list.h"
#include <ange/containers/container.h>
#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>
#include <ange/vessel/slot.h>
#include <stdexcept>
#include "document/pools/pool.h"

using ange::angelstow::StowType;
using ange::containers::Container;
using ange::vessel::Slot;

container_stow_command_t::container_stow_command_t(document_t* document, pool_t* pool, bool add_to_selection,
                                                   QUndoCommand* parent):
    QUndoCommand(parent),
    m_document(document),
    m_add_to_selection(add_to_selection),
    m_dont_change_selection_in_first_redo(false),
    m_pool(pool)
{
  set_text();
}

container_stow_command_t::container_stow_command_t(document_t* document, QUndoCommand* parent)
    : QUndoCommand(parent), m_document(document), m_add_to_selection(false),
    m_dont_change_selection_in_first_redo(false), m_pool(0)
{
  set_text();
}

container_stow_command_t* container_stow_command_t::createStowageDependentStowCommand(document_t* document, pool_t* pool,
                                                      const Container* container, const Slot* slot,
                                                      const ange::schedule::Call* load_call,
                                                      const ange::schedule::Call* discharge_call,
                                                      StowType type, bool add_to_selection,
                                                      bool ignore_selection_for_first_redo, QUndoCommand* parent)
{
    container_stow_command_t* command = new container_stow_command_t(document, pool, add_to_selection, parent);
    command->m_dont_change_selection_in_first_redo = ignore_selection_for_first_redo;
    command->add_stow(container, slot, load_call, discharge_call, type);
    return command;
}

container_stow_command_t* container_stow_command_t::add_stow(const Container* container,
                                        const ange::vessel::Slot* slot,
                                        const ange::schedule::Call* load_call,
                                        const ange::schedule::Call* discharge_call,
                                        StowType type) {
  Q_ASSERT(!m_moves.contains(container->id()));
  Q_ASSERT(*load_call < *discharge_call);
  Q_ASSERT(load_call != discharge_call);
  Q_ASSERT(m_document->containers()->find_container(container) != -1);
  const Slot* pos = prepare(container,load_call);
  m_list << container->id();
  if (pos != slot) {
    move_t move_load_or_shift(slot, load_call->id(), type);
    m_moves[container->id()].appended << move_load_or_shift;
  }
  move_t move_discharge(0L, discharge_call->id(), type);
  m_moves[container->id()].appended << move_discharge;
  return this;
}

const ange::vessel::Slot* container_stow_command_t::prepare(const ange::containers::Container* container,
                                                        const ange::schedule::Call* call) {
  if (m_moves.contains(container->id())) {
    throw std::runtime_error("Internal error: container moves multiple times in same command");
  }
  // Clear contanier moves from this call onwards.
  QList<move_t> removes;
  const Slot* pos = 0L;
  Q_FOREACH(const container_move_t* move, m_document->stowage()->moves(container)) {
    if (call->distance(move->call()) >= 0) {
      removes << move_t(move->slot(), move->call()->id(), move->type());
    } else {
      pos = move->slot();
    }
  }
  if (!removes.empty()) {
    m_moves[container->id()].removed = removes;
  }
  return pos;
}

container_stow_command_t* container_stow_command_t::clearMoves(const Container* container) {
    prepare(container, m_document->stowage()->container_routes()->portOfLoad(container));
    m_list << container->id();
    set_text();
    return this;
}

container_stow_command_t* container_stow_command_t::add_discharge(const Container* container,
                                             const ange::schedule::Call* call,
                                             StowType type) {
  if (!m_moves.contains(container->id())) {
    m_list << container->id();
    const Slot* pos = prepare(container,call);
    if (pos) {
      move_t move_discharge(0L, call->id(), type);
      m_moves[container->id()].appended << move_discharge;
    }
  } else {
    QList<move_t>& moves = m_moves[container->id()].appended;
    // Verify that appending a discharge is ok (that is, the last move left the container on board)
    if (moves.empty()) {
      throw std::runtime_error("Internal error in add_discharge. No moves to append to.");
    }
    if (!moves.last().slot) {
      throw std::runtime_error("Internal error in add_discharge. Last move was not a load.");
    }
    if (m_document->schedule()->getCallById(moves.last().call_id)->distance(call)<=0) {
      throw std::runtime_error("Internal error in add_discharge. Last move's call was not in the past.");
    }
    move_t move_discharge(0L, call->id(), type);
    moves << move_discharge;
  }
  set_text();
  return this;
}

container_stow_command_t* container_stow_command_t::change_moves(
        const Container* container, QList<const container_move_t*> removed, QList<container_move_t> appended) {
  if (m_moves.contains(container->id())) {
    throw std::runtime_error("Add moves to the same container multiple times is not supported");
  }
  m_list << container->id();
  container_moves_t& moves = m_moves[container->id()];
  Q_FOREACH(const container_move_t* move, removed) {
    moves.removed << move_t(move->slot(), move->call()->id(), move->type());
  }
  Q_FOREACH(const container_move_t& move, appended) {
    moves.appended << move_t(move.slot(), move.call()->id(), move.type());
  }
  m_moves.insert(container->id(), moves);
  set_text();
  return this;
}

container_stow_command_t* container_stow_command_t::add_load(const Container* container,
                                        const ange::vessel::Slot* slot,
                                        const ange::schedule::Call* call,
                                        StowType type) {
  Q_ASSERT(!m_moves.contains(container->id()));
  m_list << container->id();
  const Slot* pos = prepare(container,call);
  if (pos != slot) {
    move_t move_load(slot, call->id(), type);
    m_moves[container->id()].appended << move_load;
  }
  set_text();
  return this;
}

container_stow_command_t* container_stow_command_t::addMoves(const Container* container, QList<container_move_t> moves) {
    change_moves(container, QList<const container_move_t*>(), moves);
    return this;
}

void container_stow_command_t::undo() {
  QSharedPointer<stowage_signal_lock_t> signal_lock;
  if (m_list.size() > 100) {
    signal_lock = m_document->stowage()->prepare_for_major_stowage_change();
  }
  container_list_t* containers = m_document->containers();
  // Undo backwards to make it a true undo
  int i = m_list.size();
  while (i>0) {
    const int container_id = m_list[--i];
    const Container* container = containers->get_container_by_id(container_id);
    Q_ASSERT(container);
    container_moves_t& cm = m_moves[container_id];
    QList<const container_move_t*> removed_moves = generate_removed_moves(container, cm.appended);
    QList<const container_move_t*> added_moves = generate_added_moves(container, cm.removed);
    m_document->stowage()->change_moves(container, removed_moves, added_moves);
  }
  signal_lock.clear();
  if (m_pool) {
    m_pool->reset_included_rows(m_selected);
  }
  QUndoCommand::undo();
}

void container_stow_command_t::redo() {
  QUndoCommand::redo();
  if (m_pool) {
    m_selected = m_pool->included_rows();
  }
  QSharedPointer<stowage_signal_lock_t> signal_lock;
  if (m_list.size() > 100) {
    // Big change. Tell stowage
    signal_lock = m_document->stowage()->prepare_for_major_stowage_change();
  }
  container_list_t* containers = m_document->containers();
  QList<int> moved_rows;
  Q_FOREACH(int container_id, m_list) {
    const Container* container = containers->get_container_by_id(container_id);
    Q_ASSERT(container);
    int container_row = containers->find_container(container);
    Q_ASSERT(container_row != -1);
    moved_rows << container_row;
    container_moves_t& cm = m_moves[container_id];
    QList<const container_move_t*> removed_moves = generate_removed_moves(container, cm.removed);
    QList<const container_move_t*> added_moves = generate_added_moves(container, cm.appended);
    m_document->stowage()->change_moves(container, removed_moves, added_moves);
  }
  if (m_pool) {
    if (!m_dont_change_selection_in_first_redo) {
      if (m_add_to_selection) {
        m_pool->include(moved_rows);
      } else {
        m_pool->exclude(moved_rows);
      }
    } else {
      m_dont_change_selection_in_first_redo = false;
    }
  }
}

void container_stow_command_t::set_text() {
  setText(QString("Move %1 containers").arg(m_moves.size()));
}

QList<const container_move_t*> container_stow_command_t::generate_added_moves(const Container* container,
                                                                              QList<move_t> moves) {
    QList<const container_move_t*> rv;
    Q_FOREACH(move_t move, moves) {
        // Make check to see if the move can be done. TODO: Make it possibe to stow two containers in one slot and then mark it as a problem.
        rv << new container_move_t(container, move.slot, m_document->schedule()->getCallById(move.call_id), move.type);
    }
    return rv;
}

QList<const container_move_t*> container_stow_command_t::generate_removed_moves(
    const ange::containers::Container* container, QList<container_stow_command_t::move_t> moves) {
    QList<const container_move_t*> rv;
    QList<const container_move_t*> container_moves = m_document->stowage()->moves(container);
    Q_FOREACH (move_t move, moves) {
        Q_FOREACH(const container_move_t* container_move, container_moves) {
            if (m_document->schedule()->getCallById(move.call_id) == container_move->call()
                    && move.slot == container_move->slot()
                    && move.type == container_move->type()) {
                rv << container_move;
                break;
            }
        }
    }
    Q_ASSERT(rv.size() == moves.size());
    return rv;
}

QDebug operator<<(QDebug dbg, const container_stow_command_t::move_t& move) {
  dbg << "[" << move.call_id << move.slot << move.type << "]";
  return dbg;
}
