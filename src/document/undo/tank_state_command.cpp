#include "tank_state_command.h"

#include "document/document.h"
#include "document/stowage/stowage.h"
#include "document/tanks/tankconditions.h"

#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>
#include <ange/vessel/vessel.h>
#include <ange/vessel/vesseltank.h>

using ange::vessel::VesselTank;

tank_state_command_t::tank_state_command_t(document_t* document, const ange::schedule::Call* call, QUndoCommand* parent)
    : QUndoCommand(parent), m_document(document), m_call_id(call->id()) {
      setText(QLatin1String("Change tank condition"));
}

void tank_state_command_t::insert_tank_condition(const ange::vessel::VesselTank* tank, ange::units::Mass condition) {
    const ange::schedule::Call* currentcall = m_document->schedule()->getCallById(m_call_id);
    Q_ASSERT(currentcall);
    bool found_start = false;
    Q_FOREACH(const ange::schedule::Call* call, m_document->schedule()->calls() ) {
        if(currentcall == call || found_start ) {
            m_old_conditions[call->id()][tank->id()]=m_document->tankConditions()->tankCondition(call,tank);
            m_new_conditions[call->id()][tank->id()]=condition;
            found_start = true;
        }
    }
}

void tank_state_command_t::undo() {
    Q_FOREACH(int callId, m_old_conditions.keys()) {
        QHash<int, ange::units::Mass> old_conditions = m_old_conditions.value(callId);
        const ange::schedule::Call* call = m_document->schedule()->getCallById(callId);
        Q_ASSERT(call);
        Q_FOREACH(int tankId, old_conditions.keys()) {
            const ange::vessel::VesselTank* tank = m_document->vessel()->tankById(tankId);
            Q_ASSERT(tank);
            m_document->tankConditions()->changeTankCondition(call,tank,old_conditions[tank->id()]);
        }
    }
}

void tank_state_command_t::redo(){
    Q_FOREACH(int callId, m_new_conditions.keys()) {
        QHash<int, ange::units::Mass> new_conditions = m_new_conditions.value(callId);
        const ange::schedule::Call* call = m_document->schedule()->getCallById(callId);
        Q_ASSERT(call);
        Q_FOREACH(int tankId, new_conditions.keys()) {
            const ange::vessel::VesselTank* tank = m_document->vessel()->tankById(tankId);
            Q_ASSERT(tank);
            m_document->tankConditions()->changeTankCondition(call,tank,new_conditions[tank->id()]);
        }
    }
}

