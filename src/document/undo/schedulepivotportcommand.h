#ifndef SCHEDULEPIVOTPORTCOMMAND_H
#define SCHEDULEPIVOTPORTCOMMAND_H

#include <QUndoCommand>

namespace ange {
namespace schedule {
class Schedule;
}
}

class SchedulePivotPortCommand : public QUndoCommand {
    public:
        static SchedulePivotPortCommand* setPivot(ange::schedule::Schedule* schedule, int newPivotIndex, QUndoCommand* parent = 0);
        virtual void undo();
        virtual void redo();
    private:
        explicit SchedulePivotPortCommand(ange::schedule::Schedule* schedule, int newPivotIndex, QUndoCommand* parent);
        const int m_oldPivotIndex;
        const int m_newPivotIndex;
        ange::schedule::Schedule* m_schedule;
};

#endif // SCHEDULEPIVOTPORTCOMMAND_H
