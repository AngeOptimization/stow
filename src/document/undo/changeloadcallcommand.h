/*
 * Copyright (C) 2013 Ange Optimization ApS <contact@ange.dk>
 * See COPYING file that comes with this distribution
 */
#ifndef CHANGELOADCALLCOMMAND_H
#define CHANGELOADCALLCOMMAND_H

#include <QUndoStack>

class document_t;
namespace ange {
namespace containers {
class Container;
}
namespace schedule {
class Call;
}
}

/**
 * Change the Load Call of a container
 */
class ChangeLoadCallCommand : public QUndoCommand {
public:
    explicit ChangeLoadCallCommand(document_t* document, const ange::containers::Container* container,
                                   const ange::schedule::Call* newCall, QUndoCommand* parent = 0);
    virtual void redo();
    virtual void undo();
private:
    document_t* m_document;
    int m_containerId;
    int m_newCallId;
    int m_oldCallId;
};

#endif // CHANGELOADCALLCOMMAND_H
