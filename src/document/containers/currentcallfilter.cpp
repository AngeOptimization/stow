#include "currentcallfilter.h"
#include <ange/schedule/call.h>
#include "basic_container_list.h"

using ange::schedule::Call;

CurrentCallFilter::CurrentCallFilter(basic_container_list_t* underlying_model)
  : AbstractFilter(underlying_model),
    m_from(0L)
{

    setShortName(QString::fromLocal8Bit("CCL"));
    setName(QString::fromLocal8Bit("Current Call"));

}

bool CurrentCallFilter::operator()(int row) const {
    const Call* source = qobject_cast<const Call*>(underlyingModel()->index(row, basic_container_list_t::LOAD_CALL_COL).data(Qt::EditRole).value<QObject*>());
    if (!source || source->distance(m_from)<0) {
        return false;
    }
    const Call* destination = qobject_cast<const Call*>(underlyingModel()->index(row, basic_container_list_t::DISCHARGE_CALL_COL).data(Qt::EditRole).value<QObject*>());
    if (!destination || destination->distance(m_from)>0) {
        return false;
    }
    return true;
}

void CurrentCallFilter::setCurrentCall(const ange::schedule::Call* call) {
    m_from = call;
}

#include "currentcallfilter.moc"

