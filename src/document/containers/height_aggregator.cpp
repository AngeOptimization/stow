/*
 Author: Ange Optimization <esben@ange.dk>  (C) Ange Optimization ApS 2009

 Copyright: See COPYING file that comes with this distribution
*/

#include "height_aggregator.h"

#include <QString>
#include <ange/containers/container.h>
#include <QAbstractItemModel>
#include "basic_container_list.h"

using ange::containers::IsoCode;
using ange::containers::Container;

HeightAggregator::HeightAggregator(QAbstractItemModel* model): AbstractAggregator(model) {
    Q_FOREACH (IsoCode::IsoHeight height, IsoCode::knownHeights()) {
        m_categorize_map[height] = m_categories.length();
        m_categories << IsoCode::heightToString(height);
    }
    m_categories << "Other";
    setName(QString::fromLocal8Bit("height"));
}

int HeightAggregator::operator()(int row) const {
    QModelIndex index(underlyingModel()->index(row, basic_container_list_t::ISOCODE_COL));
    IsoCode isocode(index.data().toString());
    int category = m_categorize_map.value(isocode.height(), m_categorize_map.count());
    return category;
}

int HeightAggregator::categoryCount() const {
    return m_categories.count();
}

QVariant HeightAggregator::categoryHeaderData(int category, int role) const {
    if(role == Qt::DisplayRole) {
        return m_categories.at(category);
    }
    return QVariant();
}


