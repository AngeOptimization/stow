/*
 Author: Ange Optimization <esben@ange.dk>  (C) Ange Optimization ApS 2009

 Copyright: See COPYING file that comes with this distribution
*/

#include "length_aggregator.h"

#include <QString>
#include <ange/containers/container.h>
#include <QAbstractItemModel>
#include "basic_container_list.h"

using ange::containers::IsoCode;
using ange::containers::Container;

LengthAggregator::LengthAggregator(QAbstractItemModel* model): AbstractAggregator(model) {
    Q_FOREACH (IsoCode::IsoLength length, IsoCode::allLengths()) {
        m_categorize_map[length] = m_categories.length();
        m_categories << IsoCode::lengthToString(length);
    }
    m_categories << "Other";
    setName(QString::fromLocal8Bit("length"));
}

int LengthAggregator::operator()(int row) const {
    QModelIndex index(underlyingModel()->index(row, basic_container_list_t::ISOCODE_COL));
    IsoCode isocode(index.data().toString());
    int category = m_categorize_map.value(isocode.length(), m_categorize_map.count());
    if (category == m_categorize_map.count()) {
        Q_ASSERT(false);
    }
    return category;
}

int LengthAggregator::categoryCount() const {
    return m_categories.count();
}

QVariant LengthAggregator::categoryHeaderData(int category, int role) const {
    if (role == Qt::DisplayRole) {
        return m_categories.at(category);
    }
    return QVariant();
}
