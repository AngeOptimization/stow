#ifndef STOW_CODE_AGGREGATOR_H
#define STOW_CODE_AGGREGATOR_H

#include <qdatacube/columnaggregator.h>

class basic_container_list_t;
class StowCodeAggregator : public qdatacube::ColumnAggregator {
    Q_OBJECT
    public:
        StowCodeAggregator(basic_container_list_t* model);
        virtual QVariant categoryHeaderData(int category, int role = Qt::DisplayRole) const;
    private:
        QList< QString > m_categories;

};
#endif // STOW_CODE_AGGREGATOR_H
