#ifndef TRAVERSINGFILTER_H
#define TRAVERSINGFILTER_H

#include <qdatacube/abstractfilter.h>

class stowage_t;
class basic_container_list_t;
namespace ange {
namespace schedule {
class Call;
}
}

/**
 * Checks with stowage is container is planned onboard in current call
 */
class TraversingFilter : public qdatacube::AbstractFilter {

    Q_OBJECT

public:
    explicit TraversingFilter(basic_container_list_t* underlyingModel, const stowage_t* stowage);
    virtual bool operator()(int row) const;

public Q_SLOTS:
    void setCurrentCall(const ange::schedule::Call* call);

private:
    const stowage_t* m_stowage;
    const ange::schedule::Call* m_currentCall;

};

#endif // TRAVERSINGFILTER_H
