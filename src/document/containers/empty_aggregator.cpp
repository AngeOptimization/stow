#include "empty_aggregator.h"
#include <QAbstractItemModel>
#include "basic_container_list.h"

EmptyAggregator::EmptyAggregator(QAbstractItemModel* model)
  : AbstractAggregator(model)
{
  m_category << "MTY" << "FUL";
  setName(QString::fromLocal8Bit("empty"));
}

int EmptyAggregator::operator()(int row) const {
  const bool empty = underlyingModel()->index(row, basic_container_list_t::EMPTY_COL).data(Qt::EditRole).toBool();
  return empty ? 0 : 1;
}

int EmptyAggregator::categoryCount() const {
    return m_category.count();
}

QVariant EmptyAggregator::categoryHeaderData(int category, int role) const {
    if(role == Qt::DisplayRole) {
        return m_category.at(category);
    }
    return QVariant();
}

