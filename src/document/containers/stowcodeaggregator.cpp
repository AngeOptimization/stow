#include "stowcodeaggregator.h"
#include "basic_container_list.h"

StowCodeAggregator::StowCodeAggregator(basic_container_list_t* model)
  : ColumnAggregator(model, basic_container_list_t::HANDLING_CODES_COL)
{
    setName(QString::fromLocal8Bit("Stow code"));
}


QVariant StowCodeAggregator::categoryHeaderData(int category, int role) const {
    QVariant result = qdatacube::ColumnAggregator::categoryHeaderData(category, role);
    if(role == Qt::DisplayRole) {
        if(result.toString().isEmpty()) {
            result = "-";
        }
    }
    return result;
}


#include "stowcodeaggregator.moc"
