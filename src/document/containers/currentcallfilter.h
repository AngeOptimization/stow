#ifndef CURRENTCALLFILTER_H
#define CURRENTCALLFILTER_H

#include <qdatacube/abstractfilter.h>

class basic_container_list_t;
namespace ange {
  namespace schedule {
    class Call;
  }
}
/**
 * A filter limiting the recap to containers being moved and/or traversing the current call.
 * It is the same as the Traversing filter, except that it also includes discharge moves
 */
class CurrentCallFilter : public qdatacube::AbstractFilter {
  Q_OBJECT
  public:
    explicit CurrentCallFilter(basic_container_list_t* underlying_model);
    virtual bool operator()(int row) const;
  public Q_SLOTS:
    void setCurrentCall(const ange::schedule::Call* call);
  private:
    const ange::schedule::Call* m_from;
};

#endif // CURRENTCALLFILTER_H
