#include "recapformatterweight.h"
#include <QAbstractItemModel>
#include <QEvent>
#include "basic_container_list.h"
#include <qdatacube/datacubeview.h>
#include <ange/units/units.h>

QString RecapFormatterWeight::format(QList< int > rows) const
{
  double weight = 0;
  Q_FOREACH(int row, rows) {
    weight += underlyingModel()->index(row, basic_container_list_t::WEIGHT_COL).data(Qt::EditRole).value<ange::units::Mass>()/ange::units::ton;
  }
  return QString::number(weight, 'f', 0)+"t"; //FIXME: is this function ever executed?
}

RecapFormatterWeight::RecapFormatterWeight(QAbstractItemModel* underlying_model,
                                                   qdatacube::DatacubeView* view)
 : AbstractFormatter(underlying_model, view)
{
    update(qdatacube::AbstractFormatter::CellSize);
    setShortName(tr("WGT"));
    setName("weight");
}


void RecapFormatterWeight::update(qdatacube::AbstractFormatter::UpdateType element) {
    if(element == qdatacube::AbstractFormatter::CellSize) {
        recalculateCellSize();
    }
}


void RecapFormatterWeight::recalculateCellSize() {
    if(datacubeView()) {
        // Set the cell size, by summing up all the data in the model, and using that as input
        double accumulator = 0;
        for (int element = 0, nelements = underlyingModel()->rowCount(); element < nelements; ++element) {
            accumulator += underlyingModel()->index(element, basic_container_list_t::WEIGHT_COL).data().toDouble();
        }
        QString big_cell_contents = QString::number(accumulator, 'f', 0) + "t";
        setCellSize(QSize(datacubeView()->fontMetrics().width(big_cell_contents), datacubeView()->fontMetrics().lineSpacing()));
    }
}
