#ifndef BAY_AGGREGATOR_H
#define BAY_AGGREGATOR_H

#include <qdatacube/abstractaggregator.h>

class basic_container_list_t;
namespace ange {
namespace schedule {
class Call;
}
}

/**
 * A class implementing the bay aggregator, i.e. telling which bay the container is affecting
 * in the considered call
 */
class BayAggregator : public qdatacube::AbstractAggregator {
  Q_OBJECT
  public:
    virtual int operator()(int row) const;
    explicit BayAggregator(basic_container_list_t* underlying_model);
    virtual int categoryCount() const;
    virtual QVariant categoryHeaderData(int category, int role = Qt::DisplayRole) const;
  public Q_SLOTS:
    void set_current_call(const ange::schedule::Call* call);
  public:
      static int CAT_NA;
      static int MAX_BAYS;
  private:
    QList<QString> m_categories;
    const ange::schedule::Call* m_consideredCall;
};

#endif // BAY_AGGREGATOR_H
