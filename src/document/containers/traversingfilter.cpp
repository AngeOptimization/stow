#include "traversingfilter.h"

#include "basic_container_list.h"
#include "stowage.h"

#include <ange/schedule/call.h>

using ange::schedule::Call;

TraversingFilter::TraversingFilter(basic_container_list_t* underlyingModel, const stowage_t* stowage)
    : AbstractFilter(underlyingModel), m_stowage(stowage),
      m_currentCall(0L)
{
    setShortName(QString::fromLocal8Bit("TRA"));
    setName(QString::fromLocal8Bit("Traversing"));
}

bool TraversingFilter::operator()(int row) const {
    if (!m_currentCall) {
        // this is likely a temporary state happening between creation
        // and actually setting current call. Just play safe and include everything
        return true;
    }
    const basic_container_list_t* containerList = qobject_cast<const basic_container_list_t*>(underlyingModel());
    return m_stowage->load_call(containerList->at(row), m_currentCall);
}

void TraversingFilter::setCurrentCall(const Call* call) {
    m_currentCall = call;
}

#include "traversingfilter.moc"
