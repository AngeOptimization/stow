#ifndef BUNDLABLEAGGREGATOR_H
#define BUNDLABLEAGGREGATOR_H

#include <qdatacube/abstractaggregator.h>

/**
 * \brief aggregator based on bundlability of containers
 */

class BundlableAggregator : public qdatacube::AbstractAggregator {
    Q_OBJECT

    public:
        virtual int operator()(int row) const;
        BundlableAggregator(QAbstractItemModel* model);
        virtual int categoryCount() const;
        virtual QVariant categoryHeaderData(int category, int role = Qt::DisplayRole) const;
    private:
        QList<QString> m_categories;
};

#endif // BUNDLABLEAGGREGATOR_H
