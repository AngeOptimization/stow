#ifndef CARRIER_CODE_AGGREGATOR_H
#define CARRIER_CODE_AGGREGATOR_H

#include <qdatacube/columnaggregator.h>

/**
 * replaces a empty category with a dash.
 */
class basic_container_list_t;
class CarrierCodeAggregator : public qdatacube::ColumnAggregator
{
    Q_OBJECT
public:
    CarrierCodeAggregator(basic_container_list_t* model);
    virtual QVariant categoryHeaderData(int category, int role = Qt::DisplayRole) const;
};
#endif // CARRIER_CODE_AGGREGATOR_H
