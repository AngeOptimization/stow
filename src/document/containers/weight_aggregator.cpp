
#include "weight_aggregator.h"
#include <ange/containers/container.h>

#include <cmath>
#include <cassert>
#include "basic_container_list.h"

using ange::containers::Container;

WeightAggregator::WeightAggregator(basic_container_list_t* underlying_model)
  : AbstractAggregator(underlying_model)
{
    Q_ASSERT(underlying_model);
    const int nrows = underlying_model->rowCount();
    for (int row = 0; row < nrows; ++row) {
      add_row_if_neccessary(row);
    }
    connect(underlying_model, SIGNAL(dataChanged(QModelIndex,QModelIndex)), SLOT(refresh_categories(QModelIndex,QModelIndex)));
    connect(underlying_model, SIGNAL(rowsInserted ( const QModelIndex&,int,int)), SLOT(refresh_categories(const QModelIndex&,int,int)));

    if (m_category_list.isEmpty()) {
        // No categories not supported
        m_category_list << "?";
        m_index.insert(-1,0);
    }
    setName(QString::fromLocal8Bit("weight"));
}

int WeightAggregator::categoryCount() const {
    return m_category_list.count();
}

QVariant WeightAggregator::categoryHeaderData(int category, int role) const {
    if(role == Qt::DisplayRole) {
        return m_category_list.at(category);
    }
    return QVariant();
}






int WeightAggregator::operator()(int row) const {
  QModelIndex index(underlyingModel()->index(row, basic_container_list_t::WEIGHT_COL));
  bool ok = true;
  const int weight_in_tonnes = qRound(underlyingModel()->data(index).toDouble(&ok));
  Q_UNUSED(ok);
  Q_ASSERT(ok);
  Q_ASSERT(weight_in_tonnes >= 0);
  Q_ASSERT(weight_in_tonnes <= 1000);
  if (!m_index.contains(weight_in_tonnes)) {
    add_new_category(weight_in_tonnes);
  }
  return m_index.value(weight_in_tonnes);
}

void WeightAggregator::add_new_category(int weight_in_tonnes) const {
  // Create new category
  QString category = QString::number(weight_in_tonnes) + QString::fromLocal8Bit("t");

  // Find the index where this will be inserted
  int index = 0;
  for (; index < m_category_list.size(); ++index) {
    QString candidate = m_category_list.at(index);
    if (candidate.endsWith("t")) {
      int candidate_weight = candidate.left(candidate.size()-1).toInt();
      if (candidate_weight > weight_in_tonnes) {
        break;
      }
    }
  }

  // Insert it in list, and rebuild hash
  m_category_list.insert(index, category);
  typedef QHash<int, int> index_t;
  for (index_t::iterator it = m_index.begin(), iend = m_index.end(); it != iend; ++it) {
    if (*it>=index) {
      ++*it;
    }
  }
  m_index.insert(weight_in_tonnes, index);
  emit categoryAdded(index);
}


void WeightAggregator::add_row_if_neccessary(int row) const {
  const int weight_in_tonnes = qRound(underlyingModel()->index(row, basic_container_list_t::WEIGHT_COL).data().toDouble());
  if (m_index.contains(weight_in_tonnes)) {
    return;
  }
  add_new_category(weight_in_tonnes);

}

void WeightAggregator::refresh_categories(QModelIndex top_left, QModelIndex bottom_right) {
  if (top_left.parent().isValid()) {
    return;
  }
  if (top_left.column() <= basic_container_list_t::WEIGHT_COL && bottom_right.row()>= basic_container_list_t::WEIGHT_COL) {
    for (int row=top_left.row(); row <= bottom_right.row(); ++row) {
      add_row_if_neccessary(row);
    }
  }
}

void WeightAggregator::refresh_categories(const QModelIndex& parent, int start, int end) {
  if (parent.isValid()) {
    return;
  }
  for (int row=start; row <= end; ++row) {
    add_row_if_neccessary(row);
  }
}

#include "weight_aggregator.moc"
