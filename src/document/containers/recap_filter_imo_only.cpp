#include "recap_filter_imo_only.h"
#include <QAbstractItemModel>
#include "basic_container_list.h"
#include <ange/containers/dangerousgoodscode.h>

recap_filter_imo_only_t::recap_filter_imo_only_t(QAbstractItemModel* underlying_model)
  : AbstractFilter(underlying_model) {
    setShortName("IMO");
    setName("IMO only");
}

bool recap_filter_imo_only_t::operator()(int row) const {
    const QList<ange::containers::DangerousGoodsCode> dg_codes = underlyingModel()->index(row, basic_container_list_t::DG_COL).data(Qt::EditRole).value<QList<ange::containers::DangerousGoodsCode> >();
    return !dg_codes.empty();
}

