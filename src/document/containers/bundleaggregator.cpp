#include "bundleaggregator.h"
#include "basic_container_list.h"
#include <ange/containers/container.h>
#include <stdexcept>


int BundleAggregator::operator()(int row) const {
    const basic_container_list_t* list = qobject_cast<const basic_container_list_t*>(underlyingModel());
    Q_ASSERT(list);
    if(!list) {
        throw std::runtime_error("bad cast in ");
    }

    const ange::containers::Container* container = list->get_container_at(row);
    if(!container->bundleChildren().isEmpty()) {
        return 1;
    } else if(container->bundleParent()) {
        return 2;
    } else {
        return 0;
    }
}

BundleAggregator::BundleAggregator(QAbstractItemModel* model) : AbstractAggregator(model) {
    m_categories << "Container" << "Bottom" << "Bundled";
    setName("Bundle");
}

int BundleAggregator::categoryCount() const {
    return m_categories.count();
}

QVariant BundleAggregator::categoryHeaderData(int category, int role) const {
    if(role == Qt::DisplayRole) {
        return m_categories.at(category);
    }
    return QVariant();
}



#include "bundleaggregator.moc"
