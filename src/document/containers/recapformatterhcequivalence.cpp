#include "recapformatterhcequivalence.h"
#include <QAbstractItemModel>
#include <QEvent>
#include "basic_container_list.h"
#include <qdatacube/datacubeview.h>
#include <ange/units/units.h>
#include <ange/containers/isocode.h>
#include "hcequivalenceconstants.h"

QString RecapFormatterHCEquivalence::format(QList< int > rows) const {
    return QString::number(HCequivalence(rows), 'f', 0);
}

RecapFormatterHCEquivalence::RecapFormatterHCEquivalence(QAbstractItemModel* underlying_model,
                                                   qdatacube::DatacubeView* view)
 : AbstractFormatter(underlying_model, view)
{
    update(qdatacube::AbstractFormatter::CellSize);
    setShortName(tr("HCQ"));
    setName("hc equivalence");
}


void RecapFormatterHCEquivalence::update(qdatacube::AbstractFormatter::UpdateType element) {
    if(element == qdatacube::AbstractFormatter::CellSize) {
        recalculateCellSize();
    }
}

double RecapFormatterHCEquivalence::HCequivalence(const QList< int > rows) const {
    // Set the cell size, by summing up all the data in the model, and using that as input
    double accumulator = 0;
    Q_FOREACH(const int row, rows) {
        QModelIndex index(underlyingModel()->index(row, basic_container_list_t::ISOCODE_COL));
        const ange::containers::IsoCode isocode(index.data().toString());
        double equivalence  = 0;
        if (isocode.height() == ange::containers::IsoCode::HC) {
            if (isocode.length() == ange::containers::IsoCode::Fourty) {
                equivalence = hc40teuEquivalence;
            } else if (isocode.length() == ange::containers::IsoCode::FourtyFive) {
                equivalence = hc45teuEquivalence;
            }
        } else if (isocode.length() == ange::containers::IsoCode::Twenty) {
            equivalence = 1.0;
        } else {
            equivalence = 2.0;
        }
        accumulator += equivalence;
    }
    return accumulator;
}


void RecapFormatterHCEquivalence::recalculateCellSize() {
    if(!datacubeView()) {
        return;
    }
    QList <int> allIndices;
    for(int i = 0; i < underlyingModel()->rowCount(); ++i) {
        allIndices << i;
    }
    QString big_cell_contents = QString::number(HCequivalence(allIndices), 'f', 0) + "t";
    setCellSize(QSize(datacubeView()->fontMetrics().width(big_cell_contents), datacubeView()->fontMetrics().lineSpacing()));
}
