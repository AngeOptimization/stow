#include "recapfiltermovesonly.h"
#include <qdatacube/filterbyaggregate.h>
#include <QSharedPointer>
#include <basic_container_list.h>

using namespace qdatacube;

RecapFilterMovesOnly::RecapFilterMovesOnly(basic_container_list_t* underlyingModel)
           : OrFilter(underlyingModel), m_stowageStatusAggregator(new StowageStatusAggregator(underlyingModel)) {
    setShortName("MOV");
    setName("moves only");
    QObject::connect(this, SIGNAL(setCurrentCall(const ange::schedule::Call*)),
             m_stowageStatusAggregator.data(), SLOT(setCurrentCall(const ange::schedule::Call*)));
    addFilter(AbstractFilter::Ptr(new FilterByAggregate(m_stowageStatusAggregator,
                                    StowageStatusAggregator::CAT_LOADED)));
    addFilter(AbstractFilter::Ptr(new FilterByAggregate(m_stowageStatusAggregator,
                                    StowageStatusAggregator::CAT_SHIFTED)));
    addFilter(AbstractFilter::Ptr(new FilterByAggregate(m_stowageStatusAggregator,
                                    StowageStatusAggregator::CAT_DISCHARGED)));
    // TODO some UNPLANNED containers are also moved, see #1409
}

RecapFilterMovesOnly::~RecapFilterMovesOnly() {}
