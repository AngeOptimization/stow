#include "container_list.h"

#include "document/stowage/stowage.h"
#include "document/stowage/container_move.h"
#include "document/stowage/container_leg.h"
#include "document/utils/callutils.h"
#include "document/document.h"
#include "document/undo/changecontainercommand.h"
#include "pluginmanager.h"

#include <QUndoCommand>
#include <QStringList>

#include <stowplugininterface/validatorplugin.h>
#include <ange/vessel/slot.h>
#include <ange/vessel/stack.h>
#include <ange/schedule/call.h>
#include <ange/containers/container.h>

using ange::vessel::Slot;
using ange::vessel::Stack;
using ange::vessel::BayRowTier;
using ange::schedule::Call;
using ange::containers::Container;

container_list_t::container_list_t(stowage_t* stowage, QObject* parent) :
    basic_container_list_t(parent),
    m_stowage(stowage),
    m_current_call(0L) {
  Q_ASSERT(stowage);
  connect(this, SIGNAL(rowsInserted (const QModelIndex, int, int)), SLOT(inform_plugins_of_insertions(const QModelIndex, int, int)));
  connect(this, SIGNAL(rowsAboutToBeRemoved(const QModelIndex, int, int)), SLOT(inform_plugins_of_removals(const QModelIndex, int, int)));
  connect(this, SIGNAL(dataChanged(QModelIndex,QModelIndex)), SLOT(inform_plugins_of_changes(QModelIndex,QModelIndex)));
  connect(stowage->container_routes(), SIGNAL(containerChangedPOL(const ange::containers::Container*)), SLOT(emit_container_changed_source(const ange::containers::Container*)));
  connect(stowage->container_routes(), SIGNAL(containerChangedPOD(const ange::containers::Container*)), SLOT(emit_container_changed_destination(const ange::containers::Container*)));
}

void container_list_t::inform_plugins_of_insertions(const QModelIndex index, int start, int end) {
  Q_ASSERT(!index.isValid());
  Q_UNUSED(index);
  Q_FOREACH (ange::angelstow::ValidatorPlugin* plugin, m_stowage->pluginManager()->validatorPlugins()) {
    for (int row = start; row<=end; ++row) {
      validateReverseIndex();
      plugin->containerAdded(m_containers[row]);
      validateReverseIndex();
    }
  }
}

void container_list_t::inform_plugins_of_removals(const QModelIndex index, int start, int end)
{
  Q_ASSERT(!index.isValid());
  Q_UNUSED(index);
  Q_FOREACH (ange::angelstow::ValidatorPlugin* plugin, m_stowage->pluginManager()->validatorPlugins()) {
    for (int row = start; row<=end; ++row) {
      validateReverseIndex();
      plugin->containerRemoved(m_containers[row]);
      validateReverseIndex();
    }
  }
}

void container_list_t::inform_plugins_of_changes(const QModelIndex top_left, const QModelIndex bottom_right) {
  if (top_left.column() >= PLANNED_COL && bottom_right.column() >= LOCATION_COL) {
    // Only planned/location updated, those column are not really part of the container
    return;
  }
  Q_FOREACH (ange::angelstow::ValidatorPlugin* plugin, m_stowage->pluginManager()->validatorPlugins()) {
    for(int row = top_left.row(); row <= bottom_right.row(); ++row) {
      validateReverseIndex();
      plugin->containerChanged(m_containers[row]);
      validateReverseIndex();
    }
  }
}

QVariant container_list_t::container_display_data(const Container* container, container_list_t::container_columns_t col) const {
  switch (col) {
    case EQUIPMENT_NO_COL:
    case ISOCODE_COL:
    case REEFER_COL:
    case LIVE_COL:
    case WEIGHT_COL:
    case VGM_COL:
    case DG_COL:
    case TEMPERATURE_COL:
    case CARRIER_COL:
    case NOMINAL_POD_COL:
    case PLACE_OF_DELIVERY_COL:
    case NOMINAL_POL_COL:
    case EMPTY_COL:
    case BOOKING_NUMBER_COL:
    case OOG_TOP_COL:
    case OOG_BACK_COL:
    case OOG_FRONT_COL:
    case OOG_LEFT_COL:
    case OOG_RIGHT_COL:
    case HANDLING_CODES_COL:
    case BUNDLE_COL:
      return basic_container_list_t::container_display_data(container, col);
    case MOVES_COL: {
        QStringList calls;
        Q_FOREACH(const container_move_t* move, moves_for_container(container)) {
            calls << move->call()->uncode();
        }
        return calls.join(":");
    }
    case LOAD_CALL_COL: {
      const ange::schedule::Call* call = m_stowage->container_routes()->portOfLoad(container);
      if(is_befor(call) || is_after(call)) {
        return call->uncode() + ": " + container->loadPort();
      } else {
        return call->uncode();
      }
    }
    case DISCHARGE_CALL_COL: {
      const ange::schedule::Call* call = m_stowage->container_routes()->portOfDischarge(container);
      if(is_befor(call) || is_after(call)) {
        return call->uncode() + ": " + container->dischargePort();
      } else {
        return call->uncode();
      }
    }
    case LOCATION_COL: {
      const Container* c;
      if(container->bundleParent()) {
        c = container->bundleParent();
      } else {
        c = container;
      }
      if (m_current_call) {
        if (m_stowage->container_routes()->portOfLoad(c)->distance(m_current_call) < 0 ||
          m_stowage->container_routes()->portOfDischarge(c)->distance(m_current_call) >= 0) {
          return "n/a";
        }
        BayRowTier pos = m_stowage->nominal_position(c, m_current_call);
        if (pos.placed()) {
          return pos.toString();
        } else {
          return "yard";
        }
      }
      return QVariant();
    }
    case PLANNED_COL: {
      if(container->bundleParent()) {
        return "B";
      }
      QList<const container_move_t*> moves = m_stowage->moves(container);
      const ange::schedule::Call* const destination = m_stowage->container_routes()->portOfDischarge(container);
      bool unplanned = (moves.empty() || moves.last()->call() != destination);
      if (!unplanned) {
          return "P";
      } else {
        return QVariant();
      }
    }
    case NCOLS: {
      Q_ASSERT(false);
      break;
    }
  }
  return QVariant();
}

void container_list_t::slot_container_moved(const Container* container, const ange::vessel::Slot* /*previous*/, const ange::vessel::Slot* /*current*/, const Call* /*call*/) {
  const int row = find_container(container);
  if (row != -1) {
    emit dataChanged(index(row, PLANNED_COL), index(row, LOCATION_COL));
    Q_FOREACH(Container* subcontainer, container->bundleChildren()) {
        const int subrow = find_container(subcontainer);
        if(subrow != -1) {
            emit dataChanged(index(subrow, PLANNED_COL), index(subrow, LOCATION_COL));
        }
    }
  }
}

void container_list_t::update_container_positions() {
      validateReverseIndex();
  if(!m_containers.empty()) {
    emit dataChanged(index(0, PLANNED_COL), index(rowCount()-1, LOCATION_COL));
  }
}

void container_list_t::set_current_call(const ange::schedule::Call* call) {
    Q_ASSERT(call);
    Q_ASSERT(!m_current_call || !call || m_current_call->parent() == call->parent());
    m_current_call = call;
    if (rowCount()>0) {
        emit dataChanged(index(0, LOCATION_COL), index(rowCount()-1, LOCATION_COL));
    }
}

QVariant container_list_t::move_data(const container_move_t* move, basic_container_list_t::moves_columns_t column, int role) const {
      validateReverseIndex();
  if (role == Qt::EditRole) {
    switch (column) {
      case MOVES_CALL_COL:
        return QVariant::fromValue<QObject*>(const_cast<Call*>(move->call()));
      case MOVES_SLOT_COL:
        return QVariant::fromValue<QObject*>(const_cast<Slot*>(move->slot()));
      case MOVES_POSITION_COL:
        return QVariant::fromValue<ange::vessel::BayRowTier>(m_stowage->nominal_position(move->container(), move->call()));
      case MOVES_TYPE_COL:
        return QVariant::fromValue(move->type());
      case MOVES_NCOLS:
        return QVariant();
    }
  }
  return QVariant();
}

QList<const container_move_t*> container_list_t::moves_for_container(const ange::containers::Container* container) const {
    return m_stowage->moves(container);
}

QVariant container_list_t::container_edit_data(const ange::containers::Container* container, basic_container_list_t::container_columns_t col) const {
  switch(col) {
    case LOAD_CALL_COL: {
        Call* call = const_cast<Call*>(m_stowage->container_routes()->portOfLoad(container));
        Q_ASSERT(!m_current_call || m_current_call->parent() == call->parent());
        return QVariant::fromValue<QObject*>(call);
    }
    case DISCHARGE_CALL_COL:
      return QVariant::fromValue<QObject*>(const_cast<Call*>(m_stowage->container_routes()->portOfDischarge(container)));
    default:
      return basic_container_list_t::container_edit_data(container, col);
  }
}


QList< const ange::containers::Container* > container_list_t::get_containers_by_discharge_port(const ange::schedule::Call* call) const {
  QList<const ange::containers::Container*> rv;
  Q_FOREACH(const ange::containers::Container* container, m_containers) {
    if(m_stowage->container_routes()->portOfDischarge(container)==call) {
      rv << container;
    }
  }
  return rv;
}

QList< const ange::containers::Container* > container_list_t::get_containers_by_load_port(const ange::schedule::Call* call) const {
  QList<const ange::containers::Container*> rv;
  Q_FOREACH(const ange::containers::Container* container, m_containers) {
    if(m_stowage->container_routes()->portOfLoad(container)==call) {
      rv << container;
    }
  }
  return rv;
}

void container_list_t::slot_container_changed(const ange::containers::Container* container) {
  const int row = find_container(container);
  if ( row>=0 ) {
    emit dataChanged(index(row, 0), index(row, NCOLS-1));
  }
}


void container_list_t::emit_container_changed_destination(const ange::containers::Container* container)
{
  const int row = find_container(container);
  if (row != -1) {
    const QModelIndex i = index(row,DISCHARGE_CALL_COL);
    emit dataChanged(i,i);
    emit container_changed(container);
  }
}
void container_list_t::emit_container_changed_source(const ange::containers::Container* container)
{
  const int row = find_container(container);
  if (row != -1) {
    const QModelIndex i = index(row,LOAD_CALL_COL);
    emit dataChanged(i,i);
    emit container_changed(container);
  }
}

#include "container_list.moc"
