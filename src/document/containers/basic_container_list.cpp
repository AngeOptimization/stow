#include "basic_container_list.h"
#include <ange/containers/container.h>
#include <ange/containers/dangerousgoodscode.h>
#include <ange/containers/oog.h>
#include <ange/schedule/call.h>
#include "document/undo/changecontainercommand.h"
#include <document/utils/containerutils.h>

using namespace ange::units;
using ange::containers::Container;

basic_container_list_t::basic_container_list_t( QObject* parent ) :
    QAbstractItemModel( parent ), m_reverseindex_dirty(false)
{
    validateReverseIndex();
    // Checking that we haven't done something bad. This should preferably be a static_assert once we have them available
    // later on, we do something like (parent.row() << 4) + parent.column() for these columns, and if they are
    // larger than 15, the row count and column count start to overlap.
    Q_ASSERT(MOVES_COL < 16);
    Q_ASSERT(DG_COL < 16);
}

QVariant basic_container_list_t::data( const QModelIndex& index, int role ) const {
    validateReverseIndex();
    if(!index.isValid()) {
        return QVariant();
    }
  if ( !index.parent().isValid() ) {
    // toplevel
    container_columns_t col = static_cast<container_columns_t>( index.column() );
    if ( role == Qt::DisplayRole ) {
      return container_display_data( m_containers.at( index.row() ), col );
    } else if ( role == Qt::EditRole) {
      return container_edit_data( m_containers.at( index.row() ), col );
    } else if( role == Qt::UserRole) {
        Container* container = m_containers.at(index.row());
        return QVariant::fromValue<ange::containers::Container*>(container);
    }
  } else if ( !index.parent().parent().isValid() ) {
    QModelIndex parent = index.parent();
    if (parent.column() == MOVES_COL) {
      QList<const container_move_t*> moves = moves_for_container(m_containers.at(parent.row()));
      if (index.row() < moves.size()) {
        return move_data(moves.at(index.row()), static_cast<moves_columns_t>(index.column()), role);
      } else {
        return QVariant();
      }
    } else if(parent.column() == DG_COL) {
      return ContainerUtils::dangerousGoodsCodesAsParsableString(m_containers.at(parent.row())->dangerousGoodsCodes());
    } else if (parent.column() == HANDLING_CODES_COL) {
      if (index.column() == 0) {
        return m_containers.at(index.parent().row())->handlingCodes().value(index.row()).code();
      }
    }
    return QVariant();
  }
  return QVariant();
}

Qt::ItemFlags basic_container_list_t::flags(const QModelIndex& index) const {
    validateReverseIndex();
    return QAbstractItemModel::flags(index);
}

int basic_container_list_t::columnCount( const QModelIndex& parent ) const {
    validateReverseIndex();
  if ( !parent.isValid() ) {
    // toplevel
    return NCOLS;
  } else if ( !parent.parent().isValid() ) {
    // 2nd level item
    container_columns_t col = static_cast<container_columns_t>( parent.column() );
    switch ( col ) {
      case DISCHARGE_CALL_COL:
      case LOAD_CALL_COL:
      case EQUIPMENT_NO_COL:
      case ISOCODE_COL:
      case REEFER_COL:
      case LIVE_COL:
      case WEIGHT_COL:
      case VGM_COL:
      case TEMPERATURE_COL:
      case PLANNED_COL:
      case CARRIER_COL:
      case NOMINAL_POD_COL:
      case NOMINAL_POL_COL:
      case PLACE_OF_DELIVERY_COL:
      case EMPTY_COL:
      case BOOKING_NUMBER_COL:
      case OOG_TOP_COL:
      case OOG_BACK_COL:
      case OOG_FRONT_COL:
      case OOG_RIGHT_COL:
      case OOG_LEFT_COL:
      case BUNDLE_COL:
        return 0; // no subitems
      case LOCATION_COL:
        return 0;
      case MOVES_COL: {
        return MOVES_NCOLS;
      }
      case DG_COL:
        return DG_NCOLS;
      case HANDLING_CODES_COL: {
        return 1; // List
      }
      case NCOLS:
        Q_ASSERT( false );
        return 0L;
    }
  }
  return 0;
}

int basic_container_list_t::rowCount( const QModelIndex& parent ) const {
    validateReverseIndex();
  if (parent.isValid()) {
    if (parent.parent().isValid()) {
      return 0;
    }
    container_columns_t col = static_cast<container_columns_t>( parent.column() );
    switch (col) {
      case MOVES_COL:
        return moves_for_container(m_containers.at(parent.row())).size();
      case DG_COL:
        return m_containers.at(parent.row())->dangerousGoodsCodes().size();
      case HANDLING_CODES_COL:
        return m_containers.at(parent.row())->handlingCodes().size();
      default:
        return 0;
    }
  } else {
    return m_containers.size();
  }
}

QModelIndex basic_container_list_t::parent( const QModelIndex& child ) const {
    validateReverseIndex();
  quint32 internalid = child.internalId();
  return internalid == 0xFFFFFFFF ? QModelIndex() : index(internalid >> 4, internalid & 0xF);
}

QModelIndex basic_container_list_t::index( int row, int column, const QModelIndex& parent ) const {
    validateReverseIndex();
  Q_ASSERT( row >= 0 );
  Q_ASSERT( column >= 0 );
    if(!hasIndex(row,column, parent)) {
        return QModelIndex();
    }
  if (!parent.isValid()) {
    Q_ASSERT( row < m_containers.size() );
    Q_ASSERT( column < columnCount() );
  }
  return parent.isValid() ? createIndex(row, column, (parent.row() << 4) + parent.column()) : createIndex( row, column, 0xFFFFFFFF );
}

QVariant basic_container_list_t::headerData( int section, Qt::Orientation orientation, int role ) const {
    validateReverseIndex();
  if ( role == Qt::DisplayRole ) {
    if ( orientation == Qt::Vertical ) {
      return section;
    } else {
      container_columns_t col = static_cast<container_columns_t>( section );
      return vertical_header_display_data( col );
    }
  }
  return QVariant();
}

QVariant basic_container_list_t::vertical_header_display_data( basic_container_list_t::container_columns_t column ) const {
    validateReverseIndex();
  switch ( column ) {
    case EQUIPMENT_NO_COL:
      return tr( "Equipment No" );
    case ISOCODE_COL:
      return tr( "Type" );
    case REEFER_COL:
      return tr( "Reefer" );
    case LIVE_COL:
      return tr( "Live" );
    case WEIGHT_COL:
      return tr( "Weight" );
    case VGM_COL:
      return tr( "VGM" );
    case MOVES_COL:
      return tr( "Moves" );
    case DG_COL:
      return tr( "DG" );
    case TEMPERATURE_COL:
      return tr( "Temp" );
    case DISCHARGE_CALL_COL:
      return tr( "POD" );
    case LOAD_CALL_COL: {
      return tr( "POL" );
    }
    case PLANNED_COL: {
      return tr( "Planned" );
    }
    case CARRIER_COL: {
      return tr( "Carrier" );
    }
    case LOCATION_COL: {
      return tr("Location");
    }
    case NOMINAL_POD_COL: {
      return tr("Nom.POD");
    }
    case PLACE_OF_DELIVERY_COL: {
      return tr("PoDelivery");
    }
    case NOMINAL_POL_COL: {
      return tr("Nom.POL");
    }
    case EMPTY_COL: {
      return tr("Empty");
    }
    case BOOKING_NUMBER_COL: {
      return tr("Book. N.");
    }
    case HANDLING_CODES_COL: {
      return tr("Stow.");
    }
    case OOG_TOP_COL: {
        return tr( "O.Top" );
    }
    case OOG_BACK_COL: {
        return tr( "O.Back" );
    }
    case OOG_FRONT_COL: {
        return tr( "O.Front" );
    }
    case OOG_LEFT_COL: {
        return tr( "O.Left" );
    }
    case OOG_RIGHT_COL: {
        return tr( "O.Right" );
    }
    case BUNDLE_COL: {
        return tr("Bundling");
    }
    case NCOLS:
      break;
  }
  return QVariant();
}

QVariant basic_container_list_t::container_edit_data(const ange::containers::Container* container, basic_container_list_t::container_columns_t col) const {
    validateReverseIndex();
  switch ( col ) {
    case EQUIPMENT_NO_COL:
      return container->equipmentNumber();
    case ISOCODE_COL:
      return container->isoCode().code();
    case REEFER_COL: {
      return container->isoCode().reefer();
    }
    case LIVE_COL: {
      return container->live() == Container::Live;
    }
    case WEIGHT_COL:
      return QVariant::fromValue<ange::units::Mass>(container->weight());
    case VGM_COL:
      return container->weightIsVerified();
    case MOVES_COL:
      return QVariant();
    case DG_COL: {
      QVariant rv = ContainerUtils::dangerousGoodsCodesAsParsableString(container->dangerousGoodsCodes());
      return rv;
    }
    case TEMPERATURE_COL:
      return QVariant(); // Not implemented
    case EMPTY_COL:
      return container->empty();
    default:
      break;
  }
  return QVariant();
}

QVariant basic_container_list_t::container_display_data( const Container* container, basic_container_list_t::container_columns_t col ) const {
    validateReverseIndex();
  switch ( col ) {
    case EQUIPMENT_NO_COL:
      return container->equipmentNumber();
    case ISOCODE_COL:
      return container->isoCode().code();
    case REEFER_COL: {
      return container->isoCode().reefer() ? tr("RF") : QVariant();
    }
    case LIVE_COL: {
      return container->live() == Container::Live? tr("L") : QVariant();
    }
    case WEIGHT_COL:
      return QString::number(container->weight()/ton, 'd', 0);
    case VGM_COL:
      return container->weightIsVerified()? "Y" : QVariant();
    case MOVES_COL:
      return QVariant();
    case DG_COL: {
        return ContainerUtils::dangerousGoodsCodesAsParsableString(container->dangerousGoodsCodes());
    }
    case TEMPERATURE_COL: {
        if(container->temperature() != Container::unknownTemperature()) {
            return container->temperature();
        } else {
            return QVariant();
        }
    }
    case CARRIER_COL:
      return container->carrierCode();
    case NOMINAL_POD_COL:
      return container->dischargePort();
    case PLACE_OF_DELIVERY_COL:
      return container->placeOfDelivery();
    case NOMINAL_POL_COL:
      return container->loadPort();
    case EMPTY_COL: {
       if (container->empty()) {
            return tr("E");
       } else {
            return QVariant();
       }
    }
    case BOOKING_NUMBER_COL:
      return container->bookingNumber();
    case HANDLING_CODES_COL: {
      QString rv;
      QList<ange::containers::HandlingCode> handlingCodes = container->handlingCodes();
      if (!handlingCodes.isEmpty()) {
        QList<ange::containers::HandlingCode>::const_iterator it = handlingCodes.constBegin(), end = handlingCodes.constEnd();
        rv += it->code();
        for (++it; it != end; ++it) {
          rv += " ";
          rv += it->code();
        }
      }
      return rv;
    }
    case OOG_BACK_COL: {
        const double back = container->oog().back() / centimeter;
        return back != 0 ? back : QVariant();
    }
    case OOG_TOP_COL: {
        const double top = container->oog().top() / centimeter;
        return top != 0 ? top : QVariant();
    }
    case OOG_FRONT_COL: {
        const double front = container->oog().front() / centimeter;
        return front != 0 ? front : QVariant();
    }
    case OOG_LEFT_COL: {
        const double left = container->oog().left() / centimeter;
        return left != 0 ? left : QVariant();
    }
    case OOG_RIGHT_COL: {
        const double right = container->oog().right() / centimeter;
        return right != 0 ? right : QVariant();
    }
    case BUNDLE_COL:
        if(container->bundleParent()) {
            return "Bundled";
        }
        if(container->bundleChildren().isEmpty()) {
            return "None";
        }
        return "Bottom";
    case LOAD_CALL_COL:
    case PLANNED_COL:
    case LOCATION_COL:
    case NCOLS:
    case DISCHARGE_CALL_COL:
        return QVariant();
  }
  return QVariant();
}

const Container* basic_container_list_t::get_container_by_equipment_number( const QString& equipment_number ) const {
    validateReverseIndex();
  for ( const_iterator it = begin(), theend = end(); it != theend; ++it ) {
    if (( *it )->equipmentNumber() == equipment_number ) {
      return *it;
    }
  }
  return 0L;
}

Container* basic_container_list_t::get_container_by_id( int id ) const {
    validateReverseIndex();
  redo_reverseindex();
    validateReverseIndex();
  QHash<int,int>::const_iterator it = m_reverse_index.find(id);
  if(it!= m_reverse_index.constEnd()) {
    return m_containers.at(it.value());
  }
  return 0L;
}

int basic_container_list_t::find_container(const ange::containers::Container* container) const {
  redo_reverseindex();
    validateReverseIndex();
  return m_reverse_index.value(container->id(), -1);
}

QList<  Container* > basic_container_list_t::get_containers_by_id(QList< int > ids) {
  redo_reverseindex();
  validateReverseIndex();
  QList<Container*> rv;
  Q_FOREACH(int id, ids) {
    rv << m_containers.at(m_reverse_index.value(id));
    Q_ASSERT(rv.last());
  }
  return rv;
}

QList<const container_move_t*> basic_container_list_t::moves_for_container(const ange::containers::Container* container) const {
    Q_UNUSED(container);
    Q_ASSERT(false);
    return QList<const container_move_t*>();
}

QVariant basic_container_list_t::move_data(const container_move_t* move, basic_container_list_t::moves_columns_t column, int role) const {
    Q_UNUSED(move);
    Q_UNUSED(column);
    Q_UNUSED(role);
    Q_ASSERT(false);
    return QVariant();
}

bool basic_container_list_t::remove_container(const ange::containers::Container* container) {
  int index = find_container(container);
  if (index == -1) {
    return false;
  }
  beginRemoveRows(QModelIndex(),index,index);
  const Container* c = m_containers.takeAt(index);
  m_reverseindex_dirty=true;
  delete c;
  endRemoveRows();
  return true;
}

void basic_container_list_t::redo_reverseindex() const {
  if(!m_reverseindex_dirty) {
    validateReverseIndex();
    return;
  }
  m_reverse_index.clear();
  for(int i = 0 ; i < m_containers.size() ; i++) {
    m_reverse_index[m_containers[i]->id()]=i;
  }
    validateReverseIndex();
  m_reverseindex_dirty=false;
}

void basic_container_list_t::add_container( ange::containers::Container* container,int position, bool more) {
    validateReverseIndex();
    Q_ASSERT(container);
  const int insert_position = (position >=0 && position < m_containers.size() + m_backlog.size()) ? position : (m_containers.size()+m_backlog.size());
  if (!m_backlog.empty() && (m_backlog_first_position > insert_position  || m_backlog_first_position + m_backlog.size() < insert_position)) {
    // This add cannot be inserted into the same backlog, so we'll have to flush
    add_containers_from_backlog();
  }
  if (m_backlog.empty()) {
    m_backlog << container;
    m_backlog_first_position = insert_position;
  } else {
    int p = insert_position - m_backlog_first_position;
    Q_ASSERT(p>=0);
    Q_ASSERT(p<=m_backlog.size());
    m_backlog.insert(p, container);
  }
  if (!more) {
    add_containers_from_backlog();
  }
    validateReverseIndex();
}

void basic_container_list_t::add_containers_from_backlog() {
    Q_ASSERT(m_backlog.size() > 0);
  beginInsertRows( QModelIndex(), m_backlog_first_position, m_backlog_first_position + m_backlog.size() - 1);
  int p = m_backlog_first_position;
  const bool update_reverse_index = !m_reverseindex_dirty && m_backlog_first_position == m_containers.size();
  Q_FOREACH(Container* container, m_backlog) {
    container->setParent( this );
    connect(container, &Container::featureChanged, this, &basic_container_list_t::emit_container_feature_changed);
    connect(container, &Container::bundleStatusChanged, this, &basic_container_list_t::bundlesChanged);
    if (update_reverse_index) {
      m_reverse_index.insert(container->id(), p);
    }
    m_containers.insert(p++, container );
  }
  if (!update_reverse_index) {
    m_reverseindex_dirty=true;
  }
  endInsertRows();
  m_backlog.clear();
  validateReverseIndex();
}

void basic_container_list_t::emit_container_feature_changed(Container* container, Container::Feature containerFeature) {
    const int row = find_container(container);
    if (row != -1) {
        Q_FOREACH (basic_container_list_t::container_columns_t column, containerFeatureToColumns(containerFeature)) {
            const QModelIndex idx = index(row, column);
            emit dataChanged(idx, idx);
        }
        emit container_changed(container);
    }
    validateReverseIndex();
}

void basic_container_list_t::bundlesChanged(Container* container) {
    validateReverseIndex();
    const int row = find_container(container);
    if(row!=-1) {
        const QModelIndex idx = index(row,0);
        const QModelIndex idx2 = index(row,columnCount()-1);
        emit dataChanged(idx,idx2);
        emit container_changed(container);
    }
}

basic_container_list_t::Columns basic_container_list_t::containerFeatureToColumns(Container::Feature containerFeature) {
    validateReverseIndex();
    switch (containerFeature) {
        case Container::EquipmentNumber: return Columns() << EQUIPMENT_NO_COL;
        case Container::Weight: return Columns() << WEIGHT_COL;
        case Container::VGM: return Columns() << VGM_COL;
        case Container::IsoCode: return Columns() << ISOCODE_COL;
        case Container::Reefer: return Columns() << REEFER_COL;
        case Container::Height:  return Columns();
        case Container::Length:  return Columns();
        case Container::Oog: return Columns() << OOG_FRONT_COL << OOG_BACK_COL << OOG_RIGHT_COL << OOG_LEFT_COL << OOG_TOP_COL;
        case Container::LiveFeature: return Columns() << LIVE_COL;
        case Container::Temperature: return Columns() << TEMPERATURE_COL;
        case Container::Empty: return Columns() << EMPTY_COL;
        case Container::DangerousGoodsCodes: return Columns() << DG_COL;
        case Container::Carrier: return Columns() << CARRIER_COL;
        case Container::LoadPort: return Columns() << NOMINAL_POL_COL;
        case Container::DischargePort: return Columns() << NOMINAL_POD_COL;
        case Container::PlaceOfDelivery: return Columns() << PLACE_OF_DELIVERY_COL;
        case Container::BookingNumber: return Columns() << BOOKING_NUMBER_COL;
        case Container::HandlingCodes: return Columns() << HANDLING_CODES_COL;
    }
    Q_ASSERT(false);
    return Columns();
}

#include "basic_container_list.moc"
