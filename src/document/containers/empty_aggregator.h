#ifndef EMPTY_AGGREGATOR_H
#define EMPTY_AGGREGATOR_H

#include <qdatacube/abstractaggregator.h>


class EmptyAggregator : public qdatacube::AbstractAggregator {

  public:
    virtual int operator()(int row) const;
    explicit EmptyAggregator(QAbstractItemModel* model);
    virtual int categoryCount() const;
    virtual QVariant categoryHeaderData(int category, int role = Qt::DisplayRole) const;
  private:
    QList<QString> m_category;
};

#endif // EMPTY_AGGREGATOR_H
