#ifndef RECAP_FILTER_DANGEROUS_GOODS_H
#define RECAP_FILTER_DANGEROUS_GOODS_H

#include <qdatacube/abstractaggregator.h>
#include <QStringList>


class DangerousGoodsAggregator : public qdatacube::AbstractAggregator {
    public:
        explicit DangerousGoodsAggregator(QAbstractItemModel* underlying_model);
        virtual int operator()(int row) const;
        virtual int categoryCount() const;
        virtual QVariant categoryHeaderData(int category, int role = Qt::DisplayRole) const;
    private:
        QStringList m_categories;
};

#endif // RECAP_FILTER_DANGEROUS_GOODS_H
