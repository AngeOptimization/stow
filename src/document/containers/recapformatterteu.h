#ifndef RECAP_SUMMARIZER_TEU_H
#define RECAP_SUMMARIZER_TEU_H

#include <qdatacube/abstractformatter.h>

class RecapFormatterTEU : public qdatacube::AbstractFormatter
{
  public:
    RecapFormatterTEU(QAbstractItemModel* underlying_model, qdatacube::DatacubeView* parent = 0,
                      qreal HCequivalenceFactor = 2.0);
    virtual QString format(QList< int > rows) const;
    protected:
        virtual void update(UpdateType element);
    private:
        void recalculateCellSize();
        qreal m_HCequivalenceFactor;

};

#endif // RECAP_SUMMARIZER_TEU_H
