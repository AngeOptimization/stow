#include "abstractschedulebasedaggregator.h"

#include "utils/callutils.h"
#include "basic_container_list.h"
#include <userconfiguration.h>
#include <gui/colortools.h>
#include <ange/schedule/schedule.h>
#include <QColor>

using ange::schedule::Call;
using ange::schedule::Schedule;

AbstractScheduleBasedAggregator::AbstractScheduleBasedAggregator(
    basic_container_list_t* underlyingModel, const Schedule* schedule,
    basic_container_list_t::container_columns_t column, const UserConfiguration* userConfiguration)
    : super(underlyingModel), m_schedule(schedule), m_column(column), m_userConfiguration(userConfiguration)
{
    Q_ASSERT(m_schedule);
    Q_FOREACH (Call* call, schedule->calls()) {
        m_categoriesMap.insert(call, m_categories.size());
        m_categories << three_letter_call_code(call);
    }
    connect(schedule, &Schedule::callAdded, this, &AbstractScheduleBasedAggregator::callAdded);
    connect(schedule, &Schedule::callRemoved, this, &AbstractScheduleBasedAggregator::callRemoved);
    connect(schedule, &Schedule::rotationAboutToBeChanged, this, &AbstractScheduleBasedAggregator::removeMovingCall);
    connect(schedule, &Schedule::rotationChanged, this, &AbstractScheduleBasedAggregator::insertMovingCall);
}

QVariant AbstractScheduleBasedAggregator::categoryHeaderData(int category, int role) const {
    if (category >= categoryCount()) {
        return QVariant();
    }
    if (role == Qt::DisplayRole) {
        return m_categories.at(category);
    }
    if (role == Qt::UserRole) {
        return QVariant::fromValue<Call*>(m_schedule->at(category));
    }
    if (role == FULL_UNCODE) {
        return m_schedule->at(category)->uncode();
    }
    if (role == Qt::ToolTipRole) {
        return m_schedule->at(category)->assembledName();
    }
    if (m_userConfiguration) {
        QColor background = m_userConfiguration->get_color_by_call(m_schedule->at(category));
        if (role == Qt::BackgroundRole) {
            return QVariant::fromValue<QColor>(background);
        }
        if (role == Qt::ForegroundRole) {
            if (backgroundRequiresColorInversion(background)) {
                return QVariant::fromValue<QColor>(Qt::white);
            } else {
                return QVariant::fromValue<QColor>(Qt::black);
            }
        }
    } else {
        if (role == Qt::BackgroundRole) {
            return QVariant::fromValue<QColor>(Qt::white);
        }
        if (role == Qt::ForegroundRole) {
            return QVariant::fromValue<QColor>(Qt::black);
        }
    }
    return QVariant();
}

int AbstractScheduleBasedAggregator::categoryCount() const {
    return m_categories.count();
}

int AbstractScheduleBasedAggregator::operator()(int row) const {
    const Call* call = callForRow(row);
    if (m_categoriesMap.contains(call)) {
        return m_categoriesMap.value(call);
    } else {
        Q_ASSERT(false); // 2014-05-07 Does this still happen?
        // Might perhaps happen when a call is deleted and datacube is rebuild before container is deleted?
        return m_categories.size() - 1; // fall back to return last call
    }
}

const Call* AbstractScheduleBasedAggregator::callForRow(int row) const {
    QModelIndex index = underlyingModel()->index(row, m_column);
    const Call* dest = qobject_cast<Call*>(index.data(Qt::EditRole).value<QObject*>());
    return dest;
}

void AbstractScheduleBasedAggregator::callAdded(const Call* call) {
    const int index = m_schedule->indexOf(call);
    Q_ASSERT(index >= 0);
    m_categories.insert(index, three_letter_call_code((call)));
    m_categoriesMap.insert(call, index);
    for (int i = index + 1; i < m_schedule->size(); ++i) {
        m_categoriesMap.insert(m_schedule->at(i), i);
        Q_ASSERT(m_categories.at(i) == three_letter_call_code(m_schedule->at(i)));
    }
    emit categoryAdded(index);
}

void AbstractScheduleBasedAggregator::callRemoved(const Call* call) {
    const int index = m_categoriesMap.value(call, -1);
    Q_ASSERT(index >= 0);
    m_categoriesMap.remove(call);
    for (CategoriesMap::iterator it = m_categoriesMap.begin(), iend = m_categoriesMap.end(); it != iend; ++it) {
        if (*it > index) {
            --*it;
        }
    }
    m_categories.removeAt(index);
    emit categoryRemoved(index);
}

void AbstractScheduleBasedAggregator::insertMovingCall(int fromPosition, int toPosition) {
    Q_UNUSED(fromPosition);
    callAdded(m_schedule->at(toPosition));
}

void AbstractScheduleBasedAggregator::removeMovingCall(int fromPosition, int toPosition) {
    Q_UNUSED(toPosition);
    callRemoved(m_schedule->at(fromPosition));
}

    AbstractScheduleBasedAggregator::~AbstractScheduleBasedAggregator() {

}


#include "abstractschedulebasedaggregator.moc"
