#ifndef ISO_CODE_AGGREGATOR_H
#define ISO_CODE_AGGREGATOR_H

#include <qdatacube/columnaggregator.h>
namespace ange {
namespace containers {
class Container;
}
}

/**
 * An aggregator for container ISO codes
 */
class basic_container_list_t;
class IsoCodeAggregator : public qdatacube::ColumnAggregator {
    Q_OBJECT
public:
    IsoCodeAggregator(basic_container_list_t* model);
    virtual QVariant categoryHeaderData(int category, int role = Qt::DisplayRole) const;
private Q_SLOTS:
    void updateCategories(const ange::containers::Container*  container);   
};
#endif // ISO_CODE_AGGREGATOR_H
