#include "isocodeaggregator.h"
#include "basic_container_list.h"
#include <ange/containers/container.h>
#include <QSet>

using ange::containers::Container;

IsoCodeAggregator::IsoCodeAggregator(basic_container_list_t* model)
  : ColumnAggregator(model, basic_container_list_t::ISOCODE_COL)
{
    setName(QString::fromLocal8Bit("iso code"));
    connect(model, &basic_container_list_t::container_changed, this, &IsoCodeAggregator::updateCategories);
}

QVariant IsoCodeAggregator::categoryHeaderData(int category, int role) const {
    QVariant result = qdatacube::ColumnAggregator::categoryHeaderData(category, role);
    if(role == Qt::DisplayRole) {
        if(result.toString().isEmpty()) {
            result = "-";
        }
    }
    return result;
}

void IsoCodeAggregator::updateCategories(const ange::containers::Container* container) {
    for(int i = 0; i < categoryCount(); ++i) {
        if(container->isoCode().code() == categoryHeaderData(i)) {
            return;
        }
    }
    resetCategories();//slightly large operation that should be hit very rarely
}

#include "isocodeaggregator.moc"
