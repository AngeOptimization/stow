#include "containerbundlefilter.h"
#include <ange/containers/container.h>

ContainerBundleFilter::ContainerBundleFilter(QObject* parent) : QSortFilterProxyModel(parent) {
}

bool ContainerBundleFilter::filterAcceptsRow(int source_row, const QModelIndex& source_parent) const {
    QModelIndex idx = sourceModel()->index(source_row,0,source_parent);
    ange::containers::Container* container = idx.data(Qt::UserRole).value<ange::containers::Container*>();
    if(!container) {
        qWarning("something not a container returned from container list");
        return true;
    }
    return !container->bundleChildren().empty();
}

ContainerBundleList::ContainerBundleList(QObject* parent): QSortFilterProxyModel(parent) {

}

bool ContainerBundleList::filterAcceptsRow(int source_row, const QModelIndex& source_parent) const {
    QModelIndex idx = sourceModel()->index(source_row,0,source_parent);
    ange::containers::Container* container = idx.data(Qt::UserRole).value<ange::containers::Container*>();
    if(!container) {
        qWarning("something not a container returned from container list");
        return false;
    }
    return container->bundleChildren().empty();
}

BundledContainerList::BundledContainerList(QObject* parent) : QSortFilterProxyModel(parent) {

}

bool BundledContainerList::filterAcceptsRow(int source_row, const QModelIndex& source_parent) const {
    QModelIndex idx = sourceModel()->index(source_row,0,source_parent);
    ange::containers::Container* container = idx.data(Qt::UserRole).value<ange::containers::Container*>();
    if(!container) {
        qWarning("something not a container returned from container list");
        return false;
    }
    return container->bundleParent();
}





#include "containerbundlefilter.moc"
