#ifndef RECAP_SUMMARIZER_WEIGHT_H
#define RECAP_SUMMARIZER_WEIGHT_H

#include <qdatacube/abstractformatter.h>

class RecapFormatterWeight : public qdatacube::AbstractFormatter
{
    public:
        RecapFormatterWeight(QAbstractItemModel* underlying_model, qdatacube::DatacubeView* parent = 0);
        virtual QString format(QList< int > rows) const;
    protected:
        virtual void update(UpdateType element);
    private:
        void recalculateCellSize();

};

#endif // RECAP_SUMMARIZER_WEIGHT_H
