#ifndef RECAPFILTERBUNDLEDCONTAINERS_H
#define RECAPFILTERBUNDLEDCONTAINERS_H

#include <qdatacube/abstractfilter.h>

class basic_container_list_t;
class BundledContainerList;
class RecapFilterBundledContainers : public qdatacube::AbstractFilter {
    Q_OBJECT
    public:
        explicit RecapFilterBundledContainers(basic_container_list_t* underlying_model, BundledContainerList* bundledContainerList);
        virtual bool operator()(int row) const;
    private:
        BundledContainerList* m_bundledContainerList;

};

#endif // RECAPFILTERBUNDLEDCONTAINERS_H
