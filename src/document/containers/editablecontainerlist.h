#ifndef EDITABLECONTAINERLIST_H
#define EDITABLECONTAINERLIST_H

#include "changecontainercommand.h"
#include "container_list.h"

class document_t;

/**
 * @brief adds the needed bits on top of container_list_t to edit the containers
 */
class EditableContainerList : public container_list_t {
    typedef container_list_t super;
    Q_OBJECT

public:

    EditableContainerList(document_t* document, QObject* parent = 0);

    /**
     * @override
     * Sets the data to value for index & role. Note that it is the views responsibility of sanity checking.
     */
    virtual bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole);
    virtual Qt::ItemFlags flags(const QModelIndex& index) const;

private:

    void changeFeature(const ange::containers::Container* container, ChangeContainerCommand::Feature feature,
                       const QVariant data);

private:
    document_t* m_document;

};

#endif // EDITABLECONTAINERLIST_H
