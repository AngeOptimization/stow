
#include "dischargecallaggregator.h"

#include "basic_container_list.h"
#include <ange/schedule/call.h>
#include <QModelIndex>

using ange::schedule::Call;
using ange::schedule::Schedule;

DischargeCallAggregator::DischargeCallAggregator(basic_container_list_t* underlyingModel, const Schedule* schedule,
                                                 UserConfiguration* userConfiguration)
    : super(underlyingModel, schedule, basic_container_list_t::DISCHARGE_CALL_COL, userConfiguration)
{
    setName(QString::fromLocal8Bit("discharge call"));
}

#include "dischargecallaggregator.moc"
