/*
 *
 */

#include "recapfilterbundledcontainers.h"
#include "basic_container_list.h"
#include <ange/containers/container.h>

RecapFilterBundledContainers::RecapFilterBundledContainers(basic_container_list_t* underlying_model, BundledContainerList* bundledContainerList): AbstractFilter(underlying_model), m_bundledContainerList(bundledContainerList) {
    setShortName("Bundled");
    setName("Filter bundled");

}

bool RecapFilterBundledContainers::operator()(int row) const {
    const ange::containers::Container* container = static_cast<const basic_container_list_t*>(underlyingModel())->at(row);
    return container->bundleParent() == 0;
}

#include "recapfilterbundledcontainers.moc"
