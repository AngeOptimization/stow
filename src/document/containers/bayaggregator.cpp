#include "bayaggregator.h"
#include "container_list.h"
#include <ange/schedule/call.h>
#include <ange/vessel/slot.h>
#include <ange/containers/container.h>

using ange::schedule::Call;
using ange::vessel::Slot;
using ange::vessel::BayRowTier;
using ange::containers::Container;

int BayAggregator::CAT_NA = 0;
int BayAggregator::MAX_BAYS = 120;

BayAggregator::BayAggregator(basic_container_list_t* underlying_model)
  : AbstractAggregator(underlying_model),
    m_consideredCall(0L)
{
    setName(QString::fromLocal8Bit("bay number"));
}

int BayAggregator::operator()(int row) const {
    if(!m_consideredCall) {
        return CAT_NA;
    }
    // If before source or after destination, return N/A (=1)
    const Call* source = qobject_cast<const Call*>(underlyingModel()->index(row, container_list_t::LOAD_CALL_COL).data(Qt::EditRole).value<QObject*>());
    if (!source || source->distance(m_consideredCall)<0) {
        return CAT_NA;
    }
    const Call* destination = qobject_cast<const Call*>(underlyingModel()->index(row, container_list_t::DISCHARGE_CALL_COL).data(Qt::EditRole).value<QObject*>());
    if (!destination || destination->distance(m_consideredCall)>0) {
        return CAT_NA;
    }
    QModelIndex moves_index(underlyingModel()->index(row, container_list_t::MOVES_COL));
    int nmoves = underlyingModel()->rowCount(moves_index);
    if (nmoves == 0) {
        // If no moves, it is N/A for any call but the source port, where it is on the yard
        return CAT_NA;
    }
    const bool finalDischargeHere = destination->distance(m_consideredCall)==0;
    int lastPositionOnVessel = CAT_NA;
    for (int move_index = 0; move_index < nmoves; ++move_index) {
        const Call* call = qobject_cast<Call*>(underlyingModel()->index(move_index,
                                                  container_list_t::MOVES_CALL_COL,
                                                  moves_index).data(Qt::EditRole).value<QObject*>());
        if (call->distance(m_consideredCall) < 0) {
            // We have moved past m_consideredCall.
            break;
        }
        const Slot* slot = qobject_cast<Slot*>(underlyingModel()->index(move_index,
                                                                    container_list_t::MOVES_SLOT_COL,
                                                                    moves_index).data(Qt::EditRole).value<QObject*>());
        if (call == m_consideredCall) {
            if(finalDischargeHere) {
                Q_ASSERT(lastPositionOnVessel != CAT_NA);
                return lastPositionOnVessel;
            }
            if (slot) {
                const BayRowTier position = underlyingModel()->index(move_index,
                                                                    container_list_t::MOVES_POSITION_COL,
                                                                    moves_index).data(Qt::EditRole).value<BayRowTier>();
                return position.bay();
            }
        }
        if(slot) {
            lastPositionOnVessel = slot->brt().bay();
        }
    }
    return lastPositionOnVessel;
}

int BayAggregator::categoryCount() const {
    return MAX_BAYS;
}

QVariant BayAggregator::categoryHeaderData(int category, int role) const {
    if(role == Qt::DisplayRole) {
        if(category == CAT_NA) {
            return "N/A";
        } else {
            return QString("%1").arg(category);
        }
    }
    return QVariant();
}

void BayAggregator::set_current_call(const ange::schedule::Call* call) {
  m_consideredCall = call;
}

#include "bayaggregator.moc"
