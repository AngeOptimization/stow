#ifndef CONTAINERBUNDLEFILTER_H
#define CONTAINERBUNDLEFILTER_H

#include <QSortFilterProxyModel>

/**
 * \brief Container list without bundles
 */

class ContainerBundleFilter : public QSortFilterProxyModel {
    Q_OBJECT

    public:
        ContainerBundleFilter(QObject* parent = 0);

    protected:
        virtual bool filterAcceptsRow(int source_row, const QModelIndex& source_parent) const;
};

/**
 * \brief bundle list
 */

class ContainerBundleList : public QSortFilterProxyModel {
    Q_OBJECT
    public:
        ContainerBundleList(QObject* parent = 0);
    protected:
        virtual bool filterAcceptsRow(int source_row, const QModelIndex& source_parent) const;
};

/**
 * \brief list of bundled containers
 */

class BundledContainerList : public QSortFilterProxyModel {
    Q_OBJECT
    public:
        BundledContainerList(QObject* parent = 0);
    protected:
        virtual bool filterAcceptsRow(int source_row, const QModelIndex& source_parent) const;
};

#endif // CONTAINERBUNDLEFILTER_H
