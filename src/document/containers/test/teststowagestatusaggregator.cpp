
#include <QTest>
#include "test/util/vesseltestutils.h"
#include <stowagestatusaggregator.h>

using ange::containers::Container;
using ange::containers::EquipmentNumber;
using ange::containers::IsoCode;
using ange::schedule::Schedule;
using ange::schedule::Call;
using ange::vessel::BayRowTier;
using ange::vessel::Slot;
using ange::vessel::Vessel;

class TestStowageStatusAggregator : public QObject {
    Q_OBJECT
private Q_SLOTS:
    void testSimpleLoadDischarge();
    void testComplexShifts();
};

static Vessel* loadVessel() {
    // Bay 1,2,3
    // Row 0
    // Tier 80, 82, 84
    Vessel* vessel = loadVessel(QFINDTESTDATA("test-data/single-stack.json"));
    return vessel;
}

void TestStowageStatusAggregator::testSimpleLoadDischarge() {
    // tests a container from call1 to call 3
    Schedule* schedule = buildSchedule(5);
    Vessel* vessel = loadVessel();
    stowage_t* stowage = new stowage_t(schedule);
    document_t* document = new document_t(schedule, vessel, stowage);

    const Slot* slot = document->vessel()->slotAt(BayRowTier(2, 0, 80));
    createContainerOnboard(document, schedule->at(1), schedule->at(3), slot);

    StowageStatusAggregator aggregator(document->containers());

    // Before
    aggregator.setCurrentCall(schedule->at(0));
    QCOMPARE(aggregator(0), (int) StowageStatusAggregator::CAT_NA);

    // Load
    aggregator.setCurrentCall(schedule->at(1));
    QCOMPARE(aggregator(0), (int) StowageStatusAggregator::CAT_LOADED);

    // Between
    aggregator.setCurrentCall(schedule->at(2));
    QCOMPARE(aggregator(0), (int) StowageStatusAggregator::CAT_ONBOARD);

    // Discharge
    aggregator.setCurrentCall(schedule->at(3));
    QCOMPARE(aggregator(0), (int) StowageStatusAggregator::CAT_DISCHARGED);

    // After
    aggregator.setCurrentCall(schedule->at(4));
    QCOMPARE(aggregator(0), (int) StowageStatusAggregator::CAT_NA);

    delete document;
}

void TestStowageStatusAggregator::testComplexShifts() {
    // tests a container from call0 to call5
    //  - is unplaced in call 0
    //  - placed in a slot in call1
    //  - shifted to a different slot in call2
    //  - and unstowed in call3.
    Schedule* schedule = buildSchedule(6);
    Vessel* vessel = loadVessel();
    stowage_t* stowage = new stowage_t(schedule);
    document_t* document = new document_t(schedule, vessel, stowage);

    Container* newContainer = new Container(EquipmentNumber("EQNO"), 10 * ton, IsoCode("22G1"));
    container_list_change_command_t* listChangeCommand = new container_list_change_command_t(document);
    listChangeCommand->add_container(newContainer, schedule->at(0), schedule->at(5));
    document->undostack()->push(listChangeCommand);
    Container* container = document->containers()->get_container_by_id(newContainer->id());
    delete newContainer;

    const Slot* slot1 = document->vessel()->slotAt(BayRowTier(1, 0, 82));
    container_stow_command_t* stowCommand1 = new container_stow_command_t(document);
    stowCommand1->add_stow(container, slot1, schedule->at(1), schedule->at(2), ange::angelstow::MicroStowedType);
    document->undostack()->push(stowCommand1);

    const Slot* slot2 = document->vessel()->slotAt(BayRowTier(3, 0, 82));
    container_stow_command_t* stowCommand2 = new container_stow_command_t(document);
    stowCommand2->add_stow(container, slot2, schedule->at(2), schedule->at(3), ange::angelstow::MicroStowedType);
    document->undostack()->push(stowCommand2);

    StowageStatusAggregator aggregator(document->containers());

    // Load port where containers is not moved
    aggregator.setCurrentCall(schedule->at(0));
    QCOMPARE(aggregator(0), (int) StowageStatusAggregator::CAT_UNPLANNED);

    // Container is loaded where it is not available (is not possible to create in GUI)
    aggregator.setCurrentCall(schedule->at(1));
    QCOMPARE(aggregator(0), (int) StowageStatusAggregator::CAT_SHIFTED);

    // Normal shift
    aggregator.setCurrentCall(schedule->at(2));
    QCOMPARE(aggregator(0), (int) StowageStatusAggregator::CAT_SHIFTED);

    // Discharge before destination call
    aggregator.setCurrentCall(schedule->at(3));
    QCOMPARE(aggregator(0), (int) StowageStatusAggregator::CAT_UNPLANNED);

    // Not loaded in between call
    aggregator.setCurrentCall(schedule->at(4));
    QCOMPARE(aggregator(0), (int) StowageStatusAggregator::CAT_ROLL);

    // Not loaded in destination call
    aggregator.setCurrentCall(schedule->at(5));
    QCOMPARE(aggregator(0), (int) StowageStatusAggregator::CAT_ROLL);

    delete document;
}

QTEST_GUILESS_MAIN(TestStowageStatusAggregator);

#include "teststowagestatusaggregator.moc"
