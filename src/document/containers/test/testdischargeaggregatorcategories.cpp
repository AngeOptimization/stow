#include <QTest>
#include <qsignalspy.h>
#include <QObject>
#include <ange/schedule/schedule.h>
#include <ange/schedule/call.h>
#include <dischargecallaggregator.h>
#include <basic_container_list.h>

using ange::schedule::Schedule;
using ange::schedule::Call;

class TestDischargeAggregatorCategories : public QObject {
    Q_OBJECT
    private Q_SLOTS:
        void testSimpleSetup();
        void testAddCall();
        void testRemoveCall();
        void testMoveCallDown();
        void testMoveCallUp();
};

static Schedule* buildSchedule() {
    Call* callA = new Call("A", "");
    Call* callB = new Call("B", "");
    Call* callC = new Call("C", "");
    Call* callD = new Call("D", "");
    Call* callE = new Call("E", "");
    Call* callF = new Call("F", "");
    Call* callG = new Call("G", "");
    Schedule* schedule = new Schedule(QList<Call*>() << callA << callB << callC << callD << callE << callF << callG);
    schedule->setPivotCall(schedule->size()-1); // needed for running schedulemodel with modeltest
    return schedule;
}

static basic_container_list_t* buildEmptyContainerList() {
    return new basic_container_list_t();
}

void TestDischargeAggregatorCategories::testSimpleSetup() {
    Schedule* schedule = buildSchedule();
    basic_container_list_t* containers = buildEmptyContainerList();
    DischargeCallAggregator aggregator(containers, schedule);

    QCOMPARE(QStringLiteral("A"),aggregator.categoryHeaderData(0).toString());
    QCOMPARE(QStringLiteral("B"),aggregator.categoryHeaderData(1).toString());
    QCOMPARE(QStringLiteral("C"),aggregator.categoryHeaderData(2).toString());
    QCOMPARE(QStringLiteral("D"),aggregator.categoryHeaderData(3).toString());
    QCOMPARE(QStringLiteral("E"),aggregator.categoryHeaderData(4).toString());
    QCOMPARE(QStringLiteral("F"),aggregator.categoryHeaderData(5).toString());
    QCOMPARE(QStringLiteral("G"),aggregator.categoryHeaderData(6).toString());
    delete containers;
    delete schedule;
}


void TestDischargeAggregatorCategories::testAddCall() {
    Schedule* schedule = buildSchedule();
    basic_container_list_t* containers = buildEmptyContainerList();
    DischargeCallAggregator aggregator(containers, schedule);

    QSignalSpy addedSpy(&aggregator, SIGNAL(categoryAdded(int)));
    QVERIFY(addedSpy.isValid());
    QSignalSpy removedSpy(&aggregator, SIGNAL(categoryRemoved(int)));
    QVERIFY(removedSpy.isValid());

    schedule->insert(3, new Call("NEW",""));

    QCOMPARE(QStringLiteral("A"),aggregator.categoryHeaderData(0).toString());
    QCOMPARE(QStringLiteral("B"),aggregator.categoryHeaderData(1).toString());
    QCOMPARE(QStringLiteral("C"),aggregator.categoryHeaderData(2).toString());
    QCOMPARE(QStringLiteral("NEW"),aggregator.categoryHeaderData(3).toString());
    QCOMPARE(QStringLiteral("D"),aggregator.categoryHeaderData(4).toString());
    QCOMPARE(QStringLiteral("E"),aggregator.categoryHeaderData(5).toString());
    QCOMPARE(QStringLiteral("F"),aggregator.categoryHeaderData(6).toString());
    QCOMPARE(QStringLiteral("G"),aggregator.categoryHeaderData(7).toString());

    QCOMPARE(1,addedSpy.count());
    QCOMPARE(3,addedSpy.at(0).at(0).toInt());

    QCOMPARE(0,removedSpy.count());
    delete containers;
    delete schedule;
}

void TestDischargeAggregatorCategories::testRemoveCall() {
    Schedule* schedule = buildSchedule();
    basic_container_list_t* containers = buildEmptyContainerList();
    DischargeCallAggregator aggregator(containers, schedule);

    QSignalSpy addedSpy(&aggregator, SIGNAL(categoryAdded(int)));
    QVERIFY(addedSpy.isValid());
    QSignalSpy removedSpy(&aggregator, SIGNAL(categoryRemoved(int)));
    QVERIFY(removedSpy.isValid());

    schedule->removeAt(3);

    QCOMPARE(QStringLiteral("A"),aggregator.categoryHeaderData(0).toString());
    QCOMPARE(QStringLiteral("B"),aggregator.categoryHeaderData(1).toString());
    QCOMPARE(QStringLiteral("C"),aggregator.categoryHeaderData(2).toString());
    QCOMPARE(QStringLiteral("E"),aggregator.categoryHeaderData(3).toString());
    QCOMPARE(QStringLiteral("F"),aggregator.categoryHeaderData(4).toString());
    QCOMPARE(QStringLiteral("G"),aggregator.categoryHeaderData(5).toString());

    QCOMPARE(0,addedSpy.count());

    QCOMPARE(1,removedSpy.count());
    QCOMPARE(3,removedSpy.at(0).at(0).toInt());
    delete containers;
    delete schedule;
}

void TestDischargeAggregatorCategories::testMoveCallDown() {
    Schedule* schedule = buildSchedule();
    basic_container_list_t* containers = buildEmptyContainerList();
    DischargeCallAggregator aggregator(containers, schedule);

    QSignalSpy addedSpy(&aggregator, SIGNAL(categoryAdded(int)));
    QVERIFY(addedSpy.isValid());
    QSignalSpy removedSpy(&aggregator, SIGNAL(categoryRemoved(int)));
    QVERIFY(removedSpy.isValid());

    schedule->changeOfRotation(3,4);
    QCOMPARE(QStringLiteral("A"),aggregator.categoryHeaderData(0).toString());
    QCOMPARE(QStringLiteral("B"),aggregator.categoryHeaderData(1).toString());
    QCOMPARE(QStringLiteral("C"),aggregator.categoryHeaderData(2).toString());
    QCOMPARE(QStringLiteral("E"),aggregator.categoryHeaderData(3).toString());
    QCOMPARE(QStringLiteral("D"),aggregator.categoryHeaderData(4).toString());
    QCOMPARE(QStringLiteral("F"),aggregator.categoryHeaderData(5).toString());
    QCOMPARE(QStringLiteral("G"),aggregator.categoryHeaderData(6).toString());


    QCOMPARE(1,addedSpy.count());
    QCOMPARE(4,addedSpy.at(0).at(0).toInt());

    QCOMPARE(1,removedSpy.count());
    QCOMPARE(3,removedSpy.at(0).at(0).toInt());

    delete containers;
    delete schedule;
}

void TestDischargeAggregatorCategories::testMoveCallUp() {
    Schedule* schedule = buildSchedule();
    basic_container_list_t* containers = buildEmptyContainerList();
    DischargeCallAggregator aggregator(containers, schedule);

    QSignalSpy addedSpy(&aggregator, SIGNAL(categoryAdded(int)));
    QVERIFY(addedSpy.isValid());
    QSignalSpy removedSpy(&aggregator, SIGNAL(categoryRemoved(int)));
    QVERIFY(removedSpy.isValid());

    schedule->changeOfRotation(3,2);
    QCOMPARE(QStringLiteral("A"),aggregator.categoryHeaderData(0).toString());
    QCOMPARE(QStringLiteral("B"),aggregator.categoryHeaderData(1).toString());
    QCOMPARE(QStringLiteral("D"),aggregator.categoryHeaderData(2).toString());
    QCOMPARE(QStringLiteral("C"),aggregator.categoryHeaderData(3).toString());
    QCOMPARE(QStringLiteral("E"),aggregator.categoryHeaderData(4).toString());
    QCOMPARE(QStringLiteral("F"),aggregator.categoryHeaderData(5).toString());
    QCOMPARE(QStringLiteral("G"),aggregator.categoryHeaderData(6).toString());


    QCOMPARE(1,addedSpy.count());
    QCOMPARE(2,addedSpy.at(0).at(0).toInt());

    QCOMPARE(1,removedSpy.count());
    QCOMPARE(3,removedSpy.at(0).at(0).toInt());

    delete containers;
    delete schedule;
}



QTEST_GUILESS_MAIN(TestDischargeAggregatorCategories);

#include "testdischargeaggregatorcategories.moc"
