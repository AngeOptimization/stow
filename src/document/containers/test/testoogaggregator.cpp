#include <QTest>
#include "test/util/vesseltestutils.h"
#include <oogaggregator.h>
#include <ange/containers/oog.h>

using ange::containers::Container;
using ange::containers::EquipmentNumber;
using ange::containers::IsoCode;
using ange::schedule::Schedule;
using ange::schedule::Call;
using ange::vessel::BayRowTier;
using ange::vessel::Slot;
using ange::vessel::Vessel;

class TestOogAggregator : public QObject {
    Q_OBJECT
private Q_SLOTS:
    void testOogCategories();
};

static Vessel* loadVessel() {
    // Bay 1,2,3
    // Row 0
    // Tier 80, 82, 84
    Vessel* vessel = loadVessel(QFINDTESTDATA("test-data/single-stack.json"));
    return vessel;
}

void TestOogAggregator::testOogCategories() {
    Schedule* schedule = buildSchedule(5);
    Vessel* vessel = loadVessel();
    stowage_t* stowage = new stowage_t(schedule);
    document_t* document = new document_t(schedule, vessel, stowage);

    const Slot* slot1 = document->vessel()->slotAt(BayRowTier(2, 0, 80));
    const Slot* slot2 = document->vessel()->slotAt(BayRowTier(2, 0, 82));
    createContainerOnboard(document, schedule->at(1), schedule->at(3), slot1);
    createContainerOnboard(document, schedule->at(1), schedule->at(3), slot2);

    ange::containers::Oog oog;
    oog.setTop(0.1 * meter);
    document->containers()->list().back()->setOog(oog);
    OogAggregator aggregator(document->containers());

    QCOMPARE(aggregator(0), (int) OogAggregator::CAT_NOR);
    QCOMPARE(aggregator(1), (int) OogAggregator::CAT_OOG);

    delete document;
}

QTEST_GUILESS_MAIN(TestOogAggregator);

#include "testoogaggregator.moc"
