#ifndef RECAPFILTERCONTAINERID_H
#define RECAPFILTERCONTAINERID_H

#include <qdatacube/abstractfilter.h>
#include <QSet>

/**
 * \brief filter to pick specific containers from the list
 *
 * This filter accepts a specific list of container ids.
 */

class basic_container_list_t;
class RecapFilterContainerId : public qdatacube::AbstractFilter {
    Q_OBJECT
    public:
        virtual bool operator()(int row) const;
        RecapFilterContainerId(QSet< int > container_ids, basic_container_list_t* underlying_model);

    private:
        QSet<int> m_container_ids;
};

#endif // RECAPFILTERCONTAINERID_H
