#include "stowagestatusaggregator.h"

#include "container_list.h"

#include <stowplugininterface/stowtype.h>
#include <ange/containers/container.h>
#include <ange/schedule/call.h>
#include <ange/vessel/slot.h>

using ange::schedule::Call;
using ange::vessel::Slot;
using ange::containers::Container;

StowageStatusAggregator::StowageStatusAggregator(basic_container_list_t* underlyingModel)
  : super(underlyingModel), m_currentCall(0L)
{
    m_categories << "UNP" << "LOA" << "ROB" << "SHF" << "DSC" << "ROL" << "N/A" << "???";
    setName(QString::fromLocal8Bit("stowage status"));
}

int StowageStatusAggregator::operator()(int row) const {
    if (!m_currentCall) {
        // This happens when we clone the aggregator while loading a stowage using the GUI
        return CAT_TEMPORARY_BROKEN;
    }

    const Container* container = underlyingModel()->index(row, 0).data(Qt::UserRole).value<Container*>();
    if (container->bundleParent()) {
        return CAT_NA; // NA = bundled
    }

    // If before source return N/A
    QModelIndex sourceIndex = underlyingModel()->index(row, container_list_t::LOAD_CALL_COL);
    const Call* source = qobject_cast<const Call*>(sourceIndex.data(Qt::EditRole).value<QObject*>());
    if (!source) {
        Q_ASSERT(false); // 2014-05-05 when can this happen?
        return CAT_NA; // NA = ???
    }
    if (source->distance(m_currentCall) < 0) {
        return CAT_NA; // NA = out of history
    }

    // If after destination return N/A
    QModelIndex destinationIndex = underlyingModel()->index(row, container_list_t::DISCHARGE_CALL_COL);
    const Call* destination = qobject_cast<const Call*>(destinationIndex.data(Qt::EditRole).value<QObject*>());
    if (!destination) {
        Q_ASSERT(false); // 2014-05-05 when can this happen?
        return CAT_NA; // NA = ???
    }
    if (destination->distance(m_currentCall) > 0) {
        return CAT_NA; // NA = out of history
    }

    /*-
     * Possible combinations
     *               |   Move in currentCall is:  |
     * currentCall   | load | none    | discharge |
     * --------------+------+---------+-----------+
     * source        | LOA  | UNP     | -         |
     * between       | SHF  | ROB/ROL | UNP       |
     * destination   | -    | ROL     | DSC       |
     *
     * ROB/ROL is determined by onboard status
     */
    QModelIndex movesIndex(underlyingModel()->index(row, container_list_t::MOVES_COL));
    int movesRowCount = underlyingModel()->rowCount(movesIndex);
    if (movesRowCount == 0) { // Are in 'none' column of combinations table
        if (m_currentCall == source) {
            return CAT_UNPLANNED;
        } else {
            return CAT_ROLL; // Can not be onboard as there are no moves
        }
    }

    if (m_currentCall == source) { // Are in 'source' row of combinations table
        QModelIndex firstCallIndex = underlyingModel()->index(0, container_list_t::MOVES_CALL_COL, movesIndex);
        const Call* firstCall = qobject_cast<const Call*>(firstCallIndex.data(Qt::EditRole).value<QObject*>());
        Q_ASSERT(firstCall);
        if (m_currentCall == firstCall) { // Are in 'load' column of combinations table
            return CAT_LOADED;
        } else {
            return CAT_UNPLANNED;
        }
    }

    if (m_currentCall == destination) { // Are in 'destination' row of combinations table
        QModelIndex lastCallIndex = underlyingModel()->index(movesRowCount-1, container_list_t::MOVES_CALL_COL, movesIndex);
        const Call* lastCall = qobject_cast<const Call*>(lastCallIndex.data(Qt::EditRole).value<QObject*>());
        Q_ASSERT(lastCall);
        if (m_currentCall == lastCall) { // Are in 'discharge' column of combinations table
            return CAT_DISCHARGED;
        } else {
            return CAT_ROLL;
        }
    }

    // Are in 'between' row of combinations table
    bool onboard = false;
    for (int movesRow = 0; movesRow < movesRowCount; ++movesRow) {
        QModelIndex callIndex = underlyingModel()->index(movesRow, container_list_t::MOVES_CALL_COL, movesIndex);
        const Call* call = qobject_cast<Call*>(callIndex.data(Qt::EditRole).value<QObject*>());
        Q_ASSERT(call);
        if (call->distance(m_currentCall) < 0) {
            // We have moved past m_currentCall
            break;
        }
        QModelIndex slotIndex = underlyingModel()->index(movesRow, container_list_t::MOVES_SLOT_COL, movesIndex);
        const Slot* slot = qobject_cast<Slot*>(slotIndex.data(Qt::EditRole).value<QObject*>());
        if (m_currentCall == call) { // Have found move for currentCall
            if (slot) { // Are in 'load' column of combinations table
                return CAT_SHIFTED;
            } else { // Are in 'discharge' column of combinations table
                return CAT_UNPLANNED;
            }
        } else {
            onboard = (slot != 0L);
        }
     }
    // Are in 'none' column of combinations table
    return onboard ? CAT_ONBOARD : CAT_ROLL;
}

int StowageStatusAggregator::categoryCount() const {
    return m_categories.count();
}

QVariant StowageStatusAggregator::categoryHeaderData(int category, int role) const {
    if (role == Qt::DisplayRole) {
        return m_categories.at(category);
    }
    return QVariant();
}

void StowageStatusAggregator::setCurrentCall(const ange::schedule::Call* call) {
    m_currentCall = call;
}

#include "stowagestatusaggregator.moc"
