#ifndef DISCHARGE_CALL_AGGREGATOR_H
#define DISCHARGE_CALL_AGGREGATOR_H

#include "abstractschedulebasedaggregator.h"

class UserConfiguration;
class basic_container_list_t;
namespace ange {
namespace schedule {
class Schedule;
class Call;
}
}

/**
 * An aggregator on call of discharge
 */
class DischargeCallAggregator : public AbstractScheduleBasedAggregator {
    typedef AbstractScheduleBasedAggregator super;
    Q_OBJECT
public:

    /**
     * Constructor
     * @param userConfiguration is used for coloring, if it is null no colors are used
     */
    DischargeCallAggregator(basic_container_list_t* underlyingModel, const ange::schedule::Schedule* schedule,
                            UserConfiguration* userConfiguration = 0);

};

#endif // DISCHARGE_CALL_AGGREGATOR_H
