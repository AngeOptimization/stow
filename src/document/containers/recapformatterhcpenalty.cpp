#include "recapformatterhcpenalty.h"
#include <QAbstractItemModel>
#include <QEvent>
#include "basic_container_list.h"
#include <qdatacube/datacubeview.h>
#include <ange/units/units.h>
#include <ange/containers/isocode.h>
#include "hcequivalenceconstants.h"

QString RecapFormatterHCPenalty::format(QList< int > rows) const {
    return QString::number(HCpenalty(rows), 'f', 0);
}

RecapFormatterHCPenalty::RecapFormatterHCPenalty(QAbstractItemModel* underlying_model,
                                                   qdatacube::DatacubeView* view)
 : AbstractFormatter(underlying_model, view)
{
    update(qdatacube::AbstractFormatter::CellSize);
    setShortName(tr("HCO"));
    setName("hc penalty");
}


void RecapFormatterHCPenalty::update(qdatacube::AbstractFormatter::UpdateType element) {
    if(element == qdatacube::AbstractFormatter::CellSize) {
        recalculateCellSize();
    }
}

double RecapFormatterHCPenalty::HCpenalty(const QList< int > rows) const {
    // Set the cell size, by summing up all the data in the model, and using that as input
    double accumulator = 0;
    Q_FOREACH(const int row, rows) {
        QModelIndex index(underlyingModel()->index(row, basic_container_list_t::ISOCODE_COL));
        const ange::containers::IsoCode isocode(index.data().toString());
        double penalty  = 0;
        if (isocode.height() == ange::containers::IsoCode::HC) {
            if (isocode.length() == ange::containers::IsoCode::Fourty) {
                penalty = hc40teuEquivalence - 2.0;
            } else if (isocode.length() == ange::containers::IsoCode::FourtyFive) {
                penalty = hc45teuEquivalence - 2.0;
            }
        }
        accumulator += penalty;
    }
    return accumulator;
}


void RecapFormatterHCPenalty::recalculateCellSize() {
    if(!datacubeView()) {
        return;
    }
    QList <int> allIndices;
    for(int i = 0; i < underlyingModel()->rowCount(); ++i) {
        allIndices << i;
    }
    QString big_cell_contents = QString::number(HCpenalty(allIndices), 'f', 0) + "t";
    setCellSize(QSize(datacubeView()->fontMetrics().width(big_cell_contents), datacubeView()->fontMetrics().lineSpacing()));
}
