
#ifndef LOAD_CALL_AGGREGATOR
#define LOAD_CALL_AGGREGATOR

#include "abstractschedulebasedaggregator.h"

class UserConfiguration;
class basic_container_list_t;
namespace ange {
namespace schedule {
class Schedule;
class Call;
}
}

/**
 * An aggregator on load call
 */
class LoadCallAggregator : public AbstractScheduleBasedAggregator {
    typedef AbstractScheduleBasedAggregator super;

    Q_OBJECT

public:

    /**
     * Constructor
     * @param userConfiguration is used for coloring, if it is null no colors are used
     */
    LoadCallAggregator(basic_container_list_t* underlyingModel, const ange::schedule::Schedule* schedule,
                       UserConfiguration* userConfiguration = 0);

};

#endif // LOAD_CALL_AGGREGATOR
