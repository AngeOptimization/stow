#ifndef WEIGHT_AGGREGATOR_H
#define WEIGHT_AGGREGATOR_H

#include <QHash>
#include <qdatacube/abstractaggregator.h>
#include <QAbstractItemModel>

class basic_container_list_t;

class WeightAggregator : public qdatacube::AbstractAggregator
{
  Q_OBJECT
  public:
    WeightAggregator(basic_container_list_t* underlying_model);
    virtual int operator()(int row) const;
    virtual int categoryCount() const;
    virtual QVariant categoryHeaderData(int category, int role = Qt::DisplayRole) const;
  private:
    void add_new_category(int weight_in_tonnes) const;
    void add_row_if_neccessary(int row) const;
    typedef QHash<int, int> index_t;
    mutable index_t m_index;
    mutable QList<QString> m_category_list;
  private Q_SLOTS:
    void refresh_categories(QModelIndex top_left, QModelIndex bottom_right);
    void refresh_categories(const QModelIndex& parent, int start, int end);

};

#endif // WEIGHT_AGGREGATOR_H
