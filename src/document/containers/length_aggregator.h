/*
 Author: Ange Optimization <esben@ange.dk>  (C) Ange Optimization ApS 2009

 Copyright: See COPYING file that comes with this distribution

*/

#ifndef LENGTH_AGGREGATOR_H
#define LENGTH_AGGREGATOR_H

#include <qdatacube/abstractaggregator.h>
#include <ange/containers/isocode.h>
#include <QStringList>

class LengthAggregator : public qdatacube::AbstractAggregator {
    public:
        explicit LengthAggregator(QAbstractItemModel* model);
        virtual int operator()(int row) const;
        virtual int categoryCount() const;
        virtual QVariant categoryHeaderData(int category, int role = Qt::DisplayRole) const;
    private:
        QStringList m_categories;
        QHash<ange::containers::IsoCode::IsoLength, int> m_categorize_map;
};

#endif // LENGTH_AGGREGATOR_H
