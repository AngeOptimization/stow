#include "oogaggregator.h"
#include "basic_container_list.h"

int OogAggregator::operator()(int row) const {
  if (underlyingModel()->index(row, basic_container_list_t::OOG_TOP_COL).data(Qt::DisplayRole).toDouble() > 0) return CAT_OOG;
  if (underlyingModel()->index(row, basic_container_list_t::OOG_FRONT_COL).data(Qt::DisplayRole).toDouble() > 0) return CAT_OOG;
  if (underlyingModel()->index(row, basic_container_list_t::OOG_BACK_COL).data(Qt::DisplayRole).toDouble() > 0) return CAT_OOG;
  if (underlyingModel()->index(row, basic_container_list_t::OOG_LEFT_COL).data(Qt::DisplayRole).toDouble() > 0) return CAT_OOG;
  if (underlyingModel()->index(row, basic_container_list_t::OOG_RIGHT_COL).data(Qt::DisplayRole).toDouble() > 0) return CAT_OOG;
  return CAT_NOR;
}

OogAggregator::OogAggregator(QAbstractItemModel* model)
  : AbstractAggregator(model)
{
  m_categories << "OOG" << "NOR";
  setName(QString::fromLocal8Bit("oog"));
}

int OogAggregator::categoryCount() const {
    return m_categories.count();
}

QVariant OogAggregator::categoryHeaderData(int category, int role) const {
    if(role == Qt::DisplayRole) {
        return m_categories.at(category);
    }
    return QVariant();
}

