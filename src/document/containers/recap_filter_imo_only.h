#ifndef RECAP_FILTER_IMO_ONLY_H
#define RECAP_FILTER_IMO_ONLY_H

#include <qdatacube/abstractfilter.h>

class recap_filter_imo_only_t : public qdatacube::AbstractFilter {
public:
    virtual bool operator()(int row) const;
    explicit recap_filter_imo_only_t(QAbstractItemModel* underlying_model);
};

#endif // RECAP_FILTER_IMO_ONLY_H
