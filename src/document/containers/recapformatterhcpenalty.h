#ifndef RECAP_FORMATTER_HC_PENALTY_H
#define RECAP_FORMATTER_HC_PENALTY_H

#include <qdatacube/abstractformatter.h>
/**
 * A recap formatter counting the penalty induced by having HC containers.
 * Normal HC count as 2.25 TEU and 45' as 2.53, i.e. we loose 0.25 and 0.53 TEU
 * per 4x HC unit
 */
class RecapFormatterHCPenalty : public qdatacube::AbstractFormatter
{
    public:
        RecapFormatterHCPenalty(QAbstractItemModel* underlying_model, qdatacube::DatacubeView* parent = 0);
        virtual QString format(QList< int > rows) const;
    protected:
        virtual void update(UpdateType element);
    private:
        void recalculateCellSize();
        /**
         * return the slot count total penalty caused by HC containers in @param rows
         */
        double HCpenalty(const QList< int > rows) const;

};

#endif // RECAP_FORMATTER_HC_PENALTY_H
