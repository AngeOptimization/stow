
#include "loadcallaggregator.h"

#include "basic_container_list.h"
#include <ange/schedule/call.h>
#include <QModelIndex>

using ange::schedule::Call;
using ange::schedule::Schedule;

LoadCallAggregator::LoadCallAggregator(basic_container_list_t* underlyingModel, const Schedule* schedule,
                                       UserConfiguration* userConfiguration)
    : super(underlyingModel, schedule, basic_container_list_t::LOAD_CALL_COL, userConfiguration)
{
    setName(QString::fromLocal8Bit("load call"));
}

#include "loadcallaggregator.moc"
