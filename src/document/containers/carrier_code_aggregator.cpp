#include "carrier_code_aggregator.h"
#include "basic_container_list.h"
#include <ange/containers/container.h>
#include <QSet>

using ange::containers::Container;

CarrierCodeAggregator::CarrierCodeAggregator(basic_container_list_t* model)
  : ColumnAggregator(model, basic_container_list_t::CARRIER_COL)
{
    setName(QString::fromLocal8Bit("carrier"));
}

QVariant CarrierCodeAggregator::categoryHeaderData(int category, int role) const
{
    QVariant result = qdatacube::ColumnAggregator::categoryHeaderData(category, role);
    if(role == Qt::DisplayRole) {
        if(result.toString().isEmpty()) {
            result = "-";
        }
    }
    return result;
}

#include "carrier_code_aggregator.moc"
