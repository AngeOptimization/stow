#include "vgmaggregator.h"
#include <QAbstractItemModel>
#include "basic_container_list.h"

VGMAggregator::VGMAggregator(QAbstractItemModel* model)
  : AbstractAggregator(model)
{
    m_category << "-" << "VGM";
    setName(QString::fromLocal8Bit("VGM"));
}

int VGMAggregator::operator()(int row) const {
    return underlyingModel()->index(row, basic_container_list_t::VGM_COL).data(Qt::DisplayRole).toBool();
}

int VGMAggregator::categoryCount() const {
    return m_category.count();
}

QVariant VGMAggregator::categoryHeaderData(int category, int role) const {
    if(role == Qt::DisplayRole) {
        return m_category.at(category);
    }
    return QVariant();
}

