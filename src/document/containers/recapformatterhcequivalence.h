#ifndef RECAP_FORMATTER_HC_EQUIVALENCE_H
#define RECAP_FORMATTER_HC_EQUIVALENCE_H

#include <qdatacube/abstractformatter.h>
/**
 * A recap formatter counting the TEU equivalence induced by having HC containers.
 */
class RecapFormatterHCEquivalence : public qdatacube::AbstractFormatter
{
    public:
        RecapFormatterHCEquivalence(QAbstractItemModel* underlying_model, qdatacube::DatacubeView* parent = 0);
        virtual QString format(QList< int > rows) const;
    protected:
        virtual void update(UpdateType element);
    private:
        void recalculateCellSize();
        /**
         * return the slot count total penalty caused by HC containers in @param rows
         */
        double HCequivalence(const QList< int > rows) const;

};

#endif // RECAP_FORMATTER_HC_EQUIVALENCE_H
