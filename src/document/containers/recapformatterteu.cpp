#include "recapformatterteu.h"
#include <QAbstractItemModel>
#include <QEvent>
#include "basic_container_list.h"
#include <qdatacube/datacubeview.h>
#include <ange/containers/container.h>

QString RecapFormatterTEU::format(QList< int > rows) const {
    double teuCount = 0;
    Q_FOREACH(int row, rows) {
        Q_ASSERT(qobject_cast<basic_container_list_t*>(underlyingModel()));
        teuCount += static_cast<basic_container_list_t*>(underlyingModel())->get_container_at(row)
                                                                           ->equivalentTEU(m_HCequivalenceFactor);
    }
    return QString::number(teuCount, 'f', 0);
}

RecapFormatterTEU::RecapFormatterTEU(QAbstractItemModel* underlying_model, qdatacube::DatacubeView* parent,
                                     qreal HCequivalenceFactor)
 : AbstractFormatter(underlying_model, parent), m_HCequivalenceFactor(HCequivalenceFactor) {
    recalculateCellSize();
    setName(QString("TwentyFootEquivalentUnits"));
    setShortName(QString("TEU"));
}

void RecapFormatterTEU::update(qdatacube::AbstractFormatter::UpdateType element) {
    if(element == qdatacube::AbstractFormatter::CellSize) {
        recalculateCellSize();
    }
}


void RecapFormatterTEU::recalculateCellSize() {
    if(datacubeView()) {
        // Set the cell size, by summing up all the data in the model, and using that as input
        double accumulator = 0;
        for (int element = 0, nelements = underlyingModel()->rowCount(); element < nelements; ++element) {
            Q_ASSERT(qobject_cast<basic_container_list_t*>(underlyingModel()));
            accumulator += static_cast<basic_container_list_t*>(underlyingModel())->get_container_at(element)
                                                                                    ->equivalentTEU();
        }
        QString big_cell_contents = QString::number(accumulator, 'f', 0) + "t";
        setCellSize(QSize(datacubeView()->fontMetrics().width(big_cell_contents),
                            datacubeView()->fontMetrics().lineSpacing()));
    }
}
