
#include "bundlableaggregator.h"
#include "basic_container_list.h"
#include <ange/containers/container.h>
#include <stdexcept>

int BundlableAggregator::operator()(int row) const {
    const basic_container_list_t* list = qobject_cast<const basic_container_list_t*>(underlyingModel());
    Q_ASSERT(list);
    if(!list) {
        throw std::runtime_error("bad cast in ");
    }

    const ange::containers::Container* container = list->get_container_at(row);
    if (container->isBundlable()) {
        return 1;
    } else {
        return 0;
    }
}

BundlableAggregator::BundlableAggregator(QAbstractItemModel* model) : AbstractAggregator(model) {
    m_categories << QString::fromLatin1("-") << QString::fromLatin1("BUN");
    setName(QString::fromLatin1("Bundlable"));
}

int BundlableAggregator::categoryCount() const {
    return m_categories.count();
}

QVariant BundlableAggregator::categoryHeaderData(int category, int role) const {
    if(role == Qt::DisplayRole) {
        return m_categories.at(category);
    }
    return QVariant();
}



#include "bundlableaggregator.moc"
