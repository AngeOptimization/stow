
#ifndef ABSTRACTSCHEDULEBASEDAGGREGATOR_H
#define ABSTRACTSCHEDULEBASEDAGGREGATOR_H

#include <qdatacube/abstractaggregator.h>
#include "basic_container_list.h"

class UserConfiguration;
namespace ange {
namespace schedule {
class Call;
class Schedule;
}
}

/**
 * Baseclass to help handle call based aggregating and adding/removing/moving of calls
 */
class AbstractScheduleBasedAggregator : public qdatacube::AbstractAggregator {
    typedef qdatacube::AbstractAggregator super;

    Q_OBJECT

public:
    enum Roles {
        FULL_UNCODE = Qt::UserRole + 1
    };

public:
    /**
     * Constructor
     * @param userConfiguration is used for coloring, if it is null no colors are used
     */
    explicit AbstractScheduleBasedAggregator(basic_container_list_t* underlyingModel,
                                             const ange::schedule::Schedule* schedule,
                                             basic_container_list_t::container_columns_t column,
                                             const UserConfiguration* userConfiguration = 0);

    // Inherited:
    virtual QVariant categoryHeaderData(int category, int role = Qt::DisplayRole) const;
    virtual int categoryCount() const;
    virtual int operator()(int row) const;

    /**
     * Gets call out of the underlyingModel
     */
    const ange::schedule::Call* callForRow(int row) const;
    virtual ~AbstractScheduleBasedAggregator();

private Q_SLOTS:
    void callRemoved(const ange::schedule::Call* call);
    void callAdded(const ange::schedule::Call* call);
    void removeMovingCall(int fromPosition, int toPosition);
    void insertMovingCall(int fromPosition, int toPosition);

private:
    const ange::schedule::Schedule* m_schedule;
    const basic_container_list_t::container_columns_t m_column;
    const UserConfiguration* m_userConfiguration;
    QList<QString> m_categories;
    typedef QHash<const ange::schedule::Call*, int> CategoriesMap;
    CategoriesMap m_categoriesMap;

};

#endif // ABSTRACTSCHEDULEBASEDAGGREGATOR_H
