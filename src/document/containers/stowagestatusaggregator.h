#ifndef STOWED_AGGREGATOR_H
#define STOWED_AGGREGATOR_H

#include <qdatacube/abstractaggregator.h>

class basic_container_list_t;
namespace ange {
namespace schedule {
class Call;
}
}

/**
 * A class implementing the Stowage Status aggregator, i.e. telling if the container
 * was already on-board, loaded, restowed (aka. shifted), master plan loaded, discharged
 * or are unplanned
 */
class StowageStatusAggregator : public qdatacube::AbstractAggregator {
    typedef qdatacube::AbstractAggregator super;

    Q_OBJECT

public:
    // public for testing
    enum Categories {
        CAT_UNPLANNED, // is on the quay and should be loaded
        CAT_LOADED, // planned for load
        CAT_ONBOARD, // stays on board
        CAT_SHIFTED, // planned for shift/restow
        CAT_DISCHARGED, // planned for final discharge
        CAT_ROLL, // should have been onboard but was left on quay in previous call
        CAT_NA, // out of history or child in a bundle
        CAT_TEMPORARY_BROKEN, // This should not be displayed to the user, happens when building the aggregator
    };

public:
    explicit StowageStatusAggregator(basic_container_list_t* underlyingModel);

    virtual int operator()(int row) const;

    virtual int categoryCount() const;

    virtual QVariant categoryHeaderData(int category, int role = Qt::DisplayRole) const;

public Q_SLOTS:
    /**
     * Set the call to determine status compared to
     */
    void setCurrentCall(const ange::schedule::Call* call);

private:
    /**
     * Could be a shared constant right now, but we might want to make this aggregator configurable later and then
     * this would change.
     */
    QList<QString> m_categories;

    /**
     * The call to determine status compared to, is initialized to null
     */
    const ange::schedule::Call* m_currentCall;

};

#endif // STOWED_AGGREGATOR_H
