#ifndef HC_EQUIVALENCE_CONSTANTS_H
#define HC_EQUIVALENCE_CONSTANTS_H
/*
 * Normal HC count as 2.25 TEU and 45' as 2.53, i.e. we loose 0.25 and 0.53 TEU
 * per 4x HC unit
 */
static const double hc40teuEquivalence = 2.25;
static const double hc45teuEquivalence = 2.53;

#endif
