#ifndef BASIC_CONTAINER_LIST_H
#define BASIC_CONTAINER_LIST_H

#include <ange/containers/container.h>

#include <QAbstractItemModel>

class container_move_t;
namespace ange {
namespace schedule {
class Call;
}
namespace containers {
class Container;
}
}

/**
 * Container list, except the bits that requies dependencies on stowage
 * and other inconvinient bits.  See container_list_t for details
 */
class basic_container_list_t : public QAbstractItemModel {

    Q_OBJECT

public:

    enum container_columns_t {
      EQUIPMENT_NO_COL,
      ISOCODE_COL,
      WEIGHT_COL, // in tons. Should preferably have been a mass_t but it's not clear how that would be formatted properly through a QVariant.
      VGM_COL, //Verified gross mass status
      DG_COL, // parent for dangerous good details. Value is true if dg, false otherwise. // Due to magic done for DG_COL later on, this MUST NOT be at a position later than 15.
      REEFER_COL, // bool: reefer or not
      LIVE_COL, // bool (== Live): For instance heated tanks are powered non-reefers
      TEMPERATURE_COL,
      LOAD_CALL_COL,
      DISCHARGE_CALL_COL,
      PLACE_OF_DELIVERY_COL,
      PLANNED_COL,
      LOCATION_COL,
      CARRIER_COL,
      EMPTY_COL,
      // MOVES_COL: Parent for moves data for container. No data in the indexed item, it is only used as parent item.
      // Due to magic done for MOVES_COL later on, this MUST NOT be at a position later than 15.
      // The values stored in the child rows are moves as returned from stowage_t::moves()
      MOVES_COL,
      BOOKING_NUMBER_COL,
      HANDLING_CODES_COL, // These codes are called "Stow Codes" in the ui.
      OOG_TOP_COL,
      OOG_FRONT_COL,
      OOG_BACK_COL,
      OOG_LEFT_COL,
      OOG_RIGHT_COL,
      BUNDLE_COL,
      NOMINAL_POL_COL,
      NOMINAL_POD_COL,
      NCOLS // Automatic update number of columns
    };

    enum moves_columns_t {
      MOVES_CALL_COL, // Call*
      MOVES_SLOT_COL, // slot_t*
      MOVES_POSITION_COL, // BayRowTier
      MOVES_TYPE_COL, // stow_type_t
      MOVES_NCOLS // Automatic update number of columns
    };

    enum dangerous_goods_columns_t {
      DG_UNCODE_COL,
      DG_IMDG_COL,
      DG_NCOLS // Automatic update number of columns
    };

    /**
     * Construct empty basic_container_list. Use add_container() to add containers
     */
    basic_container_list_t( QObject* parent = 0 );

    /**
     * @return  (a) container with equipment number or null if none found
     * @param equipment_number
     */
    const ange::containers::Container* get_container_by_equipment_number( const QString& equipment_number ) const;

    /**
     * @return  (a) container with id or null if none found
     * @param id
     */
    ange::containers::Container* get_container_by_id( int id ) const;

    /**
     * @returns the nth container
     */
    const ange::containers::Container* get_container_at( int index ) const {
      return m_containers.at( index );
    }

    ange::containers::Container* get_container_at( int index ) {
      return m_containers.at( index );
    }

    // container_list_t is _not_ compatible with foreach
    typedef QList< ange::containers::Container*>::const_iterator const_iterator;
    const_iterator begin() const {
      return  m_containers.begin();
    }
    const_iterator end() const {
      return m_containers.end();
    }

    /**
     * Removes the container and deletes it
     */
    bool remove_container(const ange::containers::Container* container);

    // container_list_t::list() _is_ compatible with foreach
    const QList<ange::containers::Container*> list() {
      return m_containers;
    }

    const ange::containers::Container* at( int i ) const {
      return m_containers.at( i );
    }

    /**
     * @return container for index
     */
    const ange::containers::Container* at( const QModelIndex& index ) const {
      return m_containers.at( index.row() );
    }

    int find_container( const ange::containers::Container* container ) const;

    /**
     * @returns size of list
     */
    int size() const {
      return m_containers.size();
    }

    /**
     * Return containers from ids ( != row indexes!)
     */
    QList< ange::containers::Container* > get_containers_by_id(QList< int > ids);

    virtual QVariant data( const QModelIndex& index, int role = Qt::DisplayRole ) const;
    virtual int columnCount( const QModelIndex& parent = QModelIndex() ) const;
    virtual int rowCount( const QModelIndex& parent = QModelIndex() ) const;
    virtual QModelIndex parent( const QModelIndex& child ) const;
    virtual QModelIndex index( int row, int column, const QModelIndex& parent = QModelIndex() ) const;
    virtual QVariant headerData( int section, Qt::Orientation orientation, int role = Qt::DisplayRole ) const;
    /**
     * Adds container to list at
     * @param position
     * @param more if true, the container will not be added before this function is called again either with more
     *     set to false or position not in the same range. This enables container_list to insert multiple containers
     *     in one batch, immensely speeding up big insertions. It is safe to always is more=false.
     *
     */
    void add_container(ange::containers::Container* container, int position = -1, bool more = false);
    virtual Qt::ItemFlags flags(const QModelIndex& index) const;

private Q_SLOTS:
    void emit_container_feature_changed(ange::containers::Container* container, ange::containers::Container::Feature containerFeature);
    void bundlesChanged(ange::containers::Container* container);

Q_SIGNALS:
    void container_changed(const ange::containers::Container* container);

protected:
    void validateReverseIndex() const {
        Q_ASSERT(m_reverseindex_dirty || m_reverse_index.size() == m_containers.size());
    }
    virtual QVariant container_display_data( const ange::containers::Container* container, container_columns_t col ) const;
    virtual QVariant container_edit_data( const ange::containers::Container* container, container_columns_t col ) const;
    virtual QVariant vertical_header_display_data( container_columns_t column ) const;
    /**
     *  Virtual functions related to the stowage (i.e. the container's position on the vessel/yard)
     *  - implemented in subclasses (container_list_t).
     */
    virtual QList<const container_move_t*> moves_for_container(const ange::containers::Container*) const;
    virtual QVariant move_data(const container_move_t* move, moves_columns_t  column, int role) const;

protected:
    QList<ange::containers::Container*> m_containers;

private:
    typedef QVarLengthArray<container_columns_t, 5> Columns;
    Columns containerFeatureToColumns(ange::containers::Container::Feature containerFeature);
    void redo_reverseindex() const;
    void add_containers_from_backlog();

private:
    mutable QHash<int, int> m_reverse_index;
    mutable bool m_reverseindex_dirty;
    int m_backlog_first_position;
    QList<ange::containers::Container*> m_backlog;

};

#endif // BASIC_CONTAINER_LIST_H
