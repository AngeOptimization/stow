#include "recapfiltercontainerid.h"
#include "basic_container_list.h"
#include <ange/containers/container.h>
#include <QAbstractItemModel>

bool RecapFilterContainerId::operator()(int row) const {
    return m_container_ids.contains(static_cast<const basic_container_list_t*>(underlyingModel())->get_container_at(row)->id());
}

RecapFilterContainerId::RecapFilterContainerId(QSet< int > container_ids, basic_container_list_t* underlying_model): AbstractFilter(underlying_model), m_container_ids(container_ids) {
    setShortName("Sel");
    setName("Selection");

}



#include "recapfiltercontainerid.moc"
