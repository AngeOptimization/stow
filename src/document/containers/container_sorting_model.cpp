#include "container_sorting_model.h"
#include <ange/schedule/call.h>
#include "container_list.h"

container_sorting_model_t::container_sorting_model_t(QObject* parent): QSortFilterProxyModel(parent) {
}


bool container_sorting_model_t::lessThan(const QModelIndex& left, const QModelIndex& right) const{
  if((left.column() == container_list_t::DISCHARGE_CALL_COL && right.column() == container_list_t::DISCHARGE_CALL_COL)
      || (left.column()  == container_list_t::LOAD_CALL_COL && right.column() == container_list_t::LOAD_CALL_COL)) {
    if(ange::schedule::Call* leftcall = qobject_cast<ange::schedule::Call*>(left.data(Qt::EditRole).value<QObject*>())) {
      if(ange::schedule::Call* rightcall = qobject_cast<ange::schedule::Call*>(right.data(Qt::EditRole).value<QObject*>())) {
        int distance = leftcall->distance(rightcall);
        if(distance==0) {
          return QSortFilterProxyModel::lessThan(left, right);
        }
        return distance >= 0;
      } else {
        Q_ASSERT(false);
      }
    }
  }
  return QSortFilterProxyModel::lessThan(left, right);
}

