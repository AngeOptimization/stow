#ifndef CONTAINER_LIST_H
#define CONTAINER_LIST_H
#include <QList>
#include <QAbstractItemModel>
#include "basic_container_list.h"

class QUndoStack;

namespace ange {
  namespace schedule {
    class Call;
  }
  namespace vessel {
    class Slot;
  }
}

class stowage_t;

/**
 * This is the master model containing all the information about the containers themselves
 * (basic_container_list) and their relations to the vessel and the schedule.
 */
class container_list_t : public basic_container_list_t {
    Q_OBJECT
  public:
    /**
     * Constructor
     */
    container_list_t( stowage_t* stowage, QObject* parent = 0 );

    /**
     *  FIXME: rename to getContainersByLoadCall and dischargeCall respectively
     * @returns the list of containers loaded at @param call
     */
    QList<const ange::containers::Container*> get_containers_by_load_port(const ange::schedule::Call* call) const;
    /**
     * @returns the list of containers discharged at @param call
     */
    QList<const ange::containers::Container*> get_containers_by_discharge_port(const ange::schedule::Call* call) const;
  public Q_SLOTS:
    void slot_container_moved( const ange::containers::Container* container,
                               const ange::vessel::Slot* previous,
                               const ange::vessel::Slot* current,
                               const ange::schedule::Call* call );
    void set_current_call(const ange::schedule::Call* call);
    void slot_container_changed( const ange::containers::Container* container );
    void update_container_positions();


  private Q_SLOTS:
    void inform_plugins_of_insertions(const QModelIndex index, int start, int end);
    void inform_plugins_of_removals(const QModelIndex index, int start, int end);
    void inform_plugins_of_changes(const QModelIndex top_left, const QModelIndex bottom_right);
    //FIXME: rename to emit_containerChangedLoadCall and dischargeCall respectively
    void emit_container_changed_source(const ange::containers::Container* container);
    void emit_container_changed_destination(const ange::containers::Container* container);

  protected:
    /**
     * @override
     */
    virtual QVariant container_display_data( const ange::containers::Container* container, container_columns_t col ) const;
    virtual QVariant container_edit_data(const ange::containers::Container* container, container_columns_t col) const;

    stowage_t* m_stowage;
    const ange::schedule::Call* m_current_call;

    virtual QList<const container_move_t*> moves_for_container(const ange::containers::Container* container) const;
    virtual QVariant move_data(const container_move_t* move, basic_container_list_t::moves_columns_t column, int role) const;
};

#endif // CONTAINER_LIST_H
