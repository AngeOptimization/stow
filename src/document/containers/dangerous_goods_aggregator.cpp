#include "dangerous_goods_aggregator.h"
#include "basic_container_list.h"
#include <stdexcept>
#include <ange/containers/container.h>
#include <ange/containers/dangerousgoodscode.h>

static QList< QString > private_categories() {
  const char* array[] = {
    "NOR", "?.?", "1.1" "1.2", "1.5", "2", "2.1", "2.2","3","4.1", "4.2", "4.3", "5.1", "5.2","6.1", "6.2", "7","8","9"
  };
  QList<QString> rv;
  for (unsigned i=0; i<sizeof(array)/sizeof(const char*); ++i) {
    rv << QString::fromLocal8Bit(array[i]);
  }
    return rv;
}

DangerousGoodsAggregator::DangerousGoodsAggregator(QAbstractItemModel* underlying_model): AbstractAggregator(underlying_model)
{
    m_categories = private_categories();
    setName(QString("Dangerous goods"));
}

int DangerousGoodsAggregator::categoryCount() const {
    return m_categories.count();
}

QVariant DangerousGoodsAggregator::categoryHeaderData(int category, int role) const {
    if(role == Qt::DisplayRole) {
        return m_categories.at(category);
    }
    return QVariant();
}



int DangerousGoodsAggregator::operator()(int row) const {
  const basic_container_list_t* list = qobject_cast<const basic_container_list_t*>(underlyingModel());
  Q_ASSERT(list);
  if(!list) {
    throw std::runtime_error("bad cast in ");
  }

  const ange::containers::Container* container = list->get_container_at(row);
  int rv = 0;
  if (!container->dangerousGoodsCodes().isEmpty()) {
    const ange::containers::DangerousGoodsCode& code = container->dangerousGoodsCodes().front();
    int new_cat = m_categories.indexOf(code.imdgClass());
    rv = (new_cat >=2) ? new_cat : 1;
  }
  return rv;

}
