/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010

 Copyright: See COPYING file that comes with this distribution

*/

#ifndef REEFER_AGGREGATOR_H
#define REEFER_AGGREGATOR_H

#include <qdatacube/abstractaggregator.h>
#include <QStringList>


class ReeferAggregator : public qdatacube::AbstractAggregator {
    public:
        explicit ReeferAggregator(QAbstractItemModel* model);
        virtual int operator()(int row) const;
        virtual int categoryCount() const;
        virtual QVariant categoryHeaderData(int category, int role = Qt::DisplayRole) const;
    private:
        QStringList m_categories;

};

#endif // REEFER_AGGREGATOR_H
