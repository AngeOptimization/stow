#ifndef VGM_AGGREGATOR_H
#define VGM_AGGREGATOR_H

#include <qdatacube/abstractaggregator.h>

class VGMAggregator : public qdatacube::AbstractAggregator {

  public:
    virtual int operator()(int row) const;
    explicit VGMAggregator(QAbstractItemModel* model);
    virtual int categoryCount() const;
    virtual QVariant categoryHeaderData(int category, int role = Qt::DisplayRole) const;
  private:
    QList<QString> m_category;
};

#endif // VGM_AGGREGATOR_H
