/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010

 Copyright: See COPYING file that comes with this distribution

*/

#ifndef RECAP_FILTER_UNPLANNED_H
#define RECAP_FILTER_UNPLANNED_H

#include <qdatacube/abstractfilter.h>

class basic_container_list_t;
namespace ange {
namespace schedule {
class Call;
}
}

class RecapFilterUnplanned : public qdatacube::AbstractFilter {
  Q_OBJECT
  public:
    RecapFilterUnplanned(basic_container_list_t* underlying_model, const ange::schedule::Call* from);
    virtual bool operator()(int row) const;
  public Q_SLOTS:
    void set_load_call(const ange::schedule::Call* call);
  private:
    bool unplanned(int row) const;
    const ange::schedule::Call* m_from;
};

#endif // RECAP_FILTER_UNPLANNED_H
