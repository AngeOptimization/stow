#include "editablecontainerlist.h"

#include "changecontainercommand.h"
#include "containerchanger.h"
#include "containerutils.h"
#include "document.h"

#include <ange/containers/oog.h>
#include <ange/schedule/call.h>

using ange::containers::Container;
using ange::containers::Oog;

void EditableContainerList::changeFeature(const Container* container, ChangeContainerCommand::Feature feature,
                                          const QVariant data) {
    ChangeContainerCommand* command = ChangeContainerCommand::change_feature(this, container, feature, data);
    m_document->undostack()->push(command);
}

bool EditableContainerList::setData(const QModelIndex& index, const QVariant& value, int role) {
    if (role == Qt::EditRole) {
        container_columns_t column = static_cast<container_columns_t>(index.column());
        Container* container = m_containers[index.row()];
        switch (column) {
            case VGM_COL:
            case NCOLS:
            case BUNDLE_COL:
            case LOCATION_COL:
            case PLANNED_COL:
            case MOVES_COL:
            case ISOCODE_COL:
            case NOMINAL_POD_COL:
            case NOMINAL_POL_COL:
                // either not relevant or not implemented. Description more accurate in \ref flags function
                Q_ASSERT(false);
                return false;
            case DG_COL: {
                QList<ange::containers::DangerousGoodsCode> dg_codes = ContainerUtils::parseDangerousGoodsString(value.value<QString>());
                if(dg_codes.isEmpty() == value.value<QString>().isEmpty()) {
                    changeFeature(container, ChangeContainerCommand::Dg,
                                  QVariant::fromValue<QList<ange::containers::DangerousGoodsCode> >(dg_codes));
                    return true;
                } else {
                    return false;
                }
            }
            case EQUIPMENT_NO_COL: {
                Q_ASSERT(value.canConvert<QString>());
                ange::containers::EquipmentNumber equipmentNumber(value.toString());
                changeFeature(container, ChangeContainerCommand::EquipmentNo,
                              QVariant::fromValue<ange::containers::EquipmentNumber>(equipmentNumber));
                return true;
            }
            case PLACE_OF_DELIVERY_COL: {
                changeFeature(container, ChangeContainerCommand::PlaceOfDelivery, value);
                return true;
            }
            case TEMPERATURE_COL: {
                changeFeature(container, ChangeContainerCommand::Temperature, value);
                return true;
            }
            case REEFER_COL:
                Q_ASSERT(value.canConvert<bool>());
                changeFeature(container, ChangeContainerCommand::Reefer, value);
                return true;
            case LIVE_COL:
                Q_ASSERT(value.canConvert<bool>());
                changeFeature(container, ChangeContainerCommand::Live, value);
                return true;
            case EMPTY_COL:
                Q_ASSERT(value.canConvert<bool>());
                changeFeature(container, ChangeContainerCommand::Empty, value);
                return true;
            case WEIGHT_COL:
                Q_ASSERT(value.canConvert<ange::units::Mass>());
                changeFeature(container, ChangeContainerCommand::Weight, value);
                return true;
            case LOAD_CALL_COL: {
                Q_ASSERT(value.canConvert<ange::schedule::Call*>());
                QSharedPointer<ChangerResult> result = ContainerChanger::changeLoadCall(m_document, container, value.value<ange::schedule::Call*>());
                // TODO emit error of result.
                m_document->undostack()->push(result->takeCommand());
                return true;
            }
            case DISCHARGE_CALL_COL: {
                Q_ASSERT(value.canConvert<ange::schedule::Call*>());
                QSharedPointer<ChangerResult> result = ContainerChanger::changeDischargeCall(m_document, container, value.value<ange::schedule::Call*>());
                // TODO emit error of result.
                m_document->undostack()->push(result->takeCommand());
                return true;
            }
            case CARRIER_COL:
                changeFeature(container, ChangeContainerCommand::Carrier, value);
                return true;
            case BOOKING_NUMBER_COL:
                changeFeature(container, ChangeContainerCommand::BookingNumber, value);
                return true;
            case HANDLING_CODES_COL: {
                QList<ange::containers::HandlingCode> handlingCodes = ContainerUtils::parseHandlingCodesString(value.value<QString>());
                changeFeature(container, ChangeContainerCommand::HandlingCodes,
                              QVariant::fromValue<QList<ange::containers::HandlingCode> >(handlingCodes));
                break;
            }
            case OOG_TOP_COL:
            case OOG_FRONT_COL:
            case OOG_BACK_COL:
            case OOG_LEFT_COL:
            case OOG_RIGHT_COL:
                ange::containers::Oog oog = container->oog();
                using ange::units::centimeter;
                switch(column) {
                    case OOG_TOP_COL:
                        oog.setTop(value.value<double>() * centimeter);
                        break;
                    case OOG_BACK_COL:
                        oog.setBack(value.value<double>() * centimeter);
                        break;
                    case OOG_FRONT_COL:
                        oog.setFront(value.value<double>() * centimeter);
                        break;
                    case OOG_LEFT_COL:
                        oog.setLeft(value.value<double>() * centimeter);
                        break;
                    case OOG_RIGHT_COL:
                        oog.setRight(value.value<double>() * centimeter);
                        break;
                    default:
                        Q_ASSERT(false);
                        return false;;
                }
                changeFeature(container, ChangeContainerCommand::OogFeature, QVariant::fromValue<Oog>(oog));
                break;
        }
    }
    return QAbstractItemModel::setData(index, value, role);
}

EditableContainerList::EditableContainerList(document_t* document, QObject* parent): container_list_t(document->stowage(), parent), m_document(document) {
}

Qt::ItemFlags EditableContainerList::flags(const QModelIndex& index) const {
    Qt::ItemFlags flags = super::flags(index);
    container_columns_t column = static_cast<container_columns_t>(index.column());
    switch (column) {
        case BOOKING_NUMBER_COL:
        case HANDLING_CODES_COL:
        case OOG_TOP_COL:
        case OOG_BACK_COL:
        case OOG_FRONT_COL:
        case OOG_LEFT_COL:
        case OOG_RIGHT_COL:
        case CARRIER_COL:
        case LOAD_CALL_COL:
        case DISCHARGE_CALL_COL:
        case PLACE_OF_DELIVERY_COL:
        case REEFER_COL:
        case LIVE_COL:
        case EQUIPMENT_NO_COL:
        case WEIGHT_COL:
        case EMPTY_COL:
        case TEMPERATURE_COL:
        case DG_COL: {
            flags |= Qt::ItemIsEditable;
            break;
        }
        case VGM_COL:
        case NOMINAL_POD_COL:
        case NOMINAL_POL_COL:
        case ISOCODE_COL:
            // not implemented
            break;
        case PLANNED_COL:
        case LOCATION_COL:
        case MOVES_COL:
        case NCOLS:
        case BUNDLE_COL:
            // doesn't make sense to edit
            break;
    }
    return flags;
}

#include "editablecontainerlist.moc"
