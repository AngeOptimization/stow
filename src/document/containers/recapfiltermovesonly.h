#ifndef RECAP_FILTER_MOVES_ONLY_H
#define RECAP_FILTER_MOVES_ONLY_H

#include <qdatacube/orfilter.h>
#include "stowagestatusaggregator.h"
#include <QSharedPointer>

class basic_container_list_t;

class RecapFilterMovesOnly : public qdatacube::OrFilter {
    Q_OBJECT
public:
    explicit RecapFilterMovesOnly(basic_container_list_t* underlyingModel);
    virtual ~RecapFilterMovesOnly();
Q_SIGNALS:
    void setCurrentCall(const ange::schedule::Call* call);
private:
    QSharedPointer<StowageStatusAggregator> m_stowageStatusAggregator;
};

#endif // RECAP_FILTER_MOVES_ONLY_H
