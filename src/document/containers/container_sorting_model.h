#ifndef CONTAINER_SORTING_MODEL_H
#define CONTAINER_SORTING_MODEL_H

#include <QSortFilterProxyModel>


class container_sorting_model_t : public QSortFilterProxyModel
{
  public:
    container_sorting_model_t(QObject* parent = 0);
  protected:
    virtual bool lessThan(const QModelIndex& left, const QModelIndex& right) const;
};

#endif // CONTAINER_SORTING_MODEL_H
