/*
 Author: Ange Optimization <esben@ange.dk>  (C) Ange Optimization ApS 2009

 Copyright: See COPYING file that comes with this distribution

*/

#ifndef HEIGHT_AGGREGATOR_H
#define HEIGHT_AGGREGATOR_H

#include <qdatacube/abstractaggregator.h>
#include <ange/containers/isocode.h>
#include <QStringList>

class HeightAggregator : public qdatacube::AbstractAggregator {
    public:
        explicit HeightAggregator(QAbstractItemModel* model);
        virtual int operator()(int row) const;
        virtual int categoryCount() const;
        virtual QVariant categoryHeaderData(int category, int role = Qt::DisplayRole) const;
        /**
        * @return the IsoCode height of the container given the @param category
        */
        static ange::containers::IsoCode::IsoHeight iso_height(const int category);
    private:
        QStringList m_categories;
        QHash<ange::containers::IsoCode::IsoHeight, int> m_categorize_map;
};

#endif // HEIGHT_AGGREGATOR_H
