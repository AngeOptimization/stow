#include "reefer_aggregator.h"

#include "basic_container_list.h"

#include <ange/containers/container.h>

#include <QAbstractItemModel>

using ange::containers::Container;

static QList<QString> privatecategories() {
  QList<QString> rv;
  rv << QString::fromLocal8Bit("-");
  rv << QString::fromLocal8Bit("LIV");
  return rv;
}

ReeferAggregator::ReeferAggregator(QAbstractItemModel* model): AbstractAggregator(model) {
    setName(QString::fromLocal8Bit("live (Reefer)"));
    m_categories = privatecategories();
}

int ReeferAggregator::operator()(int row) const {
    return underlyingModel()->index(row, basic_container_list_t::LIVE_COL).data(Qt::EditRole).toBool();
}

int ReeferAggregator::categoryCount() const {
    return m_categories.count();
}

QVariant ReeferAggregator::categoryHeaderData(int category, int role) const {
    if(role == Qt::DisplayRole) {
        return m_categories.at(category);
    }
    return QVariant();
}
