/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010
 Copyright: See COPYING file that comes with this distribution
*/
#include "recap_filter_unplanned.h"
#include "basic_container_list.h"
#include <ange/schedule/call.h>
#include <ange/vessel/slot.h>
#include <ange/containers/container.h>
#include <QString>
#include <stdexcept>
using ange::vessel::Slot;
using ange::schedule::Call;
using ange::containers::Container;

RecapFilterUnplanned::RecapFilterUnplanned(basic_container_list_t* underlying_model, const ange::schedule::Call* from):
    AbstractFilter(underlying_model),
    m_from(from)
{
    setName(tr("unplanned"));
    setShortName(tr("UNP"));
}

bool RecapFilterUnplanned::unplanned(int row) const {
  if(!m_from) {
    return false;
  }
  const Container* container = underlyingModel()->index(row, 0).data(Qt::UserRole).value<Container*>();
  if(container->bundleParent()) {
      row = static_cast<const basic_container_list_t*>(underlyingModel())->find_container(container->bundleParent());
  }
  // Return true iff source port is from and container has no moves OR if last discharge move is
  // from and from != discharge port
  const Call* source = qobject_cast<const Call*>(underlyingModel()->index(row, basic_container_list_t::LOAD_CALL_COL).data(Qt::EditRole).value<QObject*>());
  QModelIndex moves_index(underlyingModel()->index(row, basic_container_list_t::MOVES_COL));
  int nmoves = underlyingModel()->rowCount(moves_index);
  if (nmoves == 0) {
    return m_from == source;
  }
  const Call* destination = qobject_cast<const Call*>(underlyingModel()->index(row, basic_container_list_t::DISCHARGE_CALL_COL).data(Qt::EditRole).value<QObject*>());
  if (destination == m_from) {
    return false;
  }
  const Slot* last_slot = qobject_cast<Slot*>(underlyingModel()->index(nmoves-1, basic_container_list_t::MOVES_SLOT_COL, moves_index).data(Qt::EditRole).value<QObject*>());
  if (last_slot) {
    // Not a discharge
    return false;
  }
  const Call* last_call = qobject_cast<const Call*>(underlyingModel()->index(nmoves-1, basic_container_list_t::MOVES_CALL_COL, moves_index).data(Qt::EditRole).value<QObject*>());
  return last_call == m_from;
}

bool RecapFilterUnplanned::operator()(int row) const {
    return  unplanned(row);
}


void RecapFilterUnplanned::set_load_call(const ange::schedule::Call* call) {
    m_from = call;
}

#include "recap_filter_unplanned.moc"
