#ifndef OOG_AGGREGATOR_H
#define OOG_AGGREGATOR_H

#include <qdatacube/abstractaggregator.h>
#include <QStringList>


class OogAggregator : public qdatacube::AbstractAggregator
{
  public:
          // public for testing
    enum Categories {
        CAT_OOG, // OOG
        CAT_NOR  // Normal
    };
    virtual int operator()(int row) const;
    explicit OogAggregator(QAbstractItemModel* model);
    virtual int categoryCount() const;
    virtual QVariant categoryHeaderData(int category, int role = Qt::DisplayRole) const;
  private:
    QStringList m_categories;

};

#endif // OOG_AGGREGATOR_H
