#ifndef BUNDLEAGGREGATOR_H
#define BUNDLEAGGREGATOR_H

#include <qdatacube/abstractaggregator.h>


/**
 * \brief aggregates based on bundles
 */

class BundleAggregator : public qdatacube::AbstractAggregator {
    Q_OBJECT
    public:
        virtual int operator()(int row) const;
        BundleAggregator(QAbstractItemModel* model);
        virtual int categoryCount() const;
        virtual QVariant categoryHeaderData(int category, int role = Qt::DisplayRole) const;
    private:
        QList<QString> m_categories;
};

#endif // BUNDLEAGGREGATOR_H
