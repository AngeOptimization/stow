#include "schedulemodel.h"
#include <ange/schedule/schedule.h>
#include "userconfiguration/userconfiguration.h"
#include <ange/schedule/call.h>
#include <ange/schedule/cranerules.h>
#include <cmath>
#include <QSignalMapper>
#include <QUndoStack>
#include "undo/call_change_command.h"
#include "gui/colortools.h"
#include <QComboBox>

using ange::schedule::Call;
using ange::schedule::Schedule;

const double defaultCraneProductivity = 30.0;

ScheduleModel::ScheduleModel(Schedule* schedule, const UserConfiguration* user_configuration, QUndoStack* undostack, QObject* parent):
    QAbstractTableModel(parent),
    m_schedule(schedule),
    m_userConfiguration(user_configuration),
    m_undostack(undostack),
    m_movingInProgress(false)
{
    connect(schedule, &Schedule::callAboutToBeRemoved, this, &ScheduleModel::slotCallAboutToBeRemoved);
    connect(schedule, &Schedule::callAdded, this, &ScheduleModel::slotCallAdded);
    connect(schedule, &Schedule::callRemoved, this, &ScheduleModel::slotCallRemoved);
    connect(schedule, &Schedule::callAboutToBeAdded, this, &ScheduleModel::slotCallAboutToBeAdded);
    connect(schedule, &Schedule::rotationChanged, this, &ScheduleModel::rotationChanged);
    connect(schedule, &Schedule::rotationAboutToBeChanged, this, &ScheduleModel::rotationAboutToBeChanged);
    if(m_userConfiguration) { // at least in tests, user configuration can be null
        connect(m_userConfiguration, &UserConfiguration::colorsChanged,this, &ScheduleModel::allDataChanged);
    }
    m_callMapper = new QSignalMapper(this);
    Q_FOREACH(Call* call, schedule->calls()) {
        m_callMapper->setMapping(call, call->id());
        connect(call, &Call::cranesChanged, m_callMapper, static_cast<void(QSignalMapper::*)()>(&QSignalMapper::map));
        connect(call, &Call::heightLimitChanged, m_callMapper, static_cast<void(QSignalMapper::*)()>(&QSignalMapper::map));
        connect(call, &Call::voyageCodeChanged, m_callMapper, static_cast<void(QSignalMapper::*)()>(&QSignalMapper::map));
        connect(call, &Call::craneRuleChanged, m_callMapper, static_cast<void(QSignalMapper::*)()>(&QSignalMapper::map));
        connect(call, &Call::twinLiftingChanged, m_callMapper, static_cast<void(QSignalMapper::*)()>(&QSignalMapper::map));
        connect(call, &Call::craneProductivityChanged, m_callMapper, static_cast<void(QSignalMapper::*)()>(&QSignalMapper::map));
        connect(call, &Call::etaChanged, m_callMapper, static_cast<void(QSignalMapper::*)()>(&QSignalMapper::map));
        connect(call, &Call::etdChanged, m_callMapper, static_cast<void(QSignalMapper::*)()>(&QSignalMapper::map));
    }
    connect(m_callMapper, SIGNAL(mapped(int)), SLOT(slotCallChanged(int)));
}

Qt::ItemFlags ScheduleModel::flags(const QModelIndex& index) const
{
    Qt::ItemFlags rv = QAbstractTableModel::flags(index);
    column_t col = static_cast<column_t>(index.column());
    switch (col) {
    case UNCODE_COL:
    {
        break;
    }
    case ETA_COL:
    case ETD_COL:
    case VOYAGE_COL:
    case CRANES_COL:
    case HLIMIT_COL:
    case CRANE_RULE_COL:
    case TWINLIFT_COL :
    case PRODUCTIVITY_COL: {
            // BEFORE and AFTER cannot be edited
            if (index.row() > 0 && index.row() < m_schedule->size()-1) {
            rv |= Qt::ItemIsEditable;
        }
        break;
    }
    case NO_COLUMNS: {
        Q_ASSERT(false);
        break;
        }
    }
    return rv;
}

bool ScheduleModel::setData(const QModelIndex& index, const QVariant& value, int role)
{
    if (role == Qt::EditRole) {
        column_t col = static_cast<column_t>(index.column());
        Call* call = qvariant_cast<Call*>(data(index, Qt::UserRole));
        return setEditData(call, col, value);
    }
    return QAbstractItemModel::setData(index, value, role);
}

QVariant ScheduleModel::data(const QModelIndex& index, int role) const {
    if(!index.isValid()) {
        return QVariant();
    }
    Call* call = m_schedule->at(index.row());
    column_t col = static_cast<column_t>(index.column());
    switch (role) {
        case Qt::DisplayRole:
            return displayData(call,col);
        case Qt::EditRole:
            return editData(call,col);
        case Qt::BackgroundRole:
            return backgroundColor(call);
        case Qt::ForegroundRole:
            return backgroundRequiresColorInversion(backgroundColor(call))? QColor(Qt::white) : QColor(Qt::black);
        case Qt::UserRole:
            return qVariantFromValue<Call*>(call);
    }
    return QVariant();
}

int ScheduleModel::columnCount(const QModelIndex& ) const {
    return static_cast<int>(NO_COLUMNS);
}

int ScheduleModel::rowCount(const QModelIndex& parent) const {
    if(parent.isValid()) {
        return 0;
    } else {
        return m_schedule->size();
    }
}

QColor ScheduleModel::backgroundColor(const ange::schedule::Call* call) const {
    return m_userConfiguration->get_color_by_call(call);
}

QVariant ScheduleModel::displayData(const ange::schedule::Call* call, ScheduleModel::column_t column) const {
    switch (column) {
        case UNCODE_COL:
            return call->uncode();
        case ETA_COL:
            return call->eta();
        case ETD_COL:
            return call->etd();
        case CRANES_COL: {
            const double cranes = call->cranes();
            return std::isnan(cranes) ? QVariant() : QVariant(cranes);
        }
        case HLIMIT_COL: {
            const double height_limit = call->heightLimit();
            return std::isnan(height_limit) ? QVariant() : QVariant(height_limit);
        }
        case CRANE_RULE_COL: {
            return ange::schedule::CraneRules::toString(call->craneRule());
        }
        case TWINLIFT_COL: {
            return ange::schedule::TwinLiftRules::toString(call->twinLifting());
        }
        case PRODUCTIVITY_COL: {
            const double productivity = call->craneProductivity();
            return std::isnan(productivity) ? QVariant() : QVariant(productivity);
        }
        case VOYAGE_COL:
            return call->voyageCode();
        case NO_COLUMNS:
            Q_ASSERT(false);
    }
    Q_ASSERT(false);
    return QVariant();
}

QVariant ScheduleModel::editData(const ange::schedule::Call* call, ScheduleModel::column_t column) const {
    switch (column) {
        case UNCODE_COL:
            return call->uncode();
        case ETA_COL:
            return call->eta().isNull() ? QDateTime::currentDateTime() : call->eta();
        case ETD_COL:
            return call->etd().isNull() ? QDateTime::currentDateTime() : call->etd();
        case CRANES_COL:
            return std::isnan(call->cranes())?3.0:call->cranes();
        case HLIMIT_COL:
            return std::isnan(call->heightLimit())?10.0:call->heightLimit();
        case CRANE_RULE_COL:
            return QVariant::fromValue<ange::schedule::CraneRules::Types>(call->craneRule());
        case TWINLIFT_COL:
            return QVariant::fromValue<ange::schedule::TwinLiftRules::Types>(call->twinLifting());
        case PRODUCTIVITY_COL:
            return std::isnan(call->craneProductivity()) ? defaultCraneProductivity : call->craneProductivity();
        case VOYAGE_COL:
            return call->voyageCode();
        case NO_COLUMNS:
            Q_ASSERT(false);
    }
    Q_ASSERT(false);
    return QVariant();
}

bool ScheduleModel::setEditData(ange::schedule::Call* call, ScheduleModel::column_t column, QVariant value) {
    switch (column) {
        case UNCODE_COL:
            return false;
        case ETA_COL: {
            m_undostack->push(new call_change_command_t(m_schedule, call, column, value));
            break;
        }
        case ETD_COL: {
            m_undostack->push(new call_change_command_t(m_schedule, call, column, value));
            break;
        }
        case VOYAGE_COL: {
            QString voyage_code = value.toString();
            if (voyage_code.size() >= 4) {
                m_undostack->push(new call_change_command_t(m_schedule, call, column, value));
            }
            break;
        }
        case CRANES_COL:
        case PRODUCTIVITY_COL:
        case HLIMIT_COL:{
            bool ok;
            value.toDouble(&ok);
            if (ok) {
                m_undostack->push(new call_change_command_t(m_schedule, call, column, value));
                return true;
            }
            break;
        }
        case CRANE_RULE_COL: {
            bool ok = value.canConvert<ange::schedule::CraneRules::Types>();
            if (ok) {
                m_undostack->push(new call_change_command_t(m_schedule, call, column, value));
                return true;
            }
            break;
        }
        case TWINLIFT_COL: {
            m_undostack->push(new call_change_command_t(m_schedule, call, column, value));
            return true;
        }
        case NO_COLUMNS:
            Q_ASSERT(false);
    }
    return false;
}

QVariant ScheduleModel::headerData(int section, Qt::Orientation orientation, int role) const {
    if (orientation == Qt::Horizontal) {
        if (section<0 || section>=NO_COLUMNS) {
            qWarning("Requeste headerData in schedule_model outside of range (0 <= %d < %d)", section, NO_COLUMNS);
            return QVariant();
        }
        if (role == Qt::DisplayRole) {
            switch (static_cast<column_t>(section)) {
                case UNCODE_COL:
                    return tr("Port");
                case ETA_COL:
                    return tr("ETA");
                case ETD_COL:
                    return tr("ETD");
                case CRANES_COL:
                    return tr("Cr.");
                case HLIMIT_COL:
                    return tr("Max.H.");
                case CRANE_RULE_COL:
                    return tr("Cr.Rule");
                case TWINLIFT_COL:
                    return tr("TwinL.");
                case PRODUCTIVITY_COL:
                    return tr("Prod.");
                case VOYAGE_COL:
                    return tr("Voya.");
                case NO_COLUMNS:
                    Q_ASSERT(false);
            }
        }
    }
    return QAbstractItemModel::headerData(section, orientation, role);
}


void ScheduleModel::slotCallAboutToBeRemoved(ange::schedule::Call* call) {
    int row = m_schedule->indexOf(call);
    Q_ASSERT(row >= 0);
    beginRemoveRows(QModelIndex(), row, row);
}

void ScheduleModel::slotCallAboutToBeAdded(int position, Call* call) {
    Q_UNUSED(call);
    beginInsertRows(QModelIndex(), position, position);
}


void ScheduleModel::slotCallAdded(ange::schedule::Call* call) {
    m_callMapper->setMapping(call, call->id());
    connect(call, &Call::cranesChanged, m_callMapper, static_cast<void(QSignalMapper::*)()>(&QSignalMapper::map));
    connect(call, &Call::heightLimitChanged, m_callMapper, static_cast<void(QSignalMapper::*)()>(&QSignalMapper::map));
    connect(call, &Call::voyageCodeChanged, m_callMapper, static_cast<void(QSignalMapper::*)()>(&QSignalMapper::map));
    connect(call, &Call::craneRuleChanged, m_callMapper, static_cast<void(QSignalMapper::*)()>(&QSignalMapper::map));
    connect(call, &Call::twinLiftingChanged, m_callMapper, static_cast<void(QSignalMapper::*)()>(&QSignalMapper::map));
    connect(call, &Call::craneProductivityChanged, m_callMapper, static_cast<void(QSignalMapper::*)()>(&QSignalMapper::map));
    connect(call, &Call::etaChanged, m_callMapper, static_cast<void(QSignalMapper::*)()>(&QSignalMapper::map));
    connect(call, &Call::etdChanged, m_callMapper, static_cast<void(QSignalMapper::*)()>(&QSignalMapper::map));
    endInsertRows();
}

void ScheduleModel::slotCallRemoved(ange::schedule::Call* /*call*/) {
    endRemoveRows();
}

void ScheduleModel::slotCallChanged(int call_id) {
    const Call* call = m_schedule->getCallById(call_id);
    if (call) {
        const int row = m_schedule->indexOf(call);
        if (row != -1) {
            emit dataChanged(index(row, 0), index(row, NO_COLUMNS-1));
        }
    }
}

void ScheduleModel::allDataChanged() {
    dataChanged(index(0,0),index(rowCount()-1,columnCount()-1));
}

void ScheduleModel::rotationAboutToBeChanged(int fromPosition, int toPosition) {
    Q_ASSERT(!m_movingInProgress);
    if (fromPosition < toPosition) {
        // renumber from our move representation to QAbstractItemModel::beginMoveRows() representation
        m_movingInProgress = beginMoveRows(QModelIndex(), fromPosition, fromPosition, QModelIndex(), toPosition + 1);
    } else {
        m_movingInProgress = beginMoveRows(QModelIndex(), fromPosition, fromPosition, QModelIndex(), toPosition);
    }
}

void ScheduleModel::rotationChanged(int fromPosition, int toPosition) {
    Q_UNUSED(fromPosition);
    Q_UNUSED(toPosition);
    if (m_movingInProgress) {
        endMoveRows();
        m_movingInProgress = false;
    }
}

#include "schedulemodel.moc"
