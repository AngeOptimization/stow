#include "baplie15_writer.h"
#include <QDateTime>
#include <ange/vessel/vessel.h>
#include <ange/schedule/call.h>
#include <ange/containers/container.h>
#include "edifact_helper_routines.h"
#include "document/utils/callutils.h"


baplie15_writer_t::baplie15_writer_t(QIODevice* output,
                                     const ange::vessel::Vessel* vessel,
                                     const ange::schedule::Call* start_port,
                                     const ange::schedule::Call* next_port,
                                     const QString& vessel_code,
                                     const QString& voyage_number,
                                     const QString& carrier_code)
    :
    m_edi_writer(output),
    m_vessel(vessel),
    m_start_port(start_port),
    m_next_port(next_port),
    m_vessel_code(vessel_code),
    m_voyage_number(voyage_number),
    m_carrier_code(carrier_code),
    m_interchange_control_reference(qrand()),
    m_message_reference_number(qrand()),
    m_message_counter(0),
    m_started(false),
    m_ended(false)
{
    m_edi_writer.ignore_segment_for_count("UNA");
    m_edi_writer.ignore_segment_for_count("UNB");
    m_edi_writer.ignore_segment_for_count("UNZ");
}

baplie15_writer_t::~baplie15_writer_t(){
    if(!m_ended) {
        end();
    }
}

void baplie15_writer_t::start() {
    if(m_started) {
        qWarning("start already called once; doing nothing");
        return;
    }
    m_started=true;
    m_message_counter++;
    QDateTime now = QDateTime::currentDateTime().toUTC();

    ange::edifact_routines::writeUnb(&m_edi_writer, 1, m_carrier_code, now, m_interchange_control_reference);
    ange::edifact_routines::writeUnh(&m_edi_writer, m_message_reference_number, "BAPLIE", "1", "911", "SMDG15");
    ange::edifact_routines::writeBgm(&m_edi_writer, m_message_reference_number);
    ange::edifact_routines::writeDtm(&m_edi_writer, 137, now);
    ange::edifact_routines::writeTdtBaplie15(&m_edi_writer, m_voyage_number, m_carrier_code, m_vessel);
    ange::edifact_routines::writeLocPortBaplie15(&m_edi_writer, 5, m_start_port->uncode());
    ange::edifact_routines::writeLocPortBaplie15(&m_edi_writer, 61, m_next_port->uncode());
    ange::edifact_routines::writeDtmDay(&m_edi_writer, 178, now); //better and more times needed. FIXME
    ange::edifact_routines::writeRff(&m_edi_writer, "VON", m_voyage_number);
}

void baplie15_writer_t::end() {
    if(m_ended) {
        qWarning("already ended; doing nothing");
        return;
    }
    if(!m_started) {
        qWarning("header has not been written; doing nothing");
        return;
    }
    m_ended=true;
    ange::edifact_routines::writeUnt(&m_edi_writer, m_message_reference_number);
    ange::edifact_routines::writeUnz(&m_edi_writer, m_message_counter, m_interchange_control_reference);
}

void baplie15_writer_t::add_container(const ange::containers::Container* container, const ange::schedule::Call* source, const ange::schedule::Call* destination, const ange::vessel::BayRowTier& brt) {
    if(!m_started) {
        start();
    }
    if(m_ended) {
        qWarning("trailer has already been written; doing nothing");
        return;
    }
    Q_FOREACH(const ange::containers::Container* subcontainer, container->bundleChildren()) {
        add_container(subcontainer, source, destination, brt);
    }
    ange::edifact_routines::writeLocBrt(&m_edi_writer, brt);
    ange::edifact_routines::writeAllFtxHan(&m_edi_writer, container);
    ange::edifact_routines::writeMeaKgm(&m_edi_writer, container);
    //TODO: DIM entry for OOG, Break bulk, ...
    if(container->oog()) {
        ange::edifact_routines::writeAllDimOog(&m_edi_writer, container->oog(), "CM",9);
    }
    if(container->live()){
        ange::edifact_routines::writeTmpCel(&m_edi_writer, container->temperature());
    }
    if(!is_befor(source)) {
        ange::edifact_routines::writeLocPortBaplie15(&m_edi_writer, 6, source->uncode());
    } else {
        ange::edifact_routines::writeLocPortBaplie15(&m_edi_writer, 6, container->loadPort());
    }

    if(!is_after(destination)) {
        ange::edifact_routines::writeLocPortBaplie15(&m_edi_writer, 12, destination->uncode());
    } else {
        ange::edifact_routines::writeLocPortBaplie15(&m_edi_writer, 12, container->dischargePort());
    }
    if (!container->placeOfDelivery().isEmpty()) {
        ange::edifact_routines::writeLocPort(&m_edi_writer, 83, container->placeOfDelivery());
    }
    ange::edifact_routines::writeRffBm(&m_edi_writer, container);
    ange::edifact_routines::writeEqd(&m_edi_writer, container);
    if(!container->carrierCode().isEmpty()){
        ange::edifact_routines::writeNad(&m_edi_writer, "CA", container->carrierCode());
    }
    ange::edifact_routines::writeAllDgs(&m_edi_writer, container);
}
