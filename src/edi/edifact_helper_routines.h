#ifndef EDIFACT_HELPER_ROUTINES
#define EDIFACT_HELPER_ROUTINES

#include "edifact_writer.h"
#include "version.h"

#include <ange/containers/container.h>
#include <ange/containers/dangerousgoodscode.h>
#include <ange/containers/oog.h>
#include <ange/units/units.h>
#include <ange/vessel/bayrowtier.h>

#include <QSettings>
#include <QString>

/**
 * !!!!!!! NOTE !!!!!!!!!!!
 * Functions in this file is used both by the movins and the baplie writer, and some by the coprar writer, so when modifying,
 * be CAREFUL!
 */

namespace ange {
    namespace edifact_routines {

        inline void writeBgm(edifact_writer_t* ediWriter, int messageReferenceNumber) {
            ediWriter->write_start_segment("BGM");
            ediWriter->write_empty_element();
            ediWriter->write_element(messageReferenceNumber);
            ediWriter->write_element(9);
            ediWriter->write_end_segment();
        }

        inline void writeBgmCoprar(edifact_writer_t* ediWriter, int messageReferenceNumber) {
            ediWriter->write_start_segment("BGM");
            ediWriter->write_element(121); //LOADLIST
            ediWriter->write_element(messageReferenceNumber);
            ediWriter->write_element(9);
            ediWriter->write_element("AB");
            ediWriter->write_end_segment();
        }

        inline void writeCNT(edifact_writer_t* ediWriter, int containerCounter) {
            ediWriter->write_start_segment("CNT");
            ediWriter->write_element(16);
            ediWriter->write_component(containerCounter);
            ediWriter->write_end_segment();
        }

        inline void writeDgs(edifact_writer_t* ediWriter, const ange::containers::DangerousGoodsCode& dg) {
            ediWriter->write_start_segment("DGS");
            ediWriter->write_element("IMD");
            ediWriter->write_element(dg.imdgClass());
            ediWriter->write_empty_component();
            ediWriter->write_element(dg.unNumber());
            ediWriter->write_empty_element();   //flash point
            ediWriter->write_empty_component(); //unit for flashpoint
            ediWriter->write_empty_element();   //packaging
            ediWriter->write_empty_element();   //EMS
            ediWriter->write_empty_element();   // MFAG
            ediWriter->write_empty_element();
            ediWriter->write_empty_element();   //hazard identifcation upper part
            ediWriter->write_empty_component(); // hazard identification lower part
            ediWriter->write_empty_element();   // dangerous goods label 1
            ediWriter->write_empty_component(); // label 2
            ediWriter->write_empty_component(); // label 3
            ediWriter->write_end_segment();
        }

        inline void writeDtm(edifact_writer_t* ediWriter, int qualifier, const QDateTime& now) {
            ediWriter->write_start_segment("DTM");
            ediWriter->write_element(qualifier);
            ediWriter->write_component(now.toString("yyMMddhhmm"));
            ediWriter->write_component(201); //date format
            ediWriter->write_end_segment();
        }

        inline void writeDtmDay(edifact_writer_t* ediWriter, int qualifier, const QDateTime& now) {
            ediWriter->write_start_segment("DTM");
            ediWriter->write_element(qualifier);
            ediWriter->write_component(now.toString("yyMMdd"));
            ediWriter->write_component(101); //date format
            ediWriter->write_end_segment();
        }

        /**
         * Writes a EQD entry of the container to the edi writer.
         */
        inline void writeEqd(edifact_writer_t* ediWriter, const ange::containers::Container* container) {
            ediWriter->write_start_segment("EQD");
            ediWriter->write_element("CN");
            ediWriter->write_element(container->equipmentNumber());
            ediWriter->write_element(container->isoCode().code());
            ediWriter->write_empty_element();
            ediWriter->write_empty_element();
            ediWriter->write_element(container->empty()?"4":"5");
            ediWriter->write_end_segment();
        }

        inline void writeEqdCoprar(edifact_writer_t* ediWriter, const ange::containers::Container* container) {
            ediWriter->write_start_segment("EQD");
            ediWriter->write_element("CN");
            ediWriter->write_element(container->equipmentNumber());
            ediWriter->write_element(container->isoCode().code());
            ediWriter->write_component(102);
            ediWriter->write_component(5);
            ediWriter->write_empty_element(); //Supplier code
            ediWriter->write_empty_element(); // status code
            ediWriter->write_element(container->empty()?"4":"5");
            ediWriter->write_end_segment();
        }

        inline void writeEqn(edifact_writer_t* ediWriter) {
            ediWriter->write_start_segment("EQN");
            ediWriter->write_element(1);
            ediWriter->write_end_segment();
        }

        /**
         * Write one FTX+HAN handeling segment
         */
        inline void writeFtxHan(edifact_writer_t* ediWriter, const ange::containers::HandlingCode& handelingCode) {
            ediWriter->write_start_segment("FTX");
            ediWriter->write_element("HAN");
            ediWriter->write_element("");
            ediWriter->write_element("");
            ediWriter->write_element(handelingCode.code());
            ediWriter->write_end_segment();
        }

        inline void writeHan(edifact_writer_t* ediWriter, const QString direction) {
            ediWriter->write_start_segment("HAN");
            ediWriter->write_element(direction);
            ediWriter->write_end_segment();
        }

        /**
         * Writes the brt as a LOC entry correctly formatted.
         */
        inline void writeLocBrt(edifact_writer_t* ediWriter,const ange::vessel::BayRowTier& brt) {
            ediWriter->write_start_segment("LOC");
            ediWriter->write_element(147);
            ediWriter->write_element(QString("%1%2%3").arg(brt.bay(),3,10,QChar('0')).arg(brt.row(),2,10,QChar('0')).arg(brt.tier(),2,10,QChar('0')));
            ediWriter->write_empty_component();
            ediWriter->write_component(5);
            ediWriter->write_end_segment();
        }

        /**
         * Writes the location port with the given code (load, discharge, ...)
         */
        inline void writeLocPort(edifact_writer_t* ediWriter,int code ,const QString& uncode, edifact_writer_t::SMDGVersion smdgVersion = edifact_writer_t::SMDG22) {
            ediWriter->write_start_segment("LOC");
            ediWriter->write_element(code);
            ediWriter->write_element(uncode);
            if (smdgVersion != edifact_writer_t::SMDGVersion::SMDG15) {
                ediWriter->write_component(139);
                ediWriter->write_component(6);
            }
            ediWriter->write_end_segment();
        }

        /**
         * Writes the location port with the given code (load, discharge, ...)
         */
        inline void writeLocPortBaplie15(edifact_writer_t* ediWriter,int code ,const QString& uncode) {
            ediWriter->write_start_segment("LOC");
            ediWriter->write_element(code);
            ediWriter->write_element(uncode);
            ediWriter->write_end_segment();
        }

        /**
         * Writes the containers weight to the edifact_writer in kilograms
         */
        inline void writeMeaKgm(edifact_writer_t* ediWriter, const ange::containers::Container* container, edifact_writer_t::SMDGVersion smdgVersion = edifact_writer_t::SMDG20) {
            ediWriter->write_start_segment("MEA");
            if (smdgVersion == edifact_writer_t::SMDG22 && container->weightIsVerified()) {
                ediWriter->write_element("VGM");
            } else {
                ediWriter->write_element("WT");
            }
            ediWriter->write_empty_element();
            ediWriter->write_element("KGM");
            ediWriter->write_component(qRound(container->weight()/ange::units::kilogram));
            ediWriter->write_end_segment();
        }

        inline void writeMeaKgmCoprar(edifact_writer_t* ediWriter, const ange::containers::Container* container) {
            ediWriter->write_start_segment("MEA");
            ediWriter->write_element("AAE");
            ediWriter->write_element("G");
            ediWriter->write_element("KGM");
            ediWriter->write_component(qRound(container->weight()/ange::units::kilogram));
            ediWriter->write_end_segment();
        }

        /**
         * Writes the name and address of the given container (used for indicating the liner operator / carrier)
         * CA: Party function code qualifier for Carrier
         * CF: Party function code qualifier for Carrier
         * 172: Code list identification code for carriers
         */
        inline void writeNad(edifact_writer_t* ediWriter, const char* type, const QString& carrierCode) {
            ediWriter->write_start_segment("NAD");
            ediWriter->write_element(type);
            if (!carrierCode.isEmpty()){
                ediWriter->write_element(carrierCode);
            } else {
                ediWriter->write_element("ZZZ");
            }
            ediWriter->write_component(172);
            ediWriter->write_component(20);
            ediWriter->write_end_segment();
        }

        inline void writeRff(edifact_writer_t* ediWriter, const char* type, const QString& referenceNo) {
            ediWriter->write_start_segment("RFF");
            ediWriter->write_element(type);
            ediWriter->write_component(referenceNo);
            ediWriter->write_end_segment();
        }

        inline void writeRffBm(edifact_writer_t* ediWriter, const ange::containers::Container* container) {
            if (!container->bookingNumber().isEmpty()) {
                writeRff(ediWriter, "BM", container->bookingNumber());
            } else {
                writeRff(ediWriter, "BM", "1");
            }
        }

        inline void writeTdt(edifact_writer_t* ediWriter, const QString& voyageNumber, const QString& carrierCode,
                             const ange::vessel::Vessel* vessel, const QString& vesselCode) {
            ediWriter->write_start_segment("TDT");
            ediWriter->write_element(20);
            ediWriter->write_element(voyageNumber);
            ediWriter->write_empty_element();
            ediWriter->write_empty_element();
            if(!carrierCode.isEmpty()) {
                ediWriter->write_element(carrierCode);
                ediWriter->write_component(172);
                ediWriter->write_component(20);
            } else {
                ediWriter->write_empty_element();
            }
            ediWriter->write_empty_element();
            ediWriter->write_empty_element();
            if (!vessel->callSign().isEmpty()) {
                ediWriter->write_element(vessel->callSign());
                ediWriter->write_component(103);
                ediWriter->write_component("ZZZ");
            } else if(!vesselCode.isEmpty()) {
                ediWriter->write_element(vesselCode);  // This might be IMO number depending on how the exporter is created
                ediWriter->write_component("ZZZ");
                ediWriter->write_component("ZZZ");
            } else {
                ediWriter->write_empty_element();  // The field is "Required", but common practice appears to be to write nothing
                ediWriter->write_component(103);
                ediWriter->write_component("ZZZ");
            }
            ediWriter->write_component(vessel->name().toUpper());
            ediWriter->write_end_segment();
        }

        inline void writeTdtBaplie15(edifact_writer_t* ediWriter, const QString& voyageNumber, const QString& carrierCode,
                                     const ange::vessel::Vessel* vessel) {
            ediWriter->write_start_segment("TDT");
            ediWriter->write_element(20);
            ediWriter->write_element(voyageNumber);
            ediWriter->write_empty_element();
            ediWriter->write_element(vessel->callSign());
            ediWriter->write_component(103);
            ediWriter->write_empty_component();
            ediWriter->write_component(vessel->name().toUpper());
            ediWriter->write_component(""); // m_vessel->country()?
            ediWriter->write_empty_element();
            if(carrierCode.size() > 0) {
                ediWriter->write_element(carrierCode);
                ediWriter->write_component(172);
                ediWriter->write_component(20);
            } else {
                ediWriter->write_empty_element();
            }
            ediWriter->write_end_segment();
        }

        inline void writeTdtCoprar(edifact_writer_t* ediWriter, const QString& voyageNumber, const QString& carrierCode,
                                   const ange::vessel::Vessel* vessel) {
            ediWriter->write_start_segment("TDT");
            ediWriter->write_element(20);
            ediWriter->write_element(voyageNumber);
            ediWriter->write_element(1);
            ediWriter->write_empty_element();
            if(carrierCode.size() > 0) {
                ediWriter->write_element(carrierCode);
                ediWriter->write_component(172);
                ediWriter->write_component(20);
                ediWriter->write_component(carrierCode);
            } else {
                ediWriter->write_empty_element();
            }
            ediWriter->write_empty_element();
            ediWriter->write_empty_element();
            ediWriter->write_element(vessel->imoNumber());
            ediWriter->write_component(146);
            ediWriter->write_component(11);
            ediWriter->write_component(vessel->name().toUpper());
            // country of vessel unknown - no empty component emitted
            ediWriter->write_end_segment();
        }

        /**
         * Writes the temperature of the given container if it is a live reefer
         */
        inline void writeTmpCel(edifact_writer_t* ediWriter, const double temp) {
            double exportedTemperature;
            if (temp == ange::containers::Container::unknownTemperature()) {
                QSettings settings;
                exportedTemperature = settings.value("user/default_EDI_export_temp", ange::containers::Container::unknownTemperature()).toDouble();
            } else {
                exportedTemperature = temp;
            }
            ediWriter->write_start_segment("TMP");
            ediWriter->write_element(2);
            ediWriter->write_element(exportedTemperature, fabs(exportedTemperature) >= 99.95 ? 0 : 1);
            ediWriter->write_component("CEL");
            ediWriter->write_end_segment();
        }

        inline void writeUnb(edifact_writer_t* ediWriter, int syntaxVersion, const QString& senderId, const QDateTime& now, int interchangeControlReference) {
            QString angelstowVersion((QString("ASTOW ") + ANGESTOW_VERSION).left(15));
            ediWriter->write_start_segment("UNB");
            ediWriter->write_element("UNOA");
            ediWriter->write_component(syntaxVersion);
            if(!senderId.isEmpty()) {
                ediWriter->write_element(senderId);
            } else {
                ediWriter->write_element("SID");
            }
            ediWriter->write_element("RID"); // we do not know recepient ID
            ediWriter->write_element(now.toString("yyMMdd"));
            ediWriter->write_component(now.toString("hhmm"));
            ediWriter->write_element(interchangeControlReference);
            for(int i=0 ; i < 4 ; i++)
                ediWriter->write_empty_element();
            ediWriter->write_element(angelstowVersion); // we do not know shipping line ID - sending Angelstow ID instead of
            ediWriter->write_end_segment();
        }

        inline void writeUnh(edifact_writer_t* ediWriter, int messageReferenceNumber,
                             const char* ediMessageType, const char* ediTypeVersion, const char* ediReleaseNumber, const char* ediSmdgVersion) {
            ediWriter->write_start_segment("UNH");
            ediWriter->write_element(messageReferenceNumber);
            ediWriter->write_element(ediMessageType);
            ediWriter->write_component(ediTypeVersion);
            ediWriter->write_component(ediReleaseNumber);
            ediWriter->write_component("UN");
            ediWriter->write_component(ediSmdgVersion);
            ediWriter->write_end_segment();
        }

        inline void writeUnt(edifact_writer_t* ediWriter, int messageReferenceNumber) {
            ediWriter->write_start_segment("UNT");
            ediWriter->write_element(ediWriter->segment_count());
            ediWriter->write_element(messageReferenceNumber);
            ediWriter->write_end_segment();
        }

        inline void writeUnz(edifact_writer_t* ediWriter, int messageCounter, int interchangeControlReference) {
            ediWriter->write_start_segment("UNZ");
            ediWriter->write_element(messageCounter);
            ediWriter->write_element(interchangeControlReference);
            ediWriter->write_end_segment();
        }

        /**
         * Writes all DGS segments of the container to the edi writer.
         */
        inline void writeAllDgs(edifact_writer_t* ediWriter, const ange::containers::Container* container) {
            Q_FOREACH(const ange::containers::DangerousGoodsCode& dg, container->dangerousGoodsCodes()) {
                writeDgs(ediWriter,dg);
            }
        }

        inline void writeAllDimOog(edifact_writer_t* ediWriter, const ange::containers::Oog& oog,
                                   const QString& abbr_centimeters, int code_for_height) {
            using ange::units::centimeter;
            if (!qFuzzyIsNull(oog.front() / centimeter)) {
                ediWriter->write_start_segment("DIM");
                ediWriter->write_element(5);
                ediWriter->write_element(abbr_centimeters);
                ediWriter->write_component(oog.front() / centimeter);
                ediWriter->write_end_segment();
            }
            if (!qFuzzyIsNull(oog.back() / centimeter)) {
                ediWriter->write_start_segment("DIM");
                ediWriter->write_element(6);
                ediWriter->write_element(abbr_centimeters);
                ediWriter->write_component(oog.back() / centimeter);
                ediWriter->write_end_segment();
            }
            if (!qFuzzyIsNull(oog.right() / centimeter)) {
                ediWriter->write_start_segment("DIM");
                ediWriter->write_element(7);
                ediWriter->write_element(abbr_centimeters);
                ediWriter->write_empty_component();
                ediWriter->write_component(oog.right() / centimeter);
                ediWriter->write_end_segment();
            }
            if (!qFuzzyIsNull(oog.left() / centimeter)) {
                ediWriter->write_start_segment("DIM");
                ediWriter->write_element(8);
                ediWriter->write_element(abbr_centimeters);
                ediWriter->write_empty_component();
                ediWriter->write_component(oog.left() / centimeter);
                ediWriter->write_end_segment();
            }
            if (!qFuzzyIsNull(oog.top() / centimeter)) {
                ediWriter->write_start_segment("DIM");
                ediWriter->write_element(code_for_height);
                ediWriter->write_element(abbr_centimeters);
                ediWriter->write_empty_component();
                ediWriter->write_empty_component();
                ediWriter->write_component(oog.top() / centimeter);
                ediWriter->write_end_segment();
            }
        }

        /**
         * Writes all FTX+HAN handeling segments of the container to the edi writer.
         */
        inline void writeAllFtxHan(edifact_writer_t* ediWriter, const ange::containers::Container* container) {
            Q_FOREACH(const ange::containers::HandlingCode& handelingCode, container->handlingCodes()) {
                writeFtxHan(ediWriter, handelingCode);
            }
        }

    }
}

#endif
