#include "coprar_writer.h"

#include <QtAlgorithms>
#include "edifact_writer.h"
#include <QDateTime>
#include <ange/vessel/vessel.h>
#include "edifact_helper_routines.h"
#include <ange/schedule/call.h>

coprar_writer_t::coprar_writer_t(QIODevice* output, const ange::vessel::Vessel* vessel, const QString& voyage_number, const QString& carrier_code, const ange::schedule::Call* current_call)
 :  m_edi_writer(output),
    m_current_call(current_call),
    m_message_reference_number(qrand()),
    m_started(false),
    m_ended(false),
    m_carrier_code(carrier_code),
    m_voyage_number(voyage_number),
    m_vessel(vessel),
    m_container_counter(0),
    m_interchange_control_reference(qrand())
{
    m_edi_writer.ignore_segment_for_count("UNA");
    m_edi_writer.ignore_segment_for_count("UNB");
    m_edi_writer.ignore_segment_for_count("UNZ");
}

coprar_writer_t::~coprar_writer_t() {
    if(!m_ended) {
        end();
    }
}

void coprar_writer_t::start() {
    if(m_started) {
        qWarning("start already called once; doing nothing");
        return;
    }
    m_started = true;
    QDateTime now = QDateTime::currentDateTime().toUTC();

    ange::edifact_routines::writeUnb(&m_edi_writer, 2, m_carrier_code, now, m_interchange_control_reference);
    ange::edifact_routines::writeUnh(&m_edi_writer, m_message_reference_number, "COPRAR", "D", "00B", "SMDG20");
    ange::edifact_routines::writeBgmCoprar(&m_edi_writer, m_message_reference_number);
    ange::edifact_routines::writeRff(&m_edi_writer, "AAY", m_carrier_code);
    ange::edifact_routines::writeTdtCoprar(&m_edi_writer, m_voyage_number, m_carrier_code, m_vessel);
    ange::edifact_routines::writeRff(&m_edi_writer, "VM", m_vessel->callSign());
    ange::edifact_routines::writeLocPort(&m_edi_writer, 9, m_current_call->uncode());
    ange::edifact_routines::writeNad(&m_edi_writer, "CF", m_carrier_code);
}

void coprar_writer_t::add_container(const ange::containers::Container* container) {
    if(!m_started) {
        start();
    }
    Q_FOREACH(const ange::containers::Container* bundled_container, container->bundleChildren()) {
        add_container(bundled_container);
    }
    m_container_counter++;

    ange::edifact_routines::writeEqdCoprar(&m_edi_writer, container);
    ange::edifact_routines::writeEqn(&m_edi_writer);
    ange::edifact_routines::writeLocPort(&m_edi_writer, 9, container->loadPort());
    ange::edifact_routines::writeLocPort(&m_edi_writer, 11, container->dischargePort());
    if (!container->placeOfDelivery().isEmpty()) {
        ange::edifact_routines::writeLocPort(&m_edi_writer, 7, container->placeOfDelivery());
    }
    ange::edifact_routines::writeMeaKgmCoprar(&m_edi_writer, container);

    if(container->oog()) {
        ange::edifact_routines::writeAllDimOog(&m_edi_writer, container->oog(), "CMT", 13);
    }

    if(container->live()) {
        ange::edifact_routines::writeTmpCel(&m_edi_writer, container->temperature());
    }

    Q_FOREACH(const ange::containers::DangerousGoodsCode& dg, container->dangerousGoodsCodes()) {
        ange::edifact_routines::writeDgs(&m_edi_writer,dg);
    }
    ange::edifact_routines::writeAllFtxHan(&m_edi_writer, container);
}

void coprar_writer_t::end() {
    if(m_ended) {
        qWarning("end already called, doing nothing");
        return;
    }
    if(!m_started) {
        qWarning("header not written, doing nothing");
        return;
    }
    ange::edifact_routines::writeCNT(&m_edi_writer, m_container_counter);
    ange::edifact_routines::writeUnt(&m_edi_writer, m_message_reference_number);
    ange::edifact_routines::writeUnz(&m_edi_writer, 1, m_interchange_control_reference);
    m_ended=true;
}
