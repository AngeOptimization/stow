#include "tanstawriter.h"
#include <QDateTime>
#include <ange/vessel/vessel.h>
#include <ange/vessel/vesseltank.h>
#include <ange/schedule/call.h>

#include "edifact_helper_routines.h"

using namespace ange::units;

TanstaWriter::TanstaWriter(QIODevice* device, const ange::vessel::Vessel* vessel, const ange::schedule::Call* startPort,
                           const ange::schedule::Call* nextPort,
                           const QString& voyageNumber, const QString& carrierCode)
            : m_edi_writer(device), m_vessel(vessel), m_start_port(startPort), m_next_port(nextPort), m_voyage_number(voyageNumber), m_carrier_code(carrierCode),
            m_interchange_control_reference(qrand()),
            m_message_reference_number(qrand()),
            m_message_counter(0) {
    m_edi_writer.ignore_segment_for_count("UNA");
    m_edi_writer.ignore_segment_for_count("UNB");
    m_edi_writer.ignore_segment_for_count("UNZ");
}

void writeTank(edifact_writer_t* ediWriter, const ange::vessel::VesselTank* tank) {
    ediWriter->write_start_segment("LOC");
    ediWriter->write_element("ZZZ");
    ediWriter->write_element(tank->description());
    ediWriter->write_empty_component();
    ediWriter->write_empty_component();
    ediWriter->write_component(tank->description());
    ediWriter->write_end_segment();
}

void writeTankMeaContent(edifact_writer_t* ediWriter, ange::units::Mass mass) {
    ediWriter->write_start_segment("MEA");
    ediWriter->write_element("WT");
    ediWriter->write_empty_element();
    ediWriter->write_element("TNE"); // tonne
    ediWriter->write_component(mass/ton, 1);
    ediWriter->write_end_segment();
}

void writeTankMeaDensity(edifact_writer_t* ediWriter, const ange::vessel::VesselTank* tank) {
    ediWriter->write_start_segment("MEA");
    ediWriter->write_element("DEN");
    ediWriter->write_empty_element();
    ediWriter->write_element("D41"); // tonne pr cubic meter
    Density density = tank->density()*kilogram/meter3;
    ediWriter->write_component(density / (ton/meter3), 3);
    ediWriter->write_end_segment();
}


void TanstaWriter::start() {
    if(m_started) {
        qWarning("start already called once; doing nothing");
        return;
    }
    m_started=true;
    m_message_counter++;
    QDateTime now = QDateTime::currentDateTime().toUTC();

    ange::edifact_routines::writeUnb(&m_edi_writer, 1, m_carrier_code, now, m_interchange_control_reference);
    ange::edifact_routines::writeUnh(&m_edi_writer, m_message_reference_number, "TANSTA", "1", "93A", "SMDG03");
    ange::edifact_routines::writeBgm(&m_edi_writer, m_message_reference_number);
    ange::edifact_routines::writeDtm(&m_edi_writer, 137, now);
    ange::edifact_routines::writeTdt(&m_edi_writer, m_voyage_number, QString() /*empty on purpose*/, m_vessel, m_vessel->imoNumber());
    ange::edifact_routines::writeLocPort(&m_edi_writer, 5, m_start_port->uncode());
    ange::edifact_routines::writeLocPort(&m_edi_writer, 61, m_next_port->uncode());
    ange::edifact_routines::writeDtm(&m_edi_writer, 178, now); //better and more times needed. FIXME
    ange::edifact_routines::writeRff(&m_edi_writer, "VON", m_voyage_number);
}


void TanstaWriter::end() {
    if(m_ended) {
        qWarning("already ended; doing nothing");
        return;
    }
    if(!m_started) {
        qWarning("header has not been written; doing nothing");
        return;
    }
    m_ended=true;
    ange::edifact_routines::writeUnt(&m_edi_writer, m_message_reference_number);
    ange::edifact_routines::writeUnz(&m_edi_writer, m_message_counter, m_interchange_control_reference);
}


void TanstaWriter::addCondition(const ange::vessel::VesselTank* tank, ange::units::Mass mass) {
    writeTank(&m_edi_writer, tank);
    writeTankMeaContent(&m_edi_writer, mass);
    writeTankMeaDensity(&m_edi_writer, tank);
}


TanstaWriter::~TanstaWriter() {

}
