#include "edifact_writer.h"
#include <QIODevice>

class edifact_writer_t::private_t {
public:
    private_t(QIODevice* out,
              char component_sep,
              char element_sep,
              char repetition_sep,
              char release_char,
              char segment_term ) :
              output(out),
              component_separator(component_sep),
              element_separator(element_sep),
              repetition_separator(repetition_sep),
              release_character(release_char),
              segment_terminator(segment_term)
    {
        last_element = no_write;
        segment_count = 0;
    }

    enum last_t {
        no_write,
        start_segment,
        end_segment,
        element,
        component,
        repetition
    };

    QIODevice* output;
    char component_separator;
    char element_separator;
    char repetition_separator;
    char release_character;
    char segment_terminator;
    last_t last_element;
    int segment_count;
    QStringList ignored_segments_for_count;

    /**
     * Handles escaping of special characters and converts to upper case (required by the Level A character set)
     */
    QByteArray escapeAndToUpper(const QString& input) {
        QByteArray quoted;
        const int count = input.count();
        quoted.reserve(count * 2);
        for (int i = 0; i < count; i++) {
            char current = input.at(i).toLatin1();
            if(current == component_separator ||
                current == element_separator ||
                current == repetition_separator ||
                current == release_character ||
                current == segment_terminator ) {
                quoted.append(release_character);
                }
                quoted.append(input.at(i));
        }
        quoted.squeeze();
        return quoted.toUpper();
    }
};



edifact_writer_t::edifact_writer_t(QIODevice* output) : d(new edifact_writer_t::private_t(output,':','+','*','?','\'')) {
}

edifact_writer_t::edifact_writer_t(QIODevice* output,
                                   char component_sep,
                                   char element_sep,
                                   char repetition_sep,
                                   char release_char,
                                   char segment_term) : d(new edifact_writer_t::private_t(output,component_sep,element_sep,repetition_sep,release_char,segment_term))
{
}


edifact_writer_t::~edifact_writer_t() {
}

char edifact_writer_t::component_separator() const {
    return d->component_separator;
}

char edifact_writer_t::element_separator() const {
    return d->element_separator;
}

char edifact_writer_t::release_character() const {
    return d->release_character;
}

char edifact_writer_t::repetition_separator() const {
    return d->repetition_separator;
}

char edifact_writer_t::segment_terminator() const {
    return d->segment_terminator;
}

void edifact_writer_t::write_start_segment(const QString& name) {
    Q_ASSERT(name.size() == 3);
    if(d->last_element != private_t::end_segment && d->last_element != private_t::no_write) {
        write_end_segment();
    }
    if(!d->ignored_segments_for_count.contains(name)) { //TODO? move counting out of the edi writer.
        d->segment_count++;
    }
    d->output->write(d->escapeAndToUpper(name));
    d->last_element=private_t::start_segment;
}

void edifact_writer_t::write_end_segment() {
    d->output->putChar(d->segment_terminator);
    d->output->write("\r\n");
    d->last_element=private_t::end_segment;
}

void edifact_writer_t::write_empty_component() {
    if(d->last_element==private_t::end_segment) {
        Q_ASSERT(false);
        //we cannot have a componenent outside a segment
        return;
    }
    d->output->putChar(d->component_separator);
    d->last_element=private_t::component;
}

void edifact_writer_t::write_empty_element() {
    if(d->last_element==private_t::end_segment) {
        Q_ASSERT(false);
        //we cannot have a element outside a segment
        return;
    }
    d->output->putChar(d->element_separator);
    d->last_element=private_t::element;
}

void edifact_writer_t::write_repetition() {
    Q_ASSERT(false);
    //Unsupported
    d->output->putChar(d->repetition_separator);
    d->last_element=private_t::repetition;
}

void edifact_writer_t::write_component(const QString& data) {
    if(d->last_element==private_t::end_segment) {
        Q_ASSERT(false);
        //we cannot have a component outside a segment
        return;
    }
    if(d->last_element==private_t::start_segment) {
        write_empty_element();
    }
    d->output->putChar(d->component_separator);
    d->output->write(d->escapeAndToUpper(data));
    d->last_element = private_t::component;
}

void edifact_writer_t::write_element(const QString& data) {
    if(d->last_element==private_t::end_segment) {
        Q_ASSERT(false);
        //we cannot have a element outside a segment
        return;
    }
    d->output->putChar(d->element_separator);
    d->output->write(d->escapeAndToUpper(data));
    d->last_element=private_t::element;
}

void edifact_writer_t::write_component(int data) {
    write_component(QString::number(data));
}

void edifact_writer_t::write_element(int data) {
    write_element(QString::number(data));
}

void edifact_writer_t::write_component(double data, int precision) {
    write_component(QString::number(data, 'f', precision));
}

void edifact_writer_t::write_element(double data, int precision) {
    write_element(QString::number(data, 'f', precision));
}

int edifact_writer_t::segment_count() const {
    return d->segment_count;
}

void edifact_writer_t::ignore_segment_for_count(const QString& name) {
    if(!d->ignored_segments_for_count.contains(name)) {
        d->ignored_segments_for_count << name;
    }
}

QStringList edifact_writer_t::ignored_segments_for_count() const {
    return d->ignored_segments_for_count;
}

void edifact_writer_t::unignore_segment_for_count(const QString& name) {
    d->ignored_segments_for_count.removeAll(name);
}



