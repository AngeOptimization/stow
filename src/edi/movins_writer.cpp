#include "movins_writer.h"
#include <QDateTime>
#include <ange/vessel/vessel.h>
#include <ange/schedule/call.h>
#include <ange/containers/container.h>

#include "edifact_helper_routines.h"
#include <ange/schedule/schedule.h>
#include "document/utils/callutils.h"

using ange::schedule::Schedule;

movins_writer_t::movins_writer_t(QIODevice* output,
                                 const ange::vessel::Vessel* vessel,
                                 const Schedule* schedule,
                                 const ange::schedule::Call* current_port,
                                 const QString& vessel_code,
                                 const QString& voyage_number,
                                 const QString& carrier_code)
  : m_edi_writer(output),
    m_vessel(vessel),
    m_schedule(schedule),
    m_current_port(current_port),
    m_vessel_code(vessel_code),
    m_voyage_number(voyage_number),
    m_carrier_code(carrier_code),
    m_interchange_control_reference(qrand()),
    m_message_reference_number(qrand()),
    m_message_counter(0),
    m_started(false),
    m_ended(false),
    m_current_action(None)
{
    m_edi_writer.ignore_segment_for_count("UNA");
    m_edi_writer.ignore_segment_for_count("UNB");
    m_edi_writer.ignore_segment_for_count("UNZ");
}

movins_writer_t::~movins_writer_t() {
    if(!m_ended) {
        end();
    }
}

void movins_writer_t::start() {
    if(m_started) {
        return;
    }
    m_started=true;
    QDateTime now = QDateTime::currentDateTime();
    m_message_counter++;

    ange::edifact_routines::writeUnb(&m_edi_writer, 1, m_carrier_code, now, m_interchange_control_reference);
    ange::edifact_routines::writeUnh(&m_edi_writer, m_message_reference_number, "MOVINS", "D", "95B", "SMDG20");
    ange::edifact_routines::writeBgm(&m_edi_writer, m_message_reference_number);
    ange::edifact_routines::writeDtm(&m_edi_writer, 137, now);

    // Group grp1(M1) : TDT(M1) - LOC(M99) - DTM(M99) - RFF(C1) - FTX(C9)
    // Different from UN EDIFACT D95B spec, where group grp1 is (M3)
    ange::edifact_routines::writeTdt(&m_edi_writer, m_voyage_number, m_carrier_code, m_vessel, m_vessel_code);
    ange::edifact_routines::writeLocPort(&m_edi_writer,5,m_current_port->uncode());
    const ange::schedule::Call* nextCall = m_schedule->next(m_current_port);
    if (nextCall) {
        if(!(nextCall == m_schedule->calls().last())) {
            ange::edifact_routines::writeLocPort(&m_edi_writer, 61, nextCall->uncode());
        }
        const ange::schedule::Call* nextNextCall = m_schedule->next(nextCall);
        if (nextNextCall) {
            Q_FOREACH(const ange::schedule::Call* call, m_schedule->between(nextNextCall, m_schedule->calls().last())){
                ange::edifact_routines::writeLocPort(&m_edi_writer, 92, call->uncode());
            }
        }
    }
    ange::edifact_routines::writeDtm(&m_edi_writer, 132, m_current_port->eta());
    ange::edifact_routines::writeRff(&m_edi_writer, "VON", m_voyage_number);
}

void movins_writer_t::end() {
    if(m_ended) {
        qWarning("end already called, doing nothing");
        return;
    }
    if(!m_started) {
        qWarning("header not written, doing nothing");
        return;
    }
    m_ended=true;
    ange::edifact_routines::writeUnt(&m_edi_writer, m_message_reference_number);
    ange::edifact_routines::writeUnz(&m_edi_writer, m_message_counter, m_interchange_control_reference);
}

void movins_writer_t::writeContainerInformation(const ange::containers::Container* container, const ange::schedule::Call* call,
                                      const ange::vessel::BayRowTier& brt,   // old location in case of restow
                                      const ange::vessel::BayRowTier& brtNew // new location in case of restow, null in case of load and discharge
                                     ) {
    // Group grp3(C9999) : LOC(M1) - GID(C1) - GDS(C1) - FTX(C9) - MEA(C9) - DIM(C9) - TMP(C1) -
    //                     RNG(C1) - LOC(M9) - RFF(M9) - grp4(C3) - grp5(C999)
    // !!! This group layout is from SMDG 2.1.2, it is very different from EDIFACT 95B

    // LOC (M1) PLACE/LOCATION IDENTIFICATION (grp3) - LOC+147 = Place/Location Qualifier: Code "147" (Stowage Cell)
    ange::edifact_routines::writeLocBrt(&m_edi_writer,brt);  // old location for restow

    // GID (C1) GOODS ITEM DETAILS (grp3)
    // not written

    // GDS (C1) GOODS DESCRIPTION (grp3)
    // not written

    // FTX (C9) FREE TEXT (grp3) - FTX+HAN = Handling Instructions
    ange::edifact_routines::writeAllFtxHan(&m_edi_writer, container);

    // MEA (C9) MEASUREMENTS (grp3)
    ange::edifact_routines::writeMeaKgm(&m_edi_writer,container);

    // DIM (C9) DIMENSIONS (grp3)
    if(container->oog()) {
        ange::edifact_routines::writeAllDimOog(&m_edi_writer, container->oog(), "CMT",9);
    }

    // TMP (C1) TEMPERATURE (grp3)
    if(container->live()){
        ange::edifact_routines::writeTmpCel(&m_edi_writer, container->temperature());
    }

    // RNG (C1) RANGE DETAILS (grp3)
    // not written

    // LOC (M9) PLACE/LOCATION IDENTIFICATION (grp3)
    //  "9" = Port of Loading
    // "11" = Port of discharge
    // "13" = Transhipment port
    // "64" = 1st optional port of discharge
    // "68" = 2nd optional port of discharge
    // "70" = 3rd optional port of discharge
    // "80" = Original port of loading
    // "83" = Place of delivery (to be used as final destination)
    // "97" = Optional port of discharge.
    // "152" = Next port of discharge
    if (m_current_action == Load) {
        ange::edifact_routines::writeLocPort(&m_edi_writer, 9, m_current_port->uncode());
        if(!is_after(call)) {
            ange::edifact_routines::writeLocPort(&m_edi_writer, 11, call->uncode());
        } else {
            ange::edifact_routines::writeLocPort(&m_edi_writer, 11, container->dischargePort());
        }
    } else if (m_current_action == Discharge) {
        if(!is_befor(call)) {
            ange::edifact_routines::writeLocPort(&m_edi_writer, 9, call->uncode());
        } else {
            ange::edifact_routines::writeLocPort(&m_edi_writer, 9, container->dischargePort());
        }
        ange::edifact_routines::writeLocPort(&m_edi_writer, 11, m_current_port->uncode());
    } else if (m_current_action == Restow) {
        if(!is_after(call)) {
            ange::edifact_routines::writeLocPort(&m_edi_writer, 11, call->uncode());
        } else {
            ange::edifact_routines::writeLocPort(&m_edi_writer, 11, container->dischargePort());
        }
    }
    if (!container->placeOfDelivery().isEmpty()) {
        ange::edifact_routines::writeLocPort(&m_edi_writer, 83, container->placeOfDelivery());
    }

    // RFF(M9) REFERENCE (grp3)
    ange::edifact_routines::writeRffBm(&m_edi_writer, container);
    if (m_current_action == Restow) {
        QString newLocString = QString("%1%2%3").arg(brtNew.bay(), 3, 10, QChar('0')).arg(brtNew.row(), 2, 10, QChar('0')).arg(brtNew.tier(), 2, 10, QChar('0')); // BBBRRTT
        ange::edifact_routines::writeRff(&m_edi_writer, "DSI", newLocString);
    }

    // Group grp4 (C9) (M1) EQD - EQA - NAD

    // EQD EQUIPMENT DETAILS (grp4)
    ange::edifact_routines::writeEqd(&m_edi_writer, container);

    // EQA (C9) EQUIPMENT ATTACHED (grp4)
    // not written

    // NAD (C1) NAME AND ADDRESS (grp4)
    ange::edifact_routines::writeNad(&m_edi_writer, "CA", container->carrierCode());

    // Group grp5 (C999) DGS - FTX
    //   DGS (M1) DANGEROUS GOODS (grp5)
    //   FTX (C1) FREE TEXT (grp5)
    //     FTX+AAC = Dangerous goods additional information
    //     FTX+AAD = Dangerous goods, technical name, proper shipping name.
    ange::edifact_routines::writeAllDgs(&m_edi_writer, container);
}

void movins_writer_t::load_container(const ange::containers::Container* container, const ange::schedule::Call* destination, const ange::vessel::BayRowTier& brt) {
    if(!m_started) {
        start();
    }
    if(m_ended) {
        qWarning("trailer already written;_doing nothing");
        return;
    }
    Q_FOREACH(const ange::containers::Container* subcontainer, container->bundleChildren()) {
        load_container(subcontainer, destination, brt);
    }

    // Group grp2 (M9999) HAN - grp3

    // HAN Handling instruction (grp2) (M1)
    if(m_current_action != Load) {
        ange::edifact_routines::writeHan(&m_edi_writer, "LOA");
        m_current_action = Load;
    }

    writeContainerInformation(container, destination, brt, brt); // second brt unused in load
}

void movins_writer_t::disch_container(const ange::containers::Container* container, const ange::schedule::Call* source, const ange::vessel::BayRowTier& brt) {
    if(!m_started) {
        start();
    }
    if(m_ended) {
        qWarning("trailer already written;_doing nothing");
        return;
    }
    Q_FOREACH(const ange::containers::Container* subcontainer, container->bundleChildren()) {
        disch_container(subcontainer, source, brt);
    }
    if(m_current_action != Discharge) {
        ange::edifact_routines::writeHan(&m_edi_writer, "DIS");
        m_current_action = Discharge;
    }

    writeContainerInformation(container, source, brt, brt); // second brt unused in discharge
}

void movins_writer_t::restow_container(const ange::containers::Container* container,const ange::schedule::Call* destination,
                                       const ange::vessel::BayRowTier& brtOld, const ange::vessel::BayRowTier& brtNew) {
    if(!m_started) {
        start();
    }
    if(m_ended) {
        qWarning("trailer already written;_doing nothing");
        return;
    }
    Q_FOREACH(const ange::containers::Container* subcontainer, container->bundleChildren()) {
        restow_container(subcontainer,destination, brtOld, brtNew);
    }
    if(m_current_action != Restow) {
        ange::edifact_routines::writeHan(&m_edi_writer, "RES");
        m_current_action = Restow;
    }

    writeContainerInformation(container, destination, brtOld, brtNew);
}
