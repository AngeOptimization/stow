#ifndef MOVINS_WRITER_H
#define MOVINS_WRITER_H

#include "edifact_writer.h"

namespace ange {
  namespace vessel {
    class Vessel;
    class BayRowTier;
  }
  namespace schedule {
    class Call;
  class Schedule;
}
  namespace containers {
    class Container;
  }
}

/**
 * MOVINS writer that writes the SMDG version 2.1.2 of MOVINS
 *
 * Notice that this version of MOVINS is not compliant with EDIFACT MOVINS 95B even though it claims to be.
 *
 * Note that, all load moves should be added, then all dischare moves. Do not mix.
 */
class QIODevice;
class movins_writer_t {
  public:
    movins_writer_t(QIODevice* output, const ange::vessel::Vessel* vessel, const ange::schedule::Schedule* schedule, const ange::schedule::Call* current_port, const QString& vessel_code, const QString& voyage_number, const QString& carrier_code);
    ~movins_writer_t();
    /**
     * Writes the header of the document
     */
    void start();
    /**
     * adds a load move with a given final destination and on board location
     */
    void load_container(const ange::containers::Container* container, const ange::schedule::Call* destination, const ange::vessel::BayRowTier& brt);
    /**
     * adds a discharge move with a origin call and a location on board to be moved from
     */
    void disch_container(const ange::containers::Container* container, const ange::schedule::Call* source, const ange::vessel::BayRowTier& brt);
    /**
     * Adds a restow move with a soruce and a destination.
     * SMDG MOVINS standard demands that a containers with a restow move also gets a load move later in the file,
     * call load_container() on all containers you have called restow_container() on
     */
    void restow_container(const ange::containers::Container* container, const ange::schedule::Call* destination, const ange::vessel::BayRowTier& old_location, const ange::vessel::BayRowTier& new_location);
    /**
     * writes the movins trailer.
     */
    void end();
  private:
    /**
     * meat of writing container information
     * \param container to write
     * \param destination call for container
     * \param brt location for load/discharge. Old location in case of restow
     * \param brtNew new location in case of restow
     */
    void writeContainerInformation(const ange::containers::Container* container, const ange::schedule::Call* destination,
                         const ange::vessel::BayRowTier& brt,   // old location in case of restow
                         const ange::vessel::BayRowTier& brtNew // new location in case of restow, not used in case of load and discharge
                        );
    edifact_writer_t m_edi_writer;
    const ange::vessel::Vessel* m_vessel;
    const ange::schedule::Schedule* m_schedule;
    const ange::schedule::Call* m_current_port;
    QString m_vessel_code;
    QString m_voyage_number;
    QString m_carrier_code;
    int m_interchange_control_reference;
    int m_message_reference_number;
    int m_message_counter;
    bool m_started;
    bool m_ended;
    enum current_action {
      None,
      Restow,
      Load,
      Discharge
    };

    current_action m_current_action;
};

#endif // MOVINS_WRITER_H
