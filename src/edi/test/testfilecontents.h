#ifndef TESTFILECONTENTS_H
#define TESTFILECONTENTS_H

#include <QObject>
#include <QIODevice>
#include <QPointer>

/**
 * \brief compares text lines in iodevices
 *
 * This test class compares the lines in two random access io devices
 * and if requested skips some lines at the top and bottom
 *
 * Note that this class can throw exceptions and in general
 * it is recommended to use the extramacros ERUN functionality to run it
 *
 * For example to be used like:
 *
 * QFile expectedData("path/to/expected/file);
 * QVERIFY(expectedData.open(QIODevice::ReadOnly));
 *
 * QBuffer testData;
 * QVERIFY(testdata.open(QIODevice::ReadWrite));
 * fillBufferWithTestData(&testData);
 * testData.reset();
 *
 * TestFileContents tester;
 * tester.setHeaderLineCount(2);
 * ERUN(tester.setControlData(&expectedData));
 * ERUN(tester.setTestData(&testData));
 * ERUN(tester.compare());
 *
 */

class TestFileContents {
    public:
        TestFileContents();
        /**
         * Sets the header line count to be ignored in both files
         */
        void setHeaderLineCount(int count);
        /**
         * Sets the header line count to be ignored in both files
         */
        void setFooterLineCount(int count);
        /**
         * sets the iodevice to read control data from.
         * The iodevice must be open and ready to read from
         */
        void setControlData(QIODevice* iodevice);
        /**
         * sets the test data.
         * The iodevice must be open and ready to read from
         */
        void setTestData(QIODevice* iodevice);

        /**
         * Compares the files. Note that the iodevices will be read from
         * and their positions changed.
         */
        void compare();
    private:
        QPointer<QIODevice> m_controlData;
        QPointer<QIODevice> m_testData;
        int m_footerCount;
        int m_headerCount;

};

#endif // TESTFILECONTENTS_H
