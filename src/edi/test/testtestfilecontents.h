#ifndef TESTTESTFILECONTENTS_H
#define TESTTESTFILECONTENTS_H

#include <QObject>

/**
 * Tests the TestFileContents class. Note that some tests are
 * QSKIP'ed. They are the ones that are supposed to be failing
 * so consider temporarily comment out those when actually working on
 * the TestFileContents class
 */

class TestTestFileContents : public QObject {
    Q_OBJECT
    private Q_SLOTS:
        void differentContentWithMetadata();
        void sameContentWithMetadata();
        void differentContentNoMetadata();
        void sameContentNoMetadata();
        void oneEmpty();
        void twoEmpty();
};

#endif // TESTTESTFILECONTENTS_H
