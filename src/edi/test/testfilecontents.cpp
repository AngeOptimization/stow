#include "testfilecontents.h"

#include "test/util/extramacros.h"

#include <QtTest/QTest>

TestFileContents::TestFileContents() : m_footerCount(0), m_headerCount(0) {

}



void TestFileContents::setFooterLineCount(int count) {
    m_footerCount = count;
}

void TestFileContents::setHeaderLineCount(int count) {
    m_headerCount = count;
}

void TestFileContents::setControlData(QIODevice* iodevice) {
    m_controlData = iodevice;
    EQVERIFY(!m_controlData.data()->isSequential());
}

void TestFileContents::setTestData(QIODevice* iodevice) {
    m_testData = iodevice;
    EQVERIFY(!m_testData.data()->isSequential());
}

/**
 * reads thru the file from where it is and counts lines and returns to
 * the same position
 */
int lineCount(QIODevice* device) {
    int pos = device->pos();
    int lineCount = 0;
    while(!device->atEnd()) {
        lineCount++;
        QByteArray line = device->readLine();
    }
    device->seek(pos);
    return lineCount;
}



void TestFileContents::compare() {
    EQVERIFY(!m_testData.isNull());
    EQVERIFY(!m_controlData.isNull());
    EQVERIFY(m_testData.data()->isOpen());
    EQVERIFY(m_controlData.data()->isOpen());

    int testDataLineCount = lineCount(m_testData.data());
    int controlDataLineCount = lineCount(m_controlData.data());

    EQCOMPARE(testDataLineCount, controlDataLineCount);

    for(int i = 0 ; i < m_headerCount ; i ++) {
        EQVERIFY(!m_testData.data()->atEnd());
        EQVERIFY(!m_controlData.data()->atEnd());
        m_testData.data()->readLine();
        m_controlData.data()->readLine();
    }

    for(int i = m_headerCount ; i < testDataLineCount-m_footerCount ; i++  ) {
        EQVERIFY(!m_testData.data()->atEnd());
        EQVERIFY(!m_controlData.data()->atEnd());
        QByteArray controldata = m_controlData.data()->readLine();
        QByteArray testdata = m_testData.data()->readLine();
        EQCOMPARE(QString(controldata),QString(testdata));
    }

    for(int i = 0 ; i < m_footerCount ; i ++) {
        EQVERIFY(!m_testData.data()->atEnd());
        EQVERIFY(!m_controlData.data()->atEnd());
        m_testData.data()->readLine();
        m_controlData.data()->readLine();
    }

    EQVERIFY(m_testData.data()->atEnd());
    EQVERIFY(m_controlData.data()->atEnd());
}
