/*
 *
 */
#include "testtestfilecontents.h"
#include "testfilecontents.h"

#include "test/util/extramacros.h"

#include <QtTest/QTest>
#include <QBuffer>
#include <QTextStream>

QTEST_GUILESS_MAIN(TestTestFileContents);

void TestTestFileContents::differentContentWithMetadata() {
    QBuffer test;
    QVERIFY(test.open(QIODevice::ReadWrite));
    {
        QTextStream stream(&test);
        stream << "topmetadataline1 testdata" <<  endl;
        stream << "topmetadataline2 testdata" <<  endl;

        stream << "importantdata line 1" << endl;
        stream << "importantdata line 2" << endl;
        stream << "importantdata broken" << endl;
        stream << "importantdata line 4" << endl;
        stream << "importantdata line 5" << endl;

        stream << "bottommetadataline1 testdata" <<  endl;
        stream.flush();
    }
    test.reset();

    QBuffer control;
    QVERIFY(control.open(QIODevice::ReadWrite));
    {
        QTextStream stream(&control);
        stream << "topmetadataline1 controldata" <<  endl;
        stream << "topmetadataline2 controldata" <<  endl;

        stream << "importantdata line 1" << endl;
        stream << "importantdata line 2" << endl;
        stream << "importantdata line 3" << endl;
        stream << "importantdata line 4" << endl;
        stream << "importantdata line 5" << endl;

        stream << "bottommetadataline1 controldata" <<  endl;
        stream.flush();
    }
    control.reset();

    TestFileContents tester;
    ERUN(tester.setControlData(&control));
    ERUN(tester.setTestData(&test));
    tester.setHeaderLineCount(2);
    tester.setFooterLineCount(1);
#if QT_VERSION >= QT_VERSION_CHECK(5, 3, 0)
    EFAILRUN(tester.compare(), "\"importantdata line 3\\n\" \"importantdata broken\\n\"");
#else
    EFAILRUN(tester.compare(), "importantdata line 3\n importantdata broken\n");
#endif
}

void TestTestFileContents::sameContentWithMetadata() {
    QBuffer test;
    QVERIFY(test.open(QIODevice::ReadWrite));
    {
        QTextStream stream(&test);
        stream << "topmetadataline1 testdata" <<  endl;
        stream << "topmetadataline2 testdata" <<  endl;

        stream << "importantdata line 1" << endl;
        stream << "importantdata line 2" << endl;
        stream << "importantdata line 3" << endl;
        stream << "importantdata line 4" << endl;
        stream << "importantdata line 5" << endl;

        stream << "bottommetadataline1 testdata" <<  endl;
        stream.flush();
    }
    test.reset();

    QBuffer control;
    QVERIFY(control.open(QIODevice::ReadWrite));
    {
        QTextStream stream(&control);
        stream << "topmetadataline1 controldata" <<  endl;
        stream << "topmetadataline2 controldata" <<  endl;

        stream << "importantdata line 1" << endl;
        stream << "importantdata line 2" << endl;
        stream << "importantdata line 3" << endl;
        stream << "importantdata line 4" << endl;
        stream << "importantdata line 5" << endl;

        stream << "bottommetadataline1 controldata" <<  endl;
        stream.flush();
    }
    control.reset();

    TestFileContents tester;
    tester.setControlData(&control);
    tester.setTestData(&test);
    ERUN(tester.setHeaderLineCount(2));
    ERUN(tester.setFooterLineCount(1));
    ERUN(tester.compare());
}

void TestTestFileContents::differentContentNoMetadata() {
    QBuffer test;
    QVERIFY(test.open(QIODevice::ReadWrite));
    {
        QTextStream stream(&test);
        stream << "importantdata line 1" << endl;
        stream << "importantdata line 2" << endl;
        stream << "importantdata broken" << endl;
        stream << "importantdata line 4" << endl;
        stream << "importantdata line 5" << endl;

        stream.flush();
    }
    test.reset();

    QBuffer control;
    QVERIFY(control.open(QIODevice::ReadWrite));
    {
        QTextStream stream(&control);
        stream << "importantdata line 1" << endl;
        stream << "importantdata line 2" << endl;
        stream << "importantdata line 3" << endl;
        stream << "importantdata line 4" << endl;
        stream << "importantdata line 5" << endl;
        stream.flush();
    }
    control.reset();

    TestFileContents tester;
    ERUN(tester.setControlData(&control));
    ERUN(tester.setTestData(&test));
#if QT_VERSION >= QT_VERSION_CHECK(5, 3, 0)
    EFAILRUN(tester.compare(), "\"importantdata line 3\\n\" \"importantdata broken\\n\"");
#else
    EFAILRUN(tester.compare(), "importantdata line 3\n importantdata broken\n");
#endif
}

void TestTestFileContents::sameContentNoMetadata() {
    QBuffer test;
    QVERIFY(test.open(QIODevice::ReadWrite));
    {
        QTextStream stream(&test);
        stream << "importantdata line 1" << endl;
        stream << "importantdata line 2" << endl;
        stream << "importantdata line 3" << endl;
        stream << "importantdata line 4" << endl;
        stream << "importantdata line 5" << endl;

        stream.flush();
    }
    test.reset();

    QBuffer control;
    QVERIFY(control.open(QIODevice::ReadWrite));
    {
        QTextStream stream(&control);
        stream << "importantdata line 1" << endl;
        stream << "importantdata line 2" << endl;
        stream << "importantdata line 3" << endl;
        stream << "importantdata line 4" << endl;
        stream << "importantdata line 5" << endl;
        stream.flush();
    }
    control.reset();

    TestFileContents tester;
    tester.setControlData(&control);
    tester.setTestData(&test);
    ERUN(tester.compare());
}

void TestTestFileContents::oneEmpty() {
    QBuffer emptytest;
    QBuffer control;
    QVERIFY(emptytest.open(QIODevice::ReadWrite));
    QVERIFY(control.open(QIODevice::ReadWrite));
    {
        QTextStream stream(&control);
        stream << "importantdata line 1" << endl;
        stream << "importantdata line 2" << endl;
        stream << "importantdata line 3" << endl;
        stream << "importantdata line 4" << endl;
        stream << "importantdata line 5" << endl;
        stream.flush();
    }
    control.reset();

    TestFileContents contents;
    ERUN(contents.setTestData(&emptytest));
    ERUN(contents.setControlData(&control));

    EFAILRUN(contents.compare() , "0 5");
}

void TestTestFileContents::twoEmpty() {
    QBuffer emptytest;
    QBuffer emptycontrol;
    QVERIFY(emptytest.open(QIODevice::ReadWrite));
    QVERIFY(emptycontrol.open(QIODevice::ReadWrite));

    TestFileContents contents;
    ERUN(contents.setTestData(&emptytest));
    ERUN(contents.setControlData(&emptycontrol));

    ERUN(contents.compare());
}


#include "testtestfilecontents.moc"
