
FIND_PACKAGE(Qt5Test 5.2.0 REQUIRED NO_MODULE)


add_library(testfilecontents STATIC testfilecontents.cpp)

target_link_libraries(testfilecontents Qt5::Test Qt5::Core)

set(TESTTESTFILECONTENTS testtestfilecontents.cpp)

add_executable(testtestfilecontents ${TESTTESTFILECONTENTS})
target_link_libraries(testtestfilecontents testfilecontents Qt5::Core)
add_test(testtestfilecontents testtestfilecontents)
