#ifndef TANSTAWRITER_H
#define TANSTAWRITER_H

#include <QString>
#include "edifact_writer.h"
#include <ange/units/units.h>

namespace ange {
    namespace schedule {
        class Call;
    }
    namespace vessel {
        class VesselTank;
        class Vessel;
    }
} // namespace ange

class QIODevice;
class TanstaWriter {
    public:
        TanstaWriter(QIODevice* device, const ange::vessel::Vessel* vessel, const ange::schedule::Call* startPort, const ange::schedule::Call* nextPort, const QString& voyageNumber, const QString& carrierCode );
        ~TanstaWriter();
        void addCondition(const ange::vessel::VesselTank* tank, ange::units::Mass mass);
        void start();
        void end();
    private:
        edifact_writer_t m_edi_writer;
        const ange::vessel::Vessel* m_vessel;
        const ange::schedule::Call* m_start_port;
        const ange::schedule::Call* m_next_port;
        QString m_voyage_number;
        QString m_carrier_code;
        int m_interchange_control_reference;
        int m_message_reference_number;
        int m_message_counter;
        bool m_started;
        bool m_ended;
};

#endif // TANSTAWRITER_H
