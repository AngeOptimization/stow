#ifndef COPRAR_WRITER_H
#define COPRAR_WRITER_H
#include <QString>

#include "edifact_writer.h"

class QIODevice;
namespace ange {
namespace vessel {
class Vessel;
}

namespace schedule {
class Call;
}

namespace containers {
  class Container;
}
}

class coprar_writer_t {
  public:
    coprar_writer_t(QIODevice* output, const ange::vessel::Vessel* vessel, const QString& voyage_number, const QString& carrier_code, const ange::schedule::Call* current_call);
    ~coprar_writer_t();
    /**
     * writes the header and stuff. Does nothing after first call.
     */
    void start();
    /**
     * adds a container to the baplie with given source and destination and location on ship. Also ensures that start has been run.
     */
    void add_container(const ange::containers::Container* container);
    /**
     * writes the footer of the baplie. The destructor also calls end.
     */
    void end();
  private:
    edifact_writer_t m_edi_writer;
    const ange::schedule::Call* m_current_call;
    int m_message_reference_number;
    bool m_started;
    bool m_ended;
    QString m_carrier_code;
    QString m_voyage_number;
    const ange::vessel::Vessel* m_vessel;
    int m_container_counter;
    int m_interchange_control_reference;

};

#endif // COPRAR_WRITER_H
