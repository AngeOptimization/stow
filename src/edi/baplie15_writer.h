#ifndef BAPLIE15_WRITER_H
#define BAPLIE15_WRITER_H
#include "edifact_writer.h"

namespace ange {
  namespace vessel {
    class Vessel;
    class BayRowTier;
  }
  namespace schedule {
    class Call;
  }
  namespace containers {
    class Container;
  }
}

class QIODevice;
class baplie15_writer_t {
  public:
    baplie15_writer_t(QIODevice* output, const ange::vessel::Vessel* vessel, const ange::schedule::Call* start_port, const ange::schedule::Call* next_port,
                    const QString& vessel_code, const QString& voyage_number, const QString& carrier_code);
    ~baplie15_writer_t();
    /**
     * writes the header and stuff. Does nothing after first call.
     */
    void start();
    /**
     * adds a container to the baplie with given source and destination and location on ship. Also ensures that start has been run.
     */
    void add_container(const ange::containers::Container* container, const ange::schedule::Call* source, const ange::schedule::Call* destination, const ange::vessel::BayRowTier& brt);
    /**
     * writes the footer of the baplie. The destructor also calls end.
     */
    void end();
  private:
    edifact_writer_t m_edi_writer;
    const ange::vessel::Vessel* m_vessel;
    const ange::schedule::Call* m_start_port;
    const ange::schedule::Call* m_next_port;
    QString m_vessel_code;
    QString m_voyage_number;
    QString m_carrier_code;
    int m_interchange_control_reference;
    int m_message_reference_number;
    int m_message_counter;
    bool m_started;
    bool m_ended;
};

#endif // BAPLIE15_WRITER_H
