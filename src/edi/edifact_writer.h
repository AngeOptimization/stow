#ifndef EDIFACT_WRITER_H
#define EDIFACT_WRITER_H
#include <QStringList>

class QIODevice;
class QString;

/**
 * Generic edifact file writer classs
 */
class edifact_writer_t {

  public:
    enum SMDGVersion {
        SMDG15,
        SMDG20,
        SMDG22
    };

    /**
     * Constructor. takes a pointer to a IO device to write to
     * This constructor uses the default characters for separator, terminator and release character.
     */
    edifact_writer_t(QIODevice* output);
    ~edifact_writer_t();
    /**
     * @return the used component separator
     */
    char component_separator() const;
    /**
     * @return the used element_separator
     */
    char element_separator() const;
    /**
     * @return the used repetition separator
     */
    char repetition_separator() const;
    /**
     * @return the used release (escape) character
     */
    char release_character() const;
    /**
     * @return the segment terminattor
     */
    char segment_terminator() const;

    /**
     * Begins a segment with @param name as name.
     * If previous segment isn't ended, this function will call write_end_segment();
     */
    void write_start_segment(const QString& name);
    /**
     * ends current segment (writes segment terminator and newlines)
     */
    void write_end_segment();
    /**
     * Writes a new element with @param data as content
     */
    void write_element(const QString& data);
    void write_element(int data);
    /**
     * writes a new element with @param data as content with @param precision
     */
    void write_element(double data, int precision);
    /**
     * writes a empty element
     */
    void write_empty_element();
    /**
     * Writes a new element with @param data as content
     */
    void write_component(const QString& data);
    void write_component(int data);
    /**
     * writes a new component with @param data as content with @param precision
     */
    void write_component(double data, int precision);
    /**
     * writes empty component
     */
    void write_empty_component();
    /**
     * writes repetition characters
     */
    void write_repetition();
    /**
     * returns the current segment count
     */
    int segment_count() const;
    /**
     * adds a segment to ignore fro the segment count to the list
     */
    void ignore_segment_for_count(const QString& name);
    /**
     * @returns the list of ignored segments for segment count. can be empty.
     */
    QStringList ignored_segments_for_count() const;
    /**
     * removes a segment from the ignore when counting list.
     */
    void unignore_segment_for_count(const QString& name);
  protected:
    /**
     * protected constructor to make it possible to give special characters to use for separators, terminators and release characters
     */
    edifact_writer_t(QIODevice* output, char component_sep, char element_separator, char repetition_separator,
                     const char release_char, char segment_term );

  private:
    class private_t;
    private_t *d;

};

#endif // EDIFACT_WRITER_H
