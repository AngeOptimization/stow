#include "layoutcontroller.h"

#include "delayedrestorer.h"
#include "mainwindow.h"
#include "recapview.h"
#include "ui_main_window.h"
#include "userconfiguration.h"

#include <QInputDialog>
#include <QScopedPointer>
#include <QSettings>

LayoutController::LayoutController(MainWindow* mainWindow, Ui_MainWindow* ui)
  : QObject(mainWindow), m_mainWindow(mainWindow), m_ui(ui),
    m_builtinLayouts(UserConfiguration::newLayoutSettings()), m_savedLayouts(new QSettings())
{
    connect(m_ui->actionSaveLayout, &QAction::triggered, this, &LayoutController::saveLayout);
    updateRestoreMenu(m_builtinLayouts.data(), m_ui->menuRestoreBuiltinLayout);
    updateRestoreMenu(m_savedLayouts.data(), m_ui->menuRestoreLayout);
    updateDeleteMenu(m_savedLayouts.data(), m_ui->menuDeleteLayout);
}

LayoutController::~LayoutController() {
    // Empty
}

QStringList LayoutController::layoutNames(QSettings* settings) {
    settings->beginGroup("layout");
    QStringList names;
    Q_FOREACH (const QString& groupName, settings->childGroups()) {
        settings->beginGroup(groupName);
        names << settings->value("name").toString();
        settings->endGroup();
    }
    settings->endGroup();
    names.sort(Qt::CaseInsensitive);
    return names;
}

void LayoutController::updateRestoreMenu(QSettings* settings, QMenu* menu) {
    menu->clear();
    Q_FOREACH (const QString& name, layoutNames(settings)) {
        QAction* action = menu->addAction(name);
        action->setData(QVariant::fromValue<QSettings*>(settings));
        connect(action, &QAction::triggered, this, &LayoutController::restoreLayout);
    }
    menu->setDisabled(menu->isEmpty());
}

void LayoutController::updateDeleteMenu(QSettings* settings, QMenu* menu) {
    menu->clear();
    Q_FOREACH (const QString& name, layoutNames(settings)) {
        QAction* action = menu->addAction(name);
        action->setData(QVariant::fromValue<QSettings*>(settings));
        connect(action, &QAction::triggered, this, &LayoutController::deleteLayout);
    }
    menu->setDisabled(menu->isEmpty());
}

void LayoutController::saveLayout() {
    QString name = QInputDialog::getText(m_mainWindow, tr("Save Layout"), tr("Layout name:"));
    if (name.isEmpty()) {
        return; // Canceled the dialog or empty name entered
    }
    saveState(name);
    updateRestoreMenu(m_savedLayouts.data(), m_ui->menuRestoreLayout);
    updateDeleteMenu(m_savedLayouts.data(), m_ui->menuDeleteLayout);
}

static QString escapeKey(const QString& name) {
    // Maps: "_" -> "__", "/" -> "_>", "\" -> "_<"
    return QString(name).replace("_", "__").replace("/", "_>").replace("\\", "_<");
}

void LayoutController::saveState(const QString& name) {
    QSettings settings;
    settings.beginGroup("layout");
    settings.beginGroup(escapeKey(name));
    settings.setValue("name", name);
    settings.setValue("mainWindow/geometry", m_mainWindow->saveGeometry());
    settings.setValue("mainWindow/state", m_mainWindow->saveState());
    settings.setValue("allContainers/horizontalHeader/state",
                      m_ui->all_containers_table_view->horizontalHeader()->saveState());
    settings.setValue("selectedContainers/horizontalHeader/state",
                      m_ui->selected_containers_table_view->horizontalHeader()->saveState());
    // Save recaps
    settings.beginGroup("recaps");
    settings.remove("");
    Q_FOREACH (RecapView* recapView, m_mainWindow->recapViews()) {
        QVariant recapState = recapView->saveState();
        if (recapState.toMap().isEmpty()) {
            continue;
        }
        QString name = recapView->objectName().remove("recap_").remove("_view");
        settings.beginGroup(escapeKey(name));
        settings.setValue("name", name);
        settings.setValue("state", recapState);
        settings.endGroup();
    }
    settings.endGroup(); // recaps
    settings.endGroup(); // name
    settings.endGroup(); // layout
}

bool LayoutController::restoreState(const QString& name) {
    return restoreState(name, m_savedLayouts.data());
}

bool LayoutController::restoreBuiltinState(const QString& name) {
    return restoreState(name, m_builtinLayouts.data());
}

bool LayoutController::restoreState(const QString& name, QSettings* settings) {
    settings->beginGroup("layout");
    settings->beginGroup(escapeKey(name));
    if (!settings->contains("name")) {
        return false; // Layout of this name does not exist
    }
    Q_ASSERT(settings->value("name").toString() == name);
    // Close all dock widgets before setting geometry as open dock widgets can force the window too large
    Q_FOREACH (QDockWidget* dockWidget, m_mainWindow->findChildren<QDockWidget*>()) {
        dockWidget->close();
    }
    settings->beginGroup("recaps");
    Q_FOREACH (const QString& groupName, settings->childGroups()) {
        settings->beginGroup(groupName);
        m_mainWindow->add_recap(settings->value("name").toString(), tr("Unnamed"));
        m_mainWindow->recapViews().last()->restoreState(settings->value("state"));
        settings->endGroup();
    }
    settings->endGroup(); // recaps
    if (m_mainWindow->isMaximized()) {
        // Workaround problem where geometry of main window is seen as normal when window is maximised
        // Reproduce problem by restoring saved maximised geometry from menu while window is maximised
        m_mainWindow->showNormal();
    }
    DelayedRestorer::timedRestore(m_mainWindow, settings->value("mainWindow/geometry").toByteArray(),
                                  settings->value("mainWindow/state").toByteArray());
    m_ui->all_containers_table_view->horizontalHeader()->restoreState(
        settings->value("allContainers/horizontalHeader/state").toByteArray());
    m_ui->selected_containers_table_view->horizontalHeader()->restoreState(
        settings->value("selectedContainers/horizontalHeader/state").toByteArray());
    // Given we always start up in micro mode, ensure that macro toolbar is not shown, even if it was shown on close:
    m_ui->macro_stow_toolbar->hide();
    settings->endGroup(); // name
    settings->endGroup(); // layout
    return true;
}

void LayoutController::restoreLayout() {
    QAction* action = qobject_cast<QAction*>(sender());
    Q_ASSERT(action);
    restoreState(action->text(), action->data().value<QSettings*>());
}

void LayoutController::deleteLayout() {
    QAction* action = qobject_cast<QAction*>(sender());
    Q_ASSERT(action);
    QString name = action->text();
    QSettings* settings = action->data().value<QSettings*>();
    settings->beginGroup("layout");
    settings->beginGroup(escapeKey(name));
    if (!settings->contains("name")) {
        Q_ASSERT(false); // Should never delete non-existing layouts
        return; // Layout of this name does not exist
    }
    Q_ASSERT(settings->value("name").toString() == name);
    settings->remove("");
    settings->endGroup(); // name
    settings->endGroup(); // layout
    updateRestoreMenu(m_savedLayouts.data(), m_ui->menuRestoreLayout);
    updateDeleteMenu(m_savedLayouts.data(), m_ui->menuDeleteLayout);
}

#include "layoutcontroller.moc"
