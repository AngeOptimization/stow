/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010

 Copyright: See COPYING file that comes with this distribution

*/

#include "schedule_view_item_delegate.h"

#include <QApplication>
#include <QPainter>
#include <QSpinBox>
#include <QLineEdit>
#include <QItemEditorFactory>

#include "document/schedulemodel.h"
#include <QComboBox>
#include <ange/schedule/cranerules.h>
#include <ange/schedule/twinliftrules.h>
#include <QCheckBox>
#include <QDateTimeEdit>

void schedule_view_item_delegate_t::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const {
  QStyleOptionViewItemV4 myoptions(option);
  initStyleOption(&myoptions, index);
  if (myoptions.state & QStyle::State_Selected) {
    myoptions.font.setBold(true);
    QColor c = myoptions.backgroundBrush.color();
    myoptions.palette.setBrush(QPalette::Highlight, c);
    myoptions.palette.setColor(QPalette::HighlightedText, myoptions.palette.color(QPalette::Text));

  }
  QStyledItemDelegate::paint(painter, myoptions, index);
  painter->setPen(QPen(Qt::black, 2));
  if (myoptions.state & QStyle::State_Selected) {
    painter->drawLine(myoptions.rect.left(), myoptions.rect.top()+1, myoptions.rect.right(), myoptions.rect.top()+1);
    painter->drawLine(myoptions.rect.left(), myoptions.rect.bottom(), myoptions.rect.right(), myoptions.rect.bottom());
  }
}

schedule_view_item_delegate_t::schedule_view_item_delegate_t(QObject* parent): QStyledItemDelegate(parent) {

}

QSize schedule_view_item_delegate_t::sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const {
  QStyleOptionViewItemV4 opt = option;
  opt.font.setBold(true);
  return QStyledItemDelegate::sizeHint(opt, index);
}

QWidget* schedule_view_item_delegate_t::createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const {
  if (qobject_cast<const ScheduleModel*>(index.model())) {
    ScheduleModel::column_t column = static_cast<ScheduleModel::column_t>(index.column());
    switch (column) {
      case ScheduleModel::UNCODE_COL:
      case ScheduleModel::ETA_COL:
      case ScheduleModel::ETD_COL:
        break;
      case ScheduleModel::VOYAGE_COL: {
        QLineEdit* rv = getLineEditEditor(parent);
        rv->setValidator(new QRegExpValidator(QRegExp("[0-9A-Z]{4,}"), parent));
        return rv;
      }
      case ScheduleModel::CRANES_COL: {
        QDoubleSpinBox* rv = new QDoubleSpinBox(parent);
        rv->setFrame(false);
        rv->setMinimum(1.0);
        rv->setMaximum(15.0);
        rv->setDecimals(2);
        return rv;
      }
      case ScheduleModel::HLIMIT_COL: {
        QDoubleSpinBox* rv = new QDoubleSpinBox(parent);
        rv->setFrame(false);
        rv->setMinimum(0.0);
        rv->setMaximum(20.0);
        rv->setDecimals(1);
        return rv;
      }
      case ScheduleModel::CRANE_RULE_COL: {
        QComboBox* rv = new QComboBox(parent);
        rv->addItem(ange::schedule::CraneRules::toString(ange::schedule::CraneRules::CraneRuleFourty)
            ,QVariant::fromValue<ange::schedule::CraneRules::Types>(ange::schedule::CraneRules::CraneRuleFourty));
        rv->addItem(ange::schedule::CraneRules::toString(ange::schedule::CraneRules::CraneRuleEighty)
            ,QVariant::fromValue<ange::schedule::CraneRules::Types>(ange::schedule::CraneRules::CraneRuleEighty));
        rv->addItem(ange::schedule::CraneRules::toString(ange::schedule::CraneRules::CraneRuleFourtySuperBins)
            ,QVariant::fromValue<ange::schedule::CraneRules::Types>(ange::schedule::CraneRules::CraneRuleFourtySuperBins));
        rv->addItem(ange::schedule::CraneRules::toString(ange::schedule::CraneRules::CraneRuleUnknown)
            ,QVariant::fromValue<ange::schedule::CraneRules::Types>(ange::schedule::CraneRules::CraneRuleUnknown));
        QString current_text = index.model()->data(index, Qt::DisplayRole).toString();
        int current_index = rv->findText(current_text);
        rv->setCurrentIndex(current_index);
        return rv;
      }
      case ScheduleModel::TWINLIFT_COL: {
        QComboBox* rv = new QComboBox(parent);
        rv->addItem(ange::schedule::TwinLiftRules::toString(ange::schedule::TwinLiftRules::TwinliftRuleAll)
            ,QVariant::fromValue<ange::schedule::TwinLiftRules::Types>(ange::schedule::TwinLiftRules::TwinliftRuleAll));
        rv->addItem(ange::schedule::TwinLiftRules::toString(ange::schedule::TwinLiftRules::TwinliftRuleNothing)
            ,QVariant::fromValue<ange::schedule::TwinLiftRules::Types>(ange::schedule::TwinLiftRules::TwinliftRuleNothing));
        rv->addItem(ange::schedule::TwinLiftRules::toString(ange::schedule::TwinLiftRules::TwinliftRuleBelow)
            ,QVariant::fromValue<ange::schedule::TwinLiftRules::Types>(ange::schedule::TwinLiftRules::TwinliftRuleBelow));
        rv->addItem(ange::schedule::TwinLiftRules::toString(ange::schedule::TwinLiftRules::TwinliftRuleUnknown)
            ,QVariant::fromValue<ange::schedule::TwinLiftRules::Types>(ange::schedule::TwinLiftRules::TwinliftRuleUnknown));
        QString current_text = index.model()->data(index, Qt::DisplayRole).toString();
        int current_index = rv->findText(current_text);
        rv->setCurrentIndex(current_index);
        return rv;
      }
      case ScheduleModel::PRODUCTIVITY_COL: {
        QDoubleSpinBox* rv = new QDoubleSpinBox(parent);
        rv->setFrame(false);
        rv->setMinimum(0.0);
        rv->setMaximum(150.0);
        rv->setDecimals(1);
        return rv;
      }
      case ScheduleModel::NO_COLUMNS:
        Q_ASSERT(false);
        break;
    }
  }
  return QStyledItemDelegate::createEditor(parent, option, index);
}

void schedule_view_item_delegate_t::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const
{
  if (qobject_cast<const ScheduleModel*>(index.model())) {
    ScheduleModel::column_t column = static_cast<ScheduleModel::column_t>(index.column());
    switch (column) {
      case ScheduleModel::UNCODE_COL:
      case ScheduleModel::ETA_COL:
      case ScheduleModel::ETD_COL:
      case ScheduleModel::VOYAGE_COL:
      case ScheduleModel::CRANES_COL:
      case ScheduleModel::HLIMIT_COL:
      case ScheduleModel::PRODUCTIVITY_COL:
        break;
      case ScheduleModel::TWINLIFT_COL:
      case ScheduleModel::CRANE_RULE_COL: {
        QComboBox* combobox = qobject_cast<QComboBox *>(editor);
        if (!combobox) {
          return;
        }
        int valueindex = combobox->currentIndex();
        QVariant value = combobox->itemData(valueindex,Qt::UserRole);
        model->setData(index, value, Qt::EditRole);
        return;
      }
      case ScheduleModel::NO_COLUMNS:
        Q_ASSERT(false);
        break;
    }
  }
  QStyledItemDelegate::setModelData(editor, model, index);
}

QLineEdit* schedule_view_item_delegate_t::getLineEditEditor(QWidget* parent) const {
  const QItemEditorFactory* factory = itemEditorFactory();
  if (!factory) {
    factory = QItemEditorFactory::defaultFactory();
  }
  QWidget* factory_widget = factory->createEditor(QVariant::String, parent);
  if (QLineEdit* rv = qobject_cast<QLineEdit*>(factory_widget)) {
    return rv;
  }
  delete factory_widget; // Not what we wanted
  QLineEdit* lineedit = new QLineEdit(parent);
  lineedit->setFrame(false);
  return lineedit;
}
