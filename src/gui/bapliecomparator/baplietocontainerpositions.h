#ifndef BAPLIETOCONTAINERPOSITIONS_H
#define BAPLIETOCONTAINERPOSITIONS_H

#include <ange/edifact/edifact_parser.h>
#include "basicbapliecomparator.h"

class BaplieToContainerPositions : public ange::edifact::edifact_parser_t
{
    public:
        BaplieToContainerPositions();
        bool success() const;
        QList<BasicBaplieComparator::ContainerPosition> result();
    protected:
        virtual void handle_container_location(const ange::containers::Container& container, const ange::vessel::BayRowTier& position);
        virtual void handle_doc_type(ange::edifact::edifact_parser_t::doc_type_t doctype);
        virtual void handle_error(int error);
    private:
        bool m_success;
        QList<BasicBaplieComparator::ContainerPosition> m_result;
};

#endif // BAPLIETOCONTAINERPOSITIONS_H
