#ifndef BASICBAPLIECOMPARATOR_H
#define BASICBAPLIECOMPARATOR_H
#include <ange/vessel/bayrowtier.h>
#include <ange/containers/equipmentnumber.h>

namespace ange {
namespace schedule {

class Call;
}

namespace containers {
class Container;
}
}

class document_t;
class BasicBaplieComparator {
    public:
        struct ContainerPosition {
            ange::containers::EquipmentNumber container;
            ange::vessel::BayRowTier brt;
        };
        struct DifferentPosition {
            ange::containers::EquipmentNumber container;
            ange::vessel::BayRowTier baplieposition;
            ange::vessel::BayRowTier documentposition;
        };
        BasicBaplieComparator(document_t* document, const ange::schedule::Call* currentCall);
        void setCompareWithData(QList<ContainerPosition> positions);
        bool isEqual() const;
        QList<ContainerPosition> missingContainers() const;
        QList< ContainerPosition > extraContainers() const;
        QList<DifferentPosition> differentPositions() const;


    private:
        document_t* m_document;
        const ange::schedule::Call* m_currentCall;
        void parse() const; // lol
        mutable bool m_stuffParsed;
        mutable bool m_isEqual;
        QList<ContainerPosition> m_containerPositions;
        mutable QList<ContainerPosition> m_missingContainers;
        mutable QList<ContainerPosition> m_extraContainers;
        mutable QList<DifferentPosition> m_differentPositions;
};
inline bool operator==(const BasicBaplieComparator::DifferentPosition& lhs, const BasicBaplieComparator::DifferentPosition& rhs)
{
    return lhs.container == rhs.container && lhs.baplieposition == rhs.baplieposition && lhs.documentposition == rhs.documentposition;
}

#endif // BASICBAPLIECOMPARATOR_H
