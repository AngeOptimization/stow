#include "baplietocontainerpositions.h"

bool BaplieToContainerPositions::success() const {
    return m_success;
}

BaplieToContainerPositions::BaplieToContainerPositions() : edifact_parser_t(), m_success(true) {

}

void BaplieToContainerPositions::handle_doc_type(ange::edifact::edifact_parser_t::doc_type_t doctype) {
    if(doctype != ange::edifact::edifact_parser_t::BAPLIE) {
        m_success = false;
    }
}

void BaplieToContainerPositions::handle_error(int error) {
    m_success = false;
    ange::edifact::edifact_parser_t::handle_error(error);
}

void BaplieToContainerPositions::handle_container_location(const ange::containers::Container& container, const ange::vessel::BayRowTier& position) {

    BasicBaplieComparator::ContainerPosition p = { container.equipmentNumber(), position };
    m_result << p;
}

QList< BasicBaplieComparator::ContainerPosition > BaplieToContainerPositions::result() {
    return m_result;
}





