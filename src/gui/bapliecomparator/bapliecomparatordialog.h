#ifndef BAPLIECOMPARATORDIALOG_H
#define BAPLIECOMPARATORDIALOG_H

#include <QDialog>
#include "../dialogs/nonmodaldialoghelper.h"
#include "../schedule_view.h"
#include <ange/schedule/call.h>

class document_t;
namespace Ui {
    class BaplieComparatorDialog;
}

class BaplieComparatorDialog : public QDialog {
    Q_OBJECT

    public:
        BaplieComparatorDialog(document_t* document, QWidget* parent = 0, Qt::WindowFlags f = 0);
        void setCurrentCall(const ange::schedule::Call* call) {
            m_currentCall = call;
            connect(call, &QObject::destroyed, this, &QObject::deleteLater);
        }
    private Q_SLOTS:
        void doCompare();
        void openFile();

    private:
        Ui::BaplieComparatorDialog* m_ui;
        document_t* m_document;
        const ange::schedule::Call* m_currentCall;
};

class BaplieComparatorSetupper : public DialogSetupper<BaplieComparatorDialog> {
    public:
        BaplieComparatorSetupper(schedule_view_t* schedule_view) : m_scheduleView(schedule_view) { }
        virtual void setup(BaplieComparatorDialog* dialog) {
            dialog->setCurrentCall(m_scheduleView->current_call());
        }
    private:
        schedule_view_t* m_scheduleView;
};

#endif // BAPLIECOMPARATORDIALOG_H
