#include <QTest>
#include <QObject>
#include <QFile>
#include <document.h>
#include "../basicbapliecomparator.h"
#include "../baplietocontainerpositions.h"
#include <fileopener.h>
#include <ange/schedule/schedule.h>
#include <ange/schedule/call.h>

class BasicComparatorTest : public QObject {
    Q_OBJECT
    private Q_SLOTS:
        void testSimpleSuccesfulComparison();
        void testSimple1differenceComparison();
        void testSimpleEqdifferenceComparison();

};

QList<BasicBaplieComparator::ContainerPosition> parseBaplie(QIODevice* device) {
    BaplieToContainerPositions b;
    b.parse(device);
    return b.result();
}


void BasicComparatorTest::testSimpleSuccesfulComparison() {
    document_t* document = FileOpener::openStoFile(QFINDTESTDATA("comparison/lego-maersk-with-containers.sto")).document;

    QString baplieFile = QFINDTESTDATA("comparison/Lego Maersk Small-BBBBB-0001.BAPLIE.edi");

    QFile f(baplieFile);
    QVERIFY(f.open(QIODevice::ReadOnly));

    QList<BasicBaplieComparator::ContainerPosition> containerPositions = parseBaplie(&f);

    ange::schedule::Call* b = document->schedule()->at(2);
    QCOMPARE(b->uncode(), QLatin1Literal("BBBBB"));
    BasicBaplieComparator c(document, b);

    c.setCompareWithData(containerPositions);

    QVERIFY(c.isEqual());

    delete document;
}

void BasicComparatorTest::testSimple1differenceComparison() {
    document_t* document = FileOpener::openStoFile(QFINDTESTDATA("comparison/lego-maersk-with-containers.sto")).document;

    QString baplieFile = QFINDTESTDATA("comparison/Lego Maersk Small-BBBBB-0001-twolocswapped.BAPLIE.edi");

    QFile f(baplieFile);
    QVERIFY(f.open(QIODevice::ReadOnly));

    QList<BasicBaplieComparator::ContainerPosition> containerPositions = parseBaplie(&f);

    ange::schedule::Call* b = document->schedule()->at(2);
    QCOMPARE(b->uncode(), QLatin1Literal("BBBBB"));
    BasicBaplieComparator c(document, b);

    c.setCompareWithData(containerPositions);

    QVERIFY(!c.isEqual());

    QVERIFY(c.extraContainers().isEmpty());
    QVERIFY(c.missingContainers().isEmpty());

    QCOMPARE(c.differentPositions().size(),2);

    BasicBaplieComparator::DifferentPosition diff1 = { ange::containers::EquipmentNumber("ZZZU0000003"), ange::vessel::BayRowTier(18,01,80), ange::vessel::BayRowTier(2,2,2) };
    BasicBaplieComparator::DifferentPosition diff2 = { ange::containers::EquipmentNumber("ZZZU0000004"), ange::vessel::BayRowTier(2,2,2),ange::vessel::BayRowTier(18,01,80) };

    QVERIFY(c.differentPositions().contains(diff1));
    QVERIFY(c.differentPositions().contains(diff2));

    delete document;
}

void BasicComparatorTest::testSimpleEqdifferenceComparison() {
    document_t* document = FileOpener::openStoFile(QFINDTESTDATA("comparison/lego-maersk-with-containers.sto")).document;

    QString baplieFile = QFINDTESTDATA("comparison/Lego Maersk Small-BBBBB-0001-differenteqnumber.BAPLIE.edi");

    QFile f(baplieFile);
    QVERIFY(f.open(QIODevice::ReadOnly));

    QList<BasicBaplieComparator::ContainerPosition> containerPositions = parseBaplie(&f);

    ange::schedule::Call* b = document->schedule()->at(2);
    QCOMPARE(b->uncode(), QLatin1Literal("BBBBB"));
    BasicBaplieComparator c(document, b);

    c.setCompareWithData(containerPositions);

    QVERIFY(!c.isEqual());

    QVERIFY(c.differentPositions().isEmpty());

    QCOMPARE(c.extraContainers().size(),1);
    QCOMPARE(c.missingContainers().size(),1);

    QCOMPARE(QString(c.extraContainers().at(0).container),QString("ZZZX0000003"));
    QCOMPARE(c.extraContainers().at(0).brt,ange::vessel::BayRowTier(2,2,2));
    QCOMPARE(QString(c.missingContainers().at(0).container),QString("ZZZU0000003"));
    QCOMPARE(c.missingContainers().at(0).brt,ange::vessel::BayRowTier(2,2,2));

    delete document;
}


QTEST_GUILESS_MAIN(BasicComparatorTest);

#include "basicbapliecomparatortest.moc"
