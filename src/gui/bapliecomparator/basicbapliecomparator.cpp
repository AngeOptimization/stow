#include "basicbapliecomparator.h"
#include <document.h>
#include <container_list.h>
#include <stowage.h>
#include <ange/containers/container.h>
#include <ange/vessel/vessel.h>
#include <ange/vessel/slot.h>

BasicBaplieComparator::BasicBaplieComparator(document_t* document, const ange::schedule::Call* currentCall) : m_document(document), m_currentCall(currentCall) , m_stuffParsed(true), m_isEqual(true) {

}

void BasicBaplieComparator::setCompareWithData(QList< BasicBaplieComparator::ContainerPosition > positions) {
    m_stuffParsed = false;
    m_containerPositions = positions;
}

QList< BasicBaplieComparator::DifferentPosition > BasicBaplieComparator::differentPositions() const {
    if(!m_stuffParsed) {
        parse();
    }
    return m_differentPositions;
}

QList< BasicBaplieComparator::ContainerPosition > BasicBaplieComparator::extraContainers() const {
    if(!m_stuffParsed) {
        parse();
    }
    return m_extraContainers;
}

QList< BasicBaplieComparator::ContainerPosition> BasicBaplieComparator::missingContainers() const {
    if(!m_stuffParsed) {
        parse();
    }
    return m_missingContainers;
}

bool BasicBaplieComparator::isEqual() const {
    if(!m_stuffParsed) {
        parse();
    }
    return m_isEqual;
}

void BasicBaplieComparator::parse() const {
    QSet<const ange::containers::Container*> unseenOnboards = m_document->stowage()->get_containers_on_board(m_currentCall);
    Q_FOREACH(const ContainerPosition position, m_containerPositions) {
        const ange::containers::Container* documentContainer = m_document->containers()->get_container_by_equipment_number(position.container);
        if(!documentContainer) {
            m_extraContainers << position;
        } else {
            unseenOnboards.remove(documentContainer);

            ange::vessel::BayRowTier brt = m_document->stowage()->nominal_position(documentContainer, m_currentCall);
            if(brt != position.brt) {
                DifferentPosition p;
                p.container = position.container;
                p.documentposition = brt;
                p.baplieposition = position.brt;
                m_differentPositions << p;
            }
        }
    }

    Q_FOREACH(const ange::containers::Container* container, unseenOnboards) {
        ContainerPosition p;
        p.brt = m_document->stowage()->nominal_position(container, m_currentCall);
        p.container = container->equipmentNumber();
        m_missingContainers << p;
    }

    m_isEqual = m_extraContainers.isEmpty() && m_missingContainers.isEmpty() && m_differentPositions.isEmpty();
    m_stuffParsed=true;
}









