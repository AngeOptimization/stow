#include "bapliecomparatordialog.h"
#include "basicbapliecomparator.h"
#include "baplietocontainerpositions.h"
#include "ui_bapliecomparatordialog.h"
#include <ange/schedule/call.h>
#include <QFileDialog>
#include <QSettings>
#include <QStandardPaths>

BaplieComparatorDialog::BaplieComparatorDialog(document_t* document, QWidget* parent, Qt::WindowFlags f)
    : QDialog(parent,f), m_ui(new Ui::BaplieComparatorDialog()), m_document(document), m_currentCall(0) {
    m_ui->setupUi(this);
    connect(m_ui->compareButton, &QAbstractButton::clicked, this, &BaplieComparatorDialog::doCompare);
    connect(m_ui->openButton, &QAbstractButton::clicked, this, &BaplieComparatorDialog::openFile);
    setAttribute(Qt::WA_DeleteOnClose, true);
    QSettings settings;
    QString baplieCompareDirectory = settings.value("baplieCompareDirectory", QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation)).toString();
    m_ui->filename->setText(baplieCompareDirectory);
}

void BaplieComparatorDialog::doCompare() {
    if(!m_currentCall) {
        qWarning() << Q_FUNC_INFO << "current call not set. something went wrong";
        return;
    }
    if(m_ui->filename->text().isEmpty()) {
        return;
    }
    QString html = QString("<html><h2>Comparing BAPLIE with document in %1</h2>").arg(m_currentCall->uncode());
    QFile f(m_ui->filename->text());
    if(f.open(QIODevice::ReadOnly)) {
        BaplieToContainerPositions containerpos;
        try {
            containerpos.parse(&f);
        } catch (std::exception& err) {
            html += "<p>";
            html += err.what();
            html += "</p>";
        }
        if(containerpos.success()) {
            BasicBaplieComparator comparator(m_document, m_currentCall);
            comparator.setCompareWithData(containerpos.result());

            if(comparator.isEqual()) {
                html += "<p>No changes</p>";

            } else {
                html += "<p>Differences found</p>";

                if(!comparator.missingContainers().isEmpty()) {
                    html += "<h3>Containers on board not in BAPLIE</h3>";
                    html += "<table border='1' cellpadding='3' cellspacing='0'>";
                    html += "<tr><th>Equipment Id</th><th>Position</th></tr>";
                    Q_FOREACH(const BasicBaplieComparator::ContainerPosition& position, comparator.missingContainers()) {
                        html += "<tr>";
                        html += "<td>";
                        html += position.container;
                        html += "</td>";
                        html += "<td>";
                        html += position.brt.toString();
                        html += "</td>";
                        html += "</tr>";
                    }
                    html += "</table>";
                }
                if(!comparator.extraContainers().isEmpty()) {
                    html += "<h3>Containers in BAPLIE not found in document</h3>";
                    html += "<table border='1' cellpadding='3' cellspacing='0'>";
                    html += "<tr><th>Equipment Id</th><th>Position</th></tr>";
                    Q_FOREACH(const BasicBaplieComparator::ContainerPosition& position, comparator.extraContainers()) {
                        html += "<tr>";
                        html += "<td>";
                        html += position.container;
                        html += "</td>";
                        html += "<td>";
                        html += position.brt.toString();
                        html += "</td>";
                        html += "</tr>";
                    }
                    html += "</table>";
                }
                if(!comparator.differentPositions().isEmpty()) {
                    html += "<h3>Containers in BAPLIE found in document at a different position</h3>";
                    html += "<table border='1' cellpadding='3' cellspacing='0'>";
                    html += "<tr><th>Equipment Id</th><th>Document</th><th>Baplie</th></tr>";
                    Q_FOREACH(const BasicBaplieComparator::DifferentPosition& position, comparator.differentPositions()) {
                        html += "<tr>";
                        html += "<td>";
                        html += position.container;
                        html += "</td>";
                        html += "<td>";
                        html += position.documentposition.toString();
                        html += "</td>";
                        html += "<td>";
                        html += position.baplieposition.toString();
                        html += "</td>";
                        html += "</tr>";
                    }
                    html += "</table>";
                }
            }
        } else {
            html += "<p>Parsing edifact file failed</p>";
        }
    } else {
        html += "<p>Failed to open file</p>";
    }
    html +="</html>";
    m_ui->resultbox->setHtml(html);
}

void BaplieComparatorDialog::openFile() {
  QString filename = QFileDialog::getOpenFileName(this,
                                                  tr("Open BAPLIE file"),
                                                    m_ui->filename->text(),
                                                  tr("All files (*.*)")
                                                  );
  if(!filename.isEmpty()){
    m_ui->filename->setText(filename);
    QSettings settings;
    QFileInfo fi(filename);
    settings.setValue("baplieCompareDirectory",fi.absoluteDir().absolutePath());
  }
}

#include "bapliecomparatordialog.moc"
