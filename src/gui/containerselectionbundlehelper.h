#ifndef SELECTEDCONTAINERSTOBUNDLESINFO_H
#define SELECTEDCONTAINERSTOBUNDLESINFO_H

#include <QObject>
#include <QPointer>

/**
 * \brief helper utility for bundling and unbundling the container
 *
 * this class offer tests wether or not selection can be bundled or not
 * also offers QActions, that when triggered() can actually do the bundling
 * and unbundling
 */

namespace ange {
namespace schedule {
class Call;
}
}

class document_t;
class QAction;
class pool_t;
class ContainerSelectionBundleHelper : public QObject {
    Q_OBJECT
    public:
        ~ContainerSelectionBundleHelper();
        ContainerSelectionBundleHelper(pool_t* selection_pool, document_t* parent);

        /**
         * \return number of bundles in the selection (for unbundling)
         */
        int bundleCount();

        /**
         * \return action, when triggered() doing the actual bundling
         */
        QAction* bundleAction();

        /**
         * \return action, when triggered() doing the actual unbundling
         */
        QAction* unbundleAction();

        /**
         * sets current call to \param call
         */
        void setCurrentCall(const ange::schedule::Call* call);

    private Q_SLOTS:
        /**
         * sets the internal state of the calculaniots to 'dirty' and
         * will be recalculated on next call to some of the functions
         */
        void setDirty();

        void bundleSelectedContainers();

        void unbundleSelectedContainers();
    private:
        /**
         * \return true if selection can be bundled
         */
        bool selectionCanBeBundled();

        /**
         * \return true if selection can be unbundled
         */
        bool selectionCanBeUnbundled();
        int m_bundleCount;
        bool m_selectionCanBeUnbundled;
        bool m_selectionCanBeBundled;
        pool_t* m_pool;
        document_t* m_document;
        bool m_canBeBundledDirty;
        bool m_canBeUnbundledDirty;
        bool recalculateCanBeBundled();
        bool recalculateCanBeUnbundled();
        QPointer<QAction> m_bundleAction;
        QPointer<QAction> m_unbundleAction;
        QString m_unbundleReason;
        QString m_bundleReason;
        const ange::schedule::Call* m_currentCall;
};

#endif // SELECTEDCONTAINERSTOBUNDLESINFO_H
