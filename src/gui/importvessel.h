#ifndef IMPORTVESSEL_H
#define IMPORTVESSEL_H

#include <QByteArray>

class document_t;
class QWidget;
class QFile;
namespace ange {
namespace vessel {
class Vessel;
}
}

// These are a couple of loose functions, they might as well have been included in mainwindow.cpp
// except that file is already too large is it is.

struct VesselParseResult {
    const ange::vessel::Vessel* m_vessel;
    QByteArray m_vesselFileData;
};

/**
 * @return the Vessel* read from file @param fileName
 */
VesselParseResult readVessel(const QString& fileName);

/**
 * Helper function that asks the user for a vessel to import, and then does the actual import
 * @param document document to add vessel to
 * @param parentWidget widget to use as parent for dialogs
 */
void importVessel(document_t* document, QWidget* parentWidget);

#endif // IMPORTVESSEL_H
