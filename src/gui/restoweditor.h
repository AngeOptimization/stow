#ifndef RESTOWEDITOR_H
#define RESTOWEDITOR_H

#include <QObject>

class ContainerLeg;
class document_t;
class pool_t;
class schedule_view_t;
namespace ange {
namespace containers {
class Container;
}
namespace schedule {
class Call;
}
}

/**
 * Handles changes to restows in the stowage
 */
class RestowEditor : public QObject {

    Q_OBJECT

public:

    /**
     * Constructor
     */
    RestowEditor(QObject* parent);

    /**
     * Set scheduleView for keeping track of current call
     */
    void setScheduleView(schedule_view_t* scheduleView);

    /**
     * Set document and selection pool
     */
    void documentReset(document_t* document, pool_t* pool);

public Q_SIGNAL:

    /**
     * Cancel restow of all selected containers in current call.
     * A restow is a load move that is preceded by a load move.
     * Will not change selection.
     */
    void restowCancel();

private:

    /**
     * Check if call is in ]source;destination[ (both end points exclusive)
     */
    bool callIsInsideContainerSchedule(const ange::containers::Container* container, const ange::schedule::Call* call);

private:

    schedule_view_t* m_scheduleView;
    document_t* m_document;
    pool_t* m_pool;
    ContainerLeg* m_containerLegs;

};

#endif // RESTOWEDITOR_H
