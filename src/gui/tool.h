/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010

 Copyright: See COPYING file that comes with this distribution

*/

#ifndef TOOL_H
#define TOOL_H

#include <QString>
#include <QObject>
#include <QDebug>
#include <QKeySequence>
#include "gui/mode.h"
#include <QPoint>

class QAction;
class QCursor;
class QIcon;

/**
 * Class representing a tool in the vessel view (e.g. erase, fill, select, ...)
 */
class tool_t : public QObject {
  Q_OBJECT
  public:
    /**
     * Construct tool
     * @param name Used in menus, actions, toolbars etc.
     * @param icon_resource used to construct icons and pointers
     * @param area_tool if true, this tool will work across an area (with rubber band-like graphics)
     */
    tool_t(QString name,
           QString icon_resource,
           bool area_tool,
           QPoint hot_spot,
           mode_enum_t::mode_t mode = mode_enum_t::MICRO_MODE,
           const QKeySequence& shortcut = QKeySequence(),
           QObject* parent = 0L);

    ~tool_t();
    /**
     * @return cursor for tool
     */
    const QCursor cursor(Qt::KeyboardModifiers modifiers) const;

    /**
     * sets the cursor icon
     */
    void add_cursor_icon(const int modifier_key, const QString icon_resource, const QPoint& hot_spot);

    /**
     * @return name for tool
     */
    QString name() const {
      return m_name;
    }

    /**
     * @return true if area tool
     */
    bool area_tool() const {
      return m_area_tool;
    }

    /**
     * @return the mode this tool is good for
     */
    mode_enum_t::mode_t mode() const {
      return m_mode;
    }

    /**
     * @return action for tool
     */
    QAction* action() const {
      return m_action;
    }

  private:
    QString m_name;
    /**
     * hash mapping Qt keyboard modifiers to cursor components
     */
    QHash<int, QCursor* > m_cursors;
    bool m_area_tool;
    mode_enum_t::mode_t m_mode;
    QAction* m_action;

};

QDebug operator<<(QDebug dbg, const tool_t* tool);
QDebug operator<<(QDebug dbg, const tool_t& tool);

#endif // TOOL_H
