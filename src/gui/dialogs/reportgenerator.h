#ifndef GENERATEREPORTS_H
#define GENERATEREPORTS_H

#include <QDialog>
#include <QValidator>
#include <document/document.h>

class QCheckBox;
class pool_t;
namespace Ui
{
class ReportGenerator;
}

class ReportGenerator : public QDialog {
    Q_OBJECT
    public:
        ReportGenerator(document_t* document, pool_t* selectionPool, const ange::schedule::Call* currentCall, QWidget* parent);
        virtual void accept();
        virtual ~ReportGenerator();
    private Q_SLOTS:
        // directory and file handling
        void openDir();
        void changeReportName(const QString& newName);

        // reports
        void previewDischScanPlan();

        void previewDepartureReport();

        void previewImoList();

        void previewLoadScanPlan();

        void previewOogList();

        void previewReeferList();

        void previewSpecialsReport();

        void previewRestowList();

        void previewBwLoadPlan();

        void previewBwDischargePlan();

        void previewBwDetailedScanPlan();

        void previewDetailedScanPlan();

        void previewStabilityReport();

        /**
         * Update checkBoxAllReports according to the state of all the report check boxes
         */
        void updateCheckBoxAllReports();

        /**
         * Update all the report check boxes based on the value of checkBoxAllReports
         */
        void updateAllReportCheckBoxes();

    private:
        QList<QCheckBox*> allReportCheckBoxes();

    private:
        Ui::ReportGenerator* ui;
        document_t* m_document;
        pool_t* m_selectionPool;
        const ange::schedule::Call* m_currentCall;
        QString m_basename;
        const ange::schedule::Call* m_previousCall;
        QList< const ange::containers::Container* > containersForScope();
        QRegExpValidator m_nameValidator;
};

#endif // GENERATEREPORTS_H
