#ifndef GENERATECONTAINERSDIALOG_H
#define GENERATECONTAINERSDIALOG_H

#include <QDialog>
#include "generatecontainersmodel.h"
#include "scheduledcontainer.h"

namespace Ui
{
class GenerateContainersDialog;
}

namespace ange {
namespace schedule {
class Call;
class Schedule;
}
namespace containers {
class Container;
}
}

class ScheduleModel;

class GenerateContainersDialog : public QDialog {
    Q_OBJECT
public:

    GenerateContainersDialog(ange::schedule::Schedule* schedule, const ange::schedule::Call* currentCall,
                             ScheduleModel* scheduleModel, QWidget* parent);

    QList<ange::angelstow::ScheduledContainer> containers();

private:
    Ui::GenerateContainersDialog* m_ui;
    QScopedPointer<GenerateContainersModel> m_model;
};

#endif // GENERATECONTAINERSDIALOG_H
