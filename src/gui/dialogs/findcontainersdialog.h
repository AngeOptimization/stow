#ifndef FIND_CONTAINERS_DIALOG_H
#define FIND_CONTAINERS_DIALOG_H

#include <QDialog>

namespace Ui {
class FindContainersDialog;
}
class pool_t;
class document_t;

class FindContainersDialog : public QDialog
{
    Q_OBJECT
public:
    explicit FindContainersDialog(document_t* document, pool_t* selectionPool, QWidget* parent = 0, Qt::WindowFlags f = 0);
private Q_SLOTS:
    /**
     * adds the containers defined in equipmentIdTextEdit to m_selectionPool
     */
    void selectionFromPattern();
    void done();
private:
    Ui::FindContainersDialog* m_ui;
    const document_t* m_document;
    pool_t* m_selectionPool;
};

#endif // FIND_CONTAINERS_DIALOG_H
