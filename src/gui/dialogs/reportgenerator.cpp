#include "reportgenerator.h"
#include "reporthelpers/scanplanprinter.h"
#include "reporthelpers/departurereport.h"
#include "reporthelpers/imolistexporter.h"
#include "reporthelpers/exportrestowlist.h"
#include "reporthelpers/ooglistexporter.h"
#include "reporthelpers/reeferlistexporter.h"
#include "reporthelpers/specialcontainerslistexporter.h"
#include "reporthelpers/onebayperpagescanplanprinter.h"
#include "reporthelpers/stabilityreport.h"
#include <gui/utils/override_cursor_handler.h>
#include <gui/utils/dialogutils.h>
#include <document/exporters/baplie_exporter.h>
#include <document/exporters/baplie15_exporter.h>
#include <document/exporters/movins_exporter.h>
#include <document/exporters/coprar_exporter.h>
#include <tanstaexporter.h>
#include <document/containers/container_list.h>
#include <document/pools/pool.h>
#include "ui_report_generator.h"
#include <ange/vessel/vessel.h>
#include <ange/containers/container.h>
#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>
#include <QFile>
#include <QDir>
#include <QSettings>
#include <QDesktopServices>
#include <QFileDialog>

using ange::containers::Container;
using ange::schedule::Call;

ReportGenerator::ReportGenerator(document_t* document, pool_t* selectionPool,
                                 const Call* currentCall, QWidget* parent):
                                 QDialog(parent), ui(new Ui::ReportGenerator),
                                 m_document(document), m_selectionPool(selectionPool),
                                 m_currentCall(currentCall),
                                 m_nameValidator(QRegExp("[^\\\\/]*")) {
    ui->setupUi(this);
    ui->errorLabel->setText("");
    setWindowModality(Qt::WindowModal);
    m_basename = QString("%1-%2-%3").arg(m_document->vessel()->name())
                                    .arg(currentCall->uncode()).arg(currentCall->voyageCode());
    QSettings settings;
    QString reportsDirectory = settings.value("reportsDirectory", QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation)).toString();
    ui->editDirectory->setText(reportsDirectory);
    m_basename.replace("\\","-");
    m_basename.replace("/","-");
    ui->editName->setText("report-"+m_basename);
    ui->editName->setValidator(&m_nameValidator);
    connect(ui->editName, SIGNAL(textChanged(QString)), this, SLOT(changeReportName(const QString&)));
    connect(ui->button_file_name,SIGNAL(clicked(bool)),this,SLOT(openDir()));
    connect(ui->buttonImoPreview,SIGNAL(clicked(bool)),SLOT(previewImoList()));
    connect(ui->buttonRestowPreview,SIGNAL(clicked(bool)),SLOT(previewRestowList()));
    connect(ui->loadScanPreview,SIGNAL(clicked(bool)),SLOT(previewLoadScanPlan()));
    connect(ui->dischPlanPreview,SIGNAL(clicked(bool)),SLOT(previewDischScanPlan()));
    connect(ui->buttonDepartureReportPreview,SIGNAL(clicked(bool)),SLOT(previewDepartureReport()));
    connect(ui->buttonOogPreview,SIGNAL(clicked(bool)),SLOT(previewOogList()));
    connect(ui->buttonReeferPreview, SIGNAL(clicked(bool)), SLOT(previewReeferList()));
    connect(ui->buttonSpecialsPreview, SIGNAL(clicked(bool)), SLOT(previewSpecialsReport()));
    connect(ui->buttonBwLoadPlanPreview, SIGNAL(clicked(bool)), SLOT(previewBwLoadPlan()));
    connect(ui->buttonBwDischargePlanPreview, SIGNAL(clicked(bool)), SLOT(previewBwDischargePlan()));
    connect(ui->buttonDetailedBWScanplan, SIGNAL(clicked(bool)), SLOT(previewBwDetailedScanPlan()));
    connect(ui->buttonDetailedScanplan, SIGNAL(clicked(bool)), SLOT(previewDetailedScanPlan()));
    connect(ui->buttonStabilityReportPreview, SIGNAL(clicked(bool)), SLOT(previewStabilityReport()));
    m_previousCall = document->schedule()->previous(m_currentCall);
    if(!m_previousCall) {
        ui->dischPlanPreview->setEnabled(false);
        ui->dischScanPlan->setEnabled(false);
    }
    ui->edit_vesselcode->setText(document->vessel()->name());
    ui->edit_voyagenumber->setText(m_currentCall->voyageCode());
    connect(ui->checkBoxAllReports, SIGNAL(clicked(bool)), SLOT(updateAllReportCheckBoxes()));
    Q_FOREACH (QCheckBox* checkBox, allReportCheckBoxes()) {
        connect(checkBox, SIGNAL(clicked(bool)), SLOT(updateCheckBoxAllReports()));
    }
    DialogUtils::makeDialogMaximizable(this);
}

ReportGenerator::~ReportGenerator() {
    delete ui;
}

void ReportGenerator::previewDepartureReport() {
    DepartureReport departureReport(m_document, m_currentCall);
    departureReport.viewHtml();
}

static PrintMode dischargeScanPlan() {
    PrintMode mode = PrintMode::printing();
    mode.grayNotMovedInNextCall = true;
    mode.scanPlanType = mode_enum_t::DISCHARGE;
    return mode;
}

void ReportGenerator::previewDischScanPlan() {
    ScanplanPrinter printer("Discharge Scanplan", m_document, m_selectionPool, m_previousCall, dischargeScanPlan());
    printer.print_preview();
}

void ReportGenerator::previewImoList() {
    ImolistExporter e(m_document, m_currentCall);
    e.viewHtml();
}

static PrintMode loadScanPlan() {
    PrintMode mode = PrintMode::printing();
    mode.grayRemainOnboard = true;
    return mode;
}

void ReportGenerator::previewLoadScanPlan() {
    ScanplanPrinter printer("Load Scanplan", m_document, m_selectionPool, m_currentCall, loadScanPlan());
    printer.print_preview();
}

void ReportGenerator::previewDetailedScanPlan() {
    PrintMode mode;
    mode.highDetailPrint = true;
    OneBayPerPageScanPlanPrinter printer(m_document, m_selectionPool, m_currentCall, mode);
    printer.print_preview();
}


static PrintMode bwLoadPlanPort() {
    PrintMode mode = PrintMode::printing();
    mode.grayRemainOnboard = true;
    mode.whiteContainers = true;
    mode.displayDischargeCallLetter = true;
    return mode;
}

static PrintMode bwLoadPlanWeight() {
    PrintMode mode = bwLoadPlanPort();
    mode.displayDischargeCallLetter = false;
    mode.displayContainerWeight = true;
    return mode;
}

void ReportGenerator::previewBwLoadPlan() {
    ScanplanPrinter printer("Load Scanplan", m_document, m_selectionPool, m_currentCall, bwLoadPlanPort());
    printer.print_preview();
    ScanplanPrinter printer2("Load Scanplan", m_document, m_selectionPool, m_currentCall, bwLoadPlanWeight());
    printer2.print_preview();
}

static PrintMode detailedBwPlan() {
    PrintMode mode;
    mode.whiteContainers=true;
    mode.displayDischargeCallLetter=true;
    mode.displayContainerWeight=false;
    mode.notPrinting=false;
    return mode;
}

void ReportGenerator::previewBwDetailedScanPlan() {
    PrintMode mode = detailedBwPlan();
    mode.highDetailPrint = true;
    OneBayPerPageScanPlanPrinter printer(m_document, m_selectionPool, m_currentCall, mode);
    printer.print_preview();
}

static PrintMode bwDischargePlan() {
    PrintMode mode = PrintMode::printing();
    mode.grayNotMovedInNextCall = true;
    mode.scanPlanType = mode_enum_t::DISCHARGE;
    mode.whiteContainers = true;
    mode.displayContainerWeight = true;
    return mode;
}

void ReportGenerator::previewBwDischargePlan() {
    PrintMode mode = bwDischargePlan();
    ScanplanPrinter printer("Discharge Scanplan", m_document, m_selectionPool, m_previousCall, mode);
    printer.print_preview();
}

void ReportGenerator::previewOogList() {
    OogListExporter e(m_document, m_currentCall);
    e.viewHtml();
}

void ReportGenerator::previewReeferList() {
    ReeferListExporter e(m_document, m_currentCall);
    e.viewHtml();
}
void ReportGenerator::previewSpecialsReport() {
    SpecialContainersListExporter e(m_document, m_currentCall);
    e.viewHtml();
}

void ReportGenerator::previewRestowList() {
    ExportRestowList e(m_document, m_currentCall);
    e.viewHtml();
}

void ReportGenerator::previewStabilityReport() {
    StabilityReport s(m_document, m_currentCall);
    s.viewHtml();
}

void ReportGenerator::accept() {
    override_cursor_handler_t  overridecursor(qApp,Qt::WaitCursor);
    QDir basedir(ui->editDirectory->text());
    if(!basedir.mkpath(ui->editName->text())) {
        ui->errorLabel->setText("creating directory failed.");
        //error;
        return;
    }
    basedir.cd(ui->editName->text());
    QStringList errorMessages;
    QSettings settings;
    QString carrierCode = settings.value("user/carrier code").toString();

    if(ui->baplie->isChecked()) {
        qDebug() << "trying to do baplie";
        QFile bapliefile(basedir.filePath(m_basename + ".BAPLIE.edi"));
        if(bapliefile.open(QIODevice::Truncate | QIODevice::WriteOnly)) {
            baplie_exporter_t exporter(m_document, ui->edit_voyagenumber->text(), carrierCode, m_document->vessel()->imoNumber(), m_currentCall);
            exporter.write(&bapliefile,containersForScope());
        } else {
            errorMessages << "Writing baplie failed";
        }
    }
    if(ui->baplie15->isChecked()) {
        QFile baplie15file(basedir.filePath(m_basename + ".BAPLIE15.edi"));
        if(baplie15file.open(QIODevice::Truncate | QIODevice::WriteOnly)) {
            baplie15_exporter_t exporter(m_document, ui->edit_voyagenumber->text(), carrierCode, m_document->vessel()->imoNumber(), m_currentCall);
            exporter.write(&baplie15file,containersForScope());
        } else {
            errorMessages << "Writing baplie15 failed";
        }
    }
    if(ui->movins->isChecked()) {
        QFile movinsfile(basedir.filePath(m_basename + ".MOVINS.edi"));
        if(movinsfile.open(QIODevice::Truncate | QIODevice::WriteOnly)) {
            movins_exporter_t exporter(m_document, ui->edit_voyagenumber->text(), carrierCode, m_document->vessel()->imoNumber(), m_currentCall);
            exporter.write(&movinsfile,containersForScope());
        } else {
            errorMessages << "Writing movins failed";
        }
    }
    if(ui->coprar->isChecked()) {
        QFile coprarfile(basedir.filePath(m_basename + ".COPRAR.edi"));
        if(coprarfile.open(QIODevice::Truncate | QIODevice::WriteOnly)) {
            coprar_exporter_t exporter(m_document, ui->edit_voyagenumber->text(), carrierCode, m_currentCall);
            exporter.write(&coprarfile,containersForScope());
        } else {
            errorMessages << "Writing coprar failed";
        }
    }
    if(ui->tansta->isChecked()) {
        QFile tanstafile(basedir.filePath(m_basename + ".TANSTA.edi"));
        if(tanstafile.open(QIODevice::Truncate | QIODevice::WriteOnly)) {
            TanstaExporter exporter(m_document, ui->edit_voyagenumber->text(), carrierCode, m_currentCall);
            exporter.write(&tanstafile);
        } else {
            errorMessages << "Writing tansta failed";
        }

    }
    if(ui->loadScanPlan->isChecked()) {
        QString pdf = basedir.filePath(m_basename+".loadplan.pdf");
        ScanplanPrinter scanplanPrinter("Load Scanplan", m_document, m_selectionPool, m_currentCall, loadScanPlan());
        if(!scanplanPrinter.writePdf(pdf)) {
            errorMessages << "Writing load scan plan failed";
        }
    }
    if(ui->detailedScanPlan->isChecked()) {
        QString pdf = basedir.filePath(m_basename+".detailedloadplan.pdf");
        OneBayPerPageScanPlanPrinter printer(m_document, m_selectionPool, m_currentCall, PrintMode());
        if(!printer.writePdf(pdf)) {
            errorMessages << "Writing detailed scanplan failed";
        }
    }
    if(m_previousCall && ui->dischScanPlan->isChecked()) {
        QString pdf = basedir.filePath(m_basename+".dischplan.pdf");
        ScanplanPrinter scanplanPrinter("Discharge Scanplan", m_document, m_selectionPool, m_previousCall, dischargeScanPlan());
        if(!scanplanPrinter.writePdf(pdf)) {
            errorMessages << "Writing discharge scan plan failed";
        }
    }
    if (ui->checkBoxBwLoadPlan->isChecked()) {
        QString filename = basedir.filePath(m_basename + ".bw-loadplan-port.pdf");
        ScanplanPrinter scanplanPrinter("Load Scanplan", m_document, m_selectionPool, m_currentCall, bwLoadPlanPort());
        if (!scanplanPrinter.writePdf(filename)) {
            errorMessages << "Writing B/W load scan plan for ports failed";
        }
        QString filename2 = basedir.filePath(m_basename + ".bw-loadplan-weight.pdf");
        ScanplanPrinter scanplanPrinter2("Load Scanplan, weights", m_document, m_selectionPool, m_currentCall, bwLoadPlanWeight());
        if (!scanplanPrinter2.writePdf(filename2)) {
            errorMessages << "Writing B/W load scan plan for weights failed";
        }
    }
    if (m_previousCall && ui->checkBoxBwDischargePlan->isChecked()) {
        QString filename = basedir.filePath(m_basename + ".bw-dischargeplan.pdf");
        ScanplanPrinter scanplanPrinter("Discharge Scanplan", m_document, m_selectionPool, m_previousCall, bwDischargePlan());
        if (!scanplanPrinter.writePdf(filename)) {
            errorMessages << "Writing bw discharge scan plan failed";
        }
    }
    if (ui->checkboxDetailedBWScanplan->isChecked()) {
        QString pdf = basedir.filePath(m_basename+".bw-detailedloadplan.pdf");
        OneBayPerPageScanPlanPrinter printer(m_document, m_selectionPool, m_currentCall, detailedBwPlan());
        if(!printer.writePdf(pdf)) {
            errorMessages << "Writing detailed scanplan failed";
        }
    }
    if(ui->departureReport->isChecked()) {
        QString pdf = basedir.filePath(m_basename + ".departurereport.pdf");
        DepartureReport departureReport(m_document, m_currentCall, this);
        departureReport.writePdf(pdf);
    }
    if(ui->imoList->isChecked()) {
        ImolistExporter imolist(m_document, m_currentCall, this);
        QString pdf = basedir.filePath(m_basename+".imolist.pdf");
        imolist.writePdf(pdf);
    }
    if(ui->oogList->isChecked()) {
        OogListExporter ooglist(m_document, m_currentCall, this);
        QString pdf = basedir.filePath(m_basename+".ooglist.pdf");
        ooglist.writePdf(pdf);
    }
    if(ui->reeferList->isChecked()) {
        ReeferListExporter reeferlist(m_document, m_currentCall, this);
        QString pdf = basedir.filePath(m_basename+".reeferlist.pdf");
        reeferlist.writePdf(pdf);
    }
    if(ui->specialsList->isChecked()) {
        SpecialContainersListExporter specialslist(m_document, m_currentCall, this);
        QString pdf = basedir.filePath(m_basename+".specialcontainerlist.pdf");
        specialslist.writePdf(pdf);
    }
    if(ui->restowList->isChecked()) {
        ExportRestowList restowlist(m_document, m_currentCall, this);
        QString pdf = basedir.filePath(m_basename+".restow.pdf");
        restowlist.writePdf(pdf);
    }
    if(ui->stabilityReport->isChecked()) {
        StabilityReport stabilityReport(m_document, m_currentCall, this);
        QString pdf = basedir.filePath(m_basename+".stability.pdf");
        stabilityReport.writePdf(pdf);
    }
    if(errorMessages.isEmpty()) {
        QDialog::accept();
    } else {
        ui->errorLabel->setText(errorMessages.join("\n"));
    }
}

struct ContainerEqdLess {
    bool operator()(const ange::containers::Container* a, const ange::containers::Container* b) const {
        Q_ASSERT(a);
        Q_ASSERT(b);
        return a->equipmentNumber() < b->equipmentNumber();
    }
};

template <typename ContainerType>
QList<const Container*> copyAndSortContainers(QList<ContainerType> containers) {
    QList<const ange::containers::Container*> rv;
    #if (QT_VERSION >= QT_VERSION_CHECK(4, 7, 0)) // Delete this check when we have upgraded QT on windows
    rv.reserve(containers.size());
    #endif
    std::copy(containers.begin(), containers.end(), std::back_inserter(rv));
    qSort(rv.begin(), rv.end(), ContainerEqdLess());  // sorting for stable edifact output order for improved testing
    return rv;
}

QList<const Container* > ReportGenerator::containersForScope() {
    int scope = ui->edit_scope->currentIndex();

    switch (scope) {
        case 0: { // all containers
            return copyAndSortContainers < ange::containers::Container* >(m_document->containers()->list());
        }
        case 1: {  // selection
            return copyAndSortContainers < const ange::containers::Container* >(m_selectionPool->containers());
        }
    }
    Q_ASSERT(false);
    return QList<const ange::containers::Container*>();
}

void ReportGenerator::openDir() {
    QSettings settings;

    QString directory = QFileDialog::getExistingDirectory(0,"Reports directory", ui->editDirectory->text());
    ui->editDirectory->setText(directory);
    settings.setValue("reportsDirectory", directory);

}

void ReportGenerator::changeReportName(const QString& newName) {
    m_basename = newName;
}

QList<QCheckBox*> ReportGenerator::allReportCheckBoxes() {
    QList<QCheckBox*> list;
    list << ui->baplie;
    list << ui->baplie15;
    list << ui->coprar;
    list << ui->movins;
    list << ui->tansta;

    list << ui->restowList;
    list << ui->imoList;
    list << ui->oogList;
    list << ui->reeferList;
    list << ui->specialsList;
    list << ui->departureReport;

    list << ui->loadScanPlan;
    list << ui->dischScanPlan;
    list << ui->detailedScanPlan;
    list << ui->checkBoxBwLoadPlan;
    list << ui->checkBoxBwDischargePlan;
    list << ui->checkboxDetailedBWScanplan;
    list << ui->stabilityReport;
    return list;
}

void ReportGenerator::updateCheckBoxAllReports() {
    bool allChecked = true;
    bool allUnchecked = true;
    Q_FOREACH (QCheckBox* checkBox, allReportCheckBoxes()) {
        allChecked &= checkBox->isChecked();
        allUnchecked &= !checkBox->isChecked();
    }
    if (allChecked) {
        Q_ASSERT(!allUnchecked);
        ui->checkBoxAllReports->setCheckState(Qt::Checked);
        ui->checkBoxAllReports->setTristate(false);
    } else {
        if (allUnchecked) {
            ui->checkBoxAllReports->setCheckState(Qt::Unchecked);
            ui->checkBoxAllReports->setTristate(false);
        } else {
            ui->checkBoxAllReports->setCheckState(Qt::PartiallyChecked);
        }
    }
}

void ReportGenerator::updateAllReportCheckBoxes() {
    bool checked = ui->checkBoxAllReports->checkState() != Qt::Unchecked;
    ui->checkBoxAllReports->setTristate(false);
    Q_FOREACH (QCheckBox* checkBox, allReportCheckBoxes()) {
        checkBox->setChecked(checked);
    }
}

#include "reportgenerator.moc"
