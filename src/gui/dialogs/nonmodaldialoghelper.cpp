#include "nonmodaldialoghelper.h"

#include <QWidget>

NonModalDialogHelper::NonModalDialogHelper(QSharedPointer<DialogInterface> dialogInterface, QObject* parent)
  : QObject(parent), m_dialogInterface(dialogInterface)
{
    setObjectName("NonModalDialogHelper");
}

NonModalDialogHelper::~NonModalDialogHelper() {
    // Empty
}

void NonModalDialogHelper::raise() {
    if(m_dialog){
        m_dialog.data()->raise();
        return;
    }
    m_dialog = m_dialogInterface->widget();
    m_dialog.data()->setAttribute(Qt::WA_DeleteOnClose);
    m_dialog.data()->show();
}

#include "nonmodaldialoghelper.moc"
