#include "generatecontainersmodel.h"

#include "containerutils.h"

#include <ange/containers/container.h>
#include <ange/containers/equipmentnumber.h>
#include <ange/containers/isocode.h>
#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>

using namespace ange::containers;
using namespace ange::schedule;
using namespace ange::units;

// ContainerGroup:

GenerateContainersModel::ContainerGroup::ContainerGroup(int amount, const IsoCode& isoCode, const Mass weight,
                                                        const Call* loadCall, const Call* dischCall, bool isEmpty)
  : m_amount(amount), m_isoCode(isoCode), m_weight(weight),
    m_loadCall(const_cast<Call*>(loadCall)), m_dischCall(const_cast<Call*>(dischCall)), m_isEmpty(isEmpty)
{
    Q_ASSERT(m_loadCall);
    Q_ASSERT(m_dischCall);
}

// GenerateContainersModel:

GenerateContainersModel::GenerateContainersModel(const ange::schedule::Schedule* schedule,
                                                 const ange::schedule::Call* currentCall)
  : QAbstractTableModel()
{
    const Call* loadCall = currentCall;
    const Call* dischCall = schedule->next(currentCall);
    if (!dischCall) {  // Caused by currentCall being After
        loadCall = schedule->previous(currentCall);
        dischCall = currentCall;
    }
    // Insert the first default row
    m_containerGroups.append(ContainerGroup(0, IsoCode("22G0"), 10.0*ton, loadCall, dischCall, false));
}

QVariant GenerateContainersModel::data(const QModelIndex& index, int role) const {
    Q_UNUSED(index);
    Q_UNUSED(role);
    Q_ASSERT(index.row() <= m_containerGroups.count()); //There should never be more than a single empty row
                                                        //at the bottom of the table
    if (index.row() == m_containerGroups.count()) { //empty bottom row
        return QVariant();
    }
    if (role == Qt::DisplayRole) {
        switch (index.column()) {
            case CONTAINER_COUNT: return m_containerGroups.at(index.row()).m_amount;
            case ISO_NUMBER: return m_containerGroups.at(index.row()).m_isoCode.code();
            case WEIGHT: return m_containerGroups.at(index.row()).m_weight/ton;
            case LOAD_PORT: return m_containerGroups.at(index.row()).m_loadCall->uncode();
            case DISCHARGE_PORT: return m_containerGroups.at(index.row()).m_dischCall->uncode();
            case EMPTY_STATUS: return m_containerGroups.at(index.row()).m_isEmpty;
            case CARRIER_CODE: return m_containerGroups.at(index.row()).m_carrierCode;
            case DG_CODES: return ContainerUtils::dangerousGoodsCodesAsParsableString(m_containerGroups.at(index.row()).m_dgCodes);
            case NCOLUMNS: Q_ASSERT(false);
        }
        Q_ASSERT(false);
    }
    if (role == Qt::EditRole) {
        switch (index.column()) {
            case CONTAINER_COUNT: return m_containerGroups.at(index.row()).m_amount;
            case ISO_NUMBER: return m_containerGroups.at(index.row()).m_isoCode.code();
            case WEIGHT: return qVariantFromValue<Mass>(m_containerGroups.at(index.row()).m_weight);
            case LOAD_PORT: {
                Call* call = const_cast<Call*>(m_containerGroups.at(index.row()).m_loadCall);
                return QVariant::fromValue<QObject*>(call);
            }
            case DISCHARGE_PORT: {
                Call* call = const_cast<Call*>(m_containerGroups.at(index.row()).m_dischCall);
                return QVariant::fromValue<QObject*>(call);
            }
            case EMPTY_STATUS: return m_containerGroups.at(index.row()).m_isEmpty;
            case CARRIER_CODE: return m_containerGroups.at(index.row()).m_carrierCode;
            case DG_CODES: return ContainerUtils::dangerousGoodsCodesAsParsableString(
                                                                          m_containerGroups.at(index.row()).m_dgCodes);
            case NCOLUMNS: Q_ASSERT(false);
        }
        Q_ASSERT(false);
    }
    return QVariant();
}

bool GenerateContainersModel::setData(const QModelIndex& index, const QVariant& value, int role) {
    if (role == Qt::EditRole) {
        switch (index.column()) {
            case CONTAINER_COUNT: m_containerGroups[index.row()].m_amount = value.toInt(); break;
            case ISO_NUMBER: m_containerGroups[index.row()].m_isoCode = IsoCode(value.toString()); break;
            case WEIGHT: m_containerGroups[index.row()].m_weight = qvariant_cast<ange::units::Mass>(value); break;
            case LOAD_PORT: m_containerGroups[index.row()].m_loadCall = qvariant_cast<Call*>(value); break;
            case DISCHARGE_PORT: m_containerGroups[index.row()].m_dischCall = qvariant_cast<Call*>(value); break;
            case EMPTY_STATUS: m_containerGroups[index.row()].m_isEmpty = value.toBool(); break;
            case CARRIER_CODE: m_containerGroups[index.row()].m_carrierCode = value.toString(); break;
            case DG_CODES: m_containerGroups[index.row()].m_dgCodes =
                                                        ContainerUtils::parseDangerousGoodsString(value.toString()); break;
            case NCOLUMNS: return false;//Q_ASSERT(false);
        }
        return true;
    }
    return super::setData(index, value, role);
}

QVariant GenerateContainersModel::headerData(int section, Qt::Orientation orientation, int role) const {
    if (role != Qt::DisplayRole) {
        return QVariant();
    }
    if (orientation == Qt::Vertical) {
        return QVariant();
    }
    switch (section) {
        case CONTAINER_COUNT: return QString("Amount");
        case ISO_NUMBER: return QString("Type");
        case WEIGHT: return QString("Weight");
        case LOAD_PORT: return QString("POL");
        case DISCHARGE_PORT: return QString("POD");
        case EMPTY_STATUS: return QString("Empty");
        case CARRIER_CODE: return QString("Carrier");
        case DG_CODES: return QString("DG Codes");
        case NCOLUMNS: Q_ASSERT(false);
    }
    return QVariant();
}

int GenerateContainersModel::columnCount(const QModelIndex& parent) const {
    Q_UNUSED(parent);
    return NCOLUMNS;
}

int GenerateContainersModel::rowCount(const QModelIndex& parent) const {
    Q_UNUSED(parent);
    return m_containerGroups.count() + 1; //empty row at the bottom of the table
}

Qt::ItemFlags GenerateContainersModel::flags(const QModelIndex& index) const {
    if (index.row() < rowCount(QModelIndex()) - 1) {
        return super::flags(index) | Qt::ItemIsEditable;
    }
    return super::flags(index);
}

QList<ange::angelstow::ScheduledContainer> GenerateContainersModel::containers() const {
    int nbContainers = 0;
    for (int row = 0; row < m_containerGroups.count(); ++row) {
        nbContainers +=  m_containerGroups.at(row).m_amount;
    }
    QList <EquipmentNumber> eqNoList = EquipmentNumber::generateRandomEquipmentNumbers(nbContainers);
    QList <ange::angelstow::ScheduledContainer> containerList;
    for (int row = 0; row < m_containerGroups.count(); ++row) {
        for (int containerNumber = 0; containerNumber < m_containerGroups.at(row).m_amount; ++containerNumber) {
            Container* container = new Container(eqNoList.takeFirst(),
                                                 m_containerGroups.at(row).m_weight,
                                                 m_containerGroups.at(row).m_isoCode);
            container->setLive(m_containerGroups.at(row).m_isoCode.reefer() ? Container::Live : Container::NonLive);
            container->setEmpty(m_containerGroups.at(row).m_isEmpty);
            container->setCarrierCode(m_containerGroups.at(row).m_carrierCode);
            container->setDangerousGoodsCodes(m_containerGroups.at(row).m_dgCodes);
            const Call* pol = m_containerGroups.at(row).m_loadCall;
            container->setLoadPort(pol->uncode());
            const Call* pod = m_containerGroups.at(row).m_dischCall;
            container->setDischargePort(pod->uncode());
            containerList.append(ange::angelstow::ScheduledContainer(container, pol, pod));
        }
    }
    return containerList;
}

void GenerateContainersModel::addBottomRow(const QModelIndex& current, const QModelIndex&) {
    Q_ASSERT(m_containerGroups.size() > 0);
    if (current.row() == m_containerGroups.count()) {
        //copy last row as new row
        beginInsertRows(QModelIndex(), m_containerGroups.count() + 1, m_containerGroups.count() + 1);
        m_containerGroups.append(m_containerGroups.back());
        m_containerGroups[m_containerGroups.count()-1].m_amount = 0;
        endInsertRows();
    }
}
