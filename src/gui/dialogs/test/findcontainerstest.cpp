#include <QtTest>

#include "eqidmatchingutils.h"

#include <QObject>

/**
 * A test class to ensure the regexps for finding containers are working.
 */
class FindContainersTest : public QObject {
    Q_OBJECT
private Q_SLOTS:
    void findContainersInBlob1();
    void findContainersInBlob2();
    void findContainersInBlob3();
    void findContainersInBlob4();
    void findContainersInBlob5();
};

void FindContainersTest::findContainersInBlob1() {
    // Eq Ids separated by newlines
    QString testBlob = "ZZZU0003930\nZZZU0000510\nZZZU0006770\nZZZU0006260\nZZZU0006790\nZZZU0001160\n";
    QCOMPARE(extractEqIdsFromTextBlob(testBlob).count(), 6);
}

void FindContainersTest::findContainersInBlob2() {
    // Eq Ids separated by space and newlines
    QString testBlob = "ZZZU0003930 \nZZZU0000510 \nZZZU0006770 \nZZZU0006260 \nZZZU0006790 \nZZZU0001160\n";
    QCOMPARE(extractEqIdsFromTextBlob(testBlob).count(), 6);
}

void FindContainersTest::findContainersInBlob3() {
    // Eq Ids separated by commas
    QString testBlob = "ZZZU0003930,ZZZU0000510,ZZZU0006770,ZZZU0006260,ZZZU0006790,ZZZU0001160";
    QCOMPARE(extractEqIdsFromTextBlob(testBlob).count(), 6);
}

void FindContainersTest::findContainersInBlob4() {
    // Equipment ids matched: "ABCD1234567", "ABCD 1234567", "ABCD  1234567" (why with 2 spaces?)
    QString testBlob = "ABCD1234567,ABCD 1234567,ABCD  1234567";
    QStringList eqIds = extractEqIdsFromTextBlob(testBlob);
    QCOMPARE(eqIds.count(), 3);
    Q_FOREACH (const QString& eqId, eqIds) {
        QCOMPARE(eqId, QStringLiteral("ABCD1234567"));
    }
}

void FindContainersTest::findContainersInBlob5() {
    // Not matched: "A(BCDE1234567)8", "8(BCDE1234567)A"
    QString testBlob = "ABCDE12345678,8BCDE1234567A";
    QCOMPARE(extractEqIdsFromTextBlob(testBlob).count(), 0);
}

QTEST_GUILESS_MAIN(FindContainersTest);

#include "findcontainerstest.moc"
