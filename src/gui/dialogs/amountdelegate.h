#ifndef AMOUNTDELEGATE_H
#define AMOUNTDELEGATE_H

#include <QStyledItemDelegate>

class AmountDelegate : public QStyledItemDelegate {
    Q_OBJECT
    public:
        explicit AmountDelegate(QObject* parent = 0);
        virtual QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const ;
        virtual void setEditorData(QWidget* editor, const QModelIndex& index) const;
        virtual void setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const;
};

#endif // AMOUNTDELEGATE_H
