#include "amountdelegate.h"
#include <ange/units/units.h>
#include <QLineEdit>

AmountDelegate::AmountDelegate(QObject* parent): QStyledItemDelegate(parent) {
   // nothing
}

QWidget* AmountDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const {
    Q_UNUSED(option);
    Q_UNUSED(index);
    QLineEdit* edit = new QLineEdit(parent);
    QIntValidator* validator = new QIntValidator(edit);
    validator->setTop(10000);
    validator->setBottom(0);
    edit->setValidator(validator);
    return edit;
}

void AmountDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const {
    Q_ASSERT(index.data(Qt::EditRole).canConvert<int>());
    const int amount = index.data(Qt::EditRole).value<int>();
    QLineEdit* edit = qobject_cast<QLineEdit*>(editor);
    Q_ASSERT(edit);
    edit->setText(QString::number(amount, 'f', 0));
}

void AmountDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const {
    QLineEdit* edit = qobject_cast<QLineEdit*>(editor);
    Q_ASSERT(edit);
    QString text = edit->text();
    bool ok;
    int amount = text.toInt(&ok);
    Q_ASSERT(ok);
    model->setData(index, QVariant::fromValue<int>(amount), Qt::EditRole);
}

#include "amountdelegate.moc"
