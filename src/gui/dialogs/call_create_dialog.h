#ifndef CALL_CREATE_DIALOG_H
#define CALL_CREATE_DIALOG_H
#include <QDialog>

namespace Ui {
  class call_dialog;
}

class document_t;
class CallCreateDialog : public QDialog {
  Q_OBJECT
  public:
    CallCreateDialog(document_t* document, int index, QWidget* parent);
  private Q_SLOTS:
    virtual bool validate();
    void addCall();
  private:
    document_t* m_document;
    Ui::call_dialog* m_ui;
    int m_index;
public slots:
    /**
     * intermediate slot needed to connect the Done button's clicked(bool) signal
     * to the dialog's done(int) function (incompatible signal/slot signatures)
     */
    void done();
};

#endif // CALL_CREATE_DIALOG_H
