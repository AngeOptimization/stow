#include "about_dialog.h"
#include "ui_aboutdialog.h"

#include "dialogutils.h"
#include "crashhandler.h"
#include "pluginmanager.h"

#include <QAbstractTableModel>
#include <QFile>
#include <QScrollBar>

class PluginAboutDataModel : public QAbstractTableModel {
    private:
        enum  Columns {
            NameColumn,
            VersionColumn,
            StatusColumn,
            AuthorColumn,
            LicenseColumn
        };
        PluginManager* m_manager;
    public:
    explicit PluginAboutDataModel(PluginManager* manager, QObject* parent) : QAbstractTableModel(parent), m_manager(manager) {

    }
    virtual int columnCount(const QModelIndex& = QModelIndex()) const {
        return 5;
    }
    virtual int rowCount(const QModelIndex& = QModelIndex()) const {
        return m_manager->pluginHandles().size();
    }
    virtual QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const {
        if(!index.isValid()) {
            return QVariant();
        }
        if(role==Qt::DisplayRole) {
            if(index.column()==NameColumn) {
                return m_manager->pluginHandles().at(index.row()).name;
            } else if(index.column()==VersionColumn) {
                return m_manager->pluginHandles().at(index.row()).pluginVersion;
            } else if(index.column()==StatusColumn) {
                return m_manager->pluginHandles().at(index.row()).status;
            } else if(index.column()==AuthorColumn) {
                return m_manager->pluginHandles().at(index.row()).author;
            } else if(index.column()==LicenseColumn) {
                return m_manager->pluginHandles().at(index.row()).licenseType;
            }
        } else if(role==Qt::BackgroundRole) {
            if(!m_manager->pluginHandles().at(index.row()).plugin) {
                return QColor(Qt::red);
            }
        } else if(role==Qt::ToolTipRole) {
            if(index.column()==NameColumn) {
                return m_manager->pluginHandles().at(index.row()).filePath;
            } else if(index.column()==AuthorColumn) {
                return m_manager->pluginHandles().at(index.row()).authorLink;
            } else if(index.column()==LicenseColumn) {
                return m_manager->pluginHandles().at(index.row()).licenseText;
            }

        }
        return QVariant();
    }

};

AboutDialog::AboutDialog(PluginManager* manager)
  : QDialog(), m_ui(new Ui::about_dialog())
{
    m_ui->setupUi(this);
    DialogUtils::makeDialogMaximizable(this);
    // Tab 1: About
    m_ui->textBrowser->setDocument(new QTextDocument());
    QFile file(":/data/data/about.html");
    file.open(QIODevice::ReadOnly);
    m_ui->textBrowser->document()->setHtml(file.readAll());
    // It would be nice if we could scroll m_ui->textBrowser such that 0,0 was shown, but this only works after show()
    // m_ui->textBrowser->horizontalScrollBar()->setValue(0);
    // m_ui->textBrowser->verticalScrollBar()->setValue(0);
    // Tab 2: Plugins
    m_ui->tableView->setModel(new PluginAboutDataModel(manager, this));
    m_ui->tableView->resizeColumnsToContents();
    // Tab 3: Dumps
    m_ui->dumpsText->setDocument(new QTextDocument());
    QString crashDir(CrashHandler::crashDir());
    if (crashDir.isNull()) {
        m_ui->dumpsText->document()->setPlainText(QString("Dumps are disabled"));
    } else {
        m_ui->dumpsText->document()->setPlainText(QString("Dumps will be saved in: %1").arg(crashDir));
    }
}
