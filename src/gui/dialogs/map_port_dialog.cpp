/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010

 Copyright: See COPYING file that comes with this distribution

*/

#include "map_port_dialog.h"
#include "ui_map_port_dialog.h"
#include <QPushButton>
#include <ange/schedule/schedule.h>
#include <ange/schedule/call.h>
#include <QSortFilterProxyModel>

using ange::schedule::Call;
using ange::schedule::Schedule;

map_port_dialog_t::map_port_dialog_t(Schedule* schedule, const QString& uncode, const QString& voyage_code, const ange::schedule::Call* first_valid_call, const ange::schedule::Call* last_valid_call, QWidget* parent, Qt::WindowFlags f):
    QDialog(parent, f),
    m_ui(new Ui::map_port_dialog),
    m_schedule(schedule)
{
  m_ui->setupUi(this);
  Q_ASSERT(first_valid_call);
  Q_ASSERT(last_valid_call);
  const Call* first_call = first_valid_call ? first_valid_call : schedule->calls().front();
  const Call* last_call = last_valid_call ? last_valid_call : schedule->calls().back();
  m_ui->prompt_text_label->setText(tr("Please map %1 %2 to call in schedule").arg(uncode).arg(voyage_code.isEmpty()?QString():QString("and voyage code %1").arg(voyage_code)));
  const Call* guess = schedule->getCallByUncodeAfterCall(uncode, schedule->at(0));
  Q_FOREACH(const Call* call, schedule->between(first_call, last_call)) {
    add_call_to_combobox(call, guess);
  }
  add_call_to_combobox(last_call, guess);
  connect(m_ui->call_combo_box, SIGNAL(activated(int)), SLOT(call_selected(int)));
  connect(m_ui->buttonBox, SIGNAL(accepted()), SLOT(accept()));
  connect(m_ui->buttonBox, SIGNAL(rejected()), SLOT(reject()));
}

void map_port_dialog_t::add_call_to_combobox(const ange::schedule::Call* call_to_add, const ange::schedule::Call* current_call)
{
  m_ui->call_combo_box->addItem(call_to_add->uncode()+" "+call_to_add->voyageCode(), QVariant::fromValue(call_to_add));
  if (current_call == call_to_add) {
    m_ui->call_combo_box->setCurrentIndex(m_ui->call_combo_box->count()-1);
  }

}

void map_port_dialog_t::call_selected(int row) {
  const bool ok = (row>=0);
  QPushButton* ok_button = m_ui->buttonBox->button(QDialogButtonBox::Ok);
  ok_button->setEnabled(ok);
}

const ange::schedule::Call* map_port_dialog_t::mapped_to() const {
  return m_ui->call_combo_box->itemData(m_ui->call_combo_box->currentIndex()).value<const Call*>();
}

map_port_dialog_t::~map_port_dialog_t() {
  delete m_ui;
}

#include "map_port_dialog.moc"
