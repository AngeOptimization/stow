#ifndef HTMLTABLEFORMATTER_H
#define HTMLTABLEFORMATTER_H
#include <QString>

class QAbstractItemModel;
/**
 * A class that produces an Html table from a QAbstractItemModel as a table.
 * Currently supports a horizontal header at the top of the table
 * but not vertical headers
 */
class HtmlReportFormatter {
public:
    static QString reportHeader(const QString& reportName, const QString& vesselName, const QString& portAndVoyage);
    static QString tableToHtml(const QAbstractItemModel* table);
    static QString reportFooter(const QAbstractItemModel* table);
};

#endif // HTMLTABLEFORMATTER_H
