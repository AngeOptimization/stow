/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2013
 Copyright: See COPYING file that comes with this distribution
*/

#ifndef DEPARTUREREPORTPRINTER_H
#define DEPARTUREREPORTPRINTER_H

#include <QObject>
#include <QScopedPointer>
#include <QColor>
#include "gui/mode.h"
#include "abstractprinttextdocument.h"
#include "datacubehtmlexporter.h"

class QTextDocument;
class document_t;

namespace ange {
namespace schedule {
class Call;
}
}

namespace qdatacube {
class Datacube;
class AbstractFormatter;
}

class QPrinter;
class QAbstractItemView;
class container_list_t;
class DischargeCallAggregator;
class LoadCallAggregator;
class EmptyAggregator;
class CarrierCodeAggregator;
class LengthAggregator;
class HeightAggregator;
class ReeferAggregator;

class DepartureReport : public AbstractPrintTextDocument  {
  Q_OBJECT
  public:
    DepartureReport(document_t* document, const ange::schedule::Call* call, QWidget* parent = 0);
    virtual ~DepartureReport();

private:
    QTextDocument* createTextDocument(QObject* parent);
    virtual QString title();

    QString formatHtmlDocHeader() const;
    QString formatHtmlDocSchedule() const;
    QString formatHtmlDocEtaNextPort() const;
    QString formatHtmlDocContainerCount() const;
    QString formatHtmlDocTotalAwkCargoPlaceholder() const;
    QString formatHtmlDocTEUandWeightUtilization() const;
    QString formatHtmlDocAwkwardCargo() const;
    QString formatHtmlDocHCsummary() const;
    QString formatHtmlDocReeferCount() const;
    QString formatHtmlDocDgOnBoard() const;
    QString formatHtmlDocVesselAllocationPlaceHolder() const;
    QString formatHtmlDocSlotsReleasedPlaceHolder() const;
    QString formatHtmlDocSlotsSwappedPlaceHolder() const;
    QString formatHtmlDocRestows() const;
    QString formatHtmlDocHatchCoversAndGearBoxesPlaceholder() const;
    QString formatHtmlDocOogOnBoard() const;
    QString formatHtmlDocBundlesOnBoard() const;
    QString formatHtmlDocFooter() const;
    QSharedPointer<DischargeCallAggregator> m_destinationAggregator;
    QSharedPointer<LoadCallAggregator> m_originAggregator;
    QSharedPointer<EmptyAggregator> m_emptyAggregator;
    QSharedPointer<LengthAggregator> m_lengthAggregator;
    QSharedPointer<HeightAggregator> m_heightAggregator;
    QSharedPointer<CarrierCodeAggregator> m_carrierCodeAggregator;
    QSharedPointer<ReeferAggregator> m_reeferAggregator;
};

#endif // DEPARTUREREPORTPRINTER_H
