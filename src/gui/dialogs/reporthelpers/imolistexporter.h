/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010

 Copyright: See COPYING file that comes with this distribution

*/

#ifndef EXPORT_IMOLIST_H
#define EXPORT_IMOLIST_H

#include "abstractprinttextdocument.h"

namespace ange {
    namespace schedule {
        class Call;
    }
}

class QTextDocument;
class document_t;

class ImolistExporter : public AbstractPrintTextDocument {
    Q_OBJECT

public:
    ImolistExporter(document_t* document, const ange::schedule::Call* call, QWidget* parent = 0);

private:
    QTextDocument* createTextDocument(QObject* parent);
    virtual QString title();
};

#endif // IMOLIST_H
