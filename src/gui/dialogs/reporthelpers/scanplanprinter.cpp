#include "scanplanprinter.h"

#include <QPainter>
#include <QPrinter>
#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>
#include "document/document.h"
#include "gui/vesselview/vesselscene.h"
#include "gui/vesselview/vesselgraphicsitem.h"
#include <ange/vessel/vessel.h>

using ange::schedule::Call;

ScanplanPrinter::ScanplanPrinter(QString title,document_t* document, pool_t* pool, const Call* printedCall, PrintMode printMode, QObject* parent):
    AbstractScanplanPrinter(title, document, pool, printedCall, printMode, parent) {
    m_printScene->setSceneRect(m_printScene->vessel_item()->mapToScene(
                                                      m_printScene->vessel_item()->boundingRect()).boundingRect());
    const Call* currentCall = printedCall;
    if(printMode.scanPlanType) {
        Q_ASSERT(currentCall != document->schedule()->calls().last());
        currentCall = document->schedule()->next(printedCall);
    }
    m_header_text = QString("%4 -- %1 %2 %3").arg(document->vessel()->name()).arg(currentCall->uncode())
                                                                     .arg(currentCall->voyageCode()).arg(title);
}

ScanplanPrinter::~ScanplanPrinter() {
}

void ScanplanPrinter::initPrinter(QPrinter* printer) {
    printer->setPaperSize(QPrinter::A4);
    printer->setOrientation(QPrinter::Landscape);
}


void ScanplanPrinter::drawPage(QPainter* painter, QRectF rect, int pageNumber) {
    Q_UNUSED(pageNumber);
    m_printScene->render(painter, rect);
}

int ScanplanPrinter::pageCount() const {
    return 1;
}

#include "scanplanprinter.moc"
