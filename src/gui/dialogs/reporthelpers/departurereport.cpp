/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2013
 Copyright: See COPYING file that comes with this distribution
*/

#include "departurereport.h"

#include <QPainter>
#include <QPrintDialog>
#include <QPrintPreviewDialog>
#include <QTextDocument>
#include <QPrinter>
#include <QFileDialog>
#include <QMap>

#include <ange/containers/container.h>
#include <ange/containers/oog.h>
#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>

#include "document/pools/pool.h"
#include "document/document.h"
#include "document/stowage/stowage.h"
#include "document/containers/container_list.h"
#include "document/stowage/container_leg.h"
#include "document/stowage/container_move.h"
#include "document/userconfiguration/userconfiguration.h"

#include "gui/vesselview/vesselscene.h"
#include "gui/vesselview/vesselgraphicsitem.h"
#include "gui/cranesplit/crane_split_item.h"

#include <gui/colortools.h>
#include <ange/vessel/vessel.h>
#include <qdatacube/datacube.h>
#include <qdatacube/filterbyaggregate.h>
#include <qdatacube/countformatter.h>
#include "document/containers/length_aggregator.h"
#include "document/containers/traversingfilter.h"
#include "document/containers/currentcallfilter.h"
#include <document/containers/dischargecallaggregator.h>
#include <document/containers/loadcallaggregator.h>
#include <document/containers/empty_aggregator.h>
#include <document/containers/height_aggregator.h>
#include <document/containers/carrier_code_aggregator.h>
#include <document/containers/reefer_aggregator.h>
#include <document/containers/recapformatterhcpenalty.h>
#include <document/utils/callutils.h>
#include <document/containers/recapformatterweight.h>
#include <document/containers/recapformatterteu.h>
#include <recapformatterhcequivalence.h>
#include <ange/vessel/slottablesummary.h>

using namespace ange::units;
using ange::containers::Container;
using ange::containers::DangerousGoodsCode;
using ange::containers::EquipmentNumber;
using ange::containers::IsoCode;
using ange::schedule::Call;
using namespace qdatacube;

DepartureReport::DepartureReport(document_t* document, const Call* call, QWidget* parent)
    : AbstractPrintTextDocument(document, call, parent),
    m_destinationAggregator(new DischargeCallAggregator(document->containers(), document->schedule())),
    m_originAggregator(new LoadCallAggregator(document->containers(), document->schedule())),
    m_emptyAggregator(new EmptyAggregator(document->containers())),
    m_lengthAggregator(new LengthAggregator(document->containers())),
    m_heightAggregator(new HeightAggregator(document->containers())),
    m_carrierCodeAggregator(new CarrierCodeAggregator(document->containers())),
    m_reeferAggregator(new ReeferAggregator(document->containers()))
{}

DepartureReport::~DepartureReport() {
}


QString DepartureReport::title() {
    return QString("Departure Report");
}

QTextDocument* DepartureReport::createTextDocument(QObject* parent) {
    Q_ASSERT(document());
    Q_ASSERT(document()->vessel());
    Q_ASSERT(currentCall());
    Q_ASSERT(currentCall() != document()->schedule()->calls().last());
    QString departureReport;
    departureReport += formatHtmlDocHeader();
    departureReport += "<br/><br/>";
    departureReport += formatHtmlDocSchedule();
    departureReport += "<br/><br/>";
    departureReport += formatHtmlDocEtaNextPort();
    departureReport += "<br/><br/>";
    departureReport += formatHtmlDocContainerCount();
    departureReport += "<br/><br/>";
    departureReport += formatHtmlDocTotalAwkCargoPlaceholder();
    departureReport += "<br/><br/>";
    departureReport += formatHtmlDocTEUandWeightUtilization();
    departureReport += "<br/><br/>";
    departureReport += formatHtmlDocAwkwardCargo();
    departureReport += "<br/><br/>";
    departureReport += formatHtmlDocReeferCount();
    departureReport += "<br/><br/>";
    departureReport += formatHtmlDocDgOnBoard();
    departureReport += "<br/><br/>";
    departureReport += formatHtmlDocVesselAllocationPlaceHolder();
    departureReport += "<br/><br/>";
    departureReport += formatHtmlDocSlotsReleasedPlaceHolder();
    departureReport += "<br/><br/>";
    departureReport += formatHtmlDocSlotsSwappedPlaceHolder();
    departureReport += "<br/><br/>";
    departureReport += formatHtmlDocRestows();
    departureReport += "<br/><br/>";
    departureReport += formatHtmlDocHatchCoversAndGearBoxesPlaceholder();
    departureReport += "<br/><br/>";
    departureReport += formatHtmlDocBundlesOnBoard();
    departureReport += "<br/><br/>";
    departureReport += "<h2>15. Remarks</h2>";
    departureReport += "<br/><br/>";
    departureReport += formatHtmlDocFooter();
    QTextDocument* rv = new QTextDocument(parent);
    rv->setHtml(departureReport);
    return rv;
}


QString DepartureReport::formatHtmlDocHeader() const {
    QString html;
    html += "<html>";
    html += "<head>";
    html += "</head>";
    html += "<body style=\"font-size: 10pt;\">";
    // <body style=" font-family:'Sans'; font-size:11pt; font-weight:400; font-style:normal;">
    html += QString("<h1>Regional Departure Report<br>");
    html += QString("%1<br>").arg(document()->vessel()->name());
    html += QString("%1<br>").arg(currentCall()->voyageCode());
    html += QString("%1<br>").arg(currentCall()->uncode());
    const ange::vessel::SlotTableSummary& slotSummary = document()->vessel()->slotTableSummary();
    html += QString("Nom. TEU: %1, Plugs: %2<br>").arg(slotSummary.totalTEU()).arg(slotSummary.reeferPlugs());
    html += QString("</h1>\n");

    return html;
}

QString DepartureReport::formatHtmlDocSchedule() const {
    UserConfiguration* config = document()->userconfiguration();

    QString html = QString("<h2>1. Vessel Movement</h2>\n");
    html += "<table border='1' cellpadding='3' cellspacing='0'>";
    html += "<tr><th>Voyage</th><th>Call</th><th>ETA</th><th>Berthing</th><th>Unberthing</th><th>ETD</th></tr>";
    Q_FOREACH(const Call* call, document()->schedule()->calls()) {
        if(is_befor_or_after(call)) {
            continue;
        }
        bool isCurrent = call == currentCall();
        QString eta = call->eta().isValid()?call->eta().toString(Qt::DefaultLocaleShortDate):QString("--");
        QString etd = call->etd().isValid()?call->etd().toString(Qt::DefaultLocaleShortDate):QString("--");
        QColor background = config->get_color_by_call(call);
        QColor foreground = backgroundRequiresColorInversion(background) ? Qt::white : Qt::black;
        if (isCurrent) {
            html += QString("<tr><td><b>%1</b></td><td bgcolor='%3' style='color:%4'><b>%2</b></td><td>%5</td><td></td><td></td><td>%6</td></tr>")
            .arg(call->voyageCode()).arg(call->uncode()).arg(background.name()).arg(foreground.name())
            .arg(eta).arg(etd);
        } else {
            html += QString("<tr><td>%1</td><td bgcolor='%3' style='color:%4'>%2</td><td>%5</td><td></td><td></td><td>%6</td></tr>")
            .arg(call->voyageCode()).arg(call->uncode()).arg(background.name()).arg(foreground.name())
            .arg(eta).arg(etd);
        }
        //future calls not included per user request
        if(isCurrent) {
            break;
        }
    }
    html += "</table>";
    return html;
}

QString DepartureReport::formatHtmlDocEtaNextPort() const {
    QString html("<h2>2. Eta Next Port</h2>\n");
    const Call* nextCall = document()->schedule()->next(currentCall());
    if(nextCall) {
    html += QString("ETA next port (%1): %2").arg(nextCall->uncode())
            .arg(nextCall->eta().toString(Qt::LocaleDate));
    }
    return html;
}

QString DepartureReport::formatHtmlDocContainerCount() const {
    QSharedPointer<TraversingFilter> traversingFilter(new TraversingFilter(document()->containers(), document()->stowage()));
    traversingFilter->setCurrentCall(currentCall());
    qdatacube::Datacube datacube(document()->containers(), m_carrierCodeAggregator, m_originAggregator);
    datacube.addFilter(traversingFilter);
    datacube.split(Qt::Vertical, 1, m_destinationAggregator);
    datacube.split(Qt::Vertical, 2, m_emptyAggregator);
    datacube.split(Qt::Vertical, 3, m_lengthAggregator);
    datacube.split(Qt::Vertical, 4, m_heightAggregator);
    QString htmlTEUcount = DatacubeHtmlExporter::datacubeToHtml(&datacube, CountFormatter(document()->containers()));
    QString htmlWeight = DatacubeHtmlExporter::datacubeToHtml(&datacube, RecapFormatterWeight(document()->containers()));
    QString html = "<h2>3. Unit count and weight per carrier</h2>\n";
    if(htmlTEUcount.isEmpty()) {
        html += "<p>None</p>";
    } else {
        html += "<h3>TEU count:</h3>" + htmlTEUcount + "<h3>Weight:</h3>" + htmlWeight;
    }
    return html;
}


QString DepartureReport::formatHtmlDocTotalAwkCargoPlaceholder() const {
    QString html("<h2>4. Total additional slots used by uncontainerized/awkward and HC containers (TEU)</h2>");
    html += "<table border='1' cellpadding='3' cellspacing='0'>\n";
    html += "<tr>";
    for(int i = 0 ; i < m_carrierCodeAggregator->categoryCount(); i++) {
        html+=QString("<th>%1</th>").arg(m_carrierCodeAggregator->categoryHeaderData(i).toString());
    }
    html += "</tr>\n";
    html += "<tr>";
    for(int i = 0 ; i < m_carrierCodeAggregator->categoryCount(); i++) {
        html+=QString("<td></td>");
    }
    html += "</tr>\n";
    html += "</table>\n";
    return html;
}

QString DepartureReport::formatHtmlDocTEUandWeightUtilization() const {
    double HCtoTEUratio = 2.0;
    QSharedPointer<TraversingFilter> traversingFilter(new TraversingFilter(document()->containers(), document()->stowage()));
    traversingFilter->setCurrentCall(currentCall());
    qdatacube::Datacube datacubeTEU(document()->containers(), m_emptyAggregator, m_carrierCodeAggregator);
    datacubeTEU.addFilter(traversingFilter);
    QString htmlTEU = DatacubeHtmlExporter::datacubeToHtml(&datacubeTEU, RecapFormatterTEU(document()->containers(), 0L, HCtoTEUratio));
    if(htmlTEU.isEmpty()) {
        htmlTEU = "<p>None</p>";
    }
    htmlTEU = "<h3>Slot utilization (TEU)</h3>" + htmlTEU;
    QString htmlHClost = "<h3>TEU slots lost due to HC</h3>";
    traversingFilter->setCurrentCall(currentCall());
    qdatacube::Datacube datacubeHC(document()->containers());
    datacubeHC.addFilter(traversingFilter);
    datacubeHC.addFilter(qdatacube::AbstractFilter::Ptr(new qdatacube::FilterByAggregate(m_heightAggregator, "HC")));
    datacubeHC.split(Qt::Horizontal, 0, m_carrierCodeAggregator);
    datacubeHC.split(Qt::Vertical, 0, m_lengthAggregator);
    htmlHClost += DatacubeHtmlExporter::datacubeToHtml(&datacubeHC, RecapFormatterHCPenalty(document()->containers(), 0L),
                             DatacubeHtmlExporter::SHOWTOTALS);
    qdatacube::Datacube datacubeWeight(document()->containers(), m_emptyAggregator, m_carrierCodeAggregator);
    datacubeWeight.addFilter(traversingFilter);
    QString htmlWeight = DatacubeHtmlExporter::datacubeToHtml(&datacubeWeight, RecapFormatterWeight(document()->containers()));
    QString htmlTEUactual = DatacubeHtmlExporter::datacubeToHtml(&datacubeTEU, RecapFormatterHCEquivalence(document()->containers(), 0L));
    if(htmlTEUactual.isEmpty()) {
        htmlTEUactual = "<p>None</p>";
    }
    htmlTEUactual = "<h3>Slot utilization (TEU, HC-corrected)</h3>" + htmlTEUactual;
    if(htmlWeight.isEmpty()) {
        htmlWeight = "<p>None</p>";
    }
    htmlWeight = "<h3>Weight utilization (tons)</h3>" + htmlWeight;
    QString html = QString("<h2>5. Total slot/weight (TEU/tons) utilization</h2>\n")
                   + htmlTEU + htmlHClost + htmlTEUactual + htmlWeight;
    return html;
}

QString DepartureReport::formatHtmlDocAwkwardCargo() const {
    QString html("<h2>6. Awkward cargo</h2>");
    html += formatHtmlDocOogOnBoard();
    html += "<h3>2. Uncontainerized cargo</h3>\n";
    html += "[Please fill in]\n";
    html += formatHtmlDocHCsummary();
    return html;
}

QString DepartureReport::formatHtmlDocHCsummary() const {
    QSharedPointer<TraversingFilter> traversingFilter(new TraversingFilter(document()->containers(), document()->stowage()));
    traversingFilter->setCurrentCall(currentCall());
    qdatacube::Datacube datacube(document()->containers());
    datacube.addFilter(traversingFilter);
    datacube.addFilter(qdatacube::AbstractFilter::Ptr(new qdatacube::FilterByAggregate(m_heightAggregator, "HC")));
    datacube.split(Qt::Vertical, 0, m_carrierCodeAggregator);
    datacube.split(Qt::Horizontal, 0, m_lengthAggregator);
    QString html = DatacubeHtmlExporter::datacubeToHtml(&datacube, CountFormatter(document()->containers()), DatacubeHtmlExporter::NOTOTALS);
    if(html.isEmpty()) {
        html= "<p>None</p>\n";
    }
    html = "<h3>3. HC containers</h3>\n" + html;
    return html;
}

QString DepartureReport::formatHtmlDocReeferCount() const {
    QSharedPointer<TraversingFilter> traversingFilter(new TraversingFilter(document()->containers(), document()->stowage()));
    traversingFilter->setCurrentCall(currentCall());
    qdatacube::AbstractFilter::Ptr reeferFilter(new qdatacube::FilterByAggregate(m_reeferAggregator, "RF"));
    qdatacube::Datacube datacube(document()->containers(), m_carrierCodeAggregator, m_originAggregator);
    datacube.addFilter(traversingFilter);
    datacube.addFilter(reeferFilter);
    datacube.split(Qt::Vertical, 1, m_originAggregator);
    datacube.split(Qt::Vertical, 2, m_emptyAggregator);
    datacube.split(Qt::Vertical, 3, m_lengthAggregator);
    datacube.split(Qt::Vertical, 4, m_heightAggregator);
    QString html = DatacubeHtmlExporter::datacubeToHtml(&datacube, CountFormatter(document()->containers()));
    if(html.isEmpty()) {
        html = "<p>None</p>";
    }
    html = "<h2>7. Reefer Containers</h2>\n" + html;
    return html;
}

QString DepartureReport::formatHtmlDocDgOnBoard() const {
    bool haveContainersToPrint = false;
    QString html;
    html += "<table border='1' cellpadding='3' cellspacing='0'>";
    html += "<tr>";
    html += "<th>Equipment ID</th><th>Type</th><th>Length</th><th>Weight</th><th>POL</th><th>POD</th><th>Position</th><th>Operator</th><th>UNNO(IMO class)</th>";
    html += "</tr>";
    const ContainerLeg* routes = document()->stowage()->container_routes();
    Q_FOREACH(const Container* container, document()->containers()->list()) {
        if (container->isDangerous()) {
            const ange::vessel::BayRowTier brt = document()->stowage()->nominal_position(container, currentCall());
            if (!brt.placed()) {
                continue;
            }
            haveContainersToPrint = true;
            html += "<tr>";
            html += QString("<td>%1</td><td>%2</td><td>%3</td><td>%4</td><td>%5</td><td>%6</td>")
                        .arg(container->equipmentNumber()).arg(container->isoCode().code()).arg(container->isoLength()).arg(container->weight()/ton)
                        .arg(routes->portOfLoad(container)->uncode()).arg(routes->portOfDischarge(container)->uncode());
            html += QString("<td>%1%2%3</td>")
                        .arg(brt.bay(),brt.bay()<100 ? 2 : 3,10,QChar('0')).arg(brt.row(),2,10,QChar('0')).arg(brt.tier(),2,10,QChar('0'));
            html += QString("<td>%1</td>").arg(container->carrierCode());
            html += "<td>";
            Q_FOREACH(DangerousGoodsCode dg, container->dangerousGoodsCodes()) {
                html += QString(" %1(%2) ").arg(dg.unNumber()).arg(dg.imdgClass());
            }
            html += "</td>";
            html += "</tr>";
        }
    }
    html += "</table>";
    if (!haveContainersToPrint) {
        html = "<p>None</p>";
    }
    html = "<h2>8. Dangerous Cargo</h2>\n" + html;
    return html;
}

QString htmlAllocationSectionRow(const QString& headerEntry, int nbDataCols) {
    QString html = "<tr>";
    html += "<th>" + headerEntry + "</th>\n";
    for(int i = 0 ; i < nbDataCols; i++) {
        html+=QString("<td></td>");
    }
    html += "</tr>\n";
    return html;
}

QString DepartureReport::formatHtmlDocVesselAllocationPlaceHolder() const {
    QString html = "<h2>9. Vessel Allocation (TEU/ton)</h2>\n";
    QString tableStart;
    tableStart += "<table border='1' cellpadding='3' cellspacing='0'>\n";
    tableStart += "<tr>";
    tableStart += "<td></td>"; //upper left corner
    for(int i = 1 ; i < m_carrierCodeAggregator->categoryCount(); i++) { //skip the "-"
        tableStart+=QString("<th>%1</th>").arg(m_carrierCodeAggregator->categoryHeaderData(i).toString());
    }
    tableStart += "</tr>\n";
    html += "<h3>TEU</h3>\n";
    html += tableStart;
    int nbCategories = m_carrierCodeAggregator->categoryCount()-1;
    html += htmlAllocationSectionRow("Basic Allocation", nbCategories);
    html += htmlAllocationSectionRow("Slot Release", nbCategories);
    html += htmlAllocationSectionRow("Slot Swap", nbCategories);
    html += htmlAllocationSectionRow("Additional Cargo", nbCategories);
    html += htmlAllocationSectionRow("Total Allocation", nbCategories);
    html += "</table>\n";
    html += "<br>\n";
    html += "<h3>Weight</h3>\n";
    html += tableStart;
    html += htmlAllocationSectionRow("Basic Cargo Weight", nbCategories);
    html += htmlAllocationSectionRow("Slot Release (Weight)", nbCategories);
    html += htmlAllocationSectionRow("Slot Swap (Weight)", nbCategories);
    html += htmlAllocationSectionRow("Additional Cargo Weight", nbCategories);
    html += htmlAllocationSectionRow("Total Allocation (Weight)", nbCategories);
    html += "</table>\n";

    return html;
}

QString DepartureReport::formatHtmlDocSlotsReleasedPlaceHolder() const {
    QString html = "<h2>10. Slots Released</h2>\n";
    html += "[Please fill in]\n";
    return html;
}

QString DepartureReport::formatHtmlDocSlotsSwappedPlaceHolder() const {
    QString html = "<h2>11. Slots Swapped</h2>\n";
    html += "[Please fill in]\n";
    return html;
}


QString DepartureReport::formatHtmlDocRestows() const {
    QString html = "<h2>12. Restows</h2>\n";
    html += "<table border='1' cellpadding='3' cellspacing='0'>\n";
    html += "<tr><th>Port of call</th><th>Voyage</th><th>Restows</th></tr>\n";
    Q_FOREACH(const Call* call, document()->schedule()->calls()) {
        if(is_befor_or_after(call)) {
            continue;
        }
        html += QString("<tr><td>%1</td><td>%2</td><td align=\"center\">%3</td></tr>\n")
                .arg(call->uncode()).arg(call->voyageCode())
                .arg(document()->stowage()->restow_count(call));
        if(call == currentCall()) {
            break;
        }
    }
    html += "</table>\n";
    return html;
}

QString DepartureReport::formatHtmlDocHatchCoversAndGearBoxesPlaceholder() const {
    QString html = "<h2>13. Hatch Cover and Gear Box Moves</h2>\n";
    html += "<table border='1' cellpadding='3' cellspacing='0'>\n";
    html += "<tr><th>Port of call</th><th>Voyage</th><th>Hatch Cover Moves</th><th>Gear Box Moves</th></tr>\n";
    Q_FOREACH(const Call* call, document()->schedule()->calls()) {
        if(is_befor_or_after(call)) {
            continue;
        }
        html += QString("<tr><td>%1</td><td>%2</td><td></td><td></td></tr>\n")
                .arg(call->uncode()).arg(call->voyageCode());
        if(call == currentCall()) {
            break;
        }
    }
    html += "</table>\n";
    return html;
}

QString DepartureReport::formatHtmlDocOogOnBoard() const {
    bool haveContainersToPrint = false;
    QString html;
    html += "<table border='1' cellpadding='3' cellspacing='0'>";
    html += "<tr>";
    html += "<th>Equipment ID</th><th>Type</th><th>Length</th><th>Weight</th><th>POL</th><th>POD</th><th>Position</th><th>Operator</th>";
    html += "<th>OOGF</th><th>OOGB</th><th>OOGR</th><th>OOGL</th><th>OOGT</th>";
    html += "</tr>";
    const ContainerLeg* routes = document()->stowage()->container_routes();
    Q_FOREACH(const Container* container, document()->containers()->list()) {
        if (container->oog()) {
            const ange::vessel::BayRowTier brt = document()->stowage()->nominal_position(container, currentCall());
            if (!brt.placed()) {
                continue;
            }
            haveContainersToPrint = true;
            html += "<tr>";
            html += QString("<td>%1</td><td>%2</td><td>%3</td><td>%4</td><td>%5</td><td>%6</td>")
                        .arg(container->equipmentNumber()).arg(container->isoCode().code()).arg(container->isoLength()).arg(container->weight()/ton)
                        .arg(routes->portOfLoad(container)->uncode()).arg(routes->portOfDischarge(container)->uncode());
            html += QString("<td>%1%2%3</td>")
                        .arg(brt.bay(),brt.bay()<100 ? 2 : 3,10,QChar('0')).arg(brt.row(),2,10,QChar('0')).arg(brt.tier(),2,10,QChar('0'));
            html += QString("<td>%1</td>").arg(container->carrierCode());
            html += QString("<td>%1</td><td>%2</td><td>%3</td><td>%4</td><td>%5</td>")
                        .arg(container->oog().front() / centimeter).arg(container->oog().back() / centimeter)
                        .arg(container->oog().right() / centimeter).arg(container->oog().left() / centimeter)
                        .arg(container->oog().top() / centimeter);
            html += "</tr>";
        }
    }
    html += "</table>";
    if (!haveContainersToPrint) {
        html = "<p>None</p>";
    }
    html = "<h3>1. OOG containers</h3>\n" + html;
    return html;
}

QString DepartureReport::formatHtmlDocBundlesOnBoard() const {
    bool haveContainersToPrint = false;
    QString html;
    html += "<table border='1' cellpadding='3' cellspacing='0'>";
    html += "<tr>";
    html += "<th>Equipment ID</th><th>Type</th><th>Length</th><th>Weight</th><th>POL</th><th>POD</th><th>Position</th><th>Operator</th><th>Bundled Equipment</th>";
    html += "</tr>";
    const ContainerLeg* routes = document()->stowage()->container_routes();
    Q_FOREACH(const Container* container, document()->containers()->list()) {
        if (container->bundleChildren().size() > 0) {
            const ange::vessel::BayRowTier brt = document()->stowage()->nominal_position(container, currentCall());
            if (!brt.placed()) {
                continue;
            }
            haveContainersToPrint = true;
            html += "<tr>";
            html += QString("<td>%1</td><td>%2</td><td>%3</td><td>%4</td><td>%5</td><td>%6</td>")
            .arg(container->equipmentNumber()).arg(container->isoCode().code()).arg(container->isoLength()).arg(container->weight()/ton)
            .arg(routes->portOfLoad(container)->uncode()).arg(routes->portOfDischarge(container)->uncode());
            html += QString("<td>%1%2%3</td>")
            .arg(brt.bay(),brt.bay()<100 ? 2 : 3,10,QChar('0')).arg(brt.row(),2,10,QChar('0')).arg(brt.tier(),2,10,QChar('0'));
            html += QString("<td>%1</td>").arg(container->carrierCode());
            html += "<td>";
            Q_FOREACH(const Container* childContainer, container->bundleChildren()) {
                html += QString(" %1 ").arg(childContainer->equipmentNumber());
            }
            html += "</td>";
            html += "</tr>";
        }
    }
    html += "</table>";
    if (!haveContainersToPrint) {
        html = "<p>None</p>";
    }
    html = "<h2>14. Bundled units</h2>\n" + html;
    return html;
}

QString DepartureReport::formatHtmlDocFooter() const {
    QString html;
    html += "</body>";
    html += "</html>";
    return html;
}

#include "departurereport.moc"
