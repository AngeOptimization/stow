/*
 *
 */

#ifndef EXPORTREEFERLIST_H
#define EXPORTREEFERLIST_H
#include "abstractprinttextdocument.h"

class ReeferListExporter : public AbstractPrintTextDocument {
    Q_OBJECT
    public:
        ReeferListExporter(document_t* document, const ange::schedule::Call* currentCall, QWidget* parent = 0);
    private:
        virtual QTextDocument* createTextDocument(QObject* parent);
        virtual QString title();
};

#endif // EXPORTREEFERLIST_H
