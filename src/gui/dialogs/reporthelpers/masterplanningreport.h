#ifndef MASTERPLANNING_REPORT_H
#define MASTERPLANNING_REPORT_H

#include <QWidget>

class PluginManager;
class document_t;
class QTextStream;
namespace ange {
namespace schedule {
class Call;
}
}

/**
 * @brief Display report about the master planning result
 * Mainly intended for debugging purposes
 */
class MasterPlanningReport {

public:
    /**
     * Generate the report based on the document, will not access the document after the constructor has returned.
     */
    MasterPlanningReport(PluginManager* pluginManager, const ange::schedule::Call* call);

    /**
     * Show report using the given parent for the window.
     */
    void showWindow(QWidget* parent);

private:
    PluginManager* m_pluginManager;
    QString m_html;
};

#endif // MASTERPLANNING_REPORT_H
