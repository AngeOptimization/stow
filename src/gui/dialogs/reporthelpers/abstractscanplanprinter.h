#ifndef ABSTRACTSCANPLANPRINTER_H
#define ABSTRACTSCANPLANPRINTER_H

#include <QObject>
#include <QRectF>
#include "gui/mode.h"
class QTextDocument;
class QPrinter;
class QPainter;
class document_t;
class pool_t; // TODO: figure out if needed
class vessel_scene_t;
namespace ange {
    namespace schedule {
        class Call;
    }
}

/**
 * Base class for scanplan printers
 */
class AbstractScanplanPrinter : public QObject {
    Q_OBJECT
    public:
        AbstractScanplanPrinter(QString title, document_t* document, pool_t* pool, const ange::schedule::Call* printedCall, PrintMode printMode, QObject* parent = 0);
        virtual ~AbstractScanplanPrinter();
        /**
         * Writes the pdf to \param filename
         */
        bool writePdf(QString filename);
        /**
         * Prints to the (physical) printer
         */
        void print();
        /**
         * Launches a print preview window
         */
        void print_preview();
        /**
         * The amount of pages you have to report
         */
        virtual int pageCount() const = 0;
    private Q_SLOTS:
        /**
         * Prints the scene
         */
        bool print_scene(QPrinter* printer);
    private:
        /**
         * Initializes the printer for this thing. including orientation
         */
        virtual void initPrinter(QPrinter* printer) = 0;
        /**
         * Creates a summary, and if needed, ensures that the one-letter things are updated
         */
        QTextDocument* create_summary_doc(QObject* parent);
    protected:
        virtual void drawPage(QPainter* painter, QRectF rect, int pageNumber) = 0 ;
    protected:
        document_t* m_document;
        QScopedPointer<vessel_scene_t> m_printScene;
        const ange::schedule::Call* m_printedCall;
        QString m_header_text;
        QString m_eta_etd;

};

#endif // ABSTRACTSCANPLANPRINTER_H
