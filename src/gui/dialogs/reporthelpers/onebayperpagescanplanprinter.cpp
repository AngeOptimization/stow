#include "onebayperpagescanplanprinter.h"

#include "bayslicegraphicsitem.h"
#include "dialogutils.h"
#include "document.h"
#include "vesselgraphicsitem.h"
#include "vesselscene.h"

#include <ange/schedule/schedule.h>
#include <ange/vessel/bayslice.h>
#include <ange/vessel/vessel.h>

#include <QPainter>
#include <QPrintDialog>
#include <QPrintPreviewDialog>
#include <QPrinter>

OneBayPerPageScanPlanPrinter::OneBayPerPageScanPlanPrinter(document_t* document, pool_t* pool, const ange::schedule::Call* printedCall, PrintMode printMode, QObject* parent)
  : AbstractScanplanPrinter(QString(), document, pool, printedCall, printMode,parent)
{
    Q_ASSERT(document);
    Q_ASSERT(document->vessel());

    m_header_text = QString("Detailed Load Scanplan -- %1 %2 %3").arg(document->vessel()->name()).arg(m_printedCall->uncode())
                           .arg(m_printedCall->voyageCode());

    m_printScene->set_print_bay_numbers(true);

    // aft first to ensure that the aft bay in 40' only bays gets overwritten
    Q_FOREACH (BaySliceGraphicsItem* aftSlice, m_printScene->vessel_item()->aft_slices()) {
        if (aftSlice) {
            m_baysliceitems.insert(aftSlice->baySlice()->bays().last(), aftSlice);
            aftSlice->setVisible(false);
        }
    }
    Q_FOREACH (BaySliceGraphicsItem* foreSlice, m_printScene->vessel_item()->fore_slices()) {
        if (foreSlice) {
            m_baysliceitems.insert(foreSlice->baySlice()->bays().first(), foreSlice);
            foreSlice->setVisible(false);
        }
    }
}

void OneBayPerPageScanPlanPrinter::initPrinter(QPrinter* printer) {
    printer->setPaperSize(QPrinter::A4);
    printer->setOrientation(QPrinter::Portrait);
}

int OneBayPerPageScanPlanPrinter::pageCount() const {
    return m_baysliceitems.count();
}

void OneBayPerPageScanPlanPrinter::drawPage(QPainter* painter, QRectF rect, int pageNumber) {
    QMap<int, BaySliceGraphicsItem*>::iterator it = m_baysliceitems.begin();
    for (int i = 0 ; i < pageNumber; i++) {
        it++; // just move forward;
    }
    BaySliceGraphicsItem* item = it.value();
    item->setVisible(true);
    QRectF boundingRect = item->mapRectToScene(item->boundingRect());

    m_printScene->setSceneRect(boundingRect);
    m_printScene->render(painter, rect, boundingRect);
    item->setVisible(false);
}

#include "onebayperpagescanplanprinter.moc"
