/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2013
 Copyright: See COPYING file that comes with this distribution
*/

#include "datacubehtmlexporter.h"
#include <qdatacube/datacube.h>
#include <qdatacube/abstractaggregator.h>
#include <QSharedPointer>
#include <QVector>
#include <qdatacube/abstractformatter.h>

using namespace qdatacube;

QString DatacubeHtmlExporter::datacubeToHtml(const qdatacube::Datacube* datacube,
                                        const qdatacube::AbstractFormatter& outputFormatter,
                                        RecapFormat recapFormat) {
    QString htmlReport;
    if(!(datacube->rowCount() > 0) || !(datacube->columnCount() > 0)) {
        return htmlReport;
    }
    const int countHeaderRows = datacube->headerCount(Qt::Horizontal); // number of col headers in top row and summations in bottom row
    const int countHeaderCols = datacube->headerCount(Qt::Vertical);   // number of row headers in left column and summations in right column
    const int countDataRows = datacube->rowCount();
    const int countDataCols = datacube->columnCount();

    htmlReport += "<table border='1' cellpadding='3'  cellspacing='0'>\n";
    // horizontal headers
    for(int i = 0 ; i < countHeaderRows ; i++) {
        QList<Datacube::HeaderDescription > horizontalHeader = datacube->headers(Qt::Horizontal, i);
        AbstractAggregator::Ptr aggregator = datacube->columnAggregators().at(i);
        htmlReport += "<tr>";
        if(i == 0 && countHeaderCols > 0) { // place holder top left corner
            htmlReport += QString("<td rowspan=\"%1\" colspan = \"%2\"align=\"center\" valign=\"middle\"></td>")
                                .arg(countHeaderRows).arg(countHeaderCols);
        }
        for(int j = 0; j < horizontalHeader.size(); j++) {  // actual headers
            htmlReport += QString("<td colspan=\"%1\"  align=\"center\" valign=\"middle\" ><b>%2</b></td>")
                                .arg(horizontalHeader.at(j).span).arg(aggregator-> categoryHeaderData(horizontalHeader.at(j).categoryIndex).toString());
        }
        if(recapFormat & SHOWTOTALS) {
            if(i == 0 && countHeaderCols > 0) { // place holder top right corner
                htmlReport += QString("<td rowspan=\"%1\" colspan = \"%2\" align=\"center\" valign=\"middle\"></td>")
                                    .arg(countHeaderRows).arg(countHeaderCols);
            }
        }
        htmlReport += "</tr>\n";
    }

    // data rows
    // need to keep track of how many rows a specific vertical header fills to send correct rowspan value
    QVector<int> verticalHeaderRowCount(countHeaderCols, 0);
    QVector<int> verticalHeaderRowCountBackwards(countHeaderCols, 0);
    for(int i = 0; i < countDataRows; i++) {
        htmlReport += "<tr>";
        // vertical headers
        for(int j = 0; j < countHeaderCols; j++) { // vertical headers
            if (verticalHeaderRowCount[j] == 0) {
                QList<Datacube::HeaderDescription > verticalHeader = datacube->headers(Qt::Vertical, j);
                AbstractAggregator::Ptr aggregator = datacube->rowAggregators().at(j);
                int headerSection = datacube->toHeaderSection(Qt::Vertical, j, i);
                int rowsInHeader = verticalHeader.at(headerSection).span;
                verticalHeaderRowCount[j] = rowsInHeader;
                htmlReport += QString("<td rowspan=\"%1\"  align=\"center\" valign=\"middle\" ><b>%2</b></td>")
                                .arg(rowsInHeader).arg(aggregator->categoryHeaderData(verticalHeader.at(headerSection).categoryIndex).toString());
            }
            verticalHeaderRowCount[j] = verticalHeaderRowCount[j] - 1;
        }
        // data cells
        for(int j = 0; j < countDataCols; j++) { // numeric data
            QList<int> elements = datacube->elements(i,j);
            htmlReport += QString("<td align=\"right\" valign=\"middle\">%1</td>")
                                .arg(elements.isEmpty() ? QString() : outputFormatter.format(elements));
        }
        if(recapFormat & SHOWTOTALS) {
            // right hand summation cells
            for(int j = countHeaderCols-1; j >= 0; j--) { // row sums - backwards traversing
                if (verticalHeaderRowCountBackwards[j] == 0) {
                    QList<Datacube::HeaderDescription > verticalHeader = datacube->headers(Qt::Vertical, j);
                    int headerSection = datacube->toHeaderSection(Qt::Vertical, j, i);
                    int rowsInHeader = verticalHeader.at(headerSection).span;
                    verticalHeaderRowCountBackwards[j] = rowsInHeader;
                    htmlReport += QString("<td rowspan=\"%1\" align=\"right\" valign=\"middle\" ><b>%2</b></td>")
                                    .arg(rowsInHeader).arg(outputFormatter.format(datacube->elements(Qt::Vertical, j, headerSection)));
                }
                verticalHeaderRowCountBackwards[j] = verticalHeaderRowCountBackwards[j] - 1;
            }
        }
        htmlReport += "</tr>\n";
    }
    if(recapFormat & SHOWTOTALS) {
        // horizontal totals - run backwards through rows
        for(int i = countHeaderRows - 1 ; i >= 0 ; i--) {
            QList<Datacube::HeaderDescription > horizontalHeader = datacube->headers(Qt::Horizontal, i);
            htmlReport += "<tr>";
            if(i == countHeaderRows - 1) { // place holder bottom left corner - running backwards
                htmlReport += QString("<td rowspan=\"%1\" colspan = \"%2\"align=\"center\" valign=\"middle\"></td>")
                                    .arg(countHeaderRows).arg(countHeaderCols);
            }
            for(int j = 0; j < horizontalHeader.size(); j++) {  // actual sums
                htmlReport += QString("<td colspan=\"%1\"  align=\"%2\" valign=\"middle\" ><b>%3</b></td>")
                                .arg(horizontalHeader.at(j).span).arg(i==countHeaderRows-1?"right":"center")
                                .arg(outputFormatter.format(datacube->elements(Qt::Horizontal,i,j)));
            }
            if(i == countHeaderRows - 1) { // Total summary bottom right corner - running backwards
                htmlReport += QString("<td rowspan=\"%1\" colspan = \"%2\" align=\"%3\" valign=\"middle\"><b>%4</b></td>")
                                    .arg(countHeaderRows).arg(countHeaderCols).arg(i==countHeaderRows-1?"right":"center")
                                    .arg(outputFormatter.format(datacube->elements()));
            }
            htmlReport += "</tr>\n";
        }
    }

    htmlReport += "</table>\n";
    return htmlReport;
}
