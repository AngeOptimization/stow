#ifndef HTMLSECTIONEDREPORT_H
#define HTMLSECTIONEDREPORT_H

#include <QString>
#include <QTextStream>

class QTextDocument;

/**
 * Class for building the HTML reports, this is more complex than a QTextStream as we want to be able to build a TOC as
 * we add sections to the report.
 */
class HtmlSectionedReport {
public:
    HtmlSectionedReport(const QString& title);
    void addSection(const QString& sectionHeader, const QString& sectionHtml);
    QString toString() const;
    QTextDocument* toTextDocument(QObject* parent) const;
private:
    int m_sectionNumber;
    QString m_title;
    QString m_toc;
    QTextStream m_tocStream;
    QString m_sections;
    QTextStream m_sectionsStream;
};

#endif // HTMLSECTIONEDREPORT_H
