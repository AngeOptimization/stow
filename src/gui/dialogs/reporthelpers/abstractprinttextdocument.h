#ifndef ABSTRACTPRINTTEXTDOCUMENT_H
#define ABSTRACTPRINTTEXTDOCUMENT_H

#include <QObject>

class QTextDocument;
namespace ange {
namespace schedule {
class Call;
}
}

class document_t;
class AbstractPrintTextDocument : public QObject {
    Q_OBJECT

public Q_SLOTS:
        /**
         * Sends Html preview to printer
         */
        void printHtml();

        /**
         * Writes Html preview as PDF to \param filename
         */
        void writePdf(const QString& filename);

        /**
         * Opens window with Html preview
         */
        void viewHtml();

    protected:

        AbstractPrintTextDocument(document_t* document, const ange::schedule::Call* currentCall, QWidget* parent=0);
        const ange::schedule::Call* currentCall() const;
        document_t* document() const;

    private:

        /**
         * The text document generated for this thing
         *
         * \param parent is set as parent for the returned textDocument. if 0 is provided,
         * cleanup is left to the caller.
         */
        virtual QTextDocument* createTextDocument(QObject* parent) = 0;

        /**
         * The (window) title for this thing
         */
        virtual QString title() = 0;

        document_t* m_document;

        const ange::schedule::Call* m_currentCall;
};

#endif // ABSTRACTPRINTTEXTDOCUMENT_H
