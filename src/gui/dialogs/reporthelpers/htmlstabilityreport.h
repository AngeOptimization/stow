#ifndef HTMLSTABILITYREPORT_H
#define HTMLSTABILITYREPORT_H

#include "htmlsectionedreport.h"
class document_t;
namespace ange {
namespace schedule {
class Call;
}
namespace vessel {
class BlockWeight;
}
}

class HtmlStabilityReport : public HtmlSectionedReport {
public:
    HtmlStabilityReport (const document_t* document, const ange::schedule::Call* call, const QString& title);
    void sectionBlockWeights(const QString sectionTitle, const QList< ange::vessel::BlockWeight > blockWeights);
    void sectionCargoBays();
    void sectionDraftTrimList();
    void sectionObserverPosition();
    void sectionPortCall();
    void sectionStresses();
    void sectionTanks();
    void sectionVessel();
    void sectionWeightsMoments();
private:
    const document_t* m_document;
    const ange::schedule::Call* m_call;
};

#endif // HTMLSTABILITYREPORT_H
