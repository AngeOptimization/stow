#include "masterplanningreport.h"

#include "htmldisplaywindow.h"
#include "htmlsectionedreport.h"
#include "pluginmanager.h"

#include <ange/schedule/call.h>

#include <stowplugininterface/masterplanningreporterinterface.h>

MasterPlanningReport::MasterPlanningReport(PluginManager* pluginManager, const ange::schedule::Call* call)
    : m_pluginManager(pluginManager)
{
    HtmlSectionedReport report("Master Planning Report");

    QMap<QString, QString> reportSections; // Stored in QMap for sorting on the headers
    Q_FOREACH (ange::angelstow::MasterPlanningReporterInterface* masterPlanningReporter,
               m_pluginManager->masterPlanningReporterPlugins()) {
        reportSections.insertMulti(masterPlanningReporter->headerForMasterPlanningReport(),
                                   masterPlanningReporter->sectionForMasterPlanningReport(call));
    }
    for (QMap<QString, QString>::iterator it = reportSections.begin(); it != reportSections.end(); ++it) {
        report.addSection(it.key(), it.value());
    }
    m_html = report.toString();
}

void MasterPlanningReport::showWindow(QWidget* parent) {
    HtmlDisplayWindow* window = new HtmlDisplayWindow("Master Planning Report", m_html, parent);
    window->show();
}

#include "masterplanningreport.moc"
