/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2014
 Copyright: See COPYING file that comes with this distribution
*/
#ifndef SPECIALS_LIST_EXPORTER_H
#define SPECIALS_LIST_EXPORTER_H

#include "abstractprinttextdocument.h"

namespace ange {
    namespace schedule {
        class Call;
    }
}

class QTextDocument;
class document_t;
/**
 * A class to export the list of special containers (i.e. with handling/stow codes)
 */
class SpecialContainersListExporter : public AbstractPrintTextDocument {
    Q_OBJECT

public:
    SpecialContainersListExporter(document_t* document, const ange::schedule::Call* call, QWidget* parent = 0);

private:
    QTextDocument* createTextDocument(QObject* parent);
    virtual QString title();
};

#endif // SPECIALS_LIST_EXPORTER_H
