#include "htmlreportformatter.h"

#include <QStandardItemModel>
#include <QTextStream>

QString HtmlReportFormatter::reportHeader(const QString& reportName, const QString& vesselName,
                                          const QString& portAndVoyage) {
    QString header;
    header += QString("<h2>%1</h2>").arg(reportName);;
    header += QString("<h2>%1 %2</h2><br>").arg(vesselName).arg(portAndVoyage);
    return header;
}

QString HtmlReportFormatter::tableToHtml(const QAbstractItemModel* model) {
    const int rows = model->rowCount();
    const int columns = model->columnCount();
    QString tableSection;
    QTextStream outTable(&tableSection);
    outTable << QStringLiteral("<table border='1' cellpadding='3' cellspacing='0'>");
    outTable << QStringLiteral("<tr>");
    for(int j = 0; j < columns; ++j){
        outTable << QStringLiteral("<th>") << model->headerData(j, Qt::Horizontal).toString() << QStringLiteral("</th>");
    }
    outTable << QStringLiteral("</tr>");
    for(int i = 0; i < rows; ++i){
        outTable << QStringLiteral("<tr>");
        for(int j = 0; j < columns; ++j){
            outTable << QStringLiteral("<td>") << model->index(i,j).data(Qt::DisplayRole).toString() << QStringLiteral("</td>");
        }
        outTable << QStringLiteral("</tr>");
    }
    outTable << QStringLiteral("</table>");
    return tableSection;
}

QString HtmlReportFormatter::reportFooter(const QAbstractItemModel* model) {
    return QString("<p>Container count:%1</p>").arg(model->rowCount());
}
