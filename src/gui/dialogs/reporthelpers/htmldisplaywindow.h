#ifndef HTML_DISPLAY_WINDOW_H
#define HTML_DISPLAY_WINDOW_H

#include <QWidget>

/**
 * @brief Window for previewing HTML documents
 *
 * This is an attemt to make a general widget for previewing and printing HTML, right now it only supports
 * preview. Later this should be changed to a QPrintPreviewWindow with the HTML stored in a QTextDocument.
 */
class HtmlDisplayWindow : public QWidget {
    Q_OBJECT
public:
    /**
     * Create window with the HTML to render
     */
    HtmlDisplayWindow(const QString& title, const QString& html, QWidget* parent = 0);
private:
    QString m_title;
    QString m_html;
};

#endif