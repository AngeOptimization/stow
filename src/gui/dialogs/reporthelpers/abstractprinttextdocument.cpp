#include "abstractprinttextdocument.h"
#include <gui/utils/dialogutils.h>
#include <document/document.h>
#include <QPrinter>
#include <QPrintDialog>
#include <QHBoxLayout>
#include <QTextBrowser>
#include <QTextDocument>


AbstractPrintTextDocument::AbstractPrintTextDocument(document_t* document, const ange::schedule::Call* currentCall, QWidget* parent)
  : QObject(parent), m_document(document), m_currentCall(currentCall)
{
    setObjectName("AbstractPrintTextDocument");
    connect(m_document, SIGNAL(destroyed(QObject*)), SLOT(deleteLater()));
}

const ange::schedule::Call* AbstractPrintTextDocument::currentCall() const {
    return m_currentCall;
}

document_t* AbstractPrintTextDocument::document() const {
    return m_document;
}

void AbstractPrintTextDocument::printHtml() {
    QPrinter printer;
    printer.setPaperSize(QPrinter::A4);
    printer.setOrientation(QPrinter::Portrait);
    QPrintDialog printdialog(&printer);
    DialogUtils::makeDialogMaximizable(&printdialog);
    if (QTextDocument* doc = createTextDocument(&printdialog)) {
        if (printdialog.exec() == QDialog::Accepted) {
            doc->print(&printer);
        }
    }
}

void AbstractPrintTextDocument::viewHtml() {

    // TODO: Merge AbstractPrintTextDocument with class HtmlSectionedReport ??? #1535
    QDialog d;
    d.setWindowTitle(title());
    DialogUtils::makeDialogMaximizable(&d);
    QTextBrowser* view = new QTextBrowser(&d);
    QHBoxLayout* lay = new QHBoxLayout();
    d.setLayout(lay);
    lay->addWidget(view);
    if (QTextDocument* doc = createTextDocument(view)) {
        view->setDocument(doc);
        view->setReadOnly(true);
        int newWidth = view->fontMetrics().averageCharWidth() * 120;
        d.resize(newWidth, newWidth * 21 / 13);  // 21/13 ~= Golden ratio
        d.exec();
    }
}

void AbstractPrintTextDocument::writePdf(const QString& filename)  {
    QPrinter printer(QPrinter::HighResolution);
    printer.setPaperSize(QPrinter::A4);
    printer.setOrientation(QPrinter::Landscape);
    printer.setOutputFileName(filename);
    printer.setOutputFormat(QPrinter::PdfFormat);
    if (QTextDocument* doc = createTextDocument(0)) {
        doc->print(&printer);
        delete doc;
    }
}

#include "abstractprinttextdocument.moc"
