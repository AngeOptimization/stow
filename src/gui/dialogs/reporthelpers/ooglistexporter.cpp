#include "ooglistexporter.h"
#include <document/document.h>
#include <document/stowage/stowage.h>
#include <document/stowage/container_leg.h>
#include <document/containers/container_list.h>
#include <ange/containers/container.h>
#include <ange/containers/oog.h>
#include <QTextDocument>
#include <QWidget>
#include <ange/vessel/vessel.h>
#include <ange/schedule/call.h>
#include "htmlreportformatter.h"
#include <QStandardItemModel>

using namespace ange::units;
using ange::containers::Container;

OogListExporter::OogListExporter(document_t* document, const ange::schedule::Call* currentCall, QWidget* parent): AbstractPrintTextDocument(document, currentCall, parent) {

}

QString OogListExporter::title() {
    return "OOG Load List";
}

QTextDocument* OogListExporter::createTextDocument(QObject* parent) {
    QStandardItemModel model;
    model.setHorizontalHeaderItem(0, new QStandardItem("Equipment No"));
    model.setHorizontalHeaderItem(1, new QStandardItem("Length"));
    model.setHorizontalHeaderItem(2, new QStandardItem("Type"));
    model.setHorizontalHeaderItem(3, new QStandardItem("Weight"));
    model.setHorizontalHeaderItem(4, new QStandardItem("POD"));
    model.setHorizontalHeaderItem(5, new QStandardItem("Position"));
    model.setHorizontalHeaderItem(6, new QStandardItem("OOG Top"));
    model.setHorizontalHeaderItem(7, new QStandardItem("OOG Left"));
    model.setHorizontalHeaderItem(8, new QStandardItem("OOG Right"));
    model.setHorizontalHeaderItem(9, new QStandardItem("OOG Front"));
    model.setHorizontalHeaderItem(10, new QStandardItem("OOG Back"));
    const ContainerLeg* routes = document()->stowage()->container_routes();
    Q_FOREACH(const Container* container, document()->containers()->list()) {
        if (container->oog() && routes->portOfLoad(container) == currentCall()) {
            const ange::vessel::BayRowTier brt = document()->stowage()->nominal_position(container, currentCall());
            if (!brt.placed()) {
                continue;
            }
            const double oogTop = container->oog().top() / centimeter;
            const double oogLeft = container->oog().left() / centimeter;
            const double oogRight = container->oog().right() / centimeter;
            const double oogFront = container->oog().front() / centimeter;
            const double oogBack = container->oog().back() / centimeter;
            QList< QStandardItem* > nextRow;
            nextRow << new QStandardItem(container->equipmentNumber());
            nextRow << new QStandardItem(QString::number(container->isoLength()));
            nextRow << new QStandardItem(container->isoCode().code());
            nextRow << new QStandardItem(QString::number(container->weight()/ton));
            nextRow << new QStandardItem(routes->portOfDischarge(container)->uncode());
            nextRow << new QStandardItem(brt.toString());
            nextRow << new QStandardItem(oogTop   > 0.0? QString::number(oogTop)   : QString());
            nextRow << new QStandardItem(oogLeft  > 0.0? QString::number(oogLeft)  : QString());
            nextRow << new QStandardItem(oogRight > 0.0? QString::number(oogRight) : QString());
            nextRow << new QStandardItem(oogFront > 0.0? QString::number(oogFront) : QString());
            nextRow << new QStandardItem(oogBack  > 0.0? QString::number(oogBack)  : QString());
            model.appendRow(nextRow);
        }
    }
    QString headerSection = HtmlReportFormatter::reportHeader(title(), document()->vessel()->name(),
                                  QString("%1 %2").arg(currentCall()->uncode()).arg(currentCall()->voyageCode()));
    QString oogTable = HtmlReportFormatter::tableToHtml(&model);
    QString footerSection = HtmlReportFormatter::reportFooter(&model);
    QTextDocument* oogReport = new QTextDocument(parent);
    oogReport->setHtml(headerSection + oogTable + footerSection);
    return oogReport;
}

#include "ooglistexporter.moc"
