#include "stabilityreport.h"

#include "document/document.h"
#include "document/stowage/stowage.h"
#include "document/stowage/stability/stresses.h"
#include "stabilityforcer.h"
#include "htmlstabilityreport.h"

#include <ange/schedule/call.h>
#include <ange/vessel/blockweight.h>
#include <ange/vessel/vessel.h>

#include <QString>
#include <QTextDocument>

StabilityReport::StabilityReport(document_t* document, const ange::schedule::Call* call, QWidget* parent):
AbstractPrintTextDocument(document, call, parent) {
}

QString StabilityReport::title() {
    return QStringLiteral("Stability Report");
}

QTextDocument* StabilityReport::createTextDocument(QObject* parent) {
    HtmlStabilityReport report(document(), currentCall(), title());
    report.sectionVessel();
    report.sectionPortCall();
    report.sectionDraftTrimList();
    report.sectionObserverPosition();
    report.sectionWeightsMoments();
    report.sectionStresses();
    // Lightship weight and LCG should not be summed over BlockWeight vessel->lightshipWeights(), as these do not add up to the total lightship weight
    report.sectionBlockWeights(QStringLiteral("Lightship Weights (corrected)"), document()->stresses_bending()->lightshipWeights());
    report.sectionBlockWeights(QStringLiteral("Constant Weights"), document()->vessel()->constantWeights());
    QList<ange::vessel::BlockWeight> deadLoadList;
    deadLoadList << document()->stabilityForcer()->deadLoad(currentCall());
    report.sectionBlockWeights(QStringLiteral("Deadload Weight Adjustment"), deadLoadList);
    report.sectionCargoBays();
    report.sectionTanks();

    return report.toTextDocument(parent);
}

#include "stabilityreport.moc"
