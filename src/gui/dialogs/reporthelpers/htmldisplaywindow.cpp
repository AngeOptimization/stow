#include "htmldisplaywindow.h"
#include <QHBoxLayout>
#include <QTextBrowser>

HtmlDisplayWindow::HtmlDisplayWindow(const QString& title, const QString& html, QWidget* parent)
    : QWidget(parent, Qt::Window), m_title(title), m_html(html) {
    setAttribute(Qt::WA_DeleteOnClose, true);
    setWindowTitle(m_title);
    QTextBrowser* view = new QTextBrowser(this);
    QHBoxLayout* layout = new QHBoxLayout(this);
    setLayout(layout);
    layout->addWidget(view);
    QTextDocument* document = new QTextDocument(view);
    document->setHtml(m_html);
    view->setDocument(document);
    view->setLineWrapMode(QTextEdit::NoWrap); //Note: this seems to be the only way to impose wrapping
                                              //and causes significant speed-ups (x100) on large documents
    view->setReadOnly(true);
    int newWidth = view->fontMetrics().averageCharWidth() * 120;
    resize(newWidth, newWidth * 21 / 13);  // 21/13 ~= Golden ratio
}
