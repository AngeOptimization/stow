/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2013
 Copyright: See COPYING file that comes with this distribution
*/

#ifndef DATACUBEHTMLEXPORTER_H
#define DATACUBEHTMLEXPORTER_H

#include <QString>
namespace qdatacube {
    class Datacube;
    class AbstractFormatter;
}

/**
 * A class providing functionality to represent a Databube as an html table
 * Move this to Datacube when mature.
 */
class DatacubeHtmlExporter {
public:
    enum RecapFormat {
        NOTOTALS = 0x0,
        SHOWTOTALS = 0x1
    };
    static QString datacubeToHtml(const qdatacube::Datacube* datacube,
                                  const qdatacube::AbstractFormatter& outputFormatter,
                                  RecapFormat recapFormat = SHOWTOTALS);
};

#endif // DATACUBEHTMLEXPORTER_H
