#ifndef ONEBAYPERPAGESCANPLANPRINTER_H
#define ONEBAYPERPAGESCANPLANPRINTER_H

#include "abstractscanplanprinter.h"

class QPrinter;
class BaySliceGraphicsItem;
class pool_t;
class document_t;
class vessel_scene_t;

namespace ange {
    namespace schedule {
        class Call;
    };
};
class OneBayPerPageScanPlanPrinter : public AbstractScanplanPrinter {
    Q_OBJECT
    public:
        explicit OneBayPerPageScanPlanPrinter(document_t* document, pool_t* pool, const ange::schedule::Call* printedCall,
                                              PrintMode printMode, QObject* parent = 0);
        virtual int pageCount() const Q_DECL_OVERRIDE;
    protected:
        virtual void drawPage(QPainter* painter, QRectF rect, int pageNumber) Q_DECL_OVERRIDE;
    private:
        virtual void initPrinter(QPrinter* printer);
    private:
        QMap<int, BaySliceGraphicsItem*> m_baysliceitems;
};

#endif // ONEBAYPERPAGESCANPLANPRINTER_H
