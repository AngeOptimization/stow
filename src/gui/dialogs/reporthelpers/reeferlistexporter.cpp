#include "reeferlistexporter.h"
#include <document/document.h>
#include <document/stowage/stowage.h>
#include <document/stowage/container_leg.h>
#include <document/containers/container_list.h>
#include <ange/containers/container.h>
#include <ange/vessel/vessel.h>
#include <ange/schedule/call.h>
#include <QTextDocument>
#include "htmlreportformatter.h"
#include <QtGui/QStandardItemModel>

using namespace ange::units;
using ange::containers::Container;

ReeferListExporter::ReeferListExporter(document_t* document, const ange::schedule::Call* currentCall, QWidget* parent): AbstractPrintTextDocument(document, currentCall, parent) {

}

QString ReeferListExporter::title() {
    return QStringLiteral("Reefer Load List");
}

QTextDocument* ReeferListExporter::createTextDocument(QObject* parent) {
    QStandardItemModel model;
    model.setHorizontalHeaderItem(0, new QStandardItem("Equipment No"));
    model.setHorizontalHeaderItem(1, new QStandardItem("Length"));
    model.setHorizontalHeaderItem(2, new QStandardItem("Type"));
    model.setHorizontalHeaderItem(3, new QStandardItem("Weight"));
    model.setHorizontalHeaderItem(4, new QStandardItem("POD"));
    model.setHorizontalHeaderItem(5, new QStandardItem("Position"));
    model.setHorizontalHeaderItem(6, new QStandardItem("Temperature"));
    const ContainerLeg* routes = document()->stowage()->container_routes();
    Q_FOREACH(const Container* container, document()->containers()->list()) {
        if ( (container->live() == Container::Live) && routes->portOfLoad(container) == currentCall()) {
            const ange::vessel::BayRowTier brt = document()->stowage()->nominal_position(container, currentCall());
            if (!brt.placed()) {
                continue;
            }
            QList< QStandardItem* > nextRow;
            nextRow << new QStandardItem(container->equipmentNumber());
            nextRow << new QStandardItem(QString::number(container->isoLength()));
            nextRow << new QStandardItem(container->isoCode().code());
            nextRow << new QStandardItem(QString::number(container->weight()/ton));
            nextRow << new QStandardItem(routes->portOfDischarge(container)->uncode());
            nextRow << new QStandardItem(brt.toString());
            nextRow << new QStandardItem(QString::number(container->temperature()));
            model.appendRow(nextRow);
        }
    }
    QString headerSection = HtmlReportFormatter::reportHeader("Reefer Load List", document()->vessel()->name(),
                                  QString("%1 %2").arg(currentCall()->uncode()).arg(currentCall()->voyageCode()));
    QString reeferTable = HtmlReportFormatter::tableToHtml(&model);
    QString footerSection = HtmlReportFormatter::reportFooter(&model);
    QTextDocument* rv = new QTextDocument(parent);
    rv->setHtml(headerSection + reeferTable + footerSection);
    return rv;
}

#include "reeferlistexporter.moc"
