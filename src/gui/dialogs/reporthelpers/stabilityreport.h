
#ifndef STABILITYREPORT_H
#define STABILITYREPORT_H

#include "abstractprinttextdocument.h"

namespace ange {
    namespace schedule {
        class Call;
    }
}

class QString;
class QTextDocument;
class document_t;

class StabilityReport : public AbstractPrintTextDocument {
    Q_OBJECT

public:
    StabilityReport(document_t* document, const ange::schedule::Call* call, QWidget* parent = 0);

private:
    QTextDocument* createTextDocument(QObject* parent);
    virtual QString title();
};

#endif // STABILITYREPORT_H
