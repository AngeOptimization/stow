#include "htmlsectionedreport.h"
#include <QString>
#include <QTextDocument>
#include <QTextStream>
#include <qnumeric.h> // qFinite(qreal) - workaround for a Qt 5.2 bug

HtmlSectionedReport::HtmlSectionedReport(const QString& title)
: m_sectionNumber(0), m_title(title), m_tocStream(&m_toc), m_sectionsStream(&m_sections) {
}

void HtmlSectionedReport::addSection(const QString& sectionHeader, const QString& sectionHtml) {
    m_sectionNumber += 1;
    QString anchor = QString("section_%1").arg(m_sectionNumber);
    m_tocStream << QStringLiteral("<li><a href='#") << anchor << QStringLiteral("'>") << sectionHeader << QStringLiteral("</a></li>\n");
    m_sectionsStream << QStringLiteral("<a name='") << anchor << QStringLiteral("'></a>\n");
    m_sectionsStream << QStringLiteral("<h2>") << sectionHeader << QStringLiteral("</h2>\n");
    m_sectionsStream << sectionHtml;
}

QString HtmlSectionedReport::toString() const {
    QString html;
    QTextStream htmlStream(&html);
    htmlStream << QStringLiteral("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n");
    htmlStream << QStringLiteral("<html>\n");
    htmlStream << QStringLiteral("<head>\n");
    htmlStream << QStringLiteral("<title>") << m_title << QStringLiteral("</title>\n");
    htmlStream << QStringLiteral("</head>\n");
    htmlStream << QStringLiteral("<body style=\"font-size: 10pt;\">\n");
    htmlStream << QStringLiteral("<h1>") << m_title << QStringLiteral("</h1>\n");

    htmlStream << QStringLiteral("<ul>");
    htmlStream << m_toc;
    htmlStream << QStringLiteral("</ul>");

    htmlStream << m_sections;

    htmlStream << QStringLiteral("</body>");
    htmlStream << QStringLiteral("</html>");
    return html;
}

QTextDocument* HtmlSectionedReport::toTextDocument(QObject* parent) const {
    QTextDocument* td = new QTextDocument(parent);
    td->setHtml(toString());
    return td;
}