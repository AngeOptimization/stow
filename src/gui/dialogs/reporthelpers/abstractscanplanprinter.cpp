#include "abstractscanplanprinter.h"

#include "gui/vesselview/vesselscene.h"
#include "gui/vesselview/vesselgraphicsitem.h"
#include "gui/utils/dialogutils.h"
#include <document.h>
#include <length_aggregator.h>
#include <container_list.h>
#include <stowagestatusaggregator.h>
#include <dischargecallaggregator.h>
#include <currentcallfilter.h>
#include <ange/schedule/schedule.h>
#include <QPrinter>
#include <QPrintDialog>
#include <QPrintPreviewDialog>
#include <QPainter>
#include <QTextDocument>
#include <qdatacube/datacube.h>
#include <qdatacube/filterbyaggregate.h>

using ange::schedule::Call;
using namespace qdatacube;

AbstractScanplanPrinter::AbstractScanplanPrinter(QString title, document_t* document, pool_t* pool,
                                                 const ange::schedule::Call* printedCall, PrintMode printMode,
                                                 QObject* parent)
  : QObject(parent), m_document(document), m_printScene(new vessel_scene_t(document,pool)), m_printedCall(printedCall),
    m_header_text(title)
{
    setObjectName("AbstractScanplanPrinter");
    Q_ASSERT(document);
    Q_ASSERT(document->vessel());
    m_printScene->set_active_call(printedCall);
    m_printScene->setPrintMode(printMode);
    m_printScene->vessel_item()->set_rows(2);
    m_printScene->set_print_bay_numbers(true);
    Q_FOREACH(Call* call, document->schedule()->calls()) {
        if (call != printedCall) {
            m_printScene->remove_call(call);
        }
    }
    if (printedCall->eta().isValid() && printedCall->etd().isValid()) {
        m_eta_etd = printedCall->eta().toString(Qt::DefaultLocaleShortDate) + " -- "
                                                 + printedCall->etd().toString(Qt::DefaultLocaleShortDate);
    }
}

void AbstractScanplanPrinter::print() {
    QPrinter printer(QPrinter::HighResolution);
    initPrinter(&printer);
    QPrintDialog dlg(&printer);
    dlg.setWindowTitle("Print file");
    DialogUtils::makeDialogMaximizable(&dlg);
    if (dlg.exec() == QDialog::Accepted) {
        print_scene(&printer);
    }
}

bool AbstractScanplanPrinter::writePdf(QString filename) {
    QPrinter printer(QPrinter::HighResolution);
    initPrinter(&printer);
    if (!filename.isEmpty()) {
        printer.setOutputFileName(filename);
        printer.setOutputFormat(QPrinter::PdfFormat);
        return print_scene(&printer);
    }
    return false;
}

void AbstractScanplanPrinter::print_preview() {
    QPrinter printer(QPrinter::HighResolution);
    initPrinter(&printer);
    QPrintPreviewDialog preview(&printer);
    DialogUtils::makeDialogMaximizable(&preview);
    connect(&preview, SIGNAL(paintRequested(QPrinter*)), this, SLOT(print_scene(QPrinter*)));
    preview.exec();
}


bool AbstractScanplanPrinter::print_scene(QPrinter* printer) {
    QPainter painter;
    if(!painter.begin(printer)) {
        return false;
    }
    // get some information about the rect we are to draw in
    const qreal linespacing = painter.fontMetrics().lineSpacing();
    QRect pagerect(0,0,printer->width(), printer->height());
    QFont headerfont = painter.font();
    headerfont.setPointSize(headerfont.pointSize()*1.2);
    headerfont.setBold(true);
    QFontMetrics headerfont_metrics = QFontMetrics(headerfont);
    // Draw header
    QFont default_font = painter.font();
    // Create html documents (for legend and summary)
    QTextDocument* summary_doc = create_summary_doc(this);
    summary_doc->setDefaultFont(painter.font());
    QRectF pageRect = printer->pageRect();
    QRectF headerRect(pageRect.left(), pageRect.top(), pageRect.width(), headerfont_metrics.lineSpacing());
    QRectF subheaderRect;
    if(!m_eta_etd.isEmpty()) {
          subheaderRect = QRect(pageRect.left(), pageRect.top()+headerfont_metrics.lineSpacing(),
                             pageRect.width(), linespacing);
    }

    int headerHeights = headerRect.height() + (subheaderRect.isNull() ? 0 : subheaderRect.height());

    QRectF contentRect(pageRect.left(), pageRect.top() + headerHeights, pageRect.width(), pageRect.height() - headerHeights);

    for(int i = 0 ; i < pageCount(); i++) {
        painter.setFont(headerfont);
        painter.drawText(headerRect,
                        Qt::AlignCenter, m_header_text);
        painter.setFont(default_font);
        if (!m_eta_etd.isEmpty()) {
            painter.drawText(subheaderRect, Qt::AlignCenter, m_eta_etd);
        }
       // m_print_scene->render(&painter, contentRect);
        drawPage(&painter, contentRect, i);
        printer->newPage();
    }
    painter.setFont(headerfont);
    painter.drawText(headerRect,
                     Qt::AlignCenter, m_header_text);
    painter.setFont(default_font);
    if (!m_eta_etd.isEmpty()) {
        painter.drawText(subheaderRect, Qt::AlignCenter, m_eta_etd);
    }
    painter.translate(contentRect.topLeft());
    summary_doc->drawContents(&painter);
    return true;
}


QTextDocument* AbstractScanplanPrinter::create_summary_doc(QObject* parent) {
    UserConfiguration* config = m_printScene->document()->userconfiguration();
    QSharedPointer<LengthAggregator> lengthAggregator(
                                           new LengthAggregator(m_printScene->document()->containers()));
    QSharedPointer<StowageStatusAggregator> stowageStatusAggregator(
                                           new StowageStatusAggregator(m_printScene->document()->containers()));
    QSharedPointer<DischargeCallAggregator> destinationAggregator(new DischargeCallAggregator(m_printScene->document()->containers(), m_printScene->document()->schedule(), config ));
    QSharedPointer<CurrentCallFilter> traversingFilter(new CurrentCallFilter(m_printScene->document()->containers()));
    const Call* currentCall = m_printScene->current_call();
    PrintMode printMode = m_printScene->printMode();
    QString summaryType;
    if (printMode.scanPlanType==mode_enum_t::LOAD) {
        stowageStatusAggregator->setCurrentCall(currentCall);
        traversingFilter->setCurrentCall(currentCall);
        summaryType = "Load";
    } else {
        Q_ASSERT(currentCall != m_printScene->document()->schedule()->calls().last());
        stowageStatusAggregator->setCurrentCall(m_printScene->document()->schedule()->next(currentCall));
        traversingFilter->setCurrentCall(m_printScene->document()->schedule()->next(currentCall));
        summaryType = "Discharge";
    }
    Datacube datacube(m_printScene->document()->containers(), destinationAggregator, lengthAggregator);
    datacube.addFilter(traversingFilter);
    if (printMode.scanPlanType == mode_enum_t::LOAD) {
        datacube.addFilter(AbstractFilter::Ptr(new FilterByAggregate(stowageStatusAggregator, QString("LOA"))));
    } else {
        datacube.addFilter(AbstractFilter::Ptr(new FilterByAggregate(stowageStatusAggregator, QString("DSC"))));
    }
    Q_ASSERT(datacube.headerCount(Qt::Vertical) == 1);
    QList< Datacube::HeaderDescription > verticalCallHeaders = datacube.headers(Qt::Vertical, 0);

    QHash<const ange::schedule::Call*, QString> dischargeCallLetters;
    QString summary;
    if(datacube.rowCount() > 0 && datacube.columnCount() > 0) {
        summary = "<h2>"+ summaryType + " Summary</h2><table  cellpadding='50' border='1'>\n";
        // horizontal headers
        for(int i = 0 ; i < datacube.headerCount(Qt::Horizontal) ; i++) {
            QList<Datacube::HeaderDescription > horizontalHeader = datacube.headers(Qt::Horizontal, i);
            AbstractAggregator::Ptr aggregator = datacube.columnAggregators().at(i);

            summary += "<tr>";
            if(i == 0) {
                // place holder top left corner
                summary += QString("<td rowspan=\"%1\" colspan = \"%2\"></td>").arg(datacube.headerCount(Qt::Horizontal)).arg(datacube.headerCount(Qt::Vertical));
            }
            for(int j = 0; j < horizontalHeader.size(); ++j) {
                // actual headers
                summary += QString("<td colspan=\"%1\"  align=\"center\">%2</td>").arg(horizontalHeader.at(j).span).arg(aggregator->categoryHeaderData(horizontalHeader.at(j).categoryIndex).toString());
            }
            if(i == 0) {
                // place holder top right corner
                summary += QString("<td rowspan=\"%1\" colspan = \"%2\" align=\"center\" valign=\"middle\">Total</td>").arg(datacube.headerCount(Qt::Horizontal)).arg(datacube.headerCount(Qt::Vertical));
            }
            summary += "</tr>";

        }
        summary += "</tr>\n";
        QSet<QString> usedDischargeCallLetters;
        for(int i = 0; i < verticalCallHeaders.size(); ++i) {
            summary += "<tr>";
            //vertical headers
            int categoryIndex = verticalCallHeaders.at(i).categoryIndex;
            QString callHeader = destinationAggregator->categoryHeaderData(categoryIndex, DischargeCallAggregator::FULL_UNCODE).toString();
            if (printMode.displayDischargeCallLetter) {
                QString candidates = destinationAggregator->categoryHeaderData(categoryIndex).toString() + callHeader + "123456789*";
                const Call* call = destinationAggregator->categoryHeaderData(categoryIndex, Qt::UserRole).value<Call*>();
                usedDischargeCallLetters.insert("*"); // Use * as the fallback if all else fails, last in candidates
                QString dischargeCallLetter;
                do {
                    dischargeCallLetter = candidates.left(1);
                    candidates.remove(0, 1);
                } while (usedDischargeCallLetters.contains(dischargeCallLetter) && !candidates.isEmpty());
                usedDischargeCallLetters.insert(dischargeCallLetter);
                dischargeCallLetters[call] = dischargeCallLetter;
                callHeader = dischargeCallLetter + " - " + callHeader;
            }
            QColor background = printMode.whiteContainers ? Qt::white : destinationAggregator->categoryHeaderData(categoryIndex,Qt::BackgroundRole).value<QColor>();
            QColor foreground = printMode.whiteContainers ? Qt::black : destinationAggregator->categoryHeaderData(categoryIndex,Qt::ForegroundRole).value<QColor>();
            summary += QString("<th bgcolor=\"%1\" style=\"color:%2\">%3</th>").arg(background.name()).arg(foreground.name()).arg(callHeader);
            //the content
            for(int j = 0; j < datacube.columnCount(); ++j) {
                summary += "<td align=\"right\">";
                summary += QString("%1").arg(datacube.elements(i,j).size());
                summary += "</td>";
            }
            // right totals
            summary+="<td align = \"right\">";
            summary+=QString("%1").arg(datacube.elements(Qt::Vertical,0,i).size());
            summary+="</td>";
            summary += "</tr>\n";
        }
        bool first = true;
        for(int i = datacube.headerCount(Qt::Horizontal)-1 ; i >= 0 ; i--) {
            summary += "<tr>";
            if(first) {
                // lower left corner place holder
                summary += QString("<td rowspan=\"%1\" colspan = \"%2\" valign=\"middle\" align=\"center\">Total</td>").arg(datacube.headerCount(Qt::Horizontal)).arg(datacube.headerCount(Qt::Vertical));
            }
            // bottom totals
            QList<Datacube::HeaderDescription > horizontalHeader = datacube.headers(Qt::Horizontal, i);
            for(int j = 0; j < horizontalHeader.size(); ++j) {
                summary += QString("<td colspan=\"%1\" align=\"%3\">%2</td>").arg(horizontalHeader.at(j).span)
                    .arg(datacube.elements(Qt::Horizontal,i,j).size())
                    .arg(horizontalHeader.at(j).span == 1 ? "right" : "center");
            }
            if(first) {
                // grand totals
                summary += QString("<td rowspan=\"%1\" colspan = \"%2\" valign=\"middle\" align=\"right\">%3</td>").arg(datacube.headerCount(Qt::Horizontal)).arg(datacube.headerCount(Qt::Vertical)).arg(datacube.elements().size());
                first = false;
            }
            summary += "</tr>";
        }
        summary += "</table>\n";
    }
    if (printMode.displayDischargeCallLetter) {
        printMode.dischargeCallLetters = dischargeCallLetters;
        m_printScene->setPrintMode(printMode);
    }
    QTextDocument* rv = new QTextDocument(parent);
    rv->setHtml(summary);
    return rv;
}

AbstractScanplanPrinter::~AbstractScanplanPrinter() {

}


#include "abstractscanplanprinter.moc"
