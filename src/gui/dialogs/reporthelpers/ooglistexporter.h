/*
 *
 */

#ifndef EXPORTOOGLIST_H
#define EXPORTOOGLIST_H

#include "abstractprinttextdocument.h"

namespace ange {
namespace schedule {
class Call;
}
}

class document_t;
class OogListExporter : public AbstractPrintTextDocument
{
    Q_OBJECT
    public:
        OogListExporter(document_t* document, const ange::schedule::Call* currentCall, QWidget* parent = 0);
    private:
        virtual QTextDocument* createTextDocument(QObject* parent);
        virtual QString title();
};

#endif // EXPORTOOGLIST_H
