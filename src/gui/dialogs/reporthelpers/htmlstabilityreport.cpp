#include "htmlstabilityreport.h"

#include "document.h"
#include "stability.h"
#include "stabilityforcer.h"
#include "stowage.h"
#include "stresses.h"
#include "tankconditions.h"

#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>
#include <ange/units/units.h>
#include <ange/vessel/bayslice.h>
#include <ange/vessel/bilinearinterpolator.h>
#include <ange/vessel/blockweight.h>
#include <ange/vessel/slottablesummary.h>
#include <ange/vessel/vessel.h>
#include <ange/vessel/vesseltank.h>

#include <QPointF>

#include <cmath>

using namespace ange::units;
using ange::vessel::BlockWeight;
using ange::vessel::Vessel;
using ange::vessel::BaySlice;

static qreal rad2Deg(double rad) {
    return rad / M_PI * 180.0;
}

static QString formatFinite(double value, int fieldWidth, char format, int precision) {
    return (qIsFinite(value)) ?  QString("%1").arg(value, fieldWidth, format, precision) : QStringLiteral("");
}

class HtmlBlockWeights {
public:
    static void headerRowBlockWeights(QTextStream& htmlStream, const QString& headerName) {
        htmlStream
        << QStringLiteral("<tr>")
        << QString("<th>%1</th>").arg(headerName)
        << QStringLiteral("<th>Weight[t]</th>")
        << QStringLiteral("<th>Aft[m]</th>")
        << QStringLiteral("<th>LCG[m]</th>")
        << QStringLiteral("<th>Fore[m]</th>")
        << QStringLiteral("<th>TCG[m]</th>")
        << QStringLiteral("<th>VCG[m]</th>")
        << QStringLiteral("<th>LMom[tm]</th>")
        << QStringLiteral("<th>TMom[tm]</th>")
        << QStringLiteral("<th>VMom[tm]</th>")
        << QStringLiteral("<th>Aft[t/m]</th>\n")
        << QStringLiteral("<th>Fore[t/m]</th>")
        << QStringLiteral("</tr>\n");
    }

    static void dataRowBlockWeights(QTextStream& htmlStream, const QString& description, const BlockWeight& blockWeight) {
        htmlStream
        << QStringLiteral("<tr>")
        << QString("<td>%1</td>").arg(description)
        << QString("<td align='right'>%1</td>").arg(blockWeight.weight() / ton, 0, 'f', 2)
        << QString("<td align='right'>%1</td>").arg(blockWeight.aftLimit()/meter, 0, 'f', 2)
        << QString("<td align='right'>%1</td>").arg(formatFinite(blockWeight.lcg() / meter, 0, 'f', 2))
        << QString("<td align='right'>%1</td>").arg(blockWeight.foreLimit()/meter, 0, 'f', 2)
        << QString("<td align='right'>%1</td>").arg(formatFinite(blockWeight.tcg() / meter, 0, 'f', 2))
        << QString("<td align='right'>%1</td>").arg(formatFinite(blockWeight.vcg() / meter, 0, 'f', 2))
        << QString("<td align='right'>%1</td>").arg(blockWeight.longitudinalMoment() / tonmeter, 0, 'f', 0)
        << QString("<td align='right'>%1</td>").arg(blockWeight.transverseMoment() / tonmeter, 0, 'f', 0)
        << QString("<td align='right'>%1</td>").arg(blockWeight.verticalMoment() / tonmeter, 0, 'f', 0)
        << QString("<td align='right'>%1</td>").arg(formatFinite(blockWeight.aftDensity()/ton_per_meter, 0, 'f', 2))
        << QString("<td align='right'>%1</td>").arg(formatFinite(blockWeight.foreDensity()/ton_per_meter, 0, 'f', 2))
        << QStringLiteral("</tr>\n");
    }
};

HtmlStabilityReport::HtmlStabilityReport(const document_t* document, const ange::schedule::Call* call, const QString& title) :
    HtmlSectionedReport(title), m_document(document), m_call(call) {
}

void HtmlStabilityReport::sectionBlockWeights(const QString sectionTitle, const QList< ange::vessel::BlockWeight > blockWeights) {

    QString html;
    QTextStream htmlStream(&html);
    htmlStream << QStringLiteral("<table summary='' border='1' cellpadding='3' cellspacing='0'>\n");

    HtmlBlockWeights::headerRowBlockWeights(htmlStream, sectionTitle);

    Q_FOREACH(const BlockWeight blockWeight, blockWeights) {
        HtmlBlockWeights::dataRowBlockWeights(htmlStream, blockWeight.description(), blockWeight);
    }

    BlockWeight totalBlock = BlockWeight::totalBlockWeight(blockWeights);
    HtmlBlockWeights::dataRowBlockWeights(htmlStream, sectionTitle + QStringLiteral(" (total)"), totalBlock);

    htmlStream << QStringLiteral("</table>\n");
    addSection(sectionTitle, html);
}

void HtmlStabilityReport::sectionCargoBays() {

    QString html;
    QTextStream htmlStream(&html);
    htmlStream << QStringLiteral("<table summary='' border='1' cellpadding='3' cellspacing='0'>\n");

    HtmlBlockWeights::headerRowBlockWeights(htmlStream, QStringLiteral("Cargo bays"));

    BlockWeight totalBlock;
    const Vessel* vessel = m_document->vessel();
    Q_FOREACH(const BaySlice * baySlice, vessel->baySlices()) {
        QString bayName = "";
        Q_FOREACH(const int bay, baySlice->bays()){
            bayName += QString::number(bay) + " ";
        }
        const BlockWeight cargoBlock = m_document->stowage()->bay_block_weights(m_call).value(baySlice);
        HtmlBlockWeights::dataRowBlockWeights(htmlStream, bayName, cargoBlock);
        totalBlock.extendWith(cargoBlock);
    }

    HtmlBlockWeights::dataRowBlockWeights(htmlStream, QStringLiteral("Cargo Bays (total)"), totalBlock);

    htmlStream << QStringLiteral("</table>\n");
    addSection(QStringLiteral("Cargo Bays"), html);
};


void HtmlStabilityReport::sectionDraftTrimList() {
    const StabilityData* stabilityData = m_document->stability()->data(m_call);
    QString html;
    QTextStream htmlStream(&html);
    htmlStream << QStringLiteral("<table summary='' border='1' cellpadding='3' cellspacing='0'>\n");

    htmlStream
    << QStringLiteral("<tr>")
    << QStringLiteral("<th>Draft [m]</th>")
    << QStringLiteral("<th>Trim [m]</th>")
    << QStringLiteral("<th>List [deg]</th>")
    << QStringLiteral("<th>LCG [m]</th>")
    << QStringLiteral("<th>TCG [m]</th>")
    << QStringLiteral("<th>VCG [m]</th>")
    << QStringLiteral("<th>Free surface [m]</th>")
    << QStringLiteral("<th>KM [m]</th>")
    << QStringLiteral("<th>GM [m]</th>")
    << QStringLiteral("</tr>\n");
    htmlStream
    << QStringLiteral("<tr>")
    << QString("<td>%1</td>").arg(stabilityData->draft / meter, 0, 'f', 2)
    << QString("<td>%1</td>").arg(stabilityData->trim / meter, 0, 'f', 2)
    << QString("<td>%1</td>").arg(rad2Deg(stabilityData->list), 0, 'f', 2)
    << QString("<td>%1</td>").arg(stabilityData->totalLcg / meter, 0, 'f', 2)
    << QString("<td>%1</td>").arg(stabilityData->totalTcg / meter, 0, 'f', 2)
    << QString("<td>%1</td>").arg(stabilityData->totalVcg / meter, 0, 'f', 2)
    << QString("<td>%1</td>").arg(stabilityData->freeSurfaceCorrection / meter, 0, 'f', 2)
    << QString("<td>%1</td>").arg(stabilityData->km / meter, 0, 'f', 2)
    << QString("<td>%1</td>").arg(stabilityData->gm / meter, 0, 'f', 2)
    << QStringLiteral("</tr>\n");

    htmlStream << QStringLiteral("</table>\n");
    addSection(QStringLiteral("Draft, Trim, List, CG, FS, KM and GM"), html);
}

void HtmlStabilityReport::sectionObserverPosition() {
    QString html;
    QTextStream htmlStream(&html);
    htmlStream << QStringLiteral("<table summary='' border='1' cellpadding='3' cellspacing='0'>\n");
    htmlStream
    << QStringLiteral("<tr>")
    << QStringLiteral("<th>Lcg [m]</th>")
    << QStringLiteral("<th>Vcg [m]</th>")
    << QStringLiteral("</tr>\n");
    htmlStream
    << QStringLiteral("<tr>")
    << QString("<td>%1</td>").arg(m_document->vessel()->observerLcg() / meter, 0, 'f', 2)
    << QString("<td>%1</td>").arg(m_document->vessel()->observerVcg() / meter, 0, 'f', 2)
    << QStringLiteral("</tr>\n");

    htmlStream << QStringLiteral("</table>\n");
    addSection(QStringLiteral("Observer Position"), html);
}

void HtmlStabilityReport::sectionPortCall() {
    QString html;
    QTextStream htmlStream(&html);
    htmlStream << QStringLiteral("<table summary='' border='1' cellpadding='3' cellspacing='0'>\n");

    htmlStream
    << QStringLiteral("<tr>")
    << QStringLiteral("<th>Voyage</th>")
    << QStringLiteral("<th>Call</th>")
    << QStringLiteral("<th>ETA</th>")
    << QStringLiteral("<th>ETD</th>")
    << QStringLiteral("</tr>\n");
    htmlStream
    << QStringLiteral("<tr>")
    << QString("<td>%1</td>").arg(m_call->voyageCode())
    << QString("<td>%1</td>").arg(m_call->uncode())
    << QString("<td>%1</td>").arg(m_call->eta().toString(Qt::DefaultLocaleShortDate))
    << QString("<td>%1</td>").arg(m_call->etd().toString(Qt::DefaultLocaleShortDate))
    << QStringLiteral("</tr>\n");

    htmlStream << QStringLiteral("</table>\n");
    addSection(QStringLiteral("Port Call"), html);
}

struct StressSummary {
    Length longitudinalPos;
    Torque bendingMin;
    Torque bendingAct;
    Torque bendingMax;
    qreal relativeBending;
    Force shearMin;
    Force shearAct;
    Force shearMax;
    qreal relativeShear;
    Torque torsionMin;
    Torque torsionAct;
    Torque torsionMax;
    qreal relativeTorsion;
};

void HtmlStabilityReport::sectionStresses() {
    QString html;
    QTextStream htmlStream(&html);
    htmlStream << QStringLiteral("<table summary='' border='1' cellpadding='3' cellspacing='0'>\n");

    htmlStream
    << QStringLiteral("<tr>")
    << QStringLiteral("<th>Longitudinal Pos. [m]</th>")
    << QStringLiteral("<th>Bending Min.[tm]</th>")
    << QStringLiteral("<th>Bending Act.[tm]</th>")
    << QStringLiteral("<th>Bending Max.[tm]</th>")
    << QStringLiteral("<th>Bending [%]</th>")
    << QStringLiteral("<th>Shear Min.[t]</th>")
    << QStringLiteral("<th>Shear Act.[t]</th>")
    << QStringLiteral("<th>Shear Max.[t]</th>")
    << QStringLiteral("<th>Shear [%]</th>")
    << QStringLiteral("<th>Torsion Min.[tm]</th>")
    << QStringLiteral("<th>Torsion Act.[tm]</th>")
    << QStringLiteral("<th>Torsion Max.[tm]</th>")
    << QStringLiteral("<th>Torsion [%]</th>")
    << QStringLiteral("</tr>\n");

    const Stresses* stresses = m_document->stresses_bending();
    const Vessel* vessel = m_document->vessel();
    QMap<Length, StressSummary> stressTable;
    for(int i = 0; i < stresses->relativeBendingCurve(m_call).count(); ++i) {
        StressSummary summary;
        //we don't (and won't) support differing frame values for the various stress curves.
        Q_ASSERT((stresses->relativeBendingCurve(m_call).at(i).x() == stresses->relativeShearForceCurve(m_call).at(i).x())
        && (stresses->relativeBendingCurve(m_call).at(i).x() == stresses->relativeTorsionCurve(m_call).at(i).x()));

        qreal longitudinalPos = stresses->relativeBendingCurve(m_call).at(i).x();
        summary.longitudinalPos = longitudinalPos * meter;

        summary.bendingMin = vessel->minBendingLimitsData().valueAt(longitudinalPos, 0.0) * newtonmeter;
        summary.bendingMax = vessel->maxBendingLimitsData().valueAt(longitudinalPos, 0.0) * newtonmeter;
        summary.relativeBending = stresses->relativeBendingCurve(m_call).at(i).y();
        summary.bendingAct = summary.relativeBending * summary.bendingMax / 100.0; // relative is in percent

        summary.shearMin = vessel->minShearForceLimitsData().valueAt(longitudinalPos, 0.0) * newton;
        summary.shearMax = vessel->maxShearForceLimitsData().valueAt(longitudinalPos, 0.0) * newton;
        summary.relativeShear = stresses->relativeShearForceCurve(m_call).at(i).y();
        summary.shearAct = summary.relativeShear * summary.shearMax / 100.0; // relative is in percent

        summary.torsionMin = vessel->minTorsionLimitsData().valueAt(longitudinalPos, 0.0) * newtonmeter;
        summary.torsionMax = vessel->maxTorsionLimitsData().valueAt(longitudinalPos, 0.0) * newtonmeter;
        summary.relativeTorsion = stresses->relativeTorsionCurve(m_call).at(i).y();
        summary.torsionAct = summary.relativeTorsion * summary.torsionMax / 100.0; // relative is in percent

        stressTable.insert(summary.longitudinalPos, summary);
    }

    const Acceleration gravidityConstant = 9.81 * meter / second2;

    Q_FOREACH(const StressSummary summary, stressTable) {
        htmlStream
        << QStringLiteral("<tr>")
        << QString("<td>%1</td>").arg(summary.longitudinalPos / meter)
        << QString("<td>%1</td>").arg(summary.bendingMin / (gravidityConstant * tonmeter), 0, 'f', 0)
        << QString("<td>%1</td>").arg(summary.bendingAct / (gravidityConstant * tonmeter), 0, 'f', 0)
        << QString("<td>%1</td>").arg(summary.bendingMax / (gravidityConstant * tonmeter), 0, 'f', 0)
        << QString("<td>%1</td>").arg(summary.relativeBending, 0, 'f', 2)
        << QString("<td>%1</td>").arg(summary.shearMin / (gravidityConstant * ton), 0, 'f', 0)
        << QString("<td>%1</td>").arg(summary.shearAct / (gravidityConstant * ton), 0, 'f', 0)
        << QString("<td>%1</td>").arg(summary.shearMax / (gravidityConstant * ton), 0, 'f', 0)
        << QString("<td>%1</td>").arg(summary.relativeShear, 0, 'f', 2)
        << QString("<td>%1</td>").arg(summary.torsionMin / (gravidityConstant * tonmeter), 0, 'f', 0)
        << QString("<td>%1</td>").arg(summary.torsionAct / (gravidityConstant * tonmeter), 0, 'f', 0)
        << QString("<td>%1</td>").arg(summary.torsionMax / (gravidityConstant * tonmeter), 0, 'f', 0)
        << QString("<td>%1</td>").arg(summary.relativeTorsion, 0, 'f', 2)
        << QStringLiteral("</tr>\n");
    }

    htmlStream << QStringLiteral("</table>\n");
    addSection(QStringLiteral("Stresses"), html);
}

void HtmlStabilityReport::sectionTanks() {
    class HtmlTanks {
    public:
        static void formatHtmlTableTankRow(QTextStream& htmlStream, const QString& description, const QString& group,
                                           bool isBallast, Mass maxWeight, MassMoment fsm, Density density,
                                           const BlockWeight& tankBlock) {
            htmlStream
            << "<tr>"
            << QString("<td align='center'>%1</td>").arg(description)
            << QString("<td align='right'>%1</td>").arg(group)
            << QString("<td align='right'>%1</td>").arg(isBallast ? QStringLiteral("Yes") : QStringLiteral("No"))
            << QString("<td align='right'>%1</td>").arg(maxWeight / ton, 0, 'f', 2)
            << QString("<td align='right'>%1</td>").arg(tankBlock.weight() / ton, 0, 'f', 2)
            << QString("<td align='right'>%1</td>").arg((tankBlock.weight() / maxWeight) * 100, 0, 'f', 2) // fill %
            << QString("<td align='right'>%1</td>").arg(tankBlock.aftLimit() / meter, 0, 'f', 2)
            << QString("<td align='right'>%1</td>").arg(formatFinite(tankBlock.lcg() / meter, 0, 'f', 2))
            << QString("<td align='right'>%1</td>").arg(tankBlock.foreLimit() / meter, 0, 'f', 2)
            << QString("<td align='right'>%1</td>").arg(formatFinite(tankBlock.tcg() / meter, 0, 'f', 2))
            << QString("<td align='right'>%1</td>").arg(formatFinite(tankBlock.vcg() / meter, 0, 'f', 2))
            << QString("<td align='right'>%1</td>").arg(tankBlock.longitudinalMoment() / tonmeter, 0, 'f', 0)
            << QString("<td align='right'>%1</td>").arg(tankBlock.transverseMoment() / tonmeter, 0, 'f', 0)
            << QString("<td align='right'>%1</td>").arg(tankBlock.verticalMoment() / tonmeter, 0, 'f', 0)
            << QString("<td align='right'>%1</td>").arg(fsm / tonmeter, 0, 'f', 0)
            << QString("<td align='right'>%1</td>").arg(density / (ton/meter3), 0, 'f', 3)
            << "</tr>\n";
        }
    };

    const StabilityData* stabilityData = m_document->stability()->data(m_call);

    QString html;
    QTextStream htmlStream(&html);
    htmlStream << QStringLiteral("<table summary='' border='1' cellpadding='3' cellspacing='0'>\n");

    htmlStream
    << QStringLiteral("<tr>")
    << QStringLiteral("<th>Name</th>")
    << QStringLiteral("<th>Group</th>")
    << QStringLiteral("<th>Ballast</th>")
    << QStringLiteral("<th>Max weight [t]</th>")
    << QStringLiteral("<th>Weight [t]</th>")
    << QStringLiteral("<th>Fill [%]</th>")
    << QStringLiteral("<th>Aft [m]</th>")
    << QStringLiteral("<th>LCG [m]</th>")
    << QStringLiteral("<th>Fore [m]</th>")
    << QStringLiteral("<th>TCG [m]</th>")
    << QStringLiteral("<th>VCG [m]</th>")
    << QStringLiteral("<th>LMom [tm]</th>")
    << QStringLiteral("<th>TMom [tm]</th>")
    << QStringLiteral("<th>VMom [tm]</th>")
    << QStringLiteral("<th>FSM [tm]</th>")
    << QStringLiteral("<th>Density [t/m^3]</th>")
    << QStringLiteral("</tr>\n");

    BlockWeight tanksTotalBlock;
    MassMoment totalFreeSurfaceMoment;
    Mass totalWeight;
    Mass totalMaxWeight;
    Q_FOREACH(const TankData & tankData, stabilityData->tanksData) {
        Mass tankWeight = m_document->tankConditions()->tankCondition(m_call, tankData.tank);
        totalWeight += tankWeight;
        Mass tankMaxWeight = tankData.tank->massCapacity() * kilogram;
        totalMaxWeight += tankMaxWeight;
        Density tankDensity = (tankData.tank->density() / 1000.0) * ton/meter3;
        BlockWeight tankBlock(tankData.tank->lcg(tankWeight) * meter, tankData.tank->aftEnd() * meter, tankData.tank->foreEnd() * meter,
                              tankData.tank->vcg(tankWeight) * meter, tankData.tank->tcg(tankWeight) * meter, tankWeight);
        tanksTotalBlock.extendWith(tankBlock);
        totalFreeSurfaceMoment += tankData.freeSurfaceMoment;
        HtmlTanks::formatHtmlTableTankRow(htmlStream, tankData.tank->description(), tankData.tank->tankGroup(),
                                          tankData.tank->isBallast(), tankMaxWeight, tankData.freeSurfaceMoment,
                                          tankDensity, tankBlock);
    }
    HtmlTanks::formatHtmlTableTankRow(htmlStream, "Tanks (total)", "all", false, totalMaxWeight, totalFreeSurfaceMoment,
                                      0.0 * ton/meter3, tanksTotalBlock);

    htmlStream << QStringLiteral("</table>\n");
    addSection(QStringLiteral("Tanks"), html);
}

void HtmlStabilityReport::sectionVessel() {
    QString html;
    QTextStream htmlStream(&html);
    htmlStream << QStringLiteral("<table summary='' border='1' cellpadding='3' cellspacing='0'>\n");

    htmlStream << QStringLiteral("<tr>")
    << QStringLiteral("<th>Vessel</th>")
    << QStringLiteral("<th>Code</th>")
    << QStringLiteral("<th>IMO</th>")
    << QStringLiteral("<th>Callsign</th>")
    << QStringLiteral("<th>Classification Society</th>")
    << QStringLiteral("<th>Max TEU</th>")
    << QStringLiteral("<th>Max reefer plugs</th>")
    << QStringLiteral("</tr>\n");

    const ange::vessel::SlotTableSummary& slotSummary = m_document->vessel()->slotTableSummary();
    htmlStream
    << QStringLiteral("<tr>")
    << QString("<td>%1</td>").arg(m_document->vessel()->name())
    << QString("<td>%1</td>").arg(m_document->vessel()->vesselCode())
    << QString("<td>%1</td>").arg(m_document->vessel()->imoNumber())
    << QString("<td>%1</td>").arg(m_document->vessel()->callSign())
    << QString("<td>%1</td>").arg(m_document->vessel()->classificationSociety())
    << QString("<td>%1</td>").arg(slotSummary.totalTEU())
    << QString("<td>%1</td>").arg(slotSummary.reeferPlugs())
    << QStringLiteral("</tr>\n");

    htmlStream << QStringLiteral("</table>\n");
    addSection(QStringLiteral("Vessel"), html);
}

void HtmlStabilityReport::sectionWeightsMoments() {

    QString html;
    QTextStream htmlStream(&html);
    htmlStream << QStringLiteral("<table summary='' border='1' cellpadding='3' cellspacing='0'>\n");

    HtmlBlockWeights::headerRowBlockWeights(htmlStream, QStringLiteral("Type"));

    const StabilityData* data = m_document->stability()->data(m_call);
    const Vessel* vessel = m_document->vessel();

#if 0 // Debugging only
    const BlockWeight lightProfileBlock = BlockWeight::totalBlockWeight(m_document->vessel()->lightshipWeights());
    HtmlBlockWeights::dataRowBlockWeights(htmlStream, QStringLiteral("Lightship (profile)"), lightProfileBlock);
#endif

    // Lightship weight and LCG should not be summed over BlockWeight vessel->lightshipWeights(), as these do not add up to the total lightship weight
    const BlockWeight lightCorrectedBlock = BlockWeight::totalBlockWeight(m_document->stresses_bending()->lightshipWeights());
    HtmlBlockWeights::dataRowBlockWeights(htmlStream, QStringLiteral("Lightship (corrected)"), lightCorrectedBlock);

    const BlockWeight constBlock = BlockWeight::totalBlockWeight(vessel->constantWeights());
    HtmlBlockWeights::dataRowBlockWeights(htmlStream, QStringLiteral("Constant weights"), constBlock);

    const BlockWeight deadLoadBlock = m_document->stabilityForcer()->deadLoad(m_call);
    HtmlBlockWeights::dataRowBlockWeights(htmlStream, QStringLiteral("Deadload adjustment"), deadLoadBlock);

    const BlockWeight tankBallastBlock = BlockWeight::totalBlockWeight(m_document->stresses_bending()->ballastTankWeights(m_call));
    HtmlBlockWeights::dataRowBlockWeights(htmlStream, QStringLiteral("Tanks (ballast)"), tankBallastBlock);

    //WeightAndMoments otherTankWcm;
    BlockWeight tankOtherBlock;
    Q_FOREACH(const ange::vessel::VesselTank * tank, vessel->tanks()) {
        if (tank->isBallast()) {
            continue;
        }

        Mass tankWeight = m_document->tankConditions()->tankCondition(m_call, tank);
        const BlockWeight tankBlock(tank->lcg(tankWeight) * meter, tank->aftEnd() * meter, tank->foreEnd() * meter,
            tank->vcg(tankWeight) * meter, tank->tcg(tankWeight) * meter, tankWeight);
        tankOtherBlock.extendWith(tankBlock);
    }
    HtmlBlockWeights::dataRowBlockWeights(htmlStream, QStringLiteral("Tanks (other)"), tankOtherBlock);

#if 0 // Debugging only
    const BlockWeight tankAllBlock = BlockWeight::totalBlockWeight(m_document->stowage()->stresses_bending()->tankWeights(m_call));
    HtmlBlockWeights::dataRowBlockWeights(htmlStream, QStringLiteral("Tanks (all)"), tankAllBlock);
#endif

    BlockWeight cargoBlock = BlockWeight::totalBlockWeight(m_document->stowage()->bay_block_weights(m_call).values());
    HtmlBlockWeights::dataRowBlockWeights(htmlStream, QStringLiteral("Cargo"), cargoBlock);

    BlockWeight totalBlock;
    totalBlock.extendWith(lightCorrectedBlock);
    totalBlock.extendWith(constBlock);
    totalBlock.extendWith(deadLoadBlock);
    totalBlock.extendWith(tankBallastBlock );
    totalBlock.extendWith(tankOtherBlock);
    totalBlock.extendWith(cargoBlock);
#if 0 // Debugging only
    // great for testing that all weight and moment changes have been captured in signals+slots
    // totalBlock output must be equal to displacementBlock output
    HtmlBlockWeights::dataRowBlockWeights(htmlStream, QStringLiteral("Light(corrected)+Const+Dead+Tanks+Cargo"), totalBlock);
#endif

    // used aft and fore from totalBlock
    const BlockWeight displacementBlock(data->totalLcg, totalBlock.aftLimit(), totalBlock.foreLimit(), data->totalVcg, data->totalTcg, data->totalWeight);
    HtmlBlockWeights::dataRowBlockWeights(htmlStream, QStringLiteral("Displacement (total)"), displacementBlock);

    htmlStream << QStringLiteral("</table>\n");
    addSection(QStringLiteral("Weights and Moments"), html);
}
