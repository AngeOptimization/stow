#ifndef SCANPLAN_PRINTER_H
#define SCANPLAN_PRINTER_H

#include <abstractscanplanprinter.h>
#include <QScopedPointer>
#include <QHash>
#include <QColor>
#include "gui/mode.h"

class QTextDocument;
class vessel_scene_t;
class pool_t;
class document_t;
namespace ange {
  namespace schedule {
    class Call;
  }
}
class QPrinter;
class QAbstractItemView;

class ScanplanPrinter : public AbstractScanplanPrinter {
    Q_OBJECT
    public:
        ScanplanPrinter(QString title, document_t* document, pool_t* pool, const ange::schedule::Call* printedCall, PrintMode printMode, QObject* parent = 0);
    virtual ~ScanplanPrinter();
        virtual int pageCount() const Q_DECL_OVERRIDE;
    protected:
        virtual void drawPage(QPainter* painter, QRectF rect, int pageNumber) Q_DECL_OVERRIDE;
    private:
        virtual void initPrinter(QPrinter* printer) Q_DECL_OVERRIDE;

};

#endif // SCANPLAN_PRINTER_H
