#ifndef EXPORT_RESTOW_LIST_H
#define EXPORT_RESTOW_LIST_H

#include "abstractprinttextdocument.h"

class document_t;
class QTextStream;
class QTextDocument;
namespace ange {
namespace schedule {
class Call;
}
namespace vessel {
class BayRowTier;
}
}

/**
 * Write a report table showing the all the containers restowed in current call
 */
class ExportRestowList : public AbstractPrintTextDocument {

    Q_OBJECT

public:
    ExportRestowList(document_t* document, const ange::schedule::Call* currentCall, QWidget* parent=0);

private:
    virtual QTextDocument* createTextDocument(QObject* parent);
    virtual QString title();

};

#endif // EXPORT_RESTOW_LIST_H
