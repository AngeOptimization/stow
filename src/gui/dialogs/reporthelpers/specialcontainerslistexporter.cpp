#include "specialcontainerslistexporter.h"
#include "document/document.h"
#include "document/containers/container_list.h"
#include "document/stowage/stowage.h"
#include "document/stowage/container_leg.h"

#include <ange/containers/container.h>
#include <ange/schedule/call.h>
#include <QTextEdit>
#include <ange/vessel/slot.h>
#include <ange/vessel/vessel.h>
#include <ange/vessel/bayslice.h>
#include <QFileDialog>
#include <QHBoxLayout>
#include <QStandardItemModel>
#include "htmlreportformatter.h"

using namespace ange::units;
using ange::containers::Container;

SpecialContainersListExporter::SpecialContainersListExporter(document_t* document, const ange::schedule::Call* call, QWidget* parent):
AbstractPrintTextDocument(document, call, parent)
{
}

QString SpecialContainersListExporter::title() {
    return QStringLiteral("Special Containers Load List");
}

QTextDocument* SpecialContainersListExporter::createTextDocument(QObject* parent) {
    QStandardItemModel model;
    model.setHorizontalHeaderItem(0, new QStandardItem("Equipment No"));
    model.setHorizontalHeaderItem(1, new QStandardItem("Length"));
    model.setHorizontalHeaderItem(2, new QStandardItem("Type"));
    model.setHorizontalHeaderItem(3, new QStandardItem("Weight"));
    model.setHorizontalHeaderItem(4, new QStandardItem("POD"));
    model.setHorizontalHeaderItem(5, new QStandardItem("Position"));
    model.setHorizontalHeaderItem(6, new QStandardItem("Handling Codes"));
    const ContainerLeg* routes = document()->stowage()->container_routes();
    Q_FOREACH(const Container* container, document()->containers()->list()) {
        if ( !container->handlingCodes().isEmpty() && routes->portOfLoad(container) == currentCall()) {
            const ange::vessel::BayRowTier brt = document()->stowage()->nominal_position(container, currentCall());
            if (!brt.placed()) {
                continue;
            }
            QString handlingCodes;
            bool isFirst = true;
            Q_FOREACH(const ange::containers::HandlingCode& handlingCode, container->handlingCodes()) {
                if(!isFirst) {
                    handlingCodes += ", ";
                }
                isFirst = false;
                handlingCodes += handlingCode.code();
            }
            QList< QStandardItem* > nextRow;
            nextRow << new QStandardItem(container->equipmentNumber());
            nextRow << new QStandardItem(QString::number(container->isoLength()));
            nextRow << new QStandardItem(container->isoCode().code());
            nextRow << new QStandardItem(QString::number(container->weight()/ton));
            nextRow << new QStandardItem(routes->portOfDischarge(container)->uncode());
            nextRow << new QStandardItem(brt.toString());
            nextRow << new QStandardItem(handlingCodes);
            model.appendRow(nextRow);
        }
    }
    QString headerSection = HtmlReportFormatter::reportHeader(title(), document()->vessel()->name(),
                                  QString("%1 %2").arg(currentCall()->uncode()).arg(currentCall()->voyageCode()));
    QString specialContainersTable = HtmlReportFormatter::tableToHtml(&model);
    QString footerSection = HtmlReportFormatter::reportFooter(&model);
    QTextDocument* rv = new QTextDocument(parent);
    rv->setHtml(headerSection + specialContainersTable + footerSection);
    return rv;
}

#include "specialcontainerslistexporter.moc"
