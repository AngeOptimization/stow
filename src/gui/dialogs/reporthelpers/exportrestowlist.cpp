#include "exportrestowlist.h"

#include "document/containers/container_list.h"
#include "document/document.h"
#include "document/stowage/container_leg.h"
#include "document/stowage/container_move.h"
#include "document/stowage/stowage.h"

#include <ange/containers/container.h>
#include <ange/containers/dangerousgoodscode.h>

#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>

#include <ange/vessel/vessel.h>

#include <QFileDialog>
#include <QHBoxLayout>
#include <QPrintDialog>
#include <QPrinter>
#include <QTextDocument>
#include <QTextEdit>
#include <QTextStream>


using namespace ange::units;
using ange::containers::Container;
using ange::schedule::Call;
using ange::containers::DangerousGoodsCode;

ExportRestowList::ExportRestowList(document_t* document, const Call* currentCall, QWidget* parent)
: AbstractPrintTextDocument(document, currentCall, parent)
{
}

QString ExportRestowList::title() {
    return QStringLiteral("Restow List");
}

static void writeBrtCell(QTextStream& out, ange::vessel::BayRowTier brt) {
    out << QStringLiteral("<td>");
    if (brt.placed()) {
        out << QString("%1%2%3").arg(brt.bay(),brt.bay()<100 ? 2 : 3,10,QChar('0')).arg(brt.row(),2,10,QChar('0'))
                                .arg(brt.tier(),2,10,QChar('0')); // TODO Use toString på brt  #1568
    } else {
        out << QStringLiteral("??????"); // TODO Use toString på brt  #1568
    }
    out << QStringLiteral("</td>");
}

QTextDocument* ExportRestowList::createTextDocument(QObject* parent) {
    const Call* currentCall = this->currentCall();
    if (!currentCall) {
        Q_ASSERT(false); // TODO #1523 probably this never happens, and currentCall() can be called directly in this function - delete this code if not triggered in future
        return 0L;
    }

    int containerCounter = 0;

    QString tableSection;
    QTextStream outTable(&tableSection);

    outTable << QStringLiteral("<table border='1' cellpadding='3' cellspacing='0'>")
    << QStringLiteral("<tr>")
    << QStringLiteral("<th>Equipment No</th><th>Length</th><th>Type</th><th>Weight</th><th>POL</th><th>POD</th><th>Arrival Slot</th><th>Departure Slot</th>")
    << QStringLiteral("<th>Imo Class</th><th>UN #</th><th>Imo Class</th><th>UN #</th><th>Imo Class</th><th>UN #</th>")
    << QStringLiteral("</tr>");

    const ContainerLeg* routes = document()->stowage()->container_routes();
    Q_FOREACH (const Container* container, document()->containers()->list()) {
        if (routes->portOfLoad(container)->distance(currentCall) < 1) {
            continue; // Loaded here or hereafter, cannot possibly be restowed
        }
        if (currentCall->distance(routes->portOfDischarge(container)) < 1) {
            continue; // Discharged here or or earlier; cannot possibly be restowed
        }
        const Call* previousCall = document()->schedule()->previous(currentCall);
        Q_ASSERT(previousCall); // Should not happen as we can't make reports in Befor
        const ange::vessel::BayRowTier previousBrt = document()->stowage()->nominal_position(container, previousCall);
        const ange::vessel::BayRowTier currentBrt = document()->stowage()->nominal_position(container, currentCall);
        if (previousBrt != currentBrt) {
            containerCounter += 1;
            outTable << QStringLiteral("<tr>")
                << QStringLiteral("<td>") << container->equipmentNumber() << QStringLiteral("</td>")
                << QStringLiteral("<td>") << container->isoLength() << QStringLiteral("</td>")
                << QStringLiteral("<td>") << container->isoCode().code() << QStringLiteral("</td>")
                << QStringLiteral("<td>") << container->weight()/ton << QStringLiteral("</td>")
                << QStringLiteral("<td>") << routes->portOfLoad(container)->uncode() << QStringLiteral("</td>")
                << QStringLiteral("<td>") << routes->portOfDischarge(container)->uncode() << QStringLiteral("</td>");
            writeBrtCell(outTable, previousBrt);
            writeBrtCell(outTable, currentBrt);
            Q_FOREACH(DangerousGoodsCode dg, container->dangerousGoodsCodes()) {
                outTable
                    << QStringLiteral("<td>") << dg.imdgClass() << QStringLiteral("</td>")
                    << QStringLiteral("<td>") << dg.unNumber() << QStringLiteral("</td>");
            }
            outTable << QStringLiteral("</tr>");
        }
    }
    outTable << QStringLiteral("</table>");

    QString headerSection;
    QTextStream outHeader(&headerSection);
    outHeader << QStringLiteral("<h2>Restow List</h2>")
    << QStringLiteral("<h2>")
    << document()->vessel()->name() << " " << currentCall->uncode()
    << QStringLiteral(" ") << currentCall->voyageCode()
    << QStringLiteral("</h2>")
    << QStringLiteral("<p>")
    << QStringLiteral("<b>Restow container count: ") << containerCounter << QStringLiteral("</b>")
    << QStringLiteral("</p>");

    QTextDocument* rv = new QTextDocument(parent);
    rv->setHtml(headerSection + tableSection);
    return rv;
}

#include "exportrestowlist.moc"
