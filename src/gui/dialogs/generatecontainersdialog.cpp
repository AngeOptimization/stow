#include "generatecontainersdialog.h"

#include "ui_generatecontainersdialog.h"
#include "generatecontainersmodel.h"
#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>
#include "../containerviewitemdelegates/emptydelegate.h"
#include "../containerviewitemdelegates/dischargecalldelegate.h"
#include "../containerviewitemdelegates/loadcalldelegate.h"
#include "../containerviewitemdelegates/weightdelegate.h"
#include "../containerviewitemdelegates/isocodedelegate.h"
#include "amountdelegate.h"

GenerateContainersDialog::GenerateContainersDialog(ange::schedule::Schedule* schedule, const ange::schedule::Call* currentCall,
                                                   ScheduleModel* scheduleModel, QWidget* parent)
  : QDialog(parent), m_ui(new Ui::GenerateContainersDialog)
{
    m_ui->setupUi(this);
    QTableView* tableView = m_ui->generateContainersTableView;
    m_model.reset(new GenerateContainersModel(schedule, currentCall));
    tableView->setModel(m_model.data());
    tableView->setItemDelegateForColumn(GenerateContainersModel::CONTAINER_COUNT, new AmountDelegate(this));
    tableView->setItemDelegateForColumn(GenerateContainersModel::ISO_NUMBER, new IsoCodeDelegate(this));
    tableView->setItemDelegateForColumn(GenerateContainersModel::LOAD_PORT,
                         new LoadCallDelegate(scheduleModel, schedule, GenerateContainersModel::DISCHARGE_PORT, this));
    tableView->setItemDelegateForColumn(GenerateContainersModel::DISCHARGE_PORT,
                         new DischargeCallDelegate(scheduleModel, schedule, GenerateContainersModel::LOAD_PORT, this));
    tableView->setItemDelegateForColumn(GenerateContainersModel::WEIGHT, new WeightDelegate(this));
    tableView->setItemDelegateForColumn(GenerateContainersModel::EMPTY_STATUS, new EmptyDelegate(this));

    connect(tableView->selectionModel(), &QItemSelectionModel::currentRowChanged,
            m_model.data(), &GenerateContainersModel::addBottomRow);
}

QList<ange::angelstow::ScheduledContainer> GenerateContainersDialog::containers() {
    return m_model->containers();
}
