#ifndef ABOUT_DIALOG_H
#define ABOUT_DIALOG_H

#include <QDialog>

class PluginManager;
namespace Ui {
class about_dialog;
};

class AboutDialog : public QDialog {

public:
    AboutDialog(PluginManager* manager);

private:
    Ui::about_dialog* m_ui;

};

#endif // ABOUT_DIALOG_H
