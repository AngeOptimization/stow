#ifndef EQIDMATCHINGUTILS
#define EQIDMATCHINGUTILS

#include <QStringList>

/**
 * A function to extract equipment IDs from a text blob
 */
QStringList extractEqIdsFromTextBlob(const QString& textBlob) {
    // The use case for mathing as broad as done here is to paste a COPRAR or BAPLIE file into the find containers
    // dialog and get all the containers from the file selected in the stowage.
    // Equipment ids matched: "ABCD1234567", "ABCD 1234567", "ABCD  1234567" (why with 2 spaces?)
    // Not matched: "A(BCDE1234567)8", "8(BCDE1234567)A"
    QStringList eqIdList;
    QRegExp regExp("(?:^|[^A-Z0-9])([A-Z]{4} {0,2}[0-9]{7})(?:[^A-Z0-9]|$)", Qt::CaseInsensitive);
    int pos = 0;
    while ((pos = regExp.indexIn(textBlob, pos, QRegExp::CaretAtOffset)) != -1) {
        eqIdList << regExp.cap(1).replace(" ", "");
        pos += regExp.matchedLength();
    }
    return eqIdList;
}

#endif
