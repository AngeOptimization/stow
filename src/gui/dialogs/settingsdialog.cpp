#include "settingsdialog.h"

#include "dialogutils.h"
#include "ui_settingsdialog.h"

#include <ange/containers/container.h>

#include <QDesktopServices>
#include <QFileDialog>
#include <QPushButton>
#include <QSettings>

using ange::containers::Container;

SettingsDialog::SettingsDialog(QWidget* parent, Qt::WindowFlags f)
  : QDialog(parent, f), m_ui(new Ui::SettingsDialog)
{
    m_ui->setupUi(this);
    QSettings settings;
    m_ui->organisaton_name_widget->setText(settings.value("user/organisation").toString());
    m_ui->two_rows_widget->setChecked(settings.value("rows per vessel").toInt() == 2);
    m_ui->show_equipment_numbers_widget->setChecked(settings.value("view/equipment number").toBool());
    m_ui->display_bulkheads_widget->setChecked(settings.value("view/display bulkheads").toBool());
    m_ui->display_visibilityline_widget->setChecked(settings.value("view/display visibilityline",false).toBool());
    m_ui->hide_nonactive_widget->setChecked(settings.value("view/hide_nonactive").toBool());
    m_ui->usePivotColoring->setChecked(settings.value("view/use pivot port based coloring", true).toBool());
    m_ui->linkForeAftSlots->setChecked(settings.value("link_fore_aft_slots", true).toBool());
    m_ui->displayContainerWeightCheckBox->setChecked(settings.value("behavior/display container weights", true).toBool());
    if(settings.contains("customconfigurationdir")) { //FIXME: why do we need this if()?
        m_ui->customConfigurationDirLineEdit->setText(settings.value("customconfigurationdir").toString());
    }
    QString carrier_code;
    if (settings.contains("user/carrier code")) {
        carrier_code = settings.value("user/carrier code").toString();
    } else {
        carrier_code = settings.value("carrier code").toString(); // Old setting //FIXME: remove
    }
    m_ui->defaultEDItempSpinBox->setValue(settings.value("user/default_EDI_export_temp", Container::unknownTemperature()).toDouble());
    m_ui->carrier_code_widget->setText(carrier_code);
    connect(m_ui->buttonBox->button(QDialogButtonBox::Apply), SIGNAL(clicked(bool)), SLOT(apply_settings()));
    connect(m_ui->buttonBox->button(QDialogButtonBox::Cancel), SIGNAL(clicked(bool)), SLOT(reject()));
    connect(m_ui->buttonBox->button(QDialogButtonBox::Ok), SIGNAL(clicked(bool)), SLOT(accept()));
    connect(m_ui->button_port_color_file, SIGNAL(clicked()), this, SLOT(showFileDialogForPortColorFile()));
    DialogUtils::makeDialogMaximizable(this);
}

void SettingsDialog::accept() {
    apply_settings();
    QDialog::accept();
}

namespace {
/**
  * Helper function.
  * @return true if setting was actually changed
*/
bool apply_setting(QSettings& settings, const QString& key, QVariant value) {
    if (!settings.contains(key) || settings.value(key) != value) {
        settings.setValue(key, value);
        return true;
    }
        return false;
    }
}

void SettingsDialog::apply_settings() {
    QSettings settings;
    settings.setValue("user/organisation", m_ui->organisaton_name_widget->text());
    settings.setValue("user/carrier code", m_ui->carrier_code_widget->text());
    if (apply_setting(settings, "rows per vessel", m_ui->two_rows_widget->isChecked() ? 2 : 1)) {
        emit show_2_rows(m_ui->two_rows_widget->isChecked());
    }
    if (apply_setting(settings, "view/equipment number", m_ui->show_equipment_numbers_widget->isChecked())) {
        emit show_equipment_number(m_ui->show_equipment_numbers_widget->isChecked());
    }
    const bool display_bulkheads = m_ui->display_bulkheads_widget->isChecked();
    if (apply_setting(settings, "view/display bulkheads", display_bulkheads)) {
        emit display_bulkheads_changed(display_bulkheads);
    }
    const bool display_visibilityline = m_ui->display_visibilityline_widget->isChecked();
    if (apply_setting(settings, "view/display visibilityline", display_visibilityline)) {
        emit display_visibilityline_changed(display_visibilityline);
    }
    const bool hide_nonactive = m_ui->hide_nonactive_widget->isChecked();
    if (apply_setting(settings, "view/hide_nonactive", hide_nonactive)) {
        emit hide_nonactive_changed(hide_nonactive);
    }
    const bool displayContainerWeight = m_ui->displayContainerWeightCheckBox->isChecked();
    if (apply_setting(settings, "behavior/display container weights", displayContainerWeight)) {
        emit displayContainerWeightChanged(displayContainerWeight);
    }
    const QString customConfigurationDir = m_ui->customConfigurationDirLineEdit->displayText();
    if(apply_setting(settings, "customconfigurationdir", customConfigurationDir)) {
        emit newColorFileSelected();
    }
    const bool usePivotColoring = m_ui->usePivotColoring->isChecked();
    if (apply_setting(settings, "view/use pivot port based coloring", usePivotColoring)) {
        emit usePivotBasedPortColoringChanged(usePivotColoring);
    }
    const bool linkForeAftSlots = m_ui->linkForeAftSlots->isChecked();
    if (apply_setting(settings, "link_fore_aft_slots", linkForeAftSlots)) {
        emit linkForeAftSlotsChanged(linkForeAftSlots);
    }
    settings.setValue("user/default_EDI_export_temp", m_ui->defaultEDItempSpinBox->value());
}

void SettingsDialog::set_show_equipment_id(bool state) {
    m_ui->show_equipment_numbers_widget->setChecked(state);
}

void SettingsDialog::set_show_visibilityline_changed(bool enabled) {
    m_ui->display_visibilityline_widget->setChecked(enabled);
}

void SettingsDialog::showFileDialogForPortColorFile() {
    QString path = m_ui->customConfigurationDirLineEdit->displayText();
    if(path.isEmpty()) {
        path = QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation);
    }
    QString newPath = QFileDialog::getExistingDirectory(this,"Select configuration directory",path);
    m_ui->customConfigurationDirLineEdit->setText(newPath);
}

#include "settingsdialog.moc"
