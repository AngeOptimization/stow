#ifndef SCHEDULEDCONTAINER_H
#define SCHEDULEDCONTAINER_H

namespace ange {
namespace schedule {
class Call;
}
namespace containers {
class Container;
}
}

namespace ange {
namespace angelstow {
/**
 * A struct container with its scheduled load and discharged calls
 * Currently only used to return values from the generate containers dialog.
 */
struct ScheduledContainer {
    ange::containers::Container* m_container;
    const ange::schedule::Call* m_portOfLoad;
    const ange::schedule::Call* m_portOfDischarge;
    ScheduledContainer(ange::containers::Container* container, const ange::schedule::Call* portOfLoad,
                       const ange::schedule::Call* portOfDischarge)
                       : m_container(container), m_portOfLoad(portOfLoad),m_portOfDischarge(portOfDischarge) {};
};

}
}
#endif //SCHEDULEDCONTAINER_H