/*
 *
 */

#ifndef NONMODALDIALOGHELPER_H
#define NONMODALDIALOGHELPER_H

#include <QtCore/QObject>
#include <QSharedPointer>
#include <QPointer>

class pool_t;
class QWidget;
class document_t;

class DialogInterface {
    public:
        /**
         * \return the widget used as a dialog. Ownership is the responsibility of the caller.
         */
        virtual QWidget* widget() const = 0;
        virtual ~DialogInterface() { };
};

// Still needed for getting current call into BaplieComparatorDialog
template <typename T> class DialogSetupper {
    public:
        virtual void setup(T*) { };
        virtual ~DialogSetupper() { };
};

/**
 * Templated class to help generate dialogs that takes a document and pool as constructors
 */
template <typename T> class DocumentPoolDialog : public DialogInterface {
    public:
        /**
         * \override
         */
        T* widget() const {
            T* w = new T(m_document, m_selectionPool, m_parentWidget);
            return w;
        }
        /**
         * Returns a shared pointer to a DialogInterface that can provide dialogs of type T
         */
        static QSharedPointer<DialogInterface> ptr(pool_t* selection_pool, document_t* document, QWidget* parent = 0) {
            return QSharedPointer<DialogInterface>(new DocumentPoolDialog(selection_pool, document, parent));
        }
    private:
        DocumentPoolDialog<T>(pool_t* selection_pool, document_t* document, QWidget* parent)
          : m_selectionPool(selection_pool), m_document(document), m_parentWidget(parent) {}
        pool_t* m_selectionPool;
        document_t* m_document;
        QWidget* m_parentWidget;
};

template <typename T> class DocumentDialog : public DialogInterface {
    public:
        /**
         * \override
         */
        T* widget() const {
            T* w = new T(m_document, m_parentWidget);
            if(m_setupper) {
                m_setupper->setup(w);
            }
            return w;
        }
        /**
         * Returns a shared pointer to a DialogInterface that can provide dialogs of type T
         */
        static QSharedPointer<DialogInterface> ptr(document_t* document, QWidget* parent = 0 ,QSharedPointer<DialogSetupper< T> > setupper = QSharedPointer<DialogSetupper<T> >()) {
            return QSharedPointer<DialogInterface>(new DocumentDialog(document, parent, setupper));
        }
    private:
        DocumentDialog<T>(document_t* document, QWidget* parent,  QSharedPointer<DialogSetupper<T> > setupper) : m_document(document), m_parentWidget(parent), m_setupper(setupper) {  }
        document_t* m_document;
        QWidget* m_parentWidget;
        QSharedPointer<DialogSetupper<T> > m_setupper;
};


/**
 * Class to help managing dialogs
 *
 * Creates the dialog on 'raise' if it doesn't exist, and brings it to front if it exists.
 * Ensures deletion (and thus recreation) on closing the dialog (and trying to open a new)
 */
class NonModalDialogHelper : public QObject {
    Q_OBJECT

    public:
        ~NonModalDialogHelper();
        NonModalDialogHelper(QSharedPointer<DialogInterface> dialogInterface,QObject* parent);
    public Q_SLOTS:
        void raise();
    private:
        QSharedPointer<DialogInterface> m_dialogInterface;
        QPointer<QWidget> m_dialog;
};

#endif // NONMODALDIALOGHELPER_H
