/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010

 Copyright: See COPYING file that comes with this distribution

*/

#include "reallydialog.h"
#include <QApplication>

reallydialog_t::reallydialog_t(QWidget* parent, QString name, QString message):
    QMessageBox(Warning,
                tr("%1 - %2").arg(QApplication::applicationName(), name),
                tr("%1").arg(message),
                StandardButtons(Save | Discard | Cancel),
                parent)
{

}
