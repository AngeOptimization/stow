#include "call_create_dialog.h"
#include <gui/utils/dialogutils.h>
#include "ui_call_create_dialog.h"
#include <QPushButton>
#include <ange/schedule/call.h>
#include <ange/schedule/cranerules.h>
#include "document/undo/schedule_change_command.h"
#include "document/document.h"
#include <document/stowage/stowage.h>
#include <document/tanks/tankconditions.h>
#include <ange/schedule/schedule.h>
#include <limits>

using namespace ange::schedule;

CallCreateDialog::CallCreateDialog(document_t* document, int index, QWidget* parent): QDialog(parent), m_document(document),
    m_ui(new Ui::call_dialog()),
    m_index(index) {
    m_ui->setupUi(this);
  
    QComboBox* cranerules = m_ui->cranerule_combobox;
    cranerules->addItem(CraneRules::toString(CraneRules::CraneRuleFourty)
      ,QVariant::fromValue<CraneRules::Types>(CraneRules::CraneRuleFourty));
    cranerules->addItem(CraneRules::toString(CraneRules::CraneRuleEighty)
      ,QVariant::fromValue<CraneRules::Types>(CraneRules::CraneRuleEighty));
    cranerules->addItem(CraneRules::toString(CraneRules::CraneRuleFourtySuperBins)
      ,QVariant::fromValue<CraneRules::Types>(CraneRules::CraneRuleFourtySuperBins));
    cranerules->addItem(CraneRules::toString(CraneRules::CraneRuleUnknown)
      ,QVariant::fromValue<CraneRules::Types>(CraneRules::CraneRuleUnknown));
  
    QComboBox* twinliftrules = m_ui->twinlifting_combobox;
    twinliftrules->addItem(TwinLiftRules::toString(TwinLiftRules::TwinliftRuleAll)
      ,QVariant::fromValue<TwinLiftRules::Types>(TwinLiftRules::TwinliftRuleAll));
    twinliftrules->addItem(TwinLiftRules::toString(TwinLiftRules::TwinliftRuleNothing)
      ,QVariant::fromValue<TwinLiftRules::Types>(TwinLiftRules::TwinliftRuleNothing));
    twinliftrules->addItem(TwinLiftRules::toString(TwinLiftRules::TwinliftRuleBelow)
      ,QVariant::fromValue<TwinLiftRules::Types>(TwinLiftRules::TwinliftRuleBelow));
    twinliftrules->addItem(TwinLiftRules::toString(TwinLiftRules::TwinliftRuleUnknown)
      ,QVariant::fromValue<TwinLiftRules::Types>(TwinLiftRules::TwinliftRuleUnknown));
  
    connect(m_ui->uncode_edit,SIGNAL(textChanged(QString)),this,SLOT(validate()));
    connect(m_ui->voyage_code_edit,SIGNAL(textChanged(QString)),this,SLOT(validate()));
    connect(m_ui->port_name_edit,SIGNAL(textChanged(QString)),this,SLOT(validate()));
    connect(m_ui->eta_edit,SIGNAL(dateTimeChanged(QDateTime)),this,SLOT(validate()));
    connect(m_ui->etd_edit,SIGNAL(dateTimeChanged(QDateTime)),this,SLOT(validate()));
    connect(m_ui->cranes_edit,SIGNAL(editingFinished()),this,SLOT(validate()));
    connect(m_ui->height_limit_edit,SIGNAL(editingFinished()),this,SLOT(validate()));
    connect(m_ui->productivitySpinBox, SIGNAL(editingFinished()), this, SLOT(validate()));
    connect(m_ui->addButton, SIGNAL(clicked(bool)), this, SLOT(addCall()));
    connect(m_ui->doneButton, SIGNAL(clicked(bool)), this, SLOT(done()));
    DialogUtils::makeDialogMaximizable(this);
    validate();
}

bool CallCreateDialog::validate() {
  bool ret = true;
  QString msg;
  if(m_ui->uncode_edit->text().size()!=5 && m_ui->uncode_edit->text().size() != 7) {
    if(!msg.isEmpty()) {
      msg.append("\n");
    }
    msg+=tr("UNCODE has to be exactly 5 or 7 characters long");
    ret = false;
  }
  if(m_ui->voyage_code_edit->text().size()<4) {
    if(!msg.isEmpty()) {
      msg.append("\n");
    }
    msg+=tr("Voyage code has to be at least 4 characters long");
    ret = false;
  }
  if(m_ui->eta_edit->dateTime() > m_ui->etd_edit->dateTime()) {
    if(!msg.isEmpty()) {
      msg.append("\n");
    }
    msg+=tr("ETD cannot be before ETA");
    ret = false;
  }
  m_ui->error_message->setText(msg);
  m_ui->addButton->setEnabled(ret);
  return ret;
}

void CallCreateDialog::addCall      () {
  if(validate()) {
    int valueindex = m_ui->cranerule_combobox->currentIndex();
    ange::schedule::CraneRules::Types rule = m_ui->cranerule_combobox->itemData(valueindex,Qt::UserRole).value<ange::schedule::CraneRules::Types>();
    ange::schedule::TwinLiftRules::Types twinlift_rule = m_ui->twinlifting_combobox->itemData(valueindex,Qt::UserRole).value<ange::schedule::TwinLiftRules::Types>();
    const QHash<const ange::vessel::VesselTank*, ange::units::Mass> tank_conditions = m_document->tankConditions()->tankConditions(m_document->schedule()->calls().at(m_index-1));
    
    schedule_change_command_t* cmd = schedule_change_command_t::insert_call(m_index,
                                                                            m_ui->uncode_edit->text(),
                                                                            m_ui->voyage_code_edit->text(),
                                                                            m_ui->port_name_edit->text(),
                                                                            m_ui->eta_edit->dateTime(),
                                                                            m_ui->etd_edit->dateTime(),
                                                                            m_ui->cranes_edit->value(),
                                                                            m_ui->height_limit_edit->value(),
                                                                            rule,
                                                                            twinlift_rule,
                                                                            m_ui->productivitySpinBox->value(),
                                                                            m_document,
                                                                            tank_conditions
                                                                           );
    m_document->undostack()->push(cmd);
    ++m_index;
  }
}

void CallCreateDialog::done() {
    QDialog::done(0);
}


#include "call_create_dialog.moc"
