#include "modify_containers_dialog.h"

#include "passivenotifications.h"
#include "ui_modify_containers_dialog.h"

#include <gui/utils/dialogutils.h>
#include <document/document.h>
#include <document/pools/pool.h>
#include <document/containers/container_list.h>
#include <document/containers/loadcallaggregator.h>
#include <document/containers/dischargecallaggregator.h>
#include <document/utils/containerutils.h>
#include <document/stowage/stowage.h>
#include <document/schedulemodel.h>
#include <document/undo/changecontainercommand.h>
#include <document/undo/merger_command.h>
#include <document/stowage/container_move.h>
#include <document/stowage/container_leg.h>
#include <document/undo/containerchanger.h>
#include <document/scheduleproxymodel.h>

#include <ange/containers/container.h>
#include <ange/containers/oog.h>
#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>

#include <QPushButton>

using namespace ange::units;
using namespace ange::containers;
using ange::schedule::Call;
using ange::schedule::Schedule;

static const QString& wildcardSymbol() {
    static const QString wildcardSymbol("%");
    return wildcardSymbol;
}

static const QRegExp& weightRegExp() {
    static const QRegExp weightRegExp("^([0-9]{1,2})t{0,1}|%$"); // regexp contains wildcardSymbol()
    return weightRegExp;
}

static void setupComboLength(QComboBox* combo) {
    combo->clear();
    combo->addItem(wildcardSymbol());
    Q_FOREACH (IsoCode::IsoLength length, IsoCode::knownLengths()) {
        combo->addItem(IsoCode::lengthToString(length), QVariant::fromValue<IsoCode::IsoLength>(length));
    }
    combo->setCurrentIndex(0);
}

static void setupComboHeight(QComboBox* combo) {
    combo->clear();
    combo->addItem(wildcardSymbol());
    Q_FOREACH (IsoCode::IsoHeight height, IsoCode::knownHeights()) {
        combo->addItem(IsoCode::heightToString(height), QVariant::fromValue<IsoCode::IsoHeight>(height));
    }
    combo->setCurrentIndex(0);
}

static void setupComboEmpty(QComboBox* combo) {
    combo->clear();
    combo->addItem(wildcardSymbol());
    combo->addItem("Empty", QVariant::fromValue<bool>(true));
    combo->addItem("Full", QVariant::fromValue<bool>(false));
    combo->setCurrentIndex(0);
}

static void setupComboReefer(QComboBox* combo) {
    combo->clear();
    combo->addItem(wildcardSymbol());
    combo->addItem("Reefer", QVariant::fromValue<bool>(true));
    combo->addItem("Dry", QVariant::fromValue<bool>(false));
    combo->setCurrentIndex(0);
}

static void setupComboLive(QComboBox* combo) {
    combo->clear();
    combo->addItem(wildcardSymbol());
    combo->addItem("Live", QVariant::fromValue<bool>(true));
    combo->addItem("Non-Live", QVariant::fromValue<bool>(false));
    combo->setCurrentIndex(0);
}

ModifyContainersDialog::ModifyContainersDialog(document_t* document, pool_t* selectionPool, QWidget* parent, Qt::WindowFlags f)
    : QDialog(parent, f), m_ui(new Ui::ModifyContainersDialog), m_document(0), m_selectionPool(0),
    m_dischargeCallSchedule(0)
{
    m_ui->setupUi(this);
    // Setup column 1:
    m_ui->eq_id_lineEdit->setText(wildcardSymbol());
    m_ui->booking_no_lineEdit->setText(wildcardSymbol());
    m_ui->carrier_code_lineEdit->setText(wildcardSymbol());
    setupComboLength(m_ui->size_type_combo);
    setupComboHeight(m_ui->height_combo);
    setupComboReefer(m_ui->reefer_combo);
    setupComboLive(m_ui->liveComboBox);
    // Setup column 2:
    m_loadCallFormatter.setSourceModel(&m_loadCallSchedule);
    m_loadCallComboBoxModel.setSourceModel(&m_loadCallFormatter);
    m_ui->load_port_call_combo->setModel(&m_loadCallComboBoxModel);
    m_ui->load_port_call_combo->setCurrentIndex(0);
    connect(m_ui->disch_port_call_combo, SIGNAL(currentIndexChanged(int)), SLOT(updateCallFilters()));
    m_dischargeCallFormatter.setSourceModel(&m_dischargeCallSchedule);
    m_dischargeCallComboBoxModel.setSourceModel(&m_dischargeCallFormatter);
    m_ui->disch_port_call_combo->setModel(&m_dischargeCallComboBoxModel);
    m_ui->disch_port_call_combo->setCurrentIndex(0);
    connect(m_ui->load_port_call_combo, SIGNAL(currentIndexChanged(int)), this, SLOT(updateCallFilters()));
    m_ui->weight_lineEdit->setText(wildcardSymbol());
    m_ui->weight_lineEdit->setValidator(new QRegExpValidator(weightRegExp()));
    setupComboEmpty(m_ui->is_empty_combo);
    m_ui->UN_number_lineEdit->setText(wildcardSymbol());
    m_ui->handling_codes_edit->setText(wildcardSymbol());
    // Setup column 3:
    m_ui->OOG_OH_lineEdit->setText(wildcardSymbol());
    m_ui->OOG_OW_R_lineEdit->setText(wildcardSymbol());
    m_ui->OOG_OW_L_lineEdit->setText(wildcardSymbol());
    m_ui->OOG_OL_FWd_lineEdit->setText(wildcardSymbol());
    m_ui->OOG_OL_Aft_lineEdit->setText(wildcardSymbol());
    m_ui->tempLineEdit->setText(wildcardSymbol());
    // Setup rest:
    // OK and Cancel button connected in UI file
    connect(m_ui->buttonBox->button(QDialogButtonBox::Apply), SIGNAL(clicked(bool)), this, SLOT(applyChanges()));
    set_document(document, selectionPool);
    DialogUtils::makeDialogMaximizable(this);
}

void ModifyContainersDialog::set_document(document_t* document, pool_t* selectionPool) {
    if (m_selectionPool) {
        disconnect(m_selectionPool, SIGNAL(changed()), this, SLOT(reset()));
    }
    m_document = document;
    m_selectionPool = selectionPool;
    m_loadCallSchedule.setSchedule(document->schedule());
    m_loadCallSchedule.setSourceModel(document->schedule_model());
    m_dischargeCallSchedule.setSchedule(document->schedule());
    m_dischargeCallSchedule.setSourceModel(document->schedule_model());
    connect(m_selectionPool, SIGNAL(changed()), this, SLOT(reset()));
    // Have to set indexes for combo boxes after they get content from the document
    m_ui->load_port_call_combo->setCurrentIndex(0);
    m_ui->disch_port_call_combo->setCurrentIndex(0);
    reset();
}

void ModifyContainersDialog::reset() {
    updateCallFilters();
    // editing of eq id is only possible for single containers
    bool oneContainerSelected = m_selectionPool->included_rows().size() == 1;
    m_ui->eq_id_lineEdit->setEnabled(oneContainerSelected);
    bool someContainersSelected = !m_selectionPool->included_rows().empty();
    m_ui->booking_no_lineEdit->setEnabled(someContainersSelected);
    m_ui->carrier_code_lineEdit->setEnabled(someContainersSelected);
    m_ui->size_type_combo->setEnabled(someContainersSelected);
    m_ui->height_combo->setEnabled(someContainersSelected);
    m_ui->reefer_combo->setEnabled(someContainersSelected);
    m_ui->load_port_call_combo->setEnabled(someContainersSelected);
    m_ui->disch_port_call_combo->setEnabled(someContainersSelected);
    m_ui->weight_lineEdit->setEnabled(someContainersSelected);
    m_ui->is_empty_combo->setEnabled(someContainersSelected);
    m_ui->UN_number_lineEdit->setEnabled(someContainersSelected);
    m_ui->handling_codes_edit->setEnabled(someContainersSelected);
    m_ui->OOG_OH_lineEdit->setEnabled(someContainersSelected);
    m_ui->OOG_OW_R_lineEdit->setEnabled(someContainersSelected);
    m_ui->OOG_OW_L_lineEdit->setEnabled(someContainersSelected);
    m_ui->OOG_OL_FWd_lineEdit->setEnabled(someContainersSelected);
    m_ui->OOG_OL_Aft_lineEdit->setEnabled(someContainersSelected);
    m_ui->liveComboBox->setEnabled(someContainersSelected);
    m_ui->tempLineEdit->setEnabled(someContainersSelected);
}

void ModifyContainersDialog::updateCallFilters() {
    const Schedule* schedule = m_document->schedule();
    const ContainerLeg* containerLeg = m_document->stowage()->container_routes();
    // Update discharge filter based on the latest load call
    const Call* latestLoadCall;
    if (m_ui->load_port_call_combo->currentIndex() != 0) {
        latestLoadCall = qvariant_cast<Call*>(
            m_ui->load_port_call_combo->itemData(m_ui->load_port_call_combo->currentIndex()));
    } else {
        latestLoadCall = schedule->at(0);
        Q_FOREACH (const Container* container, m_selectionPool->containers()) {
            const Call* containerLoadCall = containerLeg->portOfLoad(container);
            if (schedule->less(latestLoadCall, containerLoadCall)) {
                latestLoadCall = containerLoadCall;
            }
        }
    }
    m_dischargeCallSchedule.setLoadCall(latestLoadCall);
    // Update load filter based on the earliest discharge call
    const Call* earliestDischargeCall;
    if (m_ui->disch_port_call_combo->currentIndex() != 0) {
        earliestDischargeCall = qvariant_cast<Call*>(
            m_ui->disch_port_call_combo->itemData(m_ui->disch_port_call_combo->currentIndex()));
    } else {
        earliestDischargeCall = schedule->at(schedule->size() - 1);
        Q_FOREACH (const Container* container, m_selectionPool->containers()) {
            const Call* containerDischargeCall = containerLeg->portOfDischarge(container);
            if (schedule->less(containerDischargeCall, earliestDischargeCall)) {
                earliestDischargeCall = containerDischargeCall;
            }
        }
    }
    m_loadCallSchedule.setDischargeCall(earliestDischargeCall);
}

void ModifyContainersDialog::accept() {
    applyChanges();
    QDialog::accept();
}

template<typename T>
static void comboDataHandler(document_t* m_document, QComboBox* combo, merger_command_t* merger, const Container* container,
                      ChangeContainerCommand::Feature feature) {
    if (combo->currentIndex() != 0) {
        T data = combo->itemData(combo->currentIndex()).value<T>();
        ChangeContainerCommand* command = ChangeContainerCommand::change_feature(
            m_document->containers(), container, feature, qVariantFromValue<T>(data));
        merger->push(command);
    }
}

void ModifyContainersDialog::applyChanges() {
    QList<const Container*> containers = m_selectionPool->containers();
    merger_command_t* merged_modify_command = new merger_command_t(tr("Modify %1 containers").arg(containers.size()));
    int problemsCount = 0;
    // Note: the container's setters will emit the container_changed signals
    // iff the value has changed, i.e. checking whether the value has actually
    // changed is done in Container
    Q_FOREACH (const ange::containers::Container* container, containers) {
        // Update column 1:
        if (m_ui->eq_id_lineEdit->displayText() != wildcardSymbol()) {
            EquipmentNumber new_eq_id = m_ui->eq_id_lineEdit->displayText();
            ChangeContainerCommand* command =
                ChangeContainerCommand::change_feature(m_document->containers(), container, ChangeContainerCommand::EquipmentNo,
                                                           qVariantFromValue<EquipmentNumber>(new_eq_id));
            merged_modify_command->push(command);
        }
        if (m_ui->booking_no_lineEdit->displayText() != wildcardSymbol()) {
            ChangeContainerCommand* command =
                ChangeContainerCommand::change_feature(m_document->containers(), container, ChangeContainerCommand::BookingNumber,
                                                           qVariantFromValue<QString>(m_ui->booking_no_lineEdit->displayText()));
            merged_modify_command->push(command);
        }
        if (m_ui->carrier_code_lineEdit->displayText() != wildcardSymbol()) {
            ChangeContainerCommand* command =
                ChangeContainerCommand::change_feature(m_document->containers(), container, ChangeContainerCommand::Carrier,
                                                           qVariantFromValue<QString>(m_ui->carrier_code_lineEdit->displayText()));
            merged_modify_command->push(command);
        }
        // We treat container height, length, reefer (and at some point in time isocode) separately
        // The reason is that we alternatively would have to syntethize the container's
        // iso code probably overwriting the existing one (if it is different) at every call
        // to apply_changes()
        if (m_ui->size_type_combo->currentIndex() > 0) {
            IsoCode::IsoLength newLength = m_ui->size_type_combo->itemData(m_ui->size_type_combo->currentIndex()).value<IsoCode::IsoLength>();
            Q_ASSERT(newLength != 0);
            Q_ASSERT(newLength != -1);
            QSharedPointer<ChangerResult> result = ContainerChanger::changeLength(m_document, container, newLength);
            merged_modify_command->push(result->takeCommand());
            problemsCount += result->problemsGenerated();
        }
        comboDataHandler<IsoCode::IsoHeight>(m_document, m_ui->height_combo, merged_modify_command, container, ChangeContainerCommand::Height);
        comboDataHandler<bool>(m_document, m_ui->reefer_combo, merged_modify_command, container, ChangeContainerCommand::Reefer);
        // Update column 2:
        if (m_ui->load_port_call_combo->currentIndex() > 0 || m_ui->disch_port_call_combo->currentIndex() > 0) {
            const Call* loadCall = m_ui->load_port_call_combo->itemData(m_ui->load_port_call_combo->currentIndex()).value<Call*>();
            const Call* dischargeCall = m_ui->disch_port_call_combo->itemData(m_ui->disch_port_call_combo->currentIndex()).value<Call*>();
            QSharedPointer<ChangerResult> result = ContainerChanger::changeCalls(m_document, container, loadCall, dischargeCall);
            merged_modify_command->push(result->takeCommand());
            problemsCount += result->problemsGenerated();
        }
        weightRegExp().indexIn(m_ui->weight_lineEdit->displayText());
        if (weightRegExp().cap(0) != wildcardSymbol()) {
            const Mass new_weight = weightRegExp().cap(1).toDouble() * ton;
            ChangeContainerCommand* command =
                ChangeContainerCommand::change_feature(m_document->containers(), container, ChangeContainerCommand::Weight,
                                                           qVariantFromValue<Mass>(new_weight));
            merged_modify_command->push(command);
        }
        comboDataHandler<bool>(m_document, m_ui->is_empty_combo, merged_modify_command, container, ChangeContainerCommand::Empty);
        if (m_ui->UN_number_lineEdit->displayText() != wildcardSymbol()) {
            QList<DangerousGoodsCode> dg_list = ContainerUtils::parseDangerousGoodsString(m_ui->UN_number_lineEdit->displayText());
            ChangeContainerCommand* command =
                ChangeContainerCommand::change_feature(m_document->containers(), container, ChangeContainerCommand::Dg,
                                                           qVariantFromValue<QList<DangerousGoodsCode> >(dg_list));
            merged_modify_command->push(command);
        }
        if (m_ui->handling_codes_edit->text() != wildcardSymbol()) {
            QList<HandlingCode> handling_code_list = ContainerUtils::parseHandlingCodesString(m_ui->handling_codes_edit->text());
            ChangeContainerCommand* command =
                ChangeContainerCommand::change_feature(m_document->containers(), container, ChangeContainerCommand::HandlingCodes,
                                                           qVariantFromValue<QList<HandlingCode> >(handling_code_list));
            merged_modify_command->push(command);
        }
        // Update column 3:
        if (m_ui->OOG_OH_lineEdit->displayText() != wildcardSymbol()
                || m_ui->OOG_OL_Aft_lineEdit->displayText() != wildcardSymbol()
                || m_ui->OOG_OL_FWd_lineEdit->displayText() != wildcardSymbol()
                || m_ui->OOG_OW_L_lineEdit->displayText() != wildcardSymbol()
                || m_ui->OOG_OW_R_lineEdit->displayText() != wildcardSymbol()) {
            Oog oog = container->oog();
            if (m_ui->OOG_OH_lineEdit->displayText() != wildcardSymbol()) {
                oog.setTop(m_ui->OOG_OH_lineEdit->displayText().toDouble() * centimeter);
            }
            if (m_ui->OOG_OL_Aft_lineEdit->displayText() != wildcardSymbol()) {
                oog.setBack(m_ui->OOG_OL_Aft_lineEdit->displayText().toDouble() * centimeter);
            }
            if (m_ui->OOG_OL_FWd_lineEdit->displayText() != wildcardSymbol()) {
                oog.setFront(m_ui->OOG_OL_FWd_lineEdit->displayText().toDouble() * centimeter);
            }
            if (m_ui->OOG_OW_L_lineEdit->displayText() != wildcardSymbol()) {
                oog.setLeft(m_ui->OOG_OW_L_lineEdit->displayText().toDouble() * centimeter);
            }
            if (m_ui->OOG_OW_R_lineEdit->displayText() != wildcardSymbol()) {
                oog.setRight(m_ui->OOG_OW_R_lineEdit->displayText().toDouble() * centimeter);
            }
            merged_modify_command->push(ChangeContainerCommand::change_feature(
                m_document->containers(), container, ChangeContainerCommand::OogFeature, qVariantFromValue<Oog>(oog)));
        }
        comboDataHandler<bool>(m_document, m_ui->liveComboBox, merged_modify_command, container, ChangeContainerCommand::Live);
        if (m_ui->tempLineEdit->displayText() != wildcardSymbol()) {
            double temperature = m_ui->tempLineEdit->displayText().toDouble();
            ChangeContainerCommand* command =
                ChangeContainerCommand::change_feature(m_document->containers(), container, ChangeContainerCommand::Temperature,
                                                           qVariantFromValue<double>(temperature));
            merged_modify_command->push(command);
        }
    }
    m_document->undostack()->push(merged_modify_command);
    if (problemsCount > 0) {
        QString message = QString("The container modifications created %1 problems in the problem view").arg(problemsCount);
        m_document->passiveNotifications()->addWarningWithUndo(message);
    }
}

#include "modify_containers_dialog.moc"
