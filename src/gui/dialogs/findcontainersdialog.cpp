#include "findcontainersdialog.h"

#include "container_list.h"
#include "dialogutils.h"
#include "document.h"
#include "eqidmatchingutils.h"
#include "override_cursor_handler.h"
#include "pool.h"
#include "ui_findcontainersdialog.h"

#include <QPushButton>
#include <QRegExp>

using ange::containers::Container;

FindContainersDialog::FindContainersDialog(document_t* document, pool_t* selectionPool,
                                           QWidget* parent, Qt::WindowFlags f)
  : QDialog(parent, f), m_ui(new Ui::FindContainersDialog), m_document(document), m_selectionPool(selectionPool)
{
    m_ui->setupUi(this);
    connect(m_ui->equipmentIdTextEdit, &QPlainTextEdit::textChanged, this, &FindContainersDialog::selectionFromPattern);
    connect(m_ui->patternLineEdit, &QLineEdit::textChanged, this, &FindContainersDialog::selectionFromPattern);
    connect(m_ui->buttonBox->button(QDialogButtonBox::Close), &QPushButton::clicked, this, &FindContainersDialog::done);
    connect(document, &QObject::destroyed, this, &FindContainersDialog::done);
    DialogUtils::makeDialogMaximizable(this);
}

void FindContainersDialog::selectionFromPattern() {
    override_cursor_handler_t cursorHandler(qApp, QCursor(Qt::WaitCursor));

    QSet<const Container*> newSelection;
    QStringList eqIdList = extractEqIdsFromTextBlob(m_ui->equipmentIdTextEdit->toPlainText());
    Q_FOREACH (QString equipmentId, eqIdList) {
        const Container* container = m_document->containers()->get_container_by_equipment_number(equipmentId.toUpper());
        if (container) {
            newSelection << container;
        }
    }
    QString pattern = m_ui->patternLineEdit->text();
    if (!pattern.isEmpty()) {
        QRegExp globMatcher(pattern, Qt::CaseInsensitive, QRegExp::WildcardUnix);
        Q_FOREACH (const Container* container, m_document->containers()->list()) {
            if (globMatcher.exactMatch(container->equipmentNumber())) {
                newSelection << container;
            }
        }
    }
    m_ui->unitsFoundLabel->setText(QString("%1 containers found").arg(newSelection.count()));

    QSet<const Container*> oldSelection = QSet<const Container*>::fromList(m_selectionPool->containers());
    QSet<const Container*> keepSelection = QSet<const Container*>(oldSelection).intersect(newSelection);

    // All the time spent "searching" is used here in modification of the selection
    m_selectionPool->include(newSelection.subtract(keepSelection));
    m_selectionPool->exclude(oldSelection.subtract(keepSelection));
}

void FindContainersDialog::done() {
    QDialog::done(0);
}

#include "findcontainersdialog.moc"
