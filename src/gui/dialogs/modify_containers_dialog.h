#ifndef MODIFY_CONTAINERS_DIALOG_H
#define MODIFY_CONTAINERS_DIALOG_H

#include <QDialog>
#include <document/schedule_wildcard_proxy_model.h>
#include <document/scheduleproxymodel.h>
#include <document/callformatterproxymodel.h>
#include "nonmodaldialoghelper.h"

class QComboBox;
class QLineEdit;
class document_t;
class pool_t;
namespace Ui {
class ModifyContainersDialog;
}
namespace ange{
namespace containers{
class Container;
}
}

class ModifyContainersDialog : public QDialog {
    Q_OBJECT

public:
    explicit ModifyContainersDialog(document_t* document, pool_t* selectionPool, QWidget* parent = 0, Qt::WindowFlags f = 0);

    void set_document(document_t* document, pool_t* selectionPool);

    virtual void accept();

public Q_SLOTS:
    void reset();

private Q_SLOTS:
    void applyChanges();
    void updateCallFilters();

private:
    Ui::ModifyContainersDialog* m_ui;
    document_t* m_document;
    pool_t* m_selectionPool;

    ScheduleProxyModel m_loadCallSchedule;
    ScheduleWildcardProxyModel m_loadCallComboBoxModel;
    ScheduleProxyModel m_dischargeCallSchedule;
    ScheduleWildcardProxyModel m_dischargeCallComboBoxModel;
    CallFormatterProxyModel m_loadCallFormatter;
    CallFormatterProxyModel m_dischargeCallFormatter;

};

#endif // MODIFY_CONTAINERS_DIALOG_H
