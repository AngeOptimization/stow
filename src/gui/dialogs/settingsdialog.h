#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>

namespace Ui {
class SettingsDialog;
}

class SettingsDialog : public QDialog {

    Q_OBJECT

public:
    SettingsDialog(QWidget* parent = 0, Qt::WindowFlags f = 0);

public Q_SLOTS:
    virtual void accept();
    void apply_settings();
    void set_show_equipment_id(bool state);
    void set_show_visibilityline_changed(bool enabled);

private Q_SLOTS:
    void showFileDialogForPortColorFile();

Q_SIGNALS:
    void show_2_rows(bool enabled);
    void show_equipment_number(bool enabled);
    void display_bulkheads_changed(bool enabled);
    void display_visibilityline_changed(bool enabled);
    void hide_nonactive_changed(bool hidden);
    void displayContainerWeightChanged(bool display);
    void usePivotBasedPortColoringChanged(bool enabled);
    void linkForeAftSlotsChanged(bool enabled);
    void newColorFileSelected();

private:
    Ui::SettingsDialog* m_ui;

};

#endif // SETTINGSDIALOG_H
