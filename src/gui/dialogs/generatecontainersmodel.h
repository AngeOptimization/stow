#ifndef GENERATECONTAINERSMODEL_H
#define GENERATECONTAINERSMODEL_H

#include <QAbstractItemModel>
#include <ange/containers/isocode.h>
#include <ange/units/units.h>
#include <ange/containers/dangerousgoodscode.h>
#include "scheduledcontainer.h"

namespace ange {
namespace schedule {
class Call;
class Schedule;
}
namespace containers {
class Container;
}
}

class GenerateContainersModel : public QAbstractTableModel {
    typedef QAbstractTableModel super;
public:
    enum Columns {
      CONTAINER_COUNT = 0,
      ISO_NUMBER = 1,
      WEIGHT = 2,
      LOAD_PORT = 3,
      DISCHARGE_PORT = 4,
      EMPTY_STATUS = 5,
      CARRIER_CODE = 6,
      DG_CODES = 7,
      NCOLUMNS
    };
    GenerateContainersModel(const ange::schedule::Schedule* schedule, const ange::schedule::Call* currentCall);
    virtual QVariant data(const QModelIndex& index, int role) const;
    virtual bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole);
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    virtual int columnCount(const QModelIndex& parent) const;
    virtual int rowCount(const QModelIndex& parent) const;
    virtual Qt::ItemFlags flags(const QModelIndex& index) const Q_DECL_OVERRIDE;
    QList<ange::angelstow::ScheduledContainer> containers() const;
public Q_SLOTS:
    /**
     * Initialize the values of the bottom row by copying the next-to-last row
     */
    void addBottomRow(const QModelIndex& current, const QModelIndex&);
private:
    /**
     * A struct representing a row in the model, that is a group of identical containers to be created.
     */
    struct ContainerGroup {
        ContainerGroup(int amount, const ange::containers::IsoCode& isoCode, const ange::units::Mass weight,
                            const ange::schedule::Call* loadCall, const ange::schedule::Call* dischCall, bool isEmpty);
        int m_amount;
        ange::containers::IsoCode m_isoCode;
        ange::units::Mass m_weight;
        ange::schedule::Call* m_loadCall;
        ange::schedule::Call* m_dischCall;
        bool m_isEmpty;
        QString m_carrierCode;
        QList<ange::containers::DangerousGoodsCode> m_dgCodes;
    };
    QList<ContainerGroup> m_containerGroups;
};

#endif // GENERATECONTAINERSMODEL_H
