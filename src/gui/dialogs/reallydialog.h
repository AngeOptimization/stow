/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010

 Copyright: See COPYING file that comes with this distribution

*/

#ifndef REALLYDIALOG_H
#define REALLYDIALOG_H

#include <QMessageBox>

namespace Ui {
class reallydialog;
}

/**
 * Display the really-quit message
 */
class reallydialog_t : public QMessageBox {
  public:
  reallydialog_t(QWidget* parent, QString name, QString message);
};

#endif // REALLYDIALOG_H
