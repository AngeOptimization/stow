/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010

 Copyright: See COPYING file that comes with this distribution

*/

#ifndef MAP_PORT_DIALOG_H
#define MAP_PORT_DIALOG_H

#include <QDialog>

namespace ange {
namespace schedule {
class Call;
class Schedule;
}
}

class document_t;
namespace Ui {
class map_port_dialog;
}


class map_port_dialog_t : public QDialog {
  Q_OBJECT
  public:
    map_port_dialog_t(ange::schedule::Schedule* schedule, const QString& uncode, const QString& voyage_code,
                      const ange::schedule::Call* first_valid_call, const ange::schedule::Call* last_valid_call,
                      QWidget* parent = 0, Qt::WindowFlags f = 0);
    virtual ~map_port_dialog_t();
    const ange::schedule::Call* mapped_to() const;
  private:
    Ui::map_port_dialog* m_ui;
    ange::schedule::Schedule* m_schedule;
    void add_call_to_combobox(const ange::schedule::Call* call_to_add, const ange::schedule::Call* current_call);
  public Q_SLOTS:
    void call_selected(int row);
};

#endif // MAP_PORT_DIALOG_H
