/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010
 Copyright: See COPYING file that comes with this distribution
*/

#include "recapview.h"
#include <qdatacube/datacube.h>
#include <ange/schedule/schedule.h>
#include <ange/schedule/call.h>
#include <stdexcept>
#include <QMenu>
#include <QMouseEvent>
#include <qdatacube/datacubeselection.h>
#include <qdatacube/countformatter.h>
#include "document/containers/dischargecallaggregator.h"
#include "document/containers/length_aggregator.h"
#include "document/containers/height_aggregator.h"
#include "document/containers/weight_aggregator.h"
#include "document/containers/vgmaggregator.h"
#include "document/containers/reefer_aggregator.h"
#include "document/containers/dangerous_goods_aggregator.h"
#include "document/containers/recap_filter_unplanned.h"
#include "document/containers/empty_aggregator.h"
#include "document/containers/oogaggregator.h"
#include "document/containers/stowcodeaggregator.h"
#include "document/containers/carrier_code_aggregator.h"
#include "document/containers/isocodeaggregator.h"
#include <document/undo/merger_command.h>
#include <document/undo/containerchanger.h>
#include "document/document.h"
#include "document/containers/container_list.h"
#include "document/pools/pool.h"
#include "gui/containerselectionbundlehelper.h"
#include "filteredcontainerlist.h"
#include "showhidecolumnsintableview.h"
#include <tableviewutils.h>
#include "dialogs/reporthelpers/datacubehtmlexporter.h"
#include "containerviewitemdelegates/setupdelegates.h"
#include "gui_interface.h"
#include "recapmanagerinterface.h"
#include <QDockWidget>
#include <QLineEdit>
#include <QSettings>
#include <QMainWindow>
#include <QHeaderView>
#include <QShortcut>
#include <QHBoxLayout>
#include <QApplication>
#include <QMimeData>
#include <qdatacube/filterbyaggregate.h>
#include <qdatacube/columnaggregator.h>
#include <ange/containers/container.h>
#include "document/containers/stowagestatusaggregator.h"
#include "document/containers/traversingfilter.h"
#include "document/containers/recapformatterweight.h"
#include "document/containers/recap_filter_imo_only.h"
#include "document/containers/loadcallaggregator.h"
#include <document/containers/recapfiltercontainerid.h>
#include <document/containers/bundleaggregator.h>
#include <document/containers/recapfilterbundledcontainers.h>
#include <document/containers/bundlableaggregator.h>
#include <document/containers/recapformatterteu.h>
#include "bayaggregator.h"
#include "recapfiltermovesonly.h"
#include "document/stowage/stowage.h"
#include "document/stowage/container_leg.h"
#include <QClipboard>

using ange::schedule::Call;
using qdatacube::Datacube;
using qdatacube::AbstractFilter;
using qdatacube::AbstractAggregator;

RecapView::RecapView(QString name, QWidget* parent):
    super(parent),
    m_current_call(0L),
    m_document(0),
    m_selection_pool(0L),
    m_name(name),
    m_has_custom_filter(false),
    m_recapManager(0)
{
    connect(this, &DatacubeView::horizontalHeaderContextMenu, this, &RecapView::displayHorizontalHeaderContextMenu);
    connect(this, &DatacubeView::verticalHeaderContextMenu, this, &RecapView::displayVerticalHeaderContextMenu);
    connect(this, &DatacubeView::cornerContextMenu, this, &RecapView::displayCornerContextMenu);
    connect(this, &RecapView::cellContextMenu, this, &RecapView::displayCellContextMenu);
    QFont small(font());
    small.setPointSizeF(small.pointSizeF()*0.8);
    setFont(small);
    QShortcut* shortcut = new QShortcut(this);
    shortcut->setKey(QKeySequence(tr("Ctrl+L", "Open as list")));
    shortcut->setContext(Qt::WidgetWithChildrenShortcut);
    connect(shortcut, &QShortcut::activated, this, &RecapView::displayRecapAsList);
    QSettings settings;
}


/**
 * Builds all filters that aren't based on aggregators
 * \param document
 * \param currentCall
 * \param view to connect to for current call changes, or null if the filters shouldn't listen for changes
 */
static QList<AbstractFilter::Ptr> buildAllFilters(document_t* document, const ange::schedule::Call* currentCall, RecapView* view) {
    QList<AbstractFilter::Ptr> filters;
    RecapFilterUnplanned* unplanned_filter = new RecapFilterUnplanned(document->containers(), currentCall);
    if(view) {
        QObject::connect(view, &RecapView::current_call_changed, unplanned_filter, &RecapFilterUnplanned::set_load_call);
    }
    filters << AbstractFilter::Ptr(unplanned_filter);
    TraversingFilter* traversing_filter = new TraversingFilter(document->containers(), document->stowage());
    traversing_filter->setCurrentCall(currentCall);
    if(view) {
        QObject::connect(view, &RecapView::current_call_changed, traversing_filter, &TraversingFilter::setCurrentCall);
    }
    filters << AbstractFilter::Ptr(traversing_filter);
    filters << AbstractFilter::Ptr(new recap_filter_imo_only_t(document->containers()));
    filters << AbstractFilter::Ptr(
            new RecapFilterBundledContainers(document->containers(),document->bundledContainers()->bundledContainers));
    RecapFilterMovesOnly* filterMovesOnly = new RecapFilterMovesOnly(document->containers());
    if(view) {
        QObject::connect(view, &RecapView::current_call_changed, filterMovesOnly, &RecapFilterMovesOnly::setCurrentCall);
    }
    filterMovesOnly->setCurrentCall(currentCall);
    filters << AbstractFilter::Ptr(filterMovesOnly);
    return filters;
}

/**
 * Builds all aggregators
 * \param document
 * \param currentCall
 * \param view to connect to for current call changes, or null if the aggregators shouldn't listen for changes
 */
static QList<AbstractAggregator::Ptr> buildAllAggregators(document_t* document, const ange::schedule::Call* currentCall, RecapView* view) {
    QList<AbstractAggregator::Ptr> aggregators;
    aggregators << AbstractAggregator::Ptr(new HeightAggregator(document->containers()));
    aggregators << AbstractAggregator::Ptr(new LengthAggregator(document->containers()));
    aggregators << AbstractAggregator::Ptr(new WeightAggregator(document->containers()));
    aggregators << AbstractAggregator::Ptr(new VGMAggregator(document->containers()));
    aggregators << AbstractAggregator::Ptr(new ReeferAggregator(document->containers()));
    aggregators << AbstractAggregator::Ptr(
                                            new DangerousGoodsAggregator(document->containers()));
    aggregators << AbstractAggregator::Ptr(new EmptyAggregator(document->containers()));
    aggregators << AbstractAggregator::Ptr(new OogAggregator(document->containers()));
    aggregators << AbstractAggregator::Ptr(new StowCodeAggregator(document->containers()));
    qdatacube::ColumnAggregator* nominal_destination_filter = new qdatacube::ColumnAggregator(
                                              document->containers(), basic_container_list_t::NOMINAL_POD_COL);
    nominal_destination_filter->setTrimNewCategoriesFromRight(3);
    aggregators << AbstractAggregator::Ptr(nominal_destination_filter);
    qdatacube::ColumnAggregator* nominal_origin_filter = new qdatacube::ColumnAggregator(
                                              document->containers(), basic_container_list_t::NOMINAL_POL_COL);
    nominal_origin_filter->setTrimNewCategoriesFromRight(3);
    aggregators << AbstractAggregator::Ptr(nominal_origin_filter);
    aggregators << AbstractAggregator::Ptr(
                                            new DischargeCallAggregator(document->containers(), document->schedule(), document->userconfiguration()));
    aggregators << AbstractAggregator::Ptr(
                                           new LoadCallAggregator(document->containers(), document->schedule(), document->userconfiguration()));
    aggregators << AbstractAggregator::Ptr(new CarrierCodeAggregator(document->containers()));
    aggregators << AbstractAggregator::Ptr(new IsoCodeAggregator(document->containers()));
    aggregators << AbstractAggregator::Ptr(new BundleAggregator(document->containers()));
    aggregators << AbstractAggregator::Ptr(
                                           new BundlableAggregator(document->containers()));
    StowageStatusAggregator* stowed_aggregator = new StowageStatusAggregator(document->containers());
    stowed_aggregator->setCurrentCall(currentCall);
    if(view) {
        QObject::connect(view, &RecapView::current_call_changed, stowed_aggregator, &StowageStatusAggregator::setCurrentCall);
    }
    aggregators << AbstractAggregator::Ptr(stowed_aggregator);
    BayAggregator* bayAggregator = new BayAggregator(document->containers());
    bayAggregator->set_current_call(currentCall);
    if(view) {
        QObject::connect(view, &RecapView::current_call_changed, bayAggregator, &BayAggregator::set_current_call);
    }
    aggregators << AbstractAggregator::Ptr(bayAggregator);
    return aggregators;
}

void RecapView::setDocument(document_t* document, pool_t* selection_pool,
                                QItemSelectionModel* synchronized_selection_model) {
    m_unused_aggregators.clear();
    if (m_has_custom_filter) {
        if(datacube()) {
            datacube()->deleteLater();
        }
        deleteLater();
        return;
    }
    // ensure currentCall is from this schedule
    // setDocument is called both on load document and on vessel import. If the former, current call
    // doesn't come from this schedule
    if (m_document != document) {
        m_current_call = 0;
    }
    m_unused_aggregators = buildAllAggregators(document, m_current_call, this);
    // Setup pool of filters (besides the filters derived from aggregates)
    m_unused_filters.clear();
    m_unused_filters = buildAllFilters(document, m_current_call, this);
    m_document = document;
    m_selection_pool = selection_pool;
    // Check if an extra formatter was used
    QString extra_formatter = (formatters().size()>1) ? formatters().at(1)->name() : QString();
    // Create new datacube and split it as before
    Datacube* old_datacube = datacube();
    Datacube* datacube = new Datacube(document->containers(),  this);
    setDatacube(datacube);
    if (old_datacube) {
        cloneFilterAndAggregatorsFromDataCube(old_datacube, this);
        old_datacube->deleteLater();
    }
    datacubeSelection()->synchronizeWith(synchronized_selection_model);
    addFormatter(new qdatacube::CountFormatter(m_document->containers(), this));
    if (extra_formatter == "weight") {
        addFormatter(new RecapFormatterWeight(m_document->containers(), this));
    } else if(extra_formatter == "teu") {
        addFormatter(new RecapFormatterTEU(m_document->containers(), this));
    }
}

Datacube* RecapView::detachedFilteredCube() const {
    Datacube* newCube = new Datacube(m_document->containers());
    QList<AbstractFilter::Ptr> possibleFilters = buildAllFilters(m_document, m_current_call, 0);
    QList<AbstractAggregator::Ptr> possibleAggregators = buildAllAggregators(m_document, m_current_call, 0);
    Q_FOREACH(AbstractFilter::Ptr filter, datacube()->filters()) {
        if (QSharedPointer<qdatacube::FilterByAggregate> aggregate_filter =
                filter.objectCast<qdatacube::FilterByAggregate>()) {
            Q_FOREACH(AbstractAggregator::Ptr possibleAggregator, possibleAggregators) {
                if(aggregate_filter->aggregator()->name() == possibleAggregator->name()) {
                    // Verify that categories havn't changed
                    if (possibleAggregator->categoryCount()
                            == aggregate_filter->aggregator()->categoryCount()) {
                        const int category = aggregate_filter->categoryIndex();
                        if (aggregate_filter->aggregator()->categoryHeaderData(category).toString()
                                == possibleAggregator->categoryHeaderData(category).toString()) {
                            qdatacube::AbstractFilter::Ptr filter(new qdatacube::FilterByAggregate(
                                possibleAggregator, category));
                            newCube->addFilter(filter);
                            int result = possibleAggregators.removeAll(possibleAggregator);
                            Q_ASSERT(result == 1);
                            Q_UNUSED(result);
                        } else {
                            Q_ASSERT(false); // when cloning, the aggregators shouldn't have changed underneath.
                        }
                    } else {
                        Q_ASSERT(false); // when cloning, the aggregators
                    }
                }
            }

        } else {
            Q_FOREACH(AbstractFilter::Ptr possibleFilter, possibleFilters) {
                if(possibleFilter->name() == filter->name()) {
                    newCube->addFilter(possibleFilter);
                    int result = possibleFilters.removeAll(possibleFilter);
                    Q_ASSERT(result == 1);
                    Q_UNUSED(result);
                    break;
                }
            }
        }
    }
    return newCube;
}


void RecapView::cloneFilterAndAggregatorsFromDataCube(Datacube* oldCube, RecapView* newRecap) {
    Datacube::Aggregators column_aggregators = oldCube->columnAggregators();
    for (int i=0; i<column_aggregators.size(); ++i) {
        const int aggregator_index = newRecap->findUnusedAggregatorByName(column_aggregators.at(i)->name());
        if (aggregator_index>=0) {
            newRecap->split(Qt::Horizontal, i, aggregator_index);
        }
    }
    Datacube::Aggregators row_aggregators = oldCube->rowAggregators();
    for (int i=0; i<row_aggregators.size(); ++i) {
        const int aggregator_index = newRecap->findUnusedAggregatorByName(row_aggregators.at(i)->name());
        if (aggregator_index>=0) {
            newRecap->split(Qt::Vertical, i, aggregator_index);
        }
    }
    Q_FOREACH(AbstractFilter::Ptr filter, oldCube->filters()) {
        if (QSharedPointer<qdatacube::FilterByAggregate> aggregate_filter =
                filter.objectCast<qdatacube::FilterByAggregate>()) {
            const int aggregator_index = newRecap->findUnusedAggregatorByName(aggregate_filter->aggregator()->name());
            if (aggregator_index>=0) {
                // Verify that categories havn't changed
                if (newRecap->m_unused_aggregators.at(aggregator_index)->categoryCount()
                        == aggregate_filter->aggregator()->categoryCount()) {
                    const int category = aggregate_filter->categoryIndex();
                    if (aggregate_filter->aggregator()->categoryHeaderData(category).toString()
                            == newRecap->m_unused_aggregators.at(aggregator_index)->categoryHeaderData(category).toString()) {
                        qdatacube::AbstractFilter::Ptr filter(new qdatacube::FilterByAggregate(
                        newRecap->m_unused_aggregators.takeAt(aggregator_index), category));
                        newRecap->datacube()->addFilter(filter);
                    }
                }
            }
        } else {
            const int filter_index = newRecap->findUnusedFilterByName(filter->name());
            if (filter_index >= 0) {
                newRecap->datacube()->addFilter(newRecap->m_unused_filters.takeAt(filter_index));
            }
        }
    }
}


void RecapView::setupStandardRecap() {
  m_unused_aggregators.append(datacube()->columnAggregators());
  m_unused_aggregators.append(datacube()->rowAggregators());

  int unused_filter_index = findUnusedAggregatorByName("height");
  if (unused_filter_index != -1) {
    split(Qt::Horizontal, 0, unused_filter_index);
  }
  unused_filter_index = findUnusedAggregatorByName("length");
  if (unused_filter_index != -1) {
    split(Qt::Horizontal, 0, unused_filter_index);
  }
  unused_filter_index = findUnusedAggregatorByName("weight");
  if (unused_filter_index != -1) {
    split(Qt::Vertical, 0, unused_filter_index);
  }
  unused_filter_index = findUnusedAggregatorByName("destination");
  if (unused_filter_index != -1) {
    split(Qt::Vertical, 0, unused_filter_index);
  }
  const int filter_index = findUnusedFilterByName("unplanned");
  if (filter_index >= 0) {
    datacube()->addFilter(m_unused_filters.takeAt(filter_index));
  }
}

QAction* RecapView::createFilterAction(AbstractAggregator::Ptr filter, QMenu* parent) {
  QString name = filter->name();
  name[0] = name[0].toUpper();
  QAction* rv = parent->addAction(name);
  return rv;
}

void RecapView::displayCornerContextMenu(QPoint ) {
    headerContextMenu(Qt::Orientation(), 0, true);
}

void RecapView::headerContextMenu(Qt::Orientation orientation, int headerno, bool split_after) {
  Q_ASSERT(headerno>=0 || split_after == true);
  QMenu contextmenu(this);
  QAction* as_list = contextmenu.addAction(tr("Display list"));
  QList<QAction*> global_filter_actions;
  QMenu* global = m_has_custom_filter ? 0L : contextmenu.addMenu(tr("filter..."));
  QAction* collapse_action = 0L;
  if (!datacube()->filters().isEmpty() && !m_has_custom_filter) {
    QAction* action = global->addAction(tr("&Clear"));
    QVariantHash data;
    data.insert("type", "none");
    action->setData(data);
    global_filter_actions << action;
  }
  QAction* copyToClipboard = contextmenu.addAction(tr("Copy to clipboard"));

  // Create display menu
  QMenu* display_menu = contextmenu.addMenu(tr("&Display"));
  QActionGroup* display_group = new QActionGroup(&contextmenu);
  QAction* display_just_counts = display_menu->addAction(tr("&Counts only"));
  display_just_counts->setCheckable(true);
  display_group->addAction(display_just_counts);
  display_just_counts->setData("counts");
  QAction* display_weights = display_menu->addAction(tr("&Weights and counts"));
  display_weights->setCheckable(true);
  display_group->addAction(display_weights);
  display_weights->setData("weights");
  QAction* displayTEU = display_menu->addAction(tr("&TEU and counts"));
  displayTEU->setCheckable(true);
  display_group->addAction(displayTEU);
  displayTEU->setData("TEU");
  display_group->setExclusive(true);
  if (formatters().size() == 1) {
    display_just_counts->setChecked(true);
  } else {
    display_weights->setChecked(true);
  }

  // Create filter menu
  if (!m_has_custom_filter) {
    for (int i= 0; i<m_unused_aggregators.size(); ++i) {
      AbstractAggregator::Ptr filter = m_unused_aggregators.at(i);
      QString global_menu_name = filter->name();
      global_menu_name[0] = global_menu_name[0].toUpper();
      QMenu* global_menu = global->addMenu(global_menu_name);
      for (int j=0; j < filter->categoryCount(); ++j) {
        QAction* action = global_menu->addAction(filter->categoryHeaderData(j).toString());
        global_filter_actions << action;
        QVariantHash data;
        data.insert("type", "aggregate_filter");
        data.insert("aggregator_index", i);
        data.insert("category_index", j);
        action->setData(data);
      }
    }
    for (int i=0; i< m_unused_filters.size(); ++i) {
      AbstractFilter::Ptr filter =  m_unused_filters.at(i);
      QString action_name = filter->name();
      if (!action_name.isEmpty()) {
        action_name[0] = action_name[0].toUpper();
        QAction* action = global->addAction(action_name);
        QVariantHash data;
        data.insert("type", "filter");
        data.insert("filter_index", i);
        action->setData(data);
        global_filter_actions << action;
      }
    }
  }
  if (orientation & (Qt::Horizontal | Qt::Vertical)) {
    Datacube::Aggregators section_filters = (orientation==Qt::Horizontal? datacube()->columnAggregators() : datacube()->rowAggregators());
    for (int i= 0; i<m_unused_aggregators.size(); ++i) {
      AbstractAggregator::Ptr filter = m_unused_aggregators.at(i);
      QAction* filteraction = createFilterAction(filter, &contextmenu);
      filteraction->setData(i);
    }
    if (section_filters.size()>0) {
        if(headerno >= 0) {
            collapse_action = contextmenu.addAction(tr("Collapse %1").arg(section_filters.at(headerno)->name()));
        }
    }
  }
  generalContextMenu(contextmenu);
  QAction* selected = contextmenu.exec(QCursor::pos());
  if (!selected) {
    return;
  }
  if (selected == collapse_action) {
    Q_ASSERT(headerno>=0);
    collapse(orientation, headerno);
  } else if (selected == as_list) {
    display_list(detachedFilteredCube());
  } else if (selected == copyToClipboard) {
    copyRecapToClipboard();
  } else {
    if (global_filter_actions.contains(selected)) {
      QVariantHash data = selected->data().toHash();
      if (data.value("type").toString() == "aggregate_filter") {
        const int aggregate_index =  data.value("aggregator_index").toInt();
        const int category_index = data.value("category_index").toInt();
        qdatacube::AbstractAggregator::Ptr aggregator = m_unused_aggregators.takeAt(aggregate_index);
        datacube()->addFilter(AbstractFilter::Ptr(new qdatacube::FilterByAggregate(aggregator, category_index)));
      } else if (data.value("type").toString() == "none") {
        Q_FOREACH(Datacube::Filters::value_type filter, datacube()->filters()) {
          if (QSharedPointer<qdatacube::FilterByAggregate> aggregate_filter = filter.objectCast<qdatacube::FilterByAggregate>()) {
            m_unused_aggregators << aggregate_filter->aggregator();
          } else {
            m_unused_filters << filter;
          }
        }
        datacube()->resetFilter();
      } else if (data.value("type").toString() == "filter") {
        const int filter_index =  data.value("filter_index").toInt();
        datacube()->addFilter(m_unused_filters.takeAt(filter_index));
      }
    } else if (display_group->actions().contains(selected)) {
      QString data = selected->data().toString();
      if (formatters().size()>1) {
        Q_ASSERT(formatters().size()==2);
        delete(takeFormatter(1));
      }
      if (data == "weights") {
        addFormatter(new RecapFormatterWeight(m_document->containers(), this));
      } else if (data == "TEU") {
          addFormatter(new RecapFormatterTEU(m_document->containers(), this));
    }
    } else {
      bool ok;
      int filterno = selected->data().toInt(&ok);
      if (ok) {
        split(orientation, split_after ? headerno+1 : headerno, filterno);
      }
    }
  }
}

void RecapView::displayHorizontalHeaderContextMenu(QPoint pos, int headerno) {
  // Calculate header height. I suppose this should be available on the datacube_view_t...
  const int header_height = corner().height() / qMax(datacube()->headerCount(Qt::Horizontal),1) / 2.0;
  const bool near_bottom_edge = pos.y() > header_height;
  headerContextMenu(Qt::Horizontal, headerno, near_bottom_edge);
}

void RecapView::displayVerticalHeaderContextMenu(QPoint pos, int headerno) {
  // Calculate header width. I suppose this should be available on the datacube_view_t...
  const int header_width = corner().width() / qMax(datacube()->headerCount(Qt::Vertical),1) / 2.0;
  const bool near_right_edge = pos.x() > header_width;
  headerContextMenu(Qt::Vertical, headerno, near_right_edge);
}

void RecapView::setCurrentCall(const ange::schedule::Call* call) {
  m_current_call = call;
  emit current_call_changed(call);
}

void RecapView::generalContextMenu(QMenu& contextmenu) {
    if (!m_document || !m_current_call) {
        Q_ASSERT(false); // This should never happen
        contextmenu.addAction("Document missing?")->setEnabled(false);
        return;
    }
    if (!m_bundlehelper.isNull()) {
        contextmenu.addAction(m_bundlehelper.data()->bundleAction());
        contextmenu.addAction(m_bundlehelper.data()->unbundleAction());
    }
    QAction* listAction = contextmenu.addAction(tr("Selection as list"));
    connect(listAction, &QAction::triggered, this, &RecapView::displaySelectionAsList);
    QAction* recapAction = contextmenu.addAction(tr("Selection as recap"));
    connect(recapAction, &QAction::triggered, this, &RecapView::display_selection_as_recap);
    if (m_selection_pool->empty()) {
        listAction->setEnabled(false);
        recapAction->setEnabled(false);
    }
}

void RecapView::contextMenuEvent(QContextMenuEvent* event) {
  super::contextMenuEvent(event);
  if (!event->isAccepted()) {
    QMenu contextmenu;
    generalContextMenu(contextmenu);
    contextmenu.exec(event->globalPos());
    event->accept();
  }
}

QVariant RecapView::saveState() const {
  QVariantMap state;
  if (m_has_custom_filter) {
    return state;
  }
  QVariantList horizontal_filters;
  QVariantList vertical_filters;
  Q_FOREACH(AbstractAggregator::Ptr filter, datacube()->columnAggregators()) {
    horizontal_filters << filter->name();
  }
  Q_FOREACH(AbstractAggregator::Ptr filter, datacube()->rowAggregators()) {
    vertical_filters << filter->name();
  }
  state.insert("horizontal_filters", horizontal_filters);
  state.insert("vertical_filters", vertical_filters);
  QVariantList global_filters;
  Q_FOREACH(Datacube::Filters::value_type filter, datacube()->filters()) {
    QVariantHash filter_state;
    if (QSharedPointer<qdatacube::FilterByAggregate> aggregate_filter = filter.objectCast<qdatacube::FilterByAggregate>()) {
      filter_state.insert("type", "aggregate_filter");
      filter_state.insert("category_index", aggregate_filter->categoryIndex());
      filter_state.insert("name", aggregate_filter->aggregator()->name());
      filter_state.insert("category_name", aggregate_filter->aggregator()->categoryHeaderData(aggregate_filter->categoryIndex()));
    } else {
      filter_state.insert("type", "filter");
      filter_state.insert("name", filter->name());
    }
    global_filters << filter_state;
  }
  state.insert("global_filters_", global_filters);
  if (formatters().size()>1) {
    state.insert("extra_formatter", formatters().at(1)->name());
  }
  state.insert("name", m_name);
  return state;
}

void RecapView::collapse(Qt::Orientation orientation, int index) {
  m_unused_aggregators << (orientation == Qt::Vertical ?  datacube()->rowAggregators() : datacube()->columnAggregators()).at(index);
  datacube()->collapse(orientation, index);
}

void RecapView::split(Qt::Orientation orientation, int section, int unused_filter_index) {
  aggregator_list_t::value_type filter = m_unused_aggregators.takeAt(unused_filter_index);
  datacube()->split(orientation, section, filter);
}

void RecapView::restoreState(QVariant saved_state) {
  // Remove all aggregators, global filters and formatters
  m_unused_aggregators << datacube()->columnAggregators();
  while (datacube()->headerCount(Qt::Horizontal) > 0) {
    datacube()->collapse(Qt::Horizontal,0);
  }
  m_unused_aggregators << datacube()->rowAggregators();
  while (datacube()->headerCount(Qt::Vertical) > 0) {
    datacube()->collapse(Qt::Vertical,0);
  }
  Q_FOREACH(Datacube::Filters::value_type filter, datacube()->filters()) {
    if (QSharedPointer<qdatacube::FilterByAggregate> aggregate_filter = filter.objectCast<qdatacube::FilterByAggregate>()) {
      m_unused_aggregators << aggregate_filter->aggregator();
    } else {
      m_unused_filters << filter;
    }
  }
  datacube()->resetFilter();
  if (formatters().size()>1) {
    delete(takeFormatter(1));
  }

  Q_FOREACH(QVariant filter_name, saved_state.toMap().value("horizontal_filters").toList()) {
    split(Qt::Horizontal, datacube()->headerCount(Qt::Horizontal), filter_name.toString());
  }
  Q_FOREACH(QVariant filter_name, saved_state.toMap().value("vertical_filters").toList()) {
    qdatacube::AbstractAggregator::Ptr filter;
    for (int i=0; i<m_unused_aggregators.size(); ++i) {
      qdatacube::AbstractAggregator::Ptr candidate = m_unused_aggregators.value(i);
      if (candidate->name() == filter_name.toString()) {
        filter = m_unused_aggregators.takeAt(i);
        break;
      }
    }
    if (filter) {
      datacube()->split(Qt::Vertical, datacube()->headerCount(Qt::Vertical), filter);
    } else {
      qWarning("Unable to restore filter with name '%s'", filter_name.toString().toLocal8Bit().data());
    }
  }
  QVariantList global_filters = saved_state.toMap().value("global_filters_").toList();
  while (!global_filters.isEmpty()) {
    if (global_filters.front().type() == QVariant::Hash) {
      QVariantHash global_filter_state = global_filters.takeFirst().toHash();
      const QString filter_type = global_filter_state.value("type").toString();
      if (filter_type == "aggregate_filter") {
        const int aggregate_index = findUnusedAggregatorByName(global_filter_state.value("name").toString());
        if (aggregate_index>=0) {
          qdatacube::AbstractAggregator::Ptr aggregator = m_unused_aggregators.at(aggregate_index);
          const int category_index = global_filter_state.value("category_index").toInt();
          const QString category_name = global_filter_state.value("category_name").toString();
          if (aggregator->categoryHeaderData(category_index).toString() == category_name) {
            datacube()->addFilter(qdatacube::AbstractFilter::Ptr(new qdatacube::FilterByAggregate(m_unused_aggregators.takeAt(aggregate_index), category_index)));
          }
        }
      } else if (filter_type == "filter") {
        const int filter_index = findUnusedFilterByName(global_filter_state.value("name").toString());
        if (filter_index>=0) {
          AbstractFilter::Ptr filter = m_unused_filters.takeAt(filter_index);
          datacube()->addFilter(filter);
        }
      }
    } else {
      global_filters.pop_front();
    }
  }
  QString extra_formatter = saved_state.toMap().value("extra_formatter").toString();
  if (extra_formatter == "weight") {
    addFormatter(new RecapFormatterWeight(m_document->containers(), this));
  } else if (extra_formatter == "TEU") {
      addFormatter(new RecapFormatterTEU(m_document->containers(), this));
  }
  setName(saved_state.toMap().value("name").toString());
}

int RecapView::findUnusedAggregatorByName(QString name) const {
  for (int i=0; i<m_unused_aggregators.size(); ++i) {
    if (m_unused_aggregators.at(i)->name() == name) {
      return i;
    }
  }
  return -1;
}

int RecapView::findUnusedFilterByName(QString name) const {
  for (int i=0; i<m_unused_filters.size(); ++i) {
    if (m_unused_filters.at(i)->name() == name) {
      return i;
    }
  }
  return -1;
}

bool RecapView::eventFilter(QObject* object, QEvent* event) {
  if (event->type() == QEvent::MouseButtonDblClick) {
    QDockWidget* dockwidget = qobject_cast< QDockWidget* >(object);
    if(dockwidget) {
        rename(dockwidget);
        return true;
    }
  } else if (event->type() == QEvent::ContextMenu) {
    QMenu contextmenu;
    QAction* save = contextmenu.addAction(tr("&Save recap"));
    if (m_has_custom_filter) {
        save->setEnabled(false);
    }
    QAction* rename_action = contextmenu.addAction(tr("Re&name recap"));
    QMenu* load_menu = contextmenu.addMenu(tr("Load..."));
    QMenu* delete_menu = contextmenu.addMenu(tr("Delete..."));
    QList<QAction*> load_actions;
    QList<QAction*> delete_actions;
    QSettings settings;
    settings.beginGroup("saved recaps");
    QStringList saved_recaps = settings.childGroups();
    if (saved_recaps.isEmpty()) {
      load_menu->setEnabled(false);
      delete_menu->setEnabled(false);
    } else {
      Q_FOREACH(QString name, saved_recaps) {
        load_actions << load_menu->addAction(name);
        delete_actions << delete_menu->addAction(name);
      }
    }
    if (QAction* selected_action = contextmenu.exec(static_cast<QContextMenuEvent*>(event)->globalPos())) {
      if (selected_action == save) {
        settings.beginGroup(m_name);
        settings.setValue("state", saveState());
        settings.endGroup();
      } else if (selected_action == rename_action) {
        if (QDockWidget* dockwidget = qobject_cast<QDockWidget*>(object)) {
          rename(dockwidget);
        }
      } else if (load_actions.contains(selected_action)) {
        settings.beginGroup(selected_action->text());
        restoreState(settings.value("state"));
        settings.endGroup();
      } else if (delete_actions.contains(selected_action)) {
        settings.beginGroup(selected_action->text());
        settings.remove(""); //remove state *and* recap name
        settings.endGroup();
      } else {
        qWarning("Unhandled context memu action '%s'", selected_action->text().toLocal8Bit().data());
      }
      settings.endGroup();
    }
    return true;
  }
  return super::eventFilter(object, event);
}

void RecapView::rename(QDockWidget* dockwidget) {
  QLineEdit* nameedit = new QLineEdit(dockwidget);
  dockwidget->setTitleBarWidget(nameedit);
  nameedit->setText(dockwidget->windowTitle());
  nameedit->setFocus();
  connect(nameedit, &QLineEdit::editingFinished, nameedit, &QLineEdit::deleteLater);
  connect(nameedit, &QLineEdit::textEdited, this, &RecapView::setName);
}

void RecapView::setName(QString name) {
  name = name.trimmed();
  if (!name.isEmpty()) {
    parentWidget()->setWindowTitle(name);
    m_name = name;
    emit nameChanged();
  }
}

void RecapView::mouseDoubleClickEvent(QMouseEvent* event) {
    if (corner().contains(event->pos())) {
        display_list(detachedFilteredCube());
        event->accept();
    } else {
        super::mouseDoubleClickEvent(event);
        event->accept(); // Accept all double clicks in recap, prevents rename started by filter
    }
}

void RecapView::display_list(Datacube* detachedCube) {
    Q_ASSERT(detachedCube);
    // QMainWindow must be used as parent for QDockWidget, or it won't move as floating
    QMainWindow* main_window = 0L;
    for (QWidget* parent = parentWidget(); !main_window && parent; parent = parent->parentWidget()) {
        main_window = qobject_cast< QMainWindow* >(parent);
    }
    QStringList filters;
    Q_FOREACH(qdatacube::AbstractFilter::Ptr filter, detachedCube->filters()) {
        filters << filter->shortName();
    }
    QDockWidget* dock = new QDockWidget(tr("Selection list (%1)").arg(filters.join(',')), main_window);
    dock->setAttribute(Qt::WA_DeleteOnClose);
    QWidget* widget = new QWidget(dock);
    QHBoxLayout* lay = new QHBoxLayout();
    widget->setLayout(lay);
    QTableView* tv = new QTableView(dock);
    lay->addWidget(tv);
    dock->setWidget(widget);
    FilteredContainerList* filtered_list = new FilteredContainerList(detachedCube, dock);
    connect(detachedCube, &qdatacube::Datacube::destroyed, dock, &QDockWidget::close);
    detachedCube->setParent(dock);
    // ensure that the dock is closed (and deleted) when the document disappears. That takes down the datacubes, the container lists and others
    connect(m_document, &document_t::destroyed, dock, &QDockWidget::close);
    filtered_list->setSourceModel(m_document->containers());
    tv->setModel(filtered_list);
    tv->setSortingEnabled(true);
    tv->setSelectionBehavior(QAbstractItemView::SelectRows);
    tv->verticalHeader()->hide();
    TableViewUtils::setupShowHideColumnsAndCopySelection(tv);
    ContainerViewItemDelegates::setupDelegates(tv, m_document);
    QSettings settings;
    if (!settings.value("view/equipment number").toBool()) {
        // TODO: Connect this to some signal if it is shown/hidden
        tv->hideColumn(basic_container_list_t::EQUIPMENT_NO_COL);
    }
    filtered_list->synchronize(tv->selectionModel(), m_selection_pool);
    tv->resizeColumnsToContents();
    tv->resizeRowsToContents();
    dock->resize(tv->sizeHint());
    dock->setObjectName(m_name+"list_dock");
    dock->setFloating(true);
    dock->move(QCursor::pos() - (style()->pixelMetric(QStyle::PM_DockWidgetFrameWidth)+1) * QPoint(1,1));
    dock->show();
}

void RecapView::displayRecapAsList() {
    display_list(detachedFilteredCube());
}

void RecapView::copyRecapToClipboard() {
    if(formatters().isEmpty()) {
        return;
    }
    QString recapAsHtml = DatacubeHtmlExporter::datacubeToHtml(datacube(), *formatters().at(0));
    QMimeData* mimeData = new QMimeData();
    mimeData->setHtml(recapAsHtml);
    QApplication::clipboard()->setMimeData(mimeData);
}

void RecapView::display_selection_as_recap() {
    if(m_recapManager) {
        RecapView* recap = m_recapManager->add_recap();
        QSet<int> container_ids;
        Q_FOREACH(const ange::containers::Container* container, m_selection_pool->containers()) {
            container_ids << container->id();
        }
        recap->datacube()->resetFilter();
        cloneFilterAndAggregatorsFromDataCube(datacube(), recap);
        recap->addCustomFilter(qdatacube::AbstractFilter::Ptr(new RecapFilterContainerId(container_ids,m_document->containers())));
        recap->setName("Selection");
    }
}

void RecapView::displaySelectionAsList() {
    qdatacube::Datacube* detachedCube= detachedFilteredCube();
    QSet<int> container_ids;
    Q_FOREACH(const ange::containers::Container* container, m_selection_pool->containers()) {
        container_ids << container->id();
    }
    detachedCube->addFilter(qdatacube::AbstractFilter::Ptr(
                                        new RecapFilterContainerId(container_ids,m_document->containers())));
    display_list(detachedCube);
}

void RecapView::displayCellContextMenu(QPoint /*pos*/, int /*row*/, int /*column*/) {
    QMenu contextmenu(this);
    generalContextMenu(contextmenu);
    if (contextmenu.actions().isEmpty()) {
        return;
    }
    contextmenu.exec(QCursor::pos());
}

bool RecapView::split(Qt::Orientation orientation, int section, QString filter) {
  if (section <= datacube()->headerCount(orientation)) {
    int unused_filter_index = findUnusedAggregatorByName(filter);
    if (unused_filter_index >= 0) {
      split(orientation, section, unused_filter_index);
      return true;
    }
  }
  return false;
}

bool RecapView::filter(QString filter, QString category_name) {
  int unused_filter_index = findUnusedAggregatorByName(filter);
  if (unused_filter_index >= 0) {
    qdatacube::AbstractAggregator::Ptr aggregator = m_unused_aggregators.takeAt(unused_filter_index);
    int category_index = -1;
    for(int i = 0; i <  aggregator->categoryCount(); i++) {
        if (aggregator->categoryHeaderData(i).toString() == (category_name)) {
            category_index = i;
            break;
        }
    }
    Q_ASSERT(category_index != -1);
    if (category_index>=0) {
      datacube()->addFilter(AbstractFilter::Ptr(new qdatacube::FilterByAggregate(aggregator, category_index)));
      return true;
    }
  }
  return false;
}

bool RecapView::disableFilter(QString filter) {
  const int index = findUnusedAggregatorByName(filter);
  if (index>=0) {
    m_unused_aggregators.removeAt(index);
    return true;
  }
  const int filter_index = findUnusedFilterByName(filter);
  if (filter_index>=0) {
    m_unused_filters.removeAt(filter_index);
    return true;
  }
  return false;
}

void RecapView::setBundleHelper(ContainerSelectionBundleHelper* bundlehelper) {
    m_bundlehelper = bundlehelper;
}

void RecapView::wheelEvent(QWheelEvent* event) {
    bool ctrlHeld = event->modifiers() & Qt::ControlModifier;
    if (ctrlHeld) {
        QFont f = font();
        qreal pointsize = f.pointSizeF();
        if (event->delta() > 0) {
            pointsize = qMin<qreal>(40, pointsize*1.1); // 40 is a arbitrary chosen number for max. it was around what 'made sense'
        } else {
            pointsize = qMax<qreal>(1, pointsize/1.1); // 1 is a arbirtrary chosen number for min. It was around where zooming in actually stopped.
        }
        f.setPointSizeF(pointsize);
        setFont(f);
    } else {
        super::wheelEvent(event);
    }
}

void RecapView::addCustomFilter(qdatacube::AbstractFilter::Ptr filter) {
    m_has_custom_filter = true;
    datacube()->addFilter(filter);
}

void RecapView::setRecapManager(RecapManagerInterface* recapManager) {
    m_recapManager = recapManager;
}



#include "recapview.moc"
