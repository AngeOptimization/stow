#include "importvessel.h"

#include <ange/vessel/vessel.h>

#include <QtTest>
#include <QObject>

using namespace ange::vessel;

class ImportVesselTest : public QObject {
    Q_OBJECT
private Q_SLOTS:
    void testReadVessel();
};

QTEST_GUILESS_MAIN(ImportVesselTest);

void ImportVesselTest::testReadVessel() {
    QScopedPointer<const ange::vessel::Vessel> vessel;
    VesselParseResult parseResult = readVessel(QFINDTESTDATA("data/micro.xls.json"));
    vessel.reset(parseResult.m_vessel);
    QCOMPARE(parseResult.m_vesselFileData.size(), 3192);
    QCOMPARE(vessel->name(), QStringLiteral("Micro"));
}

#include "importvesseltest.moc"
