find_package(Qt5Test 5.2.0 REQUIRED NO_MODULE)

macro(create_test testbasename)
    add_executable(${testbasename} ${testbasename}.cpp)
    target_link_libraries(${testbasename}
        gui
        Ange::Vessel
        KF5::Archive
        Qt5::Test
    )
    add_test(${testbasename} ${testbasename})
endmacro(create_test)

create_test(importvesseltest)
