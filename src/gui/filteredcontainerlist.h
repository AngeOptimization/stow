#ifndef FILTERED_CONTAINER_LIST_H
#define FILTERED_CONTAINER_LIST_H

#include <QSortFilterProxyModel>

class QItemSelectionModel;
class pool_t;
namespace qdatacube {
class AbstractFilter;
class Datacube;
}

/**
 * This class models a filtered (by the global filter in a datacube) view on top of the standard container list
 */
class FilteredContainerList : public QSortFilterProxyModel
{
  Q_OBJECT
  public:
    FilteredContainerList(qdatacube::Datacube* datacube, QObject* parent = 0);
    virtual ~FilteredContainerList();

    /**
     * Use this to synchronize a selectionModel (for this model) with the given pool
     */
    void synchronize(QItemSelectionModel* selection_model, pool_t* pool);
  private Q_SLOTS:
    void updateSelectionOnPool(const QItemSelection& selected, const QItemSelection& deselected);
    void selectRows(QList<int> source_rows);
    void deselectRows(QList<int> source_rows);
  protected:
    virtual bool filterAcceptsRow(int source_row, const QModelIndex& source_parent) const;
  private:
    qdatacube::Datacube* m_datacube;
    bool m_ignoreSelectionChanges;
    QItemSelectionModel* m_selectionModel;
    pool_t* m_pool;
};

#endif // FILTERED_CONTAINER_LIST_H
