#include "containerselectionbundlehelper.h"

#include <document/pools/pool.h>
#include <document/containers/container_list.h>
#include <document/undo/bundlecontainercommand.h>
#include <document/undo/merger_command.h>
#include <document/undo/container_stow_command.h>
#include <document/document.h>
#include <document/stowage/stowage.h>
#include <document/stowage/container_move.h>
#include <ange/containers/container.h>
#include <ange/vessel/slot.h>
#include <QAction>

using ange::containers::Container;
using ange::containers::IsoCode;

ContainerSelectionBundleHelper::~ContainerSelectionBundleHelper() {
    setObjectName("ContainerSelectionBundleHelper");
}

ContainerSelectionBundleHelper::ContainerSelectionBundleHelper(pool_t* selection_pool, document_t* document)
        : QObject(document)
        , m_bundleCount(-1)
        , m_pool(selection_pool)
        , m_document(document)
        , m_canBeBundledDirty(true)
        , m_canBeUnbundledDirty(true)
        , m_currentCall(0) {
    connect(m_pool,SIGNAL(changed()),SLOT(setDirty()));
}

int ContainerSelectionBundleHelper::bundleCount() {
    if(m_canBeUnbundledDirty) {
        m_selectionCanBeUnbundled = recalculateCanBeUnbundled();
        m_canBeUnbundledDirty = false;
    }
    return m_bundleCount;
}

bool ContainerSelectionBundleHelper::recalculateCanBeBundled() {
    QList<int> rows = m_pool->included_rows();
    if(rows.size() < 2) {
        m_bundleReason = "too few";
        return false;
    }
    if(rows.size() > 7) {
        m_bundleReason = "too many";
        return false;
    }
    IsoCode::IsoLength length = IsoCode::UnknownLength;
    bool first = true;
    bool someContainersStowed = false;
    bool someContainersUnstowed = false;
    // if we find one container that doesn't fit in, return false. then it can't be bundled.
    QList<Container*> containers;
    Q_FOREACH(int row, rows) {
        containers << m_document->containers()->get_container_at(row);
    }
    Q_FOREACH(Container* container, containers) {
        if (!container->isBundlable()) {
            m_bundleReason = "not bundlable";
            if(!container->empty()) {
                m_bundleReason = "not empty";
            }
            return false;
        }
        if(container->bundleParent()) {
            m_bundleReason = "already bundled";
            return false;
        }
        if(!container->bundleChildren().isEmpty()) {
            m_bundleReason = "already bundled";
            return false;
        }
        if(first) {
            length = container->isoLength();
            first = false;
        }
        Q_ASSERT(length != ange::containers::IsoCode::UnknownLength);
        if(length != container->isoLength()) {
            m_bundleReason = "different lengths";
            return false;
        }
        QList<const container_move_t*> container_moves = m_document->stowage()->moves(container);
        if(container_moves.isEmpty()) {
            someContainersUnstowed = true;
        } else {
            someContainersStowed = true;
            Q_FOREACH(const container_move_t* move, container_moves) {
                if(move->type() == ange::angelstow::MasterPlannedType) {
                    m_bundleReason = "master planned";
                    return false;
                }
            }
        }
        if(someContainersStowed && someContainersUnstowed) {
            m_bundleReason = "mix of stowed/unstowed";
            return false;
        }
    }
    m_bundleReason.clear();
    return true;
}

bool ContainerSelectionBundleHelper::recalculateCanBeUnbundled() {
    QList<int> rows = m_pool->included_rows();
    m_bundleCount = -1;
    if(rows.isEmpty()) {
        m_unbundleReason = "no selection";
        return false;
    }
    if(rows.size() > 20) {
        m_unbundleReason = "selection too large";
        return false;
    }
    QSet<Container*> bundleParents;
    QList<Container*> containers;
    Q_FOREACH(int row, rows) {
        containers << m_document->containers()->get_container_at(row);
    }
    Q_FOREACH(Container* container, containers) {
        if(!container->bundleParent() && container->bundleChildren().isEmpty()) {
            m_unbundleReason = "not bundled";
            return false;
        }
        if(container->bundleParent()) {
            bundleParents << container->bundleParent();
        }
        if(!container->bundleChildren().isEmpty()) {
            bundleParents << container;
        }
    }
    m_bundleCount=bundleParents.size();
    m_unbundleReason.clear();
    return true;
}



bool ContainerSelectionBundleHelper::selectionCanBeBundled() {
    if(m_canBeBundledDirty) {
        m_selectionCanBeBundled = recalculateCanBeBundled();
        m_canBeBundledDirty=false;
    }
    return m_selectionCanBeBundled;
}

bool ContainerSelectionBundleHelper::selectionCanBeUnbundled() {
    if(m_canBeUnbundledDirty) {
        m_selectionCanBeUnbundled = recalculateCanBeUnbundled();
        m_canBeUnbundledDirty = false;
    }
    return m_selectionCanBeUnbundled;
}

void ContainerSelectionBundleHelper::setDirty() {
    m_canBeBundledDirty=true;
    m_canBeUnbundledDirty=true;
}

QAction* ContainerSelectionBundleHelper::bundleAction() {
    if(m_bundleAction.isNull()) {
        m_bundleAction = new QAction("will be replaced", this);
        connect(m_bundleAction.data(),SIGNAL(triggered(bool)),SLOT(bundleSelectedContainers()));
    }
    if(selectionCanBeBundled()) {
        m_bundleAction.data()->setText(QString("Bundle selected containers"));
        m_bundleAction.data()->setEnabled(true);
    } else {
        m_bundleAction.data()->setText(QString("Bundle (%1)").arg(m_bundleReason));
        m_bundleAction.data()->setEnabled(false);
    }
    return m_bundleAction.data();
}

QAction* ContainerSelectionBundleHelper::unbundleAction() {
    if(m_unbundleAction.isNull()) {
        m_unbundleAction = new QAction("will be replaced", this);
        connect(m_unbundleAction.data(),SIGNAL(triggered(bool)),SLOT(unbundleSelectedContainers()));
    }
    if(selectionCanBeUnbundled()) {
        m_unbundleAction.data()->setText(QString("Unbundle selected %1 container(s)").arg(m_bundleCount));
        m_unbundleAction.data()->setEnabled(true);
    } else {
        m_unbundleAction.data()->setText(QString("Unbundle (%1)").arg(m_unbundleReason));
        m_unbundleAction.data()->setEnabled(false);
    }
    return m_unbundleAction.data();
}

namespace {
    bool lowest_slot(const ange::vessel::Slot* first, const ange::vessel::Slot* second) {
        if(first->brt().bay() == second->brt().bay()) {
            if(first->brt().row() == second->brt().row()) {
                return first->brt().tier() < second->brt().tier();
            } else {
                return first->brt().row() < second->brt().row();
            }
        } else {
            return first->brt().bay() < second->brt().bay();
        }
    }
}

void ContainerSelectionBundleHelper::bundleSelectedContainers() {
    QList<int> rows = m_pool->included_rows();
    if(rows.size() < 2) {
        qDebug() << "we should have at least two containers to be able to bundle stuff. looks like a coding error";
        return;
    }
    QList<Container*> containers;
    Q_FOREACH(int row, rows) {
        containers << m_document->containers()->get_container_at(row);
    }
    //maybe containers are already stowed
    bool stowed = !m_document->stowage()->moves(containers.front()).isEmpty();
    if(stowed) {
        QList<const ange::vessel::Slot*> occupied_slots;
        Q_FOREACH(Container* container, containers) {
            occupied_slots << m_document->stowage()->container_position(container, m_currentCall);
        }
        qSort(occupied_slots.begin(),occupied_slots.end(),lowest_slot);

        Container* first_container = m_document->containers()->get_container_by_id(m_document->stowage()->contents(occupied_slots.first(), m_currentCall).container()->id());
        containers.removeOne(first_container);

        merger_command_t* merger_command = new merger_command_t(QString("Bundle selected %1 containers").arg(rows.size()));

        //unstow containers
        container_stow_command_t* unstow = new container_stow_command_t(m_document, 0, true);
        BundleContainerCommand* bundle = new BundleContainerCommand(BundleContainerCommand::Add,m_document,first_container);
        Q_FOREACH(Container* container, containers) {
            unstow->add_discharge(container,m_currentCall, ange::angelstow::MicroStowedType);
            bundle->addContainer(container);
        }
        merger_command->push(unstow);
        merger_command->push(bundle);
        m_document->undostack()->push(merger_command);
    } else {
        Container* first_container = containers.takeFirst();
        BundleContainerCommand* command = new BundleContainerCommand(BundleContainerCommand::Add,m_document,first_container);
        Q_FOREACH(Container* container, containers) {
            command->addContainer(container);
        }
        m_document->undostack()->push(command);
    }
    setDirty();
}

void ContainerSelectionBundleHelper::unbundleSelectedContainers() {
    QList<int> rows = m_pool->included_rows();
    if(rows.isEmpty()) {
        qDebug() << "we should have at least a container to be able to unbundle stuff. looks like a coding error";
        return;
    }

    //find all bottom containers
    QList<Container*> containers;
    Q_FOREACH(int row, rows) {
        containers << m_document->containers()->get_container_at(row);
    }
    QSet<Container*> bottom_containers;
    Q_FOREACH(Container* container, containers ) {
        if(bottom_containers.contains(container)) {
            continue;
        }
        if(!container->bundleChildren().isEmpty()) {
            bottom_containers << container;
            continue;
        }
        if(container->bundleParent()) {
            bottom_containers << container->bundleParent();
            continue;
        }
        qDebug() << "found container that aren't part of a bundle. this shouldn't happen. Better do nothing";
        return;
    }
    merger_command_t* merger_command = new merger_command_t("Unbundle containers");

    Q_FOREACH(Container* bottom_container, bottom_containers) {
        BundleContainerCommand* bundleCommand = new BundleContainerCommand(BundleContainerCommand::Remove,m_document, bottom_container);
        Q_FOREACH(Container* childcontainer, bottom_container->bundleChildren()) {
            bundleCommand->addContainer(childcontainer);
        }
        merger_command->push(bundleCommand);
    }

    m_document->undostack()->push(merger_command);
    setDirty();
}

void ContainerSelectionBundleHelper::setCurrentCall(const ange::schedule::Call* call) {
    m_currentCall = call;
}




#include "containerselectionbundlehelper.moc"
