#ifndef MAP_PORT_INTERACTIVELY_SOLVER_H
#define MAP_PORT_INTERACTIVELY_SOLVER_H

#include <QObject>
namespace ange {
namespace schedule {
class Schedule;
}
}

class ParserProblem;

class map_port_interactively_solver_t : public QObject {
  Q_OBJECT
  public:
    explicit map_port_interactively_solver_t(ange::schedule::Schedule* schedule, QObject* parent = 0);
    bool could_solve(ParserProblem* problem);
    QString name(ParserProblem* problem) const;
    bool attempt_solution(ParserProblem* problem);
  private:
    ange::schedule::Schedule* m_schedule;
};

#endif // MAP_PORT_INTERACTIVELY_SOLVER_H
