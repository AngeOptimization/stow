#include "map_port_interactively_solver.h"
#include "parserproblem.h"
#include "gui/dialogs/map_port_dialog.h"
#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>

using ange::schedule::Call;

map_port_interactively_solver_t::map_port_interactively_solver_t(ange::schedule::Schedule* schedule, QObject* parent) :
QObject(parent),
m_schedule(schedule)
{

}

QString map_port_interactively_solver_t::name(ParserProblem* problem) const {
  return tr("Map %1 %2 to call in schedule").arg(problem->data("uncode").toString()).arg(problem->data("voyage_code").toString());
}

bool map_port_interactively_solver_t::attempt_solution(ParserProblem* problem) {
  if (problem->type() == ParserProblem::UnmappedPort) {
    QString uncode = problem->data("uncode").toString();
    QString voyage_code = problem->data("voyage_code").toString();
    const Call* first_valid_call = problem->data("first_possible_call").value<const Call*>();
    const Call* last_valid_call = problem->data("last_possible_call").value<const Call*>();
    Q_ASSERT(first_valid_call);
    Q_ASSERT(last_valid_call);
    map_port_dialog_t map_port_dialog(m_schedule, uncode, voyage_code, first_valid_call, last_valid_call);
    if (QDialog::Accepted == map_port_dialog.exec()) {
      const ange::schedule::Call* call = map_port_dialog.mapped_to();
      problem->solve(QVariant::fromValue(call));
      return true;
    }
  }
  return false;
}

bool map_port_interactively_solver_t::could_solve(ParserProblem* problem) {
  return problem->type() == ParserProblem::UnmappedPort;
}

#include "map_port_interactively_solver.moc"
