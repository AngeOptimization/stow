#include "import_edifact_wizard.h"

#include "baplieparser.h"
#include "baplieparserresult.h"
#include "baplietooldstyleedifactreaderhack.h"
#include "coprar20parser.h"
#include "coprar20tooldstyleedifactreaderhack.h"
#include "document.h"
#include "edifact_container_reader.h"
#include "filterwrongcharsiodevice.h"
#include "map_port_interactively_solver.h"
#include "override_cursor_handler.h"
#include "schedule_view.h"
#include "schedulemodel.h"
#include "parserproblem.h"
#include "stowage.h"

#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>
#include <ange/vessel/vessel.h>
#include <ange/vessel/vesseltank.h>

#include <QtEdith/edifactsegmentreader.h>
#include <QtEdith/segment.h>

#include <QFileDialog>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QTextEdit>
#include <QtGui>

#include <stdexcept>

using namespace ange::units;

import_edifact_wizard_t::import_edifact_wizard_t(document_t* document, pool_t* selection, schedule_view_t* callview,
                                                 QWidget* parent, Qt::WindowFlags flags)
  : QWizard(parent, flags)
{
    m_call_view = callview;
    m_selection = selection;
    m_document = document;
    m_intropage = new intropage_t(this);
    m_errorpage = new parse_error_page_t(this);
    m_warningpage = new warning_page_t(this);
    setPage(INTRO, m_intropage);
    setPage(PARSE_ERROR, m_errorpage);
    setPage(STOW_WARNING, m_warningpage);
    setPage(SUCCESS, new sucess_page_t(this));
    setWindowTitle(tr("Import EDIFACT"));
}

intropage_t::intropage_t(QWidget* parent)
  : QWizardPage(parent), m_need_reparse(false)
{
    m_wizard = qobject_cast<import_edifact_wizard_t*>(parent);
    m_parsed = false;
    setTitle(tr("Select EDIFACT file to import"));
    setPixmap(QWizard::WatermarkPixmap, QPixmap(":icons/icons/auto-tool.svg"));

    QLabel* label = new QLabel(tr("Import EDIFACT messages\nBAPLIE\nCOARRI\nCOPRAR\nTANSTA"));
    label->setWordWrap(true);

    QLabel* nameLabel = new QLabel("Filename:");
    m_namelineedit = new QLineEdit(this);
    QSettings settings;
    QString edifactDirectory = settings.value("edifactDirectory", QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation)).toString();
    m_namelineedit->setText(edifactDirectory);
    QPushButton* openbutton = new QPushButton(this);
    openbutton->setIcon(QIcon(":/icons/icons/document-open.png"));

    registerField("filename*", m_namelineedit);
    connect(openbutton , SIGNAL(clicked()), SLOT(get_filename_dialog()));
    connect(m_namelineedit, SIGNAL(textChanged(QString)), this, SIGNAL(completeChanged()));
    connect(m_namelineedit, SIGNAL(textChanged(QString)), this, SLOT(set_reparse()));

    QHBoxLayout* flayout = new QHBoxLayout;
    flayout->addWidget(nameLabel);
    flayout->addWidget(m_namelineedit);
    flayout->addWidget(openbutton);

    QVBoxLayout* layout = new QVBoxLayout;
    layout->addWidget(label);
    layout->addSpacing(15);
    layout->addSpacing(20);
    layout->addLayout(flayout);
    setLayout(layout);
}

int intropage_t::nextId() const {
    if (!m_parse_errors.isEmpty()) {
        return import_edifact_wizard_t::PARSE_ERROR;
    } else if (!m_parse_warnings.isEmpty()) {
        return import_edifact_wizard_t::STOW_WARNING;
    } else {
        return import_edifact_wizard_t::SUCCESS;
    }
}

void intropage_t::set_reparse() {
    m_need_reparse = true;
}

bool intropage_t::validatePage() {
    QString fileName = m_namelineedit->displayText();
    if (!m_need_reparse) {
        QString text = tr("No EDIFACT parsing of file '%1'").arg(fileName);
        qDebug() << text;
        m_parse_warnings << new message_t(message_t::WARNING, text, text);
        return QWizardPage::validatePage();
    }

    override_cursor_handler_t overrideCursor(qApp, Qt::WaitCursor);
    m_need_reparse = false;
    m_parsed = false;

    if (!QFile::exists(fileName)) {
        QString text = tr("EDIFACT file '%1' does not exist").arg(fileName);
        QString detailtext("File not found");
        m_parse_errors << new message_t(message_t::ERROR_, text, detailtext);
        return QWizardPage::validatePage();
    }
    QFile file(fileName);
    FilterWrongCharsIODevice filter(&file);
    if (!filter.open(QIODevice::ReadOnly)) {
        QString text = tr("EDIFACT file '%1' does not exist").arg(fileName);
        QString detailtext("File not found");
        m_parse_errors << new message_t(message_t::ERROR_, text, detailtext);
        return QWizardPage::validatePage();
    }
    QtEdith::EdifactSegmentReader ediReader(&filter);
    if (!ediReader.hasNext()) {
        QString text = "Not an EDI file";
        QString detailtext(tr("The file '%1' does not appear to be an EDI file. "
                              "It doesn't contain any EDI segments.").arg(fileName));
        m_parse_errors << new message_t(message_t::ERROR_, text, detailtext);
        return QWizardPage::validatePage();
    }
    QtEdith::Segment unb = ediReader.next();
    if (unb.tag() != QByteArray("UNB")) {
        QString text = tr("EDIFACT file '%1' doesn't start with UNB").arg(fileName);
        QString detailtext("UNB expected");
        m_parse_errors << new message_t(message_t::ERROR_, text, detailtext);
        return QWizardPage::validatePage();
    }
    if (!ediReader.hasNext()) {
        QString text = "Bad EDI file";
        QString detailtext(tr("The file '%1' has only a single segment.").arg(fileName));
        m_parse_errors << new message_t(message_t::ERROR_, text, detailtext);
        return QWizardPage::validatePage();
    }
    if (Coprar20Parser::unhIsCoprar20(ediReader.peek())) {
        CoprarParserResult result = Coprar20Parser::parse(&ediReader, unb, m_wizard->call_view()->current_call()->uncode());
        if (result.hasErrors()) {
            QString text = tr("COPRAR 2.0 file '%1' failed to parse").arg(fileName);
            Q_FOREACH (const EdiProblem& error, result.errorList()) {
                m_parse_errors << new message_t(message_t::ERROR_, text, error.message());
            }
            return QWizardPage::validatePage();
        }
        m_reader = new Coprar20toOldStyleEdifactReaderHack(m_wizard->document(), m_wizard->pool(), m_wizard->call_view()->current_call());
        bool parse_ok = static_cast<Coprar20toOldStyleEdifactReaderHack*>(m_reader)->parse(result, fileName);
        if (!parse_ok) {
            Q_ASSERT(false); // 2015-02-16 Kim: How can we end up here?
            QString text = tr("EDIFACT file '%1' is not valid").arg(fileName);
            QString detailtext = m_reader->error_message();
            m_parse_errors << new message_t(message_t::ERROR_, text, detailtext);
            return QWizardPage::validatePage();
        }
    } else if (BaplieParser::acceptedUnh(ediReader.peek())) {
        BaplieParser parser;
        parser.parse(&ediReader);
        BaplieParserResult result = parser.result();
        if (result.hasErrors()) {
            QString text = tr("BAPLIE file '%1' failed to parse").arg(fileName);
            Q_FOREACH (const EdiProblem& problem, result.problemList) {
                m_parse_errors << new message_t(message_t::ERROR_, text, problem.message());
            }
            return QWizardPage::validatePage();
        }
        m_reader = new BaplieToOldStyleEdifactReaderHack(m_wizard->document(), m_wizard->pool(), result, fileName);
    } else {
        // Reading TANSTA and COARRI
        m_reader = new edifact_container_reader_t(m_wizard->document(), m_wizard->pool(), m_wizard->call_view()->current_call());
        bool parse_ok = m_reader->parse(fileName);
        if (!parse_ok) { // stop further processing gracefully
            QString text = tr("EDIFACT file '%1' is not valid").arg(fileName);
            QString detailtext = m_reader->error_message();
            m_parse_errors << new message_t(message_t::ERROR_, text, detailtext);
            return QWizardPage::validatePage();
        }
    }

    QStringList warnings = m_reader->warnings();
    if (!warnings.isEmpty()) {
        QString text = tr("EDIFACT file could be parsed but with formal warnings");
        QString detailedtext = warnings.join("\n");
        m_parse_warnings << new message_t(message_t::WARNING, text, detailedtext);
    }

    // handling container lists
    if (m_reader->doctype() == ange::edifact::edifact_parser_t::BAPLIE
            || m_reader->doctype() == ange::edifact::edifact_parser_t::COARRI
            || m_reader->doctype() == ange::edifact::edifact_parser_t::COPRAR) {
        try {

            map_port_interactively_solver_t solver(m_wizard->document()->schedule());
            Q_FOREACH(ParserProblem* p, m_reader->problems()) {
                Q_ASSERT(p->data("first_possible_call").value<const ange::schedule::Call*>());
                solver.attempt_solution(p);
            }

            if (m_reader->has_errors()) { // stop further processing gracefully
                QString text = tr("EDIFACT file '") + fileName + tr("' has errors");
                QString detailtext = m_reader->error_message();
                m_parse_errors << new message_t(message_t::ERROR_, text, detailtext);
                return QWizardPage::validatePage();
            }

            m_reader->add_containers_to_document();
            if (!m_reader->unplacedcontainers().isEmpty()) {
                QString text = QString("Could not place %1 containers because slot was already filled, or did not exist on the vessel. "
                                       "These needs to be stowed and are placed at port %2")
                               .arg(m_reader->unplacedcontainers().size()).arg(m_reader->loadcall()->name());
                QString detailedtext = "<h3>Containers left at port</h3>\n<table align=\"left\" border=1>";
                detailedtext += "\n<tr><th>Equipment nr.</th><th>b/r/t</th><th>Message</th></tr>";
                Q_FOREACH (const edifact_container_reader_t::container_error_t& error, m_reader->unplacedcontainers()) {
                    detailedtext += "\n<tr><td>" + error.container->equipmentNumber() + "</td><td>"
                                    + error.position.toString() + "</td><td>" + error.message + "</td></tr>";
                }
                detailedtext += "\n</table>";
                m_parse_warnings << new message_t(message_t::WARNING, text, detailedtext);
            }
            if (!m_reader->partiallyplacedcontainers().isEmpty()) {
                QString text = QString("Could only partially place %1 containers because slot were filled at some later point. "
                                       "These needs to be restowed and are placed at the port described in the table below.")
                               .arg(m_reader->partiallyplacedcontainers().size());
                QString detailedtext = "<h3>Restowed Containers</h3>\n<table align=\"left\" border=1>";
                detailedtext += "\n<tr><th>Equipment nr.</th><th>b/r/t</td><th>Restow Call</th></tr>";
                Q_FOREACH (const ange::containers::Container* container, m_reader->partiallyplacedcontainers().keys()) {
                    detailedtext += "\n<tr><td>" + container->equipmentNumber() + "</td><td>"
                                    + m_reader->partiallyplacedcontainers().value(container).first.toString()
                                    + "</td><td>" +  m_reader->partiallyplacedcontainers().value(container).second->name() + "</td></tr>";
                }
                detailedtext += "\n</table>";
                m_parse_warnings << new message_t(message_t::WARNING, text, detailedtext);
            }
            if (!m_reader->pol_remapped_containers().isEmpty()) {
                QString text = QString("Some containers had their POL (point of load) remapped to avoid clashes with existing stowage.");
                QString detailedtext = "<h3>Containers with re-mapped POL</h3>\n<table align=\"left\" border=1>";
                detailedtext += "\n<tr><th>Equipment nr.</th><th>b/r/t</td><th>Original POL</th><th>New POL</th></tr>";
                Q_FOREACH (const edifact_container_reader_t::container_pol_remapped_t& pol_remapped, m_reader->pol_remapped_containers()) {
                    detailedtext += "\n<tr><td>" + pol_remapped.container->equipmentNumber() + "</td><td>"
                                    + pol_remapped.position.toString() + "</td><td>"
                                    + pol_remapped.from->uncode() + " " + pol_remapped.from->voyageCode() + "</td><td>"
                                    + pol_remapped.to->uncode() + " " + pol_remapped.to->voyageCode() + "</td></tr>";
                }
                detailedtext += "\n</table>";
                m_parse_warnings << new message_t(message_t::WARNING, text, detailedtext);
            }
            m_parsed = true;
        } catch (std::exception& e) {
            QString text = tr("EDIFACT file '") + fileName + tr("' could not be imported");
            QString detailtext = e.what();
            m_parse_errors << new message_t(message_t::ERROR_, text, detailtext);
        }

    } else if (m_reader->doctype() == ange::edifact::edifact_parser_t::TANSTA) {

        if (m_reader->has_errors()) { // stop further processing gracefully
            QString text = QString("EDIFACT file '%1' has errors").arg(fileName);
            QString detailtext = m_reader->error_message();
            m_parse_errors << new message_t(message_t::ERROR_, text, detailtext);
            return QWizardPage::validatePage();
        }

        if (m_reader->tankMappings().size() <= 0) {
            QString text = QString("EDIFACT TANSTA file '%1' contains no tank conditions").arg(fileName);
            m_parse_warnings << new message_t(message_t::WARNING, text, text);
            return QWizardPage::validatePage();
        }

        m_reader->addTanksToDocument();
        m_parsed = true;
        return QWizardPage::validatePage();

    } else {
        QString text = tr("EDIFACT message type'") +  m_reader->doctype_string() + tr("' in file '")  + fileName + tr("' is not supported");
        QString detailtext = "Supported EDIFACT message types are:\nBAPLIE\nCOARRI\nCOPRAR\nTANSTA";
        m_parse_errors << new message_t(message_t::ERROR_, text, detailtext);
    }
    return QWizardPage::validatePage();
}

QString intropage_t::get_summary() {
    if (!m_parsed) {
        return QString("");
    }

    if (m_reader->doctype() == ange::edifact::edifact_parser_t::BAPLIE
            || m_reader->doctype() == ange::edifact::edifact_parser_t::COARRI
            || m_reader->doctype() == ange::edifact::edifact_parser_t::COPRAR) {
        QString summary = "<h3>Summary</h3>\n<table align=\"left\">";
        summary += QString("<tr><td>EDIFACT file type:</td><td> %1 </td></tr>").arg(m_reader->doctype_string());
        summary += QString("<tr><td align=\"left\">Message Ref. nr.:</td><td> %1 </td></tr>").arg(m_reader->message_ref());
        summary += QString("<tr><td>Message Date:</td><td> %1 </td></tr>").arg(m_reader->message_date().toString(Qt::DefaultLocaleShortDate));
        summary += QString("<tr><td>Vessel Name:</td><td> %1 </td></tr>").arg(m_reader->vessel_name());
        summary += QString("<tr><td>Vessel Code:</td><td> %1 </td></tr>").arg(m_reader->vessel_code());
        summary += QString("<tr><td>Voyage Code:</td><td> %1 </td></tr>").arg(m_reader->voyage_code());
        summary += QString("<tr><td>Port Code:</td><td> %1 </td></tr>").arg(m_reader->port());
        summary += QString("<tr><td>Vessel Arrival Date:</td><td> %1 </td></tr>").arg(m_reader->vessel_arrival().toString(Qt::DefaultLocaleShortDate));
        summary += QString("<tr><td>imported containers:</td><td> %1</td></tr>").arg(m_reader->containers().size());
        summary += "</table>";
        return summary;
    }

    else if (m_reader->doctype() == ange::edifact::edifact_parser_t::TANSTA) {
        int mappedTanks = 0;
        Q_FOREACH (const edifact_container_reader_t::TankMapping* tankMapping, m_reader->tankMappings()) {
            if (tankMapping->vesselTank) {
                mappedTanks++;
            }
        }

        QString summary = QString("<h3>EDIFACT TANSTA Tank Conditions Import</h3>\n");

        summary += QString("<h4>'%1'</h4>").arg(m_namelineedit->displayText());
        summary += QString("<p></p>");
        summary += QString("<table align=\"left\" border=1>\n");
        summary += QString("<tr><th>EDI Mapping</th><th>TANSTA</th><th>Angelstow</th></tr>");
        summary += QString("<tr><td>Message Date</td><td>%1</td><td></td></tr>").arg(m_reader->message_date().toString(Qt::DefaultLocaleShortDate));
        summary += QString("<tr><td>Vessel Code</td><td>%1</td><td>%2</td></tr>").arg(m_reader->vessel_code()).arg(m_wizard->document()->vessel()->vesselCode());
        summary += QString("<tr><td>Vessel Name</td><td>%1</td><td>%2</td></tr>").arg(m_reader->vessel_name()).arg(m_wizard->document()->vessel()->name());
        summary += QString("<tr><td>Call Code</td><td>%1</td><td>%2</td></tr>").arg(m_reader->port()).arg(m_reader->currentCall()->uncode());
        summary += QString("<tr><td>Voyage Code</td><td>%1</td><td>%2</td></tr>").arg(m_reader->voyage_code()).arg(m_reader->currentCall()->voyageCode());
        summary += QString("<tr><td>Arrival Date</td><td>%1</td><td>%2</td></tr>").arg(m_reader->vessel_arrival().toString()).arg(m_reader->currentCall()->eta().toString(Qt::DefaultLocaleShortDate));
        summary += QString("</table>");

        summary += QString("<h4>TANSTA file has %1 identified tanks out of %2 tanks in total</h4>").arg(mappedTanks).arg(m_reader->tankMappings().size());
        summary += QString("<p></p>");
        summary += QString("<table align=\"left\" border=1>\n");
        summary += QString("<tr><th>TANSTA Tank Code</th><th>TANSTA Tank Name</th><th>Vessel Tank</th><th>Weight [t]</th><th>Density [t/m^3]</th></tr>");
        Q_FOREACH (const edifact_container_reader_t::TankMapping* tm, m_reader->tankMappings()) {
            QString vesselTankName = QString("no mapping");
            if (tm->vesselTank) {
                vesselTankName = tm->vesselTank->description();
            }
            QString density;
            if (!qIsNaN(tm->ediTankCondition.density / ton_per_meter3)) {
                density = QString("%1").arg(tm->ediTankCondition.density / ton_per_meter3);
            }
            summary += QString("<tr><td>%1</td><td>%2</td><td>%3</td><td>%4</td><td>%5</td></tr>\n")
                       .arg(tm->ediTankCondition.tankCode).arg(tm->ediTankCondition.tankName).arg(vesselTankName).arg(tm->ediTankCondition.weight / ton).arg(density);
        }
        summary += QString("</table>");
        return summary;
    }

    else {  // should never run this - unknown edifact message types are caught in intropage_t::validatePage() with error messages
        return QString("<h3>EDIFACT message type '%1' is not processed</h3>").arg(m_reader->doctype_string());
    }
}


void intropage_t::get_filename_dialog() {
    QString filename = QFileDialog::getOpenFileName(this, tr("Open EDIFACT file"), m_namelineedit->text(),
                                                    tr("All files (*.*)"));
    if (!filename.isEmpty()) {
        m_namelineedit->setText(filename);
        QSettings settings;
        QFileInfo fi(filename);
        settings.setValue("edifactDirectory", fi.absoluteDir().absolutePath());
    }
}

bool intropage_t::isComplete() const {
    QString filename = m_namelineedit->displayText();
    QFileInfo info(filename);
    return info.exists();
}

sucess_page_t::sucess_page_t(QWidget* parent)
{
    m_wizard = qobject_cast<import_edifact_wizard_t*>(parent);
    setTitle(QString("Successfully imported EDIFACT"));
    m_textedit = new QTextEdit();
    m_textedit->setReadOnly(true);
    QVBoxLayout* layout = new QVBoxLayout;
    layout->addWidget(m_textedit);
    setLayout(layout);
}

void sucess_page_t::initializePage() {
    m_textedit->clear();
    m_textedit->setText(m_wizard->intropage()->get_summary());
}

parse_error_page_t::parse_error_page_t(QWidget* parent)
  : QWizardPage(parent)
{
    m_wizard = qobject_cast<import_edifact_wizard_t*>(parent);
    setTitle(QString("Import ERRORS"));
    m_textedit = new QTextEdit();
    m_textedit->setReadOnly(true);
}

void parse_error_page_t::initializePage() {
    m_textedit->clear();
    m_textedit->setText("Detailed report:\n");
    delete layout();
    QVBoxLayout* layout = new QVBoxLayout;
    Q_FOREACH (message_t* message, m_wizard->intropage()->get_errors()) {
        QLabel* label = new QLabel;
        label->setText(QString("- %1").arg(message->m_text));
        label->setWordWrap(true);
        layout->addWidget(label);
        m_textedit->append(message->m_detailed_text);
        m_textedit->append("\n");
    }
    layout->addSpacing(20);
    layout->addWidget(m_textedit);
    setLayout(layout);
}


warning_page_t::warning_page_t(QWidget* parent)
  : QWizardPage(parent)
{
    m_wizard = qobject_cast<import_edifact_wizard_t*>(parent);
    setTitle(QString("Import WARNINGS"));
    m_textedit = new QTextEdit();
    m_textedit->setReadOnly(true);
}

void warning_page_t::initializePage() {
    // m_parse_errors is empty and m_parse_warnings has warnings, will happen
    // e.g. when containers has a length that is not allowed in slot.
    // For an example see ticket #1656.
    m_textedit->clear();
    m_textedit->setText(m_wizard->intropage()->get_summary());
    QVBoxLayout* layout = new QVBoxLayout;
    Q_FOREACH (message_t* message, m_wizard->intropage()->get_warnings()) {
        QLabel* label = new QLabel;
        label->setText(QString("- %1").arg(message->m_text));
        label->setWordWrap(true);
        layout->addWidget(label);
        m_textedit->append(message->m_detailed_text);
        m_textedit->append("\n");
    }
    layout->addSpacing(20);
    layout->addWidget(m_textedit);
    setLayout(layout);  // XXX Will not work if it is triggered using Back and Next
}

#include "import_edifact_wizard.moc"
