#include <QtTest>

#include "document.h"
#include "fileopener.h"
#include "import_edifact_wizard.h"
#include "pool.h"
#include "schedule_view.h"

#include <ange/schedule/schedule.h>

#include <QLineEdit>
#include <QObject>

class TestImportEdifactWizard : public QObject {
    Q_OBJECT
private slots:
    void initTestCase();
    void testMissingFile();
    void testEmptyFile();
    void testOneSegmentFile();
    void testBaplieTwoSegmentFile();
    void testCoprarTwoSegmentFile();
    void testCoarriTwoSegmentFile();
    void testTanstaTwoSegmentFile();
};

QTEST_MAIN(TestImportEdifactWizard);

void TestImportEdifactWizard::initTestCase() {
    Q_INIT_RESOURCE(data);
}

void TestImportEdifactWizard::testMissingFile() {
    OnDiskData onDiskData = FileOpener::openBlankStowage();
    document_t* document = onDiskData.document;
    intropage_t* introPage = new intropage_t();

    QLineEdit* nameLineEdit = introPage->findChild<QLineEdit*>();
    nameLineEdit->setText("_NON_EXISTING_EDIFACT_FILE_");

    bool result = introPage->validatePage();
    QCOMPARE(result, true);
    QCOMPARE(introPage->get_warnings().count(), 0);
    QCOMPARE(introPage->get_errors().count(), 1);
    QCOMPARE(introPage->get_errors().at(0)->m_text, QString("EDIFACT file '_NON_EXISTING_EDIFACT_FILE_' does not exist"));
    QCOMPARE(introPage->get_errors().at(0)->m_detailed_text, QString("File not found"));

    delete introPage;
    delete document;
}

void TestImportEdifactWizard::testEmptyFile() {
    OnDiskData onDiskData = FileOpener::openBlankStowage();
    document_t* document = onDiskData.document;
    intropage_t* introPage = new intropage_t();

    QLineEdit* nameLineEdit = introPage->findChild<QLineEdit*>();
    nameLineEdit->setText(QFINDTESTDATA("data/empty.edi"));

    bool result = introPage->validatePage();
    QCOMPARE(result, true);
    QCOMPARE(introPage->get_warnings().count(), 0);
    QCOMPARE(introPage->get_errors().count(), 1);
    QCOMPARE(introPage->get_errors().at(0)->m_text, QString("Not an EDI file"));
    QCOMPARE(introPage->get_errors().at(0)->m_detailed_text,
                QString("The file '%1' does not appear to be an EDI file. It doesn't contain any EDI segments.")
                    .arg(QFINDTESTDATA("data/empty.edi")));

    delete introPage;
    delete document;
}

void TestImportEdifactWizard::testOneSegmentFile() {
    OnDiskData onDiskData = FileOpener::openBlankStowage();
    document_t* document = onDiskData.document;
    intropage_t* introPage = new intropage_t();

    QLineEdit* nameLineEdit = introPage->findChild<QLineEdit*>();
    nameLineEdit->setText(QFINDTESTDATA("data/one-segment.edi"));

    bool result = introPage->validatePage();
    QCOMPARE(result, true);
    QCOMPARE(introPage->get_warnings().count(), 0);
    QCOMPARE(introPage->get_errors().count(), 1);
    QCOMPARE(introPage->get_errors().at(0)->m_text, QString("Bad EDI file"));
    QCOMPARE(introPage->get_errors().at(0)->m_detailed_text,
                QString("The file '%1' has only a single segment.").arg(QFINDTESTDATA("data/one-segment.edi")));

    delete introPage;
    delete document;
}

void TestImportEdifactWizard::testBaplieTwoSegmentFile() {
    OnDiskData onDiskData = FileOpener::openBlankStowage();
    document_t* document = onDiskData.document;
    intropage_t* introPage = new intropage_t();

    QLineEdit* nameLineEdit = introPage->findChild<QLineEdit*>();
    nameLineEdit->setText(QFINDTESTDATA("data/baplie-two-segments.edi"));

    bool result = introPage->validatePage();
    QCOMPARE(result, true);
    QCOMPARE(introPage->get_warnings().count(), 0);
    QCOMPARE(introPage->get_errors().count(), 2);
    QString expectedText = QString("BAPLIE file '%1' failed to parse").arg(QFINDTESTDATA("data/baplie-two-segments.edi"));
    QCOMPARE(introPage->get_errors().at(0)->m_text, expectedText);
    QCOMPARE(introPage->get_errors().at(0)->m_detailed_text, QString("No DTM segment found in group 0"));
    QCOMPARE(introPage->get_errors().at(1)->m_text, expectedText);
    QCOMPARE(introPage->get_errors().at(1)->m_detailed_text, QString("No group 1 found in group 0"));

    delete introPage;
    delete document;
}

void TestImportEdifactWizard::testCoprarTwoSegmentFile() {
    OnDiskData onDiskData = FileOpener::openBlankStowage();
    document_t* document = onDiskData.document;
    pool_t* pool = new pool_t(document->containers());
    schedule_view_t* scheduleView = new schedule_view_t();
    scheduleView->set_document(document, pool);
    scheduleView->select(document->schedule()->at(0));
    import_edifact_wizard_t* wizard = new import_edifact_wizard_t(document, pool, scheduleView);
    intropage_t* introPage = new intropage_t(wizard);

    QLineEdit* nameLineEdit = introPage->findChild<QLineEdit*>();
    nameLineEdit->setText(QFINDTESTDATA("data/coprar-two-segments.edi"));

    bool result = introPage->validatePage();
    QCOMPARE(result, true);
    QCOMPARE(introPage->get_warnings().count(), 0);
    QCOMPARE(introPage->get_errors().count(), 3);
    QString expectedText = QString("COPRAR 2.0 file '%1' failed to parse").arg(QFINDTESTDATA("data/coprar-two-segments.edi"));
    QCOMPARE(introPage->get_errors().at(0)->m_text, expectedText);
    QCOMPARE(introPage->get_errors().at(0)->m_detailed_text, QString("Expected exactly one BGM segment, got: 0"));
    QCOMPARE(introPage->get_errors().at(1)->m_text, expectedText);
    QCOMPARE(introPage->get_errors().at(1)->m_detailed_text, QString("Expected exactly one group 2 starting with TDT, got: 0"));
    QCOMPARE(introPage->get_errors().at(2)->m_text, expectedText);
    QCOMPARE(introPage->get_errors().at(2)->m_detailed_text, QString("Expected load port 'Befor', got: ''"));

    delete wizard;
    delete scheduleView;
    delete pool;
    delete document;
}

void TestImportEdifactWizard::testCoarriTwoSegmentFile() {
    OnDiskData onDiskData = FileOpener::openBlankStowage();
    document_t* document = onDiskData.document;
    pool_t* pool = new pool_t(document->containers());
    schedule_view_t* scheduleView = new schedule_view_t();
    scheduleView->set_document(document, pool);
    scheduleView->select(document->schedule()->at(0));
    import_edifact_wizard_t* wizard = new import_edifact_wizard_t(document, pool, scheduleView);
    intropage_t* introPage = new intropage_t(wizard);

    QLineEdit* nameLineEdit = introPage->findChild<QLineEdit*>();
    nameLineEdit->setText(QFINDTESTDATA("data/coarri-two-segments.edi"));

    bool result = introPage->validatePage();
    QCOMPARE(result, true);
    QCOMPARE(introPage->get_warnings().count(), 0);
    QCOMPARE(introPage->get_errors().count(), 1);
    QString expectedText = QString("EDIFACT file '%1' is not valid").arg(QFINDTESTDATA("data/coarri-two-segments.edi"));
    QCOMPARE(introPage->get_errors().at(0)->m_text, expectedText);
    QCOMPARE(introPage->get_errors().at(0)->m_detailed_text,
                QString("Error: Parse error while parsing %1 occurred: Empty possible interval for unmapped port"
                        "  voyage  between After and Befor\n").arg(QFINDTESTDATA("data/coarri-two-segments.edi")));

    delete wizard;
    delete scheduleView;
    delete pool;
    delete document;
}

void TestImportEdifactWizard::testTanstaTwoSegmentFile() {
    OnDiskData onDiskData = FileOpener::openBlankStowage();
    document_t* document = onDiskData.document;
    pool_t* pool = new pool_t(document->containers());
    schedule_view_t* scheduleView = new schedule_view_t();
    scheduleView->set_document(document, pool);
    scheduleView->select(document->schedule()->at(0));
    import_edifact_wizard_t* wizard = new import_edifact_wizard_t(document, pool, scheduleView);
    intropage_t* introPage = new intropage_t(wizard);

    QLineEdit* nameLineEdit = introPage->findChild<QLineEdit*>();
    nameLineEdit->setText(QFINDTESTDATA("data/tansta-two-segments.edi"));

    bool result = introPage->validatePage();
    QCOMPARE(result, true);
    QCOMPARE(introPage->get_warnings().count(), 1);
    QString expectedText = QString("EDIFACT TANSTA file '%1' contains no tank conditions").arg(QFINDTESTDATA("data/tansta-two-segments.edi"));
    QCOMPARE(introPage->get_warnings().at(0)->m_text, expectedText);
    QCOMPARE(introPage->get_warnings().at(0)->m_detailed_text, expectedText);
    QCOMPARE(introPage->get_errors().count(), 0);

    delete wizard;
    delete scheduleView;
    delete pool;
    delete document;
}

// This is a strange workaround linking problems with CMake
#include "vesselscene.h"
void cmakeLinkingWorkaround() {
    new vessel_scene_t(0,0);
}

#include "testimportedifactwizard.moc"
