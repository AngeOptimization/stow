#ifndef IMPORT_EDIFACT_WIZARD_H
#define IMPORT_EDIFACT_WIZARD_H

#include <QWizard>

class QTextEdit;
class QLabel;
class QLineEdit;
class document_t;
class pool_t;
class schedule_view_t;
class edifact_container_reader_t;

struct message_t {
  public:
  enum type_t {
    WARNING,
    ERROR_,
    INFO,
    DEFAULT
  };
  type_t m_type;
  QString m_text;
  QString m_detailed_text;

  message_t(type_t type, QString text, QString detailed_text) : m_type(type), m_text(text), m_detailed_text(detailed_text) {
  };
  message_t() : m_type(message_t::DEFAULT), m_text(), m_detailed_text()  {
  }
  bool isEmpty() {
    return m_type == message_t::DEFAULT && m_text.isEmpty() && m_detailed_text.isEmpty();
  }
};

class import_edifact_wizard_t;

class intropage_t : public QWizardPage
{
    Q_OBJECT

  public:
    intropage_t( QWidget* parent = 0);
    bool resolvable_problems() const { return m_resolvable_problems;}
    edifact_container_reader_t* reader() { return m_reader; }
    QList<message_t*> get_warnings() const { return m_parse_warnings; };
    QList<message_t*> get_errors() const { return m_parse_errors; };
    QString get_summary();
    virtual bool isComplete() const;
    virtual bool validatePage();
    virtual int nextId() const;

  private Q_SLOTS:
    void get_filename_dialog();
    void set_reparse();

  private:
    QLineEdit* m_namelineedit;
    edifact_container_reader_t* m_reader;
    import_edifact_wizard_t* m_wizard;
    QList<message_t*> m_parse_warnings;
    QList<message_t*> m_parse_errors;
    bool m_resolvable_problems;
    bool m_need_reparse;
    bool m_parsed;
};

class parse_error_page_t : public QWizardPage
{
    Q_OBJECT

  public:
    parse_error_page_t(QWidget* parent = 0);
    virtual void initializePage();
    virtual int nextId() const { return -1; };
  private:
    import_edifact_wizard_t* m_wizard;
    QLabel* m_label;
    QTextEdit* m_textedit;
};

class warning_page_t : public QWizardPage
{
    Q_OBJECT

  public:
    warning_page_t(QWidget* parent = 0);
    virtual void initializePage();
    virtual int nextId() const { return -1; };

  private:
    import_edifact_wizard_t* m_wizard;
    QTextEdit* m_textedit;
    QLabel* m_unplacedlabel;
    QLabel* m_partiallyplacedlabel;
};

class sucess_page_t : public QWizardPage
{
    Q_OBJECT

  public:
    sucess_page_t(QWidget* parent = 0);
    virtual void initializePage();
    virtual int nextId() const { return -1; };
  private:
    import_edifact_wizard_t* m_wizard;
    QTextEdit* m_textedit;
};

class import_edifact_wizard_t : public QWizard
{
  Q_OBJECT

  public:

    enum {
      INTRO,
      PARSE_ERROR,
      STOW_WARNING,
      SUCCESS
    };

    import_edifact_wizard_t(document_t* document, pool_t* selection, schedule_view_t* callview, QWidget* parent = 0, Qt::WindowFlags flags = 0);
    document_t* document() const { return m_document;}
    pool_t* pool() const { return m_selection;}
    schedule_view_t* call_view() const { return m_call_view;}

    intropage_t* intropage() const { return m_intropage;}


  private:
    document_t* m_document;
    pool_t* m_selection;
    schedule_view_t* m_call_view;

    intropage_t* m_intropage;
    parse_error_page_t* m_errorpage;
    warning_page_t* m_warningpage;

};


#endif // IMPORT_EDIFACT_WIZARD_H
