#ifndef COLORTOOLS_H
#define COLORTOOLS_H

#include <QColor>

/**
 * @param background color of some object
 * @return true if background color should be light, false if it should be dark
 */
inline bool backgroundRequiresColorInversion(const QColor& color) {
    int brightness = (color.red() * 299 + color.green() * 587 + color.blue() * 114) / 1000; // luminance in 0-255
    return brightness < 128; // luminance < 50%
}

/**
 * @param background color of some object
 * @return a black or white QColor for foreground depending on how dark the background is.
 */
inline QColor readableTextColor(const QColor& color) {
    return backgroundRequiresColorInversion(color) ? QColor(Qt::white) : QColor(Qt::black);
}

#endif
