#ifndef SCHEDULE_VIEW_H
#define SCHEDULE_VIEW_H
#include <QTableView>

class pool_t;
class QUndoStack;
class document_t;
class schedule_view_private_t;
namespace ange {
namespace schedule {
class Call;
}
}
/**
 * A view showing the calls in the schedule
 */
class schedule_view_t : public QTableView {
  Q_OBJECT
  public:
    schedule_view_t(QWidget *parent=0);
    ~schedule_view_t();
  private:
    virtual void contextMenuEvent(QContextMenuEvent* event);
  Q_SIGNALS:
    void call_selected(const ange::schedule::Call*);
    void call_focused(const ange::schedule::Call*);
  public Q_SLOTS:
    void set_document(document_t* document, pool_t* selected_pool);
    /**
     * @return the call currently selected
     */
    const ange::schedule::Call* current_call() {
      return m_current_call;
    }

    /**
     * Select call by index
     */
    void select(int);

    /**
     * Select call
     */
    void select(const ange::schedule::Call* call);

  private Q_SLOTS:
    void create_call(int index);

    /**
     * Listen to own selection changes, and update m_current_call appropriately.
     */
    void slot_selectionChanged(QItemSelection selected,QItemSelection deselected);

    /**
     * Listen to row removal and adjust current_call() accordingly
     */
    void slot_rows_about_to_be_removed(const QModelIndex& parent, int start, int end);

    /**
     * Remove call at index in schedule if legal, or show an error dialog if not
     */
    void remove_call(ange::schedule::Call* call);

    /**
     * Validate if call can be removed;  display error message if invalid
     */
    bool validate_call_can_be_removed(ange::schedule::Call* call) const;

  private:
    const ange::schedule::Call* m_current_call;
    document_t* m_document;
    pool_t* m_selected_pool;
};

#endif // SCHEDULE_VIEW_H
