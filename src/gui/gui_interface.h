#ifndef GUI_INTERFACE_H
#define GUI_INTERFACE_H

#include "stowplugininterface/igui.h"

class AbstractRecapView;
class MainWindow;

/**
 *  Implementation of igui_t, to be passed to plugins to enable plugins to install their own gui elements
 **/
class gui_interface_t : public ange::angelstow::IGui {
  public:
    explicit gui_interface_t(MainWindow* mainwindow);

    virtual void installMenuAction(Menus menu, QAction* action);
    virtual void installViewPanel(QWidget* view_panel, QString title);
    virtual const ange::schedule::Call* currentCall() const;
    virtual QList<const ange::containers::Container*> selectedContainers() const;
  private:
    MainWindow* m_main_window;

};

#endif // GUI_INTERFACE_H
