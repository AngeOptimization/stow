#ifndef RECAP_VIEW_H
#define RECAP_VIEW_H

#include <QTableView>
#include <QPointer>

#include <qdatacube/datacubeview.h>
#include <qdatacube/abstractaggregator.h>
#include <qdatacube/abstractfilter.h>

class RecapManagerInterface;
class ContainerSelectionBundleHelper;
class QDockWidget;
class pool_t;
class document_t;
namespace qdatacube {
class Datacube;
class AbstractFilter;
class AbstractAggregator;
}
namespace ange {
namespace schedule {
class Schedule;
class Call;
}
}

class RecapView : public qdatacube::DatacubeView {
    typedef qdatacube::DatacubeView super;
    Q_OBJECT
public:
    /**
     *
     */
    RecapView(QString name, QWidget* parent = 0);

    /**
     * Setup recap with some standard filters and categories
     */
    void setupStandardRecap();

    /**
     * @param document document to track
     * @param selection_pool selection pool for recap
     */
    void setDocument(document_t* document, pool_t* selection_pool, QItemSelectionModel* synchronized_selection_model);

    void setCurrentCall(const ange::schedule::Call* call);

    /**
     * Save configuration as QVariant
     */
    QVariant saveState() const;

    /**
     * Restore configuration
     */
    void restoreState(const QVariant saved_state);

    /**
     * Start rename of the recap (dockwidget is the correspinding dockwidget to show titlevbar widget)
     **/
    void rename(QDockWidget* dockwidget);

    /**
     *  Split recap by filter name
     * @return true if successful, in case anyone needs to know
     */
    bool split(Qt::Orientation orientation, int section, QString filter);

    /**
     * Adds a custom filter to the recap
     * \param filter
     */
    void addCustomFilter(qdatacube::AbstractFilter::Ptr filter);

    /**
     * Filter using filter, and category
     * @return true if successful, in case anyone needs to know
     */
    bool filter(QString filter, QString category_name);

    /**
     * Collapse the filter.
     */
    void collapse(Qt::Orientation orientation, int index);

    /**
     * Remove filter from the unused filter stack
     * @override
     */
    virtual bool disableFilter(QString filter);
    /**
     * Sets the bundling helper to bundlehelper. Does not take ownership
     */
    void setBundleHelper(ContainerSelectionBundleHelper* bundlehelper);

    /**
     * Sets the recap manager for creating new recaps
     */
    void setRecapManager(RecapManagerInterface* recapManager);

Q_SIGNALS:
    void current_call_changed(const ange::schedule::Call* call);
    void nameChanged();

public Q_SLOTS:
    void setName(QString name);
    void displayCellContextMenu(QPoint pos, int row, int column);
  private Q_SLOTS:
    void displayHorizontalHeaderContextMenu(QPoint pos, int headerno);
    void displayVerticalHeaderContextMenu(QPoint pos, int headerno);
    void displayCornerContextMenu(QPoint);
    void headerContextMenu(Qt::Orientation orientation, int headerno, bool split_after);
    void generalContextMenu(QMenu& contextmenu);
    virtual void contextMenuEvent(QContextMenuEvent* event);
  protected:
    virtual bool eventFilter(QObject* object, QEvent* event);
    virtual void mouseDoubleClickEvent(QMouseEvent* event);
    virtual void wheelEvent(QWheelEvent* event);
  private:
    typedef QList<qdatacube::AbstractAggregator::Ptr > aggregator_list_t;
    aggregator_list_t m_unused_aggregators;
    typedef QList<qdatacube::AbstractFilter::Ptr > filter_list_t;
    filter_list_t m_unused_filters;
    const ange::schedule::Call* m_current_call;
    document_t* m_document;
    pool_t* m_selection_pool;
    QString m_name;
    bool m_has_custom_filter;
    void split(Qt::Orientation orientation, int section, int unused_filter_index);
    int findUnusedAggregatorByName(QString name) const;
    int findUnusedFilterByName(QString name) const;
    QAction* createFilterAction(qdatacube::AbstractAggregator::Ptr filter, QMenu* parent);
    QPointer<ContainerSelectionBundleHelper> m_bundlehelper;
    RecapManagerInterface* m_recapManager;
    /**
     * display the recap as a list. If @param detachedCube is set, the list will
     * live independently of the recap. Note: The list widget takes ownership of the datacube
     */
    void display_list(qdatacube::Datacube* detachedCube);
    /**
     * Clones all filters and aggregators from \param oldCube and adds them to \param newRecap
     */
    static void cloneFilterAndAggregatorsFromDataCube(qdatacube::Datacube* oldCube, RecapView* newRecap);
    /**
     * Creates a detached datacube with the same filters. Ownership is the responsibility of the caller.
     */
    qdatacube::Datacube* detachedFilteredCube() const;
  private Q_SLOTS:
    void displaySelectionAsList();
    void displayRecapAsList();
    void copyRecapToClipboard();
    void display_selection_as_recap();
};

#endif // RECAP_VIEW_H
