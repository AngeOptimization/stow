#ifndef OVERRIDE_CURSOR_HANDLER_H
#define OVERRIDE_CURSOR_HANDLER_H

#include <QApplication>

/**
 * \brief A simple class to handle overriding of cursors
 *
 * When creating a \ref override_cursor_handler_t, the override cursor
 * gets automatically set to teh given cursor, and the previous cursor
 * gets restored when \ref override_cursor_handler_t is deleted / goes out of scope
 */

class override_cursor_handler_t {
  public:
    /**
     * Constructor. Creates a \ref override_cursor_handler_t object and sets \param cursor
     * as override cursor on application \param app
     *
     * \param cursor the override cursor to set
     * \param app the application to set the override cursor to
     */
    override_cursor_handler_t(QApplication* app, const QCursor& cursor) : m_app(app) {
      app->setOverrideCursor(cursor);
    }
    /**
     * dtor
     *
     * restores the previous cursor
     */
    ~override_cursor_handler_t() {
      m_app->restoreOverrideCursor();
    }
  private:
    void* operator new(size_t );
    QApplication* m_app;
};

#endif // OVERRIDE_CURSOR_HANDLER_H
