/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010
 Copyright: See COPYING file that comes with this distribution
*/

#include "svg_cache.h"
#include <QPainter>
#include <QSvgRenderer>

svg_cache_t::svg_cache_t(QString item)
  : m_renderer(new QSvgRenderer(item)),
    m_caches(CACHE_SIZE),
    m_oldest_entry(0)
{}

svg_cache_t::cache_t::cache_t() :
    m_m11(0.0),
    m_m22(0.0)
{}

svg_cache_t* svg_cache_t::instance(const QString& item, const bool invert_render_color) {
  svg_cache_t*& rv = m_instances[SvgCacheIndex(item, invert_render_color)];
  if (!rv) {
    rv  = new svg_cache_t(item);
  }
  return rv;
}

void svg_cache_t::render(QPainter* painter, const QRectF& bounds, const bool invert_render_color) {
    QTransform t(painter->transform());
#if 1
// Enable cache. Change to #if 0 to disable cache.
    QPixmap cached_svg;
    for (int i=0; i<m_caches.size(); ++i) {
        const cache_t& cache = m_caches[i];
        if (!cache.m_cache.isNull() && bounds.size() == cache.m_size
            && t.m11() == cache.m_m11 && t.m22() == cache.m_m22) {
            cached_svg = cache.m_cache;
            break;
        }
    }
    if (cached_svg.isNull()) {
        cache_t& cache = m_caches[m_oldest_entry];
        m_oldest_entry = (m_oldest_entry+1)%m_caches.size();
        cache.m_m11 = t.m11();
        cache.m_m22 = t.m22();
        cache.m_size = bounds.size();
        QImage image = QImage(bounds.width()*cache.m_m11+2, bounds.height()*cache.m_m22+2, QImage::Format_ARGB32);
        image.fill(Qt::transparent);
        QPainter painter(&image);
        painter.setBackgroundMode(Qt::TransparentMode);
        m_renderer->render(&painter, QRectF(QPointF(), QSizeF(bounds.width()*cache.m_m11, bounds.height()*cache.m_m22)));
        if(invert_render_color) {
            image.invertPixels();
        }
        cache.m_cache = QPixmap::fromImage(image);
        cached_svg = cache.m_cache;
    }
    painter->resetTransform();
    painter->drawPixmap(t.map(bounds.topLeft()), cached_svg);
    painter->setTransform(t);
#else
    //Note: no inversion of icon colors in the uncached version
    m_renderer->render(painter, bounds);
#endif
}

svg_cache_t::~svg_cache_t() {
  delete m_renderer;
}

QHash<SvgCacheIndex, svg_cache_t*> svg_cache_t::m_instances;

uint qHash(SvgCacheIndex cacheIndex) {
    return qHash(cacheIndex.m_isInverted);
}
