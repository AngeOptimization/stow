/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010

 Copyright: See COPYING file that comes with this distribution

*/

#include "painter_utils.h"
#include <QPainter>
#include <cmath>
#include "svg_cache.h"


namespace ange {
namespace painter_utils {

namespace  {
  void draw_text_detail(QPainter* painter, const QSizeF& scale, const QRectF bounds, const QString& text, const QTransform& current_transform, const QTextOption& options) {
    qreal dx = bounds.left() * ( 1 - scale.width());
    qreal dy = bounds.top() * ( 1 - scale.height());
    const QTransform text_transform(scale.width(),0.0, 0.0, scale.height(), dx, dy);
    const QTransform inverse_text(1/scale.width(), 0.0, 0.0, 1/scale.height(), -dx/scale.width(), -dy/scale.height());
    painter->setTransform(text_transform,true);
    painter->drawText(inverse_text.mapRect(bounds), text, options);
    painter->setTransform(current_transform, false);
  }
}


void draw_text_in_rect(QPainter* painter, const QRectF& bounds, const QString& text, const QRect& text_bounds, QTextOption text_options) {
  QTransform current_transform = painter->transform();
  const QSizeF max_scale(bounds.width()/text_bounds.width(), bounds.height()/text_bounds.height());
  qreal aspect = std::fabs(current_transform.m11() / current_transform.m22());
  bool scale_to_width = max_scale.width()* aspect > max_scale.height();
  QSizeF sign((current_transform.m11()>0.0?1.0:-1.0), (current_transform.m22()>0.0?1.0:-1.0));
  QSizeF scale = scale_to_width ? QSizeF(max_scale.height()/aspect*sign.width(), max_scale.height()*sign.height())
  : QSizeF(max_scale.width()*sign.width(), max_scale.width()*aspect*sign.height());
  draw_text_detail(painter, scale, bounds, text, current_transform, text_options);
}

void draw_text_in_rect(QPainter* painter, const QRectF& bounds, const QString& text, QTextOption text_options) {
  draw_text_in_rect(painter, bounds, text, painter->fontMetrics().boundingRect(text), text_options);
}

void draw_text_in_rect(QPainter* painter, const QRectF& bounds, qreal scale, const QString& text, const QRect& text_bounds, QTextOption text_options) {
  QTransform current_transform = painter->transform();
  const QSizeF max_scale(bounds.width()/text_bounds.width(), bounds.height()/text_bounds.height());
  const QSizeF corrected_scale(scale/current_transform.m11(), scale/current_transform.m22());
  if (std::fabs(corrected_scale.width()) > std::fabs(max_scale.width()) ||
    std::fabs(corrected_scale.height()) > std::fabs(max_scale.height())) {
    return; // Not enough space to draw at specified scale
  }
 draw_text_detail(painter, corrected_scale, bounds, text, current_transform, text_options);

}

void drawTextInRect(QPainter* painter, const QRectF& bounds, const QString& text, bool invertRenderColor,
                    const QRect& textBounds, QTextOption textOptions) {
    painter->save();
    painter->setPen(invertRenderColor ? Qt::white : Qt::black);
    draw_text_in_rect(painter, bounds, text, textBounds, textOptions);
    painter->restore();
}


void draw_text_in_rect(QPainter* painter, const QRectF& bounds, qreal scale, const QString& text, QTextOption text_options) {
  draw_text_in_rect(painter,bounds,scale, text, painter->fontMetrics().boundingRect(text), text_options);
}

void draw_svg_in_rect(const QString& name, QPainter* painter, const QRectF& bounding_rect, const bool invert_render_color) {
  QTransform current_transform = painter->transform();
  QSizeF scale((current_transform.m11()>0.0?1.0:-1.0), (current_transform.m22()>0.0?1.0:-1.0));
  qreal dx = bounding_rect.left() * ( 1 - scale.width());
  qreal dy = bounding_rect.top() * ( 1 - scale.height());
  const QTransform text_transform(scale.width(),0.0, 0.0, scale.height(), dx, dy);
  const QTransform inverse_text(1/scale.width(), 0.0, 0.0, 1/scale.height(), -dx/scale.width(), -dy/scale.height());
  painter->setTransform(text_transform,true);

  // When drawing non-antialiased
  const QTransform viewport_transform = painter->combinedTransform();
  const QRectF bounds_unaligned = inverse_text.mapRect(bounding_rect);
  const QRectF mapped_bounds_unaligned(viewport_transform.mapRect(bounds_unaligned));
  const QRectF mapped_bounds_aligned = QRectF(std::ceil(mapped_bounds_unaligned.left()), std::ceil(mapped_bounds_unaligned.top()), std::ceil(mapped_bounds_unaligned.width()), std::ceil(mapped_bounds_unaligned.height()));
  QRectF bounds_aligned = viewport_transform.inverted().mapRect(mapped_bounds_aligned);
  svg_cache_t* svg = svg_cache_t::instance(name, invert_render_color);
  svg->render(painter,bounds_aligned, invert_render_color);
  painter->setTransform(current_transform, false);
}



}
}