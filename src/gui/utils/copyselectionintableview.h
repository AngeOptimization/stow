#ifndef COPYSELECTIONINTABLEVIEW_H
#define COPYSELECTIONINTABLEVIEW_H

#include <QObject>
#include <QPoint>


/**
 * Static class to help copy selections from a table view into clipboard
 * as tab separated values
 */
class CopySelectionInTableView : public QObject {
    Q_OBJECT
    public:
        static CopySelectionInTableView* self();
    public Q_SLOTS:
        /**
         * handle right click event.
         * Note that it needs to be used as a slot, since it
         * relies on sender() to produce a correct result
         */
        void handleTableViewCopyAction();
    private:
        CopySelectionInTableView();


};

#endif // COPYSELECTIONINTABLEVIEW_H
