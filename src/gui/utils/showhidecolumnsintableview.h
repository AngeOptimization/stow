/*
 *
 */

#ifndef SHOWHIDECOLUMNSINTABLEVIEW_H
#define SHOWHIDECOLUMNSINTABLEVIEW_H

#include <QObject>
#include <QPoint>

/**
 * static helper class to help show/hide columns in a table view
 * based upon the columns in the model.
 */

class ShowHideColumnsInTableView : public QObject {
    Q_OBJECT
    public:
        static ShowHideColumnsInTableView* self();

    public Q_SLOTS:
        /**
         * Note: must be called in a signal/slot connection.
         * Internally it relies on sender();
         */
        void handleContextMenuForTableViewHeaders(QPoint /*pos*/);
    private:
        ShowHideColumnsInTableView();
};

#endif // SHOWHIDECOLUMNSINTABLEVIEW_H
