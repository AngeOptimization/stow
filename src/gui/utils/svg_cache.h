/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010

 Copyright: See COPYING file that comes with this distribution

*/

#ifndef SVG_CACHE_H
#define SVG_CACHE_H
#include <QtGlobal>
#include <QHash>
#include <QSize>
#include <QVarLengthArray>
#include <QPixmap>

class QSvgRenderer;
class QRectF;
class QPainter;
class QString;

struct SvgCacheIndex{
    const QString m_name;
    const bool m_isInverted;
    SvgCacheIndex(const QString& name, const bool isInverted) : m_name(name), m_isInverted(isInverted) {};
    bool operator==(const SvgCacheIndex& other) const {
        return m_name == other.m_name && m_isInverted == other.m_isInverted;
    }
};

class svg_cache_t {
  public:
    /**
     * Get instance for item
     * @item path to svg (typically a resource path)
     */
    static svg_cache_t* instance(const QString& item, const bool isInverted);

    /**
     * Render svg into bounds. (Should be equivalent of using a QSvgRenderer::render(painter, bounds))
     */
    void render(QPainter* painter, const QRectF& bounds, const bool invert_render_color);
  private:
    svg_cache_t(QString item);
    ~svg_cache_t();
    QSvgRenderer* m_renderer;
    struct cache_t {
      cache_t();
      QPixmap m_cache;
      qreal m_m11;
      qreal m_m22;
      QSizeF m_size;
    };
    static const int CACHE_SIZE = 2;
    QVarLengthArray<cache_t, CACHE_SIZE> m_caches; // 2, because HC/DC.
    int m_oldest_entry;
    static QHash<SvgCacheIndex, svg_cache_t*> m_instances;
};

uint qHash(SvgCacheIndex cacheIndex);

#endif // SVG_CACHE_H
