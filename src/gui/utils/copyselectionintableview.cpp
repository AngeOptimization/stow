#include "copyselectionintableview.h"

#include <QAbstractItemView>
#include <QApplication>
#include <QTableView>
#include <QMenu>
#include <QClipboard>

CopySelectionInTableView::CopySelectionInTableView() {
    setObjectName("CopySelectionInTableView");
}

CopySelectionInTableView* CopySelectionInTableView::self() {
    static CopySelectionInTableView s;
    return &s;
}

struct AssignAtEnd {
    AssignAtEnd(QModelIndex* assignto, QModelIndex* value) : m_assignto(assignto), m_value(value) {

    }
    ~AssignAtEnd() {
        *m_assignto = *m_value;
    }
    private:
        QModelIndex* m_assignto;
        QModelIndex* m_value;
};

void CopySelectionInTableView::handleTableViewCopyAction() {
    QObject* object = sender();
    if(!object) {
        return;
    }
    QAction* action = qobject_cast<QAction*>(object);
    if(!action) {
        return;
    }
    QAbstractItemView* itemView = qobject_cast<QAbstractItemView*>(action->parent());
    if(!itemView) {
        return;
    }
    QAbstractItemModel * model = itemView->model();
    QModelIndexList indices = itemView->selectionModel()->selectedIndexes();
    if(indices.isEmpty()) {
        return;
    }

    QTableView* tableView = qobject_cast<QTableView*>(itemView);
    qSort(indices.begin(), indices.end());
    QString selected_text;

    QSet<int> columns;
    Q_FOREACH(const QModelIndex& index, indices) {
        columns << index.column();
    }
    QList<int> sortedColumns = columns.toList();
    qSort(sortedColumns);

    Q_FOREACH(int column, sortedColumns) {
        if(tableView->isColumnHidden(column)) {
            continue;
        }
        if(!selected_text.isEmpty()) {
            selected_text.append("\t");
        }
        selected_text.append(tableView->model()->headerData(column, Qt::Horizontal).toString());
    }
    selected_text.append("\n");

    QModelIndex previous = indices.first();
    indices.removeFirst();
    Q_FOREACH(QModelIndex current, indices) {
        AssignAtEnd aae(&previous, &current);
        if(tableView) {
            if(tableView->isColumnHidden(previous.column())) {
                continue;
            }
        }
        QVariant data = model->data(previous);
        QString text = data.toString();
        selected_text.append(text);
        if (current.row() != previous.row()) {
            selected_text.append(QLatin1Char('\n'));
        }
        else {
            selected_text.append('\t');
        }
    }
    selected_text.append(model->data(previous).toString());
    selected_text.append(QLatin1Char('\n'));
    QApplication::clipboard()->setText(selected_text);

}






#include "copyselectionintableview.moc"
