#ifndef TABLEVIEW_UTILS_H
#define TABLEVIEW_UTILS_H
#include <QObject>
#include <QTableView>
#include <QHeaderView>
#include <QAction>
#include "showhidecolumnsintableview.h"
#include "copyselectionintableview.h"

namespace TableViewUtils {

    inline void setupShowHideColumns(QTableView* view) {
        view->horizontalHeader()->setContextMenuPolicy(Qt::CustomContextMenu);
        QObject::connect(view->horizontalHeader(),&QTableView::customContextMenuRequested,
        ShowHideColumnsInTableView::self(), &ShowHideColumnsInTableView::handleContextMenuForTableViewHeaders);
    }

    inline void setupCopySelection (QTableView* view) {
        view->setContextMenuPolicy(Qt::ActionsContextMenu);
        QAction* action = new QAction("Copy selection",view);
        action->setShortcuts(QKeySequence::Copy);
        action->setShortcutContext(Qt::WidgetShortcut);
        view->addAction(action);
        QObject::connect(action, &QAction::triggered, CopySelectionInTableView::self(), &CopySelectionInTableView::handleTableViewCopyAction);
    }

    inline  void setupShowHideColumnsAndCopySelection(QTableView* view) {
        setupShowHideColumns(view);
        setupCopySelection(view);
    }
}

#endif // TABLEVIEW_UTILS_H
