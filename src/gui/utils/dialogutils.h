#include <QDialog>

/**
 * Utility functions for dealing with dialogs.
 */

namespace DialogUtils {
    /**
     * \param dialog
     * Suggest the window manager that this dialog should have a maximize button
     */
    inline void makeDialogMaximizable(QDialog* dialog) {
        dialog->setWindowFlags(dialog->windowFlags() | Qt::WindowMaximizeButtonHint);
    }
};
