#include "showhidecolumnsintableview.h"

#include <QHeaderView>
#include <QTableView>
#include <QAction>
#include <QMenu>

ShowHideColumnsInTableView::ShowHideColumnsInTableView() {
    setObjectName("ShowHideColumnsInTableView");
}

ShowHideColumnsInTableView* ShowHideColumnsInTableView::self() {
    static ShowHideColumnsInTableView s;
    return &s;
}

void ShowHideColumnsInTableView::handleContextMenuForTableViewHeaders(QPoint ) {
    QObject* object = sender();
    QHeaderView* header = qobject_cast<QHeaderView*>(object);
    if(!header) {
        return;
    }
    QTableView* parentview = qobject_cast<QTableView*>(header->parent());
    if(!parentview) {
        return;
    }
    QMenu menu;
    for(int i = 0 ; i < parentview->model()->columnCount();i++) {
        QString columnname = parentview->model()->headerData(i,header->orientation()).toString();
        if(parentview->isColumnHidden(i)) {
            QAction* action = menu.addAction(QString("Show %1").arg(columnname));
            action->setData(i);
        } else {
            QAction* action = menu.addAction(QString("Hide %1").arg(columnname));
            action->setData(i);
        }
    }
    QAction* selected = menu.exec(QCursor::pos());
    if(selected) {
        int column = selected->data().toInt();
        parentview->setColumnHidden(column,!parentview->isColumnHidden(column));
    }
    return;
}

#include "showhidecolumnsintableview.moc"
