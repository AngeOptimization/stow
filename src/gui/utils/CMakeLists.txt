add_library(gui-utils STATIC
    copyselectionintableview.cpp
    painter_utils.cpp
    showhidecolumnsintableview.cpp
    svg_cache.cpp
)
target_link_libraries(gui-utils
    Qt5::Gui
    Qt5::Svg
)
