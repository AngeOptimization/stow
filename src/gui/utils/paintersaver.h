#ifndef PAINTERSAVER_H
#define PAINTERSAVER_H

#include <QPainter>

/**
 * Small RAII struct to help save/restore painter state
 * Calls painter->save in constructor and painter->restore in destructor
 */
struct PainterSaver {
public:
    PainterSaver(QPainter* painter) : m_painter(painter) {
        m_painter->save();
    }
    ~PainterSaver() {
        m_painter->restore();
    }
private:
    void* operator new(size_t);
    QPainter* m_painter;
};

#endif // PAINTERSAVER_H
