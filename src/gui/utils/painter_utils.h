/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010

 Copyright: See COPYING file that comes with this distribution

*/

#ifndef PAINTER_UTILS_H
#define PAINTER_UTILS_H
#include <QtGlobal>
#include <QTextOption>

class QRect;
class QString;
class QPainter;
class QSvgRenderer;
namespace ange {
namespace painter_utils {

/**
 * As draw_text_in_rect, but make the text small enough that it would fit if the boundingrect of the text is actually
 * textbounds. Useful for e.g. numbers, so that 1,10,100 are drawn the same size (by using e.g.
 * text_bounds=painter->fontMetrics()->boundingRect("100")
 */
void draw_text_in_rect(QPainter* painter,
                       const QRectF& bounds,
                       const QString& text,
                       const QRect& text_bounds,
                       QTextOption text_options = QTextOption(Qt::AlignCenter | Qt::AlignVCenter));

/**
 * As draw_text_in_rect, but use the supplied (total) scale. If text will not fit, leave it out.
 */
void draw_text_in_rect(QPainter* painter,
                       const QRectF& bounds,
                       qreal scale,
                       const QString& text,
                       const QRect& text_bounds,
                       QTextOption text_options = QTextOption(Qt::AlignCenter | Qt::AlignVCenter));

/**
 * As draw_text_in_rect, but use the supplied (total) scale. If text will not fit, leave it out.
 */
void draw_text_in_rect(QPainter* painter,
                       const QRectF& bounds,
                       qreal scale,
                       const QString& text,
                       QTextOption text_options = QTextOption(Qt::AlignCenter | Qt::AlignVCenter));

/**
 * Draw text inside bounds, with correct aspect ratio and the right way up and not mirrored.
 * It should work with the current transform = ( a,0,0,b, x,y ), with a,b != 0
 **/
void draw_text_in_rect(QPainter* painter,
                       const QRectF& bounds,
                       const QString& text,
                       QTextOption text_options = QTextOption(Qt::AlignCenter | Qt::AlignVCenter));

/**
 * draws the svg in the bounding rect with qsvgrenderer->render(), but
 * makes sure to make the svg be upside down.
 */
void draw_svg_in_rect(const QString& name, QPainter* painter, const QRectF& bounding_rect, const bool invert_render_color);

void drawTextInRect(QPainter* painter,
                       const QRectF& bounds,
                       const QString& text,
                       bool invertRenderColor,
                       const QRect& textBounds,
                       QTextOption textOptions = QTextOption(Qt::AlignCenter | Qt::AlignVCenter));

}
}

#endif // PAINTER_UTILS_H
