/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010

 Copyright: See COPYING file that comes with this distribution

*/

#ifndef SVG_GRAPHICS_ITEM_H
#define SVG_GRAPHICS_ITEM_H

#include <qgraphicsitem.h>
#include "gui/stow.h"

class QSvgRenderer;

/**
 * A possibly moveable svg item with a hotspot.
 */
class svg_graphics_item_t : public QObject, public QGraphicsItem {
  Q_OBJECT
  Q_INTERFACES(QGraphicsItem)
  public:
    svg_graphics_item_t(QString filename, const QRectF& bounds = QRectF(), QGraphicsItem* parent = 0);

    /**
     * @override
     */
    virtual void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget = 0);

    /**
     * @override
     */
    virtual QRectF boundingRect() const;

    /**
     * Sets the
     * @param bounds area which the item can be moved in, in parent coordinates.
     */
    void allow_move(QRectF bounds);

    /**
     * Set the hotspot within the item (in item coordinate system)
     */
    void set_hotspot(QPointF hotspot);
    enum {
      Type = UserType + Stow::upper_limit_item_t
    };

  Q_SIGNALS:
    QPointF item_position_changed(QGraphicsItem* item, QPointF position);
  protected:
    virtual int type() const {
      return Type;
    }
    virtual void mouseMoveEvent(QGraphicsSceneMouseEvent* event);
    virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent* event);
  private:
    QSvgRenderer* m_renderer;
    QRectF m_bounds;
    QRectF m_movement_bounds;
    QPointF m_hotspot;
};

#endif // SVG_GRAPHICS_ITEM_H
