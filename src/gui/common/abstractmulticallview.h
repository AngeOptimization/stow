#ifndef ABSTRACT_MULTI_CALL_VIEW_H
#define ABSTRACT_MULTI_CALL_VIEW_H

#include <QGraphicsView>
#include <QHash>

class AbstractVesselGraphicsItem;
class schedule_header_t;
namespace ange {
namespace schedule {
class Call;
}
}
class document_t;

class AbstractMultiCallView : public QGraphicsView {
    typedef QGraphicsView super;
    Q_OBJECT
protected:
    AbstractMultiCallView(QWidget* parent = 0);

public Q_SLOTS:
    virtual void set_document(document_t* document);
    void remove_call(ange::schedule::Call* call);
    void add_call(ange::schedule::Call* call);
    void set_current_call(const ange::schedule::Call* call);
    void hide_non_active_calls_changed(bool hidden);
    void wheelEvent(QWheelEvent* event);

protected:
    /**
     * @override
     */
    virtual void resizeEvent(QResizeEvent* event);

    /**
     * @return a new abstract_vessel_graph_item to be inserted. Return null to skip this call
     *
     */
    virtual AbstractVesselGraphicsItem* create_item_from_call(ange::schedule::Call* call) = 0;

    /**
     * Update the schedule/call headers with any changes
     */
    void update_headers();

    /**
     * called after add_call successfully has created a AbstractVesselGraphicsItem* and done the relayout
     * of items.
     */
    virtual void layout_updated();

    /**
     * called at the end of current call change. Default implementation does nothing.
     */
    virtual void change_current_call();

    const ange::schedule::Call* current_call() { return m_current_call; }

private Q_SLOTS:
    void removeMovingCall(int fromPosition, int toPosition);
    void insertMovingCall(int fromPosition, int toPosition);

protected:
    document_t* m_document;
    QGraphicsScene* m_scene;
    QHash<const ange::schedule::Call*, AbstractVesselGraphicsItem*> m_items;

private:
    schedule_header_t* m_schedule_header;
    const ange::schedule::Call* m_current_call;
    bool m_hide_non_active_call;
};

#endif // ABSTRACT_MULTI_CALL_VIEW_H
