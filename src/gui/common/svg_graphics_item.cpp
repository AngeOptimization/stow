/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010

 Copyright: See COPYING file that comes with this distribution

*/

#include "svg_graphics_item.h"
#include <QSvgRenderer>
#include <QPainter>
#include <QGraphicsSceneMouseEvent>

svg_graphics_item_t::svg_graphics_item_t(QString filename, const QRectF& bounds, QGraphicsItem* parent)
  : QGraphicsItem(parent),
    m_renderer(new QSvgRenderer(filename)),
    m_bounds(bounds)
{
  if (m_bounds == QRectF()) {
    m_bounds = m_renderer->viewBoxF();
  }
  setFlag(QGraphicsItem::ItemIsMovable);
}


void svg_graphics_item_t::paint(QPainter* painter, const QStyleOptionGraphicsItem* /*option*/, QWidget* /*widget*/) {
  m_renderer->render(painter, boundingRect());
}

QRectF svg_graphics_item_t::boundingRect() const {
  return m_bounds;
}

void svg_graphics_item_t::allow_move(QRectF bounds) {
  m_movement_bounds = bounds;
  if (!m_movement_bounds.contains(pos())) {
    QPointF oldpos = pos();
    setPos(qMin(m_movement_bounds.right(), qMax(m_movement_bounds.left(), oldpos.x())),
           qMin(m_movement_bounds.bottom(), qMax(m_movement_bounds.top(), oldpos.y())));
  }

}

void svg_graphics_item_t::set_hotspot(QPointF hotspot) {
  prepareGeometryChange();
  m_bounds = QRectF(-hotspot, m_bounds.size());
}

void svg_graphics_item_t::mouseMoveEvent(QGraphicsSceneMouseEvent* event) {
  if (m_movement_bounds != QRectF()) {
    QPointF newpos = pos() + mapToParent(event->pos()) - mapToParent(event->lastPos());
    setPos(qMin(m_movement_bounds.right(), qMax(m_movement_bounds.left(), newpos.x())),
           qMin(m_movement_bounds.bottom(), qMax(m_movement_bounds.top(), newpos.y())));
  }
}

void svg_graphics_item_t::mouseReleaseEvent(QGraphicsSceneMouseEvent* event)
{
  QGraphicsItem::mouseReleaseEvent(event);
  emit item_position_changed(this, pos());
}

#include "svg_graphics_item.moc"
