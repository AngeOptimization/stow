#include "abstractmulticallview.h"

#include "abstractvesselgraphicsitem.h"
#include "document.h"
#include "scheduleheader.h"

#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>
#include <ange/vessel/vessel.h>

#include <QApplication>
#include <QScrollBar>
#include <QSettings>
#include <QWheelEvent>

using ange::schedule::Call;
using ange::schedule::Schedule;
using ange::vessel::Vessel;

AbstractMultiCallView::AbstractMultiCallView(QWidget* parent)
    : super(parent), m_document(0), m_scene(0), m_items(),
      m_schedule_header(new schedule_header_t(this)), m_current_call(0)
{
    {
        QSettings s;
        m_hide_non_active_call = s.value("view/hide_nonactive", QVariant(false)).toBool();
    }
    m_schedule_header->setSizePolicy(QSizePolicy());
    connect(verticalScrollBar(), &QScrollBar::valueChanged, m_schedule_header, static_cast<void (schedule_header_t::*)()>(&schedule_header_t::update));
    setAlignment(Qt::AlignLeft | Qt::AlignTop);
    update_headers();
}

void AbstractMultiCallView::resizeEvent(QResizeEvent* event) {
    update_headers();
    layout_updated();
    super::resizeEvent(event);
}

void AbstractMultiCallView::update_headers() {
    QRect geometry = viewport()->geometry();
    int left = geometry.left();
    const int width = m_schedule_header->minimumSizeHint().width();
    setViewportMargins(width, 0, 0, 0);
    m_schedule_header->setGeometry(left - width, geometry.top(), width, geometry.height());
}

void AbstractMultiCallView::add_call(Call* call) {
    AbstractVesselGraphicsItem* prev_item = 0L;
    Q_FOREACH (AbstractVesselGraphicsItem * item, m_items) {
        if (item->call()->distance(call) > 0) {
            if (!prev_item || prev_item->call()->distance(item->call()) > 0) {
                prev_item = item;
            }
        }
    }
    if (AbstractVesselGraphicsItem* new_item = create_item_from_call(call)) {
        connect(m_document, &document_t::vessel_changed, new_item, &AbstractVesselGraphicsItem::set_vessel);
        if (!new_item->scene()) {
            m_scene->addItem(new_item);
        }
        QPointF move;
        if (prev_item) {
            QRectF br = prev_item->mapRectToScene(prev_item->boundingRect());
            move = br.bottomLeft() - new_item->mapRectToScene(new_item->boundingRect()).topLeft();
        } else {
            move = QPointF(0.0, 0.0);
        }
        new_item->moveBy(move.x(), move.y());
        if (m_items.isEmpty())  {
            setSceneRect(new_item->mapRectToScene(new_item->boundingRect()));
        } else {
            setSceneRect(sceneRect().united(new_item->mapRectToScene(new_item->boundingRect())));
        }
        m_items.insert(call, new_item);
        m_schedule_header->add_call_item(new_item, call);
        Q_FOREACH (AbstractVesselGraphicsItem * item, m_items) {
            if (call->distance(item->call()) > 0) {
                item->moveBy(0.0, new_item->mapRectToScene(new_item->boundingRect()).height());
            }
        }
        if (call != current_call() && m_hide_non_active_call) {
            new_item->hide();
        }
        layout_updated();
    }
}

void AbstractMultiCallView::layout_updated() {
    // Default empty implementation that might be overridden
}

void AbstractMultiCallView::remove_call(Call* call) {
    m_schedule_header->remove_call_item(call);
    QGraphicsItem* item_to_be_removed = m_items.take(call);
    qreal height = item_to_be_removed->boundingRect().height();
    Q_FOREACH (AbstractVesselGraphicsItem * item, m_items) {
        if (call->distance(item->call()) > 0) {
            item->moveBy(0.0, -height);
        }
    }
    delete item_to_be_removed;
    setSceneRect(sceneRect().adjusted(0.0, 0.0, 0.0, -height));
    layout_updated();
}

void AbstractMultiCallView::set_document(document_t* document) {
    if (m_document) {
        m_document->schedule()->disconnect(this);
    }
    m_document = document;
    connect(m_document->schedule(), &Schedule::callAboutToBeRemoved, this, &AbstractMultiCallView::remove_call);
    connect(m_document->schedule(), &Schedule::callAdded, this, &AbstractMultiCallView::add_call);
    connect(m_document->schedule(), &Schedule::rotationAboutToBeChanged, this, &AbstractMultiCallView::removeMovingCall);
    connect(m_document->schedule(), &Schedule::rotationChanged, this, &AbstractMultiCallView::insertMovingCall);
    m_scene = new QGraphicsScene(document);
    m_schedule_header->set_document(document);
    qDeleteAll(m_items);
    m_items.clear();
    m_schedule_header->clear_items();
    setScene(m_scene);
    Q_FOREACH (Call * call, document->schedule()->calls()) {
        add_call(call);
    }
}

void AbstractMultiCallView::set_current_call(const Call* call) {
    m_current_call = call;
    change_current_call();
    Q_FOREACH(AbstractVesselGraphicsItem* item, m_items.values()) {
        if(item->call() == m_current_call) {
            ensureVisible(item);
        }
        item->setCurrentCall(call);
    }
}

void AbstractMultiCallView::change_current_call() {
    if (m_hide_non_active_call) {
        Q_FOREACH (const Call * call, m_items.keys()) {
            if (call == current_call()) {
                m_items.value(call)->show();
                setSceneRect(m_items.value(call)->mapRectToScene(m_items.value(call)->boundingRect()));
            } else {
                m_items.value(call)->hide();
            }
        }
    }
}

void AbstractMultiCallView::hide_non_active_calls_changed(bool hidden) {
    if (m_hide_non_active_call != hidden) {
        m_hide_non_active_call = hidden;
        if (!m_hide_non_active_call) {
            QRectF br;
            Q_FOREACH (const Call * call, m_items.keys()) {
                br = br.united(m_items.value(call)->mapRectToScene(m_items.value(call)->boundingRect()));
                m_items.value(call)->show();
            }
            setSceneRect(br);
        } else {
            change_current_call();
        }
        update();
    }
}

void AbstractMultiCallView::wheelEvent(QWheelEvent* event) {
    const bool ctrlHeld = QApplication::keyboardModifiers() & Qt::ControlModifier;
    if (ctrlHeld) {
        event->accept();
        const qreal currentScale = transform().m11();
        qreal zoomFactor = qMin(qMax(1 + qreal(event->delta())/300.0, 0.9), 1.1);
        setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
        if ((currentScale > 20.0 && event->delta() > 0.0) || (currentScale < 0.1 && event->delta() < 0.0)) {
            zoomFactor = 1.0;
        }
        scale(zoomFactor, zoomFactor);
    } else {
        QGraphicsView::wheelEvent(event);
    }
}



void AbstractMultiCallView::insertMovingCall(int fromPosition, int toPosition) {
    Q_UNUSED(fromPosition);
    add_call(m_document->schedule()->at(toPosition));
}

void AbstractMultiCallView::removeMovingCall(int fromPosition, int toPosition) {
    Q_UNUSED(toPosition);
    remove_call(m_document->schedule()->at(fromPosition));
}

#include "abstractmulticallview.moc"
