#ifndef ABSTRACT_VESSEL_GRAPH_ITEM_H
#define ABSTRACT_VESSEL_GRAPH_ITEM_H

#include <qgraphicsitem.h>
#include <ange/units/units.h>

class QTimer;
namespace ange {
namespace schedule {
class Call;
}
namespace vessel {
class Vessel;
class BaySlice;
}
}

/**
 * Common functionality between vessel graphs, current crane split and stresses.
 */
class AbstractVesselGraphicsItem : public QGraphicsObject {
  Q_OBJECT
  Q_INTERFACES(QGraphicsItem)
  public:
    AbstractVesselGraphicsItem(const ange::vessel::Vessel* vessel,
                                 const ange::schedule::Call* call,
                                 qreal bottom_margin = 0.0,
                                 QGraphicsItem* parent = 0);
    virtual QRectF boundingRect() const;
    virtual void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget = 0);

    /**
     * @return call cranesplit is for
     */
    const ange::schedule::Call* call() const {
      return m_call;
    }
    /**
     * @return the bayslice, if any, for pos (in local coordinates)
     */
    virtual ange::vessel::BaySlice* bay_slice_for_pos(const QPointF& pos) const;

    /**
     * @return bounding rect (in local coordiantes) of bay_slice
     */
    virtual QRectF bay_area(const ange::vessel::BaySlice* bay_slice) const;

    /**
     * @return transformation for writing graph (remember to clip)
     */
    QTransform get_transform_for_graph_rect(const QRectF& graph_rect) const;

    /**
     * sets current call to \param call
     */
    void setCurrentCall(const ange::schedule::Call* call);

  public Q_SLOTS:
    void update_call(const ange::schedule::Call* call);

    /**
     * Update view in 100ms or so
     */
    void update_later();

    /**
     * Update view now
     */
    void update_now();

    /**
     * Replace vessel
     */
    virtual void set_vessel(const ange::vessel::Vessel* vessel);

    /**
     * @returns the bottom margin (which default to zero)
     * This is the area where e.g. the cranesplit draws the crane plan
     */
    qreal bottom_margin() const {
      return m_bottom_margin;
    }

  protected:
      virtual void drawBackground(QPainter* painter);
    /**
     * @return the bounding box for the coordinate system. This bounding box  will be the total bounding box substracted margins
     */
    virtual QRectF coordinate_system_rect() const;

    QRectF draw_coordinate_system(QPainter* painter, const qreal max_y_label, const qreal min_y_label = 0.0);

    /**
     * @return the maximum (y) value for the coordinate system
     */
    virtual double max_value() const = 0;

    /**
     * @return the min (y) value for the coordinate system. Defaults to 0 if not overridden
     */
    virtual double min_value() const;

    /**
     * @return the widest possible y-label. By default, this just max_value() as a string
     */
    virtual QString widest_y_label() const;
    /**
     * Draws the footer. By default, this draws a line and the bay number underneath
     * @param bounding_rect bounding_rect of slice area's footer
     */
    virtual void draw_footer(QPainter* painter, ange::vessel::BaySlice* slice, const QRectF& bounding_rect) const;

    /**
     * Draw the bay area itself. By default, this function does nothing.
     * @param max_value the value returned by max_value, to avoid excessive recompuations
     * @param bounding_rect the area to draw inside
     * @param slice the bay slice that this area represents
     */
    virtual void draw_bay(QPainter* painter, ange::vessel::BaySlice* slice, const QRectF& bounding_rect, double max_value) const;

    /**
     * @return the assigned right margin width. This is usefull when overloading the graph and needing axis or other explanations
     * in the right side of the graph. By default this margin has width 0.
     */
    virtual qreal right_margin_width() const;

    ange::units::Length vessel_aft() const;
    ange::units::Length vessel_fore() const;

    const ange::vessel::Vessel* m_vessel;
    const ange::schedule::Call* m_call;
    qreal m_bottom_margin;
    QTimer* m_update_timer;
    const ange::schedule::Call* m_currentCall;

};

#endif // ABSTRACT_VESSEL_GRAPH_ITEM_H
