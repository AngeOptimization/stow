#include "abstractvesselgraphicsitem.h"

#include <QPainter>
#include <qmath.h>
#include <QTimer>
#include <QApplication>

#include <ange/vessel/vessel.h>
#include <ange/vessel/bayslice.h>

#include "gui/utils/painter_utils.h"
#include <ange/vessel/stack.h>
#include <cmath>
#include <limits>

using namespace ange::units;
using ange::vessel::BaySlice;

const qreal STRETCH_FACTOR = 2.0; // How many pixels per meter

AbstractVesselGraphicsItem::AbstractVesselGraphicsItem(const ange::vessel::Vessel* vessel,
                                                           const ange::schedule::Call* call,
                                                           qreal bottom_margin,
                                                           QGraphicsItem* parent)
  : QGraphicsObject(parent),
    m_vessel(vessel),
    m_call(call),
    m_bottom_margin(bottom_margin),
    m_update_timer(new QTimer(this)),
    m_currentCall(0)
{
  m_update_timer->setSingleShot(true);
  m_update_timer->setInterval(100);
  connect(m_update_timer, SIGNAL(timeout()), SLOT(update_now()));
}
ange::units::Length AbstractVesselGraphicsItem::vessel_aft() const {
  return m_vessel->baySlices().isEmpty() ? 0.0*meter : m_vessel->baySlices().back()->stacks().front()->bottom().l() - 10.0*meter;
}

ange::units::Length AbstractVesselGraphicsItem::vessel_fore() const {
  return m_vessel->baySlices().isEmpty() ? 0.0*meter : m_vessel->baySlices().front()->stacks().front()->bottom().l() + 10.0*meter;
}

QRectF AbstractVesselGraphicsItem::boundingRect() const {
  QList<BaySlice*> bay_slices = m_vessel->baySlices();
  double width = 0.0;
  if (!bay_slices.isEmpty()) {
    const qreal label_width = qApp->fontMetrics().width(widest_y_label());
    width = (vessel_fore()-vessel_aft())*STRETCH_FACTOR/meter+label_width;
  }
  const qreal height = 200.0;
  return QRectF(0.0, 0.0, width, height);
}

void AbstractVesselGraphicsItem::update_call(const ange::schedule::Call* call) {
  if (call == m_call) {
    update();
  }
}

QString AbstractVesselGraphicsItem::widest_y_label() const {
  return QString::number(max_value(),'f', 1);
}

QRectF AbstractVesselGraphicsItem::coordinate_system_rect() const {
  QRectF bounding_rect = boundingRect().adjusted(0,0,0,-bottom_margin());
  QRectF unscaled_label_bounding_box = qApp->fontMetrics().boundingRect(widest_y_label());
  const qreal bay_label_height = (2+unscaled_label_bounding_box.height()) * 0.8 / sceneTransform().m22();
  const qreal label_width = unscaled_label_bounding_box.width() / 0.8 / sceneTransform().m11() + 2;
  qreal bottom_y_axis = bounding_rect.bottom() - bay_label_height;
  qreal top_y_axis = bounding_rect.top() + bay_label_height / 2;
  qreal x_y_axis = bounding_rect.left() + label_width;
  QRectF rv(x_y_axis, top_y_axis, bounding_rect.width()-label_width-right_margin_width(), bottom_y_axis - top_y_axis);
  return rv;
}

qreal AbstractVesselGraphicsItem::right_margin_width() const {
  return 0.0;
}

double AbstractVesselGraphicsItem::min_value() const {
    return 0.0;
}

QRectF AbstractVesselGraphicsItem::draw_coordinate_system(QPainter* painter, const qreal max_y_label, const qreal min_y_label) {
  QRectF coordinate_rect = coordinate_system_rect();
  const qreal top_y_axis = coordinate_rect.top();
  const qreal bottom_y_axis = coordinate_rect.bottom();
  const qreal x_y_axis = coordinate_rect.left();
  painter->drawLine( QLineF(coordinate_rect.left(), top_y_axis, coordinate_rect.left(), bottom_y_axis) );
  QRect unscaled_label_bounding_box = qApp->fontMetrics().boundingRect(widest_y_label());
  const qreal label_width = unscaled_label_bounding_box.width() / 0.8 / sceneTransform().m11() + 2;
  QRectF br = boundingRect();
  // Draw coordinate system
  int last_tick_value = std::numeric_limits< int >::min();
  for (int tick = 0; tick < 5; ++tick) {
    int tick_value = min_y_label + qFloor((max_y_label - min_y_label) * tick/4.0);
    if (tick>0 && tick_value == last_tick_value) {
      continue;
    }
    last_tick_value = tick_value;
    qreal tick_y_pos = (top_y_axis - bottom_y_axis)*((tick_value-min_y_label)/(max_y_label-min_y_label))
                       + bottom_y_axis;
    painter->drawLine(QLineF(x_y_axis - 1, tick_y_pos, x_y_axis + 1, tick_y_pos));
    QString label = QString::number(tick_value);
    QRectF label_box(br.left(), tick_y_pos-8, label_width - 2, 16);
    ange::painter_utils::draw_text_in_rect(painter, label_box, 0.8, label, Qt::AlignRight | Qt::AlignVCenter);
  }
  // Draw obsever position
  const qreal observer_lcg = m_vessel->observerLcg()/meter;
  if (!std::isnan(observer_lcg) && !m_vessel->baySlices().empty()) {
    const double last_bay_slice_alongship  = m_vessel->baySlices().back()->stacks().front()->bottom().l()/meter;
    const qreal x = coordinate_rect.left() + (observer_lcg - last_bay_slice_alongship)*STRETCH_FACTOR  + 6*STRETCH_FACTOR - 1;
    painter->setPen(QColor(200,200,255));
    painter->drawLine(QLineF(x,coordinate_rect.bottom(), x, coordinate_rect.top()));
  }
  return coordinate_rect;
}

ange::vessel::BaySlice* AbstractVesselGraphicsItem::bay_slice_for_pos(const QPointF& pos) const {
  QRectF coordinate_rect = coordinate_system_rect();
  if (coordinate_rect.contains(pos)) {
    const int nslices = m_vessel->baySlices().size();
    const int index = qMin(qFloor((coordinate_rect.right() - pos.x())/coordinate_rect.width()*nslices), nslices-1);
    return m_vessel->baySlices().at(index);
  }
  return 0L;
}

QRectF AbstractVesselGraphicsItem::bay_area(const ange::vessel::BaySlice* bay_slice) const {
  QRectF coordinate_rect = coordinate_system_rect();
  const qreal bay_area_outer_width = 12*STRETCH_FACTOR; // 40 feet (*STRETCH_FACTOR)
  const qreal bay_area_margin = 1;
  qreal bay_slice_alongships = bay_slice->stacks().front()->bottom().l()/meter;
  qreal last_bay_slice_alongship = m_vessel->baySlices().back()->stacks().front()->bottom().l()/meter;
  QRectF bay_inner_area(coordinate_rect.topLeft() + QPointF((bay_slice_alongships-last_bay_slice_alongship)*STRETCH_FACTOR, 0.0),
                        QSizeF(bay_area_outer_width - 2*bay_area_margin,
                               coordinate_rect.height()));
  return bay_inner_area;

}

void AbstractVesselGraphicsItem::draw_footer(QPainter* painter, ange::vessel::BaySlice* slice, const QRectF& bounding_rect) const {
  painter->setPen(Qt::black);
  ange::painter_utils::draw_text_in_rect(painter, bounding_rect, 0.8, QString::number(slice->joinedBay()));
  painter->drawLine(bounding_rect.topLeft(), bounding_rect.topRight());
}

void AbstractVesselGraphicsItem::draw_bay(QPainter* painter,
                                            ange::vessel::BaySlice* slice,
                                            const QRectF& bounding_rect,
                                            double max_value) const {
  Q_UNUSED(painter);
  Q_UNUSED(slice);
  Q_UNUSED(bounding_rect);
  Q_UNUSED(max_value);
}

void AbstractVesselGraphicsItem::drawBackground(QPainter* painter) {
    QRectF boundingrect = boundingRect();
    if(m_call != m_currentCall) {
        QLinearGradient grad;
        grad.setColorAt(qreal(0.0),QColor("#F0F0F0"));
        grad.setColorAt(qreal(0.5),QColor("#D0D0D0"));
        grad.setColorAt(qreal(1.0),QColor("#F0F0F0"));
        grad.setStart(boundingrect.topLeft());
        grad.setFinalStop(boundingrect.topRight());
        painter->fillRect(boundingrect,QBrush(grad));
    }
}


void AbstractVesselGraphicsItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* /*option*/, QWidget* /*widget*/) {
    drawBackground(painter);
  const double max_value_cached = max_value();
  const QRectF inner_coordinate_system_area = draw_coordinate_system(painter, max_value_cached, min_value());
  const qreal bay_area_footer_height = boundingRect().bottom() - inner_coordinate_system_area.bottom();

  Q_FOREACH(BaySlice* slice, m_vessel->baySlices()) {
    const QRectF bay_inner_area = bay_area(slice);
    draw_bay(painter, slice, bay_inner_area, max_value_cached);
    draw_footer(painter, slice, QRectF(bay_inner_area.bottomLeft(), QSizeF(bay_inner_area.width(), bay_area_footer_height)));
  }
}

QTransform AbstractVesselGraphicsItem::get_transform_for_graph_rect(const QRectF& graph_rect) const {
  const QRectF coordinate_rect = coordinate_system_rect();
//   qDebug() << __LINE__;
  const qreal m11 = coordinate_rect.width() / graph_rect.width();
  const qreal m22 = -coordinate_rect.height()/graph_rect.height();
  const qreal dx = coordinate_rect.left() - graph_rect.left()*m11;
  const qreal dy = coordinate_rect.top() - graph_rect.bottom()*m22;
  QTransform rv(m11, 0.0, 0.0, m22, dx, dy);
  return rv;
}

void AbstractVesselGraphicsItem::update_later() {
  if (!m_update_timer->isActive()) {
    m_update_timer->start();
  }
}

void AbstractVesselGraphicsItem::update_now() {
  update();
  if (m_update_timer->isActive()) {
    m_update_timer->stop();
  }
}

void AbstractVesselGraphicsItem::set_vessel(const ange::vessel::Vessel* vessel) {
  m_vessel = vessel;
  update();
}

void AbstractVesselGraphicsItem::setCurrentCall(const ange::schedule::Call* call) {
    bool scheduleUpdate = false;
    if(m_currentCall == m_call || m_call == call) {
        scheduleUpdate = true;
    }
    m_currentCall = call;
    if(scheduleUpdate) {
        update_later();
    }
}


#include "abstractvesselgraphicsitem.moc"
