#include "selectionsummarylabel.h"
#include <document/pools/pool.h>
#include <document/containers/height_aggregator.h>
#include <document/containers/length_aggregator.h>
#include <document/containers/container_list.h>
#include <QTimer>

void SelectionSummaryLabel::setSelectionPool(const pool_t* selection_pool, container_list_t* containerList) {
    m_containerList = containerList;
    m_selection_pool = selection_pool;
    connect(selection_pool, &pool_t::changed, this, &SelectionSummaryLabel::selectionChanged);
}

void SelectionSummaryLabel::selectionChanged() {
    if(!m_selection_pool){
        setText("");
        return;
    }
    QString display_string = "Selection: | ";
    QMap<int, QMap<int, int> > sizeTypeCounts;
    LengthAggregator lengthAggregator(m_containerList);
    HeightAggregator heightAggregator(m_containerList);
    for(int i=0; i < m_selection_pool->containers().size(); ++i){
        sizeTypeCounts[lengthAggregator(m_selection_pool->included_rows().at(i))]
                      [heightAggregator(m_selection_pool->included_rows().at(i))]++;
    }
    Q_FOREACH(int lengthKey, sizeTypeCounts.keys()) {
        Q_FOREACH(int heightKey, sizeTypeCounts.value(lengthKey).keys()) {
        display_string.append(QString("%1:%2 | ")
                            .arg(lengthAggregator.categoryHeaderData(lengthKey).toString()
                                +heightAggregator.categoryHeaderData(heightKey).toString())
                            .arg(sizeTypeCounts.value(lengthKey).value(heightKey)));
        }
    }
    setText(display_string);
}

#include "selectionsummarylabel.moc"
