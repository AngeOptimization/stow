#include "mainwindow.h"

#include "about_dialog.h"
#include "bapliecomparatordialog.h"
#include "callutils.h"
#include "container_leg.h"
#include "container_list.h"
#include "container_list_change_command.h"
#include "container_move.h"
#include "container_sorting_model.h"
#include "container_stow_command.h"
#include "containerdeleter.h"
#include "containerselectionbundlehelper.h"
#include "crane_split_item.h"
#include "debugstabilityreport.h"
#include "document.h"
#include "document_interface.h"
#include "error_popup.h"
#include "fileopener.h"
#include "findcontainersdialog.h"
#include "fixate_command.h"
#include "generatecontainersdialog.h"
#include "gui_interface.h"
#include "import_edifact_wizard.h"
#include "importvessel.h"
#include "layoutcontroller.h"
#include "macrocompartmentcommand.h"
#include "main_statistics_widget.h"
#include "masterplanningreport.h"
#include "merger_command.h"
#include "mode.h"
#include "modify_containers_dialog.h"
#include "nonmodaldialoghelper.h"
#include "override_cursor_handler.h"
#include "pluginmanager.h"
#include "pool.h"
#include "problem_model.h"
#include "reallydialog.h"
#include "recapview.h"
#include "reportgenerator.h"
#include "restoweditor.h"
#include "schedule_change_command.h"
#include "selected_container_list.h"
#include "settings.h"
#include "settingsdialog.h"
#include "setupdelegates.h"
#include "sortfilterproblemsbycallproxymodel.h"
#include "stabilityfigures.h"
#include "stowage.h"
#include "tableviewutils.h"
#include "tankgroupselectorsynchronizer.h"
#include "tankitemselectionmodel.h"
#include "tankmodel.h"
#include "tool.h"
#include "toolmanager.h"
#include "ui_main_window.h"
#include "userconfiguration.h"
#include "version.h"
#include "vesselgraphicsitem.h"
#include "vesselscene.h"

#include <stowplugininterface/guiusinginterface.h>
#include <stowplugininterface/idocument.h>
#include <stowplugininterface/istower.h>
#include <stowplugininterface/macroplan.h>
#include <stowplugininterface/masterplanningplugin.h>
#include <stowplugininterface/problem.h>

#include <ange/containers/container.h>
#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>
#include <ange/vessel/bayslice.h>
#include <ange/vessel/compartment.h>
#include <ange/vessel/slot.h>
#include <ange/vessel/stack.h>
#include <ange/vessel/vessel.h>
#include <ange/vessel/vesseltank.h>

#include <QAbstractButton>
#include <QAbstractProxyModel>
#include <QClipboard>
#include <QCloseEvent>
#include <QDesktopServices>
#include <QEvent>
#include <QFile>
#include <QFileDialog>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QMap>
#include <QMessageBox>
#include <QPushButton>
#include <QSettings>
#include <QSortFilterProxyModel>
#include <QUndoStack>

#include <iostream>
#include <stdexcept>

using namespace ange::angelstow;
using ange::vessel::UnknownPosition;
using ange::containers::IsoCode;
using ange::containers::Container;
using ange::vessel::BaySlice;
using ange::vessel::Compartment;
using ange::vessel::Vessel;
using ange::schedule::Call;
using ange::schedule::Schedule;
using ange::angelstow::MacroPlan;
using ange::angelstow::IStower;

#define DEFAULT_UNTITLED_AUTOSAVE_FILE QStandardPaths::writableLocation(QStandardPaths::DataLocation) + "/" + QLatin1String("untitled_autosave")

MainWindow::MainWindow(PluginManager* pluginManager, document_t* initial_document, QWidget* parent) :
    QMainWindow(parent),
    m_ui(new Ui_MainWindow()),
    m_document(0L),
    m_bay_plan(0L),
    m_recent_menu(0L),
    m_recent_mapper(new QSignalMapper(this)),
    m_settings_dialog(),
    m_bundlehelper(0),
    m_selectionListSortModel(new QSortFilterProxyModel(this)),
    m_problemSortFilterProxyModel(new SortFilterProblemsByCallProxyModel(this)),
    m_toolManager(new ToolManager(this)),
    m_pluginManager(pluginManager),
    m_restowEditor(new RestowEditor(this)),
    m_inDestructor(false)
{
    m_ui->setupUi(this);
    m_layoutController = new LayoutController(this, m_ui);
    connect(m_recent_mapper,SIGNAL(mapped(QString)),SLOT(stowage_open(QString)));
    setWindowState(Qt::WindowMaximized);
    m_ui->macro_stow_toolbar->hide();
    m_ui->vessel_view->setToolManager(m_toolManager);
    m_ui->vessel_view->setRecapManager(this);
    connect(m_ui->actionQuit,SIGNAL(triggered(bool)),SLOT(close()));
    connect(m_ui->action_stowage_open, SIGNAL(triggered(bool)), SLOT(stowage_open()));
    connect(m_ui->action_stowage_save, SIGNAL(triggered(bool)), SLOT(stowage_save()));
    connect(m_ui->action_stowage_save_as, SIGNAL(triggered(bool)), SLOT(stowage_save_as()));
    connect(m_ui->actionGenerate_container_list, SIGNAL(triggered(bool)), SLOT(generate_containerlist()));
    connect(m_ui->action_import_vessel, SIGNAL(triggered(bool)), SLOT(import_vessel()));
    connect(m_ui->action_import_wizard, SIGNAL(triggered(bool)), SLOT(import_edifact_wizard()));
    connect(m_ui->action_delete_selected_containers, SIGNAL(triggered(bool)), SLOT(delete_selected_containers()));
    connect(m_toolManager, &ToolManager::toolChanged, m_ui->vessel_view, &vessel_graphics_view_t::set_tool);
    connect(m_ui->actionAbout_Angelstow, &QAction::triggered, this, &MainWindow::openAboutDialog);
    connect(m_ui->schedule_view, SIGNAL(call_selected(const ange::schedule::Call*)),
            SLOT(change_current_call(const ange::schedule::Call*)));
    connect(m_ui->schedule_view, SIGNAL(call_selected(const ange::schedule::Call*)), m_ui->macro_stow_toolbar,
            SLOT(set_current_call(const ange::schedule::Call*)));
    connect(m_ui->action_settings, SIGNAL(triggered(bool)), SLOT(display_settings()));
    connect(m_ui->actionPrevious_call,SIGNAL(triggered(bool)),SLOT(select_previous_call()));
    connect(m_ui->actionNext_call,SIGNAL(triggered(bool)),SLOT(select_next_call()));
    connect(m_ui->actionGenerate_reports,SIGNAL(triggered(bool)),this,SLOT(showGenerateReportsDialog()));
    if(qEnvironmentVariableIsSet("ANGELSTOW_DEVELOPER_MODE")) {
        addDeveloperMenu();
    } else {
#ifdef Q_OS_LINUX
        addDeveloperMenu();
#endif // Q_OS_LINUX
    }

    QActionGroup* mode_group = new QActionGroup(this);
    mode_group->addAction(m_ui->action_micro_plan);
    mode_group->addAction(m_ui->action_macro_plan);
    mode_group->setExclusive(true);
    m_ui->action_micro_plan->setChecked(true);
    QActionGroup* row_label_group = new QActionGroup(this);
    row_label_group->addAction(m_ui->actionRow_name);
    row_label_group->addAction(m_ui->actionRow_max_height);
    row_label_group->addAction(m_ui->actionRow_max_weight);
    row_label_group->addAction(m_ui->actionRow_remaining_weight);
    row_label_group->setExclusive(true);
    m_ui->actionRow_name->setChecked(true);
    connect(m_ui->action_macro_stow, SIGNAL(triggered(bool)), SLOT(slot_run_stowage_plugin()));
    connect(m_ui->action_macro_plan, SIGNAL(toggled(bool)), m_ui->action_macro_stow, SLOT(setVisible(bool)));
    m_ui->action_macro_stow->setVisible(false);
    m_main_stats = new main_statistics_widget_t(initial_document,this);
    m_ui->statistics_scroll_area->setWidget(m_main_stats);
    m_ui->undo_dock->hide();
    connect(m_ui->action_add_vessel_view, SIGNAL(triggered(bool)), SLOT(add_extra_vesselview()));
    connect(m_ui->action_add_recap, SIGNAL(triggered(bool)), SLOT(add_recap()));
    m_ui->all_container_dock->hide();
    connect(m_ui->vessel_view,SIGNAL(create_zoomed(QRectF)),SLOT(add_extra_vesselview(QRectF)));
    m_problemSortFilterProxyModel->setDynamicSortFilter(true);
    m_problemSortFilterProxyModel->setSortRole(problem_model_t::SORT_ROLE);
    m_problemSortFilterProxyModel->sort(problem_model_t::LOCATION, Qt::AscendingOrder);
    m_tankGroupSynchroniser = new TankGroupSelectorSynchronizer();
    m_ui->tankWidget->setTankGroupSelectorSynchronizer(m_tankGroupSynchroniser);
    m_ui->tankTable->setTankGroupSelectorSynchronizer(m_tankGroupSynchroniser);

    m_ui->problem_view->setModel(m_problemSortFilterProxyModel);
    TableViewUtils::setupShowHideColumnsAndCopySelection(m_ui->all_containers_table_view);

    TableViewUtils::setupShowHideColumnsAndCopySelection(m_ui->stability_figures_view);
    m_ui->stability_figures_view->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);

    m_ui->selected_containers_table_view->setSortingEnabled(true);
    m_ui->selected_containers_table_view->setModel(m_selectionListSortModel);
    TableViewUtils::setupShowHideColumnsAndCopySelection(m_ui->selected_containers_table_view);
    set_document(initial_document);
    gui_interface_t* igui = new gui_interface_t(this);
    m_pluginManager->initializeGui(igui);
    m_ui->schedule_view->select(m_document->schedule()->calls().size()>2?1:0); // initialize for the blank document.
    connect(m_ui->problem_view, &problem_view_t::zoomToProblem, m_ui->vessel_view, &vessel_graphics_view_t::zoomToProblem);

    m_autosave_timer.setInterval(3*60*1000 /*5 minutes*/);
    m_autosave_timer.stop();
    connect(&m_autosave_timer,SIGNAL(timeout()),SLOT(autosave()));
    load_settings();
    check_default_autosave();
    statusBar()->addPermanentWidget(&m_selection_summary_label);
    m_ui->stresses_view->setStowagePlugin(m_pluginManager->masterPlanningPlugin());
    m_restowEditor->setScheduleView(m_ui->schedule_view);
    connect(m_ui->actionCancelRestowContainers, &QAction::triggered, m_restowEditor, &RestowEditor::restowCancel);
    updateShowPanelMenu();
}

MainWindow::~MainWindow() {
    m_inDestructor = true;
}

bool windowTitleLessThan(const QDockWidget* d1, const QDockWidget* d2) {
    return d1->windowTitle() < d2->windowTitle();
}

void MainWindow::updateShowPanelMenu() {
    if (m_inDestructor) {
        return; // QWidgets delete children before they disconnect, avoid crashing on finding half deleted RecapView
    }
    m_ui->menuShowPanel->clear();
    QList<QDockWidget*> dockWidgets = findChildren<QDockWidget*>();
    qSort(dockWidgets.begin(), dockWidgets.end(), windowTitleLessThan);
    Q_FOREACH (QDockWidget* dockWidget, dockWidgets) {
        QAction* action = m_ui->menuShowPanel->addAction(dockWidget->windowTitle());
        action->setData(QVariant::fromValue<QDockWidget*>(dockWidget));
        connect(action, &QAction::triggered, this, &MainWindow::showDockWidget);
    }
    m_ui->menuShowPanel->setDisabled(m_ui->menuShowPanel->isEmpty());
}

void MainWindow::showDockWidget() {
    QAction* action = qobject_cast<QAction*>(sender());
    Q_ASSERT(action);
    QDockWidget* dockWidget = action->data().value<QDockWidget*>();
    Q_ASSERT(dockWidget);
    dockWidget->show();
    dockWidget->raise();
}

void MainWindow::check_default_autosave() {
  if(QFile::exists(DEFAULT_UNTITLED_AUTOSAVE_FILE)) {
    ask_recover_autosave(DEFAULT_UNTITLED_AUTOSAVE_FILE);
  }
}

bool MainWindow::ask_recover_autosave(const QString& autosave) {
  QMessageBox b(QMessageBox::Question,QLatin1String("Recover autosave?"),QString::fromLatin1("Autosave file found (%1). Do you want to recover it?").arg(autosave),QMessageBox::Ok|QMessageBox::Cancel);
  b.button(QMessageBox::Ok)->setText(tr("Use autosave"));
  b.button(QMessageBox::Cancel)->setText(tr("Discard autosave"));
  if(b.exec()==QMessageBox::Ok) {
    really_stowage_open(autosave);
    cleanup_autosave(autosave);
    m_document->stowage_filename(QString());
    return true;
  }
  return false;
}

void MainWindow::crash(bool) {
    volatile int* a = (int*)(0);
    *a = 1;
}

static QLabel* spacer(int width) {
    QLabel* space = new QLabel();
    space->setMinimumWidth(width);
    return space;
}

void MainWindow::showWarning(const PassiveNotification& warning) {
    // Create toolbar
    addToolBarBreak();
    QToolBar* toolBar = addToolBar("Warning tool bar");
    toolBar->setFloatable(false);
    toolBar->setMovable(false);
    QPalette palette = toolBar->palette();
    palette.setBrush(QPalette::Background, palette.alternateBase());
    toolBar->setPalette(palette);
    toolBar->setAutoFillBackground(true);
    toolBar->addWidget(spacer(20));
    toolBar->addWidget(new QLabel(tr("<span style='color:red;font-weight:bold'>Warning:</span> ") + warning.message));
    toolBar->addWidget(spacer(20));
    // Hide on change in undostack
    connect(document()->undostack(), SIGNAL(indexChanged(int)), toolBar, SLOT(deleteLater()));
    // Add extra button
    if (!warning.buttonText.isNull()) {
        Q_ASSERT(!warning.buttonIcon.isNull());
        Q_ASSERT(warning.buttonReceiver);
        Q_ASSERT(warning.buttonSlot);
        QPushButton* button = new QPushButton(warning.buttonText);
        button->setIcon(warning.buttonIcon);
        connect(button, SIGNAL(clicked(bool)), warning.buttonReceiver, warning.buttonSlot);
        toolBar->addWidget(button);
        toolBar->addWidget(spacer(5));
    }
    // Add Hide button
    QPushButton* hideButton = new QPushButton(tr("Hide warning"));
    hideButton->setIcon(QIcon(":/icons/icons/edit-clear.png"));
    connect(hideButton, SIGNAL(clicked(bool)), toolBar, SLOT(deleteLater()));
    toolBar->addWidget(hideButton);
}

void MainWindow::load_settings() {
    if (!m_layoutController->restoreState("Startup")) {
        m_layoutController->restoreBuiltinState("Simple");
    }
    QSettings settings;
    m_recent_files = settings.value("recent_files").toStringList();
    update_recent_opened();
}

void MainWindow::save_settings() {
  m_layoutController->saveState("Startup");
  QSettings settings;
  settings.setValue("recent_files", m_recent_files);
}

bool MainWindow::sanity_in_export_formats() {
  if(is_befor(current_call())||is_after(current_call())) {
    QMessageBox box(this);
    box.setWindowTitle(QLatin1String("Can't export current port"));
    box.setText(QString("It doesn't make sense to do this kind of export in port %1").arg(current_call()->uncode()));
    box.setStandardButtons(QMessageBox::Cancel);
    box.setDefaultButton(QMessageBox::Cancel);
    box.exec();
    return false;
  }
  return true;
}

void MainWindow::set_document(document_t* doc) {
  bool updateonly = (doc == m_document);
  QSettings settings;
  document_t* oldDocument = 0;
  if (!updateonly) {
    if (m_document) {
      oldDocument = m_document;
      delete m_bay_plan;
      m_bay_plan = 0L;
    }
    m_document = doc;
    m_document->setParent(this);
  }
  m_main_stats->set_document(m_document);
  m_ui->macro_stow_toolbar->set_document(m_document);

  if (!updateonly || m_ui->stability_figures_view->model()) {
    m_ui->stability_figures_view->setModel(m_document->stability_figures());
    m_ui->stability_figures_view->resizeColumnsToContents();
    m_ui->stability_figures_view->resizeRowsToContents();
  }

  if (!updateonly || m_ui->all_containers_table_view->model()) {
    // Here do all the things that has to be done for the container model, specifically connected to the document
    container_sorting_model_t* proxy = new container_sorting_model_t(m_document->containers());
    proxy->setDynamicSortFilter(true);
    proxy->setSourceModel(m_document->containers());
    m_ui->all_containers_table_view->setModel(proxy);
    m_ui->all_containers_table_view->setSortingEnabled(true);
    m_ui->all_containers_table_view->resizeColumnsToContents();
    m_ui->actionShow_Equipment_ID->setChecked(false);

    m_selected_pool = new pool_t(m_document->containers());
    m_selected_pool->synchronize(m_ui->all_containers_table_view->selectionModel());
    m_bundlehelper = new ContainerSelectionBundleHelper(m_selected_pool, m_document);

    m_ui->vessel_view->set_selection_pool(m_selected_pool);
    m_ui->vessel_view->set_document(m_document);
    m_ui->vessel_view->setBundleHelper(m_bundlehelper);
    connect(m_selected_pool, SIGNAL(empty_changed(bool)), this, SLOT(forwardToAutoToolHandling(bool)));
    selected_container_list_t* selected_list = new selected_container_list_t(m_document->containers(), m_selected_pool);
    m_selectionListSortModel->setSourceModel(selected_list);
    m_ui->selected_containers_table_view->hideColumn(static_cast<int>(container_list_t::MOVES_COL));
    m_ui->selected_containers_table_view->resizeColumnsToContents();
    connect(selected_list, SIGNAL(rowsInserted (const QModelIndex, int, int)), m_ui->selected_containers_table_view, SLOT(resizeColumnsToContents()));
    connect(selected_list, SIGNAL(total_display_string(QString)), m_ui->selected_containers_total_label, SLOT(setText(QString)));
  }
  if(!updateonly || m_ui->problem_view->model()) {
    m_problemSortFilterProxyModel->setSourceModel(m_document->problem_model());
  }
    if(!updateonly || !m_ui->tankTable->model()) {
        m_tankGroupSynchroniser->clear();
        m_ui->tankTable->setModel(m_document->tankModel());
        TankItemSelectionModel* tankselection = new TankItemSelectionModel(m_ui->tankTable->proxyModel(), m_document);
        m_ui->tankTable->setSelectionModel(tankselection);
        m_ui->tankWidget->setTankSelection(tankselection);
        connect(m_ui->tankWidget,&TankWidget::tanksSelected, tankselection, &TankItemSelectionModel::selectTanks);
        connect(tankselection,&TankItemSelectionModel::selectionChanged, m_ui->tankWidget, &TankWidget::selectTanks);
        m_ui->tankTable->setDocument(m_document);
        m_ui->tankWidget->setDocument(m_document);
    }
  if(!updateonly){
    // Here we do all the rest
    connect(m_ui->schedule_view, SIGNAL(call_selected(const ange::schedule::Call*)), m_document->containers(), SLOT(set_current_call(const ange::schedule::Call*)));
    m_ui->undoview->setStack(m_document->undostack());
    m_bay_plan = new vessel_scene_t(m_document, m_selected_pool);
    m_bay_plan->setToolManager(m_toolManager);
    connect(m_bay_plan, SIGNAL(containers_selected(QList<const ange::containers::Container*>)), m_selected_pool, SLOT(include(QList<const ange::containers::Container*>)));

    m_toolManager->setAutoToolSelect(m_selected_pool->included_rows().empty(),qApp->keyboardModifiers());
    m_ui->vessel_view->set_tool(m_toolManager->currentTool());

    connect(m_toolManager, &ToolManager::modeChanged, m_ui->macro_stow_toolbar, &macro_stow_toolbar_t::slot_mode_changed);
    connect(m_ui->schedule_view,SIGNAL(call_selected(const ange::schedule::Call*)), m_bay_plan, SLOT(set_active_call(const ange::schedule::Call*)));
    connect(m_ui->schedule_view,SIGNAL(call_focused(const ange::schedule::Call*)), m_ui->vessel_view, SLOT(focus_call(const ange::schedule::Call*)));
    connect(m_bay_plan, SIGNAL(call_changed(const ange::schedule::Call*)), m_ui->schedule_view, SLOT(select(const ange::schedule::Call*)));
    m_ui->vessel_view->setScene(m_bay_plan);
    Q_FOREACH(vessel_graphics_view_t* vesselview, m_extra_vessel_views){
      vesselview->setScene(m_bay_plan);
    }
    connect(m_ui->vessel_view, SIGNAL(tool_applied(QRectF,tool_t*)), m_bay_plan, SLOT(Slotool_applied(QRectF,tool_t*)));
    connect(m_ui->vessel_view, SIGNAL(call_activated(const ange::schedule::Call*)), m_ui->schedule_view, SLOT(select(const ange::schedule::Call*)) );
    m_ui->schedule_view->set_document(m_document, m_selected_pool);
    QAction* undoaction = m_document->undostack()->createUndoAction(m_document);
    QAction* redoaction = m_document->undostack()->createRedoAction(m_document);

    redoaction->setShortcut(QApplication::translate("MainWindow", "Ctrl+Shift+Z", 0));
    undoaction->setShortcut(QApplication::translate("MainWindow", "Ctrl+Z", 0));
    redoaction->setIcon(QIcon(QString::fromUtf8(":/icons/icons/edit-redo.png")));
    undoaction->setIcon(QIcon(QString::fromUtf8(":/icons/icons/edit-undo.png")));
    m_ui->menuEdit->addAction(undoaction);
    m_ui->menuEdit->addAction(redoaction);
    m_ui->main_toolbar->insertAction(0, undoaction);
    m_ui->main_toolbar->insertAction(0, redoaction);
    m_ui->action_micro_plan->setChecked(true);
    m_ui->actionRow_name->setChecked(true);
    connect(m_document->undostack(), SIGNAL(cleanChanged(bool)), m_ui->action_stowage_save, SLOT(setDisabled(bool)));
    m_ui->action_stowage_save->setDisabled(m_document->undostack()->isClean());
    connect(m_document->undostack(), SIGNAL(indexChanged(int)), SLOT(update_autosave(int)));
    connect(m_document->undostack(), SIGNAL(cleanChanged(bool)), SLOT(set_window_title()));
    connect(m_ui->schedule_view, SIGNAL(call_focused(const ange::schedule::Call*)), SLOT(set_window_title()));
    connect(m_document, SIGNAL(stowage_filename_changed(QString)), SLOT(set_window_title()));
    set_window_title();
    connect(m_document,SIGNAL(async_save_done()),SLOT(autosave_finished()));
    connect(m_document,SIGNAL(async_save_error()),SLOT(autosave_failed()));
    connect(m_ui->action_micro_plan, &QAction::triggered, m_toolManager, &ToolManager::setMicroMode);
    connect(m_ui->action_macro_plan, &QAction::triggered, m_toolManager, &ToolManager::setMacroMode);

    // set row labels
    connect(m_ui->actionRow_name, SIGNAL(triggered(bool)), SLOT(row_label_changed()));
    connect(m_ui->actionRow_max_height, SIGNAL(triggered(bool)), SLOT(row_label_changed()));
    connect(m_ui->actionRow_max_weight, SIGNAL(triggered(bool)), SLOT(row_label_changed()));
    connect(m_ui->actionRow_remaining_weight, SIGNAL(triggered(bool)), SLOT(row_label_changed()));
    connect(m_ui->actionShow_Equipment_ID, SIGNAL(triggered(bool)), SLOT(show_equipment_id_changed(bool)));

    NonModalDialogHelper* findHelper = new NonModalDialogHelper(DocumentPoolDialog<FindContainersDialog>::ptr(selection_pool(), m_document, this), m_document);
    connect(m_ui->actionFindContainers, SIGNAL(triggered(bool)), findHelper, SLOT(raise()));
    NonModalDialogHelper* modifyHelper = new NonModalDialogHelper(DocumentPoolDialog<ModifyContainersDialog>::ptr(selection_pool(), m_document, this), m_document);
    connect(m_ui->action_modify_containers, SIGNAL(triggered(bool)), modifyHelper, SLOT(raise()));
    NonModalDialogHelper* baplieComparatorHelper = new NonModalDialogHelper(DocumentDialog<BaplieComparatorDialog>::ptr(m_document, this, QSharedPointer<BaplieComparatorSetupper>(new BaplieComparatorSetupper(m_ui->schedule_view))),m_document);
    connect(m_ui->actionCompare_with_BAPLIE, &QAction::triggered, baplieComparatorHelper, &NonModalDialogHelper::raise);
    m_toolManager->setMicroMode(); // ensure everything is in micro mode on document load
    m_restowEditor->documentReset(m_document, m_selected_pool);
    connect(m_document->passiveNotifications(), &PassiveNotifications::warningAdded, this, &MainWindow::showWarning);
  }
  connect(m_ui->macro_stow_toolbar, SIGNAL(call_selected(ange::schedule::Call*)), m_bay_plan, SLOT(slot_painted_call_changed(ange::schedule::Call*)));
  connect(m_ui->action_vessel_in_2_rows, SIGNAL(triggered(bool)), m_ui->vessel_view, SLOT(show_2_rows(bool)));
  Q_FOREACH(vessel_graphics_view_t* vesselview, m_extra_vessel_views){
    vesselview->setScene(m_bay_plan);
    vesselview->setBundleHelper(m_bundlehelper);
    connect(m_ui->action_vessel_in_2_rows, SIGNAL(triggered(bool)), vesselview, SLOT(show_2_rows(bool)));
  }
  m_ui->cranesplit_view->set_document(m_document);
  m_ui->stresses_view->set_document(m_document);

    ContainerViewItemDelegates::setupDelegates(m_ui->all_containers_table_view, m_document);
    ContainerViewItemDelegates::setupDelegates(m_ui->selected_containers_table_view, m_document);

  // This is a bit silly, but it is the only way I have been able to set the show_in_2_rows correctly
  m_ui->action_vessel_in_2_rows->setChecked(false);
  const bool use_2_rows_per_vessel = (settings.value("rows per vessel").toInt() == 2);
  if (m_ui->action_vessel_in_2_rows->isChecked() != use_2_rows_per_vessel) {
    // If incorrect value, toggle it. This will also activate any signals
    m_ui->action_vessel_in_2_rows->activate(QAction::Trigger);
  }
  const bool show_equipment_number = settings.value("view/equipment number", false).toBool();
  show_equipment_id_changed(show_equipment_number);

  Q_FOREACH(RecapView* recap_view, m_recap_views) {
    recap_view->setDocument(doc, m_selected_pool, m_ui->all_containers_table_view->selectionModel());
    recap_view->setCurrentCall(m_ui->schedule_view->current_call());
    recap_view->setBundleHelper(m_bundlehelper);
  }
  m_selection_summary_label.setSelectionPool(selection_pool(), m_document->containers());
  doc->userconfiguration()->setUsePivotPortBasedColoring(
                                                  settings.value("view/use pivot port based coloring", true).toBool());
  m_bay_plan->setLinkForeAft(settings.value("link_fore_aft_slots", true).toBool());
  m_ui->tankTable->setCurrentCall(m_ui->schedule_view->current_call());
  delete oldDocument;
}

void MainWindow::row_label_changed() {
  if(m_ui->actionRow_name->isChecked()){
    m_bay_plan->slot_row_labels_changed(mode_enum_t::NAME);
  } else if (m_ui->actionRow_max_height->isChecked()){
    m_bay_plan->slot_row_labels_changed(mode_enum_t::MAX_HEIGHT);
  } else if (m_ui->actionRow_max_weight->isChecked()){
    m_bay_plan->slot_row_labels_changed(mode_enum_t::MAX_WEIGHT);
  } else if (m_ui->actionRow_remaining_weight->isChecked()) {
    m_bay_plan->slot_row_labels_changed(mode_enum_t::REMAINING_WEIGHT);
  }
}

void MainWindow::add_extra_vesselview(QRectF rect) {
  QDockWidget* vesseldock = new QDockWidget(this);
  vesseldock->setAttribute(Qt::WA_DeleteOnClose);
  QWidget* vesselwidget = new QWidget(vesseldock);
  vessel_graphics_view_t* vesselview = new vessel_graphics_view_t(vesselwidget);
  vesselview->set_selection_pool(m_selected_pool);
  vesselview->set_document(m_document);
  vesselview->setScene(m_bay_plan);
  vesselview->setToolManager(m_toolManager);
  connect(vesselview, SIGNAL(tool_applied(QRectF,tool_t*)), m_bay_plan, SLOT(Slotool_applied(QRectF,tool_t*)));
  connect(vesselview, SIGNAL(call_activated(const ange::schedule::Call*)), m_ui->schedule_view, SLOT(select(const ange::schedule::Call*)) );
  connect(m_ui->schedule_view, SIGNAL(call_focused(const ange::schedule::Call*)), vesselview, SLOT(focus_call(const ange::schedule::Call*)));
  connect(vesselview, &vessel_graphics_view_t::destroyed, this, &MainWindow::removeVesselView);
  QHBoxLayout* layout = new QHBoxLayout;
  layout->addWidget(vesselview);
  vesselwidget->setLayout(layout);
  vesselview->setBundleHelper(m_bundlehelper);
  vesseldock->setWidget(vesselwidget);
  vesseldock->setFloating(true);
  vesseldock->show();
  if(rect!=QRectF()) {
    vesselview->fitInView(rect,Qt::KeepAspectRatio);
    vesselview->set_no_zoom_and_pan(true);
  }
  m_extra_vessel_views << vesselview;
  connect(m_toolManager, &ToolManager::toolChanged, vesselview, &vessel_graphics_view_t::set_tool);
}

RecapView*  MainWindow::add_recap() {
  QString object_base_name = QDateTime::currentDateTime().time().toString("hh:mm:ss.zzz");
  RecapView* recap =  add_recap(object_base_name,tr("Recap"));
  recap->setupStandardRecap();
  return recap;
}

RecapView* MainWindow::add_recap(QString name, QString username) {
  QDockWidget* recap_view_dock = new QDockWidget(this);
  RecapView* recap_view = new RecapView(username, recap_view_dock);
  recap_view_dock->installEventFilter(recap_view);
  connect(recap_view, &QObject::destroyed, this, &MainWindow::remove_recap);
  connect(recap_view, &RecapView::nameChanged, this, &MainWindow::updateShowPanelMenu);
  recap_view->setRecapManager(this);
  recap_view->setDocument(document(), m_selected_pool, m_ui->all_containers_table_view->selectionModel());
  recap_view->setCurrentCall(m_ui->schedule_view->current_call());
  recap_view_dock->setWidget(recap_view);
  recap_view_dock->setAttribute(Qt::WA_DeleteOnClose);
  recap_view_dock->setWindowTitle(username);
  recap_view_dock->resize(recap_view->sizeHint());
  m_recap_views << recap_view;
  recap_view_dock->setObjectName(QString("recap_%1_dock").arg(name));
  recap_view->setObjectName(QString("recap_%1_view").arg(name));
  recap_view->setCurrentCall(m_ui->schedule_view->current_call());
  recap_view_dock->setFloating(true);
  recap_view_dock->move(QCursor::pos() - (style()->pixelMetric(QStyle::PM_DockWidgetFrameWidth)+1) * QPoint(1,1));
  recap_view_dock->show();
  recap_view->setBundleHelper(m_bundlehelper);
  addDockWidget(Qt::BottomDockWidgetArea, recap_view_dock);
  recap_view_dock->setFloating(true);
  updateShowPanelMenu();
  return recap_view;
}

QList<RecapView*> MainWindow::recapViews() {
    return m_recap_views;
}

void MainWindow::show_equipment_id_changed(bool state) {
  m_bay_plan->set_show_equipment_id(state);
  if(state){
    m_ui->all_containers_table_view->showColumn(static_cast<int>(container_list_t::EQUIPMENT_NO_COL));
    m_ui->selected_containers_table_view->showColumn(static_cast<int>(container_list_t::EQUIPMENT_NO_COL));
  } else {
    m_ui->all_containers_table_view->hideColumn(static_cast<int>(container_list_t::EQUIPMENT_NO_COL));
    m_ui->selected_containers_table_view->hideColumn(static_cast<int>(container_list_t::EQUIPMENT_NO_COL));
  }
  m_ui->actionShow_Equipment_ID->setChecked(state);
  if (SettingsDialog* setting_dialog =  m_settings_dialog.data()) {
    setting_dialog->set_show_equipment_id(state);
  }
  QSettings settings;
  settings.setValue("view/equipment number", state);
}

bool MainWindow::really_load(){
  bool cancel=false;
  if (m_document && !m_document->undostack()->isClean()) {
    reallydialog_t reallydialog(this, QString("Really load?"), QString("Do you want to save before loading a new file?\n\nIf you do not save, your changes will be lost."));
    int result = reallydialog.exec();
    if (result == reallydialog_t::Cancel) {
      cancel=true;
    }
    if (result == reallydialog_t::Save) {
      stowage_save();
    }
    if (!cancel) {
      cleanup_autosave();
    }
  }
  return !cancel;
}

void MainWindow::stowage_open() {
    QStringList filters;
    filters << "Known files (*.sto *.edi *.baplie *.txt)";
    filters << "Stowage documents (*.sto)";
    filters << "BAPLIE files (*.edi *.baplie *.txt)";
    filters << "JSON bundles (*.json *.json.gz)";
    filters << "All Files (*)";
    QString fileName = QFileDialog::getOpenFileName(this, "Open stowage", Settings::stowageDirectory(), filters.join(";;"));
    if (fileName.isNull()) {
        return;
    }
    Settings::setStowageDirectory(QFileInfo(fileName).absolutePath());
    stowage_open(fileName);
}

bool MainWindow::stowage_open(const QString& file) {
    if(really_load()){
        if(QFile::exists(file+QLatin1Char('~'))) {
            if(ask_recover_autosave(file+QLatin1Char('~'))) {
                return false;
            }
        }
        if(really_stowage_open(file)) {
            update_recent_opened(file);
            Q_ASSERT(!m_document->vesselFileData().isEmpty());
            return true;
        }
    }
    return false;
}

bool MainWindow::really_stowage_open(QString file) {
  try {
    override_cursor_handler_t overridecursor(qApp,QCursor(Qt::WaitCursor));
    OnDiskData data = FileOpener::openFile(file);
    data.document->setPluginManager(m_pluginManager);
    set_document(data.document);
    if(data.currentCall) {
        m_ui->schedule_view->select(data.currentCall);
    } else {
        // fallback
        m_ui->schedule_view->select(m_document->schedule()->calls().size()>2?1:0);
    }
    return true;

  } catch (std::runtime_error& error) {
    error_popup(this, tr("Open stowage failed"),
                tr("Open stowage '%1' failed, see details below").arg(file),
                error);
    return false;
  }
}

void MainWindow::update_recent_opened(const QString& fileName) {
    if (!fileName.isEmpty()) {
        QFileInfo fileInfo(fileName);
        m_recent_files.removeAll(fileName); // Backwards compatability cleanup, remove after 2015-06-01
        m_recent_files.removeAll(fileInfo.absoluteFilePath());
        m_recent_files.prepend(fileInfo.absoluteFilePath());
    }
    while (m_recent_files.size() > 5) {
        m_recent_files.removeLast();
    }
    if (!m_recent_menu) {
        m_recent_menu = new QMenu(tr("Recent stowages"), m_ui->menuFile);
        m_ui->menuFile->insertMenu(m_ui->action_stowage_save, m_recent_menu);
    }
    m_recent_menu->clear();
    Q_FOREACH (const QString& recentFileName, m_recent_files) {
        QAction* add_action = m_recent_menu->addAction(recentFileName, m_recent_mapper, SLOT(map()));
        m_recent_mapper->setMapping(add_action, recentFileName);
    }
    m_recent_menu->setDisabled(m_recent_menu->isEmpty());
}

bool MainWindow::stowage_save_as() {
  QFileInfo stofile = QFileInfo(m_document->stowage_filename());
  QString filename;
  if(stofile.exists()){
    filename = QFileDialog::getSaveFileName(this,tr("Save as"),stofile.absoluteFilePath(),tr("Stowage file (*.sto)"));
  } else {
    filename = QFileDialog::getSaveFileName(this,tr("Save as"),stofile.dir().absolutePath(),tr("Stowage file (*.sto)"));
  }
  if(!filename.isEmpty()){
    m_document->stowage_filename(filename);
    return stowage_save();
  }
  return false;
}

bool MainWindow::stowage_save() {
  try {
    QString filename = m_document->stowage_filename();
    if (filename.isEmpty()) {
      return stowage_save_as(); // stowage_save_as() will handle undostack()->setClean()
    } else {
      m_document->save_stowage(filename, current_call());
      update_recent_opened(filename);
      m_document->undostack()->setClean();
      statusBar()->showMessage(QString("Document %1 saved").arg(filename),15000);
      return true;
    }
  } catch (std::runtime_error& error) {
    error_popup(this, tr("Save file failed"),
                tr("Save stowage failed, see details below"),
                error);
    return false;
  }
}

void MainWindow::import_vessel() {
    ::importVessel(m_document, this);
}

void MainWindow::generate_containerlist() {
    GenerateContainersDialog dialog(m_document->schedule(), current_call(), m_document->schedule_model(), this);
    if (dialog.exec() == QDialog::Accepted) {
        QList<ange::angelstow::ScheduledContainer> containers = dialog.containers();
        container_list_change_command_t* cmd = new container_list_change_command_t(m_document);
        Q_FOREACH (const ange::angelstow::ScheduledContainer& container, containers) {
            cmd->add_container(container.m_container, container.m_portOfLoad, container.m_portOfDischarge);
        }
        cmd->setText(QString("Generate %1 containers").arg(containers.size()));
        m_document->undostack()->push(cmd);
    }
}

void MainWindow::change_current_call(const Call* call) {
    if(!call) {
        return;
    }
  Q_FOREACH(RecapView* recap_view, m_recap_views) {
    recap_view->setCurrentCall(call);;
  }
  m_selected_pool->clear();
  m_ui->tankWidget->setCurrentCall(call);
  m_ui->tankTable->setCurrentCall(call);
  m_ui->stresses_view->set_current_call(call);
  m_ui->cranesplit_view->set_current_call(call);
  m_bundlehelper->setCurrentCall(call);
  m_problemSortFilterProxyModel->setCurrentCall(call);
  m_document->tankModel()->setCurrentCall(call);
}

// Clear all master planed moves loaded in first call or later
static void clearMasterPlanned(document_t* document, QList<Call*> callsToMasterPlan, merger_command_t* mergerCommand) {
    if (callsToMasterPlan.isEmpty()) {
        return;
    }

    QTime timer;
    timer.start();
    const Call* callToClearFrom = callsToMasterPlan.first();
    int unstowed = 0;
    Q_FOREACH (const Container* container, document->containers()->list()) {
        QList<const container_move_t*> moves = document->stowage()->moves(container);
        const container_move_t* lastKeptMove = 0;
        QMutableListIterator<const container_move_t*> it(moves);
        while (it.hasNext()) {
            const container_move_t* move = it.next();
            if (!(move->type() == MasterPlannedType && !move->call()->lessThan(callToClearFrom))) {
                lastKeptMove = move;
                it.remove();
            }
        }
        if (!moves.isEmpty()) {
            QList<container_move_t> appended;
            if (lastKeptMove && lastKeptMove->slot()) {  // Last kept move is load move
                const container_move_t* firstRemoved = moves.first();
                appended << container_move_t(firstRemoved->container(), 0, firstRemoved->call(), firstRemoved->type());
            }
            container_stow_command_t* stowCommand = new container_stow_command_t(document);
            stowCommand->change_moves(container, moves, appended);
            mergerCommand->push(stowCommand);
            ++unstowed;
        }
    }
    qDebug() << "Unstowed" << unstowed << "containers in" << timer.elapsed() << "ms";
}

void MainWindow::slot_run_stowage_plugin() {
    ange::angelstow::MasterPlanningPlugin* stowagePlugin = m_pluginManager->masterPlanningPlugin();
    if (!stowagePlugin) {
        QMessageBox::warning(0L, tr("Error"), tr("Failed to run stowage plugin"));
        return;
    }

    override_cursor_handler_t overrideCursor(qApp, Qt::WaitCursor);
    QSharedPointer<stowage_signal_lock_t> signalLock(m_document->stowage()->prepare_for_major_stowage_change());
    const Schedule* schedule = m_document->schedule();
    QList<Call*> callsToMasterPlan = schedule->between(m_document->macro_stowage()->firstModifiedCall(), schedule->calls().last());
    stowagePlugin->setUpMacroPlan(m_document->macro_stowage());
    try {
        QScopedPointer<merger_command_t> mergerCommand(new merger_command_t("Master Plan"));
        clearMasterPlanned(m_document, callsToMasterPlan, mergerCommand.data());
        Q_FOREACH (const Call* call, callsToMasterPlan) {
            QScopedPointer<IStower> stowing(m_document->stowage()->create_stowing());
            stowagePlugin->runStow(call, stowing.data());
            mergerCommand->push(stowing->takeCommand());
            m_document->macro_stowage()->setFirstModifiedCall(m_document->schedule()->calls().back());
        }
        m_document->undostack()->push(mergerCommand.take());
    } catch (std::exception& e) {
      QMessageBox::warning(0L, tr("Error"), tr("Failed to run stowage plugin: %1").arg(e.what()));
    }
}

void MainWindow::set_window_title() {
  const Vessel* vessel = m_document->vessel();
  const Call* current_call = m_bay_plan->current_call();
  const QString stowage_filename = m_document->stowage_filename();
  QString windowtitle(tr("Angelstow %1 - %2 - %3/%4 - %5%6")
    .arg(ANGESTOW_VERSION) // %1
    .arg(vessel->name()) // %21
    .arg(current_call ? current_call->uncode() : QString()) // %3
    .arg(current_call ? current_call->voyageCode() : QString()) // %4
    .arg(!stowage_filename.isEmpty() ? stowage_filename : "Unnamed stowage") // %5
    .arg(m_document->undostack()->isClean() ? "" : "*") // %6
  );
  setWindowTitle(windowtitle);
}

void MainWindow::closeEvent(QCloseEvent* event) {
  if (m_document && !m_document->undostack()->isClean()) {
    reallydialog_t reallydialog(this, QString("Really quit?"), QString("Do you want to save before quitting?\n\nIf you do not save, your changes will be lost."));
    int result = reallydialog.exec();
    if (result == reallydialog_t::Cancel) {
      event->ignore();
    }
    if (result == reallydialog_t::Save) {
      event->setAccepted(stowage_save());
    }
  }

  if(event->isAccepted()) {
    save_settings();
    cleanup_autosave();
  }
}

void MainWindow::delete_selected_containers() {
    override_cursor_handler_t cursor(qApp, Qt::WaitCursor);
    QList<Container*> containers;
    QList<const Container*> constContainers = m_selected_pool->containers();
    Q_FOREACH(const Container* container, constContainers) {
        containers << m_document->containers()->get_container_by_id(container->id());
    }
    m_document->undostack()->push(ContainerDeleter::deleteContainers(m_document, m_selected_pool, containers));
}

const ange::schedule::Call* MainWindow::current_call() const {
  return m_ui->schedule_view->current_call();
}

void MainWindow::import_edifact_wizard() {
  import_edifact_wizard_t wizard(m_document, m_selected_pool, m_ui->schedule_view, this, Qt::Dialog);
  wizard.exec();
}

void MainWindow::display_settings() {
  if (m_settings_dialog) {
    m_settings_dialog.data()->raise();
  } else {
    m_settings_dialog = new SettingsDialog;
    connect(m_settings_dialog.data(), SIGNAL(show_2_rows(bool)), m_ui->vessel_view, SLOT(show_2_rows(bool)));
    Q_FOREACH(vessel_graphics_view_t* vesselview, m_extra_vessel_views){
      connect(m_settings_dialog.data(), SIGNAL(show_2_rows(bool)), vesselview, SLOT(show_2_rows(bool)));
    }
    connect(m_settings_dialog.data(), SIGNAL(show_equipment_number(bool)), SLOT(show_equipment_id_changed(bool)));
    connect(m_settings_dialog.data(), SIGNAL(display_bulkheads_changed(bool)), m_bay_plan, SLOT(display_bulkheads_for_all_calls(bool)));
    connect(m_settings_dialog.data(), SIGNAL(display_visibilityline_changed(bool)), m_bay_plan, SLOT(display_visibilityline_for_all_calls(bool)));
    connect(m_settings_dialog.data(), SIGNAL(hide_nonactive_changed(bool)), m_ui->tankWidget,SLOT(hideNonActiveCallsChanged(bool)));
    connect(m_settings_dialog.data(), SIGNAL(hide_nonactive_changed(bool)), m_ui->cranesplit_view,SLOT(hide_non_active_calls_changed(bool)));
    connect(m_settings_dialog.data(), SIGNAL(hide_nonactive_changed(bool)), m_ui->stresses_view,SLOT(hide_non_active_calls_changed(bool)));
    connect(m_settings_dialog.data(), SIGNAL(hide_nonactive_changed(bool)), m_bay_plan,SLOT(hide_non_active_calls_changed(bool)));
    connect(m_settings_dialog.data(), SIGNAL(hide_nonactive_changed(bool)), m_problemSortFilterProxyModel, SLOT(setFilteringEnabled(bool)));
    connect(m_settings_dialog.data(), SIGNAL(displayContainerWeightChanged(bool)), m_bay_plan, SLOT(changeDisplayContainerWeights(bool)));
    connect(m_settings_dialog.data(), SIGNAL(usePivotBasedPortColoringChanged(bool)),
            m_document->userconfiguration(), SLOT(setUsePivotPortBasedColoring(bool)));
    connect(m_settings_dialog.data(), SIGNAL(newColorFileSelected()),
            m_document->userconfiguration(), SIGNAL(colorsChanged()));
    connect(m_settings_dialog.data(), SIGNAL(linkForeAftSlotsChanged(bool)), m_bay_plan, SLOT(setLinkForeAft(bool)));
    m_settings_dialog.data()->setAttribute(Qt::WA_DeleteOnClose);
    m_settings_dialog.data()->show();
  }
}

void MainWindow::remove_recap(QObject* recap) {
    if (m_inDestructor) {
        return; // QWidgets delete children before they disconnect, avoid crashing on finding half deleted RecapView
    }
    m_recap_views.removeAll(static_cast<RecapView*>(recap));
    updateShowPanelMenu();
}

void MainWindow::removeVesselView(QObject* vesselView) {
    m_extra_vessel_views.removeAll(static_cast<vessel_graphics_view_t*>(vesselView));
}

void MainWindow::select_next_call() {
  if(is_after(current_call())) {
    return;
  }
  Schedule* schedule = m_document->schedule();
  int index = schedule->indexOf(current_call());
  Call* next = schedule->at(index+1);
  m_ui->schedule_view->select(next);
}

void MainWindow::select_previous_call() {
  if(is_befor(current_call())) {
    return;
  }
  Schedule* schedule = m_document->schedule();
  int index = schedule->indexOf(current_call());
  Call* next = schedule->at(index-1);
  m_ui->schedule_view->select(next);
}

void MainWindow::keyPressEvent(QKeyEvent* key) {
  QWidget::keyPressEvent(key);
  if (key->key() == Qt::Key_Shift) {
    // If shift is held and pool is non-empty, force the select tool
    m_toolManager->setAutoToolSelect(m_selected_pool->empty(), key->modifiers());
  }
}

void MainWindow::keyReleaseEvent(QKeyEvent* key) {
  QWidget::keyReleaseEvent(key);
  if (key->key() == Qt::Key_Shift) {
    // Remove any override
    m_toolManager->setAutoToolSelect(m_selected_pool->empty(),key->modifiers());
  }
}

void MainWindow::autosave() {
  qDebug() << __func__;
  QString autosav_path = m_document->autosave_filename();
  if(autosav_path.isEmpty()) {
    autosav_path=DEFAULT_UNTITLED_AUTOSAVE_FILE;
  }
  m_document->start_save_stowage_async(autosav_path, current_call());
  m_autosave_timer.stop();
  statusBar()->showMessage("Autosaving");
}

void MainWindow::autosave_finished() {
  statusBar()->showMessage("Autosaving done",5000);
}

void MainWindow::autosave_failed() {
  statusBar()->showMessage("Autosaving failed",10000);
}

void MainWindow::cleanup_autosave(QString autosavepath) {
  if(autosavepath.isEmpty()) {
    autosavepath = m_document->autosave_filename();
    if(autosavepath.isEmpty()) {
      autosavepath=DEFAULT_UNTITLED_AUTOSAVE_FILE;
    }
  }
  if(!autosavepath.isEmpty()) {
    qDebug() << "removing" << autosavepath;
    QFile::remove(autosavepath);
  }
}

void MainWindow::update_autosave(int idx) {
  if(idx==0) {
    m_autosave_timer.stop();
    cleanup_autosave();
  } else {
    if(!m_autosave_timer.isActive()) {
      qDebug() << "starting autosave timer";
      m_autosave_timer.start();
    }
  }
}

void MainWindow::showGenerateReportsDialog() {
    if(!sanity_in_export_formats()) {
        return;
    }
    ReportGenerator dialog(m_document,m_selected_pool,m_ui->schedule_view->current_call(),this);
    dialog.exec();
}

void MainWindow::forceClassificationSociety() {
    QAction* action = qobject_cast<QAction*>(sender());
    Q_ASSERT(action);
    m_document->documentInterface()->setProperty("debug.LashingPlugin.forceClassificationSociety", action->data());
}

QAction* MainWindow::addForceClassificationSocietyAction(QMenu* forceClassificationSocietyMenu, const QString& society) {
    QAction* action = forceClassificationSocietyMenu->addAction(society);
    action->setData(society);
    connect(action, &QAction::triggered, this, &MainWindow::forceClassificationSociety);
    return action;
}

void MainWindow::addDeveloperMenu() {
    QToolBar* crashToolBar = addToolBar("Crash");
    crashToolBar->setObjectName("crashToolBar");
    crashToolBar->close();
    QAction* printCrash = crashToolBar->addAction("CRASH!!!");
    connect(printCrash, SIGNAL(triggered(bool)), this, SLOT(crash(bool)));

    QMenu* menu = m_ui->menubar->addMenu("&Developer");
    QAction* stabilityReportAction = menu->addAction("Show &Stability Report");
    connect(stabilityReportAction, SIGNAL(triggered(bool)), SLOT(showStabilityReport()));
    QMenu* forceClassificationSocietyMenu = menu->addMenu("Force Classification Society");
    addForceClassificationSocietyAction(forceClassificationSocietyMenu, "Don't force")->setData(QString());
    addForceClassificationSocietyAction(forceClassificationSocietyMenu, "NONE");
    addForceClassificationSocietyAction(forceClassificationSocietyMenu, "DNV");
    addForceClassificationSocietyAction(forceClassificationSocietyMenu, "GL");
    addForceClassificationSocietyAction(forceClassificationSocietyMenu, "LR");
    QAction* printDebugLine = menu->addAction("&Print a line on qDebug");
    connect(printDebugLine,SIGNAL(triggered(bool)), SLOT(writeDebugLine(bool)));
    QAction* debugDumpMacroStowageAction = menu->addAction("&Write macro stowage on qDebug");
    connect(debugDumpMacroStowageAction, SIGNAL(triggered()), SLOT(debugDumpMacroStowage()));
    QAction* debugDumpToolManagerStateAction = menu->addAction("&Write tool manager state on qDebug");
    connect(debugDumpToolManagerStateAction, SIGNAL(triggered()), SLOT(debugDumpToolManagerState()));
    QAction* debugExportSettings = menu->addAction("Export settings to file");
    connect(debugExportSettings, SIGNAL(triggered(bool)), SLOT(exportSettings()));
    QAction* exportMasterPlanningDebugInfo = menu->addAction("Export &master planning dbg info");
    connect(exportMasterPlanningDebugInfo, SIGNAL(triggered(bool)), SLOT(exportMasterPlanningDbgInfo()));
}

void MainWindow::showStabilityReport() {
    DebugStabilityReport report(m_document, m_pluginManager, current_call());
    report.showWindow(this);
}

void MainWindow::writeDebugLine(bool ) {
    qDebug() << " ---------------------------- ";
}

void MainWindow::forwardToAutoToolHandling(bool selectionempty) {
    m_toolManager->setAutoToolSelect(selectionempty, qApp->keyboardModifiers());
}

void MainWindow::debugDumpMacroStowage() {
    MacroPlan* macrostowage = m_document->macro_stowage();
    Q_FOREACH(const BaySlice* slice, m_document->vessel()->baySlices()) {
        Q_FOREACH(const Compartment* compartment, slice->compartments()) {
            QStringList calllist;
            Q_FOREACH(const MacroPlan::MacroStowageElement element, macrostowage->sequenceForCompartment(compartment)) {
                calllist << element.load->uncode() << element.discharge->uncode();
            }
            qDebug() << compartment << calllist.join(",");
        }
    }
}

void MainWindow::debugDumpToolManagerState() {
    qDebug() << "current tool" << m_toolManager->currentTool()->name();
    qDebug() << "is macro stowage" << (m_toolManager->currentMode() == mode_enum_t::MACRO_MODE);
    qDebug() << "is micro stowage" << (m_toolManager->currentMode() == mode_enum_t::MICRO_MODE);
}

void MainWindow::exportSettings() {
    QString filename = QFileDialog::getSaveFileName(this, "Debug export settings to file");
    if(!filename.isEmpty()) {
        QSettings exportedSettings(filename, QSettings::IniFormat);
        QSettings appSettings;
        Q_FOREACH(const QString& key, appSettings.allKeys() ) {
            exportedSettings.setValue(key, appSettings.value(key));
        }
    }
}

void MainWindow::exportMasterPlanningDbgInfo() {
    MasterPlanningReport report(m_pluginManager, current_call());
    report.showWindow(this);
}

void MainWindow::openAboutDialog() {
    AboutDialog about(m_pluginManager);
    about.exec();
}

#include "mainwindow.moc"
