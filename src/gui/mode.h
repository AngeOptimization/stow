#ifndef MODEENUM_H
#define MODEENUM_H
#include <ange/schedule/call.h>

namespace ange {
  namespace schedule {
    class Call;
  }
}

/**
 * Mode of GUI. In a class to simulate class enums
 */
struct mode_enum_t {
    enum mode_t {
        MICRO_MODE,
        MACRO_MODE
    };
    enum row_label_mode_t {
        NAME,
        MAX_WEIGHT,
        MAX_HEIGHT,
        REMAINING_WEIGHT
    };
    enum ScanpPlanType {
        LOAD,
        DISCHARGE,
    };
};

struct PrintMode {
    bool grayOddSlots;
    bool grayKilledSlots;
    bool grayIllegalSlots;
    bool grayRemainOnboard;
    bool grayNotMovedInNextCall;
    bool notPrinting;
    mode_enum_t::ScanpPlanType scanPlanType;
    bool whiteContainers;
    bool displayContainerWeight;
    bool displayDischargeCallLetter;
    bool highDetailPrint;
    QHash<const ange::schedule::Call*, QString> dischargeCallLetters;

    PrintMode() : grayOddSlots(true), grayKilledSlots(true), grayIllegalSlots(true),
        grayRemainOnboard(false), grayNotMovedInNextCall(false), notPrinting(true), scanPlanType(mode_enum_t::LOAD),
        whiteContainers(false), displayContainerWeight(false), displayDischargeCallLetter(false), highDetailPrint(false)
    {
        // Do nothing
    }

    static PrintMode printing() {
        PrintMode mode;
        mode.grayOddSlots = false;
        mode.grayKilledSlots = false;
        mode.grayIllegalSlots = false;
        mode.notPrinting = false;
        return mode;
    }
};

#endif // MODEENUM_H
