#ifndef ERROR_POPUP_H
#define ERROR_POPUP_H
#include <QMessageBox>
#include <stdexcept>

class QMainWindow;

inline static void error_popup(QWidget* parent, const QString& title, const QString& text, const std::runtime_error& error) {
  QMessageBox box(parent);
  box.setWindowTitle(title);
  box.setText(text);
  box.setDetailedText(error.what());
  box.setStandardButtons(QMessageBox::Cancel);
  box.setDefaultButton(QMessageBox::Cancel);
  box.exec();
}

#endif // ERROR_POPUP_H