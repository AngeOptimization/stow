#include "delayedrestorer.h"

#include <QMainWindow>
#include <QTimer>

DelayedRestorer::DelayedRestorer(QMainWindow* mainWindow, const QByteArray& geometry, const QByteArray& state)
  : QObject(), m_mainWindow(mainWindow), m_geometry(geometry), m_state(state)
{
    // Empty
}

void DelayedRestorer::timedRestore(QMainWindow* mainWindow, const QByteArray& geometry, const QByteArray& state) {
    // 0 ms will place window based on size without decorations, this will make the window creep up on multiple open/close
    QTimer::singleShot(10, new DelayedRestorer(mainWindow, geometry, state), SLOT(restore1()));
}

void DelayedRestorer::restore1() {
    m_mainWindow->restoreGeometry(m_geometry);
    // Tested that 0 ms will squash dockwidgets based on the normal size of the window even if it is maximized
    // 10 ms will allow the problem sometimes
    // I have not seen the problem with 20 ms
    QTimer::singleShot(20, this, SLOT(restore2()));
}

void DelayedRestorer::restore2() {
    m_mainWindow->restoreState(m_state);
    deleteLater();
}

#include "delayedrestorer.moc"
