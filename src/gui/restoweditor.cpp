#include "restoweditor.h"

#include "container_leg.h"
#include "container_move.h"
#include "container_stow_command.h"
#include "createproblemcommand.h"
#include "document.h"
#include "mainwindow.h"
#include "merger_command.h"
#include "passivenotifications.h"
#include "pool.h"
#include "schedule_view.h"
#include "stowage.h"
#include "stowarbiter.h"

#include <ange/containers/container.h>
#include <ange/vessel/slot.h>

#include <QList>

using ange::angelstow::Problem;
using ange::containers::Container;
using ange::schedule::Call;

RestowEditor::RestowEditor(QObject* parent)
  : QObject(parent), m_scheduleView(0), m_document(0), m_pool(0), m_containerLegs(0)
{
    setObjectName("RestowEditor");
}

void RestowEditor::setScheduleView(schedule_view_t* scheduleView) {
    m_scheduleView = scheduleView;
}

void RestowEditor::documentReset(document_t* document, pool_t* pool) {
    m_document = document;
    m_pool = pool;
    m_containerLegs = m_document->stowage()->container_routes();
}

bool RestowEditor::callIsInsideContainerSchedule(const Container* container, const Call* call) {
    return m_containerLegs->portOfLoad(container)->lessThan(call)
        && call->lessThan(m_containerLegs->portOfDischarge(container));
}

namespace {

/**
 * Helper that finds the current move and its neighbours in a move list.
 */
struct CurrentMoveFinder {
    CurrentMoveFinder(QList<const container_move_t*> moves, const Call* currentCall);
    const container_move_t* previous;
    const container_move_t* current;
    const container_move_t* next;
};

CurrentMoveFinder::CurrentMoveFinder(QList<const container_move_t*> moves, const Call* currentCall)
  : previous(0), current(0), next(0)
{
    QListIterator<const container_move_t*> it(moves);
    while (it.hasNext()) {
        const container_move_t* move = it.next();
        if (move->call() == currentCall) {
            current = move;
            if (it.hasNext()) {
                next = it.next();
            }
            return;
        }
        previous = move;
    }
    previous = 0;
}

} // anonymous namespace

void RestowEditor::restowCancel() {
    const Call* currentCall = m_scheduleView->current_call();
    QList<const Container*> containersThatMightHaveRestows;
    Q_FOREACH (const Container* container, m_pool->containers()) {
        if (callIsInsideContainerSchedule(container, currentCall)) {
            containersThatMightHaveRestows << container;
        }
    }
    stowage_t* stowage = m_document->stowage();

    merger_command_t* merger = new merger_command_t("Cancel restow of selected containers");
    int notificationCount = 0;

    /*-
     * Types of moves:
     *   L = load move
     *   D = discharge move
     *   - = no move (end of moves list)
     *
     * There exists the following possible combinations of restows:
     *   move.previous  move.current  move.next  Notes
     *          L            L           L/D     Is changed to discharge in first loop.
     *         D/-           L            D      Must also remove move.next.
     *         D/-           L            L
     *          L            D            -      Must create new discharge move.
     *          L            D            L
     *
     * When the restow is canceled it is the current move that is removed.
     * When move.previous is a load move we must use an arbitor to check for illegal stowages.
     */

    /*
     * First loop handles all the load restows.
     * Discharging (D/-, L, L/D) is easy as that can never fail as there is always room on the quay.
     * Changing all current load moves in the (L, L, L/D) case to discharge moves is also safe, this is done first in
     * order to clear the vessel such that two (or more) swapped containers will not block each other.
     */
    Q_FOREACH (const Container* container, containersThatMightHaveRestows) {
        CurrentMoveFinder move(stowage->moves(container), currentCall);
        if (!move.current) {
            continue;  // If there is no move in this call there can be no restow, skip container
        }

        if (!move.current->slot()) {
            continue;  // Not a load move, skip for now
        }
        QList<const container_move_t*> movesToRemove;
        QList<container_move_t> movesToAppend;
        movesToRemove << move.current;
        if (move.previous && move.previous->slot()) {  // move.previous is a load
            movesToAppend << container_move_t(container, 0, move.current->call(), move.current->type());
        }
        if (move.next && !move.next->slot()) {
            movesToRemove << move.next;
        }

        // Create undo command and apply
        container_stow_command_t* command = new container_stow_command_t(m_document);
        command->change_moves(container, movesToRemove, movesToAppend);
        merger->push(command);
    }

    Q_FOREACH (const Container* container, containersThatMightHaveRestows) {
        CurrentMoveFinder move(stowage->moves(container), currentCall);
        if (!move.current) {
            continue;  // If there is no move in this call there can be no restow, skip container
        }

        Q_ASSERT(!move.current->slot());  // All load moves has been handled or transformed if first loop
        QList<const container_move_t*> movesToRemove;
        QList<container_move_t> movesToAppend;
        movesToRemove << move.current;  // Always remove the current move
        // Cut of the arbitor check at the next move or planned discharge
        const Call* arbitorCutoff = move.next ? move.next->call() : m_containerLegs->portOfDischarge(container);
        Q_ASSERT(arbitorCutoff);
        Q_ASSERT(move.previous);  // move.previous must exists
        Q_ASSERT(move.previous->slot());  // move.previous must be load
        if (!move.next) {  // move.next is missing
            movesToAppend << container_move_t(container, 0, arbitorCutoff, move.previous->type());
        }

        // Check for room to cancel the restow (i.e load into previous slot in current call to next call)
        StowArbiter arbiter(stowage, m_document->schedule(), move.previous->slot(), move.current->call());
        arbiter.setEssentialChecksOnly(true);
        arbiter.setException(container);
        arbiter.setCutoff(arbitorCutoff);
        if (!arbiter.isLegal(container)) {
            QString slotName = move.previous->slot()->brt().toString();
            QString details = QString("Could not cancel restow, slot %2 is occupied").arg(slotName);
            CreateProblemCommand* command;
            command = new CreateProblemCommand(m_document, Problem::Notice, "Could not cancel restow", details);
            command->setCall(move.current->call());
            command->setContainer(container);
            merger->push(command);
            ++notificationCount;
            continue;  // When arbiter complaints skip changing moves
        }

        // Create undo command and apply
        container_stow_command_t* command = new container_stow_command_t(m_document);
        command->change_moves(container, movesToRemove, movesToAppend);
        merger->push(command);
    }

    m_document->undostack()->push(merger);
    if (notificationCount > 0) {
        QString message = QString("Cancel restow created %1 notifications in the Problems view").arg(notificationCount);
        m_document->passiveNotifications()->addWarningWithUndo(message);
    }
}

#include "restoweditor.moc"
