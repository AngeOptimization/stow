#ifndef MAIN_STATISTICS_WIDGET_H
#define MAIN_STATISTICS_WIDGET_H

#include <QWidget>

class QLabel;
class QTimer;
class document_t;


namespace ange {
namespace containers {
class Container;
}
namespace schedule {
class Call;
}
}

class main_statistics_widget_t : public QWidget {
  Q_OBJECT
  public:
    main_statistics_widget_t(document_t* document, QWidget*parent=0);
  public Q_SLOTS:
    void set_document(document_t* document);
  private Q_SLOTS:
    void update_stats();
    void stowage_changed();
  private:
    document_t* m_document;
    QTimer* m_stat_timer;
    QLabel* m_stat_label;
};

#endif // MAIN_STATISTICS_WIDGET_H
