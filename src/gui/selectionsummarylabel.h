#ifndef SELECTION_SUMMARY_LABEL_H
#define SELECTION_SUMMARY_LABEL_H

#include <QLabel>

class container_list_t;
class pool_t;
class QTimer;

class SelectionSummaryLabel : public QLabel
{
    Q_OBJECT
public:
    void setSelectionPool(const pool_t* selection_pool, container_list_t* containerList);
public Q_SLOTS:
    void selectionChanged();
private:
    const pool_t* m_selection_pool;
    container_list_t* m_containerList;
};

#endif // SELECTION_SUMMARY_LABEL_H
