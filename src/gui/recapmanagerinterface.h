#ifndef RECAPMANAGERINTERFACE_H
#define RECAPMANAGERINTERFACE_H

class RecapView;
class RecapManagerInterface {
    public:
        /**
         * Creates a correctly default initialized recap view in a dock widget for use in the application
         */
        virtual RecapView* add_recap() = 0;
    protected:
        ~RecapManagerInterface() { }
};

#endif // RECAPMANAGERINTERFACE_H
