/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010

 Copyright: See COPYING file that comes with this distribution

*/

#ifndef CALL_VIEW_ITEM_DELEGATE_H
#define CALL_VIEW_ITEM_DELEGATE_H

#include <QStyledItemDelegate>


class schedule_view_item_delegate_t : public QStyledItemDelegate {

  public:
    schedule_view_item_delegate_t(QObject* parent = 0);
    virtual void paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const;
    virtual QSize sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const;
    virtual QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const;
    virtual void setModelData(QWidget* editor, QAbstractItemModel* model,  const QModelIndex& index) const;
  private:
    QLineEdit* getLineEditEditor(QWidget* parent) const;
};
#endif // CALL_VIEW_ITEM_DELEGATE_H
