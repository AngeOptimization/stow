/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010

 Copyright: See COPYING file that comes with this distribution

*/

#include "selected_container_list.h"
#include <QItemSelectionModel>
#include "document/containers/container_list.h"
#include "document/pools/pool.h"
#include <stdexcept>
#include <QTimer>

selected_container_list_t::selected_container_list_t(container_list_t* containerList, pool_t* pool):
    QSortFilterProxyModel(pool),
    m_containerList(containerList),
    m_selected_pool(pool),
    m_update_delay_timer(new QTimer(this))
{
  QSortFilterProxyModel::setSourceModel(m_containerList);
  m_update_delay_timer->setSingleShot(true);
  m_update_delay_timer->setInterval(100);
  connect(m_update_delay_timer, SIGNAL(timeout()), SLOT(resort_and_filter()));
  connect(m_selected_pool, SIGNAL(changed()), m_update_delay_timer, SLOT(start()));
  sort(0);

}

bool selected_container_list_t::filterAcceptsRow(int source_row, const QModelIndex& /*source_parent*/) const {
  return m_selected_pool->included_rows().contains(source_row);
}

void selected_container_list_t::setSourceModel(container_list_t* ) {
  throw std::runtime_error("Not implemented");
}

void selected_container_list_t::resort_and_filter_later() {
  if (!m_update_delay_timer->isActive()) {
    m_update_delay_timer->start();
  }
}

void selected_container_list_t::resort_and_filter() {
  invalidateFilter();
  sort(0);
  emit total_display_string(QString("%1 containers").arg(m_selected_pool->included_rows().size()));

}

bool selected_container_list_t::lessThan(const QModelIndex& left, const QModelIndex& right) const {
  int l = m_selected_pool->included_rows().indexOf(left.row());
  int r = m_selected_pool->included_rows().indexOf(right.row());
  return l<r;
}

#include "selected_container_list.moc"
