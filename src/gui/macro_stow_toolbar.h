/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010

 Copyright: See COPYING file that comes with this distribution

*/

#ifndef MACRO_STOW_TOOLBAR_H
#define MACRO_STOW_TOOLBAR_H

#include <QToolBar>
#include "mode.h"

class QActionGroup;
class QSignalMapper;
namespace ange {
namespace schedule {
class Call;
}
}

class document_t;

class macro_stow_toolbar_t : public QToolBar {
  Q_OBJECT
  public:
    macro_stow_toolbar_t(QWidget* parent = 0);
    void set_document(document_t* document);
  protected:
    virtual void showEvent(QShowEvent* event);
    virtual void hideEvent(QHideEvent* event);
  public Q_SLOTS:
    void slot_mode_changed(mode_enum_t::mode_t mode);
    void set_current_call(const ange::schedule::Call* call);
    void reset_actions_from_schedule();
  private Q_SLOTS:
    void emit_call_selected(QAction* trigger);
    void remove_call(ange::schedule::Call* call);
    void add_call(ange::schedule::Call* call);
  Q_SIGNALS:
    void call_selected(ange::schedule::Call* call);
    void visibility_changed(bool shown);
  private:
    void set_macro_actions();
    QAction* create_action_for_call(const ange::schedule::Call* call);

    document_t* m_document;
    QActionGroup* m_group;
    const ange::schedule::Call* m_current_call;
};

#endif // MACRO_STOW_TOOLBAR_H
