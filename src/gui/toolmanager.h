#ifndef TOOLMANAGER_H
#define TOOLMANAGER_H

#include <QObject>
#include "mode.h"

class tool_t;

class ToolManager : public QObject {
    Q_OBJECT
    public:
        /**
        * @return current tool
        */
        tool_t* currentTool() const;

        /**
        * @return fill tool
        */
        tool_t* fillTool() const;

        /**
        * @return select tool
        */
        tool_t* selectTool() const;

        /**
        * @return macro tool
        */
        tool_t* macroTool() const;

        /**
        * @return all tools
        */
        QList<tool_t*> tools() const {
            return m_tools;
        }

        /**
        * @true if auto tool is currently engaged.
        */
        bool isAutoToolActive() const {
            return !m_current_tool;
        }

        /**
         * @return current mode
         */
        mode_enum_t::mode_t currentMode() const {
            return m_current_mode;
        }

        ~ToolManager();
        ToolManager(QObject* parent = 0);
    public Q_SLOTS:
        void setAutoTool();
        void setSelectTool();
        void setFillTool();
        void setMacroTool();

        /**
         * Update autotool, which is currently either fill or select.
         * \param on true = select tool, on false = fill tool
         * If SHIFT is held, the select tool is always chosen.
         *
         * Due to bugs in Qt, please ensure that the \param mods is filled with
         * data from a QKeyEvent and not with QApplication::keyboardModifiers
         * if you call this function from a keyPress or keyRelease event.
         *
         * If you don't call this function in a keyPress or keyRelease event, you
         * pass the output from QApplication::keyboardModifiers
         */
        void setAutoToolSelect(bool on, Qt::KeyboardModifiers mods);
        /**
         * Sets macro mode and adapts tools
         */
        void setMacroMode();

        /**
         * Sets micro mode and adapts tools
         */
        void setMicroMode();
    Q_SIGNALS:
        void toolChanged(tool_t* tool);
        void modeChanged(mode_enum_t::mode_t mode);
    private:
        tool_t* m_current_tool;
        QList<tool_t*> m_tools;
        tool_t* m_auto_tool;
        mode_enum_t::mode_t m_current_mode;
        QHash<mode_enum_t::mode_t,tool_t*> m_tool_for_stow_mode;
        QHash<mode_enum_t::mode_t,tool_t*> m_auto_tool_for_stow_mode;

        // functions
        /**
         * the actual implementation of the various setSomethingTool
         */
        void setTool(tool_t* tool);
        /**
         * Switch mode
         * This will hide all the tools not for this mode, and show those who are.
         * Furthermore, if the mode has been active before, the old tool will be restored to current
         * This is the actual implementation of what setMicroMode and setMacroMode does
         */
        void changeMode(mode_enum_t::mode_t mode);
};

#endif // TOOLMANAGER_H
