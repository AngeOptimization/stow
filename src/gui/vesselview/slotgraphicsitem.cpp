#include "slotgraphicsitem.h"

#include "bayslicegraphicsitem.h"
#include "callutils.h"
#include "colortools.h"
#include "compartmentmousetracker.h"
#include "container_leg.h"
#include "container_move.h"
#include "containerutils.h"
#include "document.h"
#include "overstowcounter.h"
#include "painter_utils.h"
#include "pool.h"
#include "stackgraphicsitem.h"
#include "stowage.h"
#include "stowagestack.h"
#include "stowarbiter.h"
#include "userconfiguration.h"
#include "vesselpaintconfig.h"
#include "vesselscene.h"

#include <stowplugininterface/macroplan.h>
#include <stowplugininterface/igui.h>

#include <ange/containers/dangerousgoodscode.h>
#include <ange/containers/oog.h>
#include <ange/containers/container.h>
#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>
#include <ange/vessel/bayslice.h>
#include <ange/vessel/compartment.h>
#include <ange/vessel/slot.h>
#include <ange/vessel/stack.h>
#include <ange/vessel/vessel.h>

#include <QGraphicsSceneMouseEvent>
#include <QMenu>
#include <QPainter>
#include <QStyleOption>
#include <QTextStream>

using namespace ange::angelstow;
using namespace ange::units;
using ange::containers::IsoCode;
using ange::containers::Container;
using ange::vessel::Stack;
using ange::vessel::Slot;
using ange::vessel::Compartment;
using ange::schedule::Call;
using ange::schedule::Schedule;

QColor SlotGraphicsItem::color_for_container(const ange::containers::Container& container, const vessel_paint_config_t& config) const
{
  if (container.isoLength() != IsoCode::Twenty && m_slot->isAft()) {
    if (config.lod <= 1.5) {
      return Qt::lightGray;
    }
  }
  QColor rv;
  switch (config.paint_mode) {
    case DISCHARGE_PAINT_MODE: {
      const Call* destination = m_stowage->container_routes()->portOfDischarge(&container);
      rv = m_document->userconfiguration()->get_color_by_call(destination);
      break;
    }
    case WEIGHT_PAINT_MODE: {
      int weight_class = static_cast<int>(container.weight()/(5.0*ton));
      switch (weight_class) {
        case 0:
          rv = QColor(221, 221, 255);
          break;
        case 1:
          rv = QColor(187, 187, 255);
          break;
        case 2:
          rv = QColor(153, 153, 255);
          break;
        case 3:
          rv = QColor(119, 119, 255);
          break;
        case 4:
          rv = QColor(85, 85, 255);
          break;
        case 5:
          rv = QColor(51, 51, 255);
          break;
        case 6:
          rv = QColor(0, 0, 255);
          break;
        default: //35T+
          rv = QColor(0, 0, 221);
          break;
      }
      break;
    }
  }
  return rv;
}

QBrush SlotGraphicsItem::get_brush_for_contents(const slot_call_content_t& slot_contents, const bool killed, const bool legal, const vessel_paint_config_t& config) const {
  const Container* container = slot_contents.container();
  // Dead slots, and pairs to odd slots
  if (!m_slot) {
     if(config.printMode.grayOddSlots) {
        return QColor(Qt::lightGray).lighter(120);
     } else {
        return Qt::transparent;
     }
  }
  if (!container && (killed)) {
     if(config.printMode.grayKilledSlots) {
        return Qt::lightGray;
     } else {
        return Qt::transparent;
     }
  }
  QColor rv = container ? color_for_container(*container, config) : (!legal && config.printMode.grayIllegalSlots ? Qt::gray : Qt::transparent);
  const bool non_micro_stowed_container = ((!slot_contents.container()  && config.mode == mode_enum_t::MACRO_MODE) || slot_contents.type() == MasterPlannedType);
  // Paint macro stowage
  if (non_micro_stowed_container) {
    const CompartmentMouseTracker* macroStowageMouseTracker = stackGraphicsItem()->bay_slice_item()->macroStowageMouseTracker();
    ange::angelstow::MacroPlan* macro_stowage = m_document->macro_stowage();
    Stack* vessel_stack = slot()->stack();
    Compartment* compartment = vessel_stack->baySlice()->compartmentContainingRow(vessel_stack->row(), vessel_stack->level());
    const Call* macro_call = macro_stowage->dischargeCall(compartment, config.call);
    const Call* tentative_call = 0L;
    if (macroStowageMouseTracker->tentativeCompartment() == compartment &&
        config.call->distance(macroStowageMouseTracker->tentativeCall()) > 0 &&
        config.selected_call->distance(config.call) >= 0) {
      tentative_call = macroStowageMouseTracker->tentativeCall();
    }
    if (macro_call || tentative_call || container) {
      QColor stripe_color;
      if (tentative_call && config.mode == mode_enum_t::MACRO_MODE) {
        stripe_color = QColor(m_document->userconfiguration()->get_color_by_call(tentative_call));
        stripe_color.setAlpha(macro_call ? 162 : 128);
      }
      else  {
        stripe_color = macro_call ? QColor(m_document->userconfiguration()->get_color_by_call(macro_call)) : Qt::transparent;
        if(container) {
            const Call* destination = m_stowage->container_routes()->portOfDischarge(container);
            if (macro_call == destination) {
                stripe_color.setAlpha(196);
            }
        }
      }
      QLinearGradient grad(0.0, 0.0, 1.0, -1.0);
      grad.setColorAt(0.0, stripe_color);
      grad.setColorAt(0.3, stripe_color);
      grad.setColorAt(0.4, rv);
      grad.setColorAt(1.0, rv);
      grad.setSpread(QGradient::ReflectSpread);
      return QBrush(grad);
    }
  } else if (rv != Qt::transparent) {
    return rv;
  }
  return Qt::NoBrush;
}

void SlotGraphicsItem::paint( QPainter* painter, const QRectF& bounding_rect, const vessel_paint_config_t& config,
                         bool legal, bool killed) {
    const slot_call_content_t content = slot() ? m_stowage->contents(slot(), config.call) : slot_call_content_t();
    const Container* container = content.container();
    painter->setBrush(QBrush());
    bool invert_overlay_color = false;
    const Call* call = 0;
    if (container) {
        call = m_stowage->container_routes()->portOfDischarge(container);
        QColor color = m_document->userconfiguration()->get_color_by_call(call);
        invert_overlay_color = backgroundRequiresColorInversion(color);
    }
    if (container && container->isoLength() != IsoCode::Twenty && m_slot->isAft()) {
        painter->fillRect(bounding_rect, Qt::white);
        QPointF center = bounding_rect.center();
        qreal halfPlusWidth = 0.5 * 0.4 * bounding_rect.width(); // '+' width shall be 40% of rectangel width
        painter->setPen(QPen(Qt::black, 0.25 * halfPlusWidth, Qt::SolidLine)); // scaling pen width 1/4 of half '+' width
        bool antialiasing = painter->renderHints() & QPainter::Antialiasing;
        painter->setRenderHint(QPainter::Antialiasing, true);
        painter->drawLine(QLineF(center.x(), center.y() - halfPlusWidth, center.x(), center.y() + halfPlusWidth));
        painter->drawLine(QLineF(center.x() - halfPlusWidth, center.y(), center.x() + halfPlusWidth, center.y()));
        painter->setRenderHint(QPainter::Antialiasing, antialiasing);
        return; //we don't draw any overlays or anything if it is a 40/45 container.
    }
    const bool remain_onboard = container && !m_stowage->container_moved_at_call(container, config.call);
    const Call* next_call = m_document->schedule()->next(config.call);
    const bool moved_in_next_call = container && m_stowage->container_moved_at_call(container, next_call);
    {
        QBrush contentBrush;
        if (remain_onboard && config.printMode.grayRemainOnboard) {
            painter->fillRect(bounding_rect, Qt::gray);
            return; // just paint a gray square
        } else if (container && !moved_in_next_call && config.printMode.grayNotMovedInNextCall) {
            painter->fillRect(bounding_rect, Qt::gray);
            return; // just paint a gray square
        } else if (container && config.printMode.whiteContainers) {
            contentBrush = Qt::white;
            invert_overlay_color = false;
        } else {
            contentBrush = get_brush_for_contents(content, killed, legal, config);
        }
        painter->fillRect(bounding_rect, contentBrush.style()!=Qt::NoBrush ? contentBrush : QBrush(Qt::white));
    }
    if (remain_onboard && config.printMode.notPrinting) {
        ange::painter_utils::draw_svg_in_rect(":/slots/slot_overlays/remaining_onboard.svg",
                                              painter, bounding_rect, invert_overlay_color);
    }
    if (container) {
        // Draw load/shift rectangle
        if (config.lod >= 6.0 && config.printMode.notPrinting) {
            QRectF loadrect = bounding_rect;
            loadrect.setBottom(loadrect.bottom() - loadrect.height()*.75);
            loadrect.setLeft(loadrect.left() + loadrect.width()*.75);
            const Call* origin_call = m_stowage->container_routes()->portOfLoad(container);
            QColor origin_color = m_document->userconfiguration()->get_color_by_call(origin_call);
            painter->setBrush(origin_color);
            painter->setPen(Qt::NoPen);
            painter->drawRect(loadrect);
            if (const Call* last_load_call = m_stowage->load_call(container, config.call)) {
                if (last_load_call != origin_call) {
                    QColor last_color = m_document->userconfiguration()->get_color_by_call(last_load_call);
                    painter->setBrush(last_color);
                    QPolygonF triangle;
                    triangle << loadrect.topRight() << loadrect.bottomRight()
                             << loadrect.topLeft() << loadrect.topRight();
                    painter->drawPolygon(triangle);
                }
            }
        }
    }
    //draw various overlays
    if (config.printMode.notPrinting) {
        if (config.lod > 2.0) {
            if (config.lod > 6.0) {
                draw_overlays_high_lod(painter, content, config.call, bounding_rect, invert_overlay_color);
            } else {
                draw_overlays_medium_lod(painter, content, config.call, bounding_rect, invert_overlay_color);
            }
        } else {
            //don't print overlays when zoomed out
        }
     } else { //layout for printing
        draw_overlays_medium_lod(painter, content, config.call, bounding_rect, invert_overlay_color);
     }
    // Draw weight numbers
    //LOD == 6.0 has been chosen such that zooming-to-bay on a regular laptop (1600x900) with a regular
    //screen confiuguration allows seeing the container weights as soon as they distinguishable
    if ((6.0 < config.lod && container && config.displayContainerWeight && config.printMode.notPrinting)
        || (container && config.printMode.displayContainerWeight)
    ) {
        QString text = QString::number(qRound(container->weight() / ton));
        const qreal width = bounding_rect.width();
        QRectF box = bounding_rect.adjusted(width*0.3, width*0.3, -width*0.3, -width*0.3);
        QRect textBounds = painter->fontMetrics().boundingRect("MM");
        ange::painter_utils::drawTextInRect(painter, box, text, invert_overlay_color, textBounds);
    }
    // Draw discharge call letter in slot
    if (container && config.printMode.displayDischargeCallLetter) {
        QString text = config.printMode.dischargeCallLetters.value(call);
        QRect text_bounds = painter->fontMetrics().boundingRect("M");
        ange::painter_utils::drawTextInRect(painter, bounding_rect, text, invert_overlay_color, text_bounds);
    }
}

void SlotGraphicsItem::draw_overlays_medium_lod(QPainter* painter, const slot_call_content_t& slot_contents,
                                const ange::schedule::Call* /*current_call*/,
                                const QRectF& bounding_rect, const bool invert_overlay_color) const {
    const Container* container = slot_contents.container();
    if(container){
        if (container->live() == Container::Live) {
            ange::painter_utils::draw_svg_in_rect(":/slots/slot_overlays/powered_reefer-bold.svg",
                                                  painter, bounding_rect, invert_overlay_color);
        }
        if (container->isDangerous()) {
            ange::painter_utils::draw_svg_in_rect(":/slots/slot_overlays/dangerous-goods-bold.svg",
                                                  painter, bounding_rect, invert_overlay_color);
        }
        if (container->isoCode().height() == IsoCode::HC) {
            //make the caret fit nicely over the "e" for empty
            QRect textBounds = painter->fontMetrics().boundingRect("M");
            ange::painter_utils::drawTextInRect(painter, bounding_rect.adjusted(0, 1, 0,0), "^",
                                                invert_overlay_color, textBounds );
        }
        if(!container->bundleChildren().isEmpty()) {
            ange::painter_utils::draw_svg_in_rect(":/slots/slot_overlays/bundle-bold.svg",
                                              painter, bounding_rect, invert_overlay_color);
        } else if (container->empty()) {
            QRect textBounds = painter->fontMetrics().boundingRect("e");
            ange::painter_utils::drawTextInRect(painter, bounding_rect, "e", invert_overlay_color, textBounds);
        }
        if (container->oog()) {
            if (container->oog().top() >= 6 * centimeter) {
                ange::painter_utils::draw_svg_in_rect(":/slots/slot_overlays/oog-top-bold.svg",
                                                    painter, bounding_rect, invert_overlay_color);
            }
            if (container->oog().left() >= 6 * centimeter) {
                ange::painter_utils::draw_svg_in_rect(":/slots/slot_overlays/oog-left-bold.svg",
                                                    painter, bounding_rect, invert_overlay_color);
            }
            if (container->oog().right() >= 6 * centimeter) {
                ange::painter_utils::draw_svg_in_rect(":/slots/slot_overlays/oog-right-bold.svg",
                                                    painter, bounding_rect, invert_overlay_color);
            }
        }
    }
}

void SlotGraphicsItem::draw_overlays_high_lod(QPainter* painter,
                                const slot_call_content_t& slot_contents,
                                const ange::schedule::Call* current_call,
                                const QRectF& bounding_rect,
                                const bool invert_overlay_color) const
{
  const Container* container = slot_contents.container();
  if (m_slot) {
    if(m_slot->hasCapability(Slot::ReeferCapable) && (container&& !(container->live() == Container::Live))) {
      ange::painter_utils::draw_svg_in_rect(":/slots/slot_overlays/unpowered-reefer.svg",
                                            painter, bounding_rect, invert_overlay_color);
    }
    if(m_slot->hasCapability(Slot::ReeferCapable) && !container) {
      ange::painter_utils::draw_svg_in_rect(":/slots/slot_overlays/unpowered-reefer.svg",
                                            painter, bounding_rect, invert_overlay_color);
    }
    if (!m_slot->sister()) {
      ange::painter_utils::draw_svg_in_rect(":/slots/slot_overlays/oddslot.svg",
                                            painter, bounding_rect, invert_overlay_color);
    }
    if (!m_slot->stack()->stackSupport20Aft() && !m_slot->stack()->stackSupport20Fore()
        && m_slot->stack()->stackSupport40() && !container) {
      ange::painter_utils::draw_svg_in_rect(":/slots/slot_overlays/40only-slot.svg",
                                            painter, bounding_rect, invert_overlay_color);
    }
    if(m_slot->hasCapability(Slot::FourtyFiveCapable) && !container) {
      ange::painter_utils::draw_svg_in_rect(":/slots/slot_overlays/45-foot-slot.svg",
                                            painter,bounding_rect, invert_overlay_color);
    }
  }
  if (container) {
    if (container->live() == Container::Live) {
        if(!m_slot->hasCapability(Slot::ReeferCapable)) {
            ange::painter_utils::draw_svg_in_rect(":/slots/slot_overlays/unpowered-livereefer.svg",
                                                  painter, bounding_rect, invert_overlay_color);
        } else {
            ange::painter_utils::draw_svg_in_rect(":/slots/slot_overlays/powered_reefer.svg",
                                                  painter, bounding_rect, invert_overlay_color);
        }
    }
    if (container->isDangerous()) {
        ange::painter_utils::draw_svg_in_rect(":/slots/slot_overlays/dangerous-goods.svg",
                                              painter, bounding_rect, invert_overlay_color);
        const QRect textBounds = painter->fontMetrics().boundingRect("MM.");
        const QString imdgClass= container->dangerousGoodsCodes().at(0).imdgClass().at(0);
        const QRectF leftCenter= bounding_rect.adjusted(0, -.33*2.6, -.66*2.5, .33*2.6); //2.5 container width, 2.61 container height
        ange::painter_utils::drawTextInRect(painter, leftCenter, imdgClass, !invert_overlay_color, textBounds);

    }
    if (container->isoCode().height() == IsoCode::HC) {
        const QRect textBounds = painter->fontMetrics().boundingRect("^");
        const QRectF centerUpper = bounding_rect.adjusted(.33*2.5, 0, -.33*2.5, .66*2.6); //2.5 container width, 2.61 container height
        ange::painter_utils::drawTextInRect(painter, centerUpper, "^", invert_overlay_color, textBounds);
    }
    if (container->isoLength() == IsoCode::FourtyFive) {
      ange::painter_utils::draw_svg_in_rect(":/slots/slot_overlays/45-foot.svg",
                                            painter, bounding_rect, invert_overlay_color);
    } else if (container->isoLength() == IsoCode::FiftyThree) {
      ange::painter_utils::draw_svg_in_rect(":/slots/slot_overlays/53-foot.svg",
                                            painter, bounding_rect, invert_overlay_color);
    }
    else if (container->isoLength() == IsoCode::Twenty) {
        const QRect textBounds = painter->fontMetrics().boundingRect("MM");
        const QRectF rightUpper = bounding_rect.adjusted(.66*2.5, 0, 0, .66*2.6); //2.5 container width, 2.61 container height
        ange::painter_utils::drawTextInRect(painter, rightUpper, "20", invert_overlay_color, textBounds);
    }
    if(!container->bundleChildren().isEmpty()) {
        ange::painter_utils::draw_svg_in_rect(":/slots/slot_overlays/bundle.svg",
                                              painter, bounding_rect, invert_overlay_color);
    } else if (container->empty()) {
        const QRect textBounds = painter->fontMetrics().boundingRect("M.");
        const QRectF centerUpper = bounding_rect.adjusted(.33*2.5, 0, -.33*2.5, .66*2.6); //2.5 container width, 2.61 container height
        ange::painter_utils::drawTextInRect(painter, centerUpper, "e", invert_overlay_color, textBounds);
    }
    if (container->oog()) {
        if (container->oog().top() >= 6 * centimeter) {
            ange::painter_utils::draw_svg_in_rect(":/slots/slot_overlays/oog-top.svg",
                                                painter, bounding_rect, invert_overlay_color);
        }
        if (container->oog().left() >= 6 * centimeter) {
            ange::painter_utils::draw_svg_in_rect(":/slots/slot_overlays/oog-left.svg",
                                                painter, bounding_rect, invert_overlay_color);
        }
        if (container->oog().right() >= 6 * centimeter) {
            ange::painter_utils::draw_svg_in_rect(":/slots/slot_overlays/oog-right.svg",
                                                painter, bounding_rect, invert_overlay_color);
        }
    }
    {
        const QRect textBounds = painter->fontMetrics().boundingRect("9999");
        const QRectF centerLow = bounding_rect.adjusted(.3*2.5, -.66*2.6, -.3*2.5, 0); //2.5 container width, 2.61 container height
        ange::painter_utils::drawTextInRect(painter, centerLow, container->isoCode().code(),
                                            invert_overlay_color, textBounds);
    }
    StowArbiter arbiter(m_stowage, m_document->schedule(), slot(), current_call);
    arbiter.setException(container);
    const container_move_t* out_move = m_document->stowage()->get_out_move(slot(), current_call);
    arbiter.setMicroplacedOnly(false);
    arbiter.setCutoff(out_move->call());
    if (!arbiter.isLegal(container) || arbiter.hanging(container->isoLength()==IsoCode::Twenty)) {
      ange::painter_utils::draw_svg_in_rect(":/slots/slot_overlays/problem.svg",
                                            painter, bounding_rect, invert_overlay_color);
    }
    if (m_slot) {
      switch (m_stowage->overstowCounter()->overstowMove(current_call, m_slot)) {
        case OverstowCounter::None:
          // No marker
          break;
        case OverstowCounter::Later:
          ange::painter_utils::draw_svg_in_rect(":/slots/slot_overlays/restow-later.svg",
                                                painter, bounding_rect, invert_overlay_color);
          break;
        case OverstowCounter::Now:
          ange::painter_utils::draw_svg_in_rect(":/slots/slot_overlays/restow.svg",
                                                painter, bounding_rect, invert_overlay_color);
          break;
      }
    }
  }
}

SlotGraphicsItem::SlotGraphicsItem(ange::vessel::Slot* slot, document_t* document, const pool_t* selectionPool,
                                   StackGraphicsItem* parent)
  : QObject(parent),
    m_slot(slot),
    m_document(document),
    m_selectionPool(selectionPool),
    m_mode(mode_enum_t::MICRO_MODE),
    m_selected(false)
{
    if (m_slot) {
        setObjectName(QStringLiteral("SlotGraphicsItem(%1)").arg(m_slot->brt().toString()));
    } else {
        setObjectName(QStringLiteral("SlotGraphicsItem(null)"));
    }
    m_stowage = m_document->stowage(); // FIXME remove m_stowage
}

QString SlotGraphicsItem::toString() const {
  QString rv;
  QTextStream stream(&rv);
  const ange::vessel::BayRowTier& brt = m_slot->brt();
  stream << brt;
  stream.flush();
  return rv;
}

QDebug operator<<(QDebug Debug, const SlotGraphicsItem& slot_item) {
  return Debug << slot_item.toString();
}

QDebug operator<<(QDebug Debug, const SlotGraphicsItem* slot_item) {
  if (slot_item) {
    return Debug << *slot_item;
  } else {
    return Debug << "<null>";
  }
}

QString SlotGraphicsItem::tooltip_text(const Call* current_call, bool show_equipment_number) const {
    QString tooltip;
    const Container* container = slot() ? m_stowage->contents(slot(), current_call).container() : 0L;
    tooltip = "<table>";
    if (container) {
        if (show_equipment_number) {
            tooltip.append(QString("<tr><th align=\"right\">ID:</th><td> %1</td></tr>").arg(container->equipmentNumber()));
        }
        if(!container->bundleChildren().isEmpty()) {
            QString containers;
            Q_FOREACH(const Container* c, container->bundleChildren()) {
                if(!containers.isEmpty()) {
                    containers+="<br>";
                }
                containers+=c->equipmentNumber();
            }
            if(containers.isEmpty()) {
                containers = "None";
            }
            tooltip += QString("<tr><th align=\"right\">Bundled:</th><td>%1</td></tr>").arg(containers);
        }
        tooltip += QString("<tr><th align=\"right\">Weight:</th><td>%1</td></tr>").arg(container->weight()/ton);
        tooltip += QString("<tr><th align=\"right\">Type:</th><td>%1</td></tr>").arg(container->isoCode().code());
        const ange::schedule::Call* source = m_stowage->container_routes()->portOfLoad(container);
        if (is_befor(source) || is_after(source)) {
            tooltip += QString("<tr><th align=\"right\">POL:</th><td>%1</td></tr>").arg(container->loadPort());
        } else {
            tooltip += QString("<tr><th align=\"right\">POL:</th><td>%1</td></tr>").arg(source->uncode());
        }
        const ange::schedule::Call* portOfDischarge = m_stowage->container_routes()->portOfDischarge(container);
        if (is_befor(portOfDischarge) || is_after(portOfDischarge)) {
            tooltip += QString("<tr><th align=\"right\">POD:</th><td>%1</td></tr>").arg(container->dischargePort());
        } else {
            tooltip += QString("<tr><th align=\"right\">POD:</th><td>%1</td></tr>").arg(portOfDischarge->uncode());
        }
        if(!container->placeOfDelivery().isEmpty()) {
            tooltip += QString("<tr><th align=\"right\">Place of Delivery:</th><td>%1</td></tr>").arg(container->placeOfDelivery());
        }
        if(!container->carrierCode().isEmpty()){
            tooltip += QString("<tr><th align=\"right\">Carrier:</th><td>%1</td></tr>").arg(container->carrierCode());
        }
        tooltip += QString("<tr><th align=\"right\">Position&nbsp;(brt):</th><td>%1</td></tr>").arg(m_stowage->nominal_position(container, current_call).toString());
        QString reefer_capable = slot()->hasCapability(Slot::ReeferCapable)?"True":"False";
        tooltip += QString("<tr><th align=\"right\">Reefer slot:</th><td>%1</td></tr>").arg(reefer_capable);
        if(container->oog()) {
            tooltip += QString("<tr><th align=\"right\">OOG:</th><td><table>");
            if (container->oog().front() / centimeter > 0.0) {
                tooltip += QString("<tr><td>Front:%1 cm</td></tr>").arg(container->oog().front() / centimeter);
            }
            if (container->oog().back() / centimeter > 0.0) {
                tooltip += QString("<tr><td>Back:%1 cm</td></tr>").arg(container->oog().back() / centimeter);
            }
            if (container->oog().right() / centimeter > 0.0) {
                tooltip += QString("<tr><td>Right:%1 cm</td></tr>").arg(container->oog().right() / centimeter);
            }
            if (container->oog().left() / centimeter > 0.0) {
                tooltip += QString("<tr><td>Left:%1 cm</td></tr>").arg(container->oog().left() / centimeter);
            }
            if (container->oog().top() / centimeter > 0.0) {
                tooltip += QString("<tr><td>Top:%1 cm</td></tr>").arg(container->oog().top() / centimeter);
            }
            tooltip += "</table></td></tr>";
        }
        if (container->isDangerous()) {
            QString codes;
            Q_FOREACH(const ange::containers::DangerousGoodsCode& dg, container->dangerousGoodsCodes()) {
                if (!codes.isEmpty()) {
                    codes += ", ";
                }
                codes += QString("%1 (%2)").arg(dg.unNumber(), dg.imdgClasses().join(", "));
            }
            tooltip += QString("<tr><th align=\"right\">Dangerous:</th><td>%1</td></tr>").arg(codes);
        }
        if(!container->handlingCodes().empty()) {
            tooltip += QString("<tr><th align=\"right\">Stow code:</th><td>%1</td></tr>")
                                                .arg(ContainerUtils::handlingCodesAsParsableString(container->handlingCodes()));
        }
        QList <container_move_t> container_history = m_stowage->moves_inclusive_induced(container);
        if (container_history.count() > 2) {
            const Call* previous_call = container_history.at(0).move().call();
            QString previous_position = m_stowage->nominal_position(container, previous_call).toString();
            container_history.pop_front();
            container_history.pop_back();
            Q_FOREACH(const container_move_t c_move, container_history) {
                const Call* cur_call = c_move.call();
                const QString current_position = m_stowage->nominal_position(container, cur_call).toString();
                tooltip += QString("<tr><th align=\"right\">Restow:</th><td>%1</td></tr>").arg(
                previous_call->uncode() + " " + previous_position + "->" + cur_call->uncode() + " " + current_position);
                previous_position = current_position;
                previous_call = cur_call;
            }
        } else {
            Q_ASSERT(container_history.count() ==2);
        }
    } else {
        if (m_slot) {
            ange::vessel::BayRowTier brt = m_slot->brt();
            const bool fore_with_20_support = m_slot->isFore() && m_slot->stack()->stackSupport20Fore();
            const bool aft_with_20_support = m_slot->isAft() && m_slot->stack()->stackSupport20Aft();
            if (!fore_with_20_support && !aft_with_20_support && !m_slot->isOddSlot()) {
                brt = ange::vessel::BayRowTier(m_slot->stack()->baySlice()->joinedBay(), brt.row(), brt.tier());
            }
            tooltip += QString("<tr><th align=\"right\">Position&nbsp;(brt):</th><td>%1</td></tr>").arg(brt.toString());
            QString reefer_capable = slot()->hasCapability(Slot::ReeferCapable)?"True":"False";
            tooltip += QString("<tr><th align=\"right\">Reefer Slot:</th><td>%1</td></tr>").arg(reefer_capable);
        } else {
            return QString("odd slot in opposite bay");
        }
    }
    if (StackGraphicsItem* stack_item = qobject_cast<StackGraphicsItem*>(parent())) {
        QList<const ange::containers::Container*> list;
        if (container) {
            list << container;
        } else {
            list = m_selectionPool->containers();
        }
        QStringList illegal_reasons = stack_item->illegal_reasons(this, list);
        Q_FOREACH(QString reason, illegal_reasons) {
            QString line = "<tr><td align=\"left\" colspan='2' class='legal'>" + reason + "</td></tr>";
            tooltip += line;
        }
    }
    tooltip += "</table>";
    return tooltip;
}

void SlotGraphicsItem::set_selected(bool selected) {
  m_selected = selected;
  if (selected) {
    if (StackGraphicsItem* stack_item = qobject_cast<StackGraphicsItem*>(parent())) {
      stack_item->bay_slice_item()->start_marching_ants();
    }
  }
  update();
}

bool SlotGraphicsItem::moved_at_call(const ange::containers::Container* container, const ange::schedule::Call* call) const {
  Q_FOREACH(const container_move_t* move, m_stowage->moves(container)) {
    if (move->call() == call) {
      return true;
    }
    if (call->distance(move->call()) >= 0) {
      // No need to look into the future
      break;
    }
  }
  return false;
}

qreal SlotGraphicsItem::max_height() const {
  if (m_slot) {
    return 2.8956;
  } else {
    return 2.5908;
  }
}

qreal SlotGraphicsItem::height(const slot_call_content_t& slot_contents) const {
  if (const Container* container = slot_contents.container()) {
    return container->physicalHeight() / meter;
  }
  return 2.5908;
}

slot_call_content_t SlotGraphicsItem::slot_contents(const ange::schedule::Call* call) const {
  Q_ASSERT(call == stackGraphicsItem()->bay_slice_item()->call()); // TODO remove argument or document when when there is difference
  return m_slot ? m_stowage->contents(m_slot, call) : slot_call_content_t();
}

void SlotGraphicsItem::update() {
  if (StackGraphicsItem* stack_item = qobject_cast<StackGraphicsItem*>(parent())) {
    stack_item->update(this);
  }
}

StackGraphicsItem* SlotGraphicsItem::stackGraphicsItem() {
    return qobject_cast<StackGraphicsItem*>(parent());
}

const StackGraphicsItem* SlotGraphicsItem::stackGraphicsItem() const {
    return qobject_cast<StackGraphicsItem*>(parent());
}



#include "slotgraphicsitem.moc"
