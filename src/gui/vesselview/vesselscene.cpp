#include "vesselscene.h"

#include "bayslicegraphicsitem.h"
#include "callutils.h"
#include "compartmentmousetracker.h"
#include "container_leg.h"
#include "container_list.h"
#include "container_stow_command.h"
#include "document.h"
#include "macrocompartmentcommand.h"
#include "merger_command.h"
#include "passivenotifications.h"
#include "pool.h"
#include "slotgraphicsitem.h"
#include "stackgraphicsitem.h"
#include "stowage.h"
#include "stowarbiter.h"
#include "tool.h"
#include "toolmanager.h"
#include "userconfiguration.h"
#include "vesselgraphicsitem.h"

#include <stowplugininterface/macroplan.h>
#include <stowplugininterface/problem.h>

#include <ange/containers/container.h>
#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>
#include <ange/vessel/bayslice.h>
#include <ange/vessel/compartment.h>
#include <ange/vessel/slot.h>
#include <ange/vessel/stack.h>
#include <ange/vessel/stacksupport.h>
#include <ange/vessel/vessel.h>

#include <QAbstractItemView>
#include <QAbstractProxyModel>
#include <QApplication>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsView>
#include <QKeyEvent>
#include <QSettings>
#include <QTimer>
#include <QToolTip>

#include <algorithm>
#include <cmath>

using namespace ange::angelstow;
using namespace ange::containers;
using namespace ange::schedule;
using namespace ange::vessel;

struct vessel_scene_t::slot_call_t {
    const Slot* slot;
    const Call* call;
    slot_call_t(const Slot* slot, const Call* call): slot(slot), call(call) {}
    slot_call_t() : slot(0L), call(0L) {}
    bool operator==(const slot_call_t& rhs) const;
    bool operator==(const Call* call) const;
};

bool vessel_scene_t::slot_call_t::operator==(const Call* call) const {
    return this->call == call;
}

uint qHash(const vessel_scene_t::slot_call_t& slot_call) {
    return qHash(slot_call.call) + qHash(slot_call.slot);
}

bool vessel_scene_t::slot_call_t::operator==(const vessel_scene_t::slot_call_t& rhs) const {
    return (rhs.call == call) && (rhs.slot == slot);
}

vessel_scene_t::vessel_scene_t(document_t* document, pool_t* selection_pool):
    QGraphicsScene(document),
    m_macroStowageMouseTracker(new CompartmentMouseTracker(document)),
    m_current_call(0L),
    m_current_mode(mode_enum_t::MICRO_MODE),
    m_current_painted_call(0L),
    m_show_equipment_number(false),
    m_printMode(),
    m_print_bay_numbers(false),
    m_selection_pool(selection_pool),
    m_row_label_mode(mode_enum_t::NAME),
    m_legal(false),
    m_2_rows(false),
    m_toolManager(0)
{
    connect_to_stowage();
    {
        QSettings s;
        m_hide_non_active_calls = s.value("view/hide_nonactive", QVariant(false)).toBool();
        m_displayContainerWeights = s.value("behavior/display container weights", true).toBool();
        m_linkForeAft = s.value("link_fore_aft_slots").toBool();
    }
    connect(document->schedule(), &Schedule::callAboutToBeRemoved, this, &vessel_scene_t::remove_call);
    connect(document->schedule(), &Schedule::callAdded, this, &vessel_scene_t::add_call);
    connect(document->schedule(), &Schedule::rotationChanged, this, &vessel_scene_t::applyChangeOfRotation);
    connect(m_selection_pool, SIGNAL(changed()), SLOT(slot_selection_changed()));
    connect(document->userconfiguration(), SIGNAL(colorsChanged()), this, SLOT(update()));
    connect(document, &document_t::vessel_changed, this, &vessel_scene_t::set_vessel);
    setSceneRect(QRectF(0.0, 0.0, 0.0, 0.0));
    Q_FOREACH (Call* call, document->schedule()->calls()) {
        if (!is_after(call)) {
            add_call(call);
        }
    }
    const qreal bp_margin = margin();
    setSceneRect(sceneRect().adjusted(-bp_margin, -bp_margin, bp_margin, bp_margin));
    QSettings settings;
    display_bulkheads_for_all_calls(settings.value("view/display bulkheads").toBool());
}

void vessel_scene_t::connect_to_stowage() {
    document_t* document = this->document();
    connect(document->stowage(), &stowage_t::container_changed_position, this, &vessel_scene_t::slot_container_moved);
    connect(document->stowage(), SIGNAL(stowageChanged()), SLOT(update()));
    connect(document->containers(), &container_list_t::container_changed, this, &vessel_scene_t::slot_container_changed);
    update();
}

vessel_scene_t::~vessel_scene_t() {
    // Empty
}

document_t* vessel_scene_t::document() const {
    return static_cast<document_t*>(parent());
}

namespace {
/**
 * @returns 1 if @param first is more central (horizontally)
 * than @param second, -1 if the opposite is true and 0 if they
 * are in the same row
 */
int center_out_horizontal_order(const Slot* first, const Slot* second) {
    const Stack* first_stack = first->stack();
    const Stack* second_stack = second->stack();
    if (first_stack->row() == second_stack->row()) {
        return 0;
    }
    return first_stack->row() > second_stack->row() ? -1 : 1;
}

/**
 * @returns a positive number if @param first's tier is above
 * @param second's
 */
int vertical_order(const Slot* first, const Slot* second) {
    const int first_tier = first->brt().tier();
    const int second_tier = second->brt().tier();
    return first_tier - second_tier;
}

int joinedbayOrder(const Slot* first, const Slot* second) {
    int firstJoinedBay = first->stack()->baySlice()->joinedBay();
    int secondJoinedBay = second->stack()->baySlice()->joinedBay();
    return firstJoinedBay - secondJoinedBay;
}

/**
 * Sorts the slots for bucket filling in the following order
 * lowest/highest tier (depending on \param fromTop)
 * then lowest row
 * then lowest joinedBay (40' bay)
 * then depending on \param preferFore the fore or the aft slot. At this point, they should be siblings
 */
struct sort_center_out {
    sort_center_out(bool fromTop, bool preferFore) : m_fromTop(fromTop), m_preferFore(preferFore) {}
    bool operator()(const Slot* first, const Slot* second) {
        if (int vo = vertical_order(first, second)) {
            if (m_fromTop) {
                return vo > 0;
            } else {
                return vo < 0;
            }
        }
        if (int ho = center_out_horizontal_order(first, second)) {
            return ho > 0;
        }
        if (int joinedbay_order = joinedbayOrder(first, second)) {
            return joinedbay_order > 0;
        }
        Q_ASSERT(first->isFore() != second->isFore());
        Q_ASSERT(first->sister() == second);
        if (m_preferFore) {
            return first->isFore();
        } else {
            return first->isAft();
        }
    }
private:
    bool m_fromTop;
    bool m_preferFore;
};

struct reverse_weight_orderer_t {
    bool operator()(const Container* first, const Container* second) {
        return first->weight() > second->weight();
    }
};

} //namespace

void vessel_scene_t::bucket_fill_rect(QRectF rect) {
    QList<const Slot*> selected_slots = selected_slots_from_rect(rect);
    if (selected_slots.empty() && rect.height() < 0.01 && rect.width() < 0.01) {
        // Click or near-click. If in auto-tool mode, drop selection.
        if (m_toolManager->isAutoToolActive()) {
            m_selection_pool->clear();
        }
        return;
    }
    const bool from_top = rect.height() > 0.0;
    int foreAftCounter = 0;
    Q_FOREACH(const Slot * slot, selected_slots_from_rect(rect, false)) {
        if (slot->isFore()) {
            foreAftCounter++;
        } else {
            foreAftCounter--;
        }
    }
    const bool preferFore = foreAftCounter >= 0;
    stowage_t* stowage = document()->stowage();
    QList<const Container*> selected_containers = m_selection_pool->containers();
    /**
     * In order to avoid hanging containers when placing from top to bottom, we split the selected slot list in needed
     * slots (bottom ones) and extra slots. We then assemble the final list such as to start by filling the bottom ones.
     */
    int nbNeededSlots = 0;
    Q_FOREACH(const Container* container, selected_containers) {
        nbNeededSlots += container->equivalentTEU();
    }
    if (selected_slots.size() > nbNeededSlots) {
        qSort(selected_slots.begin(), selected_slots.end(), sort_center_out(false, preferFore));
        QList <const Slot*> bottomSlots;;
        QList <const Slot*> extraSlots;;
        for (int i = 0; i < nbNeededSlots; ++i) {
            bottomSlots << selected_slots.at(i);
        }
        for (int i = nbNeededSlots; i < selected_slots.size(); ++i) {
            extraSlots << selected_slots.at(i);
        }
        qSort(bottomSlots.begin(), bottomSlots.end(), sort_center_out(from_top, preferFore));
        qSort(extraSlots.begin(), extraSlots.end(), sort_center_out(from_top, preferFore));
        selected_slots = bottomSlots + extraSlots;
    } else {
        qSort(selected_slots.begin(), selected_slots.end(), sort_center_out(from_top, preferFore));
    }
    qSort(selected_containers.begin(), selected_containers.end(), reverse_weight_orderer_t());
    merger_command_t* merger = new merger_command_t("Stow containers");

    // If we are (probably) going to be stowing a lot of containers, prevent emitting signals constantly
    QSharedPointer<stowage_signal_lock_t> signal_lock;
    if (selected_slots.size() > 100 && selected_containers.size() > 100) {
        signal_lock = stowage->prepare_for_major_stowage_change();
    }
    int counter = 0;
    StowArbiter arbiter(stowage, document()->schedule() , 0L, current_call());
    int stowedMidTrip = 0;
    // if ctrl is pressed, we allow to 'force place', meaning we do only do the bare neccesities
    arbiter.setEssentialChecksOnly(QApplication::keyboardModifiers() & Qt::ControlModifier);
    while (!selected_slots.empty() && !selected_containers.empty()) {
        const Slot* slot = selected_slots.takeFirst();
        arbiter.setSlot(slot);
        Q_FOREACH (const Container* to_be_placed, selected_containers) {
            if (arbiter.isLegal(to_be_placed)) {
                if (stowage->container_routes()->portOfLoad(to_be_placed) != current_call()) {
                    // We might want to distingush between "Shift created" and "Late load"
                    stowedMidTrip += 1;
                }
                selected_containers.removeOne(to_be_placed);
                // Clear away any macro-stowed containers
                clear_away_macrostowed_in_slot(to_be_placed, slot, merger);
                merger->push(container_stow_command_t::createStowageDependentStowCommand(
                                    document(), m_selection_pool, to_be_placed, slot,
                                    current_call(),
                                    stowage->container_routes()->portOfDischarge(to_be_placed),
                                    MicroStowedType, false, true));
                counter++;
                break;
            }
        }
    }
    merger->setText(QStringLiteral("Move %1 containers").arg(counter));
    document()->undostack()->push(merger);
    if (stowedMidTrip) {
        document()->passiveNotifications()->addWarningWithUndo(
            stowedMidTrip == 1 ?
            QString("Stowed 1 container in the middle of its trip.") :
            QString("Stowed %1 containers in the middle of their trip.").arg(stowedMidTrip));
    }
    QList<int> included_rows;
    container_list_t*  container_list = document()->containers();
    Q_FOREACH(const Container * container, selected_containers) {
        included_rows << container_list->find_container(container);
    }
    m_selection_pool->reset_included_rows(included_rows);
}

void vessel_scene_t::move_legality_connections(VesselGraphicsItem* old_current, VesselGraphicsItem* new_current) {
    if (old_current) {
        Q_FOREACH(BaySliceGraphicsItem * slice, old_current->aft_slices() + old_current->fore_slices()) {
            if(slice) {
                Q_FOREACH(StackGraphicsItem * stack_item, slice->stack_items()) {
                    disconnect(m_selection_pool, SIGNAL(empty_changed(bool)), stack_item, SLOT(set_default_legality(bool)));
                    disconnect(m_selection_pool, SIGNAL(rows_added(QList<int>)), stack_item, SLOT(set_legality_according_to_added_container_rows(QList<int>)));
                    disconnect(m_selection_pool, SIGNAL(rows_removed(QList<int>)), stack_item, SLOT(set_legality_according_to_removed_container_rows(QList<int>)));
                    disconnect(document()->containers(), &container_list_t::container_changed,
                            stack_item, &StackGraphicsItem::set_legality_from_selection_later);
                    stack_item->set_default_legality(true);
                }
            }
        }
    }
    if (new_current) {
        const bool selection_empty = m_selection_pool->empty();
        Q_FOREACH(BaySliceGraphicsItem * slice, new_current->aft_slices() + new_current->fore_slices()) {
            if(slice) {
                Q_FOREACH(StackGraphicsItem * stack_item, slice->stack_items()) {
                    connect(m_selection_pool, SIGNAL(empty_changed(bool)), stack_item, SLOT(set_default_legality(bool)));
                    connect(m_selection_pool, SIGNAL(rows_added(QList<int>)), stack_item, SLOT(set_legality_according_to_added_container_rows(QList<int>)));
                    connect(m_selection_pool, SIGNAL(rows_removed(QList<int>)), stack_item, SLOT(set_legality_according_to_removed_container_rows(QList<int>)));
                    connect(document()->containers(), &container_list_t::container_changed,
                            stack_item, &StackGraphicsItem::set_legality_from_selection_later);
                    stack_item->set_default_legality(selection_empty);
                    stack_item->set_legality_from_selection_later();
                }
            }
        }
    }
}

QList< const Slot* > vessel_scene_t::selected_slots_from_rect(const QRectF rect, bool respectLinkForeAftSetting) {
    QSet<const Slot*> selected_slots;
    //workaround: if width==0.0, QGraphicsItem::item won't work, so we set it to something != 0.0
    QRectF new_rect = rect;
    if (new_rect.width() == 0.0) {
        new_rect.setWidth(0.1);
    }
    Q_FOREACH(QGraphicsItem * item, items(new_rect.normalized())) {
        if (BaySliceGraphicsItem* bay_slice_item = qgraphicsitem_cast<BaySliceGraphicsItem*>(item)) {
            if (bay_slice_item->call() == current_call()) {
                QList<SlotGraphicsItem*> newslots = bay_slice_item->slot_items(bay_slice_item->mapRectFromScene(new_rect).normalized(), current_call());
                Q_FOREACH(SlotGraphicsItem * slotItem, newslots) {
                    selected_slots << slotItem->slot();
                    if (m_linkForeAft && respectLinkForeAftSetting) {
                        if (slotItem->slot()->sister()) {
                            selected_slots << slotItem->slot()->sister();
                        }
                    }
                }
            }
        }
    }
    return selected_slots.toList();
}

void vessel_scene_t::set_active_call(const Call* call) {
    if (call == current_call()) {
        return;
    }
    VesselGraphicsItem* old_current = vessel_item();
    VesselGraphicsItem* new_current = vessel_item(call);
    m_current_call = call;
    m_current_painted_call = 0L;
    move_legality_connections(old_current, new_current);
    if (old_current) {
        old_current->update();
        if (m_hide_non_active_calls) {
            old_current->hide();
        }
    }
    if (new_current) {
        new_current->update();
        if (m_hide_non_active_calls) {
            new_current->show();
        }
        if (m_hide_non_active_calls) {
            setSceneRect(new_current->mapToScene(new_current->boundingRect()).boundingRect());
        }
    }
#if 0
// Debug code by Sune
#ifndef QT_NO_DEBUG
    Q_FOREACH(bay_slice_t * bay_slice, document()->vessel()->bay_slices()) {
        Q_FOREACH(stack_t * stack, bay_slice->stacks()) {
            qDebug() << stack->max_height() << "/" << stack->height(m_current_call) << "    " << stack->max_weight() << "/" << stack->weight(m_current_call);
        }
    }
#endif
#endif
    emit call_changed(call);
}

void vessel_scene_t::slot_container_moved(const Container* container, const Slot* previous, const Slot* current, const Call* call) {
    if (SlotGraphicsItem* slot_item = m_slots.value(slot_call_t(previous, call))) {
        slot_item->update();
        if (container->isoLength() != IsoCode::Twenty) {
            if (SlotGraphicsItem* sister_item = m_slots.value(slot_call_t(previous->sister(), call))) {
                sister_item->update();
            }
        }
    }
    if (SlotGraphicsItem* slot_item = m_slots.value(slot_call_t(current, call))) {
        slot_item->update();
        if (container->isoLength() != IsoCode::Twenty) {
            if (SlotGraphicsItem* sister_item = m_slots.value(slot_call_t(current->sister(), call))) {
                sister_item->update();
            }
        }
    }
}

void vessel_scene_t::slot_container_changed(const Container* container) {
    if (document_t* doc = document()) {
        if (const Slot* slot = doc->stowage()->container_position(container, current_call())) {
            if (SlotGraphicsItem* slot_item = m_slots.value(slot_call_t(slot, current_call()))) {
                slot_item->update();
            }
        }
    }
}

void vessel_scene_t::setStowageMode(mode_enum_t::mode_t mode) {
    if(m_current_mode != mode) {
        m_current_mode = mode;
        update();
    }
}

SlotGraphicsItem* vessel_scene_t::slot_item(Slot* stack_slot, Call* call) const {
    return m_slots.value(slot_call_t(stack_slot, call));
}

void vessel_scene_t::slot_painted_call_changed(Call* call) {
    m_current_painted_call = call;
}

void vessel_scene_t::set_show_equipment_id(bool state) {
    m_show_equipment_number = state;
}

void vessel_scene_t::slot_row_labels_changed(mode_enum_t::row_label_mode_t row_mode) {
    m_row_label_mode = row_mode;
    Q_FOREACH(VesselGraphicsItem * vessel_item, m_vessel_items) {
        vessel_item->update();
    }
}

void vessel_scene_t::helpEvent(QGraphicsSceneHelpEvent* event) {
    QList<QGraphicsItem*> helpitems = items(event->scenePos(), Qt::IntersectsItemShape, Qt::DescendingOrder);
    QString tooltip;
    Q_FOREACH(QGraphicsItem * item, helpitems) {
        if (BaySliceGraphicsItem* baySliceGraphicsItem = dynamic_cast<BaySliceGraphicsItem*>(item)) {
            tooltip = baySliceGraphicsItem->tooltip_text(item->mapFromScene(event->scenePos()));
        } else {
            tooltip = item->toolTip();
        }
        if (!tooltip.isEmpty()) {
            QToolTip::showText(event->screenPos(), tooltip, event->widget());
            event->setAccepted(true);
            return;
        }
    }
    event->setAccepted(false);
}

void vessel_scene_t::Slotool_applied(QRectF rect, tool_t* tool) {
    if (tool == m_toolManager->fillTool()) {
        bucket_fill_rect(rect);
    } else if (tool == m_toolManager->selectTool()) {
        select_rect(rect);
    } else if (tool == m_toolManager->macroTool()) {
        macro_select_rect(rect);
    }
}

void vessel_scene_t::select_rect(QRectF rect) {
    stowage_t* stowage = document()->stowage();
    QList<const Slot*> slots_selected = selected_slots_from_rect(rect);
    if (!(QApplication::keyboardModifiers() & Qt::ShiftModifier)) {
        m_selection_pool->clear();
    }
    QList<const Container*> selected;
    Q_FOREACH(const Slot * slot, slots_selected) {
        if (const Container* container = stowage->contents(slot, current_call()).container()) {
            if ((stowage->container_moved_at_call(container, current_call())
                    || QApplication::keyboardModifiers() & Qt::ControlModifier)) {
                selected << container;
            }
        }
    }
    emit containers_selected(selected);
}

void vessel_scene_t::macro_select_rect(QRectF rect) {
    Q_ASSERT(current_call()); // When can this happen?
    if (!m_current_painted_call || !current_call() || is_befor_or_after(current_call())) {
        return;
    }
    ange::angelstow::MacroPlan* macroStowage = document()->macro_stowage();

    // Collect all compartments selected
    QSet<Compartment*> compartmentsSelected;
    Q_FOREACH (QGraphicsItem* item, items(rect.normalized())) {
        if (BaySliceGraphicsItem* bay_slice_item = qgraphicsitem_cast<BaySliceGraphicsItem*>(item)) {
            if (bay_slice_item->call() == current_call()) {
                Q_FOREACH (SlotGraphicsItem* slot_item, bay_slice_item->slot_items(bay_slice_item->mapRectFromScene(rect).normalized(), current_call())) {
                    Stack* stack = slot_item->slot()->stack();
                    if (Compartment* compartment = bay_slice_item->baySlice()->compartmentContainingRow(stack->row(), stack->level())) {
                        compartmentsSelected << compartment;
                    } else {
                        Q_ASSERT(false);
                    }
                }
            }
        }
    }
    QList<Compartment*> compartmentsFree;
    QList<Compartment*> compartmentsSameAllocation;
    QList<Compartment*> compartmentsOtherAllocation;
    Q_FOREACH (Compartment* compartment, compartmentsSelected) {
        ange::angelstow::MacroPlan::Conflicts conflicts = macroStowage->conflicts(compartment, current_call(), m_current_painted_call);
        if (conflicts == MacroPlan::NoConflicts) {
            compartmentsFree << compartment;
        } else if (conflicts & MacroPlan::OtherConflict) {
            // ignore compartments with other conflicts
        } else if (conflicts & MacroPlan::SameAllocation) {
            compartmentsSameAllocation << compartment;
        } else if (conflicts & MacroPlan::SameLoadOtherDischarge) {
            compartmentsOtherAllocation << compartment;
        } else {
            Q_ASSERT(false);
        }
    }

    merger_command_t* mergerCommand = new merger_command_t();
    int count;
    if (compartmentsFree.isEmpty() && compartmentsOtherAllocation.isEmpty()) { // If there is nothing to paint clear same
        count = compartmentsSameAllocation.count();
        Q_FOREACH (Compartment* compartment, compartmentsSameAllocation) {
            count += 1;
            mergerCommand->push(MacroCompartmentCommand::remove(document(), compartment, current_call(), m_current_painted_call));
        }
    } else { // Clear other, set other and free
        count = compartmentsOtherAllocation.count() + compartmentsFree.count();
        Q_FOREACH (Compartment* compartment, compartmentsOtherAllocation) {
            mergerCommand->push(MacroCompartmentCommand::remove(document(), compartment, current_call(),
                                                                macroStowage->dischargeCall(compartment, current_call())));
            mergerCommand->push(MacroCompartmentCommand::insert(document(), compartment, current_call(), m_current_painted_call));
        }
        Q_FOREACH (Compartment* compartment, compartmentsFree) {
            mergerCommand->push(MacroCompartmentCommand::insert(document(), compartment, current_call(), m_current_painted_call));
        }
    }
    mergerCommand->setText(tr("Modify %1 master plan compartments").arg(count));
    document()->undostack()->push(mergerCommand);
}

void vessel_scene_t::slot_selection_changed() {
    QTimer::singleShot(0, this, SLOT(slot_update_selected_slots()));
}

void vessel_scene_t::slot_update_selected_slots() {
    // Deselect previous selection
    Q_FOREACH(SlotGraphicsItem * slot_item, m_last_selected) {
        slot_item->set_selected(false);
    }
    m_last_selected.clear();
    // Select new current selection, and record those selected
    container_list_t* containers = document()->containers();
    stowage_t* stowage = document()->stowage();
    Q_FOREACH(int row, m_selection_pool->included_rows()) {
        const Container* container = containers->at(row);
        if (const Slot* slot = stowage->container_position(container, current_call())) {
            if (SlotGraphicsItem* slot_item = m_slots.value(slot_call_t(slot, current_call()))) {
                m_last_selected << slot_item;
                slot_item->set_selected(true);
                if (container->isoLength() != IsoCode::Twenty) {
                    const Slot* sister = slot->sister();
                    if (SlotGraphicsItem* sister_item = m_slots.value(slot_call_t(sister, current_call()))) {
                        m_last_selected << sister_item;
                        sister_item->set_selected(true);
                    }
                }
            }
        }
    }
}

void vessel_scene_t::refresh_legality() {
    if (VesselGraphicsItem* current = m_vessel_items.value(current_call())) {
        Q_FOREACH(BaySliceGraphicsItem * slice, current->aft_slices() + current->fore_slices()) {
            Q_FOREACH(StackGraphicsItem * stack_item, slice->stack_items()) {
                stack_item->set_legality_from_selection_later();
            }
        }
    }
}

void vessel_scene_t::remove_call(Call* call) {
    if (!call) {
        qWarning("%s called with null call", __func__);
        Q_ASSERT(call);
        return;
    }
    if (is_after(call)) {
        return;
    }
    VesselGraphicsItem* item = m_vessel_items.take(call);
    Q_ASSERT(item);
    // Move later calls up
    const qreal height = item->boundingRect().height();
    Q_FOREACH(VesselGraphicsItem * item, m_vessel_items) {
        if (call->distance(item->call()) > 0) {
            item->moveBy(0.0, -height);
        }
    }
    for (slots_t::iterator it = m_slots.begin(), iend = m_slots.end(); it != iend;) {
        if (it.key() == call) {
            m_last_selected.remove(it.value());
            it = m_slots.erase(it);
        } else {
            ++it;
        }
    }
    setSceneRect(sceneRect().adjusted(0, 0, 0, -height));
    emit vessel_item_removed(item);
    delete item;
}

void vessel_scene_t::add_call(Call* call) {
    VesselGraphicsItem* prev_vessel_item = 0L;
    Q_FOREACH(VesselGraphicsItem * vessel_item, m_vessel_items) {
        if (vessel_item->call()->distance(call) > 0) {
            if (!prev_vessel_item || prev_vessel_item->call()->distance(vessel_item->call()) > 0) {
                prev_vessel_item = vessel_item;
            }
        }
    }
    VesselGraphicsItem* vessel_item = new VesselGraphicsItem(document(), m_selection_pool, m_macroStowageMouseTracker,
                                                             call, m_2_rows ? 2 : 1);
    QRectF vessel_scene_rect = vessel_item->mapRectToScene(vessel_item->boundingRect());
    QPointF pos = prev_vessel_item ? prev_vessel_item->pos() + QPointF(0.0, vessel_scene_rect.height()) : QPointF(0.0, 0.0);
    create_reverse_slot_index(vessel_item);
    addItem(vessel_item);
    m_vessel_items.insert(call, vessel_item);
    vessel_item->setPos(pos);
    if (m_hide_non_active_calls) {
        if (call == current_call()) {
            setSceneRect(vessel_scene_rect);
        } else {
            vessel_item->hide();
        }
    } else {
        setSceneRect(sceneRect().adjusted(0.0, 0.0, 0.0, vessel_scene_rect.height()));
    }
    Q_FOREACH(VesselGraphicsItem * vessel_item, m_vessel_items) {
        if (call->distance(vessel_item->call()) > 0) {
            vessel_item->moveBy(0.0, vessel_scene_rect.height());
        }
    }
    emit vessel_item_added(vessel_item);
}

void vessel_scene_t::applyChangeOfRotation(int previousPosition, int currentPosition) {
    VesselGraphicsItem* changedVesselItem = m_vessel_items.value(document()->schedule()->calls().at(currentPosition));
    QRectF vesselSceneRect = changedVesselItem->mapRectToScene(changedVesselItem->boundingRect());
    changedVesselItem->moveBy(0.0, vesselSceneRect.height() * (currentPosition - previousPosition));
    if (currentPosition > previousPosition) {
        for (int i = previousPosition; i < currentPosition; ++i) {
            const Call* call = document()->schedule()->calls().at(i);
            VesselGraphicsItem* interveningVesselItem = m_vessel_items.value(call);
            interveningVesselItem->moveBy(0.0, -vesselSceneRect.height());
        }
    }
    if (currentPosition < previousPosition) {
        for (int i = previousPosition; i > currentPosition; --i) {
            const Call* call = document()->schedule()->calls().at(i);
            VesselGraphicsItem* interveningVesselItem = m_vessel_items.value(call);
            interveningVesselItem->moveBy(0.0, vesselSceneRect.height());
        }
    }
}

void vessel_scene_t::create_reverse_slot_index(VesselGraphicsItem* vessel_item) {
    Q_FOREACH(QGraphicsItem * child, vessel_item->childItems()) {
        if (BaySliceGraphicsItem* bay_slice_item = qgraphicsitem_cast<BaySliceGraphicsItem*>(child)) {
            Q_FOREACH(StackGraphicsItem * stack_item, bay_slice_item->stack_items()) {
                Q_FOREACH(SlotGraphicsItem * slot_item, stack_item->slot_items()) {
                    if (Slot* vessel_slot = slot_item->slot()) {
                        m_slots.insert(slot_call_t(vessel_slot, bay_slice_item->call()), slot_item);
                    }
                }
            }
        }
    }
}

struct sort_by_y {
    bool operator()(const VesselGraphicsItem* lhs, const VesselGraphicsItem* rhs) const {
        return lhs->pos().y() < rhs->pos().y();
    }
};

void vessel_scene_t::show_2_rows(bool activated) {
    m_2_rows = activated;
    QList<VesselGraphicsItem*> vessel_items_sort_by_y = m_vessel_items.values();
    std::sort(vessel_items_sort_by_y.begin(), vessel_items_sort_by_y.end(), sort_by_y());
    qreal move_down_distance = 0;
    Q_FOREACH(VesselGraphicsItem * vessel_item, vessel_items_sort_by_y) {
        const qreal old_height = vessel_item->boundingRect().height();
        vessel_item->moveBy(0, move_down_distance);
        vessel_item->set_rows(activated ? 2 : 1);
        const qreal new_height = vessel_item->boundingRect().height();
        move_down_distance += (new_height - old_height);
    }
    set_print_bay_numbers(activated);
}

void vessel_scene_t::set_print_bay_numbers(bool on) {
    if (m_print_bay_numbers != on) {
        m_print_bay_numbers = on;
        QList<VesselGraphicsItem*> vessel_items_sort_by_y = m_vessel_items.values();
        std::sort(vessel_items_sort_by_y.begin(), vessel_items_sort_by_y.end(), sort_by_y());
        qreal move_down_distance = 0;
        QRectF scene_rect;
        Q_FOREACH(VesselGraphicsItem * vessel_item, vessel_items_sort_by_y) {
            vessel_item->moveBy(0, move_down_distance);
            const qreal old_height = vessel_item->boundingRect().height();
            vessel_item->set_print_bay_numbers(on);
            const qreal new_height = vessel_item->boundingRect().height();
            if (m_hide_non_active_calls) {
                if (vessel_item->call() == m_current_call) {
                    scene_rect = vessel_item->mapRectToScene(vessel_item->boundingRect());
                }
            } else {
                scene_rect = scene_rect.united(vessel_item->mapRectToParent(vessel_item->boundingRect()));
            }
            move_down_distance += (new_height - old_height);
        }
        setSceneRect(scene_rect);
    }
}

void vessel_scene_t::set_vessel() {
    m_slots.clear();
    QRectF scene_rect(0.0, 0.0, 0.0, 0.0);
    QList<VesselGraphicsItem*> vessel_items_sort_by_y = m_vessel_items.values();
    std::sort(vessel_items_sort_by_y.begin(), vessel_items_sort_by_y.end(), sort_by_y());
    Q_FOREACH(VesselGraphicsItem * vessel_item, vessel_items_sort_by_y) {
        vessel_item->set_vessel_from_document(document());
        QRectF br = vessel_item->mapRectToScene(vessel_item->boundingRect());
        if (scene_rect.width() < br.width()) {
            scene_rect.setWidth(br.width());
        }
        vessel_item->moveBy(-br.left(), scene_rect.bottom() - br.top());
        scene_rect.adjust(0.0, 0.0, 0.0, br.height());
        create_reverse_slot_index(vessel_item);
    }
    if (m_hide_non_active_calls) {
        VesselGraphicsItem* vesselItem = m_vessel_items.value(m_current_call);
        setSceneRect(vesselItem->mapToScene(vesselItem->boundingRect()).boundingRect());
    } else {
        scene_rect.adjust(-margin(), -margin(), 2 * margin(), 2 * margin());
        setSceneRect(scene_rect);
    }
    move_legality_connections(0L, vessel_item());
    m_last_selected.clear();
}

void vessel_scene_t::display_bulkheads_for_all_calls(bool on) {
    Q_FOREACH(VesselGraphicsItem * vessel_item, m_vessel_items) {
        vessel_item->draw_bulkheads(on);
    }
}

void vessel_scene_t::display_visibilityline_for_all_calls(bool on) {
    Q_FOREACH(VesselGraphicsItem * vessel_item, m_vessel_items) {
        vessel_item->draw_visibilityline(on);
    }
}

void vessel_scene_t::hide_non_active_calls_changed(bool hidden) {
    if (m_hide_non_active_calls != hidden) {
        m_hide_non_active_calls = hidden;
        Q_FOREACH(const Call * call, m_vessel_items.keys()) {
            if (call == current_call()) {
                m_vessel_items.value(call)->show();
                if (m_hide_non_active_calls) {
                    setSceneRect(m_vessel_items.value(call)->mapToScene(m_vessel_items.value(call)->boundingRect()).boundingRect());
                }
            } else {
                m_vessel_items.value(call)->setVisible(!m_hide_non_active_calls);
            }
        }
        if (!m_hide_non_active_calls) {
            QRectF scene_rect;
            Q_FOREACH(VesselGraphicsItem * vessel_item, m_vessel_items) {
                scene_rect = scene_rect.united(vessel_item->mapRectToParent(vessel_item->boundingRect()));
            }
            setSceneRect(scene_rect);
        } else {
            if (is_after(current_call())) {
                //Port After will fail to give a boundingRect(?), so set current call to Befor
                set_active_call(document()->schedule()->calls().first());
            }
            setSceneRect(m_vessel_items.value(current_call())->mapToScene(m_vessel_items.value(current_call())->boundingRect()).boundingRect());
        }
    }
}

void vessel_scene_t::clear_away_macrostowed_in_slot(const Container* to_be_placed, const Slot* slot, merger_command_t* command) {
    document_t* document = this->document();
    stowage_t* stowage = document->stowage();
    QList<const Container*> containers_to_clear;
    Slot* sister_slot = (to_be_placed->isoLength() != IsoCode::Twenty) ? slot->sister() : 0L;
    Q_FOREACH(Call * call, document->schedule()->between(current_call(), stowage->container_routes()->portOfDischarge(to_be_placed))) {
        slot_call_content_t contents = stowage->contents(slot, call);
        Q_ASSERT(!contents.container()  || contents.type() == MasterPlannedType);
        if (contents.container() && !containers_to_clear.contains(contents.container())) {
            containers_to_clear << contents.container();
        }
        if (sister_slot) {
            slot_call_content_t sister_contents = stowage->contents(sister_slot, call);
            Q_ASSERT(!sister_contents.container()  || sister_contents.type() == MasterPlannedType);
            if (sister_contents.container() && !containers_to_clear.contains(sister_contents.container())) {
                containers_to_clear << sister_contents.container();
            }
        }
    }
    if (!containers_to_clear.empty()) {
        container_stow_command_t* clear_command = new container_stow_command_t(document, m_selection_pool, false);
        ContainerLeg* routes = stowage->container_routes();
        Q_FOREACH(const Container * c, containers_to_clear) {
            clear_command->add_discharge(c, routes->portOfLoad(c), MicroStowedType);
        }
        command->push(clear_command);
    }
}

void vessel_scene_t::changeDisplayContainerWeights(bool display) {
    m_displayContainerWeights = display;
}

void vessel_scene_t::keyReleaseEvent(QKeyEvent* event) {
    if (event->modifiers() & Qt::ControlModifier && event->key() == Qt::Key_A) {
        stowage_t* stowage = document()->stowage();
        QList<const Container*> container_from_current_call;
        QSet<const Container*> containers = stowage->get_containers_on_board(current_call());
        Q_FOREACH(const Container * container, containers) {
            if (stowage->container_routes()->portOfLoad(container) == current_call()) {
                container_from_current_call << container;
            }
        }
        m_selection_pool->clear();
        m_selection_pool->include(container_from_current_call);
    } else {
        QGraphicsScene::keyReleaseEvent(event);
    }
}

void vessel_scene_t::setLinkForeAft(bool linkForeAft) {
    m_linkForeAft = linkForeAft;
}

QRectF vessel_scene_t::selectProblemAndReturnRects(const Problem* problem) {
    ProblemLocation location = problem->location();
    QList<const Slot*> stackslots;
    if (const ange::vessel::StackSupport* support = location.stackSupport()) {
        Q_FOREACH(Slot * slot, support->stackSlots()) {
            stackslots << slot;
        }
    } else if (location.bayRowTier().placed()) {
        const Slot* slot = document()->vessel()->slotAt(location.bayRowTier());
        stackslots << slot;
    }
    const Call* call = location.call();
    Q_ASSERT(call);
    if (call != current_call()) {
        set_active_call(call);
    }
    QRectF rect;
    QList<const Container*> containers;
    Q_FOREACH(const Slot * slot, stackslots) {
        SlotGraphicsItem* item = m_slots.value(slot_call_t(slot, call));
        BaySliceGraphicsItem* bayslice = item->stackGraphicsItem()->bay_slice_item();
        if (rect.isValid()) {
            rect = rect.united(bayslice->mapRectToScene(bayslice->boundingRect()));
        } else {
            rect = bayslice->mapRectToScene(bayslice->boundingRect());
        }
        const Container* container = document()->stowage()->contents(slot, call).container();
        if (container) {
            containers << container;
        }
    }
    m_selection_pool->clear();
    m_selection_pool->include(containers);

    return rect;
}

void vessel_scene_t::setToolManager(ToolManager* toolManager) {
    Q_ASSERT(!m_toolManager); // we should never end up in a situation where it is not part of 'initializing'
    m_toolManager = toolManager;
    connect(m_toolManager, SIGNAL(modeChanged(mode_enum_t::mode_t)), this, SLOT(setStowageMode(mode_enum_t::mode_t)));
}

#include "vesselscene.moc"
