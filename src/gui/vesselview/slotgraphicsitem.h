#ifndef SLOT_GRAPHICS_ITEM_H
#define SLOT_GRAPHICS_ITEM_H

#include <QObject>
#include <QDebug>
#include <QBrush>
#include "gui/stow.h"
#include "gui/mode.h"
#include "document/stowage/slot_call_content.h"

class pool_t;
class slot_call_content_t;
class StackGraphicsItem;
struct vessel_paint_config_t;
class stowage_t;
class document_t;

namespace ange {
namespace vessel {
class Stack;
class Slot;
}
namespace containers {
class Container;
}
namespace schedule {
class Call;
}
}

class SlotGraphicsItem : public QObject {
    Q_OBJECT
  public:
    SlotGraphicsItem(ange::vessel::Slot* slot, document_t* stowage, const pool_t* selectionPool, StackGraphicsItem* parent);

    ange::vessel::Slot* slot() const {
      return m_slot;
    }

    QString toString() const;

    /**
     * Mark slot contents as selected
     */
    void set_selected(bool selected);

    /**
     * @return true if slot contents is selected
     */
    bool selected() const {
      return m_selected;
    }

    virtual void paint(QPainter* painter, const QRectF& bounding_rect, const vessel_paint_config_t& config, bool legal, bool killed);

    /**
     * @return the maximum normal height of this slot (normally a HC height)
     */
    qreal max_height() const;

    /**
     * @return height of slot (a DC if empty or odd)
     */
    qreal height(const slot_call_content_t& slot_contents) const;

    /**
     * @return tooltip text for this item
     */
    QString tooltip_text( const ange::schedule::Call* current_call, bool show_equipment_number) const;

    /**
     * @return "main" brush for slot. Meant to be a way to quickly draw many unembellished slots
     * The slot_contents can conveniently be gotten by "slot_contents" call
     * @param legal if false, the slot is not usable for any containers in the current selection and should be drawn "disabled"
     */
    QBrush get_brush_for_contents(const slot_call_content_t& slot_contents, const bool killed, const bool legal, const vessel_paint_config_t& config) const;

    /**
     * @return slot_contents for item
     */
    slot_call_content_t slot_contents(const ange::schedule::Call* call) const;

    /**
     * @return color for container, to be turned into a brush
     */
    QColor color_for_container(const ange::containers::Container& container, const vessel_paint_config_t& config) const;

    /**
     * \return the stack item that this slot item is a part of
     */
    StackGraphicsItem* stackGraphicsItem();
    const StackGraphicsItem* stackGraphicsItem() const;

  public Q_SLOTS:
    /**
     * Schedule slot for repaint
     */
    void update();
  private:
    ange::vessel::Slot* const m_slot;
    stowage_t* m_stowage;
    document_t* m_document;
    const pool_t* m_selectionPool;
    mode_enum_t::mode_t m_mode;
    bool m_selected;
    /**
     * @return whether this container moved in call
     */
    bool moved_at_call(const ange::containers::Container* container, const ange::schedule::Call* call) const;
    void draw_overlays_high_lod(QPainter* painter, const slot_call_content_t& slot_contents, const ange::schedule::Call* current_call, const QRectF& bounding_rect, const bool invert_overlay_color) const;
    void draw_overlays_medium_lod(QPainter* painter, const slot_call_content_t& slot_contents, const ange::schedule::Call*, const QRectF& bounding_rect, const bool invert_overlay_color) const;
};

QDebug operator<<(QDebug Debug, const SlotGraphicsItem& slot_item);
QDebug operator<<(QDebug Debug, const SlotGraphicsItem*);

#endif // SLOT_GRAPHICS_ITEM_H
