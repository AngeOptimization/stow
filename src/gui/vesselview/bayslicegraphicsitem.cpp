#include "bayslicegraphicsitem.h"

#include "compartmentmousetracker.h"
#include "container_leg.h"
#include "container_list.h"
#include "document.h"
#include "stowplugininterface/macroplan.h"
#include "painter_utils.h"
#include "paintersaver.h"
#include "problem_model.h"
#include "selectionareabuilder.h"
#include "slotgraphicsitem.h"
#include "stackgraphicsitem.h"
#include "stacklabelitem.h"
#include "stackproblemgraphicsitem.h"
#include "stowage.h"
#include "stowagestack.h"
#include "vesselgraphicsitem.h"
#include "vesselpaintconfig.h"
#include "vesselscene.h"
#include "visibility_line.h"

#include <ange/containers/container.h>
#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>
#include <ange/vessel/bayslice.h>
#include <ange/vessel/compartment.h>
#include <ange/vessel/decklevel.h>
#include <ange/vessel/hatchcover.h>
#include <ange/vessel/slot.h>
#include <ange/vessel/stack.h>
#include <ange/vessel/stacksupport.h>
#include <ange/vessel/vessel.h>

#include <QApplication>
#include <QGraphicsItemAnimation>
#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QMimeData>
#include <QPainter>
#include <QStyleOption>
#include <QTimeLine>

using namespace ange::units;
using ange::containers::Container;
using ange::schedule::Call;
using ange::schedule::Schedule;
using ange::vessel::Above;
using ange::vessel::Below;
using ange::vessel::BaySlice;
using ange::vessel::Compartment;
using ange::vessel::HatchCover;
using ange::vessel::Slot;
using ange::vessel::Stack;
using ange::vessel::Vessel;

struct sort_by_z {
  bool operator()(const StackGraphicsItem* lhs, const StackGraphicsItem* rhs) const {
    return lhs->stack()->bottom().t() < rhs->stack()->bottom().t();
  }
};

struct sort_qrect_by_left_t {
  bool operator()(const QRectF& lhs, const QRectF& rhs) const {
    return lhs.left() < rhs.left();
  }
};

class SelectionPen {
    public:
        static QPen pen() {
            return SelectionPen::instance()->m_pen;
        }
    private:
        QPen m_pen;
        static SelectionPen* instance() {
            static SelectionPen self;
            return &self;
        }
        SelectionPen() {
            m_pen = QPen(Qt::yellow,1.0, Qt::CustomDashLine, Qt::RoundCap);
            QVector<qreal> dash_pattern;
            dash_pattern << 8.0 << 2.0;
            m_pen.setDashPattern(dash_pattern);
        }
};

BaySliceGraphicsItem::BaySliceGraphicsItem(ange::vessel::BaySlice* bay_slice,
                                   const Call* call,
                                   BaySliceGraphicsItem* sister,
                                   bool slice_fore,
                                   document_t* document,
                                   const pool_t* selectionPool,
                                   CompartmentMouseTracker* macroStowageMouseTracker,
                                   QGraphicsItem* itemParent):
  QGraphicsObject(itemParent),
  m_baySlice(bay_slice),
  m_call(call),
  m_fore(slice_fore),
  m_sister(sister),
  m_document(document),
  m_selectionPool(selectionPool),
  m_macroStowageMouseTracker(macroStowageMouseTracker),
  m_print_bay_numbers(false),
  m_dash_offset(0.0),
  m_timer_id(0)
{
  setObjectName(QStringLiteral("BaySliceGraphicsItem(bay=%1,%2)").arg(m_baySlice->joinedBay()).arg(m_fore?"fore":"aft"));
  m_bay_no_font = qApp->font();
  setAcceptHoverEvents(true);
  QSet<int> unique_tiers_above;
  QSet<int> unique_tiers_below;
  qreal bottom = std::numeric_limits<qreal>::infinity();
  qreal top = -std::numeric_limits<qreal>::infinity();;
  qreal tight_bottom = std::numeric_limits<qreal>::infinity();
  qreal tight_top = -std::numeric_limits<qreal>::infinity();;
  qreal left = std::numeric_limits<qreal>::infinity();;
  qreal right = -std::numeric_limits<qreal>::infinity();;
  qreal bottom_top = -std::numeric_limits<qreal>::infinity();
  qreal top_bottom = std::numeric_limits<qreal>::infinity();
  Q_FOREACH(Stack* stack, m_baySlice->stacks()) {
    StackGraphicsItem* stack_item = new StackGraphicsItem(stack, slice_fore, document, m_selectionPool, this);
    bool realSlots = false;
    Q_FOREACH(SlotGraphicsItem* item, stack_item->slot_items()) {
        if(item->slot()) {
            realSlots = true;
            break;
        }
    }
    if(!realSlots) {
        delete stack_item;
        continue;
    }
    qreal b, tight_b;
    if (stack->level()==Above) {
      m_stack_items_above << stack_item;
      b = stack->bottom().v()/meter;
      tight_b = b;
      top_bottom = qMin(top_bottom,stack->bottom().v()/meter);
      Q_FOREACH(Slot* slot, stack->stackSlots()) {
        unique_tiers_above << slot->brt().tier();
      }
    } else {
      m_stack_items_below << stack_item;
      b = stack->bottom().v()/meter - below_sinkage()-StackGraphicsItem::row_no_size();
      tight_b = stack->bottom().v()/meter - below_sinkage();
      bottom_top = qMax(bottom_top,stack->bottom().v()/meter + stack_item->max_height());
      Q_FOREACH(Slot* slot, stack->stackSlots()) {
        unique_tiers_below << slot->brt().tier();
      }
    }
    bottom = qMin(bottom, b);
    top = qMax(top, b+stack_item->max_height());
    tight_bottom = qMin(tight_bottom, tight_b);
    tight_top = qMax(tight_top, tight_b + stack_item->max_height() - StackGraphicsItem::row_no_size());
    qreal z = stack_item->stack()->bottom().t()/meter;
    left = qMin(left, z-0.5*stack_item->width());
    right = qMax(right, z+0.5*stack_item->width());
  }
  m_tiers_above = unique_tiers_above.toList();
  m_tiers_below = unique_tiers_below.toList();
  qSort(m_tiers_above);
  qSort(m_tiers_below);
  qSort(m_stack_items_above.begin(), m_stack_items_above.end(), sort_by_z());
  qSort(m_stack_items_below.begin(), m_stack_items_below.end(), sort_by_z());
  if (m_tiers_below.isEmpty()) { bottom -= 1; }

  m_stacks_bounding_rect = QRectF(left, tight_bottom, right-left, tight_top-tight_bottom);
  m_bounding_rect = QRectF(left - StackGraphicsItem::width(), bottom-StackGraphicsItem::row_no_size(), right-left+1 + StackGraphicsItem::width(), top-bottom + StackGraphicsItem::row_no_size()*2);
  m_above_tier_label_rect = QRectF(left - StackGraphicsItem::width(), top_bottom, StackGraphicsItem::width(), tight_top - top_bottom);
  m_below_tier_label_rect = QRectF(left - StackGraphicsItem::width(), tight_bottom, StackGraphicsItem::width(), bottom_top - tight_bottom);

  // Add lids
  QList<QRectF> taken_by_lids;
  Q_FOREACH(HatchCover* lid, m_document->vessel()->lids()) {
    qreal HatchCoverop = m_bounding_rect.bottom(); // Set to the bottom of the lowest stack
    qreal left = m_bounding_rect.right();
    qreal right = m_bounding_rect.left();
    bool belongs_in_bay = false;
    Q_FOREACH (const Stack* stack, lid->stacksAbove()) {
      if (stack->baySlice() == m_baySlice) {
        StackGraphicsItem* stack_item = item_for_stack(Above, stack);
        left = qMin(left, stack->bottom().t()/meter-0.5*stack_item->width());
        right = qMax(right, stack->bottom().t()/meter+0.5*stack_item->width());
        HatchCoverop = qMin(HatchCoverop, stack->bottom().v()/meter);
        belongs_in_bay = true;
      }
    }
    if (belongs_in_bay) {
      QRectF lid_rect(left, HatchCoverop - 1.0, right-left, 0.5);
      Q_FOREACH(const QRectF& rect, taken_by_lids) {
        if (rect.intersects(lid_rect)) {
          lid_rect.translate(0,-1.0);;
          break;
        }
      }
      taken_by_lids << lid_rect.adjusted(-0.05,0,0.05,0);
      m_lids << lid_rect;
    }
  }
  qSort(m_lids.begin(), m_lids.end(), sort_qrect_by_left_t());
  Q_FOREACH(StackGraphicsItem* item, m_stack_items_above) {
      QPair<QRectF,QRectF> areas = stackLabelAreas(item);
      m_stackProblemAreas.insert(item, areas.first);
      m_stackLabelAreas.insert(item, areas.second);
  }
  Q_FOREACH(StackGraphicsItem* item, m_stack_items_below) {
      QPair<QRectF,QRectF> areas = stackLabelAreas(item);
      m_stackProblemAreas.insert(item, areas.first);
      m_stackLabelAreas.insert(item, areas.second);
  }

  connect(document->macro_stowage(), SIGNAL(compartmentChanged(const ange::vessel::Compartment*)), SLOT(slot_compartment_changed(const ange::vessel::Compartment*)));
  connect(m_macroStowageMouseTracker, &CompartmentMouseTracker::compartmentChanged, this, &BaySliceGraphicsItem::slot_compartment_changed);
  connect(document->visibility_line(), SIGNAL(repaint_stowage_stack_labels()), SLOT(update_slot()));
}

void BaySliceGraphicsItem::update_slot()
{
  update();
}

void BaySliceGraphicsItem::turn_on_bay_numbers(bool on) {
  if (m_print_bay_numbers != on) {
    prepareGeometryChange();
    //need to save vertical space here
    const qreal bay_number_height = 0.2 * QFontMetricsF(m_bay_no_font).lineSpacing();
    qreal sign = on ? 1.0 : -1.0;
    if (m_fore) {
      m_bounding_rect.adjust(0.0, 0.0, 0.0, sign*bay_number_height);
    } else {
      m_bounding_rect.adjust(0.0, -sign*bay_number_height, 0.0, 0.0);
    }
    m_print_bay_numbers = on;
  }
}

void BaySliceGraphicsItem::timerEvent(QTimerEvent* event)
{
  if (QApplication::activeWindow()) {
    Q_FOREACH(StackGraphicsItem* stack_item, stack_items()) {
      Q_FOREACH(SlotGraphicsItem* slot_item, stack_item->slot_items()) {
        if (slot_item->selected()) {
          m_dash_offset += 1.0;
          if (m_dash_offset >= 10.0) {
            m_dash_offset = 0.0;
          }
          update();
          return;
        }
      }
    }
    m_timer_id = 0;
    killTimer(event->timerId()); // Nothing selected, kill timer
  }
  QObject::timerEvent(event);
}

void drawTierNumbers(QPainter* painter, QRectF labelArea, QList<int> tiers) {
    const double dcHeight = ange::containers::IsoCode::physicalHeight(ange::containers::IsoCode::DC) / meter;
    QRectF label(labelArea.left(), labelArea.top(),labelArea.width(), dcHeight);
    Q_FOREACH(int tier, tiers) {
        ange::painter_utils::draw_text_in_rect(painter, label, QString::number(tier), painter->fontMetrics().boundingRect("99."));
        label.moveBottom(label.bottom() + dcHeight);
    }
}

/**
 * @return the size of the text labels in the scan plan, in particular bay, tier and row numbers
 * This ensures that the label sizes vary in a consistent way when zooming in and out.
 * The bay number text rect size is used as the reference. Row and tier numbers are smaller (0.8)
 * and should disappear below lod 2.67.
 */
qreal BaySliceGraphicsItem::levelOfDetailBasedBayNumberTextRectSize(const PrintMode mode, qreal levelOfDetail) {
    //values obtained by simple graphical inspection / trial & error such as to
    // 1. ensure a fixed minimum size for bay numbers, i.e. they should be readable
    // 2. ensure a fixed maximum size at high zoom levels (no need to waste space)
    // 3. have the transitions be smooth
    if(mode.highDetailPrint) {
        return 2;
    }
    return levelOfDetail > 2.67 ? (levelOfDetail > 10.0 ? 30.0/levelOfDetail : 3.0) : 8.0/levelOfDetail;
}

void BaySliceGraphicsItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* , QWidget* widget) {
  // Setup paint configuration
  vessel_scene_t* scene = qobject_cast<vessel_scene_t*>(this->scene());
  if (!scene) {
    return;
  }
  vessel_paint_config_t config(scene->row_label_mode());
  if (widget) {
    config.paint_mode = widget->property("paint-mode").value<paint_mode_t>();
  }
  config.selected_call = scene->current_call();
  config.call = m_call ? m_call : config.selected_call;
  config.printMode = scene->printMode();
  QTransform tf = painter->transform();
  config.lod = QStyleOptionGraphicsItem::levelOfDetailFromTransform(tf);
  config.mode = scene->current_mode();
  config.displayContainerWeight = scene->displayContainerWeights();

  // Lids
  // About the transformation: If we don't do the trick below, the rectangles bounce up and down in size as we zoom
  painter->setTransform(QTransform());
  for (int i=0; i<m_lids.size(); ++i) {
    QRectF mapped_lid = tf.mapRect(m_lids[i]);
    mapped_lid.moveTopLeft(QPointF(std::floor(mapped_lid.left()), std::floor(mapped_lid.top())));
    painter->fillRect(mapped_lid, i%2==0?QColor(0,0,255):QColor(128,128,255) );
  }
  painter->setTransform(tf);

  // Above
  SelectionAreaBuilder selection_area_builder(1.0/config.lod);
  for (int i=0; i<m_stack_items_above.size(); ++i) {
    PainterSaver saver(painter);
    draw_stack_helper(painter, i>0 ? m_stack_items_above[i-1]:0L, m_stack_items_above[i], (i<m_stack_items_above.size()-1)?m_stack_items_above[i+1]:0L, config, selection_area_builder);
  }

  // Below
  for (int i=0; i<m_stack_items_below.size(); ++i) {
    PainterSaver saver(painter);
    draw_stack_helper(painter, i>0 ? m_stack_items_below[i-1]:0L, m_stack_items_below[i], (i<m_stack_items_below.size()-1)?m_stack_items_below[i+1]:0L, config, selection_area_builder);
  }

  // Draw selection
  if (!selection_area_builder.empty()) {
    PainterSaver saver(painter);
    QPen selection_pen = SelectionPen::pen();
    selection_pen.setWidthF(3.0/config.lod);
    selection_pen.setDashOffset(m_dash_offset);
    QPainterPath path = selection_area_builder.result();
    painter->setBrush(QBrush());
    QPen alternate(Qt::black);
    alternate.setWidthF(selection_pen.widthF());
    painter->setPen(alternate);
    painter->drawPath(path);
    painter->setPen(selection_pen);
    painter->drawPath(path);
  }
  // Bay numbers
  if (m_print_bay_numbers) {
    PainterSaver saver(painter);
    m_bay_no_font.setBold(config.lod >= 2.67);
    painter->setFont(m_bay_no_font);
    //need to save vertical space here
    //see also BaySliceGraphicsItem::turn_on_bay_numbers(bool on)
    const QTextOption text_option = Qt::AlignHCenter | Qt::AlignVCenter;
    const qreal bayNumberHeight = levelOfDetailBasedBayNumberTextRectSize(config.printMode,config.lod);
    const QRectF bayNumberRect = m_fore
                        ? QRectF(m_bounding_rect.left(), m_bounding_rect.bottom()-bayNumberHeight, m_bounding_rect.width(), bayNumberHeight)
                        : QRectF(m_bounding_rect.left(), m_bounding_rect.top(), m_bounding_rect.width(), bayNumberHeight);
    // painter->drawRect(rect);
    //Note: we don't support the (theoretical) case of a 40' bay and a single 20' bay
    QString bayNumberString;
    if(m_fore) {
        if(m_baySlice->bays().length() == 3) {
            bayNumberString = QString("%2 (%1)").arg(m_baySlice->bays().at(0)).arg(m_baySlice->bays().at(1));
        } else {
            bayNumberString = QString("%1").arg(m_baySlice->bays().first());
        }
    } else {
        bayNumberString = QString("%1").arg(m_baySlice->bays().last());
    }
    ange::painter_utils::draw_text_in_rect(painter, bayNumberRect, bayNumberString);
  }
  // draw visibility lines
  if(scene->vessel_item() && scene->vessel_item()->get_draw_visibilityline_status()) {
    if(!baySlice()->stacks().isEmpty() && m_document->visibility_line() ) {
      Length bay_lcg = baySlice()->stacks().front()->bottom().l();
      Length observer_lcg = m_document->vessel()->observerLcg();
      if( bay_lcg > observer_lcg) {
        Length visibility_height = m_document->visibility_line()->get_visibility_maxheight_coordinate(call(),baySlice());
        if(!std::isinf(visibility_height/meter) && visibility_height/meter<stacks_bounding_rect().bottom()){
            PainterSaver saver(painter);
          double PI = std::acos(-1);
          Length dangerzone = 2* std::tan(PI/18.0)*(bay_lcg-observer_lcg);
          QPen pen = QPen();
          pen.setColor(QColor(128,128,255));
          pen.setWidth(0);
          pen.setStyle(Qt::DashLine);
          painter->setPen(pen);
          painter->drawLine(QPointF(stacks_bounding_rect().left(),visibility_height/meter),QPointF(stacks_bounding_rect().right(),visibility_height/meter));
          pen.setColor(Qt::red);
          painter->setPen(pen);
          Length danger_margin;
          if(stacks_bounding_rect().width()<dangerzone/meter){
            danger_margin=0.0*meter;
          } else {
            danger_margin = (stacks_bounding_rect().width()*meter-dangerzone)/2.0;
          }
          painter->drawLine(QPointF(stacks_bounding_rect().left()+danger_margin/meter,visibility_height/meter),QPointF(stacks_bounding_rect().right()-danger_margin/meter,visibility_height/meter));
        }
      }
    }
  }
    if(config.lod >= 2.67 || config.printMode.highDetailPrint) {
        const qreal tierNumberLabelWidth = 0.8*levelOfDetailBasedBayNumberTextRectSize(config.printMode,config.lod);
        m_below_tier_label_rect.setWidth(tierNumberLabelWidth);
        m_above_tier_label_rect.setWidth(tierNumberLabelWidth);
        drawTierNumbers(painter, m_below_tier_label_rect, m_tiers_below);;
        drawTierNumbers(painter, m_above_tier_label_rect, m_tiers_above);;
    }


#if 0
    // draw bounding rect
  {
      QPen greenPen(Qt::green,0);
    painter->setPen(greenPen);
    painter->setBrush(QBrush());
    painter->drawRect(boundingRect());
      QPen redPen(Qt::red,0);
    painter->setPen(redPen);
    painter->drawRect(m_stacks_bounding_rect);
  }
#endif
}


void BaySliceGraphicsItem::start_marching_ants()
{
  if (m_timer_id == 0) {
    m_timer_id = startTimer(200);
  }
}

QPair<QRectF,QRectF> BaySliceGraphicsItem::stackLabelAreas(StackGraphicsItem* stackitem) {
    const double stack_bottom_z = stackitem->stack()->bottom().t()/meter;
    qreal left = stack_bottom_z - 0.5*stackitem->width();
    qreal width = stackitem->width();
    QRectF iconArea;
    QRectF textArea;

    if(stackitem->stack()->level() == Above) {
        iconArea = QRectF(left, m_stacks_bounding_rect.bottom(), width, StackGraphicsItem::row_no_size());
        textArea = QRectF(left, m_stacks_bounding_rect.bottom() + StackGraphicsItem::row_no_size(), width, StackGraphicsItem::row_no_size());
    } else {
        iconArea = QRectF(left, m_stacks_bounding_rect.top() - StackGraphicsItem::row_no_size(), width, StackGraphicsItem::row_no_size());
        textArea = QRectF(left, m_stacks_bounding_rect.top() - 2*StackGraphicsItem::row_no_size(), width, StackGraphicsItem::row_no_size());
    }
    return QPair<QRectF,QRectF>(iconArea,textArea);
}

void BaySliceGraphicsItem::draw_stack_helper(QPainter* painter, StackGraphicsItem* last_stack_item,
                                             StackGraphicsItem* stack_item, StackGraphicsItem* next_stack_item,
                                             const vessel_paint_config_t& config,
                                             SelectionAreaBuilder& seletion_area_builder) {
  Length height = stack_item->height(config.call);
  const bool above = stack_item->stack()->level() == Above;
  const double stack_bottom_z = stack_item->stack()->bottom().t()/meter;
  const bool collapse_with_last = last_stack_item && (std::abs(last_stack_item->stack()->bottom().t()/meter - stack_bottom_z)-stack_item->width()) *config.lod < 2.0;
  const bool collapse_with_next = next_stack_item && (std::abs(next_stack_item->stack()->bottom().t()/meter - stack_bottom_z)-next_stack_item->width())*config.lod < 2.0;
  qreal width;
  qreal left;
  bool highDetail = config.lod >= 1.5;
  if (collapse_with_last) {
    left = 0.5*(stack_bottom_z + last_stack_item->stack()->bottom().t()/meter);
    if (collapse_with_next) {
      width = 0.5*(next_stack_item->stack()->bottom().t()/meter - last_stack_item->stack()->bottom().t()/meter);
    } else {
      width = 0.5*(stack_bottom_z - last_stack_item->stack()->bottom().t()/meter + stack_item->width());
    }
  } else {
    left = stack_bottom_z - 0.5*stack_item->width();
    if (collapse_with_next) {
      width = 0.5*(next_stack_item->stack()->bottom().t()/meter - stack_bottom_z + stack_item->width());
    } else {
      width = stack_item->width();
    }
  }
  QRectF stack_bounding_rect(left,stack_item->stack()->bottom().v()/meter-(above?0.0:(below_sinkage()+StackGraphicsItem::row_no_size())),
                              width, height/meter+StackGraphicsItem::row_no_size());
    {
        painter->save();
        stack_item->stackProblemGraphicsItem()->paint(painter, m_stackProblemAreas.value(stack_item), config);
        stack_item->stack_label_item()->paint(painter, m_stackLabelAreas.value(stack_item), config);

        painter->restore();
    }

  painter->save();
  stack_item->paint(painter, stack_bounding_rect, config, seletion_area_builder);
  painter->restore();
    QPen darkgreypen = painter->pen();
    darkgreypen.setWidth(0);
    darkgreypen.setColor(Qt::darkGray);
  painter->setPen(darkgreypen);
  qreal y = stack_item->stack()->bottom().v()/meter - (above?0.0:below_sinkage());
  if(highDetail) {
    height = height - stack_item->shadowOddSlotHeight()*meter;
    y = y + stack_item->shadowOddSlotHeightBottom();
  }
  if (collapse_with_last) {
    const qreal x = left;
    qreal last_y = last_stack_item->stack()->bottom().v()/meter - (above?0.0:below_sinkage());
    Length last_height = last_stack_item->height(config.call);
    if(highDetail) {
        last_y = last_y + last_stack_item->shadowOddSlotHeightBottom();
        last_height = last_height - last_stack_item->shadowOddSlotHeight()*meter;
    }
    if (highDetail || last_stack_item->stack()->compartment() != stack_item->stack()->compartment()) {
        qreal linetop = qMax(y+height/meter,last_y+last_height/meter);
        qreal linebottom = qMin(y,last_y);
        if(!qFuzzyCompare(linetop,linebottom)) {
            painter->drawLine(QLineF(x, linebottom, x, linetop));
        }
    } else {
      if ( qAbs(last_y-y) > 1.0/config.lod ) {
        painter->drawLine(QLineF(x,qMin(last_y,y), x, qMax(last_y,y)));
      }
      const qreal top_y = y+height/meter;
      const qreal last_top_y = last_height/meter+ last_y;
      if ( qAbs(last_top_y-top_y) > 1.0/config.lod ) {
        painter->drawLine(QLineF(x,qMin(last_top_y,top_y), x, qMax(last_top_y,top_y)));
      }
    }
  } else {
    // Draw line left of bay slice
    if(!qFuzzyIsNull(height/meter)) {
        const qreal x = left;
        painter->drawLine(QLineF(x,y,x,y+height/meter));
    }
  }
  //Draw line right of bay slice
  if (!collapse_with_next) {
    if(!qFuzzyIsNull(height/meter)) {
        const qreal x = left + width;
        painter->drawLine(QLineF(x,y,x,y+height/meter));
    }
  }
}

QRectF BaySliceGraphicsItem::boundingRect() const {
  return m_bounding_rect;
}

QPair<BaySliceGraphicsItem*, BaySliceGraphicsItem*> BaySliceGraphicsItem::create_bay_slice_pair(
        BaySlice* bay_slice, const Call* call, document_t* document, const pool_t* selectionPool,
        CompartmentMouseTracker* macroStowageMouseTracker, QGraphicsItem* itemParent) {
    QPair<BaySliceGraphicsItem*, BaySliceGraphicsItem*> rv;
    rv.first = new BaySliceGraphicsItem(bay_slice, call, 0L, true, document, selectionPool, macroStowageMouseTracker, itemParent);
    if(rv.first->stack_items().isEmpty()) {
        delete rv.first;
        rv.first=0;
    }
    rv.second = new BaySliceGraphicsItem(bay_slice, call, rv.first, false, document, selectionPool, macroStowageMouseTracker, itemParent);
    if(rv.second->stack_items().isEmpty()) {
        delete rv.second;
        rv.second=0;
    }
    if(bay_slice->bays().length()==1) {
        const int baySliceNumber = bay_slice->bays().at(0);
        if(!(baySliceNumber % 2)) {
            //don't draw the aft part of a pure 40' bay
            delete rv.second;
            rv.second=0;
        }
    }
    if(rv.first) {
        rv.first->m_sister = rv.second;
    }
    Q_ASSERT(rv.first || rv.second); // at least one of them must be there.
    if(!rv.first) {
        rv.first = rv.second;
        rv.second = 0;
        rv.first->m_fore = true; // for all painting purposes, this is a fore thing.
    }
    return rv;
}

QList< SlotGraphicsItem* > BaySliceGraphicsItem::slot_items(const QRectF& rect, const ange::schedule::Call* call) const {
  Q_ASSERT(call == m_call); // TODO remove argument and use m_call or document when when there is difference
  QList< SlotGraphicsItem* > rv;
  Q_FOREACH(StackGraphicsItem* stack_item, m_stack_items_above+m_stack_items_below) {
    Stack* stack = stack_item->stack();
    if (rect.left() < stack->bottom().t()/meter + 0.5*stack_item->width() && rect.right() > stack->bottom().t()/meter - 0.5*stack_item->width()) {
      // Stack hit horizontally
      qreal stack_bottom = stack->bottom().v()/meter - (stack->level() == Above ? 0.0 : (below_sinkage()));
      if (rect.bottom() > stack_bottom && rect.top() < stack_bottom + stack_item->max_height()) {
        // Stack hit vertically
        const qreal start_height = rect.top() - stack_bottom;
        const qreal end_height = rect.bottom() - stack_bottom;
        qreal height = 0.0;
        Q_FOREACH(SlotGraphicsItem* slot_item, stack_item->slot_items()) {
          const qreal sheight = slot_item->height(slot_item->slot_contents(call));
          if (height > end_height) {
            break;
          }
          height += sheight;
          if (height > start_height && slot_item->slot()) {
            rv << slot_item;
          }
        }
      }
    }
  }
  return rv;
}

QString BaySliceGraphicsItem::tooltip_text( const QPointF& pos ) const {
  const ange::schedule::Call* call = 0L;
  bool show_equipment_number = false;
  mode_enum_t::mode_t mode = mode_enum_t::MICRO_MODE;
  if (vessel_scene_t* vessel_scene = qobject_cast<vessel_scene_t*>(scene())) {
    call = m_call ? m_call : vessel_scene->current_call();
    show_equipment_number = vessel_scene->show_equipment_number();
    mode = vessel_scene->current_mode();
  }
  if (!call) {
    return QString();
  }
  Q_FOREACH(StackGraphicsItem* stack_item, m_stack_items_above+m_stack_items_below) {
    Stack* stack = stack_item->stack();
    if(m_stackProblemAreas.value(stack_item).contains(pos)) {
        return stack_item->stackProblemGraphicsItem()->tooltip_text(call);
    } else if(m_stackLabelAreas.value(stack_item).contains(pos)) {
        return stack_item->stack_label_item()->tooltip_text(call);
    }
    if (pos.x() < stack->bottom().t()/meter + 0.5*stack_item->width() && pos.x() > stack->bottom().t()/meter - 0.5*stack_item->width()) {
      // Stack hit horizontally
      qreal stack_bottom = stack->bottom().v()/meter - (stack->level() == Above ? 0.0 : (below_sinkage()));
      if (pos.y() > stack_bottom && pos.y() < stack_bottom + stack_item->max_height()) {
        if (mode == mode_enum_t::MACRO_MODE) {
          return tooltip_text_for_compartment(m_baySlice->compartmentContainingRow(stack->row(), stack->level()));
        }
        // Stack hit vertically
        const qreal pos_height = pos.y() - stack_bottom;
        qreal height = 0.0;
        Q_FOREACH(SlotGraphicsItem* slot_item, stack_item->slot_items()) {
          height += slot_item->height(slot_item->slot_contents(call));
          if (height > pos_height) {
            return slot_item->tooltip_text(call, show_equipment_number);
          }
        }
      }
    }

  }
  Q_FOREACH(const QRectF& lid, m_lids) {
    if (lid.contains(pos)) {
      return tr("Lid");
    }
  }
  return QString();
}

void BaySliceGraphicsItem::hoverEnterEvent( QGraphicsSceneHoverEvent* event ) {
  QGraphicsItem::hoverEnterEvent( event );
}

void BaySliceGraphicsItem::hoverLeaveEvent( QGraphicsSceneHoverEvent* event ) {
  QGraphicsItem::hoverLeaveEvent( event );
  m_macroStowageMouseTracker->clearTentative();
}

void BaySliceGraphicsItem::hoverMoveEvent( QGraphicsSceneHoverEvent* event ) {
  const ange::schedule::Call* call = 0L;
  const ange::schedule::Call* currently_painted_call  = 0L;
  if (vessel_scene_t* vessel_scene = qobject_cast<vessel_scene_t*>(scene())) {
    call = m_call ? m_call : vessel_scene->current_call();
    currently_painted_call = vessel_scene->current_painted_call();
  }
  if (!call || !currently_painted_call) {
    return;
  }
  const QPointF& pos = event->pos();
  Q_FOREACH(StackGraphicsItem* stack_item, m_stack_items_above+m_stack_items_below) {
    Stack* stack = stack_item->stack();
    if (pos.x() < stack->bottom().t()/meter + 0.5*stack_item->width() && pos.x() > stack->bottom().t()/meter - 0.5*stack_item->width()) {
      // Stack hit horizontally
      qreal stack_bottom = stack->bottom().v()/meter - (stack->level() == Above ? 0.0 : (below_sinkage()));
      if (pos.y() > stack_bottom && pos.y() < stack_bottom + stack_item->height(call)/meter) {
        // Stack hit vertically
        if (Compartment* compartment = m_baySlice->compartmentContainingRow(stack->row(), stack->level())) {
          m_macroStowageMouseTracker->setTentative(currently_painted_call, compartment);
        }
        return;
      }
    }
  }
  // No stacks hovered... set tentative to null
  m_macroStowageMouseTracker->clearTentative();
}

void BaySliceGraphicsItem::slot_compartment_changed( const ange::vessel::Compartment* compartment ) {
  Q_FOREACH(Compartment* c, m_baySlice->compartments()) {
    if (c == compartment) {
      update();
      return;
    }
  }
}

const ange::schedule::Call* BaySliceGraphicsItem::call() const {
  if (m_call) {
    return m_call;
  } else {
    if (vessel_scene_t* vessel_scene = qobject_cast<vessel_scene_t*>(scene())) {
      return vessel_scene->current_call();
    }
  }
  return 0L;
}

QString BaySliceGraphicsItem::tooltip_text_for_compartment(ange::vessel::Compartment* compartment) const {
  if (compartment) {
    unsigned teu = 0;
    Q_FOREACH(const Stack* stack, compartment->stacks()) {
      if (stack->stackSupport20Aft() || stack->stackSupport20Fore()) {
        if (const ange::vessel::StackSupport* ss = stack->stackSupport20Fore()) {
          teu += ss->stackSlots().size();
        }
        if (const ange::vessel::StackSupport* ss = stack->stackSupport20Aft()) {
          teu += ss->stackSlots().size();
        }
      } else if (const ange::vessel::StackSupport* ss = stack->stackSupport40()) {
        teu += 2*ss->stackSlots().size();
      }
    }
    const Call* compartmentLoadCall = m_document->macro_stowage()->loadCall(compartment, call());
    const Call* compartmentDischCall = m_document->macro_stowage()->dischargeCall(compartment, call());
    const QString polString = compartmentLoadCall == 0L? "" : "<br>POL: " + compartmentLoadCall->uncode();
    const QString podString = compartmentLoadCall == 0L? "" : "<br>POD: " + compartmentDischCall->uncode();
    return compartment->objectName() + "<br>Capacity: " + QString::number(teu)+" teu" + polString + podString;
  }
  return QString();
}

StackGraphicsItem* BaySliceGraphicsItem::item_for_stack(ange::vessel::DeckLevel level, const ange::vessel::Stack* stack) const {
  const QList<StackGraphicsItem*> stack_items = level == Above ? m_stack_items_above : m_stack_items_below;
  Q_FOREACH(StackGraphicsItem* stack_item, stack_items) {
    if (stack_item->stack() == stack) {
      return stack_item;
    }
  }
  return 0L;
}

#include "bayslicegraphicsitem.moc"
