#include "stackproblemgraphicsitem.h"
#include "stackgraphicsitem.h"
#include "vesselpaintconfig.h"
#include <document/problems/problem_model.h>
#include <stowplugininterface/problem.h>
#include <ange/vessel/stack.h>
#include <QIcon>
#include <QPainter>

using ange::vessel::Stack;
using ange::angelstow::Problem;

StackProblemGraphicsItem::StackProblemGraphicsItem(problem_model_t* problems, const Stack* vessel_stack, bool fore,
                                                   StackGraphicsItem* parent)
  : QObject(parent), m_problems(problems), m_stack(vessel_stack), m_fore(fore)
{
    setObjectName(QStringLiteral("StackProblemGraphicsItem(%1)").arg(m_stack->objectName()));
}

QList< Problem* > StackProblemGraphicsItem::findProblems(const ange::schedule::Call* call) const {
    const ange::vessel::StackSupport* sup40 = m_stack->stackSupport40();
    const ange::vessel::StackSupport* sup20 = m_fore ? m_stack->stackSupport20Fore() : m_stack->stackSupport20Aft();
    QList<Problem*> problems;
    if(sup20) {
        problems << m_problems->problemsForCallStackSupport(call, sup20);
    }
    if(sup40) {
        QList<Problem*> problems40 = m_problems->problemsForCallStackSupport(call, sup40);
        Q_FOREACH(Problem* problem, problems40) {
            if(!problems.contains(problem)) {
                problems << problem;
            }
        }
    }
    return problems;

}


void StackProblemGraphicsItem::paint(QPainter* painter, const QRectF& boundingRect, const vessel_paint_config_t& config) {
    QList<Problem*> problems = findProblems(config.call);
    if(!problems.isEmpty()) {
        Q_FOREACH(Problem* problem, problems) {
            QIcon icon = problem->icon();
            if(!icon.isNull()) {
                painter->save();
                painter->translate(boundingRect.left(), boundingRect.bottom());
                painter->scale(boundingRect.width()/64, -boundingRect.height()/64);
                icon.paint(painter, QRect(0,0,64,64));
                painter->restore();
            }
        }
    }
#if 0
    painter->setPen(Qt::magenta);
    painter->drawRect(boundingRect);
#endif
}

QString StackProblemGraphicsItem::tooltip_text(const ange::schedule::Call* call) const {
    QList<Problem*> problems = findProblems(call);
    if(problems.isEmpty()) {
        return QString();
    }
    QString toolTip = QStringLiteral("<table>");
    Q_FOREACH(Problem* problem,problems) {
        toolTip += QStringLiteral("<tr>");
        toolTip += QStringLiteral("<td>");
        toolTip += Problem::severityToString(problem->severity());
        toolTip += QStringLiteral("</td>");
        toolTip += QStringLiteral("<td>");
        toolTip += problem->details();
        toolTip += QStringLiteral("</td>");
        toolTip += QStringLiteral("</tr>");

    }
    toolTip += QStringLiteral("</table>");
    return toolTip;

}



#include "stackproblemgraphicsitem.moc"
