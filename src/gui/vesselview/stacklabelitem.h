
#ifndef STACK_LABEL_ITEM_H
#define STACK_LABEL_ITEM_H

#include <ange/containers/isocode.h>
#include <QRectF>
#include <QObject>
#include <ange/units/units.h>

class StowageStack;
class QPainter;
struct vessel_paint_config_t;
class stowage_t;
class StackGraphicsItem;

namespace ange {
namespace schedule {

class Call;
}

namespace vessel {
class Stack;
}
}

class stack_label_item_t : public QObject {
  Q_OBJECT
  public:
    stack_label_item_t(stowage_t* stowage, ange::vessel::Stack* vessel_stack, bool fore, StackGraphicsItem* parent);
    void paint(QPainter* painter, const QRectF& boundingRect, const vessel_paint_config_t& config);
    /**
     * tool tip text
     */
    QString tooltip_text( const ange::schedule::Call* call) const;

  private:
    stowage_t* m_stowage;
    ange::vessel::Stack* m_stack;
    bool m_fore;
    private:
        qreal calc_height( StowageStack* stowage_stack) const;
        const ange::units::Mass calc_weight( StowageStack* stowage_stack, ange::containers::IsoCode::IsoLength length) const;
        ange::units::Mass get_maxweight( StowageStack* stowage_stack, ange::containers::IsoCode::IsoLength length) const;
        qreal get_maxheight( StowageStack* stowage_stack) const;
        QString rowname( StowageStack* stowage_stack);
        void paintText(QPainter* painter, const QRectF& boundingRect, const vessel_paint_config_t& config);
};


#endif // STACK_LABEL_ITEM_H
