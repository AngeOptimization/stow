#include "selectionareabuilder.h"
#include <QtGlobal>
#include <limits>

SelectionAreaBuilder::SelectionAreaBuilder(qreal tolerance)
 : m_tolerance(tolerance),
   m_current_left(-std::numeric_limits<qreal>::infinity()),
   m_current_right(-std::numeric_limits<qreal>::infinity()),
   m_next_open_index(-1) // -1 will cause an assert if add_interval is called before an begin_stack
{
//   qDebug() << __func__;
  m_vertices << QPointF(0.0,0.0); // dummy, to avoid vertex 0

}

bool SelectionAreaBuilder::empty() const
{
  return m_edges.empty();
}

void SelectionAreaBuilder::add_interval(qreal bottom, qreal top)
{
  /* Algorithm
  1 vertex_bottom = add_vertex_to_common_edge(bottom)
  2. vertex_top = add_vertex_to_common_edge(top)
  2a. last_open_vertex = vertex_bottom
  3. for each vertex v in current_common_edge with bottom< y <= top
  3.1. if v has an out_edge e to vertex w in the common edge
  3.1.1. delete e
  3.2. else
  3.2.1. add_edge from w->vertex_bottom
  4. add vertices s=(vertex_bottom.y,right) and t=(vertex_top.y, right)
  5. add edges vertex_bottom->s, s->t, t->vertex_top
  6. add vertices s and t to next_common_edge
  */
  Q_ASSERT(bottom<top);
//   qDebug() << __func__ << "common edge=" << m_current_open_edge << m_next_open_index << bottom << "->" << top;
  int vertex_bottom = add_vertex_to_common_edge(bottom);
  int vertex_top = add_vertex_to_common_edge(top);
  int last_open_vertex = vertex_bottom;
  for (; m_next_open_index < m_current_open_edge.size(); ++m_next_open_index) {
    const int v = m_current_open_edge[m_next_open_index];
    QPointF v_pos = m_vertices[v];
    if (v_pos.y() >= top + m_tolerance) {
      break; // Done.
    }
    if (bottom + m_tolerance < v_pos.y()) {
      // 3. for each vertex v in current_common_edge with bottom<=y < top
      bool has_opposite_out_edge = false;
      const int w = m_current_open_edge.value(m_next_open_index - 1, -1);
      QMultiHash<int,int>::iterator it = m_edges.find(w);
      while (it != m_edges.end() && it.key() == w) {
        if ( v == it.value()) {
//           qDebug() << __func__ << " erase " << it.key() << "->" << it.value() << " star " << m_edges.values(w) << w;
          m_edges.erase(it);
          has_opposite_out_edge = true;
          break;
        }
        ++it;
      }
      // 3.2.1. add_edge from w->v
      if (!has_opposite_out_edge) {
//         qDebug() << __func__ << "edge" << m_vertices[v] << "->" << m_vertices[last_open_vertex];
        m_edges.insert(v, last_open_vertex);
      }
      last_open_vertex = v;
    }
  }
  // 4. add vertices s=(vertex_bottom.y,right) and t=(vertex_top.y, right)
  int vertex_top_right = m_vertices.size(); m_vertices << QPointF(m_current_right, top);
  int vertex_bottom_right = m_vertices.size(); m_vertices << QPointF(m_current_right, bottom);
  // 5. add edges vertex_bottom->s, s->t, t->vertex_top
  m_edges.insert(vertex_bottom, vertex_bottom_right);
  m_edges.insert(vertex_bottom_right, vertex_top_right);
  m_edges.insert(vertex_top_right, vertex_top);
#if 0 // Debug paths after adding egde
  int path_length = 0;
  qDebug() << m_edges;
  for (int v = vertex_bottom; (v != vertex_bottom && path_length < 30) || path_length == 0; v = m_edges.value(v, -1)) {
    Q_ASSERT(v!=-1);
//     qDebug() << __func__ << "->" << v << m_vertices[v];
    ++path_length;
  }
#endif
  // 6. add vertices s and t to next_common_edge
  m_next_common_edge << vertex_bottom_right << vertex_top_right;
}

int SelectionAreaBuilder::add_vertex_to_common_edge(qreal y)
{
  /*
   *  1. if exists vertex w where |y-w.y| < tolerance
   *  1.1. return w
   * 2. return new vertex v = (y, left)
   */
  for (int i = m_next_open_index; i < m_current_open_edge.size(); ++i) {
    const int w = m_current_open_edge[i];
    const qreal w_y = m_vertices[w].y();
    if (w_y + m_tolerance > y) {
      if (w_y - m_tolerance < y) {
        return w;
      } else {
        const int rv = m_vertices.size();
        m_vertices << QPointF(m_current_left, y);
        m_current_open_edge.insert(i, rv);
        if (i>0) {
          const int r = m_current_open_edge[i-1];
          if (m_edges.contains(r,w)) {
            m_edges.remove(r,w);
            m_edges.insert(r,rv);
            m_edges.insert(rv,w);
//             qDebug() << __func__ << "insert" << rv << "between " << r << "->" << w;
          }
        }
        return rv;
      }
    }
  }
  // above the top vertex, just add & return
  const int rv = m_vertices.size();
  m_vertices << QPointF(m_current_left, y);
  m_current_open_edge << rv;
  return rv;

}


void SelectionAreaBuilder::begin_stack(qreal left, qreal right)
{
//   qDebug() << __func__ << left << right;
  Q_ASSERT(left<right);
  /*
   *  1 if |current_right-left|<tolerance:
   *   1.1. next_common_edge = current_common_edge
   *   1.2 current_left = current_right
   * 2 else
   *   2.1 current_left = left
   * 2 next_common_edge = []
   * 4. current_right = right;
   *
   */
  if (qAbs(m_current_right - left) < m_tolerance) {
    m_current_open_edge = m_next_common_edge;
    m_current_left = m_current_right;
  } else {
    m_current_left = left;
    m_current_open_edge.clear();
  }
  m_next_common_edge.clear();
  m_current_right = right;
  m_next_open_index = 0;
}

QPainterPath SelectionAreaBuilder::result()
{
  QPainterPath rv;
  rv.setFillRule(Qt::WindingFill);

  while (!m_edges.empty()) {
    edges_t::iterator start = m_edges.begin();
    const int start_vertex = start.key();
    rv.moveTo(m_vertices[start_vertex]);
    int vertex = start.value();
//     qDebug() << __func__ << start_vertex << vertex;
    m_edges.erase(start);
    for (;  vertex != start_vertex; vertex = m_edges.take(vertex)) {
      Q_ASSERT(vertex != 0);
      rv.lineTo(m_vertices[vertex]);
//       qDebug() << __func__ << vertex << start_vertex << m_edges;
    }
    rv.lineTo(m_vertices[start_vertex]);
  }
  return rv;
}

