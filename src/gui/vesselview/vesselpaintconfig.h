/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010

 Copyright: See COPYING file that comes with this distribution

*/

#ifndef VESSEL_PAINT_CONFIG_H
#define VESSEL_PAINT_CONFIG_H

#include <QtCore/QtGlobal>
#include "gui/mode.h"
#include "paintmode.h"
#include <QColor>

namespace ange {
namespace schedule {
class Call;
}
}

/**
 * Simple struct with flags affecting printing
 */
struct vessel_paint_config_t {
  vessel_paint_config_t(mode_enum_t::row_label_mode_t row_label_mode);
  PrintMode printMode;
  const ange::schedule::Call* call; // Call to be drawn
  const ange::schedule::Call* selected_call; // Call selected by user
  /**
   * level of detail
   * lod=pixel/meter, so at lod=10 a container is 26pixels wide
   **/
  qreal lod;
  mode_enum_t::row_label_mode_t row_label_mode;
  mode_enum_t::mode_t mode;
  paint_mode_t paint_mode;
  QColor selected_color;
  bool displayContainerWeight;
};

#endif // VESSEL_PAINT_CONFIG_H
