/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010

 Copyright: See COPYING file that comes with this distribution

*/

#include "bayheader.h"

#include <QPaintEvent>
#include <QGraphicsItem>

#include <limits>

#include "vesselgraphicsview.h"

/**
 * Small struct for bay headers
 */
struct bay_header_t::bay_item_t {
  bay_item_t(QGraphicsItem* fore_item, QGraphicsItem* aft_item, const QString& fore_label, const QString& aft_label, const QString& mid_label);
  QGraphicsItem* fore_item;
  QGraphicsItem* aft_item;
  QString fore_label;
  QString aft_label;
  QString mid_label;
};

bay_header_t::bay_item_t::bay_item_t(QGraphicsItem* fore_item, QGraphicsItem* aft_item, const QString& fore_label, const QString& aft_label, const QString& mid_label)
 : fore_item(fore_item),
    aft_item(aft_item),
    fore_label(fore_label),
    aft_label(aft_label),
    mid_label(mid_label)
{

}

bay_header_t::bay_header_t(vessel_graphics_view_t* parent, Qt::WindowFlags flags):
    QWidget(parent, flags),
    m_view(parent)
{
  QFont f = font();
  f.setBold(true);
  setFont(f);

}

void bay_header_t::clear_items() {
  m_bay_items.clear();
}

QSize bay_header_t::minimumSizeHint() const
{
  return QSize(0, fontMetrics().height());
}

void bay_header_t::paintEvent(QPaintEvent* event) {
  QWidget::paintEvent(event);
  QRect g = geometry();
  QPainter painter(this);
  painter.fillRect(event->rect(), palette().base());
  Q_FOREACH(bay_item_t* item, m_bay_items) {
    QRectF scene_bounding_rect;
    if(item->fore_item) {
        scene_bounding_rect = item->fore_item->mapRectToScene(item->fore_item->boundingRect());
    }
    if(item->aft_item) {
        if(scene_bounding_rect.isValid()) {
            scene_bounding_rect = scene_bounding_rect.united(item->aft_item->mapRectToScene(item->aft_item->boundingRect()));
        } else {
            scene_bounding_rect = item->aft_item->mapRectToScene(item->aft_item->boundingRect());
        }
    }
    Q_ASSERT(scene_bounding_rect.isValid());
    const int left = m_view->mapFromScene(scene_bounding_rect.topLeft()).x();
    const int right = m_view->mapFromScene(scene_bounding_rect.topRight()).x();
    int midpoint = (left+right)/2;
    painter.fillRect(left,0,midpoint-left,g.height(), palette().color(QPalette::AlternateBase));
    painter.fillRect(midpoint,0,right-midpoint,g.height(), palette().color(QPalette::Base));
    painter.setPen(palette().color(QPalette::Text));
    int textwidth = painter.fontMetrics().width(item->aft_label);
    const int intermargin = qMin(4, midpoint-left-textwidth);
    if (intermargin>0) {
      painter.drawText(left,0,midpoint-left-intermargin,g.height(), Qt::AlignRight | Qt::AlignVCenter, item->aft_label);
      painter.drawText(midpoint+intermargin,0,right-midpoint-intermargin,g.height(), Qt::AlignLeft| Qt::AlignVCenter, item->fore_label);
    } else {
      painter.drawText(left,0,right-left, g.height(), Qt::AlignCenter, item->mid_label);
    }
#if 0
    // Draw lines between numbers
    painter.setPen(palette().color(QPalette::Foreground));
    if (item->far_left == item->left) {
      painter.drawLine(left, 0, left, g.height());
    }
    painter.drawLine(right, 0, right, g.height());
#endif
  }
  painter.setPen(palette().color(QPalette::Foreground));
  painter.drawLine(event->rect().left(), event->rect().bottom(), event->rect().right(), event->rect().bottom());
}

void bay_header_t::add_bay_section(QGraphicsItem* fore_item, QGraphicsItem* aft_item, QString fore_label, QString aft_label, QString mid_label)
{
  m_bay_items << new bay_item_t(fore_item, aft_item, fore_label, aft_label, mid_label);
}

#include "bayheader.moc"
