/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010
 Copyright: See COPYING file that comes with this distribution
*/
#include "vesselpaintconfig.h"

vessel_paint_config_t::vessel_paint_config_t(mode_enum_t::row_label_mode_t row_label_mode) :
    printMode(),
    call(0L),
    lod(0.0),
    row_label_mode(row_label_mode),
    mode(mode_enum_t::MICRO_MODE),
    paint_mode(DISCHARGE_PAINT_MODE),
    selected_color(200, 200, 200, 128),
    displayContainerWeight(true)
{}
