/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010
 Copyright: See COPYING file that comes with this distribution
*/
#ifndef SCHEDULE_HEADER_H
#define SCHEDULE_HEADER_H

#include <QWidget>

class QGraphicsObject;
class QGraphicsView;
namespace ange {
namespace schedule {
class Call;
}
}
class document_t;

class schedule_header_t : public QWidget {
    Q_OBJECT
public:
    schedule_header_t(QGraphicsView* parent, Qt::WindowFlags f = 0);

    /**
     * Add call using the boundingrect and position of item to header
     */
    void add_call_item(QGraphicsObject* item, const ange::schedule::Call* call);

public Q_SLOTS:

    /**
     * Remove all call items on header
     */
    void clear_items();

    /**
     * Set new document
     */
    void set_document(document_t* document);

    /**
     * @override
     */
    virtual QSize minimumSizeHint() const;

    /**
     * Set current call
     */
    void set_current_call(const ange::schedule::Call* call);

    /**
     * Set current call
     */
    void focus_call(const ange::schedule::Call* call);

    /**
     * See remove_call_item(const ange::schedule::Call* call).
     * This overload is to make Qt's moc thingy happy. It just forwards the call
     */
    void remove_call_item(ange::schedule::Call* call);

    /**
     * Removes call item for header.
     * @param call to remove
     */
    void remove_call_item(const ange::schedule::Call* call);
    public Q_SLOTS:
        void doUpdate();

  Q_SIGNALS:
    void call_activated(const ange::schedule::Call* call);
  protected:
    virtual void paintEvent(QPaintEvent* event);
    virtual void mousePressEvent(QMouseEvent* event);
  private:
    class call_item_t;
    document_t* m_document;
    QGraphicsView* m_view;
    QList<call_item_t*> m_items;
    const ange::schedule::Call* m_current_call;
};

#endif // SCHEDULE_HEADER_H
