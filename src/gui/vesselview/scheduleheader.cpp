#include "scheduleheader.h"

#include "callutils.h"
#include "colortools.h"
#include "document.h"
#include "userconfiguration.h"
#include "vesselgraphicsitem.h"
#include "vesselgraphicsview.h"

#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>
#include <ange/vessel/vessel.h>

#include <QGraphicsObject>
#include <QPaintEvent>
#include <QPainter>
#include <QScrollBar>
#include <QSettings>

using ange::schedule::Call;
using ange::schedule::Schedule;

struct schedule_header_t::call_item_t {
  call_item_t(QGraphicsObject* item, const ange::schedule::Call* call);

  QGraphicsObject* item;
  const Call* call;
  QRectF scenerect;

  friend QDebug operator<<(QDebug dbg, const schedule_header_t::call_item_t& ci) {
    dbg.nospace() << "[" << ci.call << ":" << ci.item->mapRectToScene(ci.item->boundingRect()) << "]";
    return dbg.space();
  }
  friend QDebug operator<<(QDebug dbg, const schedule_header_t::call_item_t* ci) {
    if (ci) {
      return dbg << *ci;
    } else {
      return dbg << "[null schedule_header_t::call_item_t]";
    }
  }
};

schedule_header_t::call_item_t::call_item_t(QGraphicsObject* item, const ange::schedule::Call* call) :
    item(item),
    call(call)
{
}

schedule_header_t::schedule_header_t(QGraphicsView* parent, Qt::WindowFlags f):
    QWidget(parent, f),
    m_document(0L),
    m_view(parent),
    m_items(),
    m_current_call(0L)
{
}

void schedule_header_t::add_call_item(QGraphicsObject* item, const ange::schedule::Call* call) {
  connect(item, &QGraphicsObject::visibleChanged, this, static_cast<void (schedule_header_t::*)()>(&schedule_header_t::update));
  m_items << new call_item_t(item,call);
  update();
}

void schedule_header_t::clear_items() {
  m_items.clear();
}

void schedule_header_t::paintEvent(QPaintEvent* event) {
  QWidget::paintEvent(event);
  QPainter painter(this);
  const call_item_t* current_item = 0L;
  QRectF current_item_rect;
  if (m_document) {
    QRect g = geometry();
    painter.fillRect(event->rect(), palette().background());
    QFont normalfont = font();
    QFont boldfont = normalfont;
    boldfont.setBold(true);
    Q_FOREACH(const call_item_t* call_item, m_items) {
      if(!call_item->item->isVisible()) {
        continue;
      }
      QGraphicsItem* item = call_item->item;
      QRect item_rect = m_view->mapFromScene(item->mapRectToScene(item->boundingRect())).boundingRect();
      QRect call_rect(0, item_rect.top(), g.width()-2, item_rect.height());
      if (call_rect.intersects(event->rect())) {
        const Call* call = call_item->call;
        QColor call_color = m_document->userconfiguration()->get_color_by_call(call);
        QLinearGradient grad;
        grad.setColorAt(1.0,call_color.lighter(125));
        grad.setColorAt(0.5,call_color);
        grad.setColorAt(0.0,call_color.lighter(125));
        grad.setStart(call_rect.topLeft());
        grad.setFinalStop(call_rect.bottomLeft());
        painter.fillRect(call_rect, grad);
        const bool current = (m_current_call == call);
        if (current) {
          current_item = call_item;
          current_item_rect = item_rect;
          painter.setFont(boldfont);
        } else {
          painter.setFont(normalfont);
        }
        // Draw text in label
        QString label = call->uncode();
        QRect label_bounding_rect = painter.fontMetrics().boundingRect(label);
        QTransform transform = painter.transform();
        int top = (call_rect.top()+call_rect.bottom())/2-label_bounding_rect.width()/2;
        if (label_bounding_rect.width() < call_rect.bottom()) {
          top = qMax(0,top);
        }
        if (label_bounding_rect.width() < (height() - call_rect.top()-4)) {
          top = qMin(height() - label_bounding_rect.width()-4, top);
        }
        painter.setPen(backgroundRequiresColorInversion(call_color)? Qt::white : Qt::black);
        painter.translate((call_rect.left()+call_rect.right())/2-label_bounding_rect.height()/2+label_bounding_rect.bottom(),
                          top);
        painter.rotate(90);
        painter.drawText(0,0, label);
        painter.setTransform(transform);
      }
    }
  }
  QRect rect = event->rect();
  painter.setPen(palette().color(QPalette::Foreground));
  painter.drawLine(rect.right()-2, rect.top(), rect.right()-2, rect.bottom());
  if (current_item) {
    QRect call_rect(0, current_item_rect.top(), geometry().width(), current_item_rect.height());
    painter.setPen(QPen(palette().color(QPalette::Highlight),3, Qt::SolidLine, Qt::RoundCap));
    painter.drawLine(call_rect.right()-1, call_rect.top()+1, call_rect.right()-1, call_rect.bottom()-1);
  }
}

void schedule_header_t::set_document(document_t* document) {
  if (m_document) {
    m_document->schedule()->disconnect(this);
    m_document->disconnect(this);
  }
  m_document = document;
  connect(m_document->schedule(), &Schedule::callAboutToBeRemoved, this, static_cast<void (schedule_header_t::*)(Call*)>(&schedule_header_t::remove_call_item));
  connect(m_document->schedule(), &Schedule::scheduleChanged, this, &schedule_header_t::doUpdate);
  connect(m_document, &document_t::vessel_changed, this, &schedule_header_t::doUpdate);
}

QSize schedule_header_t::minimumSizeHint() const {
  return QSize(fontMetrics().height(), 0);
}

void schedule_header_t::set_current_call(const ange::schedule::Call* call) {
  m_current_call = call;
}

void schedule_header_t::focus_call(const ange::schedule::Call* call) {
  Q_FOREACH(const call_item_t* item, m_items) {
    if (item->call == call) {
      int current_value = m_view->verticalScrollBar()->value();
      int view_height = m_view->viewport()->height();
      QRect item_bounding_rect = m_view->mapFromScene(item->item->mapRectToScene(item->item->boundingRect()).toAlignedRect()).boundingRect();
      const int margin = std::min(std::max(m_view->viewport()->height()-item_bounding_rect.height(), 0), 50);
      if (item_bounding_rect.top() > 0.0 && item_bounding_rect.bottom() < view_height) {
        break; // item already inside view, no need to scroll
      }
      if (item_bounding_rect.top() < 0.0 && item_bounding_rect.bottom() > view_height) {
        break; // view is already inside item, no need to scroll
      }
      // Ok, we need to scroll. Calculate how much to scroll to keep resp. top or bottom just inside the view
      const int distance_to_top = item_bounding_rect.top() - margin;
      const int distance_to_bottom = item_bounding_rect.bottom() + margin - m_view->viewport()->height();
      // Scroll as little as possible
      const int scroll = (abs(distance_to_top) < abs(distance_to_bottom)) ? distance_to_top :distance_to_bottom;
      // Actually scroll
      m_view->verticalScrollBar()->setValue(current_value + scroll);
    }
  }
  update();
}

void schedule_header_t::remove_call_item(ange::schedule::Call* call) {
  const Call* const_call = call;
  remove_call_item(const_call);
}

void schedule_header_t::remove_call_item(const ange::schedule::Call* call) {
  // Not very efficient, but quickly coded...
  Q_FOREACH(call_item_t* item, m_items) {
    if (item->call == call) {
      m_items.removeOne(item);
      break;
    }
  }
  update();
}

void schedule_header_t::mousePressEvent(QMouseEvent* event) {
  QRect g = geometry();
  Q_FOREACH(const call_item_t* call_item, m_items) {
    QGraphicsItem* item = call_item->item;
    QRect item_rect = m_view->mapFromScene(item->mapRectToScene(item->boundingRect())).boundingRect();
    QRect call_rect(0, item_rect.top(), g.width(), item_rect.height());
    if (call_rect.contains(event->pos())) {
      event->accept();
      emit call_activated(call_item->call);
      break;
    }
  }
  QWidget::mousePressEvent(event);
}

void schedule_header_t::doUpdate() {
    update();
}

#include "scheduleheader.moc"
