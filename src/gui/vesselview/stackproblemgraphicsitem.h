#ifndef STACKPROBLEMGRAPHICSITEM_H
#define STACKPROBLEMGRAPHICSITEM_H

#include <QObject>

class problem_model_t;
class StowageStack;
class QPainter;
struct vessel_paint_config_t;
class StackGraphicsItem;
namespace ange {
namespace angelstow {
class Problem;
}
namespace schedule {

class Call;
}

namespace vessel {
class Stack;
}
}

class StackProblemGraphicsItem : public QObject {
    Q_OBJECT
    public:
        StackProblemGraphicsItem(problem_model_t* problems, const ange::vessel::Stack* vessel_stack, bool fore, StackGraphicsItem* parent);
        QString tooltip_text( const ange::schedule::Call* call) const;
        void paint(QPainter* painter, const QRectF& boundingRect, const vessel_paint_config_t& config);
    private:
        problem_model_t* m_problems;
        const ange::vessel::Stack* m_stack;
        bool m_fore;
        QList<ange::angelstow::Problem*> findProblems(const ange::schedule::Call* call) const;
};

#endif // STACKPROBLEMGRAPHICSITEM_H
