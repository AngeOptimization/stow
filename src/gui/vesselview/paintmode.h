#ifndef PAINT_MODE_H
#define PAINT_MODE_H

#include <QMetaType>

enum paint_mode_t {
  DISCHARGE_PAINT_MODE = 0,
  WEIGHT_PAINT_MODE = 1
};

Q_DECLARE_METATYPE(paint_mode_t)

#endif
