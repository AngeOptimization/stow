#ifndef COMPARTMENTMOUSETRACKER_H
#define COMPARTMENTMOUSETRACKER_H

#include <QObject>

namespace ange {
namespace schedule {

class Call;
}

namespace vessel {
class Compartment;
}
}
class document_t;
/**
 * \brief class to help track what compartments are under mouse.
 */

class CompartmentMouseTracker : public QObject {
    Q_OBJECT
    public:
        /**
         * Constructor
         * Needs to listen on vessel_changed on document
         * and to connect to schedule changes
         */
        CompartmentMouseTracker(document_t* parent);
        ~CompartmentMouseTracker();
        /**
         * Set tentative call/compartment
         */
        void setTentative(const ange::schedule::Call* call, const ange::vessel::Compartment* compartment);

        /**
         * @return tentatively selected compartment
         */
        const ange::vessel::Compartment* tentativeCompartment() const {
            return m_tentativeCompartment;
        }

        /**
         * @return tentatively selected (discharge) call
         */
        const ange::schedule::Call* tentativeCall() const {
            return m_tentativeCall;
        }
    public Q_SLOTS:
        /**
         * Clear tentative call/compartment
         */
        void clearTentative();
    Q_SIGNALS:
        void  compartmentChanged(const ange::vessel::Compartment* c);
    private Q_SLOTS:
        void removeCall(const ange::schedule::Call* call);
    private:
        const ange::vessel::Compartment* m_tentativeCompartment;
        const ange::schedule::Call* m_tentativeCall;
};

#endif // COMPARTMENTMOUSETRACKER_H
