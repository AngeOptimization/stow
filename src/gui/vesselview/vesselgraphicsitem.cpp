#include "vesselgraphicsitem.h"

#include "bayslicegraphicsitem.h"
#include "document.h"
#include "observerpositionitem.h"
#include "painter_utils.h"
#include "userconfiguration.h"
#include "vesselscene.h"

#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>
#include <ange/vessel/bayslice.h>
#include <ange/vessel/stack.h>
#include <ange/vessel/vessel.h>

#include <QApplication>
#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QPainter>
#include <QSettings>
#include <QStyleOption>

#include <cmath>
#include <numeric>
#include <stdexcept>

using ange::vessel::Vessel;
using ange::vessel::BaySlice;
using ange::vessel::Compartment;
using ange::containers::Container;
using ange::schedule::Call;
using namespace ange::units;

VesselGraphicsItem::VesselGraphicsItem(document_t* document, const pool_t* selectionPool,
                                       CompartmentMouseTracker* macroStowageMouseTracker, const Call* call,
                                       int nb_rows, QGraphicsItem* itemParent):
    QGraphicsObject(itemParent),
    m_selectionPool(selectionPool),
    m_macroStowageMouseTracker(macroStowageMouseTracker),
    m_bay_margin(1.0f, 0.0f),
    m_rows(nb_rows),
    m_call(call),
    m_draw_bulkheads(false),
    m_draw_visibilityline(false),
    m_observer_position_item(0L)
{
  setObjectName(QStringLiteral("VesselGraphicsItem(call=%1)").arg(m_call->assembledName()));
  setTransform(QTransform(1.0, 0.0, 0.0, -1.0, 0.0, 0.0)); // flip y-axis
  set_vessel_from_document(document);
  QSettings settings;
  m_draw_visibilityline = settings.value("view/display visibilityline",false).toBool();
}

void VesselGraphicsItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* , QWidget* ) {
// Painting these backgrounds causes repaint errors on some platforms,  Go figure
#if 0
  Q_FOREACH(bay_slice_item_t* item, m_aft_slices) {
    painter->fillRect(item->mapRectToParent(item->boundingRect()), Qt::black);
  }
#endif
  if (current()) {
    qreal width = std::fabs(2.0/painter->transform().m22());
    painter->setPen(QPen(Qt::black, width));
    painter->drawLine(QLineF(m_bounding_rect.left(), m_bounding_rect.bottom()-width, m_bounding_rect.right(), m_bounding_rect.bottom()-width)); //top line
    painter->drawLine(QLineF(m_bounding_rect.left(), m_bounding_rect.top()+width, m_bounding_rect.right(), m_bounding_rect.top()+width)); // bottom line (!)
  } else {
    QLinearGradient grad;
    grad.setColorAt(qreal(0.0),QColor("#F0F0F0"));
    grad.setColorAt(qreal(0.5),QColor("#D0D0D0"));
    grad.setColorAt(qreal(1.0),QColor("#F0F0F0"));
    grad.setStart(m_bounding_rect.topLeft());
    grad.setFinalStop(m_bounding_rect.topRight());
    painter->fillRect(m_bounding_rect,QBrush(grad));
  }

  // bounding rect debug
#if 0
  document_t* doc = qobject_cast<vessel_scene_t*>(scene())->document();
  painter->setPen(doc->userconfiguration()->get_color_by_call(m_call));
  painter->drawRect(boundingRect());
  painter->setBrush(QColor(240,240,255));
  Q_FOREACH(QGraphicsItem* item, m_aft_slices) {
    painter->drawRect(item->mapRectToParent(item->boundingRect()));
  }
  painter->setBrush(QColor(240,255,240));
  Q_FOREACH(QGraphicsItem* item, m_fore_slices) {
    painter->drawRect(item->mapRectToParent(item->boundingRect()));
  }
#endif
}

const ange::schedule::Call* VesselGraphicsItem::call() const {
  if (m_call) {
    return m_call;
  }
  if (vessel_scene_t* s = qobject_cast<vessel_scene_t*>(scene())) {
    return s->current_call();
  }
  return 0L;
}

QRectF VesselGraphicsItem::boundingRect() const {
  return m_bounding_rect;
}

void VesselGraphicsItem::layout_slices() {
  Q_ASSERT(m_aft_slices.size() == m_fore_slices.size());
  const int nslices = m_aft_slices.size();

  // Calculate row heights, and slices width
  // by computing the union of the bounding rectangle of all
  // untranslated the bayslices
  qreal max_height = 0.0;
  // max_diff is a correction factor needed to compensate for the
  // fact that the union bounding rectangle somehow gets too high
  qreal max_diff = 0.0;
  QRectF union_rect;
  for (int i = 0; i<nslices; ++i) {
      BaySliceGraphicsItem* aft = m_aft_slices.value(i);
      BaySliceGraphicsItem* fore = m_fore_slices.value(i);
      Q_ASSERT(fore||aft);
      if(aft && fore) {
        if(qAbs(aft->boundingRect().top() - fore->boundingRect().top()) > qAbs(max_diff)) {
            max_diff = aft->boundingRect().top() - fore->boundingRect().top();
        }
      }
    QRectF bound_aft_local =  aft ?  aft->boundingRect() : QRectF();
    QRectF bound_fore_local = fore ? fore->boundingRect() : QRectF();
    if (union_rect.isValid() && bound_aft_local.isValid()) {
      union_rect = union_rect.united(bound_aft_local);
    } else if(bound_aft_local.isValid()) {
      union_rect = bound_aft_local;
    }
    if (union_rect.isValid() && bound_fore_local.isValid()) {
        union_rect = union_rect.united(bound_fore_local);
    } else if(bound_fore_local.isValid()) {
        union_rect = bound_fore_local;
    }
  }
  vessel_scene_t* vessel_scene = qobject_cast<vessel_scene_t*>(scene());
  if(scene()) {
    Q_ASSERT(vessel_scene);
    Q_UNUSED(vessel_scene);
  }

  // Now lay them out side by side in 2*row_number rows with a small margin
  qreal start_x = union_rect.width()*nslices;
  max_height = union_rect.height();
  const qreal row_height = 2*(max_height+max_diff);
  qreal row_offset = (m_rows-1)*row_height; //canvas is oriented as an x-y coordinate system
  const int slices_per_row = bay_slices_per_row();
  int row_no = 0;
  start_x += (slices_per_row -1)* m_bay_margin.width();
  start_x /= 2.0;
  qreal x = start_x;
  bool observer_added = false;
  for (int i = 0; i<nslices; ++i) {
    if (i >= (row_no+1)*slices_per_row) {
      ++row_no;
      row_offset -= row_height;
      x = start_x;
    }
    BaySliceGraphicsItem* fore_slice = m_fore_slices.value(i);
    BaySliceGraphicsItem* aft_slice = m_aft_slices.value(i);
    if(fore_slice) {
        fore_slice->setPos(x-union_rect.width()/2.0, row_offset+max_height+2*max_diff);
    }
    if(aft_slice) {
        aft_slice->setPos(x-union_rect.width()/2.0, row_offset);
    }
    if (!observer_added) {
        Length slice_lcg;
        if(aft_slice) {
            slice_lcg = aft_slice->baySlice()->lcg();
        } else {
            Q_ASSERT(fore_slice);
            slice_lcg = fore_slice->baySlice()->lcg();
        }
      if (slice_lcg < m_observer_position.l()) {
        observer_added = true;
        const qreal ICON_SIZE = 8.0;
        if(aft_slice) {
            aft_slice->moveBy(-ICON_SIZE, 0);
        }
        if(fore_slice) {
            fore_slice->moveBy(-ICON_SIZE,0);
        }
        QRectF observer_rect(0.0, 0.0, ICON_SIZE, row_height);
        delete m_observer_position_item;
        m_observer_position_item = new observer_position_item_t(m_observer_position.v()/meter, observer_rect, this);
        if(aft_slice) {
            m_observer_position_item->setPos(aft_slice->mapToParent(aft_slice->boundingRect().topRight()).x()+m_bay_margin.width()/2,
                                         row_offset);
        } else {
            Q_ASSERT(fore_slice);
            m_observer_position_item->setPos(fore_slice->mapToParent(fore_slice->boundingRect().topRight()).x()+m_bay_margin.width()/2,
                                         row_offset);
        }
        x -= ICON_SIZE;
      }
    }
    x -= union_rect.width();
    x -= m_bay_margin.width();
  }
  prepareGeometryChange();
  m_bounding_rect = QRectF();
  Q_FOREACH(QGraphicsItem* item, m_aft_slices + m_fore_slices) {
      if(item) {
        m_bounding_rect = m_bounding_rect.united(item->mapRectToParent(item->boundingRect()));
      }
  }
  //Final padding around the whole vessel
  m_bounding_rect.adjust(-3.0, -5.0, 3.0, 5.0);

    // Bulkheads
    qDeleteAll(m_bulkheads);
    m_bulkheads.clear();
    if (m_draw_bulkheads) {
        const BaySliceGraphicsItem* prevFore = 0L;
        for (int i=0; i<m_aft_slices.size(); ++i) {
            const BaySliceGraphicsItem* fore = m_fore_slices[i];
            if (prevFore && prevFore->baySlice()->cargoHold() != fore->baySlice()->cargoHold()) {
                const QRectF prevForeBoundingRect = mapRectFromItem(prevFore, prevFore->boundingRect());
                const qreal right = prevForeBoundingRect.left();
                const qreal top = prevForeBoundingRect.top();
                const qreal height = 10.0;
                QGraphicsRectItem* bulkhead = new QGraphicsRectItem(right-0.4, top-height/2, 0.4, height, this);
                bulkhead->setBrush(Qt::gray);
                bulkhead->setPen(QPen(Qt::gray));
                m_bulkheads << bulkhead;
            }
            prevFore = fore;
        }
    }
}

void VesselGraphicsItem::set_rows(int rows) {
  if (rows < 1) {
    throw std::invalid_argument(QString("Rows must be >= 1, but was %1").arg(rows).toStdString());
  }
  m_rows = rows;
  layout_slices();
}

bool VesselGraphicsItem::current() const {
  if (vessel_scene_t* vessel_scene = qobject_cast<vessel_scene_t*>(scene())) {
    return (m_call && m_call == vessel_scene->current_call()) ;
  }
  return false;
}

namespace {
qreal turn_on_slice(bool on, const QList<BaySliceGraphicsItem*> bay_slices) {
  qreal rv = 0.0;
  Q_FOREACH(BaySliceGraphicsItem* slice_item, bay_slices) {
      if(slice_item) {
        const qreal old_height = slice_item->boundingRect().height();
        slice_item->turn_on_bay_numbers(on);
        const qreal new_height = slice_item->boundingRect().height();
        const qreal height_diff = new_height - old_height;
        if (std::fabs(height_diff)>std::fabs(rv)) {
        rv = height_diff;
        }
      }
  }
  return rv;
}
}

void VesselGraphicsItem::set_print_bay_numbers(bool on) {
  turn_on_slice(on, m_fore_slices);
  turn_on_slice(on, m_aft_slices);
  layout_slices();
}

void VesselGraphicsItem::set_vessel_from_document(document_t* document) {
  qDeleteAll(m_fore_slices);
  qDeleteAll(m_aft_slices);
  m_fore_slices.clear();
  m_aft_slices.clear();
  // Create bay slices, which comes in pairs: Fore, and aft
  Q_FOREACH(BaySlice* slice, document->vessel()->baySlices()) {
    QPair<BaySliceGraphicsItem*, BaySliceGraphicsItem*> slice_pair = BaySliceGraphicsItem::create_bay_slice_pair(
        slice, call(), document, m_selectionPool, m_macroStowageMouseTracker, this);
    m_fore_slices << slice_pair.first;
    m_aft_slices << slice_pair.second;
  }
  m_observer_position = ange::vessel::Point3D(document->vessel()->observerLcg(), document->vessel()->observerVcg(), 0.0*meter);
  layout_slices();
  emit bay_slice_items_changed(this);
}

void VesselGraphicsItem::draw_bulkheads(bool draw) {
  if (m_draw_bulkheads != draw) {
    m_draw_bulkheads = draw;
    if (draw) {
      layout_slices();
    } else {
      qDeleteAll(m_bulkheads);
      m_bulkheads.clear();
    }
  }
}

void VesselGraphicsItem::draw_visibilityline(bool draw) {
  if(m_draw_visibilityline != draw) {
    m_draw_visibilityline = draw;
    emit bay_slice_items_changed(this);
  }
}

int VesselGraphicsItem::bay_slices_per_row() const {
  return (m_fore_slices.size()+(m_rows-1)) / m_rows;
}

int VesselGraphicsItem::number_of_rows() const {
    return m_rows;
}

bool VesselGraphicsItem::get_draw_visibilityline_status() const {
    return m_draw_visibilityline;
}

const QList< BaySliceGraphicsItem* > VesselGraphicsItem::aft_slices() const {
    return m_aft_slices;
}

const QList< BaySliceGraphicsItem* > VesselGraphicsItem::fore_slices() const {
    return m_fore_slices;
}


#include "vesselgraphicsitem.moc"
