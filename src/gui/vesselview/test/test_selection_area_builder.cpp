#include "test_selection_area_builder.h"
#include "../selectionareabuilder.h"

void TestSelectionAreaBuilder_t::testBasic()
{
  /*  _____
   * |  _  |
   * | |_| |
   * |_____|
   */
  SelectionAreaBuilder builder(0.01);
  builder.begin_stack(0.0, 1.0);
  builder.add_interval(0.0, 3.0);
  builder.begin_stack(1.0,2.0);
  builder.add_interval(0.0,1.0);
  builder.add_interval(2.0, 3.0);
  builder.begin_stack(2.0, 3.0);
  builder.add_interval(0.0, 3.0);
  QPainterPath path = builder.result();

  QVERIFY(!path.contains(QPointF(-0.5, -0.5)));
  QVERIFY(!path.contains(QPointF(0.5, -0.5)));
  QVERIFY(!path.contains(QPointF(1.5, -0.5)));
  QVERIFY(!path.contains(QPointF(2.5, -0.5)));
  QVERIFY(!path.contains(QPointF(3.5, -0.5)));

  QVERIFY(!path.contains(QPointF(-0.5, 0.5)));
  QVERIFY(path.contains(QPointF(0.5, 0.5)));
  QVERIFY(path.contains(QPointF(1.5, 0.5)));
  QVERIFY(path.contains(QPointF(2.5, 0.5)));
  QVERIFY(!path.contains(QPointF(3.5, 0.5)));

  QVERIFY(!path.contains(QPointF(-0.5, 1.5)));
  QVERIFY(path.contains(QPointF(0.5, 1.5)));
  QVERIFY(!path.contains(QPointF(1.5, 1.5)));
  QVERIFY(path.contains(QPointF(2.5, 1.5)));
  QVERIFY(!path.contains(QPointF(3.5, 1.5)));

  QVERIFY(!path.contains(QPointF(-0.5, 2.5)));
  QVERIFY(path.contains(QPointF(0.5, 2.5)));
  QVERIFY(path.contains(QPointF(1.5, 2.5)));
  QVERIFY(path.contains(QPointF(2.5, 2.5)));
  QVERIFY(!path.contains(QPointF(3.5, 2.5)));

  QVERIFY(!path.contains(QPointF(-0.5, 3.5)));
  QVERIFY(!path.contains(QPointF(0.5, 3.5)));
  QVERIFY(!path.contains(QPointF(1.5, 3.5)));
  QVERIFY(!path.contains(QPointF(2.5, 3.5)));
  QVERIFY(!path.contains(QPointF(3.5, 3.5)));

}

void TestSelectionAreaBuilder_t::testConcentricSquares()
{
  /*
   * Test that "nested" areas gets included
   *  _________
   * |  _____  |
   * | |  _  | |
   * | | |_| | |
   * | |_____| |
   * |_________|
   *
   */
  SelectionAreaBuilder builder(0.01);
  builder.begin_stack(0.0, 1.0);
  builder.add_interval(0.0, 5.0);

  builder.begin_stack(1.0, 2.0);
  builder.add_interval(0.0, 1.0);
  builder.add_interval(4.0, 5.0);

  builder.begin_stack(2.0, 3.0);
  builder.add_interval(0.0, 1.0);
  builder.add_interval(2.0, 3.0);
  builder.add_interval(4.0, 5.0);

  builder.begin_stack(3.0, 4.0);
  builder.add_interval(0.0, 1.0);
  builder.add_interval(4.0, 5.0);

  builder.begin_stack(4.0, 5.0);
  builder.add_interval(0.0, 5.0);

  QPainterPath path = builder.result();

  qreal y=-0.5;
  QVERIFY(!path.contains(QPointF(-0.5, y)));
  QVERIFY(!path.contains(QPointF(0.5, y)));
  QVERIFY(!path.contains(QPointF(1.5, y)));
  QVERIFY(!path.contains(QPointF(2.5, y)));
  QVERIFY(!path.contains(QPointF(3.5, y)));
  QVERIFY(!path.contains(QPointF(4.5, y)));
  QVERIFY(!path.contains(QPointF(5.5, y)));

  y=0.5;
  QVERIFY(!path.contains(QPointF(-0.5, y)));
  QVERIFY(path.contains(QPointF(0.5, y)));
  QVERIFY(path.contains(QPointF(1.5, y)));
  QVERIFY(path.contains(QPointF(2.5, y)));
  QVERIFY(path.contains(QPointF(3.5, y)));
  QVERIFY(path.contains(QPointF(4.5, y)));
  QVERIFY(!path.contains(QPointF(5.5, y)));

  y=1.5;
  QVERIFY(!path.contains(QPointF(-0.5, y)));
  QVERIFY(path.contains(QPointF(0.5, y)));
  QVERIFY(!path.contains(QPointF(1.5, y)));
  QVERIFY(!path.contains(QPointF(2.5, y)));
  QVERIFY(!path.contains(QPointF(3.5, y)));
  QVERIFY(path.contains(QPointF(4.5, y)));
  QVERIFY(!path.contains(QPointF(5.5, y)));

  y=2.5;
  QVERIFY(!path.contains(QPointF(-0.5, y)));
  QVERIFY(path.contains(QPointF(0.5, y)));
  QVERIFY(!path.contains(QPointF(1.5, y)));
  QVERIFY(path.contains(QPointF(2.5, y)));
  QVERIFY(!path.contains(QPointF(3.5, y)));
  QVERIFY(path.contains(QPointF(4.5, y)));
  QVERIFY(!path.contains(QPointF(5.5, y)));

  y=3.5;
  QVERIFY(!path.contains(QPointF(-0.5, y)));
  QVERIFY(path.contains(QPointF(0.5, y)));
  QVERIFY(!path.contains(QPointF(1.5, y)));
  QVERIFY(!path.contains(QPointF(2.5, y)));
  QVERIFY(!path.contains(QPointF(3.5, y)));
  QVERIFY(path.contains(QPointF(4.5, y)));
  QVERIFY(!path.contains(QPointF(5.5, y)));

  y=4.5;
  QVERIFY(!path.contains(QPointF(-0.5, y)));
  QVERIFY(path.contains(QPointF(0.5, y)));
  QVERIFY(path.contains(QPointF(1.5, y)));
  QVERIFY(path.contains(QPointF(2.5, y)));
  QVERIFY(path.contains(QPointF(3.5, y)));
  QVERIFY(path.contains(QPointF(4.5, y)));
  QVERIFY(!path.contains(QPointF(5.5, y)));

  y=5.5;
  QVERIFY(!path.contains(QPointF(-0.5, y)));
  QVERIFY(!path.contains(QPointF(0.5, y)));
  QVERIFY(!path.contains(QPointF(1.5, y)));
  QVERIFY(!path.contains(QPointF(2.5, y)));
  QVERIFY(!path.contains(QPointF(3.5, y)));
  QVERIFY(!path.contains(QPointF(4.5, y)));
  QVERIFY(!path.contains(QPointF(5.5, y)));

}

void TestSelectionAreaBuilder_t::testDisjoint()
{
  /*
  * Test that 2 disjunct areas gets handled correctly, in both the vertical and horizontal case
  *  _   _
  * |_| |_|
  *  _
  * |_|
  */
  SelectionAreaBuilder builder(0.01);
  builder.begin_stack(0.0, 1.0);
  builder.add_interval(0.0, 1.0);
  builder.add_interval(2.0, 3.0);
  builder.begin_stack(2.0, 3.0);
  builder.add_interval(2.0, 3.0);

  QPainterPath path = builder.result();

  qreal y=-0.5;
  QVERIFY(!path.contains(QPointF(-0.5, y)));
  QVERIFY(!path.contains(QPointF(0.5, y)));
  QVERIFY(!path.contains(QPointF(1.5, y)));
  QVERIFY(!path.contains(QPointF(2.5, y)));
  QVERIFY(!path.contains(QPointF(3.5, y)));

  y=0.5;
  QVERIFY(!path.contains(QPointF(-0.5, y)));
  QVERIFY(path.contains(QPointF(0.5, y)));
  QVERIFY(!path.contains(QPointF(1.5, y)));
  QVERIFY(!path.contains(QPointF(2.5, y)));
  QVERIFY(!path.contains(QPointF(3.5, y)));

  y=1.5;
  QVERIFY(!path.contains(QPointF(-0.5, y)));
  QVERIFY(!path.contains(QPointF(0.5, y)));
  QVERIFY(!path.contains(QPointF(1.5, y)));
  QVERIFY(!path.contains(QPointF(2.5, y)));
  QVERIFY(!path.contains(QPointF(3.5, y)));

  y=2.5;
  QVERIFY(!path.contains(QPointF(-0.5, y)));
  QVERIFY(path.contains(QPointF(0.5, y)));
  QVERIFY(!path.contains(QPointF(1.5, y)));
  QVERIFY(path.contains(QPointF(2.5, y)));
  QVERIFY(!path.contains(QPointF(3.5, y)));

  y=3.5;
  QVERIFY(!path.contains(QPointF(-0.5, y)));
  QVERIFY(!path.contains(QPointF(0.5, y)));
  QVERIFY(!path.contains(QPointF(1.5, y)));
  QVERIFY(!path.contains(QPointF(2.5, y)));
  QVERIFY(!path.contains(QPointF(3.5, y)));

}

void TestSelectionAreaBuilder_t::testTouchingCorners()
{
  /*
   * Test touching corners (in all directions)
   *  _   _
   * |_|_|_|
   *  _|_|_
   * |_| |_|
   */

  SelectionAreaBuilder builder(0.01);
  builder.begin_stack(0.0, 1.0);
  builder.add_interval(0.0, 1.0);
  builder.add_interval(2.0, 3.0);
  builder.begin_stack(1.0, 2.0);
  builder.add_interval(1.0, 2.0);
  builder.begin_stack(2.0, 3.0);
  builder.add_interval(0.0, 1.0);
  builder.add_interval(2.0, 3.0);

  QPainterPath path = builder.result();

  qreal y=-0.5;
  QVERIFY(!path.contains(QPointF(-0.5, y)));
  QVERIFY(!path.contains(QPointF(0.5, y)));
  QVERIFY(!path.contains(QPointF(1.5, y)));
  QVERIFY(!path.contains(QPointF(2.5, y)));
  QVERIFY(!path.contains(QPointF(3.5, y)));

  y=0.5;
  QVERIFY(!path.contains(QPointF(-0.5, y)));
  QVERIFY(path.contains(QPointF(0.5, y)));
  QVERIFY(!path.contains(QPointF(1.5, y)));
  QVERIFY(path.contains(QPointF(2.5, y)));
  QVERIFY(!path.contains(QPointF(3.5, y)));

  y=1.5;
  QVERIFY(!path.contains(QPointF(-0.5, y)));
  QVERIFY(!path.contains(QPointF(0.5, y)));
  QVERIFY(path.contains(QPointF(1.5, y)));
  QVERIFY(!path.contains(QPointF(2.5, y)));
  QVERIFY(!path.contains(QPointF(3.5, y)));

  y=2.5;
  QVERIFY(!path.contains(QPointF(-0.5, y)));
  QVERIFY(path.contains(QPointF(0.5, y)));
  QVERIFY(!path.contains(QPointF(1.5, y)));
  QVERIFY(path.contains(QPointF(2.5, y)));
  QVERIFY(!path.contains(QPointF(3.5, y)));

  y=3.5;
  QVERIFY(!path.contains(QPointF(-0.5, y)));
  QVERIFY(!path.contains(QPointF(0.5, y)));
  QVERIFY(!path.contains(QPointF(1.5, y)));
  QVERIFY(!path.contains(QPointF(2.5, y)));
  QVERIFY(!path.contains(QPointF(3.5, y)));

}

void TestSelectionAreaBuilder_t::testSingleQuandric()
{
  /*
   * Test a single rectangle, with extra (dummy) begin_stacks
   *  _
   * |_|
   */

  SelectionAreaBuilder builder(0.01);
  builder.begin_stack(-1.0, 0.0);
  builder.begin_stack(0.0, 1.0);
  builder.add_interval(0.0, 1.0);
  builder.begin_stack(1.0, 2.0);
  builder.begin_stack(2.0,3.0);

  QPainterPath path = builder.result();

  qreal y=-0.5;
  QVERIFY(!path.contains(QPointF(-0.5, y)));
  QVERIFY(!path.contains(QPointF(0.5, y)));
  QVERIFY(!path.contains(QPointF(1.5, y)));

  y=0.5;
  QVERIFY(!path.contains(QPointF(-0.5, y)));
  QVERIFY(path.contains(QPointF(0.5, y)));
  QVERIFY(!path.contains(QPointF(1.5, y)));

  y=1.5;
  QVERIFY(!path.contains(QPointF(-0.5, y)));
  QVERIFY(!path.contains(QPointF(0.5, y)));
  QVERIFY(!path.contains(QPointF(1.5, y)));

}

void TestSelectionAreaBuilder_t::test2VerticalStacks()
{
  /*
   * Test two begin_stack where stacks are overlapping
   *  _
   * |_|
   *  _
   * |_|
   */

  SelectionAreaBuilder builder(0.01);
  builder.begin_stack(0.0, 1.0);
  builder.add_interval(0.0, 1.0);
  builder.begin_stack(0.0, 1.0);
  builder.add_interval(2.0, 3.0);

  QPainterPath path = builder.result();

  qreal y=-0.5;
  QVERIFY(!path.contains(QPointF(-0.5, y)));
  QVERIFY(!path.contains(QPointF(0.5, y)));
  QVERIFY(!path.contains(QPointF(1.5, y)));

  y=0.5;
  QVERIFY(!path.contains(QPointF(-0.5, y)));
  QVERIFY(path.contains(QPointF(0.5, y)));
  QVERIFY(!path.contains(QPointF(1.5, y)));

  y=1.5;
  QVERIFY(!path.contains(QPointF(-0.5, y)));
  QVERIFY(!path.contains(QPointF(0.5, y)));
  QVERIFY(!path.contains(QPointF(1.5, y)));

  y=2.5;
  QVERIFY(!path.contains(QPointF(-0.5, y)));
  QVERIFY(path.contains(QPointF(0.5, y)));
  QVERIFY(!path.contains(QPointF(1.5, y)));

  y=3.5;
  QVERIFY(!path.contains(QPointF(-0.5, y)));
  QVERIFY(!path.contains(QPointF(0.5, y)));
  QVERIFY(!path.contains(QPointF(1.5, y)));
}

void TestSelectionAreaBuilder_t::testTolerance() {
    SelectionAreaBuilder builder(1.0);
    builder.begin_stack(-1.0, 0.0);
    builder.add_interval(24.0, 27.0);
    builder.begin_stack(0.1, 1.0);
    builder.add_interval(24.0, 26);
}

QTEST_GUILESS_MAIN(TestSelectionAreaBuilder_t)

#include "test_selection_area_builder.moc"


