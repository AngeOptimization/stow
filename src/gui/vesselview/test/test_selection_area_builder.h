#ifndef TEST_SELECTION_AREA_BUILDER_H_
#define TEST_SELECTION_AREA_BUILDER_H_
#include <QTest>

class TestSelectionAreaBuilder_t : public QObject {
  Q_OBJECT
private Q_SLOTS:
  void testBasic();
  void testTouchingCorners();
  void testDisjoint();
  void testConcentricSquares();
  void testSingleQuandric();
  void test2VerticalStacks();
  void testTolerance();

};

#endif
