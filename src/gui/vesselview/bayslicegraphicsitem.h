#ifndef BAY_SLICE_GRAPHICS_ITEM_H
#define BAY_SLICE_GRAPHICS_ITEM_H

#include <QGraphicsObject>

#include "gui/stow.h"
#include "gui/mode.h"
#include <ange/vessel/decklevel.h>
#include <QFont>

class pool_t;
class CompartmentMouseTracker;
class SelectionAreaBuilder;
class SlotGraphicsItem;
struct vessel_paint_config_t;
class document_t;
class StackGraphicsItem;
namespace ange {
namespace schedule {
class Call;
}
namespace vessel {
class BaySlice;
class Compartment;
class Stack;
}
}

/**
 * A class representing a half (aft or fwd/40) bay
 */
class BaySliceGraphicsItem : public QGraphicsObject {
    Q_OBJECT
    Q_INTERFACES(QGraphicsItem)

public:
    /**
     * Create a fore/aft bay slice pair
     */
    static QPair<BaySliceGraphicsItem*, BaySliceGraphicsItem*> create_bay_slice_pair(
        ange::vessel::BaySlice* bay_slice, const ange::schedule::Call* call, document_t* document,
        const pool_t* selectionPool, CompartmentMouseTracker* macroStowageMouseTracker, QGraphicsItem* itemParent);

    virtual void paint(QPainter* painter, const QStyleOptionGraphicsItem*, QWidget* widget = 0);
    virtual QRectF boundingRect() const;

    /**
     * @returns the boundingrect around the stacks only, with no allowance for baynumbers and such
     */
    QRectF stacks_bounding_rect() const {
      return m_stacks_bounding_rect;
    }
    /**
     * Return the "sister" slice item, that is, the other slice in the fore/aft split.
     */
    BaySliceGraphicsItem* sister() {
      return m_sister;
    }

    enum {
      Type = UserType + Stow::bay_slice_item_t
    };
    virtual int type() const {
      return Type;
    }

    /**
     * Return the bayslice this item models (note that bay_slices are both front and back)
     */
    ange::vessel::BaySlice* baySlice() const {
      return m_baySlice;
    }

    /**
     * @return slot_items touched by rect. odd_slots "sisters" are not included.
     */
    QList<SlotGraphicsItem*> slot_items(const QRectF& rect, const ange::schedule::Call* call) const;


    /**
     * Supply tooltips for slots and stack labels
     */
    QString tooltip_text(const QPointF& pos) const;

    /**
     * @return stack_items in stack
     */
    QList<StackGraphicsItem*> stack_items() const {
      return QList<StackGraphicsItem*>() << m_stack_items_above << m_stack_items_below;
    }

    /**
     * @return the artificial sinkage of below decks, to accommodate overheight stacks (and perhaps drawing of lids)
     */
    static qreal below_sinkage() {
      return 2.0;
    }

    /**
     * @return the call this bayslice represents
     */
    const ange::schedule::Call* call() const;

    /**
     * @return true if this is visually a "fore" slice
     */
    bool isfore() const {
      return m_fore;
    }

    virtual void timerEvent(QTimerEvent* event);

    /**
     * Start marching ants, if not already started
     */
    void start_marching_ants();

    /**
     * Returns a const pointer to the mouse tracker for the macro stowage
     */
    const CompartmentMouseTracker* macroStowageMouseTracker() {
        return m_macroStowageMouseTracker;
    }

    /**
     * Tell the bayslice graphics item to show the bay numbers
     */
    void turn_on_bay_numbers(bool on);

    static qreal levelOfDetailBasedBayNumberTextRectSize(PrintMode mode, const qreal levelOfDetail);

protected:
    virtual void hoverEnterEvent( QGraphicsSceneHoverEvent* event );
    virtual void hoverMoveEvent( QGraphicsSceneHoverEvent* event );
    virtual void hoverLeaveEvent( QGraphicsSceneHoverEvent* event );

private:
    BaySliceGraphicsItem(ange::vessel::BaySlice* bay_slice, const ange::schedule::Call* call, BaySliceGraphicsItem* sister,
                         bool slice_fore, document_t* document, const pool_t* selectionPool,
                         CompartmentMouseTracker* macroStowageMouseTracker ,QGraphicsItem* itemParent);

    void draw_stack_helper(QPainter* painter, StackGraphicsItem* last_stack_item, StackGraphicsItem* stack_item, StackGraphicsItem* next_stack_item, const vessel_paint_config_t& config, SelectionAreaBuilder& seletion_area_builder);
    StackGraphicsItem* item_for_stack(ange::vessel::DeckLevel level, const ange::vessel::Stack* stack) const;
    /**
     * \return a QPair of problemArea, labelArea for a given stackitem
     */
    QPair< QRectF, QRectF > stackLabelAreas(StackGraphicsItem* stackitem);
    ange::vessel::BaySlice* m_baySlice;
    const ange::schedule::Call* m_call;
    bool m_fore;
    qreal m_default_font_height;
    QRectF m_bounding_rect;
    BaySliceGraphicsItem* m_sister;
    QPointF m_stacks_below_bottom_left;
    QList<StackGraphicsItem*> m_stack_items_below;
    QList<StackGraphicsItem*> m_stack_items_above;
    QList<QRectF> m_lids;
    document_t* m_document;
    const pool_t* m_selectionPool;
    CompartmentMouseTracker* m_macroStowageMouseTracker;
    QFont m_bay_no_font;
    bool m_print_bay_numbers;
    QRectF m_stacks_bounding_rect;// Boundingrect for stacks only
    QString tooltip_text_for_compartment(ange::vessel::Compartment* compartment) const;
    qreal m_dash_offset;
    int m_timer_id;
    QRectF m_above_tier_label_rect;
    QRectF m_below_tier_label_rect;
    QList<int> m_tiers_above;
    QList<int> m_tiers_below;
    QHash<StackGraphicsItem*, QRectF> m_stackLabelAreas;
    QHash<StackGraphicsItem*, QRectF> m_stackProblemAreas;
public Q_SLOTS:
    void update_slot();
private Q_SLOTS:
    void slot_compartment_changed( const ange::vessel::Compartment* compartment);
};

#endif // BAY_SLICE_GRAPHICS_ITEM_H
