#ifndef OBSERVER_POSITION_ITEM_H
#define OBSERVER_POSITION_ITEM_H

#include <QGraphicsObject>

class VesselGraphicsItem;

class observer_position_item_t : public QGraphicsObject {

  public:
    observer_position_item_t(double observer_vcg, QRectF bounding_rect, VesselGraphicsItem* parent);

    virtual void paint(QPainter* painter, const QStyleOptionGraphicsItem*, QWidget*);

    virtual QRectF boundingRect() const;

  private:
    QRectF m_bounding_rect;
    double m_observer_vcg;
};

#endif // OBSERVER_POSITION_ITEM_H
