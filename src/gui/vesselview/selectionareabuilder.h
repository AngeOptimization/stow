#ifndef SELECTION_AREA_BUILDER_H
#define SELECTION_AREA_BUILDER_H
#include <QPainterPath>
#include <QHash>

/**
 * Class to build up the selection area within a bay.
 *
 * For each stack, call begin_stack(), then add the selected intervals (height-ranges)
 */
class SelectionAreaBuilder
{
  public:
    /**
     * Construct empty
     * @param tolerance tolerance for merging/discarding.
     */
    explicit SelectionAreaBuilder(qreal tolerance);

    /**
     * @return true if nothing was ever added to this
     */
    bool empty() const;

    /**
     * @return the resulting path
     */
    QPainterPath result();

    /**
     * begin a new stack
     * @param left the left bound of the stack
     * @param right the right bound of the stack
     */
    void begin_stack(qreal left, qreal right);

    /**
     * Add (selected) interval to stack
     */
    void add_interval(qreal bottom, qreal top);
  private:
    qreal m_tolerance;
    qreal m_current_left;
    qreal m_current_right;

    QVector<QPointF> m_vertices;
    QVector<int> m_current_open_edge;
    QVector<int> m_next_common_edge;
    typedef QMultiHash<int, int> edges_t;
    edges_t m_edges;
    int m_next_open_index;
    int add_vertex_to_common_edge(qreal y);
};

#endif // SELECTION_AREA_BUILDER_H
