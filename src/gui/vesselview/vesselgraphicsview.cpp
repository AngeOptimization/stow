#include "vesselgraphicsview.h"

#include "bayheader.h"
#include "bayslicegraphicsitem.h"
#include "container_leg.h"
#include "container_list.h"
#include "container_stow_command.h"
#include "containerselectionbundlehelper.h"
#include "document.h"
#include "fixate_command.h"
#include "gui_interface.h"
#include "merger_command.h"
#include "paintmode.h"
#include "passivenotifications.h"
#include "pool.h"
#include "recapfiltercontainerid.h"
#include "recapmanagerinterface.h"
#include "recapview.h"
#include "scheduleheader.h"
#include "slotgraphicsitem.h"
#include "stowage.h"
#include "tool.h"
#include "toolmanager.h"
#include "vesselgraphicsitem.h"
#include "vesselscene.h"

#include <stowplugininterface/igui.h>
#include <stowplugininterface/problem.h>

#include <ange/containers/container.h>
#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>
#include <ange/vessel/bayslice.h>
#include <ange/vessel/slot.h>

#include <qdatacube/datacube.h>

#include <QApplication>
#include <QGraphicsItem>
#include <QMenu>
#include <QMouseEvent>
#include <QScrollBar>
#include <QSettings>
#include <QSvgRenderer>
#include <QTimer>
#include <QVariant>

using namespace ange::angelstow;
using namespace ange::containers;
using namespace ange::schedule;
using namespace ange::vessel;

static const double MAX_SCALE = 30.0;

vessel_graphics_view_t::vessel_graphics_view_t(QWidget* parent): QGraphicsView(parent),
  m_dragmode(false),
  m_current_tool(0L ),
  m_rubber_mode(false),
  m_selection_pool(0L),
  m_document(0L),
  m_scroll_timer(new QTimer(this)),
  m_no_zoom_and_pan(false),
  m_current_item(0L),
  m_toolManager(0L),
  m_recapManager(0L)
{
  qreal zoom_factor = matrix().m11();
  setMatrix(QMatrix(zoom_factor,0.0,0.0,zoom_factor,0.0,0.0));
  setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
  m_scroll_timer->setInterval(100);
  connect(m_scroll_timer, SIGNAL(timeout()), SLOT(slot_scroll()));
  m_schedule_header = new schedule_header_t(this);
  m_schedule_header->setSizePolicy(QSizePolicy());
  connect(verticalScrollBar(), SIGNAL(valueChanged(int)), m_schedule_header, SLOT(update()));
  m_bay_header = new bay_header_t(this);
  m_bay_header->setSizePolicy(QSizePolicy());
  connect(horizontalScrollBar(), SIGNAL(valueChanged(int)), m_bay_header, SLOT(update()));
  update_headers();
  setAlignment(Qt::AlignLeft | Qt::AlignTop);
  QSettings settings;
  m_zoom_out_action = new QAction(tr("out"),this);
  m_zoom_out_action->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_Minus));
  m_zoom_out_action->setShortcutContext(Qt::WidgetShortcut);
  connect(m_zoom_out_action,SIGNAL(triggered(bool)),this,SLOT(zoom_out()));
  m_zoom_out_large_action = new QAction(tr("out x10"),this);
  m_zoom_out_large_action->setShortcut(QKeySequence(Qt::CTRL + Qt::ALT + Qt::Key_Minus));
  m_zoom_out_large_action->setShortcutContext(Qt::WidgetShortcut);
  connect(m_zoom_out_large_action,SIGNAL(triggered(bool)),this,SLOT(zoom_out_large()));
  m_zoom_in_action = new QAction(tr("in"),this);
  m_zoom_in_action->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_Plus));
  m_zoom_in_action->setShortcutContext(Qt::WidgetShortcut);
  connect(m_zoom_in_action,SIGNAL(triggered(bool)),this,SLOT(zoom_in()));
  m_zoom_in_large_action = new QAction(tr("in x10"),this);
  m_zoom_in_large_action->setShortcut(QKeySequence(Qt::CTRL + Qt::ALT + Qt::Key_Plus));
  m_zoom_in_large_action->setShortcutContext(Qt::WidgetShortcut);
  connect(m_zoom_in_large_action,SIGNAL(triggered(bool)),this,SLOT(zoom_in_large()));
}

void vessel_graphics_view_t::update_headers() {
  const int width = m_schedule_header->minimumSizeHint().width();
  const int height = m_bay_header->isVisible() ?  m_bay_header->minimumSizeHint().height() : 0;
  setViewportMargins(width,height,0,0);
  QRect geometry = viewport()->geometry(); // Important to get geometry after we set the viewport, see ticket #333
  int left = geometry.left();
  m_schedule_header->setGeometry(left-width,geometry.top(),width,geometry.height());
  if (m_bay_header->isVisible()) {
    m_bay_header->setGeometry(left, geometry.top()-height, geometry.width(), height);
  }
}

void vessel_graphics_view_t::resizeEvent(QResizeEvent* event) {
  update_headers();
  QGraphicsView::resizeEvent(event);
}

void vessel_graphics_view_t::showEvent(QShowEvent* event) {
  update_headers();
  QGraphicsView::showEvent(event);
}

void vessel_graphics_view_t::show_2_rows(bool activated) {
  // Don't show bay header in 2-row mode
  m_bay_header->setVisible(!activated);
  update_headers();
  if (vessel_scene_t* vessel_scene = qobject_cast< vessel_scene_t* >(scene())) {
    vessel_scene->show_2_rows(activated);
  }
}

template<typename T>
void clamp(T& value, T lower, T upper) {
  value = std::min(std::max(value,lower),upper);
}

void vessel_graphics_view_t::wheelEvent(QWheelEvent* event) {
  if(m_no_zoom_and_pan) {
    return;
  }
  const bool ctrl_held = QApplication::keyboardModifiers() & Qt::ControlModifier;
  if (ctrl_held) {
    event->accept();
    qreal wheel = qreal(event->delta())/300.0;
    zoom(wheel);
  } else {
    QGraphicsView::wheelEvent(event);
    if(m_rubber_mode) {
      viewport()->update();
    }
  }
}

void vessel_graphics_view_t::zoom(double amount) {
  qreal zoom_factor = matrix().m11();
  zoom_factor += amount;
  clamp(zoom_factor, 1.0, MAX_SCALE);
  QMatrix mm = QMatrix(zoom_factor,0.0,0.0,zoom_factor,0.0,0.0);
  setMatrix(mm);
  m_schedule_header->update();
  m_bay_header->update();
}

void vessel_graphics_view_t::mousePressEvent(QMouseEvent* event) {
  switch(event->button()) {
    case Qt::MidButton: {
      m_dragmode = true;
      m_rubber_mode = false;
      m_cursor = event->pos();
      setDragMode(QGraphicsView::NoDrag);
      setCursor(Qt::ClosedHandCursor);
      event->accept();
      break;
    }
    case Qt::LeftButton: {
      Q_FOREACH(QGraphicsItem* item, items(event->pos())) {
        if (BaySliceGraphicsItem* bay_slice_item = qgraphicsitem_cast<BaySliceGraphicsItem*>(item)) {
          set_current_item(bay_slice_item, false);
          break;
        }
      }
      tool_t* current_tool = m_toolManager->currentTool();
      Q_ASSERT(current_tool);
      if (current_tool->area_tool()) {
        m_rubber_mode = true;
        m_dragmode = false;
        m_rubber_drag = QRect(event->pos(), QSize(0,0));
        event->accept();
      }
      break;
    }
    default:
      break;
  }
  QGraphicsView::mousePressEvent(event);
}

void vessel_graphics_view_t::mouseReleaseEvent(QMouseEvent* event) {
  switch (event->button()) {
    case Qt::MidButton: {
      if (m_dragmode) {
        m_dragmode=false;
        if (tool_t* current_tool = m_toolManager->currentTool()) {
          setCursor(current_tool->cursor(event->modifiers()));
        } else {
          setCursor(QCursor());
        }
        event->accept();
      }
      break;
    }
    case Qt::LeftButton: {
      if (m_rubber_mode) {
        m_scroll_speed = QPoint();
        QPointF tl = mapToScene(m_rubber_drag.topLeft());
        QPointF br = mapToScene(m_rubber_drag.bottomRight());
        emit tool_applied(QRectF(tl, br), m_toolManager->currentTool());
        m_rubber_mode = false;
        viewport()->update(m_rubber_drag.normalized().adjusted(-1,-1,1,1));
        event->accept();
      }
      break;
    }
    default:
      break;
  }
  QGraphicsView::mouseReleaseEvent(event);
}

void vessel_graphics_view_t::mouseMoveEvent(QMouseEvent* event) {
  if(m_dragmode){
    QPointF cursor = event->pos();
    int hv = horizontalScrollBar()->value();
    horizontalScrollBar()->setValue(hv-cursor.x()+m_cursor.x());
    int vv = verticalScrollBar()->value();
    verticalScrollBar()->setValue(vv-cursor.y()+m_cursor.y());
    m_cursor=event->pos();
  }
  if (m_rubber_mode) {
    if (!m_rubber_drag.size().isEmpty() || (m_rubber_drag.topLeft()-event->pos()).manhattanLength() > 10) {
      QRect old_rubber = m_rubber_drag.normalized();
      m_rubber_drag.setBottomRight(event->pos());
      if(!m_no_zoom_and_pan) {
        // Set the scroll speed (which would be zero if we are within the contentsRect)
        int dx = 0;
        int dy = 0;
        QRect viewRect = contentsRect();
        int x = event->pos().x();
        int y = event->pos().y();
        if (viewRect.left() > x) {
          dx = viewRect.left()-x;
        }  else if (viewRect.right() < x) {
          dx = viewRect.right()-x;
        }
        if (viewRect.top() > y) {
          dy = viewRect.top()-y;
        } else if (viewRect.bottom() < y) {
          dy = viewRect.bottom()-y;
        }
        m_scroll_speed = QPoint(dx,dy);
        if (dx != 0 || dy != 0) {
          if (!m_scroll_timer->isActive()) {
            m_scroll_timer->start();
          }
        } else {
          if (m_scroll_timer->isActive()) {
            m_scroll_timer->stop();
          }
        }
      }
      viewport()->update(m_rubber_drag.united(old_rubber).adjusted(-1,-1,1,1));
    }
  }
  QGraphicsView::mouseMoveEvent(event);
}

void vessel_graphics_view_t::set_tool(tool_t* tool) {
  if (m_current_tool && !tool) {
    setCursor(QCursor());
  }
  m_current_tool = tool;
  if (m_current_tool) {
    setCursor(m_current_tool->cursor(QApplication::keyboardModifiers()));
  }
}

void vessel_graphics_view_t::paintEvent(QPaintEvent* event) {
    Q_ASSERT(m_toolManager);
  QGraphicsView::paintEvent(event);
  QPainter painter(viewport());
  if (m_rubber_mode) {
    if (m_rubber_drag.intersects(event->rect())) {
      painter.setPen(Qt::black);
      painter.setBrush(QColor(200, 200, 200, 128));
      painter.drawRect(m_rubber_drag);
    }
  }
  //paints the bay selection rectancle
  if (m_current_item) {
    painter.setBrush(QBrush());
    QRect bounding_rect = mapFromScene(m_current_item->mapRectToScene(m_current_item->boundingRect())).boundingRect();
    painter.setPen(Qt::DashLine);
    painter.drawRect(bounding_rect);
  }
}

void vessel_graphics_view_t::contextMenuEvent(QContextMenuEvent* event) {
  QGraphicsView::contextMenuEvent(event);
  vessel_scene_t* vessel_scene = qobject_cast<vessel_scene_t*>(scene());
  if (!vessel_scene) {
    return;
  }
  QMenu context_menu;
  QAction* zoom_to_bay = 0L;
  QAction* zoom_to_bay_and_create_new = 0L;
  QAction* zoom_to_vessel_action = 0L;
  QAction* unstow_selected = 0L;
  QAction* fixate_selected = 0L;
  QAction* unfixate_selected = 0L;
  QMenu* painter_mode_menu = context_menu.addMenu(tr("Coloring by"));
  QAction* mode_by_discharge_port = painter_mode_menu->addAction(tr("by discharge port"));
  mode_by_discharge_port->setData(QVariant::fromValue(DISCHARGE_PAINT_MODE));
  QAction* mode_by_weight = painter_mode_menu->addAction(tr("by weight"));
  mode_by_weight->setData(QVariant::fromValue(WEIGHT_PAINT_MODE));
  QMenu* zoom_menu = context_menu.addMenu(tr("Zoom"));
  Q_FOREACH(QGraphicsItem* item, items(event->pos())) {
    if (qgraphicsitem_cast<BaySliceGraphicsItem*>(item)) {
      zoom_to_bay=zoom_menu->addAction(tr("to bay"));
      zoom_to_bay->setShortcut(Qt::Key_B);
      zoom_to_bay_and_create_new=zoom_menu->addAction(tr("to bay in new vessel view"));
      //under what conditions can we have more than one bay item in the same point ?
      break;
    }
  }
  Q_FOREACH(QGraphicsItem* item, items(event->pos())) {
    if (qgraphicsitem_cast<VesselGraphicsItem*>(item)) {
      zoom_to_vessel_action=zoom_menu->addAction(tr("to vessel"));
      zoom_to_vessel_action->setShortcut(Qt::Key_V);
      //under what conditions can we have more than one bay item in the same point ?
      break;
    }
  }

  zoom_menu->addAction(m_zoom_in_action);
  zoom_menu->addAction(m_zoom_in_large_action);
  zoom_menu->addAction(m_zoom_out_action);
  zoom_menu->addAction(m_zoom_out_large_action);

  if (m_selection_pool && !m_selection_pool->included_rows().isEmpty()) {
    unstow_selected = context_menu.addAction(QIcon(":/icons/icons/unstow.svg"), tr("Unstow selected containers"));
    unstow_selected->setShortcut(QKeySequence(Qt::Key_Backspace));
    fixate_selected = context_menu.addAction(QIcon(), tr("Fixate selected containers"));
    unfixate_selected = context_menu.addAction(QIcon(), tr("Unfixate selected containers"));
  }
  const Call* current_call = vessel_scene->current_call();
  QRectF rect(mapToScene(event->pos()), QSizeF(0.0, 0.0));
  Q_FOREACH(QGraphicsItem* item, items(event->pos())) {
    if (BaySliceGraphicsItem* bay_slice_item = qgraphicsitem_cast<BaySliceGraphicsItem*>(item)) {
      Q_FOREACH(SlotGraphicsItem* slot_item, bay_slice_item->slot_items(bay_slice_item->mapRectFromScene(rect).normalized(), current_call)) {
        if (Slot* vessel_slot = slot_item->slot()) {
          if (const Container* container = m_document->stowage()->contents(vessel_slot,current_call).container()) {
            QAction* unstow_one = context_menu.addAction(QIcon(":/icons/icons/unstow.svg"), tr("Unstow container at %1").arg(slot_item->slot()->brt().toString()));
            unstow_one->setData(QVariant::fromValue<const Container*>(container));
          }
        }
      }
    }
  }

  QAction* recap_of_selection = 0L;
  if(!m_selection_pool->included_rows().isEmpty()) {
    recap_of_selection = context_menu.addAction("Display recap of selection");
  }
  QAction* bundleSelectedContainers = 0;
  if(!m_bundlehelper.isNull()) {
        bundleSelectedContainers = m_bundlehelper.data()->bundleAction();
  }
  if(bundleSelectedContainers!=0) {
    context_menu.addAction(bundleSelectedContainers);
  }
  if (!context_menu.isEmpty()) {
    if (QAction* action = context_menu.exec(event->globalPos())) {
      if (action == recap_of_selection) {
                // FIXME should not depend on global variable
                if(m_recapManager) {
                    RecapView* recap = m_recapManager->add_recap();
                    QSet<int> container_ids;
                    Q_FOREACH(const ange::containers::Container* container, m_selection_pool->containers()) {
                        container_ids << container->id();
                    }
                    recap->setupStandardRecap();
                    recap->datacube()->resetFilter(); // setupStandardRecap adds a unplanned filter. We don't want that since our selection here most likely contains planned containers
                    recap->addCustomFilter(qdatacube::AbstractFilter::Ptr(new RecapFilterContainerId(container_ids,m_document->containers())));
                    recap->setName("Selection");
                } else {
                    Q_ASSERT(false); // shouldn't happen.
                }
      } else if (action == unstow_selected) {
        unstow_selected_containers();
      } else if (action == fixate_selected) {
        fixate_command_t* command = fixate_command_t::fixate_list(m_document, m_selection_pool->containers());
        m_document->undostack()->push(command);
      } else if (action == unfixate_selected) {
        fixate_command_t* command = fixate_command_t::unfixate_list(m_document, m_selection_pool->containers());
        m_document->undostack()->push(command);
      } else if(action == zoom_to_bay) {
        bool bayslice_found=false;
        Q_FOREACH(QGraphicsItem* item, items(event->pos())) {
          if(BaySliceGraphicsItem* bayslice = qgraphicsitem_cast<BaySliceGraphicsItem*>(item)) {
            fitInView(bayslice,Qt::KeepAspectRatio);
            bayslice_found=true;
          }
        }
        Q_ASSERT(bayslice_found);
        Q_UNUSED(bayslice_found);
      } else if(action == zoom_to_bay_and_create_new) {
        bool bayslice_found=false;
        Q_FOREACH(QGraphicsItem* item, items(event->pos())) {
          if(BaySliceGraphicsItem* bayslice = qgraphicsitem_cast<BaySliceGraphicsItem*>(item)) {
            emit create_zoomed(bayslice->mapRectToScene(bayslice->boundingRect()));
            bayslice_found=true;
          }
        }
        Q_ASSERT(bayslice_found);
        Q_UNUSED(bayslice_found);
      } else if(action == zoom_to_vessel_action) {
        bool vessel_found=false;
        Q_FOREACH(QGraphicsItem* item, items(event->pos())) {
          if(VesselGraphicsItem* vessel = qgraphicsitem_cast<VesselGraphicsItem*>(item)) {
            fitInView(vessel,Qt::KeepAspectRatio);
            zoom_to_vessel(vessel);
            vessel_found=true;
          }
        }
        Q_ASSERT(vessel_found);
        Q_UNUSED(vessel_found);
      } else if(action == bundleSelectedContainers) {
        // do nothing, is handled by triggered() in the action
      } else if (painter_mode_menu->actions().contains(action)) {
        viewport()->setProperty("paint-mode", action->data());
        viewport()->update();
      } else { // unstow specific containers, as per action->data
        if (const Container* container = action->data().value<const Container*>()) {
            container_stow_command_t* command = new container_stow_command_t(m_document, m_selection_pool, true);
            command->add_discharge(container, vessel_scene->current_call(), MicroStowedType);
            m_document->undostack()->push(command);
        }
      }
    }
  }
}

void vessel_graphics_view_t::zoom_to_vessel(VesselGraphicsItem* vessel)
{
  if (!vessel) {
    return;
  }
  QRectF vessel_rect = vessel->mapRectToScene(vessel->boundingRect());
  QRect viewport_rect = viewport()->rect();
  qreal zoom =  (viewport_rect.width() - 4) / vessel_rect.width();
  zoom = qMin(zoom, (viewport_rect.height() - 4) / vessel_rect.height() );
  clamp(zoom, 1.0, MAX_SCALE);
  setMatrix(QMatrix(zoom, 0, 0, zoom, 0, 0));
  ensureVisible(vessel, 2, 2);
}

void vessel_graphics_view_t::zoom_to_bay(BaySliceGraphicsItem* bay)
{
  if (!bay) {
    return;
  }
  QRectF bay_rect = bay->mapRectToScene(bay->boundingRect());
  QRect viewport_rect = viewport()->rect();
  qreal zoom =  (viewport_rect.width() - 4) / bay_rect.width();
  zoom = qMin(zoom, (viewport_rect.height() - 4) / bay_rect.height() );
  clamp(zoom, 1.0, MAX_SCALE);
  setMatrix(QMatrix(zoom, 0, 0, zoom, 0, 0));
  centerOn(bay->mapRectToScene(bay->boundingRect()).center());
}

void vessel_graphics_view_t::set_selection_pool(pool_t* selection_pool) {
  m_selection_pool = selection_pool;
}

void vessel_graphics_view_t::set_document(document_t* document) {
  m_document = document;
  m_current_item = 0L;
  m_schedule_header->set_document(document);
  connect(m_document, SIGNAL(vessel_changed(ange::vessel::Vessel*)), SLOT(clear_current_item()));
}

void vessel_graphics_view_t::setScene(vessel_scene_t* scene) {
  m_schedule_header->clear_items();
  m_bay_header->clear_items();
  m_current_item = 0L;
  bool bay_headers_unset = true;
  Q_FOREACH(const Call* call, m_document->schedule()->calls()) {
    if (VesselGraphicsItem* vessel_item = scene->vessel_item(call)) {
      include_vessel_item(vessel_item);
      if (bay_headers_unset) {
        bay_headers_unset = false;
        update_column_headers(vessel_item);
        connect(vessel_item, SIGNAL(bay_slice_items_changed(VesselGraphicsItem*)), SLOT(update_column_headers(VesselGraphicsItem*)));
      }
    }
  }
  connect(scene, SIGNAL(call_changed(const ange::schedule::Call*)), m_schedule_header, SLOT(set_current_call(const ange::schedule::Call*)));
  connect(m_schedule_header, SIGNAL(call_activated(const ange::schedule::Call*)), SIGNAL(call_activated(const ange::schedule::Call*)));
  connect(scene, SIGNAL(vessel_item_added(VesselGraphicsItem*)), SLOT(include_vessel_item(VesselGraphicsItem*)));
  connect(scene, SIGNAL(vessel_item_removed(VesselGraphicsItem*)), SLOT(exclude_vessel_item(VesselGraphicsItem*)));
  QGraphicsView::setScene(scene);
}

void vessel_graphics_view_t::update_column_headers(VesselGraphicsItem* vessel_item) {
    m_bay_header->clear_items();
    Q_ASSERT(vessel_item->fore_slices().size() == vessel_item->aft_slices().size());
    for(int i = 0 ; i< vessel_item->fore_slices().size() ; i++) {
        BaySliceGraphicsItem* foreSlice = vessel_item->fore_slices().at(i);
        BaySliceGraphicsItem* aftSlice = vessel_item->aft_slices().at(i);
        Q_ASSERT(foreSlice || aftSlice);
        ange::vessel::BaySlice* underlyingSlice = foreSlice ? foreSlice->baySlice() : aftSlice->baySlice();
        m_bay_header->add_bay_section(foreSlice,
                                      aftSlice,
                                      QString::number(underlyingSlice->bays().front()),
                                      QString::number(underlyingSlice->bays().back()),
                                      QString::number(underlyingSlice->joinedBay())
                                    );
    }
    m_bay_header->update();
}

void vessel_graphics_view_t::include_vessel_item(VesselGraphicsItem* vessel_item) {
  m_schedule_header->add_call_item(vessel_item, vessel_item->call());
}

void vessel_graphics_view_t::exclude_vessel_item(VesselGraphicsItem* vessel_item) {
  if (m_current_item && qgraphicsitem_cast<VesselGraphicsItem*>(m_current_item->parentItem()) == vessel_item) {
    set_current_item(0L, false);
  }
  m_schedule_header->remove_call_item(vessel_item->call());
}

void vessel_graphics_view_t::slot_scroll() {
  const int dx = m_scroll_speed.x();
  const int dy = m_scroll_speed.y();
  m_rubber_drag.translate(dx,dy);
  if (dx != 0) {
    if (QScrollBar* bar = horizontalScrollBar()) {
      bar->setValue(bar->value()-dx);
    }
  }
  if (dy != 0) {
    if (QScrollBar* bar = verticalScrollBar()) {
      bar->setValue(bar->value()-dy);
    }
  }
}

void vessel_graphics_view_t::keyPressEvent(QKeyEvent* event) {
    Q_ASSERT(m_toolManager);
  if (!m_no_zoom_and_pan) {
    //Figure out how to do this with m_zoom*action's directly.
    if (event->modifiers() & Qt::CTRL) {
      const qreal zoomamount = (event->modifiers() & Qt::ALT) ? 5 : 0.5;
      if (event->key() == '+') {
        zoom(zoomamount);
      } else if (event->key() == '-') {
        zoom(-zoomamount);
      }
    }
    if (tool_t* current_tool = m_toolManager->currentTool()) {
        setCursor(current_tool->cursor(event->modifiers()));
    }
    if (event->key() == Qt::Key_Right) {
      move_current_right();
      return;
    } else if (event->key() == Qt::Key_Left) {
      move_current_left();
      return;
    } else if (event->key() == Qt::Key_Up) {
      move_current_up();
      return;
    } else if (event->key() == Qt::Key_Down) {
      move_current_down();
      return;
    }
  }
  if (event->key() == Qt::Key_Backspace) {
    unstow_selected_containers();
    return;
  }
  if (event->key() == Qt::Key_V) {
    vessel_scene_t* vessel_scene = qobject_cast< vessel_scene_t* >(scene());
    zoom_to_vessel(vessel_scene->vessel_item());
  } else if (event->key() == Qt::Key_B) {
    zoom_to_bay(m_current_item);
  }
  QGraphicsView::keyPressEvent(event);
}

void vessel_graphics_view_t::keyReleaseEvent(QKeyEvent* event)
{
    if (tool_t* current_tool = m_toolManager->currentTool()) {
        setCursor(current_tool->cursor(event->modifiers()));
    }
    QGraphicsView::keyReleaseEvent(event);
}

void vessel_graphics_view_t::unstow_selected_containers() {
    vessel_scene_t* vesselScene = qobject_cast<vessel_scene_t*>(scene());
    Q_ASSERT(vesselScene);
    if (!vesselScene) {
        return;
    }
    int unstowedMidTrip = 0;
    container_stow_command_t* command = new container_stow_command_t(m_document, m_selection_pool, true);
    Q_FOREACH (const Container * container, m_selection_pool->containers()) {
        if (!m_document->stowage()->container_position(container, vesselScene->current_call())) {
            continue; // Skip unstow of containers that are not onboard in current call
        }
        if (m_document->stowage()->container_routes()->portOfLoad(container) != vesselScene->current_call()) {
            unstowedMidTrip += 1;
        }
        command->add_discharge(container, vesselScene->current_call(), MicroStowedType);
    }
    m_document->undostack()->push(command);
    if (unstowedMidTrip) {
        m_document->passiveNotifications()->addWarningWithUndo(
            unstowedMidTrip == 1 ?
            QString("Unstowed 1 container in the middle of its trip.") :
            QString("Unstowed %1 containers in the middle of their trip.").arg(unstowedMidTrip));
    }
}

void vessel_graphics_view_t::move_current_right()
{
  if (m_current_item) {
    if (VesselGraphicsItem* vessel_item = qgraphicsitem_cast<VesselGraphicsItem*>(m_current_item->parentItem())) {
      const bool isfore = m_current_item->isfore();
      QList<BaySliceGraphicsItem*> slices = isfore ? vessel_item->fore_slices() : vessel_item->aft_slices();
      const int index = slices.indexOf(m_current_item);
      if (index>0) {
        set_current_item(slices.at(index-1), true);
      }
    }
  }
}

void vessel_graphics_view_t::move_current_left()
{
  if (m_current_item) {
    if (VesselGraphicsItem* vessel_item = qgraphicsitem_cast<VesselGraphicsItem*>(m_current_item->parentItem())) {
      const bool isfore = m_current_item->isfore();
      QList<BaySliceGraphicsItem*> slices = isfore ? vessel_item->fore_slices() : vessel_item->aft_slices();
      const int index = slices.indexOf(m_current_item);
      if (index<slices.size()-1) {
        set_current_item(slices.at(index+1), true);
      }
    }
  }
}

void vessel_graphics_view_t::move_current_down()
{
  if (m_current_item) {
    if (VesselGraphicsItem* vessel_item = qgraphicsitem_cast<VesselGraphicsItem*>(m_current_item->parentItem())) {
      const bool isfore = m_current_item->isfore();
      QList<BaySliceGraphicsItem*> slices = isfore ? vessel_item->fore_slices() : vessel_item->aft_slices();
      const int index = slices.indexOf(m_current_item);
      if (isfore) {
        set_current_item(vessel_item->aft_slices().at(index), true);
      } else {
        const int nslices = vessel_item->bay_slices_per_row();
        if (index<(nslices*(vessel_item->number_of_rows()-1))) {
          set_current_item(vessel_item->fore_slices().at(qMin(index+nslices, slices.size()-1)), true);
        }
      }
    }
  }
}

void vessel_graphics_view_t::move_current_up()
{
  if (m_current_item) {
    if (VesselGraphicsItem* vessel_item = qgraphicsitem_cast<VesselGraphicsItem*>(m_current_item->parentItem())) {
      const bool isfore = m_current_item->isfore();
      QList<BaySliceGraphicsItem*> slices = isfore ? vessel_item->fore_slices() : vessel_item->aft_slices();
      const int index = slices.indexOf(m_current_item);
      if (!isfore) {
        set_current_item(vessel_item->fore_slices().at(index), true);
      } else {
        const int nslices = vessel_item->bay_slices_per_row();
        if (index>=nslices) {
          set_current_item(vessel_item->aft_slices().at(index-nslices), true);
        }
      }
    }
  }
}

void vessel_graphics_view_t::set_current_item(BaySliceGraphicsItem* new_current, bool focus) {
    vessel_scene_t* scene = qobject_cast<vessel_scene_t*>(this->scene());
    if(new_current) {
        if(new_current->call() != scene->current_call()) {
            return;
        }
    }
    QList<QRectF> rectsToUpdate;
    if (m_current_item) {
        QRectF old_rect = m_current_item->mapRectToScene(m_current_item->stacks_bounding_rect());
        old_rect.adjust(-3, -3, 3,3);
        rectsToUpdate<<old_rect;
    }
    m_current_item = new_current;
    if (focus && m_current_item) {
        centerOn(m_current_item->mapRectToScene(m_current_item->boundingRect()).center());
    }
    if(m_current_item) {
        QRectF new_rect = m_current_item->mapRectToScene(m_current_item->stacks_bounding_rect());
        new_rect.adjust(-3, -3, 3,3);
        rectsToUpdate << new_rect;
    }
    updateScene(rectsToUpdate);
}

void vessel_graphics_view_t::focus_call(const ange::schedule::Call* call)
{
  m_schedule_header->focus_call(call);
}


void vessel_graphics_view_t::set_no_zoom_and_pan(bool b) {
  m_no_zoom_and_pan=b;
}

void vessel_graphics_view_t::zoom_in() {
  zoom(qreal(0.5));
}

void vessel_graphics_view_t::zoom_in_large() {
  zoom(qreal(5.0));
}

void vessel_graphics_view_t::zoom_out() {
  zoom(-qreal(0.5));
}

void vessel_graphics_view_t::zoom_out_large() {
  zoom(-qreal(5.0));
}

void vessel_graphics_view_t::clear_current_item()
{
  set_current_item(0, false);
}

void vessel_graphics_view_t::setBundleHelper(ContainerSelectionBundleHelper* bundlehelper) {
    m_bundlehelper = bundlehelper;
}

void vessel_graphics_view_t::zoomToProblem(const Problem* problem) {
    vessel_scene_t* vessel_scene = qobject_cast<vessel_scene_t*>(scene());
    if (!vessel_scene) {
        return;
    }
    QRectF rect = vessel_scene->selectProblemAndReturnRects(problem);
    fitInView(rect, Qt::KeepAspectRatio);
    setFocus();
}

void vessel_graphics_view_t::setToolManager(ToolManager* toolManager) {
    m_toolManager = toolManager;
}

void vessel_graphics_view_t::setRecapManager(RecapManagerInterface* manager) {
    m_recapManager = manager;
}




#include "vesselgraphicsview.moc"
