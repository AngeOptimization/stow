/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010

 Copyright: See COPYING file that comes with this distribution

*/

#ifndef BAY_HEADER_H
#define BAY_HEADER_H

#include <QWidget>

class QGraphicsItem;
class vessel_graphics_view_t;

class bay_header_t : public QWidget {
    Q_OBJECT
  public:
    bay_header_t(vessel_graphics_view_t* parent = 0, Qt::WindowFlags flags = 0);
  public Q_SLOTS:
    /**
     * Add a section to the bay header.
     * @param left limit, in scene coordinates
     * @param right  limit, in scene coordinates
     * @param fore_label label of fore bay
     * @param aft_label label of aft bay
     */
    void add_bay_section(QGraphicsItem* fore_item, QGraphicsItem* aft_item, QString fore_label, QString aft_label, QString mid_label);


     /**
     * Remove all call items on header
     */
    void clear_items();

    /**
     * @override
     */
    virtual QSize minimumSizeHint() const;

  protected:
    virtual void paintEvent(QPaintEvent* event);
  private:
    class bay_item_t;
    vessel_graphics_view_t* m_view;
    QList<bay_item_t*> m_bay_items;
};

#endif // BAY_HEADER_H
