#ifndef VESSELSCENE_H
#define VESSELSCENE_H

#include "container_orderer.h"
#include "mode.h"

#include <QGraphicsScene>
#include <QSet>

class merger_command_t;
class ToolManager;
class CompartmentMouseTracker;
class BaySliceGraphicsItem;
class pool_t;
class tool_t;
class VesselGraphicsItem;
class VesselGraphicsItem;
class stowage_t;
class QAbstractItemView;
class SlotGraphicsItem;
class document_t;
namespace ange {
namespace containers {
class Container;
}
namespace schedule {
class Call;
class Schedule;
}
namespace vessel {
class Slot;
class Vessel;
}
namespace angelstow {
    class Problem;
}
}

/**
 * A QGraphicsScene displaying the vessel in each call
 */
class vessel_scene_t : public QGraphicsScene {

    Q_OBJECT

public:
    /**
     * @param container_view The list/table/recap where containers are currently pasted from
     */
    vessel_scene_t(document_t* document, pool_t* selection_pool);

    virtual ~vessel_scene_t();

    /**
     * @return the currently displayed call
     */
    const ange::schedule::Call* current_call() const {
        return m_current_call;
    }

    /**
     * @return the currently selected mode
     */
    mode_enum_t::mode_t current_mode() const {
        return m_current_mode;
    }

    /**
     * @return slot_item that represents slot
     */
    SlotGraphicsItem* slot_item(ange::vessel::Slot* stack_slot, ange::schedule::Call* call) const;

    /**
     * @return current painted call (for macro mode)
     */
    const ange::schedule::Call* current_painted_call() const {
        return m_current_painted_call;
    }

    /**
     * @return margin to enable user to center vessel at will
     */
    static qreal margin() {
        return 10.0;
    }

    /**
     * @return document for vessel_scene
     */
    document_t* document() const;

    /**
     * @return true if equipment number should be visible
     */
    bool  show_equipment_number() const {
        return m_show_equipment_number;
    }

    /**
     * True if print mode (draw all numbers, no matter how small)
     */
    PrintMode printMode() const {
        return m_printMode;
    }

    /**
     * Set print mode (draw all numbers, no matter how small)
     */
    void setPrintMode(PrintMode printMode) {
        m_printMode = printMode;
        update();
    }

    /**
     * Bucket-fill into selected slots
     * @param from_top from top if true, from bottom if false
     * @param from_left from left if trye, from right if false
     */
    void bucket_fill_rect(QRectF rect);

    /**
     * Select rectangle
     */
    void select_rect(QRectF rect);

    /**
     * Macro-select/paint rectangle
     */
    void macro_select_rect(QRectF rect);

    mode_enum_t::row_label_mode_t row_label_mode() const {
        return m_row_label_mode;
    }

    /**
     * Switch to showing vessels in 2-row display
     * Note that headers and such also needs updating, so
     * you probably want vessel_graphics_view_t::show_2_rows()
     */
    void show_2_rows(bool activated = false);

    /**
     * @return current vessel_item
     */
    VesselGraphicsItem* vessel_item() const {
        return m_vessel_items.value(m_current_call);
    }

    /**
     * @return vessel_item for
     * @param call
     */
    VesselGraphicsItem* vessel_item(const ange::schedule::Call* call) const {
        return m_vessel_items.value(call);
    }

    /**
     * return whether or not the container weights should be shown when drawing the
     * containers
     */
    bool displayContainerWeights() const {
        return m_displayContainerWeights;
    }

public Q_SLOTS:
    /**
     * Draw bay numbers at each bay
     */
    void set_print_bay_numbers(bool on);

    /**
     * Set the currently displayed call
     */
    void set_active_call(const ange::schedule::Call* call);

    /**
     * Update with result of container movement
     */
    void slot_container_moved(const ange::containers::Container* container,
                              const ange::vessel::Slot* previous,
                              const ange::vessel::Slot* current,
                              const ange::schedule::Call* call);

    /**
     * Tool is to be applied on some scene rect
     */
    void Slotool_applied(QRectF rect, tool_t* tool);

    /**
     * Switch to paint with this rot_no / call
     */
    void slot_painted_call_changed(ange::schedule::Call* call);

    void slot_row_labels_changed(mode_enum_t::row_label_mode_t row_mode);
    void set_show_equipment_id(bool state);

    /**
     * Refresh the legality (greyed-out slots) of the scene (from the current selection)
     */
    void refresh_legality();

    void slot_selection_changed();

    void slot_update_selected_slots();

    /**
     * A container has changed in some way
     */
    void slot_container_changed(const ange::containers::Container* container);

    /**
     * Remove (delete) call from view
     */
    void remove_call(ange::schedule::Call* call);

    /**
     * Add call to view
     */
    void add_call(ange::schedule::Call* call);

    /**
     * moves the call at @param position to @param newPosition
     */
    void applyChangeOfRotation(int previousPosition, int currentPosition);

    /**
     * Reconnect to stowage. This will also update everything
     */
    void connect_to_stowage();

    /**
     * set vessel to vessel, updating everything
     */
    void set_vessel();

    /**
     * Display bulkheads for all calls
     **/
    void display_bulkheads_for_all_calls(bool on);

    /**
     * Display visibilityline for all calls
     **/
    void display_visibilityline_for_all_calls(bool on);

    void hide_non_active_calls_changed(bool hidden);

    /**
     * to be called when the setting for display container weights in the scan plan is changed
     */
    void changeDisplayContainerWeights(bool display);

    /**
     * Sets wether fore and aft slots should be linked for selection and stowing
     */
    void setLinkForeAft(bool linkForeAft);

    /**
     * Selects the problem area and return the bay rects relevant.
     * The rect is in scene coordinates.
     */
    QRectF selectProblemAndReturnRects(const ange::angelstow::Problem* problem);

    void setStowageMode(mode_enum_t::mode_t mode);
    void setToolManager(ToolManager* m_toolManager);

Q_SIGNALS:

    /**
     * Signal current (displayed) call has changed
     */
    void call_changed(const ange::schedule::Call* call);

    /**
     * Current mode has changed
     */
    void mode_changed(mode_enum_t::mode_t new_mode);
    void row_labels_mode_changed(mode_enum_t::row_label_mode_t new_row_mode);

    /**
     * Containers were selected in scene
     */
    void containers_selected(QList< const ange::containers::Container* > containers);

    /**
     * Signal that a vessel_item has been added to the scene
     */
    void vessel_item_added(VesselGraphicsItem* vessel_item);

    /**
     * Signal that a vessel_item has been removed to the scene
     */
    void vessel_item_removed(VesselGraphicsItem* vessel_item);

protected:
    virtual void helpEvent(QGraphicsSceneHelpEvent* event);
    virtual void keyReleaseEvent(QKeyEvent* event);

private:
    void remove_non_empty_slots(QList< ange::vessel::Slot* >* list) const;

    struct slot_call_t;
    friend bool operator==(const vessel_scene_t::slot_call_t* slot_call, const ange::schedule::Call& call);
    friend uint qHash(const slot_call_t& slot_call);

    void create_reverse_slot_index(VesselGraphicsItem* vessel_item);

    /**
     * Helper function for bucket_fill_rect, which clears away macro stowed containers in slot
     */
    void clear_away_macrostowed_in_slot(const ange::containers::Container* to_be_placed, const ange::vessel::Slot* slot, merger_command_t* command);

    /**
     * Helper function, that moves connections for legality (the grey-out functionality) from old to new vessel_item
     * This is neccessary when the call has changed, and when the vessel has changed.
     */
    void move_legality_connections(VesselGraphicsItem* old_current, VesselGraphicsItem* new_current);

    /**
     * Helper function that returns the slots selected when dragging a rectangle
     * \param rect dragged rect
     * \param respectLinkForeAftSetting wether or not to respect the settings for linkForeAft. If false, fore and aft aren't linked no matter the settings.
     */
    QList< const ange::vessel::Slot* > selected_slots_from_rect(const QRectF rect, bool respectLinkForeAftSetting = true);

private:
    /**
     * remove the non-empty slots in a list, i.e. only keep the slots that are actually
     * relevant for container placement
     */
    CompartmentMouseTracker* m_macroStowageMouseTracker;
    const ange::schedule::Call* m_current_call;

    typedef QHash<slot_call_t, SlotGraphicsItem*> slots_t;
    slots_t m_slots;

    mode_enum_t::mode_t m_current_mode;
    const ange::schedule::Call* m_current_painted_call;
    bool m_show_equipment_number;
    QHash<const ange::schedule::Call*, VesselGraphicsItem*> m_vessel_items;
    PrintMode m_printMode;
    bool m_print_bay_numbers;
    pool_t* m_selection_pool;
    QSet<SlotGraphicsItem*> m_last_selected;
    mode_enum_t::row_label_mode_t m_row_label_mode;
    bool m_hide_non_active_calls;
    bool m_displayContainerWeights;
    const BaySliceGraphicsItem* m_current_item;
    bool m_legal;
    // wether or not selecting the fore slot also should select the aft slot
    bool m_linkForeAft;
    /**
     * number of rows to display the vessel in. Used when creating a new vessel view.
     */
    bool m_2_rows;
    ToolManager* m_toolManager;

};

#endif // VESSELSCENE_H
