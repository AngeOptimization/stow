#include "observerpositionitem.h"

#include "painter_utils.h"
#include "vesselgraphicsitem.h"

#include <ange/schedule/call.h>

#include <QPainter>

observer_position_item_t::observer_position_item_t(double observer_vcg, QRectF bounding_rect, VesselGraphicsItem* parent)
  : QGraphicsObject(parent),
    m_bounding_rect(bounding_rect),
    m_observer_vcg(observer_vcg)
{
    setObjectName(QStringLiteral("ObserverPositionItem(call=%1)").arg(parent->call()->assembledName()));
}

void observer_position_item_t::paint(QPainter* painter, const QStyleOptionGraphicsItem* , QWidget* )
{
  const qreal bar_width = m_bounding_rect.width()*0.2;
  const qreal center = 0.5*(m_bounding_rect.left() + m_bounding_rect.right());

  // Draw emblem (an eye, for observer)
  QRectF emblem_rect = QRectF(m_bounding_rect.left(), m_observer_vcg + bar_width, m_bounding_rect.width(), m_bounding_rect.width());
  ange::painter_utils::draw_svg_in_rect(":/slots/slot_overlays/eye.svg", painter, emblem_rect, false);
  QPen pen(QColor(128,128,255),0.0);
  painter->setPen(pen);
  painter->setBrush(QColor(200,200,255));
  // Draw pole
  QRectF bar_rect(center- bar_width/2, m_bounding_rect.top(), bar_width, m_observer_vcg - m_bounding_rect.top());
  painter->drawRect(bar_rect);
  // Draw marker line
  painter->drawLine(QLineF(center - bar_width, m_observer_vcg, center + bar_width, m_observer_vcg));
}

QRectF observer_position_item_t::boundingRect() const {
  return m_bounding_rect;
}
