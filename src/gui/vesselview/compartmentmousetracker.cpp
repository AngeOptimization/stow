/*
 *
 */

#include "compartmentmousetracker.h"
#include <document/document.h>
#include <ange/schedule/schedule.h>
#include <ange/schedule/call.h>
#include <ange/vessel/vessel.h>

CompartmentMouseTracker::~CompartmentMouseTracker() {
}

CompartmentMouseTracker::CompartmentMouseTracker(document_t* document)
        : QObject(document)
        , m_tentativeCompartment(0)
        , m_tentativeCall(0)
{
    setObjectName("CompartmentMouseTracker");
    connect(document, &document_t::vessel_changed, this, &CompartmentMouseTracker::clearTentative);
    connect(document->schedule(), &ange::schedule::Schedule::callAboutToBeRemoved, this, &CompartmentMouseTracker::removeCall);
}

void CompartmentMouseTracker::clearTentative() {
    const ange::vessel::Compartment* c = m_tentativeCompartment;
    m_tentativeCall = 0L;
    m_tentativeCompartment = 0L;
    if (c) {
        emit compartmentChanged(c);
    }
}

void CompartmentMouseTracker::setTentative(const ange::schedule::Call* call, const ange::vessel::Compartment* compartment) {
    if (m_tentativeCall || m_tentativeCompartment) {
        clearTentative();
    }
    m_tentativeCall = call;
    m_tentativeCompartment = compartment;
    emit compartmentChanged(compartment);
}

void CompartmentMouseTracker::removeCall(const ange::schedule::Call* call) {
    if (call == m_tentativeCall) {
        clearTentative();
    }
}


#include "compartmentmousetracker.moc"
