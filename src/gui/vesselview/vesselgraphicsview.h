#ifndef VESSEL_GRAPHICS_VIEW_H
#define VESSEL_GRAPHICS_VIEW_H

#include <QGraphicsView>
#include <QPointer>

class RecapManagerInterface;
class ToolManager;
class ContainerSelectionBundleHelper;
class BaySliceGraphicsItem;
class VesselGraphicsItem;
class bay_header_t;
class vessel_scene_t;
class schedule_header_t;
class QLabel;
class document_t;
class pool_t;
class tool_t;
namespace ange {
namespace angelstow {
class Problem;
}
namespace schedule {
class Call;
}
}

class vessel_graphics_view_t : public QGraphicsView {

    Q_OBJECT

public:

    vessel_graphics_view_t(QWidget* parent=0);
    void set_document(document_t* document);
    void setScene(vessel_scene_t* scene);
    void setRecapManager(RecapManagerInterface* manager);
    void set_no_zoom_and_pan(bool b);
    void set_selection_pool(pool_t* selection_pool);

public Q_SLOTS:

    void set_tool(tool_t* tool);
    void slot_scroll();
    /**
     * Add vessel_item to displayed items (well, really, the scrollbar)
     */
    void include_vessel_item(VesselGraphicsItem* vessel_item);

    /**
     * Remove vessel_item from displayed items (well, really, the scrollbar)
     */
    void exclude_vessel_item(VesselGraphicsItem* vessel_item);

   /**
    * Switch to showing vessels in 2-row display
    */
    void show_2_rows(bool activated=false);

    void update_column_headers(VesselGraphicsItem* vessel_item);

    /**
     * Move focus to call
     */
    void focus_call(const ange::schedule::Call* call);
    void setBundleHelper(ContainerSelectionBundleHelper* bundlehelper);

    void zoomToProblem(const ange::angelstow::Problem* problem);
    void setToolManager(ToolManager* m_toolManager);

private Q_SLOTS:

    void zoom_out();
    void zoom_out_large();
    void zoom_in();
    void zoom_in_large();

    /**
     * Clear current item, so that no item is current (that is, the keyboard has no focus)
     */
    void clear_current_item();

Q_SIGNALS:

    void tool_applied(QRectF scene_area, tool_t* tool);
    void call_activated(const ange::schedule::Call* call);
    void create_zoomed(QRectF zoomrect);
    void requestRecapOfSelection();

protected:

    virtual void wheelEvent(QWheelEvent* event);
    virtual void mousePressEvent(QMouseEvent * event);
    virtual void mouseReleaseEvent(QMouseEvent * event);
    virtual void mouseMoveEvent(QMouseEvent * event);
    virtual void paintEvent(QPaintEvent* event);
    virtual void contextMenuEvent(QContextMenuEvent* event);
    virtual void resizeEvent(QResizeEvent* event);
    virtual void keyPressEvent(QKeyEvent* event);
    virtual void keyReleaseEvent(QKeyEvent* event);
    virtual void showEvent(QShowEvent* event);

private:

    void update_headers();
    void zoom(double amount);

    /**
     * Sets the current item to new_current, and ensures it is visible
     * It also changes the current call if necessary
     */
    void set_current_item(BaySliceGraphicsItem* new_current, bool focus);
    void move_current_right();
    void move_current_left();
    void move_current_up();
    void move_current_down();
    void unstow_selected_containers();
    void zoom_to_vessel(VesselGraphicsItem* vessel);
    void zoom_to_bay(BaySliceGraphicsItem* bay);
    QPoint m_cursor;
    bool m_dragmode;
    tool_t* m_current_tool;
    bool m_rubber_mode;
    QRect m_rubber_drag;
    pool_t* m_selection_pool;
    document_t* m_document;
    QPoint m_scroll_speed; // Well, it really should be a vector..
    QTimer* m_scroll_timer;
    schedule_header_t* m_schedule_header;
    bay_header_t* m_bay_header;
    bool m_no_zoom_and_pan;
    QAction* m_zoom_out_action;
    QAction* m_zoom_out_large_action;
    QAction* m_zoom_in_action;
    QAction* m_zoom_in_large_action;
    BaySliceGraphicsItem* m_current_item;
    QPointer<ContainerSelectionBundleHelper> m_bundlehelper;
    ToolManager* m_toolManager;
    RecapManagerInterface* m_recapManager;

};

#endif // VESSEL_GRAPHICS_VIEW_H
