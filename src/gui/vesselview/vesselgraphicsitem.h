#ifndef VESSEL_ITEM_H
#define VESSEL_ITEM_H

#include <QGraphicsObject>
#include <ange/vessel/point3d.h>

class BaySliceGraphicsItem;
class CompartmentMouseTracker;
class document_t;
class observer_position_item_t;
class pool_t;
namespace ange {
namespace schedule {
class Call;
}
namespace vessel {
class Vessel;
}
}

class VesselGraphicsItem : public QGraphicsObject {

    Q_OBJECT

public:

    VesselGraphicsItem(document_t* document, const pool_t* m_selection_pool, CompartmentMouseTracker* macroStowageMouseTracker,
                       const ange::schedule::Call* call = 0L, int nb_rows = 1, QGraphicsItem* itemParent = 0L);

    virtual void paint(QPainter* painter, const QStyleOptionGraphicsItem*, QWidget* = 0);
    virtual QRectF boundingRect() const;

    /**
     * @return call for this vessel item
     */
    const ange::schedule::Call* call() const;

    /**
     * @return fore bay slices
     */
    const QList<BaySliceGraphicsItem*> fore_slices() const;

    /**
     * @return aft bay slices
     */
    const QList<BaySliceGraphicsItem*> aft_slices() const;

    /**
     * @return true if this vessel is the currently worked-on vessel (call)
     */
    bool current() const;

    /**
     * @return true if the visibility line should be drawn
     */
    bool get_draw_visibilityline_status() const;

    /**
     * @return number of slices per row (except last, which will contain
     * fore_slices().size % (bay_slice_per_row+1)
     */
    int bay_slices_per_row() const;

    /**
     * @return number of rows
     */
    int number_of_rows() const;


public Q_SLOTS:
    /**
     * Set the number of rows the vessel is drawn in (each row has a aft and fore row)
     */
    void set_rows(int rows);

    /**
     * Turn on/off bay number in all slice. This will adjust the bounding rect for the vessel item
     */
    void set_print_bay_numbers(bool on);

    /**
     * Turn on/off whether bulkheads are drawn
     **/
    void draw_bulkheads(bool draw);

    /**
     * Turn on/off whether visibilityline are drawn
     **/
    void draw_visibilityline(bool draw);

    /**
     * Set vessel
     */
    void set_vessel_from_document(document_t* document);

Q_SIGNALS:
    void changed();
    void changedbounding(QRectF bound);
    void bay_slice_items_changed(VesselGraphicsItem* vessel_item);

private:
    /**
     * Layout the slices
     */
    void layout_slices();

private:
    document_t* document;
    const pool_t* m_selectionPool;
    CompartmentMouseTracker* m_macroStowageMouseTracker;
    QRectF m_bounding_rect;
    QList<BaySliceGraphicsItem*> m_fore_slices;
    QList<BaySliceGraphicsItem*> m_aft_slices;
    QList<QGraphicsItem*> m_bulkheads;
    QSizeF m_bay_margin;
    int m_rows;
    const ange::schedule::Call* m_call;
    bool m_draw_bulkheads;
    bool m_draw_visibilityline;
    ange::vessel::Point3D m_observer_position;
    observer_position_item_t* m_observer_position_item;

};

#endif // VESSEL_ITEM_H
