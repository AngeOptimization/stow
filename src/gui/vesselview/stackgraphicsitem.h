#ifndef STACK_GRAPHICS_ITEM
#define STACK_GRAPHICS_ITEM

#include <QGraphicsItem>
#include <QObject>
#include <QDebug>
#include "gui/stow.h"
#include "gui/mode.h"
#include <ange/containers/isocode.h>

class pool_t;
class SelectionAreaBuilder;
class stowage_t;
struct vessel_paint_config_t;
class SlotGraphicsItem;
class stack_label_item_t;
class document_t;
class BaySliceGraphicsItem;
class StackProblemGraphicsItem;
class QTimer;
namespace ange {
namespace containers {
class Container;
}
namespace vessel {
class Stack;
}
namespace schedule {
class Call;
}
}

/**
 * The graphical representation of a stack
 */
class StackGraphicsItem : public QObject {
    Q_OBJECT

public:
    StackGraphicsItem(ange::vessel::Stack* stack, bool fore, document_t* document, const pool_t* selectionPool, BaySliceGraphicsItem* parent);

    virtual void paint(QPainter* painter, const QRectF& bounding_rect, const vessel_paint_config_t& config, SelectionAreaBuilder& selection_area_builder);
    ange::vessel::Stack* stack() const {
      return m_stack;
    }
    /**
     * The width of a container
     */
    static qreal row_no_size() {
      return 2.6;
    }

    QList<SlotGraphicsItem*> slot_items() const {
      return m_slot_items;
    }

    static qreal width();

    /**
     * @return the maximum possible height (will probably break stack_height)
     * This includes the row number
     */
    qreal max_height() const;

    /**
     * \return the height of the oddslots in the bottom of the 'sister stack'
     */
    qreal shadowOddSlotHeightBottom() const;

    /**
     * \return the height of the oddslots in the top of the 'sister stack'
     * NOTE: if there is no 'real slots' in this stack, \ref shadowOddSlotHeightBottom
     * will have the full height;
     */
    qreal shadowOddSlotHeightTop() const;

    /**
     * \return the height of the oddslots in the 'sister stack' (top and bottom combined)
     */
    qreal shadowOddSlotHeight() const {
        return shadowOddSlotHeightBottom() + shadowOddSlotHeightTop();
    }

    /**
     * @return actual height, using actual containers and no row number
     * @param call the call to measure the height in
     */
    ange::units::Length height(const ange::schedule::Call* call) const;

    /**
     * @return the stack label for this stack
     **/
    stack_label_item_t* stack_label_item() const {
      return m_stack_label_item;
    }

    StackProblemGraphicsItem* stackProblemGraphicsItem() const;

    /**
     * @return bay_slice_item_t where this stack_item_t resides
     */
    BaySliceGraphicsItem* bay_slice_item() const;

    /**
     * @returns true iff slot is legal
     */
    bool legal_for_current_selection(const SlotGraphicsItem* slot) const;

    /**
     * @return a list of human readable reasons why this slot is not legal for current selection
     */
    QStringList illegal_reasons(const SlotGraphicsItem* slot_item, QList< const ange::containers::Container* > containers) const;

  public Q_SLOTS:
    void slot_set_row_labels(mode_enum_t::row_label_mode_t row_label_mode);
    void update(SlotGraphicsItem* slot);
    void update(stack_label_item_t* slot);

    /**
     * Sets the default legality, that is, whether slots not specifically listed as legal are legal
     */
    void set_default_legality(bool default_legality);

    /**
     * Set legality to true if any of the new containers can be stowed in the slots
     */
    void set_legality_according_to_added_container_rows(QList<int> rows);

    /**
     * Set legality to false if there is no longer any containers that can be stowed in the slots
     */
    void set_legality_according_to_removed_container_rows(QList<int> rows);

    /**
     * Set the legality according to the current selection
     */
    void set_legality_from_selection_later();

  private Q_SLOTS:
    void set_legality_from_selection();
  private:
    ange::vessel::Stack* m_stack;
    document_t* m_document;
    const pool_t* m_selectionPool;
    mode_enum_t::row_label_mode_t m_row_label_mode;
    bool m_fore;
    stack_label_item_t* m_stack_label_item;
    StackProblemGraphicsItem* m_stackLabelProblemItem;
    QList<SlotGraphicsItem*> m_slot_items;
    qreal m_slots_height;
    bool m_default_legality;
    struct legality_t {
      legality_t() : count(0), more(false) {}
      int count;
      bool more;
    };
    QTimer* m_update_timer;
    friend QDebug operator<<(QDebug dbg, const StackGraphicsItem::legality_t& legality);
    void set_legality_from_selection(SlotGraphicsItem* slot_item, legality_t& legality, const ange::schedule::Call* call, stowage_t* stowage, QList< const ange::containers::Container* > containers);
    qreal m_shadowOddslotsHeightBottom;
    qreal m_shadowOddslotsHeightTop;

    static int const MAX_LEGAL = 30;

    QList<legality_t> m_slot_legality;
};

#endif // STACK_GRAPHICS_ITEM
