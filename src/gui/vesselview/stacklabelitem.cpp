#include "stacklabelitem.h"

#include <ange/vessel/stack.h>
#include <ange/vessel/stacksupport.h>
#include <ange/vessel/slot.h>
#include <ange/vessel/vessel.h>
#include <ange/vessel/bayslice.h>

#include <cmath>
#include <algorithm>
#include <limits>

#include <QPainter>
#include <QHelpEvent>
#include <QIcon>

#include "stackgraphicsitem.h"
#include "document/document.h"
#include "vesselscene.h"
#include "document/stowage/slot_call_content.h"
#include "gui/utils/painter_utils.h"
#include "document/stowage/stowage.h"
#include "document/problems/problem_model.h"
#include "stowplugininterface/problemlocation.h"
#include "document/stowage/stowagestack.h"
#include "slotgraphicsitem.h"
#include "vesselpaintconfig.h"
#include "bayslicegraphicsitem.h"
#include <document/stowage/visibility_line.h>
#include <limits>

using ange::vessel::Stack;
using ange::vessel::StackSupport;
using namespace ange::units;

stack_label_item_t::stack_label_item_t( stowage_t* stowage, ange::vessel::Stack* vessel_stack, bool fore, StackGraphicsItem* parent):
    QObject(parent),
    m_stowage(stowage),
    m_stack(vessel_stack),
    m_fore(fore)
{
    setObjectName(QStringLiteral("StackLabelItem(%1)").arg(m_stack->objectName()));
  Q_ASSERT(m_stowage);
  Q_ASSERT(m_stack);
}

void stack_label_item_t::paintText(QPainter* painter, const QRectF& boundingRect, const vessel_paint_config_t& config) {
  if(config.lod < 2.67) {
      return;
  }
  QString prot_row_label;
  qreal height = boundingRect.size().height() / 2;
  QRectF bound_top = boundingRect.adjusted(0, -0.35 * height, 0, height + 0.35 * height);
  QRectF bound_bottom = boundingRect.adjusted(0, -height + 0.35 * height, 0, 0.35 * height);
  if (StowageStack* stowage_stack = m_stowage->stowage_stack(config.call, m_stack)) {
      const Stack* vs = stowage_stack->vessel_stack();
      StackSupport* stack_support_20 = m_fore ? vs->stackSupport20Fore() : vs->stackSupport20Aft();

      switch (config.row_label_mode)
      {
        case mode_enum_t::NAME:
        {
          QString row_label = rowname(stowage_stack);
          prot_row_label = "99.";
          ange::painter_utils::draw_text_in_rect(painter, boundingRect, row_label,
                                                 painter->fontMetrics().boundingRect(prot_row_label));
          break;
        }
        case mode_enum_t::MAX_HEIGHT:
        {
          const StackSupport* ss = stack_support_20 ? stack_support_20 : vs->stackSupport40();
          QString row_label_top = ss ? QString::number(ss->maxHeight()/meter, 'f', 1) : QString();
          QString row_label_bottom = QString::number(m_fore ? stowage_stack->foreHeight()/meter : stowage_stack->aftHeight()/meter, 'f', 1);
          prot_row_label = "99.9.";
          ange::painter_utils::draw_text_in_rect(painter, bound_top, row_label_top,
                                                 painter->fontMetrics().boundingRect(prot_row_label));
          ange::painter_utils::draw_text_in_rect(painter, bound_bottom, row_label_bottom,
                                                 painter->fontMetrics().boundingRect(prot_row_label));
          break;
        }
        case mode_enum_t::MAX_WEIGHT:
        {
          QString row_label_top = vs->stackSupport40() ? QString::number(vs->stackSupport40()->maxWeight() / ton, 'f', 0) : QString();
          QString row_label_bottom = stack_support_20 ? QString::number(stack_support_20->maxWeight() / ton, 'f', 0) : QString();
          prot_row_label = "999.9.";
          ange::painter_utils::draw_text_in_rect(painter, bound_top, row_label_top,
                                                 painter->fontMetrics().boundingRect(prot_row_label));
          ange::painter_utils::draw_text_in_rect(painter, bound_bottom, row_label_bottom,
                                                 painter->fontMetrics().boundingRect(prot_row_label));
          break;
        }
        case mode_enum_t::REMAINING_WEIGHT:
        {
          QString row_label_top;
          if (StackSupport* ss = vs->stackSupport40())
          {
            const Mass w40left = ss->maxWeight() - qMax(stowage_stack->weight40Aft(), stowage_stack->weight40Fore());
            row_label_top = QString::number(w40left / ton, 'f', 1);
          }
          QString row_label_bottom;
          if (stack_support_20)
          {
            const Mass w20left = stack_support_20->maxWeight() - (m_fore ? stowage_stack->weight20Fore() : stowage_stack->weight20Aft());
            row_label_bottom = QString::number(w20left / ton, 'f', 1);
          }
          prot_row_label = "999.9.";
          ange::painter_utils::draw_text_in_rect(painter, bound_top, row_label_top,
                                                 painter->fontMetrics().boundingRect(prot_row_label));
          ange::painter_utils::draw_text_in_rect(painter, bound_bottom, row_label_bottom,
                                                 painter->fontMetrics().boundingRect(prot_row_label));
          break;
        }
      }
    }
}

void stack_label_item_t::paint(QPainter* painter, const QRectF& boundingRect, const vessel_paint_config_t& config) {
    // shrink the rect to ensure the text behaves sanely
    QRectF textRect = boundingRect;
    const qreal scaledWidth = .8*BaySliceGraphicsItem::levelOfDetailBasedBayNumberTextRectSize(config.printMode,config.lod);
    qreal moveOuter = boundingRect.width() - scaledWidth;
    textRect.setLeft(boundingRect.left() + moveOuter/2);
    textRect.setRight(boundingRect.right() - moveOuter/2);
    paintText(painter, textRect, config);
}

QString stack_label_item_t::rowname(StowageStack* stowage_stack) {
  const Stack* vs = stowage_stack->vessel_stack();
  StackSupport* support = m_fore ? vs->stackSupport20Fore() : vs->stackSupport20Aft();
  if (!support) {
    support = vs->stackSupport40();
  }
  if (!support) {
    support = !m_fore ? vs->stackSupport20Fore() : vs->stackSupport20Aft();
  }
  return support ? QString::number(support->row()) : QString();
}

ange::units::Mass stack_label_item_t::get_maxweight(StowageStack* stowage_stack, ange::containers::IsoCode::IsoLength length) const {
  StackSupport* support = 0L;
  const Stack* vs = stowage_stack->vessel_stack();
  if (length == ange::containers::IsoCode::Twenty) {
    if (m_fore) {
      support = vs->stackSupport20Fore();
    } else {
      support = vs->stackSupport20Aft();
    }
  } else {
    support = vs->stackSupport40();
  }
  return support ? support->maxWeight() : std::numeric_limits<qreal>::infinity()*kilogram;
}

qreal stack_label_item_t::get_maxheight(StowageStack* stowage_stack) const {
  const Stack* vs = stowage_stack->vessel_stack();
  StackSupport* support = m_fore ?  vs->stackSupport20Fore() : vs->stackSupport20Aft();
  if (!support) {
    support = vs->stackSupport40();
  }
  return support ? support->maxHeight()/meter : std::numeric_limits<qreal>::infinity();
}

const ange::units::Mass stack_label_item_t::calc_weight(StowageStack* stowage_stack, ange::containers::IsoCode::IsoLength length) const {
  if (length == ange::containers::IsoCode::Twenty) {
    return m_fore ? stowage_stack->weight20Fore() : stowage_stack->weight20Aft();
  } else {
    return m_fore ? stowage_stack->weight40Fore() : stowage_stack->weight40Aft();
  }
}

qreal stack_label_item_t::calc_height(StowageStack* stowage_stack) const {
  return m_fore ? stowage_stack->foreHeight()/meter : stowage_stack->aftHeight()/meter;
}

QString stack_label_item_t::tooltip_text(const ange::schedule::Call* call) const {
  StowageStack* stowage_stack = m_stowage->stowage_stack(call, m_stack);
  // get information values
  bool visibility_line = stowage_stack->is_stack_in_forward_view();
  double max_visibilityline_height = std::numeric_limits< double >::max();
  if(visibility_line){
    max_visibilityline_height = stowage_stack->get_visibility_maxheight()/meter;
  }
  const double maxheight = get_maxheight(stowage_stack);
  const double height = calc_height(stowage_stack);
  const Mass maxweight20 = get_maxweight(stowage_stack,ange::containers::IsoCode::Twenty);
  const Mass maxweight40 = get_maxweight(stowage_stack,ange::containers::IsoCode::Fourty);
  const Mass weight20 = calc_weight(stowage_stack,ange::containers::IsoCode::Twenty);
  const Mass weight40 = calc_weight(stowage_stack,ange::containers::IsoCode::Fourty);
  QString tooltipstring = "<table>";
  tooltipstring += QString("<tr><th align=\"left\">Height</th><td>%1 m / %2 m</td></tr>").arg(QString::number(height, 'f', 2)).arg(QString::number(maxheight, 'f', 2));
  if(visibility_line && max_visibilityline_height < maxheight) {
    tooltipstring += QString("<tr><th align=\"left\">Visibility</th><td>%1 m / %2 m</td></tr>").arg(QString::number(height, 'f', 2)).arg(QString::number(max_visibilityline_height,'f',2));
  }
  tooltipstring += QString("<tr><th align=\"left\">Weight 20'</th><td>%1 T / %2 T</td></tr>").arg(QString::number(weight20 / ton, 'f', 1)).arg(QString::number(maxweight20 / ton, 'f', 1));
  tooltipstring += QString("<tr><th align=\"left\">Weight 40'</th><td>%1 T / %2 T</td></tr>").arg(QString::number(weight40 / ton, 'f', 1)).arg(QString::number(maxweight40 / ton, 'f', 1));
  tooltipstring += "</table>";
  return tooltipstring;
}


#include "stacklabelitem.moc"
