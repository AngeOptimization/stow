/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010

 Copyright: See COPYING file that comes with this distribution

*/

#ifndef CRANESPLIT_VIEW_H
#define CRANESPLIT_VIEW_H

#include <QGraphicsView>
#include "../common/abstractmulticallview.h"

class schedule_header_t;
class crane_split_item_t;
namespace ange {
namespace schedule {
class Call;
}
}

class document_t;
class cranesplit_view_t : public AbstractMultiCallView {
  Q_OBJECT
  public:
    cranesplit_view_t(QWidget* parent = 0);
  protected:
    virtual AbstractVesselGraphicsItem* create_item_from_call(ange::schedule::Call* call);

};

#endif // CRANESPLIT_VIEW_H
