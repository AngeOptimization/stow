#include "stackgraphicsitem.h"

#include "bayslicegraphicsitem.h"
#include "vesselscene.h"
#include "slotgraphicsitem.h"
#include "document/document.h"
#include "vesselgraphicsitem.h"
#include "stacklabelitem.h"
#include "gui/utils/painter_utils.h"
#include "vesselpaintconfig.h"
#include "document/stowage/slot_call_content.h"
#include "document/stowage/stowage.h"
#include "stowplugininterface/macroplan.h"
#include "document/userconfiguration/userconfiguration.h"
#include "document/stowage/stowagestack.h"
#include "document/containers/container_list.h"
#include "stowplugininterface/igui.h"
#include "document/stowage/container_leg.h"
#include "document/stowage/stowarbiter.h"
#include "document/pools/pool.h"
#include "selectionareabuilder.h"
#include "stackproblemgraphicsitem.h"

#include <ange/containers/container.h>
#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>
#include <ange/vessel/bayslice.h>
#include <ange/vessel/compartment.h>
#include <ange/vessel/slot.h>
#include <ange/vessel/stack.h>
#include <ange/vessel/stacksupport.h>

#include <QPainter>
#include <QTimer>

using namespace ange::units;
using ange::vessel::Stack;
using ange::vessel::StackSupport;
using ange::vessel::Slot;
using ange::vessel::Compartment;
using ange::schedule::Call;
using ange::vessel::Above;
using ange::vessel::Below;
using ange::containers::IsoCode;
using ange::containers::Container;

StackGraphicsItem::StackGraphicsItem(ange::vessel::Stack* stack, bool fore, document_t* document,
                                     const pool_t* selectionPool, BaySliceGraphicsItem* parent):
  QObject(parent),
  m_stack(stack),
  m_document(document),
  m_selectionPool(selectionPool),
  m_row_label_mode(mode_enum_t::NAME),
  m_fore(fore),
  m_slots_height(0.0),
  m_default_legality(true),
  m_update_timer(new QTimer(this)),
  m_shadowOddslotsHeightBottom(0.0),
  m_shadowOddslotsHeightTop(0.0)
{
    setObjectName(QStringLiteral("StackGraphicsItem(%1)").arg(m_stack->objectName()));
  bool bottom = true;
  m_slots_height = 0.0;
  Q_FOREACH(Slot* slot, stack->stackSlots()) {
    bool slot_aft = slot->isAft();
    SlotGraphicsItem* item = 0L;
    if(slot_aft ^ fore) {
        item = new SlotGraphicsItem(slot, document, m_selectionPool, this);
        bottom = false;
    } else {
      if(!slot->sister()) {
        // slot does not belong to our stack, and does not have a sister. let's
        // create an odd slot item here.
        item = new SlotGraphicsItem(0L, document, m_selectionPool, this);
      }
    }
    if (item) {
        m_slots_height += item->max_height();
        m_slot_items << item;
        m_slot_legality << legality_t();
        if(!item->slot()) {
            if(bottom) {
                m_shadowOddslotsHeightBottom += item->max_height();
            } else {
                m_shadowOddslotsHeightTop += item->max_height();
            }
        }
    }
  }

  // Make the stack label items.
  m_stack_label_item = new stack_label_item_t(document->stowage(), stack, fore, this);
  m_stackLabelProblemItem = new StackProblemGraphicsItem(document->problem_model(), stack, fore, this);
  StowageStack* stowage_stack = document->stowage()->stowage_stack(bay_slice_item()->call(), stack);
  connect(stowage_stack, SIGNAL(changed()), SLOT(set_legality_from_selection_later()));

  // Setup delayed update timer
  m_update_timer->setSingleShot(true);
  connect(m_update_timer, SIGNAL(timeout()), SLOT(set_legality_from_selection()));

}

qreal StackGraphicsItem::shadowOddSlotHeightBottom() const {
    return m_shadowOddslotsHeightBottom;
}

qreal StackGraphicsItem::shadowOddSlotHeightTop() const {
    return m_shadowOddslotsHeightTop;
}

qreal StackGraphicsItem::max_height() const {
  return m_slots_height + row_no_size();
}

qreal StackGraphicsItem::width() {
  return 2.4384;
}

int killedSlots(stowage_t* stowage, const Stack* stack, const Call* call, bool fore) {
    if (StowageStack* stowage_stack = stowage->stowage_stack(call, stack)) {
        return stowage_stack->slots_killed(fore);
    }
    return 0;
}
void StackGraphicsItem::paint(QPainter* painter, const QRectF& bounding_rect, const vessel_paint_config_t& config, SelectionAreaBuilder& selection_area_builder) {

  const qreal width = bounding_rect.width();

  QPen blackCosmeticPen(painter->pen());
  blackCosmeticPen.setWidth(0);

  // Paint slots
  QPointF stack_bottom_left = (m_stack->level() == Above)
    ? bounding_rect.topLeft()
    : bounding_rect.topLeft()+QPointF(0.0, row_no_size());
  QPointF slot_bottom_left = stack_bottom_left;
  selection_area_builder.begin_stack(bounding_rect.left(), bounding_rect.right());
  QRectF selected_rect;
  const bool fastdraw = config.lod < 1.5;
  int unkilled = m_slot_items.size() - killedSlots(m_document->stowage(), m_stack, config.call,m_fore);
  qreal accumulated_fastdraw_height = 0.0;
  QBrush current_fastdraw_brush;
  int index = -1;
  SlotGraphicsItem* topitem = 0;
  Q_FOREACH(SlotGraphicsItem* item, m_slot_items) {
    ++index;
    painter->setPen(blackCosmeticPen);
    slot_call_content_t slot_contents = item->slot_contents(config.call);
    qreal height = item->height(slot_contents);
    const bool killed = --unkilled<0;
    const bool legal = m_default_legality || (m_slot_legality[index].count>0);
    if (item->selected()) {
      if (selected_rect.isValid()) {
        selected_rect.setHeight(selected_rect.height()+height);
      } else {
        selected_rect = QRectF(slot_bottom_left, QSizeF(bounding_rect.width(), height));
        if (fastdraw) {
          selected_rect.moveBottom(selected_rect.bottom()+accumulated_fastdraw_height);
        }
      }
    } else if (selected_rect.isValid()) {
      selection_area_builder.add_interval(selected_rect.top(), selected_rect.bottom());
      selected_rect = QRectF();
    }
    if (fastdraw) {
      QBrush slot_brush = item->get_brush_for_contents(slot_contents, killed, legal, config);
      if (slot_brush != current_fastdraw_brush) {
        painter->fillRect(QRectF(slot_bottom_left, QPointF(bounding_rect.right(), slot_bottom_left.y() + accumulated_fastdraw_height)), current_fastdraw_brush.style() == Qt::NoBrush ? QBrush(Qt::white) : current_fastdraw_brush);
        slot_bottom_left += QPointF(0.0, accumulated_fastdraw_height);
        accumulated_fastdraw_height = 0.0;
        current_fastdraw_brush = slot_brush;
      }
      accumulated_fastdraw_height += height;
    } else {
        painter->save();
      item->paint(painter, QRectF(slot_bottom_left, QSizeF(width, height)), config, legal, killed);
        painter->restore();
      painter->setPen(blackCosmeticPen);
      //bottom line of slots
      if(item->slot() || (m_stack->level() == ange::vessel::Above && !qFuzzyCompare(m_slots_height,m_shadowOddslotsHeightBottom) )) {
        painter->drawLine(QLineF(slot_bottom_left, slot_bottom_left+QPointF(bounding_rect.width(), 0.0)));
      }
      slot_bottom_left += QPointF(0.0, height);
    }
    topitem = item;
  }
  if (fastdraw) {
    painter->fillRect(QRectF(slot_bottom_left, QPointF(bounding_rect.right(), slot_bottom_left.y() + accumulated_fastdraw_height)), current_fastdraw_brush.style() == Qt::NoBrush ? QBrush(Qt::white) : current_fastdraw_brush);
    slot_bottom_left += QPointF(0.0, accumulated_fastdraw_height);
    painter->setPen(blackCosmeticPen);
    painter->drawLine(QLineF(slot_bottom_left, slot_bottom_left+QPointF(bounding_rect.width(), 0.0)));
    painter->drawLine(QLineF(stack_bottom_left, stack_bottom_left+QPointF(bounding_rect.width(), 0.0)));
  } else {
    // Draw top of last slot
      painter->setPen(blackCosmeticPen);
      if(topitem && topitem->slot()) {
        painter->drawLine(QLineF(slot_bottom_left, slot_bottom_left+QPointF(bounding_rect.width(), 0.0)));
      };
  }
  if (selected_rect.isValid()) {
    selection_area_builder.add_interval(selected_rect.top(), selected_rect.bottom());
  }


#if 0
  // bounding rect
  painter->setPen(Qt::yellow);
  painter->setBrush(QBrush());
  painter->drawRect(bounding_rect);
  painter->setPen(Qt::black);
#endif

}

BaySliceGraphicsItem* StackGraphicsItem::bay_slice_item() const {
  return qobject_cast< BaySliceGraphicsItem* >(parent());
}

void StackGraphicsItem::slot_set_row_labels(mode_enum_t::row_label_mode_t row_label_mode) {
  m_row_label_mode = row_label_mode;
}

ange::units::Length StackGraphicsItem::height(const ange::schedule::Call* call) const {
  const StowageStack* stowage_stack = m_document->stowage()->stowage_stack(call, stack());
  ange::units::Length height = m_fore ? stowage_stack->foreHeight() : stowage_stack->aftHeight();
  height += (m_slot_items.size() - (m_fore ? stowage_stack->fore_count() : stowage_stack->aft_count())) * 2.5908*meter;
  return height;
}

void StackGraphicsItem::update(SlotGraphicsItem* /*slot*/) {
  qobject_cast<BaySliceGraphicsItem*>(parent())->update();
}

void StackGraphicsItem::update(stack_label_item_t* /*slot*/) {
  qobject_cast<BaySliceGraphicsItem*>(parent())->update();

}

void StackGraphicsItem::set_legality_according_to_added_container_rows(QList< int > rows)
{
  container_list_t* containers = m_document->containers();
  const Call* call = bay_slice_item()->call();
  stowage_t* stowage = m_document->stowage();
  for (int i=0; i<m_slot_legality.size(); ++i) {
    if (m_slot_items[i]->slot_contents(call).container()) {
      m_slot_legality[i] = legality_t();
      continue;
    }
    if (Slot* slot = m_slot_items[i]->slot()) {
      legality_t& sl = m_slot_legality[i];
      if (sl.more) {
        continue;
      }
      StowArbiter arbiter(stowage, m_document->schedule(), slot, call);
      for (int row_index = 0, nrows = rows.size(); row_index < nrows; ++row_index) {
        const int row = rows.at(row_index);
        const ange::containers::Container* container = containers->get_container_at(row);
        if (arbiter.isLegal(container)) {
          if (sl.count > MAX_LEGAL) {
            sl.more = true;
            break;
          }
          if (++sl.count == 1) {
            update(m_slot_items[i]);
          }
        }
      }
    }
  }
}

void StackGraphicsItem::set_legality_according_to_removed_container_rows(QList< int > rows)
{
  // If container is removed, due to the delayed nature of set_legality_from_selection,
  // it can happen that we get a "removed", where the container underneath have no destination
  // This will confuse this function, and in any case, carefully tracking removals and then later
  // rebuilding the data structure is waste of cycles. So just return.
  if (m_update_timer->isActive()) {
    return;
  }
  container_list_t* containers = m_document->containers();
  QList<const Container*> selected_containers = m_selectionPool->containers();
  const bool selectedEmpty = selected_containers.isEmpty();
  const Call* call = bay_slice_item()->call();
  stowage_t* stowage = m_document->stowage();
  for (int i=0, isize=m_slot_legality.size(); i<isize; ++i) {
    legality_t& sl = m_slot_legality[i];
    if (sl.count == 0) {
      continue;
    }
    if (selectedEmpty || m_slot_items[i]->slot_contents(call).container()) {
      m_slot_legality[i] = legality_t();
      continue;
    }
    if (Slot* slot = m_slot_items[i]->slot()) {
      StowArbiter arbiter(stowage, m_document->schedule(), slot, call);
      Q_FOREACH(int row, rows) {
        const ange::containers::Container* container = containers->get_container_at(row);
        if (arbiter.isLegal(container)) {
          if (--sl.count == 0) {
            if (sl.more) {
              set_legality_from_selection(m_slot_items[i], sl, call, stowage, selected_containers);
            } else {
              update(m_slot_items[i]);
            }
            break;
          }
        }
      }
    }
  }
}

void StackGraphicsItem::set_legality_from_selection(SlotGraphicsItem* slot_item, StackGraphicsItem::legality_t& legality, const ange::schedule::Call* call, stowage_t* stowage, QList< const ange::containers::Container* > containers)
{
    //TODO: does this work? is this supposed to impose visibility line arbiter illegality?
  if (m_default_legality || slot_item->slot_contents(call).container()) {
    legality = legality_t();
    return;
  }
  const bool was_legal = legality.count > 0 || legality.more || m_default_legality;
  legality = legality_t();
  if (Slot* slot = slot_item->slot()) {
    StowArbiter arbiter(stowage, m_document->schedule() ,slot, bay_slice_item()->call());
    Q_FOREACH(const ange::containers::Container* container, containers) {
      if (arbiter.isLegal(container)) {
        if (++legality.count > MAX_LEGAL) {
          legality.more = true;
          break;
        }
      }
    }
  }
  const bool is_legal = legality.count > 0;
  if (was_legal != is_legal) {
    update(slot_item);
  }

}

void StackGraphicsItem::set_legality_from_selection()
{
  QList<const Container*> selected_containers = m_selectionPool->containers();
  stowage_t* stowage = m_document->stowage();
  const Call* call = bay_slice_item()->call();
  for (int i=0, isize=m_slot_legality.size(); i<isize; ++i) {
    set_legality_from_selection(m_slot_items[i], m_slot_legality[i], call, stowage, selected_containers);
  }
}


void StackGraphicsItem::set_legality_from_selection_later()
{
  if (!m_update_timer->isActive()) {
    m_update_timer->start();
  }
}

void StackGraphicsItem::set_default_legality(bool default_legality)
{
  m_default_legality = default_legality;
  qobject_cast<BaySliceGraphicsItem*>(parent())->update();
}

QDebug operator<<(QDebug dbg, const StackGraphicsItem::legality_t& legality)
{
  dbg.nospace() << legality.count;
  if (legality.more) {
    dbg << "+";
  }
  return dbg.space();

}

QStringList StackGraphicsItem::illegal_reasons(const SlotGraphicsItem* slot_item, QList<const ange::containers::Container*> containers) const
{
  QSet<int> reasons;
  if (Slot* slot = slot_item->slot()) {
    stowage_t* stowage = m_document->stowage();
    const Call* call = bay_slice_item()->call();
    StowArbiter arbiter(stowage, m_document->schedule() ,slot, call);
    if (containers.size() == 1) {
      // Querying one container happens when asking why a placed container is illegal.
      if (stowage->contents(slot, call).container() == containers.front()) {
        // OK, asking about this container for each own slot. That means that *free_until should use the following call..
        arbiter.setException(containers.front());
      }
    }
    Q_FOREACH(const ange::containers::Container* container, containers) {
      arbiter.whyNot(container, reasons);
    }
    // Reasons gathered, convert them to strings
    QStringList rv = arbiter.toStrings(reasons);
    return rv;
  }
  qWarning("illegal reasons queried for odd slot");
  return QStringList();
}

bool StackGraphicsItem::legal_for_current_selection(const SlotGraphicsItem* slot) const {
  if (m_default_legality == true) {
    return true;
  }
  const int index = m_slot_items.indexOf(const_cast<SlotGraphicsItem*>(slot));
  if (index >=0 ) {
    return m_slot_legality.at(index).count > 0;
  }
  qWarning("Requested legal status for slot outside of stack");
  return true;
}

StackProblemGraphicsItem* StackGraphicsItem::stackProblemGraphicsItem() const {
    return m_stackLabelProblemItem;
}


#include "stackgraphicsitem.moc"
