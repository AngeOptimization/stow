/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010

 Copyright: See COPYING file that comes with this distribution

*/

#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>

#include "cranesplitview.h"
#include "gui/cranesplit/crane_split_item.h"
#include "document/document.h"
#include "scheduleheader.h"
#include <QScrollBar>
#include "document/utils/callutils.h"

using ange::schedule::Call;
using ange::schedule::Schedule;

cranesplit_view_t::cranesplit_view_t(QWidget* parent) :
    AbstractMultiCallView(parent)
{
}

AbstractVesselGraphicsItem* cranesplit_view_t::create_item_from_call(ange::schedule::Call* call) {
  return is_befor_or_after(call) ? 0L :  new crane_split_item_t(m_document->stowage(), call, m_document->vessel(), 0L);
}

#include "cranesplitview.moc"
