#include "gui_interface.h"
#include "mainwindow.h"
#include <QMenuBar>
#include <QDockWidget>
#include "recapview.h"
#include <QDateTime>
#include <document/pools/pool.h>
#include <document/document.h>
#include <document/containers/container_list.h>

gui_interface_t::gui_interface_t(MainWindow* mainwindow)
 : ange::angelstow::IGui(mainwindow),
   m_main_window(mainwindow)
{

}

void gui_interface_t::installMenuAction(ange::angelstow::IGui::Menus menu, QAction* action)
{
  QMenuBar* menubar = m_main_window->menuBar();
  Q_FOREACH(QAction* a, menubar->actions()) {
    if (QMenu* m = a->menu()) {
      QString name = m->objectName();
      switch (menu) {
        case FileMenu: {
          if (name == "menuFile") {
            QList<QAction*> filemenu_entries = m->actions();
            int i;
            for (i=filemenu_entries.size()-1; i>=0; --i) {
              if (filemenu_entries[i]->isSeparator()) {
                break;
              }
            }
            if (i>=0) {
              m->insertAction(filemenu_entries[i], action);
            } else {
              Q_ASSERT(false);
            }
          }
          break;
        }
        case TaskMenu: {
          if(name == "menuT_asks") {
            a->menu()->addAction(action);
          }
          break;
        }
      }
    }
 }
 return;
}

void gui_interface_t::installViewPanel(QWidget* view_panel, QString title)
{
  QDockWidget* dockable_panel = new QDockWidget(title,m_main_window);
  dockable_panel->setWidget(view_panel);
  dockable_panel->show();
  dockable_panel->resize(view_panel->sizeHint());
  m_main_window->addDockWidget(Qt::LeftDockWidgetArea,dockable_panel);
  dockable_panel->setObjectName(title+"list_dock");
  m_main_window->updateShowPanelMenu();
  return;
}

const ange::schedule::Call* gui_interface_t::currentCall() const{
  return m_main_window->current_call();
}

QList< const ange::containers::Container* > gui_interface_t::selectedContainers() const
{
  QList<int> rows = m_main_window->selection_pool()->included_rows();
  QList< const ange::containers::Container* > rv;
  container_list_t* containers = m_main_window->document()->containers();
  Q_FOREACH(int row, rows) {
    rv << containers->at(row);
  }
  return rv;
}
