#include "loadcalldelegate.h"
#include <document/scheduleproxymodel.h>
#include <document/schedulemodel.h>
#include <document/containers/basic_container_list.h>
#include <ange/schedule/call.h>
#include <QComboBox>

LoadCallDelegate::LoadCallDelegate(ScheduleModel* scheduleModel, ange::schedule::Schedule* schedule,
                                   int dischargeColumn, QObject* parent)
:QStyledItemDelegate(parent), m_scheduleModel(scheduleModel), m_schedule(schedule), m_dischargeColumn(dischargeColumn) {
}


QWidget* LoadCallDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const {
    Q_UNUSED(option);
    QComboBox* combo = new QComboBox(parent);
    ScheduleProxyModel* model = new ScheduleProxyModel(combo);
    model->setSourceModel(m_scheduleModel);
    model->setSchedule(m_schedule);
    QModelIndex dischIndex = index.sibling(index.row(), m_dischargeColumn);
    QVariant dischVar = dischIndex.data(Qt::EditRole);
    Q_ASSERT(dischVar.canConvert<ange::schedule::Call*>());
    model->setDischargeCall(dischVar.value<ange::schedule::Call*>());
    combo->setModel(model);
    return combo;
}

void LoadCallDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const {
    QComboBox* combo = qobject_cast<QComboBox*>(editor);
    Q_ASSERT(combo);
    Q_UNUSED(combo);
    Q_ASSERT(index.data(Qt::EditRole).canConvert<ange::schedule::Call*>());
    Q_UNUSED(index);
}

void LoadCallDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const {
    QComboBox* combo = qobject_cast<QComboBox*>(editor);
    Q_ASSERT(combo);
    model->setData(index,combo->itemData(combo->currentIndex()), Qt::EditRole);
}

#include "loadcalldelegate.moc"
