#ifndef LOADCALLDELEGATE_H
#define LOADCALLDELEGATE_H

#include <QStyledItemDelegate>

namespace ange {
namespace schedule {
class Schedule;
}
}

class ScheduleModel;
/**
 * A delegate to populate a load call combo box
 */
class LoadCallDelegate : public QStyledItemDelegate {
    Q_OBJECT
    public:
        explicit LoadCallDelegate(ScheduleModel* scheduleModel, ange::schedule::Schedule* schedule, int dischargeColumn, QObject* parent = 0);
        virtual QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const;
        virtual void setEditorData(QWidget* editor, const QModelIndex& index) const;
        virtual void setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const;
    private:
        ScheduleModel* m_scheduleModel;
        ange::schedule::Schedule* m_schedule;
        const int m_dischargeColumn;
};

#endif // LOADCALLDELEGATE_H
