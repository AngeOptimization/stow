#include "isocodemodel.h"

IsoCodeModel::IsoCodeModel(QObject* parent) : QAbstractTableModel(parent)  {
    m_isoCodes  << "22G0"
                << "22P1"
                << "22R0"
                << "22T2"
                << "22U1"
                << "25G0"
                << "28G0"
                << "29P0"
                << "42G0"
                << "42P1"
                << "42R0"
                << "42T2"
                << "42U1"
                << "45G0"
                << "45R0"
                << "45P1"
                << "49P0"
                << "L5G0";
}

QVariant IsoCodeModel::data(const QModelIndex& index, int role) const {
    if (role == Qt::DisplayRole) {
        return m_isoCodes.at(index.row());
    }
    if (role == Qt::EditRole) {
        return m_isoCodes.at(index.row());
    }
    if (role == Qt::UserRole) {
        return m_isoCodes.at(index.row());
    }
    return QVariant();
}

int IsoCodeModel::columnCount(const QModelIndex& parent) const {
    Q_UNUSED(parent);
    return 1;
}

int IsoCodeModel::rowCount(const QModelIndex& parent) const {
    Q_UNUSED(parent);
    return m_isoCodes.count();
}

#include "isocodemodel.moc"
