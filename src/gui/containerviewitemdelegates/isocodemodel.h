#ifndef ISOCODEMODEL_H
#define ISOCODEMODEL_H

#include <QAbstractTableModel>

class IsoCodeModel : public QAbstractTableModel {
    typedef QAbstractTableModel super;
    Q_OBJECT
public:
    IsoCodeModel(QObject* parent);
    virtual QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const;
    virtual int columnCount(const QModelIndex& parent = QModelIndex()) const;
    virtual int rowCount(const QModelIndex& parent = QModelIndex()) const;

private:
    QList<QString> m_isoCodes; 
};

#endif // ISOCODEMODEL_H
