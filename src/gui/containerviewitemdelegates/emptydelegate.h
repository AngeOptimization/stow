#ifndef EMPTYDELEGATE_H
#define EMPTYDELEGATE_H
#include "truefalsewithcustomtextdelegate.h"

class EmptyDelegate : public TrueFalseWithCustomTextDelegate {
    Q_OBJECT
    public:
        explicit EmptyDelegate(QObject* parent = 0);
};

#endif // EMPTYDELEGATE_H
