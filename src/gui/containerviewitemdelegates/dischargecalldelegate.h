#ifndef DISCHARGECALLDELEGATE_H
#define DISCHARGECALLDELEGATE_H

#include <QStyledItemDelegate>

namespace ange {
namespace schedule {
class Schedule;
}
}

class ScheduleModel;
class DischargeCallDelegate : public QStyledItemDelegate {
    Q_OBJECT
    public:
        explicit DischargeCallDelegate(ScheduleModel* scheduleModel, ange::schedule::Schedule* schedule,
                                       int loadCallColumn, QObject* parent = 0);
        virtual QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const;
        virtual void setEditorData(QWidget* editor, const QModelIndex& index) const;
        virtual void setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const;
    private:
        ScheduleModel* m_scheduleModel;
        ange::schedule::Schedule* m_schedule;
        const int m_loadCallColumn;
};

#endif // DISCHARGECALLDELEGATE_H
