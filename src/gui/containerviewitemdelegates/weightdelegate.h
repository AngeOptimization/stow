#ifndef WEIGHTDELEGATE_H
#define WEIGHTDELEGATE_H

#include <QStyledItemDelegate>

class WeightDelegate : public QStyledItemDelegate {
    Q_OBJECT
    public:
        explicit WeightDelegate(QObject* parent = 0);
        virtual QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const ;
        virtual void setEditorData(QWidget* editor, const QModelIndex& index) const;
        virtual void setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const;
};

#endif // WEIGHTDELEGATE_H
