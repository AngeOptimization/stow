#include "truefalsewithcustomtextdelegate.h"
#include <QComboBox>

TrueFalseWithCustomTextDelegate::TrueFalseWithCustomTextDelegate(QString trueText, QString falseText, QObject* parent)
    : QStyledItemDelegate(parent), m_trueText(trueText), m_falseText(falseText) {
  // Empty
}


QWidget* TrueFalseWithCustomTextDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const {
    Q_UNUSED(option);
    Q_UNUSED(index);
    QComboBox* combo = new QComboBox(parent);
    combo->addItem(m_trueText, QVariant::fromValue<bool>(true));
    combo->addItem(m_falseText, QVariant::fromValue<bool>(false));
    return combo;
}

void TrueFalseWithCustomTextDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const {
    QComboBox* combo = qobject_cast<QComboBox*>(editor);
    Q_ASSERT(combo);
    Q_ASSERT(index.data(Qt::EditRole).canConvert<bool>());
    if(index.data(Qt::EditRole).value<bool>()) {
        combo->setCurrentIndex(0);
    } else {
        combo->setCurrentIndex(1);
    }
}

void TrueFalseWithCustomTextDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const {
    QComboBox* combo = qobject_cast<QComboBox*>(editor);
    Q_ASSERT(combo);
    model->setData(index, combo->itemData(combo->currentIndex()));
}

#include "truefalsewithcustomtextdelegate.moc"
