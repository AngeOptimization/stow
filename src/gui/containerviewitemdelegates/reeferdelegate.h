#ifndef REEFERDELEGATE_H
#define REEFERDELEGATE_H

#include <QStyledItemDelegate>
#include "truefalsewithcustomtextdelegate.h"

class ReeferDelegate : public TrueFalseWithCustomTextDelegate {
    Q_OBJECT
    public:
        explicit ReeferDelegate(QObject* parent = 0);
};

#endif // REEFERDELEGATE_H
