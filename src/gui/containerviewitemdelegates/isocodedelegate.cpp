#include "isocodedelegate.h"
#include "isocodemodel.h"
#include <QComboBox>

IsoCodeDelegate::IsoCodeDelegate(QObject* parent): QStyledItemDelegate(parent) {
}

QWidget* IsoCodeDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const {
    Q_UNUSED(option);
    Q_UNUSED(index);
    QComboBox* combo = new QComboBox(parent);
    IsoCodeModel* model = new IsoCodeModel(combo);
    combo->setModel(model);
    return combo;
}

void IsoCodeDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const {
    QComboBox* combo = qobject_cast<QComboBox*>(editor);
    Q_ASSERT(combo);
    Q_UNUSED(combo);
    Q_UNUSED(index);
}

void IsoCodeDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const {
    QComboBox* combo = qobject_cast<QComboBox*>(editor);
    Q_ASSERT(combo);
    model->setData(index,combo->itemData(combo->currentIndex()), Qt::EditRole);    
}

#include "isocodedelegate.moc"
