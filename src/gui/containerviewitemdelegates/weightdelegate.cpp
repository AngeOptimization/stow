#include "weightdelegate.h"
#include <ange/units/units.h>
#include <QLineEdit>

WeightDelegate::WeightDelegate(QObject* parent): QStyledItemDelegate(parent) {
   // nothing
}


QWidget* WeightDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const {
    Q_UNUSED(option);
    Q_UNUSED(index);
    QLineEdit* edit = new QLineEdit(parent);
    QDoubleValidator* validator = new QDoubleValidator(edit);
    validator->setDecimals(1);
    validator->setTop(99.9);
    validator->setBottom(0);
    edit->setValidator(validator);
    return edit;
}

void WeightDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const {
    Q_ASSERT(index.data(Qt::EditRole).canConvert<ange::units::Mass>());
    ange::units::Mass mass = index.data(Qt::EditRole).value<ange::units::Mass>();
    QLineEdit* edit = qobject_cast<QLineEdit*>(editor);
    Q_ASSERT(edit);
    edit->setText(QString::number(mass/ange::units::ton, 'f', 1));
}

void WeightDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const {
    QLineEdit* edit = qobject_cast<QLineEdit*>(editor);
    Q_ASSERT(edit);
    QString text = edit->text();
    bool ok;
    double value = text.toDouble(&ok);
    Q_ASSERT(ok);
    ange::units::Mass mass(value * 1000 /* kg to tons*/);
    model->setData(index, QVariant::fromValue<ange::units::Mass>(mass), Qt::EditRole);
}

#include "weightdelegate.moc"
