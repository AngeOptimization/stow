#ifndef REDUCEDISOCODEDELEGATE_H
#define REDUCEDISOCODEDELEGATE_H

#include <QStyledItemDelegate>

/**
 * A delegate presenting a subset of container ISO codes
 */
class IsoCodeDelegate : public QStyledItemDelegate {
    Q_OBJECT
public:
    explicit IsoCodeDelegate(QObject* parent = 0);
    virtual QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const;
    virtual void setEditorData(QWidget* editor, const QModelIndex& index) const;
    virtual void setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const;
private:
    QList<QString> m_isoCodes;
};

#endif // REDUCEDISOCODEDELEGATE_H
