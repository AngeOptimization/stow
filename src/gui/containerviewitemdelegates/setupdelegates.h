#ifndef CONTAINERVIEWITEMDELEGATES_SETUPDELEGATES_H
#define CONTAINERVIEWITEMDELEGATES_SETUPDELEGATES_H
#include <QTableView>
#include <document/containers/basic_container_list.h>
#include <document/document.h>
#include "emptydelegate.h"
#include "reeferdelegate.h"
#include "livedelegate.h"
#include "loadcalldelegate.h"
#include "dischargecalldelegate.h"
#include "weightdelegate.h"

class document_t;
namespace ContainerViewItemDelegates {
    inline void setupDelegates(QTableView* tableview, document_t* document) {
        tableview->setItemDelegateForColumn(basic_container_list_t::EMPTY_COL, new EmptyDelegate(document /* QObject parent*/));
        tableview->setItemDelegateForColumn(basic_container_list_t::REEFER_COL, new ReeferDelegate(tableview /* QObject parent */));
        tableview->setItemDelegateForColumn(basic_container_list_t::LIVE_COL, new LiveDelegate(tableview /* QObject parent */));
        tableview->setItemDelegateForColumn(basic_container_list_t::LOAD_CALL_COL, new LoadCallDelegate(
            document->schedule_model(), document->schedule(), basic_container_list_t::DISCHARGE_CALL_COL, document /* QObject parent*/ ));
        tableview->setItemDelegateForColumn(basic_container_list_t::DISCHARGE_CALL_COL, new DischargeCallDelegate(
            document->schedule_model(), document->schedule(), basic_container_list_t::LOAD_CALL_COL , document /* QObject parent*/ ));
        tableview->setItemDelegateForColumn(basic_container_list_t::WEIGHT_COL, new WeightDelegate(document /* QObject parent*/));
    }
};

#endif // CONTAINERVIEWITEMDELEGATES_SETUPDELEGATES_H
