#ifndef TRUEFALSEWITHCUSTOMTEXTDELEGATE_H
#define TRUEFALSEWITHCUSTOMTEXTDELEGATE_H

#include <QStyledItemDelegate>

class TrueFalseWithCustomTextDelegate : public QStyledItemDelegate {
    Q_OBJECT
    public:
        explicit TrueFalseWithCustomTextDelegate(QString trueText, QString falseText,QObject* parent = 0);
        virtual QWidget* createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const;
        virtual void setEditorData(QWidget* editor, const QModelIndex& index) const;
        virtual void setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const;
    private:
        QString m_trueText;
        QString m_falseText;
};

#endif // TRUEFALSEWITHCUSTOMTEXTDELEGATE_H
