#include "dischargecalldelegate.h"
#include <document/containers/basic_container_list.h>
#include <document/scheduleproxymodel.h>
#include <document/schedulemodel.h>
#include <ange/schedule/call.h>
#include <QComboBox>

DischargeCallDelegate::DischargeCallDelegate(ScheduleModel* scheduleModel, ange::schedule::Schedule* schedule,
                                             int loadCallColumn, QObject* parent)
: QStyledItemDelegate(parent), m_scheduleModel(scheduleModel), m_schedule(schedule), m_loadCallColumn(loadCallColumn) {
}


QWidget* DischargeCallDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& option, const QModelIndex& index) const {
    Q_UNUSED(option);
    QComboBox* combo = new QComboBox(parent);
    ScheduleProxyModel* model = new ScheduleProxyModel(combo);
    model->setSourceModel(m_scheduleModel);
    model->setSchedule(m_schedule);
    QModelIndex loadIndex = index.sibling(index.row(), m_loadCallColumn);
    QVariant loadVar = loadIndex.data(Qt::EditRole);
    Q_ASSERT(loadVar.canConvert<ange::schedule::Call*>());
    model->setLoadCall(loadVar.value<ange::schedule::Call*>());
    combo->setModel(model);
    return combo;
}

void DischargeCallDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const {
    QComboBox* combo = qobject_cast<QComboBox*>(editor);
    Q_ASSERT(combo);
    Q_UNUSED(combo);
    Q_ASSERT(index.data(Qt::EditRole).canConvert<ange::schedule::Call*>());
    Q_UNUSED(index);

    // maybe set current index in the combo to current value.
}

void DischargeCallDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const {
    QComboBox* combo = qobject_cast<QComboBox*>(editor);
    Q_ASSERT(combo);
    model->setData(index,combo->itemData(combo->currentIndex()), Qt::EditRole);
}

#include "dischargecalldelegate.moc"
