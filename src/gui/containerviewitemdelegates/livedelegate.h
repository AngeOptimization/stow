#ifndef LIVEDELEGATE_H
#define LIVEDELEGATE_H

#include <QStyledItemDelegate>
#include "truefalsewithcustomtextdelegate.h"

class LiveDelegate : public TrueFalseWithCustomTextDelegate {
    Q_OBJECT
    public:
        explicit LiveDelegate(QObject* parent = 0);
};

#endif // LIVEDELEGATE_H
