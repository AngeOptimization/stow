#include "schedule_view.h"

#include "callutils.h"
#include "container_leg.h"
#include "container_list.h"
#include "container_list_change_command.h"
#include "container_move.h"
#include "containerdeleter.h"
#include "dialogs/call_create_dialog.h"
#include "document.h"
#include "frontcallpopper.h"
#include "macrocompartmentcommand.h"
#include "stowplugininterface/macroplan.h"
#include "merger_command.h"
#include "schedule_change_command.h"
#include "schedule_view_item_delegate.h"
#include "schedulechangerotationcommand.h"
#include "schedulemodel.h"
#include "schedulepivotportcommand.h"
#include "stowage.h"

#include <ange/containers/container.h>
#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>
#include <ange/vessel/bayslice.h>
#include <ange/vessel/vessel.h>

#include <QApplication>
#include <QHeaderView>
#include <QMenu>
#include <QMessageBox>
#include <QMouseEvent>

using ange::schedule::Schedule;
using ange::schedule::Call;;
using ange::containers::Container;
using ange::angelstow::MacroPlan;

schedule_view_t::schedule_view_t(QWidget *parent) :
    QTableView(parent),
    m_current_call(0L),
    m_document(0L),
    m_selected_pool(0L)
{
  setSelectionBehavior(SelectRows);
  setSelectionMode(SingleSelection);
  verticalHeader()->hide();
}

void schedule_view_t::select(int i) {
  if(i<0 || !model() || model()->rowCount()<=i) {
    Q_ASSERT(false);
  } else {
    selectionModel()->select(QItemSelection(model()->index(i, 0), model()->index(i, model()->columnCount()-1)), QItemSelectionModel::ClearAndSelect);
  }
}

void schedule_view_t::select(const ange::schedule::Call* call) {
    for (int index = 0; index < model()->rowCount(); ++index) {
        QModelIndex idx = model()->index(index,0);
        if(idx.data(Qt::UserRole).canConvert<Call*>()) {
            if (idx.data(Qt::UserRole).value<Call*>() == call) {
                select(index);
                return;
            }
        }
    }
}

void schedule_view_t::set_document(document_t* document, pool_t* selected_pool) {
  if (!document || !selected_pool) {
    Q_ASSERT(document);
    Q_ASSERT(selected_pool);
    return;
  }
  m_document = document;
  m_selected_pool = selected_pool;
  ScheduleModel* model = document->schedule_model();
  setModel(model);
  setItemDelegate(new schedule_view_item_delegate_t(this));
  connect(selectionModel(), &QItemSelectionModel::selectionChanged, this, &schedule_view_t::slot_selectionChanged);

  connect(model, &ScheduleModel::rowsAboutToBeRemoved, this, &schedule_view_t::slot_rows_about_to_be_removed);
  resizeColumnsToContents();
  resizeRowsToContents();
  connect(model, &ScheduleModel::rowsInserted, this, &schedule_view_t::resizeColumnsToContents);
  connect(model, &ScheduleModel::rowsInserted, this, &schedule_view_t::resizeRowsToContents);
}

void schedule_view_t::slot_rows_about_to_be_removed(const QModelIndex& parent, int start, int end) {
  if (!parent.isValid()) {
    int current_row = m_document->schedule()->indexOf(m_current_call);
    if (start<=current_row && current_row<=end) {
      if ((end+2)>=model()->rowCount()) {
        select(model()->rowCount()<=2 ? 0 : 1);
      } else {
        select(end+1);
      }
    }
  }
}

void schedule_view_t::create_call(int index) {
  CallCreateDialog dialog(m_document, index, QApplication::activeWindow());
  dialog.exec();
}

schedule_view_t::~schedule_view_t() {
}

void schedule_view_t::slot_selectionChanged(QItemSelection selected, QItemSelection /*deselected*/) {
  if (!selected.isEmpty()) {
    QItemSelectionRange range = selected.front();
    QModelIndex index = range.topLeft();
    m_current_call = index.data(Qt::UserRole).value<ange::schedule::Call*>();
    emit call_selected(m_current_call);
    emit call_focused(m_current_call);
  }
}

static void disableAction(QAction* action, QString reason) {
    action->setEnabled(false);
    action->setText(action->text() + " (" + reason + ")");
}

void schedule_view_t::contextMenuEvent(QContextMenuEvent* event) {
    QModelIndex index = indexAt(event->pos());
    QMenu menu;
    QAction* append_call = 0L;
    QAction* insert_call = 0L;
    QAction* remove_call_action = 0L;
    QAction* changeOfRotationUp = 0L;
    QAction* changeOfRotationDown = 0L;
    QAction* setAsPivot = 0L;
    Call* call = index.data(Qt::UserRole).value<Call*>();
    int row = call ? m_document->schedule()->indexOf(call) : -1;
    if (call) {
        Call* call = index.data(Qt::UserRole).value<Call*>();
        if (!is_befor_or_after(call)) {
            remove_call_action = menu.addAction("Remove "+ call->uncode());
            Call* next = m_document->schedule()->next(call);
            Call* previous = m_document->schedule()->previous(call);
            if(previous && !is_befor_or_after(previous) ) {
                changeOfRotationUp = menu.addAction("Move Up");
                QString illegalityReason = m_document->stowage()->changeOfRotationIsLegal(call, -1);
                if (!illegalityReason.isNull()) {
                    disableAction(changeOfRotationUp, illegalityReason);
                } else {
                    QString macroStowageReason = m_document->macro_stowage()->callSwapIsLegal(previous, call);
                    if (!macroStowageReason.isNull()) {
                        disableAction(changeOfRotationUp, macroStowageReason);
                    }
                }
            }
            if(next && !is_befor_or_after(next)) {
                changeOfRotationDown = menu.addAction("Move Down");
                QString illegalityReason = m_document->stowage()->changeOfRotationIsLegal(call, 1);
                if (!illegalityReason.isNull()) {
                    disableAction(changeOfRotationDown, illegalityReason);
                } else {
                    QString macroStowageReason = m_document->macro_stowage()->callSwapIsLegal(call, next);
                    if (!macroStowageReason.isNull()) {
                        disableAction(changeOfRotationDown, macroStowageReason);
                    }
                }
            }
            setAsPivot = menu.addAction("Set as pivot");
        }
        if (row>0) {
            insert_call = menu.addAction("Insert new call");
        }
    }
    append_call = menu.addAction("Append new call");
    if (QAction* action = menu.exec(event->globalPos())) {
        if (append_call == action) {
            if(is_after(m_document->schedule()->calls().last())) {
                create_call(m_document->schedule()->calls().size()-1);
            } else {
                create_call(m_document->schedule()->calls().size());
            }
        } else if (insert_call == action) {
            create_call(row);
        } else if (remove_call_action == action) {
            if (m_document->schedule()->previous(call) && is_befor(m_document->schedule()->previous(call)) ) {
                FrontCallPopper(m_document, m_selected_pool).pop();
            } else {
                remove_call(call);
            }
        } else if(changeOfRotationUp == action) {
            ScheduleChangeRotationCommand* cmd = ScheduleChangeRotationCommand::changeOfRotation(m_document->schedule(), row, row-1);
            m_document->undostack()->push(cmd);
        } else if(changeOfRotationDown == action) {
            ScheduleChangeRotationCommand* cmd = ScheduleChangeRotationCommand::changeOfRotation(m_document->schedule(), row, row+1);
            m_document->undostack()->push(cmd);
        } else if(setAsPivot == action) {
            SchedulePivotPortCommand* cmd = SchedulePivotPortCommand::setPivot(m_document->schedule(), row);
            m_document->undostack()->push(cmd);
        }
    }
}

bool schedule_view_t::validate_call_can_be_removed(ange::schedule::Call* call) const {
  int loads = 0;
  int discharges = 0;
  int shiftings = 0;
  int pods = 0;
  ContainerLeg* routes = m_document->stowage()->container_routes();
  Q_FOREACH(const Container* container, m_document->containers()->list()) {
    const ange::vessel::Slot* current_position = 0L;
    if (routes->portOfDischarge(container) == call) {
      ++pods;
    }
    Q_FOREACH(const container_move_t* move, m_document->stowage()->moves(container)) {
      if (move->call()->distance(call) < 0) {
        break;
      }
      if (move->call() == call) {
        if (move->slot()) {
          if (current_position) {
            // Shift
            ++shiftings;
          } else {
            // Load
            ++loads;
          }
        } else {
          Q_ASSERT(current_position);
          ++discharges;
        }
      }
      current_position = move->slot();
    }
  }
  if (loads + discharges + shiftings+pods > 0) {
    QString message = tr("%1 cannot be removed because ").arg(call->uncode());
    QList<QString> reasons;
    if (loads>0) {
      reasons << tr("%1 containers are loaded").arg(loads);
    }
    if (discharges>0) {
      reasons << tr("%1 containers are discharged ").arg(discharges);
    }
    if (shiftings>0) {
      reasons << tr("%1 containers are shifted").arg(shiftings);
    }
    if (pods>0) {
      reasons << tr("%1 containers have PoD").arg(pods);
    }
    while (!reasons.isEmpty()) {
      message += reasons.front();
      if (reasons.size()>2) {
        message += ", ";
      } else if (reasons.size()==2) {
        message += tr(" and ");
      }
      reasons.pop_front();
    }
    message += tr(" here.");
    QMessageBox message_box;
    message_box.setWindowTitle(tr("Cannot removed %1").arg(call->uncode()));
    message_box.setText(message);
    message_box.setIcon(QMessageBox::Warning);
    message_box.exec();
    return false;
  }
  return true;
}

void schedule_view_t::remove_call(Call* callToRemove) {
    if (!validate_call_can_be_removed(callToRemove)) {
        return;
    }

    merger_command_t* clearMacro = new merger_command_t("Clear macro stowage");
    MacroPlan* macroStowage = m_document->macro_stowage();
    QList<Call*> previousCalls = m_document->schedule()->between(m_document->schedule()->at(0), callToRemove);
    Q_FOREACH(ange::vessel::BaySlice* baySlice, m_document->vessel()->baySlices()) {
        Q_FOREACH(const ange::vessel::Compartment* compartment, baySlice->compartments()) {
            // Remove allocations from callToRemove
            if (callToRemove == macroStowage->loadCall(compartment, callToRemove)) {
                const Call* discharge = macroStowage->dischargeCall(compartment, callToRemove);
                clearMacro->push(MacroCompartmentCommand::remove(m_document, compartment, callToRemove, discharge));
            }
            // Remove allocations to callToRemove
            Q_FOREACH (const Call* load, previousCalls) {
                if (callToRemove == macroStowage->dischargeCall(compartment, load)) {
                    clearMacro->push(MacroCompartmentCommand::remove(m_document, compartment, load, callToRemove));
                    break;
                }
            }
        }
    }

    ContainerLeg* routes = m_document->stowage()->container_routes();
    // Delete all containers from callToRemove. They should all be unstowed as per check initially in function
    QList<Container*> containersToRemove;
    Q_FOREACH (const Container* container, m_document->containers()->list()) {
        if (routes->portOfLoad(container) == callToRemove) {
            containersToRemove << m_document->containers()->get_container_by_id(container->id());
        }
    }
    QUndoCommand* removeLoads = ContainerDeleter::deleteContainers(m_document, m_selected_pool, containersToRemove);

    QUndoCommand* removeCall = schedule_change_command_t::remove_call(m_document, m_document->schedule()->indexOf(callToRemove));

    QUndoStack* stack = m_document->undostack();
    stack->beginMacro(tr("Remove %1").arg(callToRemove->uncode()));
    stack->push(clearMacro);
    stack->push(removeLoads);
    stack->push(removeCall);
    stack->endMacro();
}

#include "schedule_view.moc"
