#include "macro_stow_toolbar.h"

#include "callutils.h"
#include "document.h"
#include "userconfiguration.h"

#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>

#include <QAction>
#include <QActionGroup>
#include <QPainter>

using ange::schedule::Call;
using ange::schedule::Schedule;

macro_stow_toolbar_t::macro_stow_toolbar_t(QWidget* parent)
  : QToolBar("Master plan toolbar", parent),
    m_document(0L),
    m_group(0L),
    m_current_call(0L)
{
    setObjectName("MacroStowToolbar");
    setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
}

void macro_stow_toolbar_t::set_document(document_t* document) {
    if (m_document != document) {
        m_current_call = 0;
    }
  m_document = document;

  reset_actions_from_schedule();

  connect(m_document->schedule(), &Schedule::callAboutToBeRemoved, this, &macro_stow_toolbar_t::remove_call);
  connect(m_document->schedule(), &Schedule::callAdded, this, &macro_stow_toolbar_t::add_call);
  connect(m_document->userconfiguration(), &UserConfiguration::colorsChanged, this, &macro_stow_toolbar_t::reset_actions_from_schedule);
}

void macro_stow_toolbar_t::reset_actions_from_schedule() {
  const Schedule* schedule = m_document->schedule();
  // Create an group for the colored call actions, or clear the existing group
  if (m_group) {
    Q_FOREACH(QAction* action, m_group->actions()) {
      m_group->removeAction(action);
      delete action;
    }
  } else {
    m_group = new QActionGroup(this);
    m_group->setExclusive(true);
    connect(m_group, &QActionGroup::triggered, this, &macro_stow_toolbar_t::emit_call_selected);
  }

  // Add actions for all calls
  Q_FOREACH(Call* call, schedule->calls()) {
    if (is_befor_or_after(call)) {
      continue;
    }
    add_call(call);
  }
}

QAction* macro_stow_toolbar_t::create_action_for_call(const ange::schedule::Call* call) {
  QPixmap pixmap(iconSize());
  { //scoping the colormagic
    QColor call_color = m_document->userconfiguration()->get_color_by_call(call);
    QLinearGradient grad;
    grad.setColorAt(1.0,call_color.lighter(125));
    grad.setColorAt(0.5,call_color);
    grad.setColorAt(0.0,call_color.lighter(125));
    grad.setStart(pixmap.rect().topLeft());
    grad.setFinalStop(pixmap.rect().bottomRight());
    QPainter p(&pixmap);
    p.fillRect(pixmap.rect(),grad);
  }
  QIcon theicon(pixmap);
  QAction* action = new QAction(theicon, call->uncode(), this);
  action->setData(call->id());
  m_group->addAction(action);
  addAction(action);
  action->setCheckable(true);
  return action;

}

void macro_stow_toolbar_t::emit_call_selected(QAction* trigger) {
  int call_id = trigger->data().toInt();
  if (Call* call = m_document->schedule()->getCallById(call_id)) {
    emit call_selected(call);
  }
}

void macro_stow_toolbar_t::slot_mode_changed(mode_enum_t::mode_t mode) {
  switch (mode) {
    case mode_enum_t::MACRO_MODE: {
      set_macro_actions();
      show();
      break;
    }
    case mode_enum_t::MICRO_MODE: {
      hide();
      break;
    }
  }
}

void macro_stow_toolbar_t::set_macro_actions() {
  if (!m_document || !m_current_call) {
    return;
  }
  const Schedule* schedule = m_document->schedule();
  Q_FOREACH(QAction* action, m_group->actions()) {
    if (const Call* call = schedule->getCallById(action->data().toInt())) {
      action->setVisible(!is_befor_or_after(m_current_call) && m_current_call->distance(call)>0);
      action->setChecked(false);
    }
  }
}

void macro_stow_toolbar_t::set_current_call(const ange::schedule::Call* call) {
  m_current_call = call;
  set_macro_actions();
}

void macro_stow_toolbar_t::hideEvent(QHideEvent* event) {
  QToolBar::hideEvent(event);
  emit visibility_changed(false);
}

void macro_stow_toolbar_t::showEvent(QShowEvent* event) {
  QToolBar::showEvent(event);
  emit visibility_changed(true);
}

void macro_stow_toolbar_t::add_call(ange::schedule::Call* call) {
  QAction* action = create_action_for_call(call);
  if (m_current_call) {
    action->setVisible(m_current_call->distance(call)>0  && !is_befor_or_after(m_current_call));
  }
  Q_FOREACH(QAction* a, m_group->actions()) {
    const Call* a_call = m_document->schedule()->getCallById(a->data().toInt());
    if (call->distance(a_call)>0) {
      insertAction(a, action);
      break;
    }
  }
}

void macro_stow_toolbar_t::remove_call(ange::schedule::Call* call) {
  Q_FOREACH(QAction* action, m_group->actions()) {
    if (action->data().toInt() == call->id()) {
      m_group->removeAction(action);
      delete action;
      return;
    }
  }
}

#include "macro_stow_toolbar.moc"
