#include "toolmanager.h"

#include "tool.h"
#include <QAction>

ToolManager::ToolManager(QObject* parent)
  : QObject(parent),
    m_current_tool(0L),
    m_auto_tool(0L),
    m_current_mode(mode_enum_t::MICRO_MODE)
{
    setObjectName("ToolManager");
    m_tools.append(new tool_t("&Select", ":/icons/icons/select.svg", true, QPoint(3,3), mode_enum_t::MICRO_MODE, QKeySequence(), this));
    selectTool()->add_cursor_icon(Qt::ControlModifier, ":/icons/icons/select_bold.svg", QPoint(3,3));
    selectTool()->add_cursor_icon(Qt::ShiftModifier, ":/icons/icons/small-plus.svg", QPoint(3,3));
    m_tools.append(new tool_t("&Fill", ":/icons/icons/bucket-fill.svg", true, QPoint(3,3), mode_enum_t::MICRO_MODE, QKeySequence(), this));
    m_tools.append(new tool_t("&Macro", ":/icons/icons/select.svg", true, QPoint(3,3), mode_enum_t::MACRO_MODE, QKeySequence(), this));
    m_auto_tool_for_stow_mode[mode_enum_t::MACRO_MODE] = macroTool();
    m_auto_tool_for_stow_mode[mode_enum_t::MICRO_MODE] = selectTool();
    m_auto_tool = m_auto_tool_for_stow_mode[m_current_mode];
    connect(fillTool()->action(), &QAction::triggered, this, &ToolManager::setFillTool);
    connect(selectTool()->action(), &QAction::triggered, this, &ToolManager::setSelectTool);
    connect(macroTool()->action(), &QAction::triggered, this,&ToolManager::setMacroTool);
    m_tool_for_stow_mode.insert(mode_enum_t::MICRO_MODE, 0L);
    m_tool_for_stow_mode.insert(mode_enum_t::MACRO_MODE, macroTool());
    Q_FOREACH(tool_t* tool, m_tools) {
        tool->action()->setVisible(tool->mode() == m_current_mode);
    }
}

ToolManager::~ToolManager() {
}

void ToolManager::setMacroMode() {
    changeMode(mode_enum_t::MACRO_MODE);
}

void ToolManager::setMicroMode() {
    changeMode(mode_enum_t::MICRO_MODE);
}

tool_t* ToolManager::currentTool() const {
    return m_current_tool ? m_current_tool : m_auto_tool;
}

void ToolManager::setTool(tool_t* tool) {
    m_current_tool = tool;
    emit toolChanged(currentTool());
}

tool_t* ToolManager::fillTool() const {
    return m_tools[1];
}

tool_t* ToolManager::selectTool() const {
    return m_tools[0];
}

tool_t* ToolManager::macroTool() const {
    return m_tools[2];
}

void ToolManager::setFillTool() {
    setTool(fillTool());
}

void ToolManager::setSelectTool() {
    setTool(selectTool());
}

void ToolManager::setAutoTool() {
    setTool(0L);
}

void ToolManager::setMacroTool() {
    setTool(macroTool());
}

void ToolManager::setAutoToolSelect(bool on, Qt::KeyboardModifiers mods) {
    tool_t* old_tool = m_auto_tool;
    on = on || (mods & Qt::ShiftModifier);
    m_auto_tool_for_stow_mode[mode_enum_t::MICRO_MODE] = on ? selectTool() : fillTool();
    m_auto_tool = m_auto_tool_for_stow_mode[m_current_mode];
    if (!m_current_tool && old_tool != m_auto_tool) {
        emit toolChanged(m_auto_tool);
    }
}

void ToolManager::changeMode(mode_enum_t::mode_t mode) {
    if(mode != m_current_mode) {
        m_tool_for_stow_mode.insert(m_current_mode, m_current_tool);
        m_auto_tool = m_auto_tool_for_stow_mode[mode];
        Q_FOREACH(tool_t* tool, m_tools) {
            bool on = (tool->mode() == mode);
            tool->action()->setVisible(on);
        }
        m_current_mode = mode;
        setTool(m_tool_for_stow_mode.value(mode, 0L));
        emit modeChanged(mode);
    }
}





#include "toolmanager.moc"
