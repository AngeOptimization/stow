#include "importvessel.h"

#include "container_list.h"
#include "document.h"
#include "error_popup.h"
#include "multivesselreader.h"
#include "override_cursor_handler.h"
#include "passivenotifications.h"
#include "stowagereader.h"
#include "userconfiguration.h"
#include "vesselimportcommand.h"

#include <ange/containers/container.h>
#include <ange/vessel/vessel.h>

#include <QApplication>
#include <QBuffer>
#include <QFileDialog>
#include <QHeaderView>
#include <QSettings>
#include <QStandardItemModel>
#include <QStandardPaths>
#include <QStatusBar>
#include <QTableView>
#include <QToolBar>
#include <QUndoStack>

using namespace ange::units;

VesselParseResult readVessel(const QString& fileName) {
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly)) {
        throw std::runtime_error(QString("Failed to open file: %1").arg(file.errorString()).toStdString());
    }
    VesselParseResult vesselParseResult;
    vesselParseResult.m_vesselFileData = file.readAll();
    QBuffer vesselFileBuffer(&vesselParseResult.m_vesselFileData);
    vesselFileBuffer.open(QIODevice::ReadOnly);
    MultiVesselReader vesselReader;
    vesselParseResult.m_vessel = vesselReader.readVessel(&vesselFileBuffer);
    return vesselParseResult;
}

void importVessel(document_t* document, QWidget* parentWidget) {
    QStringList filters;
    filters << "Json vessel files (*.json *.json.gz)";
    filters << "Angelstow file (*.sto)";
    filters << "All Files (*)";
    QSettings settings;
    QString vesselDirectory = settings.value("vesselImportDirectory",
                                             QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation)).toString();
    QString fileName = QFileDialog::getOpenFileName(parentWidget, "Import vessel file", vesselDirectory, filters.join(";;"));
    if (!fileName.isNull()) {
        try {
            override_cursor_handler_t cursorHandler(qApp, Qt::WaitCursor);
            QScopedPointer<const ange::vessel::Vessel> vessel;
            QByteArray vesselFileData;
            if (fileName.endsWith(".sto")) {
                Q_ASSERT(false); // FIXME stowageReader.read_vessel does not work
                StowageReader stowageReader;
                vessel.reset(stowageReader.read_vessel(fileName));
                // FIXME set vesselFileData
            } else {
                VesselParseResult vesselParseResult = readVessel(fileName);
                vessel.reset(vesselParseResult.m_vessel);
                vesselFileData = vesselParseResult.m_vesselFileData;
            }
            if (!vessel) {
                throw std::runtime_error("Failed to read vessel");
            }
            const bool imoNumberChanged = (vessel->imoNumber() != document->vessel()->imoNumber());
            VesselImportCommand* cmd = new VesselImportCommand(vessel.take(), vesselFileData, document);
            document->undostack()->push(cmd);
            if (imoNumberChanged) {
                document->passiveNotifications()->addWarningWithUndo(QString("Imported vessel had a new imo number."));
            }
            QList<int> containersIdsLeftOnQuay = cmd->container_ids_left_on_quay();
            if (!containersIdsLeftOnQuay.isEmpty()) {
                // TODO this popup error should be done using our unified error reporting mechanisms
                QStandardItemModel* model = new QStandardItemModel(0, 4);
                Q_FOREACH (int id, containersIdsLeftOnQuay) {
                    QList<QStandardItem*> row;
                    if (const ange::containers::Container* container = document->containers()->get_container_by_id(id)) {
                        row << new QStandardItem(container->isoCode().code());
                        row << new QStandardItem(container->loadPort());
                        row << new QStandardItem(container->dischargePort());
                        row << new QStandardItem(QString("%1T").arg(std::floor(container->weight() / ton)));
                        model->appendRow(row);
                    }
                }
                QTableView* view = new QTableView();
                view->verticalHeader()->hide();
                view->horizontalHeader()->hide();
                view->setModel(model);
                view->setAttribute(Qt::WA_DeleteOnClose);
                document->passiveNotifications()->addWarning("Some containers could not be moved to imported vessel.",
                        "Show list", QIcon(":/icons/icons/system-search.svg"), view, SLOT(show()));
            }
            QFileInfo fileInfo(fileName);
            settings.setValue("vesselImportDirectory", fileInfo.absoluteDir().absolutePath());
        } catch (std::runtime_error& error) {
            error_popup(parentWidget, QObject::tr("Import vessel failed"),
                        QObject::tr("Open vessel file '%1' failed, see details below").arg(fileName),
                        error);
        }
    }
}
