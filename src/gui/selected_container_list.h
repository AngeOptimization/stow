/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010

 Copyright: See COPYING file that comes with this distribution

*/

#ifndef SELECTED_CONTAINER_LIST_H
#define SELECTED_CONTAINER_LIST_H

#include <QSortFilterProxyModel>
#include <QSet>

class QTimer;
class pool_t;
class container_list_t;

/**
 * List that follows which containers are currently selected (in the master list)
 */
class selected_container_list_t : public QSortFilterProxyModel {
  Q_OBJECT
  public:
    /**
     * Construct.
     */
    selected_container_list_t(container_list_t* containerList, pool_t* pool);

    /**
     * @override
     * sets the container_list source
     */
    virtual void setSourceModel(container_list_t* container_list);

    /**
     * @return selected pool this list is based on
     */
    pool_t* pool() const {
      return m_selected_pool;
    }

  public Q_SLOTS:
    void resort_and_filter_later();
  Q_SIGNALS:
    /**
     * Emits a string for display totals for the selection
     */
    void total_display_string(const QString& display_string);
  protected:
    /**
     * @override. Filters out all but the selected
     */
    virtual bool filterAcceptsRow(int source_row, const QModelIndex& source_parent) const;

    virtual bool lessThan(const QModelIndex& left, const QModelIndex& right) const;

  private Q_SLOTS:
    void resort_and_filter();
  private:
    container_list_t* m_containerList;
    pool_t* m_selected_pool;
    QTimer* m_update_delay_timer;
};

#endif // SELECTED_CONTAINER_LIST_H
