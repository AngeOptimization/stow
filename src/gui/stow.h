#ifndef VESSEL_STOW_H
#define VESSEL_STOW_H
namespace Stow {
        /**
          * enum to help qgraphicsitem_cast in our own items. Mostly used to avoid number collisions.
          * reimplement a int type() function that returns UserType + Stow::yourclassname
          * and take a number in this enum
          */
  enum Items {
    slot_item_t=1,
    bay_slice_item_t=2,
    container_item_t=3,
    stack_item_t=4,
    compartment_item_t=5,
    upper_limit_item_t=6
    ,TankVesselGraphicsItem=7
    ,TankGraphicsItem=8
  };
}
#endif // VESSEL_STOW_H