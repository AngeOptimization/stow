#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include "mode.h"
#include "passivenotifications.h"
#include "recapmanagerinterface.h"
#include "selectionsummarylabel.h"

#include <QItemSelectionModel>
#include <QMainWindow>
#include <QMenu>
#include <QPointer>
#include <QSignalMapper>
#include <QTimer>

class ContainerSelectionBundleHelper;
class LayoutController;
class PluginManager;
class QAbstractButton;
class QSortFilterProxyModel;
class QToolBar;
class RecapView;
class RestowEditor;
class SettingsDialog;
class SortFilterProblemsByCallProxyModel;
class TankGroupSelectorSynchronizer;
class ToolManager;
class Ui_MainWindow;
class document_t;
class main_statistics_widget_t;
class pool_t;
class stability_widget_t;
class tool_t;
class vessel_graphics_view_t;
class vessel_scene_t;
namespace ange {
namespace containers {
class Container;
}
namespace schedule {
class Call;
}
namespace vessel {
class BaySlice;
class Compartment;
class Stack;
}
}
namespace qdatacube {
class datacube_model_t;
}

class MainWindow : public QMainWindow, public RecapManagerInterface {
    Q_OBJECT
public:
    /**
     * Takes ownership of initial_document
     */
    MainWindow(PluginManager* pluginManager, document_t* initial_document, QWidget* parent = 0);

    virtual ~MainWindow();

    document_t* document() const {
      return m_document;
    }

    /**
     * Add a new recap with name
     */
    RecapView* add_recap(QString name, QString username);

    /**
     * List of current recaps
     */
    QList<RecapView*> recapViews();

    /**
    * @return the current call selected on the interface
    */
    const ange::schedule::Call* current_call() const;

    /**
     * @return selection pool
     */
    pool_t* selection_pool() const {
      return m_selected_pool;
    }

public Q_SLOTS:
    /**
     * Load stowage from file and replace the document
     */
    bool stowage_open(const QString& file);

    /**
     * Add a new recap
     */
    RecapView* add_recap();

    /**
     * A wizard to help import containers from EDIFACT file to curent document
     */
    void import_edifact_wizard();

    /**
     * Pops up a modal about dialog. Make it non-modal?
     */
    void openAboutDialog();

    /**
     * Show QDockWidget stored in QAction senders data
     */
    void showDockWidget();

    void updateShowPanelMenu();

private:
    void set_document(document_t* doc);
    Ui_MainWindow* m_ui;
    document_t* m_document;
    vessel_scene_t* m_bay_plan;
    QList<vessel_graphics_view_t*> m_extra_vessel_views;
    main_statistics_widget_t* m_main_stats;
    stability_widget_t* m_stability_widget;
    pool_t* m_selected_pool;
    bool m_auto_tool_mode;
    QList<RecapView*> m_recap_views;

    void update_recent_opened(const QString& filenamename = QString());
    QMenu* m_recent_menu;
    QSignalMapper* m_recent_mapper;
    QStringList m_recent_files;

    QPointer<SettingsDialog> m_settings_dialog;
    SelectionSummaryLabel m_selection_summary_label;

    virtual void keyPressEvent(QKeyEvent* key);
    virtual void keyReleaseEvent(QKeyEvent* key);

    /**
     * ensure exports is sane or alert the user
     */
    bool sanity_in_export_formats();

    /**
     * saves settings on exit. called by ...
     */
    void save_settings();

    /**
     * restores settings on load. called by ctor.
     */
    void load_settings();

    QTimer m_autosave_timer;
    ContainerSelectionBundleHelper* m_bundlehelper;

    /**
     * really opens the stowage. No safeguards, no 'are you sure to throw data away'-warning. no parachute. no lifejacket
     * Called among others by stowage_open(QString file) after some sanity checks. Use with care.
     */
    bool really_stowage_open(QString file);

    bool ask_recover_autosave(const QString& filename);

    /**
     * Add developer menu to menu bar
     */
    void addDeveloperMenu();

    QSortFilterProxyModel* m_selectionListSortModel;
    SortFilterProblemsByCallProxyModel* m_problemSortFilterProxyModel;
    ToolManager* m_toolManager;

  private Q_SLOTS:
    void change_current_call(const ange::schedule::Call* call);
    void row_label_changed();
    void show_equipment_id_changed(bool state);
    void add_extra_vesselview(QRectF rect = QRectF());
    void slot_run_stowage_plugin();

    /**
     * Sets window title from document
     */
    void set_window_title();

    void remove_recap(QObject* recap);
    void removeVesselView(QObject* recap);

    virtual void closeEvent(QCloseEvent* event);

    /**
     * Import a vessel to cache
     */
    void import_vessel();

    /**
     * shows the report generation including printing and print previews.
     */
    void showGenerateReportsDialog();

    /**
     * Open new stowage (that is, everything) from file. User will be prompted for filename
     */
    void stowage_open();

    /**
     * Save stowage (that is, everything) to file, using current file name, or else user will be prompted for filename
     */
    bool stowage_save();

    /**
     * Save stowage (that is, everything) to file. User will be prompted for filename
     */
    bool stowage_save_as();

    /**
     * Generate containers from user input (e.g, give me 20 30t reefers)
     */
    void generate_containerlist();

    /**
     * Checks whether the user wants to open a new file without saving changes
     * in the current one.
     **/
    bool really_load();

    /**
     *  selects next call
     */
    void select_next_call();

    /**
     *  selects previous call
    */
    void select_previous_call();

    /**
     * disable autosave
     */
    void update_autosave(int index);

    /**
     * starts autosave
     */
    void autosave();

    /**
     * messages autosave is done
     */
    void autosave_finished();

    /**
     * cleans up autosave file
     */
    void cleanup_autosave(QString autosavepath=QString());
    void check_default_autosave();

    /**
     * Shows stability report. Slot used for menu action.
     */
     void showStabilityReport();

     /**
      * Slot to help work around issues around keyboardModifier keys.
      * See also #1363
      */
    void forwardToAutoToolHandling(bool selectionempty);

    /**
     * Set the property "debug.LashingPlugin.forceClassificationSociety" on documentInterface()
     */
    void forceClassificationSociety();

public Q_SLOTS:
    /**
     * Delete all selected containers from stowage
     */
    void delete_selected_containers();

    /**
     * Display the general settings, or raise if already displayed
     */
    void display_settings();

    /**
     * Display a passive message (top of main window), perhaps with a button to
     * activate some action and another dismiss.
     * Dismiss when the undo stack has changed
     */
    void showWarning(const PassiveNotification& warning);

    void writeDebugLine(bool);
    void debugDumpMacroStowage();
    void debugDumpToolManagerState();
    void crash(bool);
    void autosave_failed();
    void exportSettings();
    void exportMasterPlanningDbgInfo();

private: // Methods
    // Used in developer menu
    QAction* addForceClassificationSocietyAction(QMenu* forceClassificationSocietyMenu, const QString& society);

private: // Fields
    PluginManager* m_pluginManager;
    TankGroupSelectorSynchronizer* m_tankGroupSynchroniser;
    RestowEditor* m_restowEditor;
    LayoutController* m_layoutController;
    bool m_inDestructor;

};

#endif // MAIN_WINDOW_H
