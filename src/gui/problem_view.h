#ifndef PROBLEM_VIEW_H
#define PROBLEM_VIEW_H

#include <QTreeView>
#include <QTimer>

namespace ange {
    namespace angelstow {
        class Problem;
    } // namespace angelstow
} // namespace ange
/**
 * Very small extension to QTreeView, handing column resizing dynamically
 */
class problem_view_t : public QTreeView {
  Q_OBJECT
  public:
    explicit problem_view_t(QWidget* parent = 0);
    virtual void setModel(QAbstractItemModel* model);
    Q_SIGNALS:
        void zoomToProblem(const ange::angelstow::Problem* problem);
  public Q_SLOTS:
    void resize_all_columns_to_contents();
  private Q_SLOTS:
    void really_resize_all_columsn_to_contents();
    void contextMenuRequested(const QPoint& pos);
    void onDoubleClick(QModelIndex index);
  private:
    static bool canZoomToProblem(const ange::angelstow::Problem* problem);
    static const ange::angelstow::Problem* problemForModelIndex(const QModelIndex& idx);
    QTimer m_delay_resize_timer;
};

#endif // PROBLEM_VIEW_H
