#include "tool.h"

#include <QIcon>
#include <QCursor>
#include <QImageReader>
#include <QAction>
#include <QPainter>

tool_t::tool_t(QString name,
               QString icon_resource,
               bool area_tool,
               QPoint hot_spot,
               mode_enum_t::mode_t mode,
               const QKeySequence& shortcut,
               QObject* parent)
  : QObject(parent),
    m_name(name),
    m_area_tool(area_tool),
    m_mode(mode),
    m_action(new QAction(icon_resource.isEmpty() ? QIcon() : QIcon(icon_resource), name, this))
{
    setObjectName("Tool");
  m_action->setShortcut(shortcut);
  add_cursor_icon(Qt::NoModifier, icon_resource, hot_spot);
  m_action->setCheckable(true);
}

tool_t::~tool_t()
{
    qDeleteAll(m_cursors);
}

const QCursor tool_t::cursor(Qt::KeyboardModifiers modifiers) const {
    const QCursor* main_cursor = m_cursors.value(0);
    QPixmap synthezised_pixmap = main_cursor->pixmap();
    if(modifiers & Qt::ControlModifier && m_cursors.contains(Qt::ControlModifier)) {
        //no need for a QPainter as ControlModifier currently fully "overwrites" the standard cursor
        synthezised_pixmap = m_cursors.value(Qt::ControlModifier)->pixmap();
    }
    if(modifiers & Qt::ShiftModifier)
        if (m_cursors.contains(Qt::ShiftModifier)) {
        QPainter painter(&synthezised_pixmap);
        painter.setBackgroundMode(Qt::TransparentMode);
        painter.drawPixmap(QRectF(0,0,32,32), m_cursors.value(Qt::ShiftModifier)->pixmap(), QRectF(0,0,32,32));
    }
    return QCursor(synthezised_pixmap, main_cursor->hotSpot().x(), main_cursor->hotSpot().y());
}

void tool_t::add_cursor_icon(const int modifier_key, const QString icon_resource, const QPoint& hot_spot)
{
  if (!icon_resource.isEmpty()) {
    QImageReader reader(icon_resource);
    reader.setScaledSize(QSize(32,32)); // Magic size, stole from KDE oxygen
    m_cursors[modifier_key] = new QCursor(QPixmap::fromImage(reader.read()), hot_spot.x(), hot_spot.y());
  } else {
    m_cursors[modifier_key]= new QCursor();
  }
}

QDebug operator<<(QDebug dbg, const tool_t* tool) {
  return dbg << (tool ? tool->name() : QString::fromLatin1("<null tool>"));
}
QDebug operator<<(QDebug dbg, const tool_t& tool) {
  return dbg << tool.name();
}

#include "tool.moc"
