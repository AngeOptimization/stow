#ifndef STATERESTORER_H
#define STATERESTORER_H

#include <QByteArray>
#include <QObject>

class QMainWindow;

/**
 * Hack for working around multiple problems when restoring a main window, the workaround works by
 * doing the restore a little later.
 */
class DelayedRestorer : public QObject {
    Q_OBJECT

public:

    /**
     * Restore @param geometry and @param state to @param mainWindow using QTimer::singleShot
     */
    static void timedRestore(QMainWindow* mainWindow, const QByteArray& geometry, const QByteArray& state);

public Q_SLOTS:

    /**
     * Restore geometry and call restore2 with a timer
     */
    void restore1();

    /**
     * Restore state and delete this object
     */
    void restore2();

private:
    DelayedRestorer(QMainWindow* mainWindow, const QByteArray& geometry, const QByteArray& state);

private:
    QMainWindow* m_mainWindow;
    QByteArray m_geometry;
    QByteArray m_state;

};

#endif // STATERESTORER_H
