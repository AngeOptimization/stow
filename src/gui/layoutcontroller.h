#ifndef LAYOUTCONTROLLER_H
#define LAYOUTCONTROLLER_H

#include <QObject>
#include <QSharedPointer>
#include <QStringList>

class MainWindow;
class QMenu;
class QSettings;
class QString;
class Ui_MainWindow;

/**
 * Class for controlling the layout of the dock widget in a QMainWindow
 */
class LayoutController : public QObject {

    Q_OBJECT

public:

    explicit LayoutController(MainWindow* mainWindow, Ui_MainWindow* ui);
    ~LayoutController();

    /**
     * Save state of main window under @param name
     */
    void saveState(const QString& name);

    /**
     * Restore saved state to main window from @param name
     * @returns true if the layout of the given name existed
     */
    bool restoreState(const QString& name);

    /**
     * Restore built-in state to main window from @param name
     * @returns true if the layout of the given name existed
     */
    bool restoreBuiltinState(const QString& name);

public Q_SLOTS:

    /**
     * Pop up a dialog box for a state name and save the state
     */
    void saveLayout();

    /**
     * Restore layout found by looking into text and data of the QAction that sent the signal
     */
    void restoreLayout();

    /**
     * Delete layout found by looking into text and data of the QAction that sent the signal
     */
    void deleteLayout();

private:

    bool restoreState(const QString& name, QSettings* settings);
    QStringList layoutNames(QSettings* settings);
    void updateRestoreMenu(QSettings* settings, QMenu* menu);
    void updateDeleteMenu(QSettings* settings, QMenu* menu);

private:

    MainWindow* m_mainWindow;
    Ui_MainWindow* m_ui;
    QScopedPointer<QSettings> m_builtinLayouts;
    QScopedPointer<QSettings> m_savedLayouts;

};

#endif // LAYOUTCONTROLLER_H
