#ifndef TANKTABLEWIDGET_H
#define TANKTABLEWIDGET_H

#include <QWidget>
#include <QPointer>

class TankItemSelectionModel;
class SelectedTankContentEditField;
class QItemSelectionModel;
class TankGroupFilterProxyModel;
class QTableView;
class QComboBox;
class QAbstractItemModel;
class TankGroupSelectorSynchronizer;
class document_t;
namespace ange {
    namespace vessel {
        class Vessel;
    } // namespace vessel
    namespace schedule {
        class Call;
    }
} // namespace ange

/**
 * Widget to show the tank table and related interactions for filling and tank group selection
 */
class TankTableWidget : public QWidget {
    Q_OBJECT
    public:
        TankTableWidget(QWidget* parent = 0,  Qt::WindowFlags f = 0);
        /**
         * Sets the tankgroupselectorsynchronizer to \param synchronizer
         */
        void setTankGroupSelectorSynchronizer(TankGroupSelectorSynchronizer* synchronizer);
        /**
         * sets the document \param document
         */
        void setDocument(document_t* document);
        /**
         * sets the selection model to \param selectionModel
         */
        void setSelectionModel(TankItemSelectionModel* selectionModel);
        /**
         * We need it to look a little bit like a table view
         */
        QAbstractItemModel* model();
        /**
         * Sets the 'bottom model' for this
         */
        void setModel(QAbstractItemModel* model);

        /**
         * The model set at the view, it is likely a TankGroupFilterProxyModel, but can be another model.
         */
        QAbstractItemModel* proxyModel();

        /**
         * Sets the current call to \param call
         */
        void setCurrentCall(const ange::schedule::Call* call);
    private Q_SLOTS:
        /**
         * Updates relevant information when document changes vessel
         */
        void setVessel(const ange::vessel::Vessel* vessel);
    private:
        QPointer<TankGroupSelectorSynchronizer> m_tankgroupselectorsynchronizer;
        QComboBox* m_tankGroups;
        document_t* m_document;
        QTableView* m_tankTableView;
        TankGroupFilterProxyModel* m_groupProxy;
        SelectedTankContentEditField* m_contentedit;
};

#endif // TANKTABLEWIDGET_H

