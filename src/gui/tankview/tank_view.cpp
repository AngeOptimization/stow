#include "tank_view.h"
#include "tankgraphicsitem.h"
#include "tankvesselgraphicsitem.h"
#include <ange/vessel/vesseltank.h>
#include <document/document.h>
#include <ange/vessel/vessel.h>
#include <QSignalMapper>
#include <ange/schedule/call.h>
#include <document/utils/callutils.h>
#include <QButtonGroup>
#include <QRadioButton>
#include <QGraphicsProxyWidget>
#include <QGraphicsLinearLayout>

using ange::vessel::VesselTank;

bool no_overlaps_of_same_type(TankGraphicsItem* item) {
  Q_FOREACH(QGraphicsItem* overlaps, item->collidingItems()) {
    if(TankGraphicsItem* tank_item = qgraphicsitem_cast<TankGraphicsItem*>(overlaps)) {
      if(tank_item->tank()->tankGroup()==item->tank()->tankGroup()) {
        if(tank_item->call() == item->call()) {
          return false;
        }
      }
    }
  }
  return true;
}

tank_view_t::tank_view_t(QWidget* parent): AbstractMultiCallView(parent) {

}

struct TankStartGreaterThan {
    bool operator()(const VesselTank* tank1, const VesselTank* tank2) {
        return tank1->aftEnd() > tank2->aftEnd();
    }
};

struct TankMaxTcdGreaterThan {
    bool operator()(const VesselTank* tank1, const VesselTank* tank2) {
        return tank1->tcg(tank1->capacity() * ange::units::kilogram) > tank2->tcg(tank2->capacity() * ange::units::kilogram);
    }
};

struct TankAbsMaxTcdLessThan {
    bool operator()(const VesselTank* tank1, const VesselTank* tank2) {
        return qAbs(tank1->tcg(tank1->capacity() * ange::units::kilogram)) < qAbs(tank2->tcg(tank2->capacity() * ange::units::kilogram));
    }
};

AbstractVesselGraphicsItem* tank_view_t::create_item_from_call(ange::schedule::Call* call) {
  if(is_after(call)) {
    return 0L;
  }
  bool buildCache = m_tankPointCache.isEmpty();
  TankVesselGraphicsItem* tankvessel = new TankVesselGraphicsItem(m_document,call);
  QHash<VesselTank*, TankGraphicsItem*> tankTankItemMap;
  m_scene->addItem(tankvessel);
    Q_FOREACH(QList<VesselTank*> tanks, m_sortedTankGroupsForLayout.values()) {
        Q_FOREACH(VesselTank* tank, tanks) {
            TankGraphicsItem* item = new TankGraphicsItem(call,m_document,tank,tankvessel);
            item->setObjectName(call->uncode() + " " + tank->description());
            connect(this,SIGNAL(currentTankGroupChanged(QString)),item,SLOT(show_tank_group(QString)));
            if(buildCache) {
                const qreal maxTcg = tank->tcg(tank->capacity()*ange::units::kilogram);
                qreal ypos = maxTcg;
                ypos -=item->boundingRect().height()*0.5;
                qreal adjustFactor = maxTcg < 0 ? -1 : 1;
                //non-centered tanks shouldn't cross the center line or they risk overlapping:
                const bool tankIsCentered = qFuzzyIsNull(maxTcg);
                if (!tankIsCentered) {
                    if (qAbs(maxTcg) < item->boundingRect().height() * 0.5) {
                        static double minDistanceFromCenterLine = 5.0;
                        ypos += adjustFactor * (qAbs(qAbs(maxTcg)
                                                    - item->boundingRect().height() * 0.5) + minDistanceFromCenterLine);
                    }
                }
                item->setPos(tank->aftEnd(),ypos);
                while(!no_overlaps_of_same_type(item)) {
                    ypos += adjustFactor * 10;
                    item->setPos(tank->aftEnd(), ypos);
                }
                m_tankPointCache.insert(tank,item->pos());
            } else {
                QPointF pos = m_tankPointCache.value(tank);
                item->setPos(pos);
            }
            tankTankItemMap.insert(tank,item);
        }
    }
    Q_FOREACH(QList<VesselTank*> tanks, m_sortedTankGroupsForSelection.values()) {
        TankGraphicsItem* first_item = 0;
        TankGraphicsItem* previous_item = 0;
        Q_FOREACH(VesselTank* tank, tanks) {
            TankGraphicsItem* item = tankTankItemMap.value(tank);
            if(first_item == 0) {
                first_item = item;
                previous_item = item;
            } else {
                previous_item->setSelectNext(item);
                previous_item = item;
            }
        }
        if(previous_item != first_item) {
            previous_item->setSelectNext(first_item);
        }
    }
    return tankvessel;
}

void tank_view_t::set_document(document_t* document) {
    m_sortedTankGroupsForLayout.clear();
    m_sortedTankGroupsForSelection.clear();
    m_tankPointCache.clear();

    /*
     * Tanks needs to be grouped in two different ways. One is for layouting where they need to be layouted from middle and out.
     * The other is for with the keyboard to switch to/from next tank that needs to be 'top down' 'left to right'
     * In both cases, it need to be grouped based on tank groups.
     */
  Q_FOREACH(VesselTank* tank, document->vessel()->tanks()) {
    m_sortedTankGroupsForSelection[tank->tankGroup()] << tank;
  }
  Q_FOREACH(const QString& vesselTankGroup, m_sortedTankGroupsForSelection.keys()) {
    QList<VesselTank* > tanksForSelection = m_sortedTankGroupsForSelection.value(vesselTankGroup);
    qSort(tanksForSelection.begin(), tanksForSelection.end(), TankStartGreaterThan()) ;
    QList<QList<VesselTank*> > tabgroupedTanks;
    int i = -10000;
    QList<VesselTank*> currentList;
    /*
     * Tanks get grouped based on 'less than 2 meter from other tank'
     * to ensure that tanks that looks like they are together stay together
     */
    Q_FOREACH(VesselTank* tank, tanksForSelection) {
        if(i - 2 > qAbs(m_document->vessel()->aftOverAll()/ange::units::meter-tank->aftEnd())) {
            if(!currentList.isEmpty()) {
                tabgroupedTanks << currentList;
                currentList.clear();
            }
        }
        i = qAbs(m_document->vessel()->aftOverAll()/ange::units::meter-tank->aftEnd());
        currentList << tank;
    }
    tabgroupedTanks << currentList;
    tanksForSelection.clear();
    QList<VesselTank*> tanksForLayout;
    /*
     * The groups here get sorted and then glued together
     * in order to be in the right order for either switching or layouting
     */
    Q_FOREACH(QList<VesselTank*> list, tabgroupedTanks) {
        qSort(list.begin(), list.end(), TankMaxTcdGreaterThan());
        tanksForSelection << list;
        qSort(list.begin(), list.end(), TankAbsMaxTcdLessThan());
        tanksForLayout << list;
    }
    m_sortedTankGroupsForSelection[vesselTankGroup] = tanksForSelection;
    m_sortedTankGroupsForLayout[vesselTankGroup] = tanksForLayout;
  }

  AbstractMultiCallView::set_document(document);
  connect(m_scene, &QGraphicsScene::selectionChanged, this, &tank_view_t::emitSelectedTanks);
}

void tank_view_t::setCurrentTankGroup(QString tankGroup) {
    emit currentTankGroupChanged(tankGroup);
}

void tank_view_t::emitSelectedTanks() {
    QList<const VesselTank*> tanks;

    Q_FOREACH(QGraphicsItem* item, m_scene->selectedItems()) {
        if(TankGraphicsItem* tankitem = qgraphicsitem_cast<TankGraphicsItem*>(item)) {
            tanks << tankitem->tank();
        }
    }
    emit tanksSelected(tanks);
}

void tank_view_t::selectTanks(QList< const VesselTank* > tanks) {
    Q_FOREACH(QGraphicsItem* item, m_scene->items()) {
        if(TankGraphicsItem* tankitem = qgraphicsitem_cast<TankGraphicsItem*>(item)) {
            if(tanks.contains(tankitem->tank()) && tankitem->call() == current_call()) {
                tankitem->setSelected(true);
                tankitem->update();
            } else {
                if(tankitem->isSelected()) {
                    tankitem->setSelected(false);
                }
            }
        }
    }
}

#include "tank_view.moc"
