#ifndef TANK_VIEW_H
#define TANK_VIEW_H

#include "../common/abstractmulticallview.h"

namespace ange {
namespace vessel {
class Vessel;
class VesselTank;
}
}

class document_t;

class tank_view_t : public AbstractMultiCallView {
  Q_OBJECT
  public:
    tank_view_t (QWidget* parent=0);
    virtual void set_document(document_t* document);
  protected:
    virtual AbstractVesselGraphicsItem* create_item_from_call(ange::schedule::Call* call);
  private:
    QHash<ange::vessel::VesselTank*, QPointF> m_tankPointCache;
    QHash<QString, QList<ange::vessel::VesselTank*> > m_sortedTankGroupsForSelection;
    QHash<QString, QList<ange::vessel::VesselTank*> > m_sortedTankGroupsForLayout;
    public Q_SLOTS:
        void setCurrentTankGroup(QString tankGroup);
        void selectTanks(QList<const ange::vessel::VesselTank*> tanks);
    Q_SIGNALS:
        void currentTankGroupChanged(QString tankGroup);
        void tanksSelected(QList<const ange::vessel::VesselTank*> tanks);
    private Q_SLOTS:
        void emitSelectedTanks();
};

#endif // TANK_VIEW_H
