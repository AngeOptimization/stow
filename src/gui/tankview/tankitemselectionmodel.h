#ifndef TANKITEMSELECTIONMODEL_H
#define TANKITEMSELECTIONMODEL_H

#include <QItemSelectionModel>
namespace ange {
    namespace vessel {
        class VesselTank;
    } // namespace vessel
} // namespace ange

#include <QList>

/**
 * ItemSelectionModel to help synchronizing selections between the various views upon the tanks
 */
class TankItemSelectionModel : public QItemSelectionModel {
    Q_OBJECT
    public:
        ~TankItemSelectionModel();
        TankItemSelectionModel(QAbstractItemModel* model);
        TankItemSelectionModel(QAbstractItemModel* model, QObject* parent);
        QList<const ange::vessel::VesselTank*> selectedTanks() const;
    Q_SIGNALS:
        /**
         * Emitted whenever the selection changes from the 'gui'.
         *
         * Programattically changing the selection with \ref selectTanks
         * does not cause this to be emitted
         *
         * \param tanks newly selected tanks
         */
        void selectionChanged(QList<const ange::vessel::VesselTank*> tanks);
    public Q_SLOTS:
        /**
         * sets the selection to \param tanks
         * \param tanks to select
         */
        void selectTanks(QList<const ange::vessel::VesselTank*> tanks);
    private:
        /**
         * Helper variable to ensure that the changes done by selectTanks doesn't emit selectionChanged
         */
        bool m_lock;
    private Q_SLOTS:
        void changeSelection(QItemSelection,QItemSelection);
};

#endif // TANKITEMSELECTIONMODEL_H
