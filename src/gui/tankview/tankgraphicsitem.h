#ifndef TANK_ITEM_H
#define TANK_ITEM_H

#include <QGraphicsObject>
#include "../stow.h"
#include <ange/units/units.h>
#include <QTimer>
#include <QEvent>

class QFocusEvent;
class TextItem;
class GraphicalTankRepresentation;
namespace ange {
namespace vessel {
class VesselTank;
}
namespace schedule {
  class Call;
}
}
class document_t;

/**
 * A class for representing a tank graphically with text describing
 * its name and content
 */
class TankGraphicsItem : public QGraphicsObject {
  Q_OBJECT
  public:
    TankGraphicsItem(const ange::schedule::Call* call, document_t* document, ange::vessel::VesselTank* tank, QGraphicsItem* parentitem);
    virtual void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget = 0);
    virtual QRectF boundingRect() const;
    enum {
      Type = UserType + Stow::TankGraphicsItem
    };
    virtual int type() const {
      return Type;
    }
    const ange::vessel::VesselTank* tank() {
      return m_tank;
    }
    const ange::schedule::Call* call() {
      return m_call;
    }
    /**
     * Sets the next in selection order to \param tankItem (and sets itself as previous for \param tankItem)
     */
    void setSelectNext(TankGraphicsItem* tankItem);
  public Q_SLOTS:
    void show_tank_group(const QString& group);
  private Q_SLOTS:
    void change_tank_condition(const ange::schedule::Call* call, const ange::vessel::VesselTank* tank, const ange::units::Mass);
    protected:
        virtual void keyReleaseEvent(QKeyEvent* event);
        virtual void keyPressEvent(QKeyEvent* event);
        virtual void focusOutEvent(QFocusEvent* event);
        virtual bool sceneEvent(QEvent* event);
  private:
      friend class GraphicalTankRepresentation;
        /**
         * accepts and stores editing of content thru the keyboard
         */
        void acceptEditString();
        /**
         * Rejects current input string and gets the old one back
         */
        void rejectEditString();
        /**
         * @return a string describing the condition of the tank
         */
        QString conditionString() const;
    document_t* m_document;
    const ange::schedule::Call* m_call;
    ange::vessel::VesselTank* m_tank;
    QPointF m_click_point; /* in scene coords*/
    TextItem* m_nameItem;
    TextItem* m_contentItem;
    GraphicalTankRepresentation* m_graphicTankItem;
    bool m_isEditing;
    QString m_editString;
    TankGraphicsItem* m_selectNext;
    TankGraphicsItem* m_selectPrev;
    QChar m_decimal_point;
};

#endif // TANK_ITEM_H
