#include "tankgroupfilterproxymodel.h"
#include <QModelIndex>

#include <document/tanks/tankmodel.h>

TankGroupFilterProxyModel::TankGroupFilterProxyModel(QObject* parent) : QSortFilterProxyModel(parent) {
    setFilterKeyColumn(TankModel::TankGroup);
}

void TankGroupFilterProxyModel::setTankGroup(const QString& newGroup) {
    setFilterFixedString(newGroup);
}

#include "tankgroupfilterproxymodel.moc"
