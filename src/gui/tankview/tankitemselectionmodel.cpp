#include "tankitemselectionmodel.h"
#include <document/tanks/tankmodel.h>
#include <ange/vessel/vesseltank.h>
#include <QAbstractProxyModel>

TankItemSelectionModel::~TankItemSelectionModel() {
}

TankItemSelectionModel::TankItemSelectionModel(QAbstractItemModel* model) : QItemSelectionModel(model), m_lock(false) {
    connect(this,&QItemSelectionModel::selectionChanged, this, &TankItemSelectionModel::changeSelection);
}

TankItemSelectionModel::TankItemSelectionModel(QAbstractItemModel* model, QObject* parent) : QItemSelectionModel(model,parent), m_lock(false) {
    connect(this,&QItemSelectionModel::selectionChanged, this, &TankItemSelectionModel::changeSelection);
}

void TankItemSelectionModel::selectTanks(QList<const ange::vessel::VesselTank* > tanks) {
    QList<const QAbstractProxyModel*> models;


    const QAbstractItemModel* m = model();
    if(const QAbstractProxyModel* proxy = qobject_cast<const QAbstractProxyModel*>(m)) {
        models << proxy;
        m=proxy->sourceModel();
    }

    const TankModel* tankmodel = qobject_cast<const TankModel*>(m);
    Q_ASSERT(tankmodel);

    QList<ange::vessel::VesselTank*> allTanks = tankmodel->vesselTanks();

    QList<QModelIndex> tankIndices;
    Q_FOREACH(const ange::vessel::VesselTank* tank, tanks) {
        int row = allTanks.indexOf(const_cast<ange::vessel::VesselTank*>(tank));
        tankIndices << tankmodel->index(row,0);
    }

    while(!models.isEmpty()) {
        const QAbstractProxyModel* proxy = models.takeLast();
        QList<QModelIndex> mapped;
        Q_FOREACH(const QModelIndex& idx, tankIndices) {
            Q_ASSERT(idx.model() == proxy->sourceModel());
            mapped << proxy->mapFromSource(idx);
        }
        tankIndices = mapped;
    }

#ifndef QT_NO_DEBUG
    Q_FOREACH(const QModelIndex& idx, tankIndices) {
        Q_ASSERT(idx.model() == model());
    }
#endif // QT_NO_DEBUG

    m_lock = true;
    clear();
    Q_FOREACH(const QModelIndex& idx, tankIndices) {
        select(idx, QItemSelectionModel::Rows| QItemSelectionModel::Select);
    }
    m_lock = false;
    Q_ASSERT(selectedRows().size() == tanks.size());
}

void TankItemSelectionModel::changeSelection(QItemSelection, QItemSelection) {
    if(m_lock) {
        return;
    }
    emit selectionChanged(selectedTanks());
}

QList< const ange::vessel::VesselTank* > TankItemSelectionModel::selectedTanks() const {
    QList<const ange::vessel::VesselTank*> tanks;
    QSet<int> rows;
    Q_FOREACH(QModelIndex idx, selectedIndexes()) {
        if(rows.contains(idx.row())) {
            continue;
        }
        Q_ASSERT(idx.data(Qt::UserRole).canConvert<const ange::vessel::VesselTank*>());
        const ange::vessel::VesselTank* tank = idx.data(Qt::UserRole).value<const ange::vessel::VesselTank*>();
        Q_ASSERT(tank);
        tanks << tank;
        rows << idx.row();
    }
    return tanks;
}




#include "tankitemselectionmodel.moc"
