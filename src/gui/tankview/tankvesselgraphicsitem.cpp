#include "tankvesselgraphicsitem.h"
#include <document/document.h>
#include <document/stowage/stowage.h>
#include <document/stowage/stability/stresses.h>
#include <QPainter>
#include <ange/vessel/vessel.h>
#include <QGraphicsSceneMouseEvent>
#include <ange/schedule/call.h>
#include <QApplication>

TankVesselGraphicsItem::TankVesselGraphicsItem(document_t* document, const ange::schedule::Call* call, QGraphicsItem* parent)
 : AbstractVesselGraphicsItem(document->vessel(),call,0.0,parent), m_document(document)
{
    setScale(2.0); // pixel/meter
    connect(document->stresses_bending(),&Stresses::stresses_changed,this, &TankVesselGraphicsItem::lcgChangedAtCall);
}

double TankVesselGraphicsItem::max_value() const {
    return 1.0;
}

void TankVesselGraphicsItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* , QWidget* ) {
    drawBackground(painter);
    QRectF bound = boundingRect();
    painter->save();
    QPen pen;
    pen.setColor(Qt::darkGray);
    pen.setStyle(Qt::DotLine);

    painter->setPen(pen);
    if (childItems().isEmpty()) {
        QTextOption text_option;
        text_option.setAlignment(Qt::AlignCenter);
        text_option.setWrapMode(QTextOption::WordWrap);
        painter->drawText(bound, tr("No tank data"));
    } else {
        painter->drawLine(bound.left(),0,bound.right(),0);
        ange::units::Length lcg = m_document->stowage()->stowageAsBlockWeight(call()).lcg();
        painter->drawLine(lcg/ange::units::meter,bound.top(), lcg/ange::units::meter,bound.bottom());
    }

    painter->setPen(Qt::black);
    painter->drawLine(bound.left(), bound.bottom()-painter->pen().widthF()/2, bound.right(), bound.bottom()-painter->pen().widthF()/2);
    painter->restore();
}

QRectF TankVesselGraphicsItem::boundingRect() const {
    const double breadth = m_vessel->breadth()/ange::units::meter;
    QRectF br(m_vessel->aftOverAll()/ange::units::meter, -breadth/2.0,
              (m_vessel->foreOverAll() - m_vessel->aftOverAll())/ange::units::meter, breadth);
    Q_FOREACH(QGraphicsItem* child, childItems()) {
        br = br.united(child->mapRectToParent(child->boundingRect()));
    }
    return br;
}

void TankVesselGraphicsItem::mousePressEvent(QGraphicsSceneMouseEvent* event) {
    if(event->buttons() & Qt::LeftButton) {
        Q_EMIT clicked();
    }
    QGraphicsItem::mousePressEvent(event);
}

void TankVesselGraphicsItem::lcgChangedAtCall(const ange::schedule::Call* call) {
    if(call == this->call()) {
        update();
    }
}

#include "tankvesselgraphicsitem.moc"
