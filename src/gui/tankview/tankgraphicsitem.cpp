#include "tankgraphicsitem.h"
#include <gui/utils/painter_utils.h>
#include <ange/vessel/vesseltank.h>
#include <QPainter>
#include <document/stowage/stowage.h>
#include <ange/schedule/call.h>
#include <QGraphicsSceneMouseEvent>
#include <document/document.h>
#include <document/undo/tank_state_command.h>
#include <document/tanks/tankconditions.h>
#include <QApplication>
#include <QKeyEvent>
#include <QGraphicsLinearLayout>

using namespace ange::units;

static const int fontSize = 3;
/**
 * A class to show the tank rectangle
 */
class GraphicalTankRepresentation : public QGraphicsItem {
    public:
        GraphicalTankRepresentation(TankGraphicsItem* tankItem) : QGraphicsItem(tankItem), m_tankItem(tankItem) {}
        virtual QRectF boundingRect() const;
    protected:
        virtual void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget = 0);
        virtual void mousePressEvent(QGraphicsSceneMouseEvent* event) {
            // if we don't get mousePressEvents, we also don't get mouseReleaseEvents.
            event->accept();
        }
        virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent* event) {
            qreal fraction = qMax(event->pos().x()/boundingRect().width(),qreal(0.0));
            tank_state_command_t* tank_state = new tank_state_command_t(m_tankItem->m_document,m_tankItem->m_call);
            tank_state->insert_tank_condition(m_tankItem->m_tank,qRound(m_tankItem->m_tank->capacity()*fraction/1000)*ton);
            m_tankItem->m_document->undostack()->push(tank_state);
        }
    private:
        TankGraphicsItem* m_tankItem;
};

/**
 * A class to represent a piece of the text on the tank
 */
class TextItem : public QGraphicsItem {
    public:
        TextItem(const QSizeF& size, const QString& text, QGraphicsItem* parent=0) : QGraphicsItem(parent), m_size(size), m_text(text), m_alignment(Qt::AlignCenter | Qt::AlignVCenter) {}
        virtual QRectF boundingRect() const {
            return QRectF(QPointF(0,0),m_size);
        }
        void setText(const QString& text) {
            m_text = text;
            update();
        }
        void setAlignment(Qt::Alignment newAlignment) {
            m_alignment = newAlignment;
            update();
        }
    protected:
        virtual void paint(QPainter* painter, const QStyleOptionGraphicsItem* /*option*/, QWidget* /*widget*/) {
            QTextOption option;
            option.setAlignment(m_alignment);
            option.setWrapMode(QTextOption::NoWrap);

            painter->setFont(QFont("Arial", fontSize));
            painter->drawText(boundingRect(), m_text, option);
        }
    private:
        QSizeF m_size;
        QString m_text;
        Qt::Alignment m_alignment;
};

void GraphicalTankRepresentation::paint(QPainter* painter, const QStyleOptionGraphicsItem* /*option*/, QWidget* /*widget*/) {
    ange::units::Mass tank_condition = m_tankItem->m_document->tankConditions()
                                                 ->tankCondition(m_tankItem->m_call,m_tankItem->m_tank);
    QRectF rect = boundingRect();
    painter->fillRect(rect, Qt::white);
    painter->setBrush(QColor(200,200,255,128));
    painter->setPen(Qt::NoPen);
    rect.setWidth(rect.width()*qMin(m_tankItem->m_document->tankConditions()
                                                    ->tankCondition(m_tankItem->m_call,m_tankItem->m_tank)/kilogram,
                                    m_tankItem->m_tank->capacity())/m_tankItem->m_tank->capacity());
    painter->drawRect(rect);
    painter->setBrush(Qt::NoBrush);
    QPen pen;
    pen.setWidth(1.5);
    if(tank_condition > m_tankItem->m_tank->capacity()*kilogram) {
        pen.setColor(Qt::red);
    } else {
        pen.setColor(QColor(200,128,255,128));
    }
    painter->setRenderHint(QPainter::Antialiasing,true);
    painter->setPen(pen);
    painter->drawRect(boundingRect());
    painter->setRenderHint(QPainter::Antialiasing,false);
}

QRectF GraphicalTankRepresentation::boundingRect() const {
    //Ensure a minimal tank width in spite of input data errors, since a width of 0 makes the 
    //tank view unusable
    const qreal tank_length = qMax(10.0, 0.9*qAbs(m_tankItem->m_tank->foreEnd()-m_tankItem->m_tank->aftEnd()));
    //We want to give an impression of the tank's size (volume) through the area of the rectangle
    //The value 3000 seems to give a good aspect ratio
    //Restrict the value to the interval [10, 30] to keep the tank visible (10) and not excessively high (30)
    const qreal tank_height = qMin(qMax(10.0, m_tankItem->m_tank->capacity() / tank_length / 3000), 30.0);
    return QRectF(0,0,tank_length,tank_height);
}

TankGraphicsItem::TankGraphicsItem(const ange::schedule::Call* call, document_t* document, ange::vessel::VesselTank* tank,
                         QGraphicsItem* parentitem): QGraphicsObject(parentitem), m_document(document),m_call(call),
                         m_tank(tank), m_isEditing(false), m_selectNext(0), m_selectPrev(0) {
    connect(m_document->tankConditions(),&TankConditions::tankConditionChanged,this, &TankGraphicsItem::change_tank_condition);
    setToolTip(tank->description() + "  " + conditionString());
    m_graphicTankItem = new GraphicalTankRepresentation(this);
    QSizeF size(m_graphicTankItem->boundingRect().width(), fontSize);
    m_nameItem = new TextItem(size,tank->description(),this);
    m_nameItem->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);
    m_contentItem = new TextItem(m_graphicTankItem->boundingRect().size(), conditionString(),this);
    setFlag(QGraphicsItem::ItemIsSelectable, true);
    setFlag(QGraphicsItem::ItemIsFocusable, true);
}

QRectF TankGraphicsItem::boundingRect() const {
    QRectF graphicsrect = m_graphicTankItem->mapRectToParent(m_graphicTankItem->boundingRect());
    QRectF contentrect = m_contentItem->mapRectToParent(m_contentItem->boundingRect());
    QRectF namerect = m_nameItem->mapRectToParent(m_nameItem->boundingRect());
    contentrect.setWidth(graphicsrect.width());
    namerect.setWidth(graphicsrect.width());
    graphicsrect |= namerect;
    graphicsrect |= contentrect;
    return graphicsrect;
}

void TankGraphicsItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* /*option*/, QWidget* /*widget*/) {
    painter->save();
    if(isSelected()) {
        QPen selectpen;
        selectpen.setStyle(Qt::DashLine);
        painter->setPen(selectpen);
        painter->drawRect(boundingRect());
    }
    painter->restore();
}

void TankGraphicsItem::change_tank_condition(const ange::schedule::Call* call, const ange::vessel::VesselTank* tank,
                                        const ange::units::Mass) {
    if(call==m_call && tank==m_tank) {
        m_contentItem->setText(conditionString());
        update();
        setToolTip(tank->description() + "  " + conditionString());
    }
}

void TankGraphicsItem::show_tank_group(const QString& group) {
    setVisible(group==m_tank->tankGroup() || group.isNull());
}

void TankGraphicsItem::keyPressEvent(QKeyEvent* event) {
    // need to accept it to get release events
    // maybe could be more fine grained to match what we have in release events
    event->accept();
}

bool TankGraphicsItem::sceneEvent(QEvent* event) {
    if(event->type() == QEvent::KeyPress ) {
        keyPressEvent(static_cast<QKeyEvent*>(event));
        return true;
    } else {
        return QGraphicsObject::sceneEvent(event);
    }
}

void TankGraphicsItem::keyReleaseEvent(QKeyEvent* event) {
    if(event->key()==Qt::Key_Enter || event->key() == Qt::Key_Return) {
        acceptEditString();
        return;
    }
    if(event->key() == Qt::Key_Escape) {
        rejectEditString();
        return;
    }
    if((event->key() >= Qt::Key_0 && event->key() <= Qt::Key_9) || event->text() == m_decimal_point ) {
        m_isEditing = true;
        m_editString += event->text();
        m_contentItem->setText(m_editString + "_");
        return;
    }
    if(event->key() == Qt::Key_N || event->key() == Qt::Key_Tab) {
        setSelected(false);
        m_selectPrev->setSelected(true);
        update();
        m_selectPrev->update();
        m_selectPrev->setFocus(Qt::ShortcutFocusReason);
        return;
    }
    if(event->key() == Qt::Key_P || event->key() == Qt::Key_Backtab) {
        setSelected(false);
        m_selectNext->setSelected(true);
        update();
        m_selectNext->update();
        m_selectNext->setFocus(Qt::ShortcutFocusReason);
        return;
    }
    if(event->key()!=Qt::Key_E && event->key()!=Qt::Key_F) {
        QGraphicsItem::keyReleaseEvent(event);
        return;
    }
    rejectEditString();

    tank_state_command_t* tank_state = new tank_state_command_t(m_document,m_call);
    if(event->key()==Qt::Key_E) {
        tank_state->insert_tank_condition(m_tank, 0*kilogram);
        m_document->undostack()->push(tank_state);
    } else if(event->key()==Qt::Key_F) {
        tank_state->insert_tank_condition(m_tank, m_tank->capacity()*kilogram);
        m_document->undostack()->push(tank_state);
    }
}

void TankGraphicsItem::acceptEditString() {
    if(!m_isEditing) {
        return;
    }
    bool success = false;
    double inputTankContent = m_editString.toDouble(&success);
    if(!success) {
        rejectEditString();
        return;
    }
    Mass maxContent  = m_tank->capacity() * kilogram;
    Mass tankContent = inputTankContent * ton;
    tankContent = qMin<Mass>(tankContent, maxContent*1.1);
    tank_state_command_t* tank_state = new tank_state_command_t(m_document,m_call);
    tank_state->insert_tank_condition(m_tank, tankContent);
    m_document->undostack()->push(tank_state);
    m_editString.clear();
    m_isEditing = false;
}

void TankGraphicsItem::rejectEditString() {
    m_contentItem->setText(conditionString());
    m_editString.clear();
    m_isEditing = false;
}

QString TankGraphicsItem::conditionString() const {
    ange::units::Mass tank_condition = m_document->tankConditions()->tankCondition(m_call, m_tank);
    return QString::number(qRound(tank_condition/ton)) + " / " +  QString::number(qRound(m_tank->capacity()/1000));
}

void TankGraphicsItem::focusOutEvent(QFocusEvent* event) {
    acceptEditString();
    QGraphicsItem::focusOutEvent(event);
}

void TankGraphicsItem::setSelectNext(TankGraphicsItem* tankItem) {
    m_selectNext = tankItem;
    tankItem->m_selectPrev = this;
}

#include "tankgraphicsitem.moc"
