#ifndef SELECTEDTANKCONTENTEDITFIELD_H
#define SELECTEDTANKCONTENTEDITFIELD_H

#include <QLineEdit>

class document_t;
class TankItemSelectionModel;
class QRegularExpressionValidator;

namespace ange {
    namespace schedule {
        class Call;
    }
}

/**
 * Edit field to let the user enter a string like
 */
class SelectedTankContentEditField : public QLineEdit {
    Q_OBJECT
    public:
        SelectedTankContentEditField(QWidget* parent = 0);
        virtual ~SelectedTankContentEditField();
        /**
         * Sets the document to work on when doing changes
         */
        void setDocument(document_t* document);
        /**
         * Sets the thing to query for selected tanks
         */
        void setTankSelection(const TankItemSelectionModel* tankselection);

        /**
         * Sets the current call to \param call
         */
        void setCurrentCall(const ange::schedule::Call* call);

        /**
         * The result type of the parseString function. Public for tests
         */
        struct ParseObject {
            enum ParseType {
                Percent,
                Ton
            };
            ParseObject(double a, ParseType u) : amount(a), unit(u) { }
            const double amount;
            const ParseType unit;
        };
        /**
         * Parses the string into a ParseObject type. Public for tests.
         */
        static ParseObject parseString(const QString& string);
    private Q_SLOTS:
        /**
         * handle the changes
         */
        void submitChanges();
    private:
        document_t* m_document;
        const TankItemSelectionModel* m_tankSelection;
        const ange::schedule::Call* m_currentCall;
        QRegularExpressionValidator* m_validator;

};

#endif // SELECTEDTANKCONTENTEDITFIELD_H
