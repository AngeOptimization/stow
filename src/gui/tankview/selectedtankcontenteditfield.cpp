#include "selectedtankcontenteditfield.h"

#include "document.h"
#include "merger_command.h"
#include "tank_state_command.h"
#include "tankitemselectionmodel.h"

#include <ange/vessel/vesseltank.h>

#include <QRegularExpression>
#include <QRegularExpressionValidator>

SelectedTankContentEditField::SelectedTankContentEditField(QWidget* parent) : QLineEdit(parent), m_document(0), m_tankSelection(0) {
    connect(this, &QLineEdit::returnPressed, this, &SelectedTankContentEditField::submitChanges);
    QRegularExpression regex("(\\d*[.]?\\d*)[t%]?");
    m_validator = new QRegularExpressionValidator(regex);
    setValidator(m_validator);
    setPlaceholderText("75% or 75t - fill selected");
}

void SelectedTankContentEditField::setDocument(document_t* document) {
    if(m_document != document) {
        m_currentCall = 0;
    }
    m_document = document;
}

void SelectedTankContentEditField::setTankSelection(const TankItemSelectionModel* tankselection) {
    m_tankSelection = tankselection;
}

void SelectedTankContentEditField::submitChanges() {
    Q_ASSERT(m_document);
    Q_ASSERT(m_tankSelection);
    Q_ASSERT(m_currentCall);
    ParseObject obj = parseString(text());

    tank_state_command_t* tankcommand = new tank_state_command_t(m_document, m_currentCall);

    Q_FOREACH(const ange::vessel::VesselTank* tank, m_tankSelection->selectedTanks()) {
        double amount = 0;
        switch(obj.unit) {
            case ParseObject::Percent: {
                double percent = qMin<double>(110, obj.amount);
                percent = qMax<double>(0, percent);
                amount = tank->capacity() * percent / 100;
                break;
            }
            case ParseObject::Ton: {
                amount = qMin<double>(tank->capacity()*1.1, obj.amount*1000);
                break;
            }
        }
        tankcommand->insert_tank_condition(tank, amount * ange::units::kilogram);
    }
    m_document->undostack()->push(tankcommand);
    clear();
}

SelectedTankContentEditField::ParseObject SelectedTankContentEditField::parseString(const QString& string) {
    Q_UNUSED(string);

    {
        QRegularExpression percent("(\\d+[.]?\\d*)%");
        QRegularExpressionMatch percentMatch = percent.match(string.trimmed());
        if(percentMatch.hasMatch()) {
            return ParseObject(percentMatch.captured(1).toDouble(), ParseObject::Percent);
        }
    }
    {
        QRegularExpression ton("(\\d+[.]?\\d*)t?");
        QRegularExpressionMatch tonMatch = ton.match(string.trimmed());
        if(tonMatch.hasMatch()) {
            return ParseObject(tonMatch.captured(1).toDouble(), ParseObject::Ton);
        }
    }
    return ParseObject(0, ParseObject::Ton);
}

void SelectedTankContentEditField::setCurrentCall(const ange::schedule::Call* call) {
    m_currentCall = call;
}

SelectedTankContentEditField::~SelectedTankContentEditField() {
    delete m_validator;
}

#include "selectedtankcontenteditfield.moc"
