#include "tankgroupselectorsynchronizer.h"

#include <QComboBox>
#include "document/document.h"
#include <ange/vessel/vessel.h>

TankGroupSelectorSynchronizer::TankGroupSelectorSynchronizer(QObject* parent) : QObject(parent) {
    setObjectName("TankGroupSelectorSynchronizer");
}

void TankGroupSelectorSynchronizer::synchronizeWithRest(QComboBox* box, document_t* document) {
    box->clear();

    QString ballastTankGroup = document->vessel()->ballastTankGroup();
    box->addItem(QString());
    Q_FOREACH(const QString& tankGroup, document->vessel()->tankGroups()) {
        box->addItem(tankGroup);
        if(m_list.isEmpty()) {
            if(tankGroup == ballastTankGroup) {
                box->setCurrentIndex(box->count()-1);
            }
        }
    }

    if(!m_list.isEmpty()) {
        box->setCurrentIndex(m_list.first()->currentIndex());
    }

    Q_FOREACH(QComboBox* existingbox, m_list) {
        connect(existingbox, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), box, &QComboBox::setCurrentIndex);
        connect(box, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), existingbox, &QComboBox::setCurrentIndex);
    }
    connect(box,&QComboBox::destroyed, this, &TankGroupSelectorSynchronizer::removeCombo);

    m_list << box;
}

void TankGroupSelectorSynchronizer::removeCombo(QObject* obj) {
    QComboBox* box = static_cast<QComboBox*>(obj);
    int count = m_list.removeAll(box);
    Q_UNUSED(count);
}

void TankGroupSelectorSynchronizer::clear() {
    m_list.clear();
}

#include "tankgroupselectorsynchronizer.moc"
