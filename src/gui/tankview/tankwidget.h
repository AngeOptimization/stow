#ifndef TANKWIDGET_H
#define TANKWIDGET_H

#include <QWidget>
#include <QPointer>

class TankItemSelectionModel;
class SelectedTankContentEditField;
class TankGroupSelectorSynchronizer;
class QComboBox;
class document_t;
class tank_view_t;
class document_t;

namespace ange {
    namespace schedule {
        class Call;
    } // namespace schedule
    namespace vessel {
        class Vessel;
        class VesselTank;
    } // namespace vessel
} // namespace ange

class TankWidget : public QWidget {
    Q_OBJECT

    public:
        TankWidget(QWidget* parent = 0, Qt::WindowFlags f = 0);
        void setTankGroupSelectorSynchronizer(TankGroupSelectorSynchronizer* synchronizer);
        void setDocument(document_t* document);
        void setCurrentCall(const ange::schedule::Call* call);
        void setTankSelection(const TankItemSelectionModel* tankselection);
    Q_SIGNALS:
        void tankGroupSelected(QString group);
        void tanksSelected(QList<const ange::vessel::VesselTank*> tank);
    public Q_SLOTS:
        void hideNonActiveCallsChanged(bool hide);
        /**
         * Selects the listed tanks at current call
         */
        void selectTanks(QList<const ange::vessel::VesselTank*> tanks);
    private Q_SLOTS:
        void setVessel(ange::vessel::Vessel*);
    private:
        document_t* m_document;
        tank_view_t* m_tankView;
        QComboBox* m_tankGroups;
        QPointer<TankGroupSelectorSynchronizer> m_tankgroupsynchronizer;
        SelectedTankContentEditField* m_contentedit;
};

#endif // TANKWIDGET_H
