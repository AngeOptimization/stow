#include "tanktablewidget.h"
#include "tankgroupselectorsynchronizer.h"
#include "tankgroupfilterproxymodel.h"
#include "selectedtankcontenteditfield.h"
#include "tankitemselectionmodel.h"
#include <document/document.h>
#include <QTableView>
#include <QComboBox>
#include <ange/vessel/vessel.h>
#include <QVBoxLayout>
#include "../utils/tableviewutils.h"



TankTableWidget::TankTableWidget(QWidget* parent, Qt::WindowFlags f) : QWidget(parent,f), m_document(0) {
    m_tankGroups = new QComboBox();
    m_tankTableView = new QTableView();
    m_tankTableView->setAlternatingRowColors(true);
    m_tankTableView->verticalHeader()->setVisible(false);
    m_contentedit = new SelectedTankContentEditField();
    TableViewUtils::setupCopySelection(m_tankTableView);
    QVBoxLayout* lay = new QVBoxLayout();
    lay->setMargin(0);
    QHBoxLayout* toplay = new QHBoxLayout();
    toplay->addWidget(m_tankGroups);
    toplay->addWidget(m_contentedit);
    lay->addLayout(toplay);
    lay->addWidget(m_tankTableView);
    m_groupProxy = new TankGroupFilterProxyModel(this);
    m_tankTableView->setModel(m_groupProxy);
    setLayout(lay);
}

void TankTableWidget::setTankGroupSelectorSynchronizer(TankGroupSelectorSynchronizer* synchronizer) {
    m_tankgroupselectorsynchronizer = synchronizer;

}

void TankTableWidget::setDocument(document_t* document) {
    Q_ASSERT(m_tankgroupselectorsynchronizer);
    if (document != m_document) {
        connect(document, &document_t::vessel_changed, this, &TankTableWidget::setVessel);
        if (m_document) {
            m_document->disconnect(this);
        }

    }
    m_document = document;
    QComboBox* tankGroups = new QComboBox();
    m_tankgroupselectorsynchronizer->synchronizeWithRest(tankGroups, document);
    connect(tankGroups, static_cast<void (QComboBox::*)(const QString&)>(&QComboBox::currentIndexChanged),m_groupProxy, &TankGroupFilterProxyModel::setTankGroup);

    QLayoutItem* result = layout()->replaceWidget(m_tankGroups, tankGroups);
    Q_ASSERT(result);
    Q_UNUSED(result);
    m_tankGroups->disconnect(this);
    m_tankGroups->disconnect(m_tankTableView);
    m_tankGroups->disconnect(m_groupProxy);
    m_tankGroups->deleteLater();
    m_tankGroups = tankGroups;
    m_contentedit->setDocument(document);
    m_tankTableView->resizeColumnsToContents();
    m_tankTableView->resizeRowsToContents();
}

void TankTableWidget::setVessel(const ange::vessel::Vessel* vessel) {
    Q_UNUSED(vessel);
    setDocument(m_document);
}

void TankTableWidget::setSelectionModel(TankItemSelectionModel* selectionModel) {
    m_contentedit->setTankSelection(selectionModel);
    m_tankTableView->setSelectionModel(selectionModel);
}

QAbstractItemModel* TankTableWidget::model() {
    return m_groupProxy->sourceModel();
}

void TankTableWidget::setModel(QAbstractItemModel* model) {
    m_groupProxy->setSourceModel(model);
}

QAbstractItemModel* TankTableWidget::proxyModel() {
    return m_groupProxy;
}

void TankTableWidget::setCurrentCall(const ange::schedule::Call* call) {
    m_contentedit->setCurrentCall(call);
}


#include "tanktablewidget.moc"
