#ifndef TANKGROUPFILTERPROXYMODEL_H
#define TANKGROUPFILTERPROXYMODEL_H

#include <QSortFilterProxyModel>



/**
 * Filter model to only show the tanks from the current tank group. Is built on top of the \ref TankModel
 */
class TankGroupFilterProxyModel : public QSortFilterProxyModel {
    Q_OBJECT
    public:
        TankGroupFilterProxyModel(QObject* parent);
    public Q_SLOTS:
        /**
         * Sets the accepted tank group to \param newGroup
         * if \param newGroup is a null QString, everything will be accepted
         */
        void setTankGroup(const QString& newGroup);
};

#endif // TANKGROUPFILTERPROXYMODEL_H
