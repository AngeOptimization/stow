#ifndef TANKGROUPSELECTORSYNCHRONIZER_H
#define TANKGROUPSELECTORSYNCHRONIZER_H

#include <QObject>
#include <QList>

class QComboBox;
class document_t;

/**
 * Initializes comboboxes and sets up synchronization with other comboboxes
 */
class TankGroupSelectorSynchronizer : public QObject {
    Q_OBJECT
    public:
        TankGroupSelectorSynchronizer(QObject* parent = 0);
        /**
         * Clears its internal state
         */
        void clear();
        /**
         * sets up and sets up synchronization between \param box and the rest,
         * with data from \param document
         */
        void synchronizeWithRest(QComboBox* box, document_t* document);
    private Q_SLOTS:
        /**
         * Removes the combobox from the internal list
         */
        void removeCombo(QObject* obj);
    private:
        QList<QComboBox*> m_list;
};

#endif // TANKGROUPSELECTORSYNCHRONIZER_H
