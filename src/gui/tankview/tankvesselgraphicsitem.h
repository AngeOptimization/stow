#ifndef TANK_VESSEL_GRAPHICS_ITEM_H
#define TANK_VESSEL_GRAPHICS_ITEM_H

#include "../stow.h"
#include "../common/abstractvesselgraphicsitem.h"

class document_t;

/**
 *A class for representing the vessel with its tanks graphically
 */
class TankVesselGraphicsItem : public AbstractVesselGraphicsItem {
  Q_OBJECT
  public:
    TankVesselGraphicsItem(document_t* vessel, const ange::schedule::Call* call, QGraphicsItem* parent = 0);
    virtual void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget* = 0);
    virtual QRectF boundingRect() const;
    virtual double max_value() const;
    enum {
      Type = UserType + Stow::TankVesselGraphicsItem
    };
    virtual int type() const {
      return Type;
    }
  protected:
    virtual void mousePressEvent(QGraphicsSceneMouseEvent* event);
  Q_SIGNALS:
    void clicked();
    private:
        document_t* m_document;
    private Q_SLOTS:
        void lcgChangedAtCall(const ange::schedule::Call* call);
};

#endif // TANK_VESSEL_GRAPHICS_ITEM_H
