#include "tankwidget.h"
#include <document/document.h>
#include <ange/vessel/vessel.h>
#include <ange/vessel/vesseltank.h>
#include <QLabel>
#include <QVBoxLayout>
#include <QComboBox>
#include <QLayout>
#include "tank_view.h"
#include "tankgroupselectorsynchronizer.h"
#include "selectedtankcontenteditfield.h"

TankWidget::TankWidget(QWidget* parent, Qt::WindowFlags f) : QWidget(parent, f), m_document(0) {
    m_tankView = new tank_view_t(this);
    m_tankGroups = new QComboBox();
    m_contentedit = new SelectedTankContentEditField();
    QVBoxLayout* layout = new QVBoxLayout();
    layout->setContentsMargins(0,0,0,0);
    QHBoxLayout* toplayout = new QHBoxLayout();
    toplayout->addWidget(m_tankGroups);
    toplayout->addWidget(m_contentedit);
    layout->addLayout(toplayout);
    layout->addWidget(m_tankView);
    connect(m_tankView, &tank_view_t::tanksSelected, this, &TankWidget::tanksSelected);
    connect(this,SIGNAL(tankGroupSelected(QString)),m_tankView,SLOT(setCurrentTankGroup(QString)));
    setLayout(layout);
}

void TankWidget::setTankGroupSelectorSynchronizer(TankGroupSelectorSynchronizer* synchronizer) {
    m_tankgroupsynchronizer = synchronizer;
}


void TankWidget::setDocument(document_t* document) {
    Q_ASSERT(m_tankgroupsynchronizer);
    if (document != m_document) {
        connect(document, SIGNAL(vessel_changed(ange::vessel::Vessel*)), SLOT(setVessel(ange::vessel::Vessel*)));
        if (m_document) {
            m_document->disconnect(this);
        }

    }
    QComboBox* tankGroups = new QComboBox();

    m_tankgroupsynchronizer->synchronizeWithRest(tankGroups, document);

    QLayoutItem* result = layout()->replaceWidget(m_tankGroups, tankGroups);
    Q_ASSERT(result);
    Q_UNUSED(result);
    m_tankGroups->disconnect(this);
    m_tankGroups->deleteLater();
    m_tankGroups = tankGroups;
    connect(m_tankGroups, static_cast<void (QComboBox::*)(const QString&)>(&QComboBox::currentIndexChanged),this, &TankWidget::tankGroupSelected);
    m_tankView->set_document(document);
    emit tankGroupSelected(document->vessel()->ballastTankGroup());
    m_document = document;
    m_contentedit->setDocument(document);
}

void TankWidget::setCurrentCall(const ange::schedule::Call* call) {
    m_tankView->set_current_call(call);
    m_contentedit->setCurrentCall(call);
}

void TankWidget::hideNonActiveCallsChanged(bool hide) {
    m_tankView->hide_non_active_calls_changed(hide);
}

void TankWidget::setVessel(ange::vessel::Vessel* ) {
    setDocument(m_document);
}

void TankWidget::selectTanks(QList< const ange::vessel::VesselTank* > tanks) {
    const bool wasBlocked = m_tankView->blockSignals(true);
    m_tankView->selectTanks(tanks);
    m_tankView->blockSignals(wasBlocked);
}

void TankWidget::setTankSelection(const TankItemSelectionModel* tankselection) {
    m_contentedit->setTankSelection(tankselection);
}

#include "tankwidget.moc"
