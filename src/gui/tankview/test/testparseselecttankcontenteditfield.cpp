#include <QtTest/QTest>
#include "../selectedtankcontenteditfield.h"


class TestParseSelectTankContentEditField : public QObject {
    Q_OBJECT
    private Q_SLOTS:
        void testStuff();
        void testStuff_data();

};

void TestParseSelectTankContentEditField::testStuff() {
    QFETCH(QString, data);
    QFETCH(double, amount);
    QFETCH(int, type);

    SelectedTankContentEditField::ParseObject obj = SelectedTankContentEditField::parseString(data);

    QCOMPARE(obj.amount, amount);
    QCOMPARE(int(obj.unit), type);
}

void TestParseSelectTankContentEditField::testStuff_data() {
    QTest::addColumn<QString>("data");
    QTest::addColumn<double>("amount");
    QTest::addColumn<int>("type");

    QTest::newRow("100t") << QStringLiteral("100t") << double(100) << int(SelectedTankContentEditField::ParseObject::Ton);
    QTest::newRow("10") << QStringLiteral("10") << double(10) << int(SelectedTankContentEditField::ParseObject::Ton);
    QTest::newRow("75%") << QStringLiteral("75%") << double(75) << int(SelectedTankContentEditField::ParseObject::Percent);
    QTest::newRow("75.0%") << QStringLiteral("75.0%") << double(75) << int(SelectedTankContentEditField::ParseObject::Percent);
    QTest::newRow("hest") << QStringLiteral("hest") << double(0) << int(SelectedTankContentEditField::ParseObject::Ton);
}



QTEST_GUILESS_MAIN(TestParseSelectTankContentEditField);
#include "testparseselecttankcontenteditfield.moc"
