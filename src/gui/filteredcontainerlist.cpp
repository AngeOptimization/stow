#include "filteredcontainerlist.h"

#include "pool.h"

#include <qdatacube/abstractfilter.h>
#include <qdatacube/datacube.h>

#include <QDate>
#include <QItemSelectionModel>
#include <QTimer>
#include <QSharedPointer>

#include <algorithm>

FilteredContainerList::FilteredContainerList(qdatacube::Datacube* datacube, QObject* parent)
  : QSortFilterProxyModel(parent),
    m_datacube(datacube),
    m_ignoreSelectionChanges(false),
    m_selectionModel(0L),
    m_pool(0L)
{
  connect(datacube, SIGNAL(reset()), SLOT(invalidate()));
  connect(datacube, SIGNAL(dataChanged(int,int)), SLOT(invalidate()));
  connect(datacube, SIGNAL(rowsAboutToBeInserted(int,int)), SLOT(invalidate()));
  connect(datacube, SIGNAL(rowsAboutToBeRemoved(int,int)), SLOT(invalidate()));
  connect(datacube, SIGNAL(columnsAboutToBeInserted(int,int)), SLOT(invalidate()));
  connect(datacube, SIGNAL(columnsAboutToBeRemoved(int,int)), SLOT(invalidate()));
  connect(datacube, &qdatacube::Datacube::destroyed, this, &FilteredContainerList::deleteLater);
}

FilteredContainerList::~FilteredContainerList() {}

void FilteredContainerList::synchronize(QItemSelectionModel* selection_model, pool_t* pool) {
  if (m_selectionModel) {
    m_selectionModel->disconnect(this);
  }
  if (m_pool) {
    m_pool->disconnect(this);
  }
  if (selection_model && pool) {
    m_pool = pool;
    m_selectionModel = selection_model;
    Q_ASSERT(!m_selectionModel->hasSelection());
    selectRows(pool->included_rows());
    connect(m_selectionModel, SIGNAL(selectionChanged(QItemSelection,QItemSelection)), SLOT(updateSelectionOnPool(QItemSelection,QItemSelection)));
    connect(m_pool, SIGNAL(rows_added(QList<int>)), SLOT(selectRows(QList<int>)));
    connect(m_pool, SIGNAL(rows_removed(QList<int>)), SLOT(deselectRows(QList<int>)));
  } else {
    m_pool = 0L;
    m_selectionModel = 0L;
  }
}

bool FilteredContainerList::filterAcceptsRow(int source_row, const QModelIndex& source_parent) const {
  Q_UNUSED(source_parent);
  QAbstractItemModel* model = sourceModel();
  // Reverse map all the way back to the source
  while (QAbstractProxyModel* proxy_model = qobject_cast<QAbstractProxyModel*>(model)) {
    QModelIndex index = proxy_model->mapToSource(proxy_model->index(source_row,0));
    source_row = index.row();
    model = proxy_model;
  }
  // Apply filter
  Q_FOREACH(qdatacube::Datacube::Filters::value_type filter, m_datacube->filters()) {
    if (!(*filter)(source_row)) {
      return false;
    }
  }
  return true;
}

void FilteredContainerList::updateSelectionOnPool(const QItemSelection& selected, const QItemSelection& deselected) {
  if (m_ignoreSelectionChanges) {
    return;
  }
  QTime timer;timer.start();
  QList<int> included_source_rows;
  Q_FOREACH(QItemSelectionRange range, mapSelectionToSource(selected)) {
    for (int row=range.top(); row<=range.bottom(); ++row) {
      included_source_rows << row;
    }
  }
  QList<int> excluded_source_rows;
  Q_FOREACH(QItemSelectionRange range, mapSelectionToSource(deselected)) {
    for (int row=range.top(); row<=range.bottom(); ++row) {
      excluded_source_rows << row;
    }
  }
  // Remove duplicates
  qSort(included_source_rows);
  included_source_rows.erase(std::unique(included_source_rows.begin(), included_source_rows.end()),
                             included_source_rows.end());
  // Remove duplicates
  qSort(excluded_source_rows);
  excluded_source_rows.erase(std::unique(excluded_source_rows.begin(), excluded_source_rows.end()),
                             excluded_source_rows.end());
  m_ignoreSelectionChanges = true;
  m_pool->include(included_source_rows);
  m_pool->exclude(excluded_source_rows);
  m_ignoreSelectionChanges = false;
}

void FilteredContainerList::deselectRows(QList< int > source_rows) {
  if (m_ignoreSelectionChanges) {
    return;
  }
  QTime timer;timer.start();
  QItemSelection deselection;
  const int last_column = sourceModel()->columnCount()-1;
  Q_FOREACH(int source_row, source_rows) {
    deselection << QItemSelectionRange(sourceModel()->index(source_row, 0), sourceModel()->index(source_row, last_column));
  }
  m_ignoreSelectionChanges = true;
  m_selectionModel->select(mapSelectionFromSource(deselection), QItemSelectionModel::Deselect);
  m_ignoreSelectionChanges = false;
}

void FilteredContainerList::selectRows(QList< int > source_rows)
{
  if (m_ignoreSelectionChanges) {
    return;
  }
  QTime timer;timer.start();
  QItemSelection selection;
  const int last_column = sourceModel()->columnCount()-1;
  Q_FOREACH(int source_row, source_rows) {
    selection << QItemSelectionRange(sourceModel()->index(source_row, 0), sourceModel()->index(source_row, last_column));
  }
  m_ignoreSelectionChanges = true;
  m_selectionModel->select(mapSelectionFromSource(selection), QItemSelectionModel::Select);
  m_ignoreSelectionChanges = false;
}

#include "filteredcontainerlist.moc"
