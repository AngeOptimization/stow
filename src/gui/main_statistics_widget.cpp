#include "main_statistics_widget.h"

#include "container_leg.h"
#include "container_list.h"
#include "document.h"
#include "stowage.h"

#include <ange/containers/container.h>
#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>
#include <ange/vessel/point3d.h>
#include <ange/vessel/slot.h>
#include <ange/vessel/vessel.h>

#include <QHBoxLayout>
#include <QLabel>
#include <QTextStream>
#include <QTimer>

using ange::containers::Container;
using ange::schedule::Schedule;

main_statistics_widget_t::main_statistics_widget_t(document_t* document, QWidget* parent): QWidget(parent), m_document(0L) {
  m_stat_timer=new QTimer(this);
  m_stat_timer->setSingleShot(true);
  m_stat_timer->setInterval(100);
  connect(m_stat_timer, &QTimer::timeout, this, &main_statistics_widget_t::update_stats);
  m_stat_label=new QLabel(this);
  m_stat_label->setAlignment(Qt::AlignTop | Qt::AlignLeft);
  set_document(document);
  QVBoxLayout* lay = new QVBoxLayout;
  lay->addWidget(m_stat_label);
  setLayout(lay);
}

void main_statistics_widget_t::set_document(document_t* document) {
  if(document!=m_document) {
    m_stat_timer->stop();
    connect(document->schedule(), &Schedule::scheduleChanged, this, &main_statistics_widget_t::update_stats);
    connect(document->stowage(), &stowage_t::container_changed_position, this, &main_statistics_widget_t::stowage_changed);
    connect(document->stowage(), &stowage_t::stowageChanged, this, &main_statistics_widget_t::stowage_changed);
    m_document=document;
    update_stats();
  }
}

void main_statistics_widget_t::stowage_changed() {
  if (!m_stat_timer->isActive()) {
    m_stat_timer->start();
  }
}

void main_statistics_widget_t::update_stats() {
  static const char* const TR = "<tr>";
  static const char* const ETR = "</tr>";
  static const char* const TH = "<th>";
  static const char* const ETH =  "</th>";
  static const char* const TD = "<td>";
  static const char* const ETD = "</td>";

  QString text;
  QTextStream stream(&text);
  stream << "<table>" << TR << TH << "call" << ETH << TH << "load" << ETH << TH << "disc" << ETH << TH << "restow"
         << ETH << TH << "count" << ETH << TH << "left" << ETH << ETR;


  Q_FOREACH(const ange::schedule::Call* call, m_document->schedule()->calls()) {
    stream << TR
           << TD << call->uncode() << ETD
           << TD << m_document->stowage()->load_count(call) << ETD
           << TD << m_document->stowage()->discharge_count(call) << ETD
           << TD << m_document->stowage()->restow_count(call) << ETD
           << TD << m_document->stowage()->container_routes()->source_count(call) << ETD
           << TD << (m_document->stowage()->container_routes()->source_count(call)-m_document->stowage()->load_count(call)) << ETD
           << ETR;
  }
  stream << "</table>";
  m_stat_label->setText(text);
}

#include "main_statistics_widget.moc"

