/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010

 Copyright: See COPYING file that comes with this distribution

*/

#include "crane_split_item.h"

#include <cmath>
#include <algorithm>

#include <QPainter>

#include <ange/schedule/call.h>

#include <ange/containers/container.h>

#include <ange/vessel/slot.h>
#include <ange/vessel/vessel.h>
#include <ange/vessel/bayslice.h>

#include "document/stowage/stowage.h"
#include "document/stowage/crane_split.h"
#include "crane_split_item_bay.h"
#include "cranesplitbinlabelitem.h"
#include "gui/utils/painter_utils.h"

using ange::containers::Container;
using ange::schedule::Call;
using ange::vessel::BaySlice;

crane_split_item_t::crane_split_item_t(const stowage_t* stowage,
                                       const ange::schedule::Call* call,
                                       const ange::vessel::Vessel* vessel,
                                       QGraphicsItem* itemParent):
    AbstractVesselGraphicsItem(vessel, call, 3.0, itemParent),
    m_stowage(stowage),
    m_bounding_rect(boundingRect())
{
  set_vessel(vessel);
  connect(stowage->crane_split(), SIGNAL(calculations_done(const ange::schedule::Call*)), SLOT(updateHorizontalMarkers(const ange::schedule::Call*)));
  connect(stowage->crane_split(), SIGNAL(calculations_done(const ange::schedule::Call*)), SLOT(update_call(const ange::schedule::Call*)));
  connect(call, &Call::craneRuleChanged, this, &crane_split_item_t::refreshCraneSplitBinLabelMap);
  connect(call, &Call::twinLiftingChanged, this, &crane_split_item_t::refreshCraneSplitBinLabelMap);
  connect(call, &Call::cranesChanged, this, &crane_split_item_t::refreshCraneSplitBinLabelMap);
  connect(call, &Call::craneProductivityChanged, this, &crane_split_item_t::refreshCraneSplitBinLabelMap);
}

void crane_split_item_t::set_vessel(const ange::vessel::Vessel* vessel) {
  qDeleteAll(m_bays);
  m_bays.clear();
  Q_FOREACH(BaySlice* bay_slice, vessel->baySlices()) {
    crane_split_bay_t* crane_split = m_stowage->crane_split()->details(call(), bay_slice->joinedBay());
    m_bays << new crane_split_item_bay_t(bay_slice, crane_split);
  }
  std::reverse(m_bays.begin(), m_bays.end());
  qDeleteAll(m_bin_label_map.values());
  m_bin_label_map.clear();
  refreshCraneSplitBinLabelMap();
  AbstractVesselGraphicsItem::set_vessel(vessel);
}

void crane_split_item_t::refreshCraneSplitBinLabelMap() {
    QSet<const crane_split_bin_t*> newbins;
    Q_FOREACH(const crane_split_bin_t* bin, m_stowage->crane_split()->bins(m_call)) {
        newbins << bin;
    }
    QSet<const crane_split_bin_t*> oldbins = m_bin_label_map.keys().toSet();
    QSet<const crane_split_bin_t*> toremove = oldbins-newbins;
    QSet<const crane_split_bin_t*> tocreate = newbins-oldbins;
    Q_FOREACH(const crane_split_bin_t* bin, tocreate) {
        CraneSplitBinLabelItem* label = new CraneSplitBinLabelItem(this);
        m_bin_label_map.insert(bin,label);
    }
    Q_FOREACH(const crane_split_bin_t* bin, toremove) {
        CraneSplitBinLabelItem* label = m_bin_label_map.take(bin);
        label->hide();
        delete label;
    }
}


double crane_split_item_t::max_value() const {
  // Calculate maximum time used allocated to any crane
  double ncranes = m_stowage->crane_split()->no_cranes(m_call);
  if (ncranes < 1.0) {
    // Guard against ncranes = 0.0;
    ncranes = 1.0;
  }
  const double total_time = m_stowage->crane_split()->expected_total_crane_time(m_call);
  const double max_ideal_time_per_crane  = total_time / ncranes;
  double max_time = max_ideal_time_per_crane;
  Q_FOREACH(const crane_split_bin_t* bin, m_stowage->crane_split()->bins(m_call)) {
    max_time = std::max(bin->moveStatistics().time/bin->max_cranes(),max_time);
  }
  return max_time;
}

void crane_split_item_t::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) {
  AbstractVesselGraphicsItem::paint(painter,option,widget);
  // Calculate maximum time used allocatd to any crane
  double ncranes = m_stowage->crane_split()->no_cranes(m_call);
  if (ncranes < 1.0) {
    // Guard against ncranes = 0.0;
    ncranes = 1.0;
  }
  const double total_time = m_stowage->crane_split()->expected_total_crane_time(m_call);
  const double max_ideal_time_per_crane  = total_time / ncranes;
  const double max_time = max_value();

  const qreal crane_area_height = bottom_margin();
  QRectF coordinate_outer_rect = boundingRect();
  QRectF coordinate_inner_rect = draw_coordinate_system(painter, max_time);

  // Draw bay bars.
  const qreal bay_width = coordinate_inner_rect.width() / m_vessel->baySlices().size();
  const qreal bay_height = coordinate_outer_rect.bottom() - coordinate_inner_rect.top();
  const QSizeF bay_size(bay_width, bay_height-crane_area_height);
  QPointF bays_top_left(coordinate_inner_rect.left(), coordinate_inner_rect.top());
  QPointF bay_top_left(bays_top_left);
  Q_FOREACH(crane_split_item_bay_t* bay, m_bays) {
    bay->paint(painter, bay_area(bay->bay_slice()), max_time);
    bay_top_left += QPointF(bay_width, 0.0);
  }

  // Draw ideal limit using actual total time
  {
    double actual_ideal = m_stowage->crane_split()->actual_total_crane_time(m_call) / ncranes;
    const qreal top_line = coordinate_inner_rect.bottom() - (actual_ideal / max_time * coordinate_inner_rect.height());
    QLinearGradient grad(coordinate_inner_rect.left(), top_line, coordinate_inner_rect.right(), top_line);
    QColor ceiling_color(Qt::gray);
    grad.setColorAt(0, ceiling_color.darker(120));
    grad.setColorAt(1, ceiling_color.lighter(120));
    QPen pen(grad, 0.0, Qt::DashLine);
    painter->setPen(pen);
    painter->drawLine(QLineF(coordinate_inner_rect.left(), top_line, m_bounding_rect.right(), top_line));
  }

  // Draw ideal limit using expected total time
  {
    const qreal top_line = coordinate_inner_rect.bottom() - (max_ideal_time_per_crane / max_time * coordinate_inner_rect.height());
    QLinearGradient grad(coordinate_inner_rect.left(), top_line, coordinate_inner_rect.right(), top_line);
    QColor ceiling_color(Qt::green);
    grad.setColorAt(0, ceiling_color.darker(120));
    grad.setColorAt(1, ceiling_color.lighter(120));
    QPen pen(grad, 0.0, Qt::DashLine);
    painter->setPen(pen);
    painter->drawLine(QLineF(coordinate_inner_rect.left(), top_line, m_bounding_rect.right(), top_line));
  }

  // Finally, draw cranes.
  double time_for_crane = max_time;
  qreal crane_left_point = coordinate_inner_rect.left();
  qreal margin = 1.0;
  qreal crane_right_point = crane_left_point + bay_size.width()/2.0 - margin;
  painter->setPen(Qt::black);
  Q_FOREACH(crane_split_item_bay_t* bay, m_bays) {
    time_for_crane -= bay->total();
    if (time_for_crane < -0.001) {
      draw_bar_with_end_pieces(painter,
                               crane_left_point,
                               crane_right_point,
                               m_bounding_rect.bottom() - crane_area_height / 2.0,
                               QString::number(max_time, 'f', 1));
      time_for_crane += max_time;
      crane_left_point = crane_right_point + 2*margin;
    }
    crane_right_point += bay_size.width();
  }
  //last crane
  qreal right_end = m_bounding_rect.right();
  if(!m_vessel->baySlices().isEmpty()) {
    right_end = bay_area(m_vessel->baySlices().front()).right();
  }
  draw_bar_with_end_pieces(painter,
                           crane_left_point,
                           right_end,
                           m_bounding_rect.bottom() - crane_area_height / 2.0,
                           QString::number(max_time - time_for_crane, 'f', 1));
}

qreal crane_split_item_t::crane_split_bin_lengths_adjustments(crane_split_bin_include_t::crane_split_bin_includes_t include, qreal total_width)
{
  switch (include) {
    case crane_split_bin_include_t::FORE20_FOURTY_AFT20_BAYS : {
      return 0.0;
    }
    case crane_split_bin_include_t::FORE20_BAY : {
      return (2.0*total_width )/3.0;
    }
    case crane_split_bin_include_t::FORE20_FORTY_BAYS : {
      return total_width / 3.0;
    }
    case crane_split_bin_include_t::FOURTY_AFT20_BAYS : {
      return -total_width / 3.0;
    }
    case crane_split_bin_include_t::AFT20_BAY : {
      return -(2.0 * total_width) / 3.0;
    }
    case crane_split_bin_include_t::PURE_FOURTY_BAY : {
      return 0.0;
    }
    case crane_split_bin_include_t::PURE_TWENTY_BAY : {
      return 0.0;
    }
    case crane_split_bin_include_t::FORE20_AFT20_BAYS : {
      return 0.0;
    }
  }
  return 0.0;
}

crane_split_item_t::~crane_split_item_t() {
  qDeleteAll(m_bays);
}

void crane_split_item_t::updateHorizontalMarkers(const ange::schedule::Call* call) {
    if(m_call!=call) {
        return;
    }
    double ncranes = m_stowage->crane_split()->no_cranes(m_call);
    if (ncranes < 1.0) {
        // Guard against ncranes = 0.0;
        ncranes = 1.0;
    }
    const double total_time = m_stowage->crane_split()->expected_total_crane_time(m_call);
    const double max_ideal_time_per_crane  = total_time / ncranes;
    QRectF coordinate_inner_rect = coordinate_system_rect();

    //move the label items to the right location
    const qreal bay_width = coordinate_inner_rect.width() / m_vessel->baySlices().size();
    Q_FOREACH(const crane_split_bin_t* bin, m_stowage->crane_split()->bins(m_call)) {
        CraneSplitBinLabelItem* labelitem = m_bin_label_map.value(bin);
        Q_ASSERT(labelitem);
        crane_split_bin_include_t::crane_split_bin_includes_t from_include = bin->bays().back().second;
        crane_split_bin_include_t::crane_split_bin_includes_t to_include = bin->bays().front().second;
        QRectF to_bay_rect = QRectF();
        QRectF from_bay_rect = QRectF();
        for (int i = 0; i<m_bays.size(); ++i) {
            if (m_bays[i]->crane_split_bay() == bin->bays().front().first) {
                to_bay_rect = bay_area(m_bays[i]->bay_slice());
            }
            if (m_bays[i]->crane_split_bay() == bin->bays().back().first) {
                from_bay_rect = bay_area(m_bays[i]->bay_slice());
            }
        }
        Q_ASSERT(to_bay_rect.isValid());
        Q_ASSERT(from_bay_rect.isValid());
        //FIXME: #1248
        //Q_ASSERT(from_bay_rect.left() <= to_bay_rect.left());

        // Draw crane work area
        crane_split_bay_t::MoveStatistics movecount = bin->moveStatistics();
        const qreal height = movecount.time/bin->max_cranes();
        if (height > max_ideal_time_per_crane * 1.05) {
            labelitem->setPen(QPen(Qt::red));
            labelitem->setVisible(true);
        } else if (height > max_ideal_time_per_crane * 0.95) {
            labelitem->setPen(QPen(QColor(255,128,0)));
            labelitem->setVisible(true);
        } else if (height > max_ideal_time_per_crane * 0.05) {
            labelitem->setPen(QPen(Qt::gray));
            labelitem->setVisible(true);
        } else {
            labelitem->setVisible(false);
            continue; // Skip very low shadows
        }

        qreal from_adjustment = crane_split_bin_lengths_adjustments(from_include,bay_width);
        qreal to_adjustment = crane_split_bin_lengths_adjustments(to_include,bay_width);


        labelitem->setToolTip(QString("%1 hours at %8 moves/hour<br>%2 moves of 40' containers<br>"
                                "%3 moves of 20' containers<br>%4 moves in total<br>%5 twinlifts<br>"
                                "%6 total ffe calculated moves<br>Crane split: max %7 cranes").arg(QString::number(height,'f',1))
                                .arg(movecount.fourty).arg(movecount.twenty).arg(QString::number(movecount.total,'f',0))
                                .arg(movecount.twinlifts).arg(movecount.totalffe).arg(QString::number(total_time / height,'f',1))
                                .arg(m_call->craneProductivity()) );
        labelitem->setPos(from_bay_rect.left()+from_adjustment,coordinate_inner_rect.bottom() - (height / max_value() * coordinate_inner_rect.height())-4);
        labelitem->setSize(QSizeF(to_bay_rect.right()+to_adjustment-(from_bay_rect.left()+from_adjustment),8));
    }
    update();
}

void crane_split_item_t::draw_bar_with_end_pieces(QPainter* painter, qreal x1, qreal x2, qreal y, const QString& label) {
  QTransform transform = painter->transform();
  painter->resetTransform();
  QPointF left_global = transform.map(QPointF(x1,y));
  QPointF right_global = transform.map(QPointF(x2, y));
  int global_x1 = qRound(left_global.x());
  int global_x2 = qRound(right_global.x());
  int global_y = qRound(left_global.y());
  painter->drawLine(global_x1, global_y, global_x2, global_y);
  painter->drawLine(global_x1, global_y-1, global_x1, global_y+1);
  painter->drawLine(global_x2, global_y-1, global_x2, global_y+1);
  painter->setTransform(transform);
  if (!label.isEmpty()) {
    ange::painter_utils::draw_text_in_rect(painter, QRectF(x1, y-8, x2-x1, 8), label, Qt::AlignHCenter | Qt::AlignBottom);
  }
}

#include "crane_split_item.moc"
