/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010

 Copyright: See COPYING file that comes with this distribution

*/

#ifndef CRANESPLIT_ITEM_H
#define CRANESPLIT_ITEM_H

#include <QGraphicsItem>
#include "gui/common/abstractvesselgraphicsitem.h"
#include "document/stowage/crane_split_bin_include.h"

class CraneSplitBinLabelItem;
class crane_split_bin_t;
class crane_split_item_bay_t;
namespace ange {
namespace containers {
class Container;
}
namespace schedule {
class Call;
}
namespace vessel {
class Vessel;
}
}
class stowage_t;

class crane_split_item_t : public AbstractVesselGraphicsItem {
  Q_OBJECT
  Q_INTERFACES(QGraphicsItem)
  public:
    /**
     * @param stowage
     * @param call to show crane_split for
     * @param vessel for bays etc.
     * No ownership is claimed.
     */
    crane_split_item_t(const stowage_t* stowage,
                      const ange::schedule::Call* call,
                      const ange::vessel::Vessel* vessel,
                      QGraphicsItem* itemParent = 0);

    virtual ~crane_split_item_t();
    virtual void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget = 0);

    virtual double max_value() const;
  public Q_SLOTS:
    virtual void set_vessel(const ange::vessel::Vessel* vessel);

  private Q_SLOTS:
    /**
     * updates the horizontal markers; position, color, tooltip etc.
     * \param call the call to do the update for.
     */
    void updateHorizontalMarkers(const ange::schedule::Call* call);

    /**
     * updates the m_bin_label_map with relations between labels and bins involved in
     * this call
     */
    void refreshCraneSplitBinLabelMap();
  private:
    const stowage_t* m_stowage;
    QRectF m_bounding_rect;
    QList<crane_split_item_bay_t*> m_bays;
    QHash<const crane_split_bin_t*, CraneSplitBinLabelItem*> m_bin_label_map;

    void draw_bar_with_end_pieces(QPainter* painter, qreal x1, qreal x2, qreal y, const QString& label = QString());
    qreal crane_split_bin_lengths_adjustments(crane_split_bin_include_t::crane_split_bin_includes_t include, qreal total_width);
};

#endif // CRANESPLIT_ITEM_H
