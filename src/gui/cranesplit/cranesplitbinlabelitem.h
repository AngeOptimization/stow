#ifndef CRANESPLITBINLABELITEM_H
#define CRANESPLITBINLABELITEM_H

#include <QGraphicsObject>
#include <QPen>


/**
 * \brief shows the label on a crane split item
 */

class CraneSplitBinLabelItem : public QGraphicsItem {
    public:
        CraneSplitBinLabelItem(QGraphicsItem* parent = 0);
        virtual void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* );
        virtual QRectF boundingRect() const;
        void setSize(const QSizeF& size);
        void setPen(const QPen& pen);
    private:
        QSizeF m_size;
        QPen m_pen;


};

#endif // CRANESPLITBINLABELITEM_H
