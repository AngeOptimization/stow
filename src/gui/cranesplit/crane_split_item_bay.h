/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010

 Copyright: See COPYING file that comes with this distribution

*/

#ifndef CRANESPLIT_BAY_H
#define CRANESPLIT_BAY_H
#include <QtGlobal>

class crane_split_bay_t;
class QBrush;
class QRectF;
namespace ange {
namespace vessel {
class BaySlice;
}
}

class QPainter;
class crane_split_item_bay_t {
  public:
    crane_split_item_bay_t(const ange::vessel::BaySlice* bay, crane_split_bay_t* crane_split);

    void paint(QPainter* painter, const QRectF& bounds, double ceiling);

    /**
     * Return the total number of hours
     */
    double total() const;

    /**
     * Return the max number of hours drawn
     */
    double max_drawn() const;

    /**
     * Change the underlying crane_split
     */
    void set_crane_split(crane_split_bay_t* crane_split);

    /**
     * @return bay_slice
     */
    const ange::vessel::BaySlice* bay_slice() const {
      return m_bay_slice;
    }

    /**
     * @return crane_split_bay
     */
    const crane_split_bay_t* crane_split_bay() const {
      return m_crane_split;
    }

    /**
    * With of margin for footer
    */
    double footer_margin(const QRectF& bounds) const;

  private:
    const ange::vessel::BaySlice* m_bay_slice;
    const crane_split_bay_t* m_crane_split;

    struct durations_t {
      double m_load;
      double m_discharge;
      double m_restow;

      durations_t();
    };

    void draw_bar(QPainter* painter, const QRectF& bounds, const durations_t& durations, double ceiling);

};

#endif // CRANESPLIT_BAY_H
