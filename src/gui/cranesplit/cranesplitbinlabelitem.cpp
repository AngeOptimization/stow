#include "cranesplitbinlabelitem.h"
#include <QPainter>
#include <QStyleOption>


CraneSplitBinLabelItem::CraneSplitBinLabelItem(QGraphicsItem* parent): QGraphicsItem(parent), m_size(10,10){
    hide();
}


void CraneSplitBinLabelItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget*)
{
    painter->setPen(m_pen);
    painter->drawLine(option->rect.bottomLeft(), option->rect.topLeft());
    painter->drawLine(option->rect.bottomRight(), option->rect.topRight());
    qreal mid = (option->rect.bottom()+option->rect.top())/2;
    painter->drawLine(QPointF(option->rect.left(),mid),QPointF(option->rect.right(),mid));
}

QRectF CraneSplitBinLabelItem::boundingRect() const
{
    return QRectF(QPointF(0,0),m_size);
}

void CraneSplitBinLabelItem::setSize(const QSizeF& size)
{
    prepareGeometryChange();
    m_size = size;
}

void CraneSplitBinLabelItem::setPen(const QPen& pen)
{
    m_pen = pen;
}



