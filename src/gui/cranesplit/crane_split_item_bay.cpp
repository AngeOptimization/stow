/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010

 Copyright: See COPYING file that comes with this distribution

*/

#include "crane_split_item_bay.h"

#include <ange/vessel/bayslice.h>

#include <QPainter>

#include <cmath>

#include "gui/utils/painter_utils.h"
#include "document/stowage/crane_split_bay.h"
#include "document/stowage/crane_split.h"

using namespace ange::painter_utils;

crane_split_item_bay_t::crane_split_item_bay_t(const ange::vessel::BaySlice* bay, crane_split_bay_t* crane_split) :
    m_bay_slice(bay),
    m_crane_split(crane_split)
{
}

double crane_split_item_bay_t::total() const {
  return m_crane_split->total_time();
}

double crane_split_item_bay_t::max_drawn() const {
  crane_split_t* cs = m_crane_split->crane_split();
  double twinlift_prod = cs->twinlift_productivity(m_crane_split->call());
  double single40_prod = cs->singlelift_productivity(m_crane_split->call());
  double max = 0.0;
  const crane_split_bay_dir_t& di = m_crane_split->discharge();
  const crane_split_bay_dir_t& lo = m_crane_split->load();
  const crane_split_bay_dir_t& rs = m_crane_split->restow();
  max = std::max(max, (di.fourty().above+di.fourty().below+lo.fourty().above+lo.fourty().below+
                rs.fourty().above+rs.fourty().below)*single40_prod);
  max = std::max(max, (di.twentyFore().above + di.twentyFore().below+lo.twentyFore().above+
  lo.twentyFore().below+rs.twentyFore().above+rs.twentyFore().below)*twinlift_prod);
  max = std::max(max, (di.twentyAft().above+di.twentyAft().below+lo.twentyAft().above+
  lo.twentyAft().below+rs.twentyAft().above+rs.twentyAft().below)*twinlift_prod);

  return max;
}

double crane_split_item_bay_t::footer_margin(const QRectF& bounds) const {
  return bounds.width() / 10.0;
}

void crane_split_item_bay_t::paint(QPainter* painter, const QRectF& bounds, double ceiling) {
  qreal bar_margin = 2.0;
  QRectF bar_bounds(bounds.left()+bar_margin, bounds.top(), (bounds.width() - 3*bar_margin)/3.0, bounds.height());
  crane_split_t* cs = m_crane_split->crane_split();
  double twinlift_prod = cs->twinlift_productivity(m_crane_split->call());
  double single40_prod = cs->singlelift_productivity(m_crane_split->call());
  durations_t timings;
//TODO MARTIN fix the twinlift based on cabability
  // Draw 20fore bars
  timings.m_discharge = (m_crane_split->discharge().twentyFore().above +  m_crane_split->discharge().twentyFore().below) * twinlift_prod;
  timings.m_load = (m_crane_split->load().twentyFore().above + m_crane_split->load().twentyFore().below) * twinlift_prod;
  timings.m_restow =( m_crane_split->restow().twentyFore().above + m_crane_split->restow().twentyFore().below) * twinlift_prod;
  draw_bar(painter, bar_bounds, timings, ceiling);

  // Draw 4x bars
  timings.m_discharge = (m_crane_split->discharge().fourty().above + m_crane_split->discharge().fourty().below)  * single40_prod;
  timings.m_load = (m_crane_split->load().fourty().above+m_crane_split->load().fourty().below) * single40_prod;
  timings.m_restow = (m_crane_split->restow().fourty().above+m_crane_split->restow().fourty().below) * single40_prod;
  bar_bounds.moveLeft(bar_bounds.right()+1*bar_margin);
  draw_bar(painter, bar_bounds, timings, ceiling);

  // Draw 20aft bats
  timings.m_discharge = (m_crane_split->discharge().twentyAft().above+m_crane_split->discharge().twentyAft().below)  * twinlift_prod;
  timings.m_load = (m_crane_split->load().twentyAft().above+m_crane_split->load().twentyAft().below) * twinlift_prod;
  timings.m_restow = (m_crane_split->restow().twentyAft().above+m_crane_split->restow().twentyAft().below) * twinlift_prod;
  bar_bounds.moveLeft(bar_bounds.right()+1*bar_margin);
  draw_bar(painter, bar_bounds, timings, ceiling);

}

void crane_split_item_bay_t::draw_bar(QPainter* painter,
                                      const QRectF& bounds,
                                      const crane_split_item_bay_t::durations_t& durations,
                                      double ceiling) {
  qreal bottom = bounds.bottom();
  qreal height = durations.m_load/ceiling*bounds.height();
  painter->setPen(Qt::black);
  if (height > 0.001) {
    QLinearGradient grad(bounds.left()+bounds.width()/2.0,
                         bottom - height,
                         bounds.left()+bounds.width()/2.0,
                         bottom);
    grad.setColorAt(0.0, QColor(Qt::red).lighter());
    grad.setColorAt(1.0, Qt::red);
    painter->setBrush(grad);
    painter->drawRect(QRectF(bounds.left(), bottom - height, bounds.width(), height));
    bottom -= height;
  }
  height = durations.m_restow/ceiling*bounds.height();
  if (height > 0.001) {
    QLinearGradient grad(bounds.left()+bounds.width()/2.0,
                         bottom - height,
                         bounds.left()+bounds.width()/2.0,
                         bottom);
    grad.setColorAt(0.0, QColor(Qt::cyan).lighter());
    grad.setColorAt(1.0, Qt::cyan);
    painter->setBrush(grad);
    painter->drawRect(QRectF(bounds.left(), bottom - height, bounds.width(), height));
    bottom -= height;
  }
  height = durations.m_discharge/ceiling*bounds.height();
  if (height > 0.0001) {
    QLinearGradient grad(bounds.left()+bounds.width()/2.0,
                         bottom - height,
                         bounds.left()+bounds.width()/2.0,
                         bottom);
    grad.setColorAt(0.0, QColor(Qt::blue).lighter());
    grad.setColorAt(1.0, Qt::blue);
    painter->setBrush(grad);
    painter->drawRect(QRectF(bounds.left(), bottom - height, bounds.width(), height));
  }
}

crane_split_item_bay_t::durations_t::durations_t() : m_load(0.0), m_discharge(0.0), m_restow(0.0 ){

}

void crane_split_item_bay_t::set_crane_split(crane_split_bay_t* crane_split) {
  m_crane_split = crane_split;
}
