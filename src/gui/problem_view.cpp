#include "problem_view.h"
#include <stowplugininterface/problem.h>
#include <ange/vessel/stacksupport.h>
#include <ange/vessel/stack.h>
#include <ange/schedule/call.h>
#include <QMenu>
#include <QDebug>

using ange::angelstow::Problem;
using ange::angelstow::ProblemLocation;

problem_view_t::problem_view_t(QWidget* parent)
 : QTreeView(parent)
{
  m_delay_resize_timer.setSingleShot(true);
  m_delay_resize_timer.setInterval(200);
  setContextMenuPolicy(Qt::CustomContextMenu);
  connect(&m_delay_resize_timer,SIGNAL(timeout()),SLOT(really_resize_all_columsn_to_contents()));
  connect(this, SIGNAL(expanded(QModelIndex)), SLOT(resize_all_columns_to_contents()));
  connect(this, SIGNAL(collapsed(QModelIndex)), SLOT(resize_all_columns_to_contents()));
  connect(this, SIGNAL(customContextMenuRequested(QPoint)), SLOT(contextMenuRequested(QPoint)));
  connect(this, SIGNAL(doubleClicked(QModelIndex)), SLOT(onDoubleClick(QModelIndex)));

}

void problem_view_t::setModel(QAbstractItemModel* model)
{
  if (this->model()) {
    disconnect(this->model(), SIGNAL(rowsInserted(const QModelIndex, int, int)), this, SLOT(resize_all_columns_to_contents()));
    disconnect(this->model(), SIGNAL(rowsRemoved(const QModelIndex, int, int)), this, SLOT(resize_all_columns_to_contents()));
  }
  connect(model, SIGNAL(rowsInserted(const QModelIndex, int, int)), SLOT(resize_all_columns_to_contents()));
  connect(model, SIGNAL(rowsRemoved(const QModelIndex, int, int)), SLOT(resize_all_columns_to_contents()));
  QTreeView::setModel(model);
}

void problem_view_t::resize_all_columns_to_contents() {
  m_delay_resize_timer.start();
}


void problem_view_t::really_resize_all_columsn_to_contents() {
  if (!model()) {
    return;
  }
  for (int c =0, nc=model()->columnCount(); c < nc; ++c) {
    resizeColumnToContents(c);
  }
}

void problem_view_t::contextMenuRequested(const QPoint& pos) {
    QModelIndex idx = indexAt(pos);
    if(!idx.isValid()) {
        return;
    }
    const Problem* problem = problemForModelIndex(idx);
    if(!problem) {
        return;
    }
    QMenu m;

    QAction* zoomToProblemLocation = 0;
    if(canZoomToProblem(problem)) {
        zoomToProblemLocation = m.addAction("Zoom to problem location");
    }
    // possibly add more actions here.
    if(m.actions().size() == 0) {
        return;
    }
    QAction* selected = m.exec(mapToGlobal(pos));
    if(!selected) {
        return;
    }
    if(selected == zoomToProblemLocation) {
        emit zoomToProblem(problem);
    }
}

bool problem_view_t::canZoomToProblem(const Problem* problem) {
    if(!problem) {
        return false;
    }
    ProblemLocation problem_location = problem->location();
    return problem_location.call() && ( problem_location.stackSupport() || problem_location.bayRowTier().placed());
}

const Problem* problem_view_t::problemForModelIndex(const QModelIndex& idx) {
    return idx.data(Qt::UserRole).value<Problem*>();
}

void problem_view_t::onDoubleClick(QModelIndex index) {
    const Problem* problem = problemForModelIndex(index);
    if(!problem) {
        return;
    }
    if(canZoomToProblem(problem)) {
        emit zoomToProblem(problem);
    }

}





#include "problem_view.moc"
