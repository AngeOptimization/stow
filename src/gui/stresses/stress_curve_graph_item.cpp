#include "stress_curve_graph_item.h"

#include "bayweightlimits.h"
#include "document.h"
#include "painter_utils.h"
#include "stresses.h"
#include "svg_graphics_item.h"
#include "tankconditions.h"
#include "userconfiguration.h"
#include "weight_limit_command.h"

#include <ange/containers/container.h>
#include <ange/schedule/call.h>
#include <ange/vessel/bayslice.h>
#include <ange/vessel/slot.h>
#include <ange/vessel/stack.h>
#include <ange/vessel/vessel.h>

#include <QApplication>
#include <QGraphicsSceneMouseEvent>
#include <QMenu>
#include <QPainter>

using namespace ange::angelstow;
using namespace ange::units;
using ange::vessel::BaySlice;
using ange::vessel::Slot;
using ange::vessel::Stack;
using ange::containers::Container;
using ange::vessel::Vessel;

const qreal STRETCH_FACTOR = 2.0; // How many pixels per meter

StressCurveGraphItem::StressCurveGraphItem(document_t* document,
                                 ange::schedule::Call* call,
                                 QGraphicsItem* parent)
  : AbstractVesselGraphicsItem(document->vessel(), call, 0.0, parent),
  m_document(document),
  m_stowage(document->stowage()),
  m_macro_stowage(document->macro_stowage()),
  m_max_weight(0.0)
{
    reset();
    connect(m_stowage, &stowage_t::container_changed_position, this, &StressCurveGraphItem::container_changed_position);
    connect(m_document->bayWeightLimits(), &BayWeightLimits::bayWeightLimitChanged, this, &StressCurveGraphItem::upper_weight_limit_changed);
    connect(m_stowage, &stowage_t::stowageChanged, this, &StressCurveGraphItem::reset);
    connect(m_document->stresses_bending(), &Stresses::stresses_changed, this, &StressCurveGraphItem::tank_condition_chanced);
    connect(m_document, &document_t::vessel_changed, this, &StressCurveGraphItem::reset_after_vessel_import);
}

void StressCurveGraphItem::reset() {
  Q_FOREACH(const ange::vessel::BaySlice* slice, m_document->vessel()->baySlices()) {
    bay_data_t& bay_data = m_bay_data[slice];
    bay_data.micro_weight = 0.0*kilogram;
    bay_data.weight = 0.0*kilogram;
  }
  Q_FOREACH(const Container* container, m_stowage->get_containers_on_board(m_call)) {
    const Slot* pos = m_stowage->container_position(container, m_call);
    BaySlice* slice = pos->stack()->baySlice();
    bay_data_t& bay_data = m_bay_data[slice];
    bay_data.weight += container->weight();
    if (m_stowage->contents(pos, m_call).type() == MicroStowedType) {
      bay_data.micro_weight += container->weight();
    }
  }
  recalculate_max_weight();
  update();
}

double StressCurveGraphItem::max_value() const {
  return m_max_weight/ton;
}

void StressCurveGraphItem::draw_bar(QPainter* painter, const QRectF& bar, QColor main_color, QColor faded_color) {
  QLinearGradient grad(bar.bottomLeft(), bar.topLeft());
  grad.setColorAt(1.0, main_color);
  grad.setColorAt(0.0, faded_color);
  painter->setBrush(grad);
  painter->setPen(Qt::NoPen);
  painter->drawRect(bar);
}

void StressCurveGraphItem::draw_bay(QPainter* painter, ange::vessel::BaySlice* slice, const QRectF& bounding_rect, double max_value) const {
  const bay_data_t data = m_bay_data.value(slice);
  Mass weight_max = max_value * ton;
  const qreal micro_top = bounding_rect.bottom() - (bounding_rect.bottom() - bounding_rect.top())*data.micro_weight/weight_max;
  const qreal macro_top = bounding_rect.bottom() - (bounding_rect.bottom() - bounding_rect.top())*data.weight/weight_max;
  draw_bar(painter, QRectF(bounding_rect.left(), micro_top, bounding_rect.width(), (bounding_rect.bottom() - micro_top)), QColor(192, 192, 192), QColor(222, 222,222));
  if (macro_top < micro_top + 1e-6) {
    draw_bar(painter, QRectF(bounding_rect.left(), macro_top, bounding_rect.width(), (micro_top - macro_top)), QColor(192,255,192), QColor(222,255,222));
  }
}

QPair<int, int> StressCurveGraphItem::draw_stress_curve(QPainter* painter, const QVector<QPointF>& stress_curve, const QColor& color)
{
  QVector<QPointF> graph = stress_curve;
  if(graph.isEmpty()){
    return QPair<int, int>(0,0);
  }
  QVector<QPointF> max_limits;
  QVector<QPointF> min_limits;
  double span = int(std::log10(100));
  double ymax = std::ceil(100/std::pow(10,span));
  QRectF graph_rect(graph.first().x(), -ymax*std::pow(10.,span),  graph.last().x() - graph.first().x(), 2.0*ymax*std::pow(10,span));
  QTransform transform = get_transform_for_graph_rect(graph_rect);
  QPen limits_pen(Qt::gray, 0, Qt::DotLine);
  QPen axispen(Qt::black, 0, Qt::DashLine);
  QPen curvepen(color, 2, Qt::SolidLine);
  curvepen.setCosmetic(true);
  painter->setTransform(transform, true);
  painter->setClipRect(graph_rect);
  painter->setPen(axispen);
  painter->drawLine(QPointF(graph.first().x(),0.0), QPointF(graph.last().x(),0.0));
  painter->setPen(curvepen);
  painter->setRenderHint(QPainter::Antialiasing);
  painter->drawPolyline(graph.data(), graph.size());
  painter->setPen(limits_pen);
  painter->drawLine(QPointF(graph.first().x(), 100), QPointF(graph.last().x(), 100));
  painter->drawLine(QPointF(graph.first().x(), -100), QPointF(graph.last().x(), -100));
  painter->setTransform(transform.inverted(),true);
  return QPair<int, int>(span, ymax);
}

void StressCurveGraphItem::container_changed_position(const ange::containers::Container* container,
                                                 const ange::vessel::Slot* from,
                                                 const ange::vessel::Slot* to,
                                                 const ange::schedule::Call* call,
                                                 StowType old_type,
                                                 StowType new_type) {
  if (call == m_call) {
    if (from) {
      BaySlice* slice = from->stack()->baySlice();
      bay_data_t& bay_data = m_bay_data[slice];
      bay_data.weight -= container->weight();
      if (old_type == MicroStowedType) {
        bay_data.micro_weight -= container->weight();
        set_upper_range_range(slice);
      }
    }
    if (to) {
      BaySlice* slice = to->stack()->baySlice();
      bay_data_t& bay_data = m_bay_data[slice];
      bay_data.weight += container->weight();
      if (new_type == MicroStowedType) {
        bay_data.micro_weight += container->weight();
        set_upper_range_range(slice);
      }
    }
  }
  update();
}

void StressCurveGraphItem::contextMenuEvent(QGraphicsSceneContextMenuEvent* event) {
  if (BaySlice* slice = bay_slice_for_pos(event->pos())) {
    const int bay = slice->joinedBay();
    QMenu menu;
    const bool bay_has_limit = m_bay_data.value(slice).upper_bound < std::numeric_limits<double>::infinity() * kilogram;
    QAction* limit_weight = menu.addAction((bay_has_limit ? tr("Remove weight limit in bay %1") : tr("Limit weight in bay %1")).arg(bay));
    QAction* selected = menu.exec(event->screenPos());
    if (selected == limit_weight) {
      if (!bay_has_limit) {
        add_weight_limit_for_bay(slice, event->pos());
      } else {
        delete_weight_limit_for_bay(slice);
      }
    }
  }
}

void StressCurveGraphItem::upper_weight_limit_changed(const ange::schedule::Call* call, const BaySlice* bay_slice, const ange::units::Mass value) {
  if (call == m_call) {
    bay_data_t& data  = m_bay_data[bay_slice];
    if (std::isinf(value / kilogram)) {
      data.upper_bound = value;
      delete data.graphics_item;
      data.graphics_item = 0L;
      data.upper_bound = value;
      emit upper_limit_removed(bay_slice->joinedBay());
    } else {
      QRectF area = bay_area(bay_slice);
      if (std::isinf(data.upper_bound / kilogram)) {
        svg_graphics_item_t* weight_limit_item = new svg_graphics_item_t(":/widgets/widgets/upper_limit.svg", QRectF(0.0,0.0,area.width(),area.width()), this);
        weight_limit_item->set_hotspot(QPointF(area.width()/2.0, area.width()-1.0)); // Middle, bottom, up 1 pixel or so.
        bay_data_t& data  = m_bay_data[bay_slice];
        data.graphics_item = weight_limit_item;
        set_upper_range_range(bay_slice);
        emit upper_limit_added(bay_slice->joinedBay());
        connect(weight_limit_item,  SIGNAL(item_position_changed(QGraphicsItem*,QPointF)), SLOT(upper_limit_item_changed(QGraphicsItem*,QPointF)));
      }
      if (std::fabs(value/kilogram - data.upper_bound/kilogram)>1e-3) {
        data.upper_bound =  value;
        QRectF area = bay_area(bay_slice);
        const qreal y = area.bottom() - area.height()*data.upper_bound/m_max_weight;
        data.graphics_item->setPos(area.left()+area.width()/2.0,y);
        emit upper_limit_set(bay_slice->joinedBay(), data.upper_bound/kilogram);
      }
    }
  }
}

void StressCurveGraphItem::add_weight_limit_for_bay(ange::vessel::BaySlice* bay_slice, QPointF position) {
  QRectF area = bay_area(bay_slice);
  const double initial_value = (area.bottom() - position.y())/area.height() * max_value() * 1000.0;
  QUndoCommand* cmd = weight_limit_command_t::add(m_document, bay_slice, m_call, initial_value);
  m_document->undostack()->push(cmd);
}

void StressCurveGraphItem::delete_weight_limit_for_bay(ange::vessel::BaySlice* bay_slice) {
  QUndoCommand* cmd = weight_limit_command_t::remove(m_document, bay_slice, m_call);
  m_document->undostack()->push(cmd);
}

void StressCurveGraphItem::set_weight_limit_for_bay(const ange::vessel::BaySlice* bay_slice, const QPointF& position) {
  QRectF area = bay_area(bay_slice);
  const double value = (area.bottom() - position.y())/area.height() * max_value() * 1000.0;
  QUndoCommand* cmd = weight_limit_command_t::set(m_document, bay_slice, m_call, value);
  m_document->undostack()->push(cmd);
}

void StressCurveGraphItem::upper_limit_item_changed(QGraphicsItem* item, QPointF position) {
  for (bay_datas_t::iterator it = m_bay_data.begin(), iend = m_bay_data.end(); it != iend; ++it) {
    if (it->graphics_item == item) {
      set_weight_limit_for_bay(it.key(), position);
    }
  }
}

void StressCurveGraphItem::set_upper_range_range(const ange::vessel::BaySlice* bay_slice) {
  const bay_data_t& bay_data = m_bay_data.value(bay_slice);
  if (bay_data.graphics_item) {
    QRectF area = bay_area(bay_slice);
    const qreal micro_height = area.height()/max_value()*bay_data.micro_weight/ton;
    bay_data.graphics_item->allow_move(QRectF(area.left()+area.width()/2.0, area.top(), 0.0, area.height()-micro_height));
  }
}

void StressCurveGraphItem::recalculate_max_weight() {
  Mass max_weight = 1.0*kilogram;
  Q_FOREACH(BaySlice* slice, m_vessel->baySlices()) {
    Mass max_bay_weight = 0.0*kilogram;
    Q_FOREACH(Stack* stack, slice->stacks()) {
      max_bay_weight += stack->maximumTotalWeight();
      if(stack->maximumTotalWeight()==0.0*kilogram){
        max_bay_weight += stack->tiers().size()*40000.0*kilogram;
      }
    }
    max_weight = qMax(max_bay_weight, max_weight);
  }
  if (m_max_weight < max_weight) {
    // Reset positions
    for (bay_datas_t::iterator it = m_bay_data.begin(), iend = m_bay_data.end(); it != iend; ++it) {
      const bay_data_t& bay_data = it.value();
      if (bay_data.graphics_item) {
        QRectF area = bay_area(it.key());
        qreal y = area.bottom() - area.height()*bay_data.upper_bound/max_weight;
        bay_data.graphics_item->setPos(bay_data.graphics_item->pos().x(), y);
      }
    }
  }
  m_max_weight = max_weight;
}

void StressCurveGraphItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
  AbstractVesselGraphicsItem::paint(painter, option, widget);
  draw_stress_curve(painter, m_document->stresses_bending()->relativeBendingCurve(call()), Qt::blue);
  draw_stress_curve(painter, m_document->stresses_bending()->relativeShearForceCurve(call()), Qt::red);
  draw_stress_curve(painter, m_document->stresses_bending()->relativeTorsionCurve(call()), Qt::yellow);
  draw_coordinateaxis_for_stresscurves(painter);
}

QRectF StressCurveGraphItem::bay_area ( const ange::vessel::BaySlice* bay_slice ) const
{
  QRectF rv = AbstractVesselGraphicsItem::bay_area ( bay_slice );
  if (m_document->vessel()->hasInfo(Vessel::HullShape)){
    qreal last_bayslice = m_vessel->baySlices().back()->stacks().front()->bottom().l()/meter;
    qreal translation = (last_bayslice - m_document->stresses_bending()->rearPerpendicular()/meter)*STRETCH_FACTOR;
    rv.adjust( translation,0,translation,0);
  }
  return rv;
}

ange::vessel::BaySlice* StressCurveGraphItem::bay_slice_for_pos ( const QPointF& pos ) const
{
  if (m_document->vessel()->hasInfo(Vessel::HullShape)){
    QRectF coordinate_rect = coordinate_system_rect();
    if (coordinate_rect.contains(pos)) {
      Q_FOREACH(ange::vessel::BaySlice* bay_slice, m_document->vessel()->baySlices()){
        if(bay_area(bay_slice).contains(pos)){
          return bay_slice;
        }
      }
    }
    return 0L;
  } else {
    return AbstractVesselGraphicsItem::bay_slice_for_pos(pos);
  }
}

void StressCurveGraphItem::draw_coordinateaxis_for_stresscurves(QPainter* painter)
{
    if (m_document->vessel()->hasInfo(Vessel::HullShape)){
        painter->setClipRect(right_margin());
        painter->setPen(Qt::black);
        draw_tickmarks_for_stresscurves(painter);
        painter->setClipping(false);
    }
}

void StressCurveGraphItem::draw_tickmarks_for_stresscurves(QPainter* painter)
{
  //FIXME: drawing the coordinate system should be handled automatically somewhere else, e.g. by the AbstractVesselGraphicsItem, #1466
  QRect unscaled_label_bounding_box = qApp->fontMetrics().boundingRect(QString("999e99 Nm"));
  const qreal label_width = unscaled_label_bounding_box.width() / 0.8 / sceneTransform().m11() + 2;
  painter->drawLine(QLine(right_margin().left()+1, coordinate_system_rect().top(), right_margin().left()+1, coordinate_system_rect().bottom()));
  for(int tick = 0; tick < 5; ++tick) {
    qreal tick_height = (coordinate_system_rect().bottom() - coordinate_system_rect().top())*tick/4.0 + coordinate_system_rect().top();
    painter->drawLine(QLineF(right_margin().left(), tick_height, right_margin().left() + 2, tick_height));
    QString label = QString("%1%").arg(100 - 2*100*tick/(5-1));
    QRectF label_box(right_margin().left()+8, tick_height-8, label_width - 2, 16);
    ange::painter_utils::draw_text_in_rect(painter, label_box, 0.8, label, Qt::AlignLeft | Qt::AlignVCenter);
  }
}

QRectF StressCurveGraphItem::coordinate_system_rect() const
{
  QRectF bounding_rect = boundingRect();
  QRectF unscaled_label_bounding_box = qApp->fontMetrics().boundingRect(widest_y_label());
  const qreal bay_label_height = (2+unscaled_label_bounding_box.height()) * 0.8 / sceneTransform().m22();
  const qreal label_width = unscaled_label_bounding_box.width() / 0.8 / sceneTransform().m11() + 2;
  qreal bottom_y_axis = bounding_rect.bottom() - bay_label_height;
  qreal top_y_axis = bounding_rect.top() + bay_label_height / 2;
  qreal x_y_axis = bounding_rect.left() + label_width;
  QRectF rv(x_y_axis, top_y_axis, bounding_rect.width()-label_width-right_margin_width(), bottom_y_axis - top_y_axis);
  return rv;
}

const QRectF StressCurveGraphItem::right_margin()
{
  return QRectF(boundingRect().right()-right_margin_width(),boundingRect().top(),right_margin_width(),boundingRect().height());
}

QRectF StressCurveGraphItem::boundingRect() const
{
  QList<BaySlice*> bay_slices = m_vessel->baySlices();
  qreal width = 0.0;
  if (!bay_slices.isEmpty()) {
    // here we find a sensible size of the graph canvas. It could have been just some fixed size.
    const qreal label_width = qApp->fontMetrics().width(widest_y_label());
    qreal graph_width_from_perpendiculars = 0.0;
    if (m_document->vessel()->hasInfo(Vessel::HullShape)){
      graph_width_from_perpendiculars = std::fabs(m_document->stresses_bending()->forePerpendicular()/meter
        - m_document->stresses_bending()->rearPerpendicular()/meter)*STRETCH_FACTOR;
    }
    qreal graphic_view_from_bay_slices = 0.0;
    if (!bay_slices.isEmpty()) {
      graphic_view_from_bay_slices = std::fabs((vessel_fore()-vessel_aft())/meter*STRETCH_FACTOR);
    }
    const qreal graph_width = std::max(graph_width_from_perpendiculars,graphic_view_from_bay_slices);
    width = label_width + graph_width + right_margin_width();
  }
  const qreal height = 200.0;
  return QRectF(0.0, 0.0, width, height);
}

qreal StressCurveGraphItem::right_margin_width() const
{
  qreal width = 0.0;
  if (m_document->vessel()->hasInfo(Vessel::HullShape)){
    QRectF unscaled_stress_axix_bounding_box = qApp->fontMetrics().boundingRect(QString("999E999 Nm "));
    width = (2+unscaled_stress_axix_bounding_box.width()) * 0.8 / sceneTransform().m22();
  }
  return width;
}

void StressCurveGraphItem::tank_condition_chanced(const ange::schedule::Call* call)
{
  if(call == m_call){
    update();
  }
}

void StressCurveGraphItem::reset_after_vessel_import(ange::vessel::Vessel* vessel)
{
  m_vessel = vessel;
  reset();
}
#include "stress_curve_graph_item.moc"
