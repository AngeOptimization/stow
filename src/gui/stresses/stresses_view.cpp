#include "stresses_view.h"

#include "bayweightlimits.h"
#include "callutils.h"
#include "document.h"
#include "stress_curve_graph_item.h"
#include "stresses.h"

#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>
#include <ange/vessel/vessel.h>

#include <cmath>

using namespace ange::units;
using ange::angelstow::MasterPlanningPlugin;
using ange::schedule::Call;

stresses_view_t::stresses_view_t(QWidget* parent)
    : AbstractMultiCallView(parent), m_stowagePlugin(0)
{
    // Empty
}

void stresses_view_t::setStowagePlugin(MasterPlanningPlugin* stowagePlugin) {
    m_stowagePlugin = stowagePlugin;
}

void stresses_view_t::set_document(document_t* document)
{
  AbstractMultiCallView::set_document(document);
  connect(document, SIGNAL(vessel_changed(ange::vessel::Vessel*)), SLOT(vesselChanged(ange::vessel::Vessel*)));
  connect(document->stresses_bending(), SIGNAL(stresses_changed(const ange::schedule::Call*)),
          SLOT(update_bending_curves_later(const ange::schedule::Call*)));
}

AbstractVesselGraphicsItem* stresses_view_t::create_item_from_call(ange::schedule::Call* call) {
  if (is_after(call)) {
    return 0L;
  }
  StressCurveGraphItem* stress_vessel = new StressCurveGraphItem(m_document, call);
  if (m_stowagePlugin) {
    if (true) {
      Q_FOREACH(const ange::vessel::BaySlice* bay_slice, m_document->vessel()->baySlices()) {
        const ange::units::Mass limit = m_document->bayWeightLimits()->weightLimitForBay(call, bay_slice);
        if (!std::isinf(limit / kilogram)) {
          stress_vessel->upper_weight_limit_changed(call, bay_slice, limit);
        }
      }
    }
  }
  return stress_vessel;
}

void stresses_view_t::update_bending_curves_later(const ange::schedule::Call* call) {
  if (AbstractVesselGraphicsItem* item = m_items.value(call)) {
    item->update_later();
  }
}

void stresses_view_t::vesselChanged(ange::vessel::Vessel* ) {
    // on vessel changed,  the stress object gets recreated
    connect(m_document->stresses_bending(), SIGNAL(stresses_changed(const ange::schedule::Call*)),
          SLOT(update_bending_curves_later(const ange::schedule::Call*)), Qt::UniqueConnection);
}

#include "stresses_view.moc"
