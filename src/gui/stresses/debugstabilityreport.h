#ifndef DEBUG_STABILITY_REPORT_H
#define DEBUG_STABILITY_REPORT_H

#include <QString>

class QWidget;
class PluginManager;
class document_t;
namespace ange {
namespace schedule {
class Call;
}
}

/**
 * @brief Display report about stability calculations
 *
 * This is unfinished for now as it just has been created as a developer tool for debugging GM differences.
 */
class DebugStabilityReport {

public:
    /**
     * Generate the report based on the document, will not access the document after the constructor has returned.
     */
    DebugStabilityReport(const document_t* document, PluginManager* pluginManager, const ange::schedule::Call* call);

    /**
     * Show report using the given parent for the window.
     */
    void showWindow(QWidget* parent);

private:
    PluginManager* m_pluginManager;
    QString m_html;

};

#endif // DEBUG_STABILITY_REPORT_H
