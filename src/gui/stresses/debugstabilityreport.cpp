#include "debugstabilityreport.h"

#include "container_list.h"
#include "document.h"
#include "document_interface.h"
#include "htmldisplaywindow.h"
#include "htmlstabilityreport.h"
#include "pluginmanager.h"
#include "stability.h"
#include "stabilityforcer.h"
#include "stowage.h"
#include "stowagestack.h"
#include "stresses.h"
#include "tankconditions.h"

#include <stowplugininterface/stabilityreporterinterface.h>
#include <stowplugininterface/lashingpatterninterface.h>
#include <stowplugininterface/idocument.h>
#include <ange/containers/container.h>
#include <ange/schedule/call.h>
#include <ange/vessel/bayslice.h>
#include <ange/vessel/bilinearinterpolator.h>
#include <ange/vessel/cargohold.h>
#include <ange/vessel/slot.h>
#include <ange/vessel/slottablesummary.h>
#include <ange/vessel/stack.h>
#include <ange/vessel/stacksupport.h>
#include <ange/vessel/vessel.h>
#include <ange/vessel/lashingpatterndata.h>

using namespace ange::units;
using ange::angelstow::LashingPatternInterface;
using ange::vessel::BlockWeight;
using ange::vessel::Vessel;
using ange::vessel::BaySlice;
using ange::vessel::BayRowTier;
using ange::vessel::Slot;
using ange::vessel::Stack;
using ange::vessel::StackSupport;
using ange::containers::Container;

static void formatHtmlVesselDetails(HtmlSectionedReport& report, const Vessel* vessel, QSharedPointer<const LashingPatternInterface> lashingPattern) {
    QString htmlString;
    QTextStream html(&htmlString);
    html << "<table summary='' border='1' cellpadding='3' cellspacing='0'>\n";
    html << "<tr><th>Profile</th><th>Details</th></tr>\n";
    html << QString("<tr><td>IMO</td><td>%1</td></tr>\n").arg(vessel->imoNumber());
    html << QString("<tr><td>Classification Society</td><td>%1</td></tr>\n").arg(vessel->classificationSociety());
    html << QString("<tr><td>Lashing pattern</td><td>%1</td></tr>\n").arg(lashingPattern->name());
    const ange::vessel::SlotTableSummary& slotSummary = vessel->slotTableSummary();
    html << QString("<tr><td>Max TEU</td><td>%1</td></tr>").arg(slotSummary.totalTEU());
    html << QString("<tr><td>Max reefer plugs</td><td>%1</td></tr>\n").arg(slotSummary.reeferPlugs());
    html << QString("<tr><td>HYDROSTATICS</td><td>%1</td></tr>\n").arg(vessel->hasInfo(Vessel::Hydrostatics) ? "true" : "false");
    html << QString("<tr><td>HULL_SHAPE</td><td>%1</td></tr>\n").arg(vessel->hasInfo(Vessel::HullShape) ? "true" : "false");
    html << QString("<tr><td>VISIBILITY_LINE_DATA</td><td>%1</td></tr>\n").arg(vessel->hasInfo(Vessel::VisibilityLineData) ? "true" : "false");
    html << QString("<tr><td>HYDROSTATICS draftData().isValid()</td><td>%1</td></tr>\n").arg(vessel->draftData().isValid() ? "true" : "false");
    html << QString("<tr><td>HYDROSTATICS trimData().isValid()</td><td>%1</td></tr>\n").arg(vessel->trimData().isValid() ? "true" : "false");
    html << QString("<tr><td>HYDROSTATICS metacenterFunction().isValid()</td><td>%1</td></tr>\n").arg(vessel->metacenterFunction().isValid() ? "true" : "false");
    html << "</table>";
    report.addSection("Vessel Details", htmlString);
}

namespace {

struct ContainerStruct {
    BayRowTier brt;
    const Container* container;
    ContainerStruct(const BayRowTier& brt_, const Container* container_) : brt(brt_), container(container_) {}
};

struct ContainerStructLessThan {
    bool operator()(const ContainerStruct& first, const ContainerStruct& second) {
        if (first.brt.bay() < second.brt.bay()) return true;
        if (first.brt.bay() > second.brt.bay()) return false;
        if (first.brt.row() < second.brt.row()) return true;
        if (first.brt.row() > second.brt.row()) return false;
        if (first.brt.tier() < second.brt.tier()) return true;
        if (first.brt.tier() > second.brt.tier()) return false;
        return false;
    }
};

struct BaySummary {
    int count;
    BlockWeight blockWeight;
    BaySummary() : count(0) {}
    void operator+=(const BaySummary& other) {
        count += other.count;
        blockWeight.extendWith(other.blockWeight);
    }
};

} // namespace

static void extendBaySummaries(QMap<int, BaySummary>* baySummaries, const StackSupport* support, const BlockWeight& block) {
    if (support) {
        baySummaries->operator[](support->bay()).blockWeight.extendWith(block);
    } else {
        Q_ASSERT(block.weight() == 0 * kilogram);
    }
}

static void formatCargo(HtmlSectionedReport& report, const document_t* document, const ange::schedule::Call* call) {
    QString htmlString;
    QTextStream html(&htmlString);
    stowage_t* stowage = document->stowage();
    QSet<const Container*> containers = stowage->get_containers_on_board(call);
    QMap<int, BaySummary> baySummaries;
    QList<ContainerStruct> list;
    Q_FOREACH (const Container* container, containers) {
        BayRowTier brt = stowage->nominal_position(container, call);
        BaySummary& baySummary = baySummaries[brt.bay()];
        baySummary.count += 1;
        list << ContainerStruct(brt, container);
    }
    qSort(list.begin(), list.end(), ContainerStructLessThan());
    Q_FOREACH (const BaySlice* baySlice, document->vessel()->baySlices()) {
        Q_FOREACH (const Stack* stack, baySlice->stacks()) {
            StowageStack* stowageStack = document->stowage()->stowage_stack(call, stack);
            extendBaySummaries(&baySummaries, stack->stackSupport20Fore(), stowageStack->block_weight_fore20());
            extendBaySummaries(&baySummaries, stack->stackSupport40(), stowageStack->block_weight_40());
            extendBaySummaries(&baySummaries, stack->stackSupport20Aft(), stowageStack->block_weight_aft20());
        }
    }

    html << "<h3>Bay summary</h3>\n";
    html << "<table summary='' border='1' cellpadding='3' cellspacing='0'>\n";
    html << "<tr>\n";
    html << "<th>Bay</th>\n";
    html << "<th>Count</th>\n";
    html << "<th>Weight [t]</th>\n";
    html << "<th>LCG [m]</th>\n";
    html << "<th>VCG [m]</th>\n";
    html << "<th>TCG [m]</th>\n";
    html << "</tr>\n";
    BaySummary bayTotal;
    Q_FOREACH (int bay, baySummaries.keys()) {
        const BaySummary& baySummary = baySummaries.value(bay);
        bayTotal += baySummary;
        html << "<tr>\n";
        html << QString("<td><a href='#cargo_%1'>%1</a></td>\n").arg(bay, 2, 10, QChar('0'));
        html << QString("<td align='right'>%1</td>\n").arg(baySummary.count);
        html << QString("<td align='right'>%1</td>\n").arg(baySummary.blockWeight.weight()/ton, 0, 'f', 3);
        html << QString("<td align='right'>%1</td>\n").arg(baySummary.blockWeight.lcg()/meter, 0, 'f', 3);
        html << QString("<td align='right'>%1</td>\n").arg(baySummary.blockWeight.vcg()/meter, 0, 'f', 3);
        html << QString("<td align='right'>%1</td>\n").arg(baySummary.blockWeight.tcg()/meter, 0, 'f', 3);
        html << "</tr>\n";
    }
    html << "<tr>\n";
    html << QString("<td><a href='#cargo_total'>Total</a></td>\n");
    html << QString("<td align='right'>%1</td>\n").arg(bayTotal.count);
    html << QString("<td align='right'>%1</td>\n").arg(bayTotal.blockWeight.weight()/ton, 0, 'f', 3);
    html << QString("<td align='right'>%1</td>\n").arg(bayTotal.blockWeight.lcg()/meter, 0, 'f', 3);
    html << QString("<td align='right'>%1</td>\n").arg(bayTotal.blockWeight.vcg()/meter, 0, 'f', 3);
    html << QString("<td align='right'>%1</td>\n").arg(bayTotal.blockWeight.tcg()/meter, 0, 'f', 3);
    html << "</tr>\n";
    html << "</table>\n";

    html << "<h3>All containers</h3>\n";
    html << "<table summary='' border='1' cellpadding='3' cellspacing='0'>\n";
    html << "<tr>\n";
    html << "<th>Position</th>\n";
    html << "<th>Name</th>\n";
    html << "<th>Weight [t]</th>\n";
    html << "<th>LCG [m]</th>\n";
    html << "<th>VCG [m]</th>\n";
    html << "<th>TCG [m]</th>\n";
    html << "</tr>\n";
    const Vessel* vessel = document->vessel();
    BlockWeight total;
    Q_FOREACH (const ContainerStruct& slotCont, list) {
        const BayRowTier brt = slotCont.brt;
        const Container* container = slotCont.container;
        const Slot* slot = vessel->slotAt(brt);
        ange::vessel::Stack* stack = slot->stack();
        StowageStack* stowageStack = stowage->stowage_stack(call, stack);
        BlockWeight blockWeight = stowageStack->containerBlockWeight(container);
        total.extendWith(blockWeight);
        Q_ASSERT(qFuzzyCompare(container->weight()/kilogram, blockWeight.weight()/kilogram));
        html << "<tr><td>\n";
        html << QString("<a name='cargo_%1'>\n").arg(brt.bay(), 2, 10, QChar('0'));
        html << QString("%1%2%3</a></td>\n").arg(brt.bay(), 2, 10, QChar('0'))
             .arg(brt.row(), 2, 10, QChar('0')).arg(brt.tier(), 2, 10, QChar('0'));
        html << QString("<td>%1</td>\n").arg(container->equipmentNumber());
        html << QString("<td align='right'>%1 ton</td>\n").arg(blockWeight.weight()/ton, 0, 'f', 3);
        html << QString("<td align='right'>%1 m</td>\n").arg(blockWeight.lcg()/meter, 0, 'f', 3);
        html << QString("<td align='right'>%1 m</td>\n").arg(blockWeight.vcg()/meter, 0, 'f', 3);
        html << QString("<td align='right'>%1 m</td>\n").arg(blockWeight.tcg()/meter, 0, 'f', 3);
        html << "</tr>\n";
    }
    html << "<tr><td>\n";
    html << QString("<a name='cargo_total'>\n");
    html << QString("Total</a></td>\n");
    html << QString("<td></td>\n");
    html << QString("<td align='right'>%1 ton</td>\n").arg(total.weight()/ton, 0, 'f', 3);
    html << QString("<td align='right'>%1 m</td>\n").arg(total.lcg()/meter, 0, 'f', 3);
    html << QString("<td align='right'>%1 m</td>\n").arg(total.vcg()/meter, 0, 'f', 3);
    html << QString("<td align='right'>%1 m</td>\n").arg(total.tcg()/meter, 0, 'f', 3);
    html << "</tr>\n";
    html << "</table>\n";
    report.addSection("Cargo", htmlString);
}

static void addLashingPatternSection(HtmlSectionedReport& report, const Vessel* vessel) {
    QString header("Lashing Pattern (in profile)");
    const ange::vessel::LashingPatternData* lashingPatternData = vessel->lashingPatternData();
    if (!lashingPatternData) {
        report.addSection(header, "There is no lashing pattern in the vessel profile");
        return;
    }
    QString htmlString;
    QTextStream html(&htmlString);

    html << "<h3>Stack patterns</h3>\n";
    html << "<table summary='' border='1' cellpadding='3' cellspacing='0'>\n";
    html << "<tr>\n";
    html << "<th>Name</th>\n";
    html << "</tr>\n";
    Q_FOREACH (const ange::vessel::StackLashingPattern* stackLashingPattern, lashingPatternData->stackLashingPatterns()) {
        html << "<tr>\n";
        html << "<td>" << stackLashingPattern->patternName << "</td>\n";
        html << "</tr>\n";
    }
    html << "</table>\n";

    report.addSection(header, htmlString);
}

/* Overall format of the report:
 * <h1>Stability and Stresses Report
 *   <h2>Call data
 *   <h2>Vessel Details
 *   <h2>Draft Trim List ...
 *   <h2>Weights, CG and Moments
 *   <h2>Lightship Weights (profile)
 *   <h2>Constant Weights
 *   <h2>Tanks
 *   <h2>Cargo Bays
 *   <h2>Stresses
 *   <h2>Cargo
 *     <h3>Bay summary
 *     <h3>All containers
 *   <h2>plugin1 header
 *   <h2>plugin2 header
 */
DebugStabilityReport::DebugStabilityReport(const document_t* document, PluginManager* pluginManager, const ange::schedule::Call* call)
    : m_pluginManager(pluginManager)
{
    HtmlStabilityReport report(document, call, "Debug Stability Report");
    report.sectionVessel();
    report.sectionPortCall();
    report.sectionDraftTrimList();

    formatHtmlVesselDetails(report, document->vessel(),
                            QSharedPointer<const LashingPatternInterface>(document->documentInterface()->lashingPattern(call)));
    addLashingPatternSection(report, document->vessel());

    report.sectionStresses();
    report.sectionWeightsMoments();
    // Lightship weight and LCG should not be summed over BlockWeight vessel->lightshipWeights(), as these do not add up to the total lightship weight
    report.sectionBlockWeights(QStringLiteral("Lightship Weights (corrected)"), document->stresses_bending()->lightshipWeights());
    report.sectionBlockWeights(QStringLiteral("Constant Weights"), document->vessel()->constantWeights());
    QList<ange::vessel::BlockWeight> deadLoadList;
    deadLoadList << document->stabilityForcer()->deadLoad(call);
    report.sectionBlockWeights(QStringLiteral("Deadload Weight Adjustment"), deadLoadList);
    report.sectionTanks();

    formatCargo(report, document, call);

    QMap<QString, QString> pluginReports; // Stored in QMap for sorting on the headers
    Q_FOREACH (ange::angelstow::StabilityReporterInterface* stabilityPlugin, m_pluginManager->stabilityReporterPlugins()) {
        pluginReports.insertMulti(stabilityPlugin->headerForStabilityReport(), stabilityPlugin->sectionForStabilityReport(call));
    }
    for (QMap<QString, QString>::iterator it = pluginReports.begin(); it != pluginReports.end(); ++it) {
        report.addSection(it.key(), it.value());
    }
    m_html = report.toString();
}

void DebugStabilityReport::showWindow(QWidget* parent) {
    HtmlDisplayWindow* window = new HtmlDisplayWindow("Stability report", m_html, parent);
    window->show();
}

#include "debugstabilityreport.moc"
