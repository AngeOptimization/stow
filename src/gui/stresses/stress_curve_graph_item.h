/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010

 Copyright: See COPYING file that comes with this distribution

*/

#ifndef STRESS_CURVE_GRAPH_ITEM_H
#define STRESS_CURVE_GRAPH_ITEM_H

#include <QGraphicsItem>
#include <QObject>
#include "document/stowage/stowage.h"
#include "stowplugininterface/macroplan.h"
#include "gui/common/abstractvesselgraphicsitem.h"
#include <limits>

class svg_graphics_item_t;
namespace ange {
namespace schedule {
class Call;
}
namespace vessel {
class Vessel;
class BaySlice;
}
}

/**
 * Visualize the stresses and forces on a vessel for a given call (when leaving the port, to be exact)
 */
class StressCurveGraphItem : public AbstractVesselGraphicsItem {
    Q_OBJECT
public:
    /**
     * Construct a vessel stress
     */
    StressCurveGraphItem(document_t* document,
                    ange::schedule::Call* call,
                    QGraphicsItem* parent = 0);
    virtual QRectF boundingRect() const;
    virtual QRectF bay_area(const ange::vessel::BaySlice* bay_slice) const;

protected:
    virtual void contextMenuEvent(QGraphicsSceneContextMenuEvent* event);

    virtual void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget = 0);
    virtual double max_value() const;
    virtual qreal right_margin_width() const;
    virtual QRectF coordinate_system_rect() const;
    virtual ange::vessel::BaySlice* bay_slice_for_pos(const QPointF& pos) const;
private:
    enum stress_type_t {
      BENDING,
      SHEAR,
      TORSION
    };
    document_t* m_document;
    stowage_t* m_stowage;
    ange::angelstow::MacroPlan* m_macro_stowage;
    struct bay_data_t {
      bay_data_t() : micro_weight(0.0), weight(0.0), upper_bound(std::numeric_limits<double>::infinity()), graphics_item(0L) {
      }
      ange::units::Mass micro_weight;
      ange::units::Mass weight;
      ange::units::Mass upper_bound;
      svg_graphics_item_t* graphics_item;
    };
    ange::units::Mass m_max_weight;
    typedef QHash<const ange::vessel::BaySlice*, bay_data_t> bay_datas_t;
    bay_datas_t m_bay_data;
    void draw_bay(QPainter* painter, ange::vessel::BaySlice* slice, const QRectF& bounding_rect, double max_value) const;
    void add_weight_limit_for_bay(ange::vessel::BaySlice* bay_slice, QPointF position);
    void delete_weight_limit_for_bay(ange::vessel::BaySlice* bay_slice);
    static void draw_bar(QPainter* painter, const QRectF& bar, QColor main_color, QColor faded_color);
    void set_upper_range_range(const ange::vessel::BaySlice* bay_slice);
    void recalculate_max_weight();
    void set_weight_limit_for_bay(const ange::vessel::BaySlice* bay_slice, const QPointF& position);
    const QRectF right_margin();

    /**
     * Draw the bendingcurves. By default, this function does nothing.
     */
    QPair<int, int> draw_stress_curve(QPainter* painter, const QVector< QPointF >& stress_curve, const QColor& color);

    void draw_coordinateaxis_for_stresscurves(QPainter* painter);
    void draw_tickmarks_for_stresscurves(QPainter* painter);

public Q_SLOTS:
    void container_changed_position(const ange::containers::Container* container,
                                    const ange::vessel::Slot* from ,
                                    const ange::vessel::Slot* to,
                                    const ange::schedule::Call* call,
                                    ange::angelstow::StowType old_type,
                                    ange::angelstow::StowType new_type);

    /**
     * Reset vessel stress calculation
     */
    void reset();
    void upper_limit_item_changed(QGraphicsItem* item, QPointF position);
    void upper_weight_limit_changed(const ange::schedule::Call* call, const ange::vessel::BaySlice* bay_slice, const ange::units::Mass value);
    void tank_condition_chanced(const ange::schedule::Call* call);
    void reset_after_vessel_import(ange::vessel::Vessel* vessel);
Q_SIGNALS:
    void upper_limit_added(QVariant bay_number);
    void upper_limit_removed(QVariant bay_number);
    void upper_limit_set(QVariant bay_number, QVariant value);
};

#endif // STRESS_CURVE_GRAPH_ITEM_H
