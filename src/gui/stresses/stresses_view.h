/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010

 Copyright: See COPYING file that comes with this distribution

*/

#ifndef STRESSES_VIEW_H
#define STRESSES_VIEW_H

#include "../common/abstractmulticallview.h"

namespace ange {
namespace angelstow {
class MasterPlanningPlugin;
}
namespace vessel {
class Vessel;
}
}

class stresses_view_t : public AbstractMultiCallView {
    Q_OBJECT

public:
    stresses_view_t(QWidget* parent = 0);

    void setStowagePlugin(ange::angelstow::MasterPlanningPlugin* stowagePlugin);

public Q_SLOTS:
    /**
     * Update bending curves (from document)
     */
    void update_bending_curves_later(const ange::schedule::Call* call);

    /**
     * @overload
     */
    virtual void set_document(document_t* document);
protected:
    virtual AbstractVesselGraphicsItem* create_item_from_call(ange::schedule::Call* call);
private Q_SLOTS:
    void vesselChanged(ange::vessel::Vessel*);

private:
    ange::angelstow::MasterPlanningPlugin* m_stowagePlugin;

};

#endif // STRESSES_VIEW_H
