#include <QString>

namespace CrashHandler {

/**
 * Register crash handler that stores minidumps if the application crases
 */
void registerCrashHandler();

/**
 * Directory the crash dumps are saved to
 */
QString crashDir();

}; // namespace CrashHandler
