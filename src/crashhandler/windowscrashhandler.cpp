#include "crashhandler.h"

#include <client/windows/handler/exception_handler.h>

#include <QDir>

// Implementation of crash handler interface using Google Breakpad on Windows

namespace CrashHandler {

using google_breakpad::ExceptionHandler;

void registerCrashHandler() {
    static ExceptionHandler exceptionHandler(crashDir().toStdWString(), 0, 0, 0, ExceptionHandler::HANDLER_ALL);
}

QString crashDir() {
    return QDir::tempPath();
}

}; // namespace CrashHandler
