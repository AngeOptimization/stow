#include "crashhandler.h"

#include <client/linux/handler/exception_handler.h>

#include <QDir>

// Implementation of crash handler interface using Google Breakpad on Linux

namespace CrashHandler {

static bool dumpCallback(const google_breakpad::MinidumpDescriptor& descriptor, void* context, bool succeeded) {
    Q_UNUSED(context);
    printf("Dump path: %s\n", descriptor.path());
    return succeeded;
}

void registerCrashHandler() {
    google_breakpad::MinidumpDescriptor descriptor(crashDir().toStdString());
    static google_breakpad::ExceptionHandler exceptionHandler(descriptor, 0, dumpCallback, 0, true, -1);
}

QString crashDir() {
    return QDir::tempPath();
}

}; // namespace CrashHandler
