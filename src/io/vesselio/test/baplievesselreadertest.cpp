#include <QtTest>

#include "baplieparser.h"
#include "baplieparserresult.h"
#include "baplievesselreader.h"

#include <ange/vessel/vessel.h>

#include <KCompressionDevice>

#include <QObject>

#include <stdexcept>

using namespace ange::vessel;

class BaplieVesselReaderTest : public QObject {
    Q_OBJECT
private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();

    void testReadVessel();
    void testReadVesselJsonError();
    void testReadVesselBaplieError();

    void testBrtMax();
    void testBrtMaxEmpty();
    void testBrtMaxBays();
};

QTEST_GUILESS_MAIN(BaplieVesselReaderTest);

void BaplieVesselReaderTest::initTestCase() {
    // Called before the first testfunction is executed
}

void BaplieVesselReaderTest::cleanupTestCase() {
    // Called after the last testfunction was executed
}

void BaplieVesselReaderTest::testReadVessel() {
    KCompressionDevice file(QFINDTESTDATA("data/Lego_Maersk_Small-AAAAA-0001.BAPLIE.edi.gz"), KCompressionDevice::GZip);
    QVERIFY(file.open(QIODevice::ReadOnly));
    QByteArray jsonData = BaplieVesselReader::transformatToJson(file.readAll());
    QBuffer buffer(&jsonData);
    QVERIFY(buffer.open(QIODevice::ReadOnly));
    Vessel* vessel = BaplieVesselReader::readVessel(&buffer);
    QCOMPARE(vessel->name(), QStringLiteral("LEGO MAERSK SMALL"));
    delete vessel;
}

void BaplieVesselReaderTest::testReadVesselJsonError() {
    QBuffer buffer;
    QVERIFY(buffer.open(QIODevice::ReadOnly));
    try {
        BaplieVesselReader::readVessel(&buffer);
        QVERIFY(false); // Expect exception
    } catch (const std::runtime_error& e) {
        QCOMPARE(e.what(), "JSON parse error: illegal value");
    }
}

void BaplieVesselReaderTest::testReadVesselBaplieError() {
    QBuffer buffer;
    buffer.buffer() = "{ \"angelStowBaplieJsonFormatVersion\":1, \"data\":\"\" }";
    QVERIFY(buffer.open(QIODevice::ReadOnly));
    try {
        BaplieVesselReader::readVessel(&buffer);
        QVERIFY(false); // Expect exception
    } catch (const std::runtime_error& e) {
        QCOMPARE(e.what(), "Problem when parsing BAPLIE file: UNB segment missing");
    }
}

void BaplieVesselReaderTest::testBrtMax() {
    KCompressionDevice file(QFINDTESTDATA("data/Lego_Maersk_Small-AAAAA-0001.BAPLIE.edi.gz"), KCompressionDevice::GZip);
    QVERIFY(file.open(QIODevice::ReadOnly));

    BaplieParserResult baplieParserResult = BaplieParser::parse(&file);
    QVERIFY(!baplieParserResult.hasErrors());

    BrtMax brtMax = BaplieVesselReader::resultToBrtMax(baplieParserResult);

    QCOMPARE(brtMax.bays40(), QList<int>() << 2 << 6 << 10 << 14 << 18 << 22);

    QCOMPARE(brtMax.rows(Above), QList<int>() << 4 << 2 << 0 << 1 << 3);
    QCOMPARE(brtMax.tiers(Above), QList<int>() << 80 << 82 << 84);

    QCOMPARE(brtMax.rows(Below), QList<int>() << 4 << 2 << 1 << 3);
    QCOMPARE(brtMax.tiers(Below), QList<int>() << 2);
}

void BaplieVesselReaderTest::testBrtMaxEmpty() {
    BrtMax brtMax;

    QCOMPARE(brtMax.bays40(), QList<int>());

    QCOMPARE(brtMax.rows(Above), QList<int>());
    QCOMPARE(brtMax.tiers(Above), QList<int>());

    QCOMPARE(brtMax.rows(Below), QList<int>());
    QCOMPARE(brtMax.tiers(Below), QList<int>());
}

void BaplieVesselReaderTest::testBrtMaxBays() {
    { // Test pattern: X XXX XXX
        BrtMax brtMax;
        Q_FOREACH (int bay, QList<int>() << 1 << 3 << 4 << 5 << 7 << 8 << 9) {
            brtMax << BayRowTier(bay, 0, 2);
        }
        QCOMPARE(brtMax.bays40(), QList<int>() << 4 << 8);
        QCOMPARE(brtMax.baysSingle20(), QList<int>() << 1);
    }

    { // Test pattern: xxx x xXx X
        BrtMax brtMax;
        Q_FOREACH (int bay, QList<int>() << 8 << 11) {
            brtMax << BayRowTier(bay, 0, 2);
        }
        QCOMPARE(brtMax.bays40(), QList<int>() << 2 << 8);
        QCOMPARE(brtMax.baysSingle20(), QList<int>() << 5 << 11);
    }

    { // Test pattern: X xXx xxx X xXx
        BrtMax brtMax;
        Q_FOREACH (int bay, QList<int>() << 1 << 4 << 11 << 14) {
            brtMax << BayRowTier(bay, 0, 2);
        }
        QCOMPARE(brtMax.bays40(), QList<int>() << 4 << 8 << 14);
        QCOMPARE(brtMax.baysSingle20(), QList<int>() << 1 << 11);
    }

    { // Test pattern: Xxx x xXx X xXx
        BrtMax brtMax;
        Q_FOREACH (int bay, QList<int>() << 1 << 8 << 11 << 14) {
            brtMax << BayRowTier(bay, 0, 2);
        }
        QCOMPARE(brtMax.bays40(), QList<int>() << 2 << 8 << 14);
        QCOMPARE(brtMax.baysSingle20(), QList<int>() << 5 << 11);
    }

}

#include "baplievesselreadertest.moc"
