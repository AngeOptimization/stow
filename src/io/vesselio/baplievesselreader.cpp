#include "baplievesselreader.h"

#include "baplieparser.h"
#include "baplieparserresult.h"

#include <ange/vessel/bayslice.h>
#include <ange/vessel/hatchcover.h>
#include <ange/vessel/slot.h>
#include <ange/vessel/stack.h>
#include <ange/vessel/stacksupport.h>
#include <ange/units/units.h>
#include <ange/vessel/vessel.h>
#include <ange/vessel/vesseltank.h>

#include <QBuffer>
#include <QIODevice>
#include <QJsonDocument>
#include <QJsonObject>
#include <QSharedData>

#include <stdexcept>

using namespace ange::units;
using namespace ange::vessel;


// Helper classes:

class BayMax {
public:

    BayMax() : maxBay20(-1) { }

    void operator<<(const BayRowTier& position) {
        int bay = position.bay();
        if (bay % 2 == 0) {  // 40' bay (even)
            maxBay20 = qMax(maxBay20, bay + 1);
            bay40 << bay;
        } else {  // 20' bay (odd)
            maxBay20 = qMax(maxBay20, bay);
        }
    }

    QList<int> bays40() const {
        QList<int> bays;
        for (int bay = 2; bay <= maxBay20 - 1;) {
            if (bay40.contains(bay)) {  // Is a known 40' bay
                bays << bay;
                bay += 4;
            } else if (!bay40.contains(bay + 2)) {  // Is an unknown 40' bay
                bays << bay;
                bay += 4;
            } else { // Fore part is a single 20' bay
                bay += 2;
            }
        }
        return bays;
    }

    QList<int> baysSingle20() const {
        QList<int> bays;
        for (int bay = 1; bay <= maxBay20;) {
            if (bay40.contains(bay + 1)) {  // Is fore in a known 40' bay
                bay += 4;
            } else if (!bay40.contains(bay + 3) && bay + 2 <= maxBay20) {  // Is fore in an unknown 40' bay
                bay += 4;
            } else { // Is a single 20' bay
                bays << bay;
                bay += 2;
            }
        }
        return bays;
    }

private:
    int maxBay20;
    QSet<int> bay40;
};


class RowMax {
public:

    RowMax() : hasRowZero(false), maxOddRow(-1), maxEvenRow(0) { }

    void operator<<(const ange::vessel::BayRowTier& position) {
        int row = position.row();
        if (row == 0) {
            hasRowZero = true;
        }
        if (row % 2 == 0) {  // row is even
            maxEvenRow = qMax(maxEvenRow, row);
        } else {  // row is odd
            maxOddRow = qMax(maxOddRow, row);
        }
    }

    QList<int> rows() const {
        int maxRow = qMax(maxOddRow + 1, maxEvenRow);  // Even number
        QList<int> rows;
        for (int row = maxRow; row > 0; row -= 2) {
            rows << row;
        }
        if (hasRowZero) {
            rows << 0;
        }
        for (int row = 1; row <= maxRow; row += 2) {
            rows << row;
        }
        return rows;
    }

private:
    bool hasRowZero;
    int maxOddRow;
    int maxEvenRow;
};


class TierMax {
public:

    TierMax() : minTier(100), maxTier(0) { }

    void operator<<(const ange::vessel::BayRowTier& position) {
        minTier = qMin(minTier, position.tier());
        maxTier = qMax(maxTier, position.tier());
    }

    QList<int> tiers() const {
        QList<int> tiers;
        for (int tier = minTier; tier <= maxTier; tier += 2) {
            tiers << tier;
        }
        return tiers;
    }

private:
    int minTier;
    int maxTier;
};


// BrtMaxData:

class BrtMaxData : public QSharedData {
public:
    BayMax bayMax;
    QHash<DeckLevel, RowMax> rowMaxs;
    QHash<DeckLevel, TierMax> tierMaxs;
};


// BrtMax:

BrtMax::BrtMax()
  : d(new BrtMaxData())
{
    // Empty
}

BrtMax::BrtMax(const BrtMax& other)
  : d(other.d)
{
    // Empty
}

BrtMax::~BrtMax() {
    // Empty
}

// XXX do we have a shared method for testing this?
static DeckLevel tierToLevel(int tier) {
    if (tier >= 50) {
        return Above;
    } else {
        return Below;
    }
}

void BrtMax::operator<<(const BayRowTier& position) {
    if (!position.placed()) {
        return;  // Ignore unplaced positions
    }

    int tier = position.tier();
    DeckLevel level = tierToLevel(tier);
    d->bayMax << position;
    d->rowMaxs[level] << position;
    d->tierMaxs[level] << position;
}

QList<int> BrtMax::bays40() const {
    return d->bayMax.bays40();
}

QList<int> BrtMax::baysSingle20() const {
    return d->bayMax.baysSingle20();
}

QList<int> BrtMax::rows(DeckLevel level) const {
    return d->rowMaxs[level].rows();
}

QList<int> BrtMax::tiers(DeckLevel level) const {
    return d->tierMaxs[level].tiers();
}


// BaplieVesselReader:

ange::vessel::Vessel* BaplieVesselReader::readVessel(QIODevice* device) {
    Q_ASSERT(device->isOpen());
    QJsonParseError jsonParseError;
    QJsonDocument jsonDocument = QJsonDocument::fromJson(device->readAll(), &jsonParseError);
    if (jsonParseError.error) {
        throw std::runtime_error(QString("JSON parse error: %1").arg(jsonParseError.errorString()).toStdString());
    }
    if (!jsonDocument.isObject()) {
        throw std::runtime_error("Root element not object");
    }
    QJsonObject root = jsonDocument.object();
    if (root["angelStowBaplieJsonFormatVersion"] != 1) {
        throw std::runtime_error("angelStowBaplieJsonFormatVersion != 1");
    }
    QJsonValueRef data = root["data"];
    QByteArray byteArray(data.toString().toLatin1());
    QBuffer buffer(&byteArray);
    buffer.open(QIODevice::ReadOnly);
    BaplieParserResult baplieParserResult = BaplieParser::parse(&buffer);
    Q_FOREACH (const EdiProblem& problem, baplieParserResult.problemList) {
        if (problem.type() == EdiProblem::Error) {
            throw std::runtime_error(QString("Problem when parsing BAPLIE file: %1").arg(problem.message()).toStdString());
        }
    }
    return newVessel(baplieParserResult);
}

QByteArray BaplieVesselReader::transformatToJson(const QByteArray& data) {
    QJsonObject root;
    root["angelStowBaplieJsonFormatVersion"] = 1;
    root["data"] = QString::fromUtf8(data);
    return QJsonDocument(root).toJson();
}

BrtMax BaplieVesselReader::resultToBrtMax(const BaplieParserResult& baplieParserResult) {
    BrtMax brtMax;
    Q_FOREACH (const BaplieParserResult::ContainerPosition& cp, baplieParserResult.containerList()) {
        brtMax << cp.position;
    }
    return brtMax;
}

Vessel* BaplieVesselReader::newVessel(const BaplieParserResult& baplieParserResult) {
    BrtMax brtMax = resultToBrtMax(baplieParserResult);
    QHash<int, QList<Stack*> > bayStacks;
    Q_FOREACH (DeckLevel level, QList<DeckLevel>() << Above << Below) {
        Length stackBottom;
        if (level == Above) {
            stackBottom = (brtMax.tiers(Below).last() + 3) / 2.0 * 9 * feet;
        } else { // BELOW;
            stackBottom = brtMax.tiers(Below).first() / 2.0 * 9 * feet;
        }
        Q_FOREACH (int bay, brtMax.bays40()) {
            Length bayPos = -4 * meter * bay;
            int rowIndex = 0;
            Q_FOREACH (int row, brtMax.rows(level)) {
                Length rowPos = (rowIndex - brtMax.rows(level).size() / 2.0) * 2.5 * meter;
                QList<Slot*> slots20fore;
                QList<Slot*> slots20aft;
                Q_FOREACH (int tier, brtMax.tiers(level)) {
                    Length tierPos = qQNaN() * meter;
                    Slot* slot20fore = new Slot(Point3D(bayPos + 10 * feet, tierPos, rowPos), BayRowTier(bay - 1, row, tier));
                    slot20fore->addCapability(Slot::TwentyCapable);
                    slot20fore->addCapability(Slot::FourtyCapable);
                    slot20fore->addCapability(Slot::FourtyFiveCapable);
                    Slot* slot20aft = new Slot(Point3D(bayPos - 10 * feet, tierPos, rowPos), BayRowTier(bay + 1, row, tier));
                    slot20aft->addCapability(Slot::TwentyCapable);
                    slot20aft->addCapability(Slot::FourtyCapable);
                    slot20aft->addCapability(Slot::FourtyFiveCapable);
                    slots20fore << slot20fore;
                    slots20aft << slot20aft;
                }
                QList<Slot*> slots40 = QList<Slot*>() << slots20fore << slots20aft;
                StackSupport* support40
                    = new StackSupport(qInf() * meter, qInf() * ton, QString::number(bay), QString::number(row), level,
                                       StackSupport::Forty, slots40, Point3D(bayPos, stackBottom, rowPos));
                StackSupport* support20fore
                    = new StackSupport(qInf() * meter, qInf() * ton, QString::number(bay - 1), QString::number(row), level,
                                       StackSupport::Twenty, slots20fore, Point3D(bayPos + 10 * feet, stackBottom, rowPos));
                StackSupport* support20aft
                    = new StackSupport(qInf() * meter, qInf() * ton, QString::number(bay + 1), QString::number(row), level,
                                       StackSupport::Twenty, slots20aft, Point3D(bayPos - 10 * feet, stackBottom, rowPos));
                Stack* stack = new Stack(Point3D(bayPos, stackBottom, rowPos), level, support20fore, support20aft, support40);
                bayStacks[bay] << stack;
                ++rowIndex;
            }
        }
        Q_FOREACH (int bay, brtMax.baysSingle20()) {
            Length bayPos = -4 * meter * bay;
            int rowIndex = 0;
            Q_FOREACH (int row, brtMax.rows(level)) {
                Length rowPos = (rowIndex - brtMax.rows(level).size() / 2.0) * 2.5 * meter;
                QList<Slot*> slots20fore;
                Q_FOREACH (int tier, brtMax.tiers(level)) {
                    Length tierPos = qQNaN() * meter;
                    Slot* slot20fore = new Slot(Point3D(bayPos + 10 * feet, tierPos, rowPos), BayRowTier(bay, row, tier));
                    slot20fore->addCapability(Slot::TwentyCapable);
                    slots20fore << slot20fore;
                }
                StackSupport* support20fore
                    = new StackSupport(qInf() * meter, qInf() * ton, QString::number(bay), QString::number(row), level,
                                       StackSupport::Twenty, slots20fore, Point3D(bayPos + 10 * feet, stackBottom, rowPos));
                Stack* stack = new Stack(Point3D(bayPos, stackBottom, rowPos), level, support20fore, 0, 0);
                bayStacks[bay] << stack;
                ++rowIndex;
            }
        }
    }
    QList<BaySlice*> baySlices;
    Q_FOREACH (int bay, bayStacks.keys()) {
        BaySlice* baySlice = new BaySlice(bayStacks[bay]);
        baySlices << baySlice;
    }
    QList<HatchCover*> lids;
    QList<VesselTank*> tanks;
    Vessel* vessel = new Vessel(baySlices, lids, tanks, baplieParserResult.vesselName, baplieParserResult.imoNumber,
                                baplieParserResult.callSign, QString());
    return vessel;
}
