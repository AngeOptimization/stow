#ifndef MULTIVESSELREADER_H
#define MULTIVESSELREADER_H

#include <QHash>
#include <QString>

class QIODevice;
namespace ange {
namespace stowbase {
class Bundle;
class ReferenceObject;
class VesselReader;
}
namespace vessel {
class Vessel;
class VesselTank;
}
}

/**
 * Reads a vessel of unknown format from device.
 * First tries the stowbase/JSON version and if that fails will try the "BAPLIE vessel format".
 * If both fails will throw the exception from the first parse attempt.
 */
class MultiVesselReader {

public:

    MultiVesselReader();
    ~MultiVesselReader();

    /**
     * Read and parse the vessel from the device,
     * Will throw a std::runtime_error if the parsing fails.
     */
    ange::vessel::Vessel* readVessel(QIODevice* device);

    /**
     * Read very old format of .sto document
     */
    ange::vessel::Vessel* readVessel(const ange::stowbase::Bundle& bundle,
                                     const ange::stowbase::ReferenceObject& vesselRo);

    /**
     * Get tank from stowbase object reference, used for reading old .sto files
     */
    ange::vessel::VesselTank* vesselTank(const QString& reference);

private:

    ange::stowbase::VesselReader* m_stoVesselReader;

};

#endif // MULTIVESSELREADER_H
