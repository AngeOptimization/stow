#ifndef BAPLIEVESSELREADER_H
#define BAPLIEVESSELREADER_H

#include <ange/vessel/decklevel.h>

#include <QByteArray>
#include <QList>
#include <QSharedDataPointer>

class BaplieParserResult;
class BrtMaxData;
class QIODevice;
namespace ange {
namespace vessel {
class BayRowTier;
class Vessel;
}
}

/**
 * Helper class that aggregates positions for a level (above/below)
 */
class BrtMax {

public:
    BrtMax();
    BrtMax(const BrtMax& other);
    ~BrtMax();

    /**
     * Include the position in the max
     */
    void operator<<(const ange::vessel::BayRowTier& position);

    /**
     * List of bays for 40' containers (2, 6, 10, ...)
     */
    QList<int> bays40() const;

    /**
     * List of single bays for 20' containers (e.g. 21)
     */
    QList<int> baysSingle20() const;

    /**
     * List of generated rows from even to odd (..., 4, 2, 0, 1, 3, ...)
     */
    QList<int> rows(ange::vessel::DeckLevel level) const;

    /**
     * List of generated tiers from bottom to top (..., 82, 84, 86, ...)
     */
    QList<int> tiers(ange::vessel::DeckLevel level) const;

private:

    QSharedDataPointer<BrtMaxData> d;

};

/**
 * Reads a "JSON BAPLIE format" vessel, that is our hacky vessel format based on a BAPLIE file wrapped in JSON.
 */
class BaplieVesselReader {

public:

    /**
     * Read and parse the vessel from the device in the "JSON BAPLIE format"
     * Will throw a std::runtime_error if the parsing fails.
     */
    static ange::vessel::Vessel* readVessel(QIODevice* device);

    /**
     * Transform the BAPLIE from device to the "JSON BAPLIE format"
     */
    static QByteArray transformatToJson(const QByteArray& data);

    /**
     * Create vessel from preparsed BAPLIE result
     */
    static ange::vessel::Vessel* newVessel(const BaplieParserResult& baplieParserResult);

    /**
     * Create BrtMax from BAPLIE result
     */
    static BrtMax resultToBrtMax(const BaplieParserResult& baplieParserResult);

};

#endif // BAPLIEVESSELREADER_H
