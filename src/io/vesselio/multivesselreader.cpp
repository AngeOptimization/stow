#include "multivesselreader.h"
#include "baplievesselreader.h"

#include <ange/stowbase/vesselreader.h>
#include <ange/vessel/vessel.h>

#include <QIODevice>

#include <stdexcept>

using namespace ange::stowbase;
using namespace ange::vessel;

MultiVesselReader::MultiVesselReader()
  : m_stoVesselReader(0)
{
    // Empty
}

MultiVesselReader::~MultiVesselReader() {
    delete m_stoVesselReader;
}

Vessel* MultiVesselReader::readVessel(QIODevice* device) {
    delete m_stoVesselReader;
    m_stoVesselReader = new VesselReader();
    try {
            Q_ASSERT(device->isOpen());
        return m_stoVesselReader->readVessel(device); // First try as stowbase vessel
    } catch (const std::runtime_error& e1) {
            Q_ASSERT(device->isOpen());
        try {
            BaplieVesselReader baplieVesselReader;
            Q_ASSERT(device->isOpen());
            bool couldReset = device->reset();
            Q_ASSERT(device->isOpen());
            if (!couldReset) {
                throw std::runtime_error("Could not reset stream");
            }
            Vessel* vessel = baplieVesselReader.readVessel(device); // Second try as BAPLIE vessel
            delete m_stoVesselReader;
            m_stoVesselReader = 0;
            return vessel;
        } catch (const std::runtime_error& e2) {
            qDebug() << QString("BAPLIE vessel parse failed: %1").arg(e2.what());
            throw e1; // If BAPLIE parse also fails throw stowbase error
        }
    }
}

Vessel* MultiVesselReader::readVessel(const Bundle& bundle, const ReferenceObject& vesselRo) {
    delete m_stoVesselReader;
    m_stoVesselReader = new VesselReader();
    return m_stoVesselReader->readVessel(bundle, vesselRo);
}

VesselTank* MultiVesselReader::vesselTank(const QString& reference) {
    return m_stoVesselReader ? m_stoVesselReader->vesselTank(reference) : 0;
}
