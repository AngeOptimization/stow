#include "fileopener.h"

#include "document.h"
#include "stowagereader.h"
#include "baplieopener.h"

#include <QUndoCommand>

OnDiskData FileOpener::openStoFile(const QString& fileName) {
    OnDiskData load = StowageReader().read(fileName, 0);
    load.document->undostack()->clear(); // All the commands used to build the document should be cleared
    load.document->stowage_filename(fileName);
    return load;
}

OnDiskData FileOpener::openBaplieFile(const QString& fileName) {
    return BaplieOpener::openBaplieFile(fileName);
}

OnDiskData FileOpener::openFile(const QString& fileName) {
    if (fileName.endsWith(".edi", Qt::CaseInsensitive) || fileName.endsWith(".txt", Qt::CaseInsensitive)
        || fileName.endsWith(".baplie", Qt::CaseInsensitive)) {
        return openBaplieFile(fileName);
    } else {
        return openStoFile(fileName);
    }
}

OnDiskData FileOpener::openBlankStowage() {
    OnDiskData blank = FileOpener::openStoFile(":/data/data/blank.sto");
    blank.document->stowage_filename(QString());
    return blank;
}
