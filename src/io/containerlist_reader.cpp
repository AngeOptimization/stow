#include "containerlist_reader.h"

#include <algorithm>
#include "document/document.h"
#include "document/containers/container_list.h"
#include "document/stowage/stowage.h"
#include "document/stowage/container_leg.h"
#include "document/stowage/container_move.h"
#include "document/stowage/stowarbiter.h"

#include <ange/vessel/slot.h>
#include <ange/containers/container.h>
#include <ange/stowbase/containermovereader.h>
#include <ange/stowbase/containerreader.h>
#include <ange/stowbase/containerbundlingreader.h>
#include <ange/schedule/schedule.h>
#include <ange/schedule/call.h>
#include <ange/vessel/stack.h>
#include <ange/vessel/stacksupport.h>
#include "document/undo/container_list_change_command.h"
#include "document/undo/container_stow_command.h"
#include "document/pools/pool.h"
#include "document/utils/callutils.h"
#include "document/undo/merger_command.h"
#include <document/undo/bundlecontainercommand.h>

using ange::angelstow::MasterPlannedType;
using ange::angelstow::MicroStowedType;
using ange::angelstow::StowType;
using ange::stowbase::ReferenceObject;
using ange::stowbase::ContainerMoveReader;
using ange::stowbase::ContainerReader;
using ange::stowbase::ContainerBundlingReader;
using ange::stowbase::ContainerBundlingReader;
using ange::containers::Container;
using ange::schedule::Call;
using ange::vessel::Slot;

containerlist_reader_t::containerlist_reader_t(const Call* current_call) : m_current_call(current_call) {

}

void containerlist_reader_t::read(const ange::stowbase::ReferenceObject& stowage_ro,
                                  const ange::stowbase::Bundle& bundle,
                                  document_t* document,
                                  const ange::stowbase::ScheduleReader* schedule_reader
                                 ) {
  ContainerMoveReader move_reader(bundle, document->vessel(), schedule_reader);
  ContainerReader container_reader(bundle);
  //  Read moves from bundle
  QVariantList moves_refs = stowage_ro.readOptionalValue<QVariantList>("moves");
  if (!moves_refs.empty()) {
    Q_FOREACH(QVariant move_reference, moves_refs) {
      ContainerMoveReader::Move move = move_reader.read(bundle.reference(move_reference));
      add_move(move, &container_reader);
    }
  } else {
    // Legacy: Scan for moves
    Q_FOREACH(const ReferenceObject ro, bundle) {
      if (ro.group() == "move") {
        ContainerMoveReader::Move move = move_reader.read(ro);
        add_move(move, &container_reader);
      }
    }
  }

  //all normal containers should be read in by now
  //read in bundles
    QVariantList containerBundlingsRefs = stowage_ro.readOptionalValue<QVariantList>("containerBundlings");
    ContainerBundlingReader bundle_reader(bundle);
    QList<BundleContainerCommand*> bundleCommands;
    Q_FOREACH(QVariant bundlingReference, containerBundlingsRefs) {
        Q_ASSERT(bundlingReference.canConvert<QString>());
        QString bundlingReferenceString = bundlingReference.toString();
        ContainerBundlingReader::ContainerBundlingData  bundling= bundle_reader.read(bundlingReferenceString);
        Container* bottom_container = m_container_refs.value(bundling.first);
        Q_ASSERT(bottom_container);
        BundleContainerCommand* bundleCommand = new BundleContainerCommand(BundleContainerCommand::Add,document,bottom_container);
        Q_FOREACH(QString bundled_reference, bundling.second ) {
            Container* bundled_container = m_container_refs.value(bundled_reference);
            Q_ASSERT(bundled_container);
            bundleCommand->addContainer(bundled_container);
        }
        bundleCommands << bundleCommand;
    }

  // Set load&discharge, and moves
  QSharedPointer<stowage_signal_lock_t> lock(document->stowage()->prepare_for_major_stowage_change());
  merger_command_t* command = new merger_command_t(document->tr("Add containers from file"));
  container_list_change_command_t* add_command = new container_list_change_command_t(document);
  for (moves_t::iterator it = m_moves.begin(), iend=m_moves.end(); it!=iend; ++it) {
      Q_ASSERT(it.key());
    read_container_into_document(it.key(), *it, document, add_command);
  }
  command->push(add_command);
  for (moves_t::iterator it = m_moves.begin(), iend=m_moves.end(); it!=iend; ++it) {
    read_container_moves_into_document(document->containers()->get_container_by_id(it.key()->id()), *it, document, command);
  }
  //add all the bundling at the end.
  Q_FOREACH(BundleContainerCommand* bundleCommand, bundleCommands) {
        command->push(bundleCommand);
  }
  document->undostack()->push(command);

  qDeleteAll(m_moves.keys());
  m_moves.clear();
  m_container_refs.clear();
}

void containerlist_reader_t::read_container_into_document(ange::containers::Container* container, QList< ange::stowbase::ContainerMoveReader::Move > moves, document_t* document, container_list_change_command_t* add_command) {
  // This bit is loong and complicated.
  // First, determine the source and destination for the container. These will enable is to interpret moves correctly, hopefully.
  const ange::schedule::Schedule* schedule = document->schedule();

  const Call* source = 0L;
  const Call* destination = 0L;

  typedef QHash<QString, QPair<const ContainerMoveReader::Move*,const ContainerMoveReader::Move*> > ld_moves_per_uncode_t;
  ld_moves_per_uncode_t ld_moves_per_uncode;

  bool was_onboard = false;
  bool left_onboard = false;
  const Call* first_scheduled_call = 0;
  const Call* last_scheduled_call = 0;
  Q_FOREACH(const ContainerMoveReader::Move& move, moves) {
    if (move.call) {
      if (!first_scheduled_call || move.call->distance(first_scheduled_call)>0) {
        first_scheduled_call = move.call;
      }
      if (!last_scheduled_call || last_scheduled_call->distance(move.call)>0) {
        last_scheduled_call = move.call;
      }
    }
    if (move.slot) {
      // Check that move is to a location with a stack support, see ticket #313
      if (move.uncode.isEmpty()) {
        if (was_onboard) {
          qWarning("Container '%s' loaded with no port given twice", container->equipmentNumber().toLocal8Bit().data());
        }
        was_onboard = true;
        source = schedule->calls().front();
        continue;
      }
      if (ld_moves_per_uncode[move.call ? move.call->uncode() : move.uncode].first != 0) {
        qWarning("Container '%s' loaded in %s twice", container->equipmentNumber().toLocal8Bit().data(),move.uncode.toLocal8Bit().data());
      }
      ld_moves_per_uncode[move.call ? move.call->uncode() : move.uncode].first = &move;
    } else {
      if (move.uncode.isEmpty()) {
        if (left_onboard) {
          qWarning("Container '%s' discharged with no port given twice", container->equipmentNumber().toLocal8Bit().data());
        }
        left_onboard = true;
        destination = schedule->calls().back();
        continue;
      }
      if (ld_moves_per_uncode[move.uncode].second) {
        qWarning("Container '%s' discharged in %s twice", container->equipmentNumber().toLocal8Bit().data(),move.uncode.toLocal8Bit().data());
      }
      ld_moves_per_uncode[move.call ? move.call->uncode() : move.uncode].second = &move;
    }
  }
  QString source_uncode;
  QString destination_uncode;
  for (ld_moves_per_uncode_t::iterator it = ld_moves_per_uncode.begin(), iend = ld_moves_per_uncode.end(); it != iend; ++it) {
    if (it->first && !it->second) {
      Q_ASSERT(source_uncode.isEmpty());
      source_uncode = it->first->uncode;
      source = it->first->call;
    } else if (!it->first && it->second) {
      Q_ASSERT(destination_uncode.isEmpty());
      destination_uncode = it->second->uncode;
      destination = it->second->call;
    }
  }
  if (!destination && !left_onboard && !destination_uncode.isEmpty()) {
    destination = schedule->getCallByUncodeAfterCall(destination_uncode, last_scheduled_call ? last_scheduled_call : m_current_call);
    if (!destination) {
      destination = schedule->calls().last();
    }
  }
  if (!source && !was_onboard && !source_uncode.isEmpty()) {
    source = schedule->getCallByUncodeBeforeCall(source_uncode, first_scheduled_call ? first_scheduled_call : m_current_call);
    if (!source) {
      source = schedule->calls().front();
    }
  }
  if (!destination) {
    destination = schedule->calls().last();
    qWarning("No destination/discharge port given for container '%s'", container->equipmentNumber().toLocal8Bit().data());
  }
  if (!source) {
    source = schedule->calls().front();
    qWarning("No source/load port given for container '%s'", container->equipmentNumber().toLocal8Bit().data());
  }

  // Set interpreted load/discharge, and "raw" load/discharge
  if (!source_uncode.isEmpty()) {
    container->setLoadPort(source_uncode);
  }
  if (!destination_uncode.isEmpty()) {
    container->setDischargePort(destination_uncode);
  }
  add_command->add_container(container, source, destination);

}

void containerlist_reader_t::read_container_moves_into_document(ange::containers::Container* container, const QList< ange::stowbase::ContainerMoveReader::Move >& moves, document_t* document, merger_command_t* command)
{
  ange::schedule::Schedule* schedule = document->schedule();
  const Call* destination = document->stowage()->container_routes()->portOfDischarge(container);
  const Call* source = document->stowage()->container_routes()->portOfLoad(container);
  const bool was_onboard = is_befor(source);
  const bool left_onboard = is_after(destination);
  // Ok, at this stage we have source and destination, so we can use this to map all the moves
  QList<container_move_t> add_moves; // Since the container is brand new, we only have new moves for it.
  Q_FOREACH(const ContainerMoveReader::Move& move, moves) {
    if (move.isPseudo) {
      continue; // Pseudo-moves. These encode source/destination of containers into moves, but do not represent actual
      // planning into some slot. So ignore on this pass. (Note that source/destination might also be encoded into a
      // non-pseudo-move. Fun, eh?)
    }
    Q_ASSERT(move.slot != ange::vessel::UnknownPosition); // is_pseudo should garantee this
    const Call* call= move.call;
    if (!call) {
      if (!move.uncode.isEmpty()) {
        call = schedule->getCallByUncodeAfterCall(move.uncode, source);
        if (call && call->distance(destination)<(move.slot ? 1 : 0)) {
          // Discharged after destination or loaded at or after destination
          call = 0L;
        }
      } else {
        if (move.slot) {
          call =  was_onboard ? schedule->calls().front() : source; // Load with no port -> source or onbaord
        } else {
          call = left_onboard ? schedule->calls().back() : destination; // Discharge with no port -> destination or onboard
        }
      }
      if (!call) {
        // Presume this move is source/"before" if load and destination/"after" if discharge.
        if (move.slot) {
          call =  was_onboard ? schedule->calls().front() : source; // Load with no port -> source or onbaord
        } else {
          call = left_onboard ? schedule->calls().back() : destination; // Discharge with no port -> destination or onboard
        }
      }
    }
    QString stowage_type = move.rawObject().readOptionalValue<QString>("stowageType");
    StowType stow_type = MicroStowedType;
    if (stowage_type == "macro-stowed") {
      stow_type = MasterPlannedType;
    }
    if (move.slot && !add_moves.isEmpty() && !add_moves.back().slot() && add_moves.back().call() == call) {
      // We have a shift. So remove the discharge
      add_moves.pop_back();
    }
    add_moves << container_move_t(container, move.slot, call, stow_type);
  }

  std::sort(add_moves.begin(), add_moves.end());

  // Walk through and discard illegal moves (possibly ending with no moves)
  const Call* last_call = 0L;
  const Slot* current_slot = 0L;
  for (int i=0; i<add_moves.size(); ++i) {
    const container_move_t& m = add_moves.at(i);
    bool ok = true;
    if (ok && !current_slot && !m.slot()) {
      qWarning("Container '%s' discharged while on quay  at '%s'. This discharge will be ignored", container->equipmentNumber().toLocal8Bit().data(), m.call()->uncode().toLocal8Bit().data());
      ok = false;
    }
    if (ok && !m.call()) {
      qWarning("Container '%s' moved, but the port of movage was unspecified. Move will be ignored.", container->equipmentNumber().toLocal8Bit().data());
      ok = false;
    }
    if (ok && m.slot() && m.call()->distance(destination)<=0) {
      qWarning("Container '%s' loaded at or after destination call '%s'. Load will be ignored.", container->equipmentNumber().toLocal8Bit().data(), m.call()->uncode().toLocal8Bit().data());
      ok = false;
    }
    if (ok && !m.slot() && m.call()->distance(destination)<0) {
      qWarning("Container '%s' discharged after destination call '%s'. Discharge will be ignored.", container->equipmentNumber().toLocal8Bit().data(), m.call()->uncode().toLocal8Bit().data());
      ok = false;
    }
    // Check that move is legal
    const Call* next_call = destination;
    if (i<add_moves.size()-1) {
      next_call = add_moves.at(i+1).call();
    }
    StowArbiter arbiter(document->stowage(), document->schedule(), m.slot(), m.call());
    arbiter.setCutoff(next_call);
    arbiter.setEssentialChecksOnly(true);
    arbiter.setMicroplacedOnly(false);
    if (ok && m.slot() && !arbiter.isLegal(container)) {
      qWarning("Container '%s' loaded into an illegal position '%s' at '%s'",
               container->equipmentNumber().toLocal8Bit().data(),
               m.slot()->brt().toString().toLocal8Bit().data(),
               arbiter.toStrings(arbiter.whyNot(container)).join("; ").toLocal8Bit().data());
               ok = false;
    }
    if (m.call() == last_call) {
      qWarning("Container '%s' moved multiple times at '%s'", container->equipmentNumber().toLocal8Bit().data(), m.call()->uncode().toLocal8Bit().data());
      ok = false;

    }
    if (m.slot()) {
      ange::vessel::StackSupport* support = 0L;
      if (container->isoLength() == ange::containers::IsoCode::Twenty) {
        if (m.slot()->stack()->stackSupport20Aft() && m.slot()->stack()->stackSupport20Aft()->bay() == m.slot()->brt().bay()) {
          support = m.slot()->stack()->stackSupport20Aft();
        } else {
          support = m.slot()->stack()->stackSupport20Fore();
        }
      } else {
        support = m.slot()->stack()->stackSupport40();
      }
      // No support => invalid move
      ok = ok && support;
    }
    if (!ok) {
      add_moves.removeAt(i);
      --i;
      continue;
    }
    last_call = m.call();
    current_slot = m.slot();
  }

  // If container was left onboard with no discharge, discharge it at its destination
  if (current_slot) {
    qWarning("Container '%s' loaded but never discharged. Assumed discharged at destination", container->equipmentNumber().toLocal8Bit().data());
    add_moves << container_move_t(container, 0L, destination, add_moves.back().type());
  }

  #if 0
  // Useful debug when debugging loading
  qDebug() << add_moves << " s=" << source << " d=" << destination << " orig=" << moves;
  #endif
  if (!add_moves.empty()) {
    container_stow_command_t* stow_command = new container_stow_command_t(document, 0, true);
    stow_command->change_moves(container, QList<const container_move_t*>(), add_moves);
    command->push(stow_command);
  }

}

void containerlist_reader_t::add_move(const ange::stowbase::ContainerMoveReader::Move& raw_move, ange::stowbase::ContainerReader* container_reader){
  Q_FOREACH(QString cargo_reference, raw_move.cargo) {
    Container*& container = m_container_refs[cargo_reference];
    if (!container) {
      container = container_reader->read(cargo_reference);
      Q_ASSERT(container);
    }
    m_moves[container] << raw_move;
 }
}

containerlist_reader_t::~containerlist_reader_t() {

}
