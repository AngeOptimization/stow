#ifndef EDIPROBLEM_H
#define EDIPROBLEM_H

#include <QString>

/**
 * EdiProblem is an immutable object representing parsing errors and warnings
 */
class EdiProblem {

public:

    enum Type { Error, Warning, Info };

    EdiProblem(const Type& type, const QString& message);

    Type type() const;

    QString typeString() const;

    QString message() const;

private:
    Type m_type;
    QString m_message;

};

#endif // EDIPROBLEM_H
