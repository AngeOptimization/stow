#include "ediproblem.h"

EdiProblem::EdiProblem(const EdiProblem::Type& type, const QString& message)
  : m_type(type), m_message(message)
{
    // Empty
}

EdiProblem::Type EdiProblem::type() const {
    return m_type;
}

QString EdiProblem::typeString() const {
    switch (m_type) {
        case Error:
            return QString("ERROR");
        case Warning:
            return QString("WARNING");
        case Info:
            return QString("INFO");
        default:
            Q_ASSERT(false);
            return QString("UNKNOWN");
    }
}

QString EdiProblem::message() const {
    return m_message;
}
