#include "parserproblem.h"
#include <ange/schedule/call.h>
#include <stdexcept>

ParserProblem* ParserProblem::unmappedPort(const QString& uncode, const QString& voyage_code,
                                           const ange::schedule::Call* first_possible_call,
                                           const ange::schedule::Call* last_possible_call) {
    // first_possible_call and last_possible_call will be null when m_load call is not set and mappings for other ports are missing
    if (first_possible_call && last_possible_call && first_possible_call->distance(last_possible_call) < 0) {
        // Later when rv->storeData("first_possible_call", ...) is called in edifact_container_reader_t this not checked again
        throw std::invalid_argument(QString("Empty possible interval for unmapped port %1 voyage %2 between %3 and %4").arg(uncode).arg(voyage_code).arg(first_possible_call->uncode()).arg(last_possible_call->uncode()).toStdString());
    }
    ParserProblem* rv = new ParserProblem(ERROR_, UnmappedPort);
    rv->setDetails(tr("Unable to automatically map %1 %2 to any call in schedule").arg(uncode).arg(voyage_code));
    rv->storeData("uncode", uncode);
    rv->storeData("voyage_code", voyage_code);
    rv->storeData("first_possible_call", QVariant::fromValue(first_possible_call));
    rv->storeData("last_possible_call", QVariant::fromValue(last_possible_call));
    return rv;
}

ParserProblem* ParserProblem::parserError(const QString& name, const QString& description) {
    ParserProblem* rv = new ParserProblem(ERROR_, ParserError);
    rv->storeData("name", name);
    rv->setDetails(tr("Parse error while parsing %2 occurred: %1").arg(description).arg(name));
    return rv;
}
ParserProblem* ParserProblem::parserWarning(const QString& name, const QString& description) {
    ParserProblem* rv = new ParserProblem(WARNING, ParserError);
    rv->storeData("name", name);
    rv->setDetails(tr("Warning during parsing %2: %1").arg(description).arg(name));
    return rv;
}



void ParserProblem::fixedSlot() {
    emit fixed(this);
    deleteLater();
}
void ParserProblem::solve(QVariant solution) {
    setProperty("solution", solution);
    emit fixed(this);
}

QVariant ParserProblem::data(QString key) const {
    return property(key.toLocal8Bit());
}

const QString& ParserProblem::details() const {
    return m_details;
}

ParserProblem::ParserProblem(const ParserProblem::severity_t& severity, const ParserProblem::Type& type, QObject* parent)
  : QObject(parent), m_severity(severity), m_type(type)
{
    setObjectName("ParserProblem");
}

void ParserProblem::setDetails(const QString& details) {
    m_details = details;
}

ParserProblem::severity_t ParserProblem::severity() const {
    return m_severity;
}

void ParserProblem::storeData(QString key, QVariant value) {
    setProperty(key.toLocal8Bit(), value);
}

ParserProblem::Type ParserProblem::type() const {
    return m_type;
}

#include "parserproblem.moc"
