#ifndef PARSERPROBLEM_H
#define PARSERPROBLEM_H

#include <QObject>

namespace ange {
    namespace schedule {
        class Call;
    } // namespace
} // namespace

/**
 * Class to represent a problem found when parsing edifact files using the medici based edifact readers
 */
class ParserProblem : public QObject {
    Q_OBJECT
    public:
        /**
        * The severity of a problem. It can be
        * 1) Notice. nice to know, but no need to actually fix it.
        * 2) Warning. Should really be fixed.
        * 3) Error. Must be fixed.
        */
        enum severity_t {
            NOTICE,
            WARNING,
            ERROR_
        };
        enum Type {
           UnmappedPort,
           ParserError
        };
        /**
        * Construct an unmapped port problem
        * @param uncode uncode for port
        * @param voyage_code for port. Empty if unknown
        * @param first_possible_call if non-null, this marks the first call that this call can be mapped to
        * @param last_possible_call if non-null, this marks the last call that this call can be mapped to
        */
        static ParserProblem* unmappedPort(const QString& uncode,
                                        const QString& voyage_code,
                                        const ange::schedule::Call* first_possible_call,
                                        const ange::schedule::Call* last_possible_call);


        /**
        * Error occurred in some parsing
        * This is a catch-all when parser cannot give a better problem description.
        * This problem probably cannot be resolved.
        * @param name name of file or resource where the error occurred
        * @param description human readable description of the problem
        */
        static ParserProblem* parserError(const QString& name, const QString& description);

        /**
        * Warning occurred in some parsing
        * This is a catch-all when parser cannot give a better problem description.
        * This problem probably cannot be resolved.
        * @param name name of file or resource where the error occurred
        * @param description human readable description of the problem
        */
        static ParserProblem* parserWarning(const QString& name, const QString& description);

        /**
        * @override
        */
        QVariant data(QString key) const;

        /**
        * @override
        */
        void storeData(QString key, QVariant value);
        /**
        * @return the problem severity
        */
        severity_t severity() const;
        /**
        * @return the details of the problem
        */
        const QString& details() const;
        /**
        */
        void setDetails(const QString& details);
        Type type() const;
    public Q_SLOTS:
        /**
        * connect to this slot when a problem is fixed
        * By default, this deletes the problem.
        */
        void fixedSlot();

        /**
        * Solve this slot.
        * Will notify any listeners (via the fixed signal) that this problem has been solved.
        * @param solution (stored as a piece of data under the key "solution")
        */
        void solve(QVariant solution);
    Q_SIGNALS:
        /**
         * emits fixed(this) when fixed_slot() is invoked;
         */
        void fixed(ParserProblem*);

    private:
        severity_t m_severity;
        Type m_type;
        QString m_details;

        /**
        * Constructor
        * @param severity of the problem
        * @param type of the problem
        * @param parent, as in QObject of the parent.
        * In time, I'd like this to be depreciated and use static constructors instead
        */
        ParserProblem(const severity_t& severity, const Type& type,QObject* parent=0);
};

#endif // PARSERPROBLEM_H
