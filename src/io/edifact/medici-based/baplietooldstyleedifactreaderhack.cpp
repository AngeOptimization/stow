#include "baplietooldstyleedifactreaderhack.h"

#include "baplieparser.h"
#include "document.h"
#include "ediproblem.h"
#include "parserproblem.h"

#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>

#include <QtEdith/edifactsegmentreader.h>

#include <QFile>
#include <QString>

using namespace ange::containers;
using namespace ange::edifact;
using namespace ange::schedule;
using namespace QtEdith;

BaplieToOldStyleEdifactReaderHack::BaplieToOldStyleEdifactReaderHack(document_t* document, pool_t* pool,
                                                                     const BaplieParserResult& result, const QString& fileName)
  : edifact_container_reader_t(document, pool, 0)
{
    Q_ASSERT(!result.hasErrors());

    // Try to find m_load_call (could be changed to always use currentCall)
    Q_ASSERT(!m_load_call);
    Q_FOREACH (const Call* call, document->schedule()->calls()) {
        if (call->voyageCode() == result.voyageCode && call->uncode() == result.departurePortCode) {
            m_load_call = call;
            break;
        }
    }

    m_filename = fileName;

    Q_ASSERT(m_problems.isEmpty());
    Q_FOREACH (const EdiProblem& problem, result.problemList) {
        switch (problem.type()) {
            case EdiProblem::Error: { // Not really needed as we should not have errors
                m_problems << ParserProblem::parserError("Error BAPLIE '" + fileName + "'", problem.message());
                break;
            }
            case EdiProblem::Warning: {
                m_problems << ParserProblem::parserWarning("Warning BAPLIE '" + fileName + "'", problem.message());
                break;
            }
            case EdiProblem::Info: {
                m_problems << ParserProblem::parserWarning("Info BAPLIE '" + fileName + "'", problem.message());
                break;
            }
        }
    }

    handle_doc_type(edifact_parser_t::BAPLIE);

    handle_doc_meta(edifact_parser_t::MESSAGE_TIME, result.messageTime.toString());
    handle_doc_meta(edifact_parser_t::VOYAGE_CODE, result.voyageCode);
    handle_doc_meta(edifact_parser_t::CARRIER_CODE, result.carrierCode);
    handle_doc_meta(edifact_parser_t::VESSEL_CODE, result.imoNumber.isEmpty() ? result.callSign : result.imoNumber);
    handle_doc_meta(edifact_parser_t::VESSEL_NAME, result.vesselName);
    handle_doc_meta(edifact_parser_t::PORT_UNCODE, result.departurePortCode);

    m_forcePort = result.departurePortCode;

    Q_FOREACH (const BaplieParserResult::ContainerPosition& cp, result.containerList()) {
        handle_container_location(*cp.container.data(), cp.position);
    }

    if (!m_load_call) {
        // Register a problem for mapping the m_load_call for later resolving.
        Schedule* schedule = document->schedule();
        ParserProblem* problem = ParserProblem::unmappedPort(result.departurePortCode, result.voyageCode,
                                                             schedule->at(0), schedule->at(schedule->size()-2));
        m_problems.prepend(problem);
        connect(problem, &ParserProblem::fixed, this, &edifact_container_reader_t::problem_with_load_port_solved);
    } else {
        // Update port mapping problems with m_load_call
        update_problems_with_load_call();  // must be called after all calls to handle_container_location()
    }

    m_parsed = true;
}

BaplieToOldStyleEdifactReaderHack::~BaplieToOldStyleEdifactReaderHack() {
    // Empty
}

edifact_container_reader_t* BaplieToOldStyleEdifactReaderHack::testRead(const QString& fileName, document_t* document, pool_t* pool) {
    QFile baplieFile(fileName);
    baplieFile.open(QIODevice::ReadOnly);
    EdifactSegmentReader ediReader(&baplieFile);

    Q_ASSERT(ediReader.hasNext());
    Segment unb = ediReader.next();
    Q_ASSERT(unb.tag() == QByteArrayLiteral("UNB"));

#ifndef NDEBUG
    Q_ASSERT(ediReader.hasNext());
    Segment unh = ediReader.peek();
    Q_ASSERT(unh.tag() == QByteArrayLiteral("UNH"));
    Q_ASSERT(BaplieParser::acceptedUnh(unh));
#endif

    BaplieParser parser;
    parser.parse(&ediReader);
    BaplieParserResult result = parser.result();
    BaplieToOldStyleEdifactReaderHack* reader = new BaplieToOldStyleEdifactReaderHack(document, pool, result, fileName);

#ifndef NDEBUG
    Q_ASSERT(ediReader.hasNext());
    Segment unz = ediReader.next();
    Q_ASSERT(unz.tag() == QByteArrayLiteral("UNZ"));

    Q_ASSERT(!ediReader.hasNext());
#endif

    return reader;
}
