#include "edifact_container_reader.h"

#include "document/document.h"
#include "document/undo/container_list_change_command.h"
#include "document/containers/container_list.h"
#include "document/stowage/container_leg.h"
#include "document/stowage/stowage.h"
#include "document/undo/container_stow_command.h"
#include "document/pools/pool.h"
#include "document/stowage/container_move.h"
#include "document/undo/merger_command.h"
#include "document/stowage/stowarbiter.h"
#include "document/tanks/tankconditions.h"
#include "bundlecontainercommand.h"
#include "document/undo/tank_state_command.h"
#include "parserproblem.h"

#include <stowplugininterface/stowtype.h>
#include <ange/containers/oog.h>
#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>
#include <ange/vessel/vessel.h>
#include <ange/vessel/slot.h>
#include <ange/vessel/vesseltank.h>

#include <QUndoStack>
#include <QFile>
#include <QDialogButtonBox>

#include <stdexcept>

using ange::angelstow::MicroStowedType;
using ange::schedule::Call;
using ange::containers::Container;
using ange::containers::EquipmentNumber;
using ange::edifact::edifact_parser_t;
using ange::vessel::BayRowTier;
using ange::vessel::Slot;
using ange::containers::IsoCode;
using ange::schedule::Schedule;

void edifact_container_reader_t::handle_container_location(const ange::containers::Container& container, const ange::vessel::BayRowTier& position) {
    ange::containers::ContainerUndoBlob* undoData = container.undoData();
    Container* copy = Container::createFromUndoData(undoData);
    Container::deleteUndoData(undoData);

    bool containerOk = true;
    // fix missing load+disch ports
    QString dischPort = container.dischargePort();
    if (dischPort.isEmpty()) {
        containerOk = false;
        dischPort = "ZZPOD"; // force user to map unknown discharge port
    }
    QString loadPort = container.loadPort();
    if(loadPort.isEmpty()) {
        containerOk = false;
        loadPort = "ZZPOL"; // force user to map unknown load port
    }
    copy->setDischargePort(dischPort);
    copy->setLoadPort(loadPort);

    verify_mapping(dischPort, false);
    verify_mapping(loadPort, true);

    m_containers << copy;

    if(containerOk && position.placed()){
        m_containerlocations.insert(copy,position);
    }
}

void edifact_container_reader_t::verify_mapping(QString uncode, bool is_load)
{
  Schedule* schedule = m_document->schedule();
  // Ask for mapping if uncode not known in schedule
  QHash<QString, const Call*>& mapping = is_load ? m_load_port_mapping : m_discharge_port_mapping;
  if (!mapping.contains(uncode)) {
    mapping.insert(uncode, 0L); // default resolution: no mapping
    if (!schedule->getCallByUncodeAfterCall(uncode, schedule->at(0))) { // If no automatic mapping possible, create problems
      ParserProblem* problem = ParserProblem::unmappedPort(uncode, QString(), 0L, 0L);
      m_problems << problem;
      problem->storeData(is_load ? "load_mapping" : "discharge_mapping", true);
      connect(problem, &ParserProblem::fixed, this, &edifact_container_reader_t::problem_with_unmapped_port_solved);
    }
  }
}


void edifact_container_reader_t::handle_doc_type(const ange::edifact::edifact_parser_t::doc_type_t type) {
  m_doctype = type;
}

edifact_container_reader_t::edifact_container_reader_t(document_t* document, pool_t* selected_pool, const ange::schedule::Call* currentCall):
    edifact_parser_t(),
    m_document(document),
    m_selected_pool(selected_pool),
    m_parsed(false),
    m_containers(),
    m_problems(),
    m_filename(),
    m_load_call(0L),
    mCurrentCall(currentCall)
{
    setObjectName("EdifactContainerReader");
}

edifact_container_reader_t::~edifact_container_reader_t()
{
//   This list of containers are only copies, and the actual containers are created elsewhere.
  qDeleteAll(m_containers);
}

bool edifact_container_reader_t::parse(QString filename) {
  try {
    m_filename = filename;
    QFile edifact_file(filename);
    if (edifact_file.open(QIODevice::ReadOnly)) {
      edifact_parser_t::parse(&edifact_file);
    }
    Q_ASSERT(m_doctype != ange::edifact::edifact_parser_t::BAPLIE);

    // registering load call problem for BAPLIE, COARRI, COPRAR, but _not_ for TANSTA
    if (doctype() == ange::edifact::edifact_parser_t::BAPLIE
        || doctype() == ange::edifact::edifact_parser_t::COARRI
        || doctype() == ange::edifact::edifact_parser_t::COPRAR) {
      const ange::schedule::Schedule* schedule = m_document->schedule();
      QString voyage_code = this->voyage_code();
      QString port = this->port();
      Q_FOREACH(const Call* call, schedule->calls()) {
        if (call->voyageCode() == voyage_code && call->uncode() == port) {
          m_load_call = call;
          break;
        }
      }
      if (!m_load_call) {
        // Register the problem for later resolving.
        ParserProblem* problem = ParserProblem::unmappedPort(port, voyage_code, schedule->at(1), schedule->at(schedule->size()-2));
        m_problems.push_front(problem);
        connect(problem, &ParserProblem::fixed, this, &edifact_container_reader_t::problem_with_load_port_solved);
      } else {
        update_problems_with_load_call();
      }
    }
    m_parsed = true;
    return true;
  } catch (std::exception& e) {
    qWarning() << "Failed to parse EDIFACT file '" << filename <<  "':" << e.what();
    ParserProblem* problem = ParserProblem::parserError(filename, e.what());
    m_problems.push_front(problem);
    return false;
  }
#ifndef NDEBUG
    Q_FOREACH(const ParserProblem* problem, m_problems) {
        Q_ASSERT(problem->data("first_possible_call").value<const Call*>());
    }
#endif
}

struct BundleKey { // local to member function bundleCollapsibleContainers(), but local template classes not allowed in pre C++11
    QString loadPort;
    QString dischargePort;
    ange::vessel::BayRowTier bayRowTier;
    ange::containers::IsoCode::IsoLength length;

    bool operator==(const BundleKey& other) const {
        return  loadPort == other.loadPort &&
                dischargePort == other.dischargePort &&
                bayRowTier == other.bayRowTier &&
                length == other.length;

    }
};

int qHash(const BundleKey bk) {
    return qHash(bk.loadPort + bk.dischargePort + bk.bayRowTier.toString());
}

QList<BundleContainerCommand*> edifact_container_reader_t::bundleCollapsibleContainers() {
    Q_ASSERT(m_parsed);

    QList<BundleContainerCommand*> bundleCommands;
    if (m_containers.isEmpty()|| m_containerlocations.isEmpty()) {
        return bundleCommands;
    }

    QHash<BundleKey, QList<ange::containers::Container*> > bundleContainers;

    Q_FOREACH(ange::containers::Container* container , m_containers) {
        if (container->isBundlable()) {
            BundleKey key;
            key.loadPort = container->loadPort();
            key.dischargePort = container->dischargePort();
            key.bayRowTier = m_containerlocations.value(container);
            key.length = container->isoLength();
            bundleContainers[key] << container;
        }
    }

    Q_FOREACH(const BundleKey key, bundleContainers.keys()){
        if (bundleContainers[key].length() > 1) {
            Container* firstContainer = bundleContainers.value(key).front();
            BundleContainerCommand* cmd = new BundleContainerCommand(BundleContainerCommand::Add,m_document,firstContainer);
            Q_FOREACH(const ange::containers::Container* container, bundleContainers.value(key)){
                if(container == firstContainer) {
                    continue;
                }
                cmd->addContainer(container);
                m_containerlocations.remove(container);
            }
            bundleCommands << cmd;
        }
    }
    return bundleCommands;
}

void edifact_container_reader_t::ensureEquipmentId() {
    if (m_containers.isEmpty()) {
        return;
    }

    QList<Container*> containersWithEmptyEquipment;
    Q_FOREACH(Container* container, m_containers) {
        if (container->equipmentNumber().isEmpty()) {
            containersWithEmptyEquipment << container;
        }
    }

    if (containersWithEmptyEquipment.isEmpty()) {
        return;
    }

    QList <EquipmentNumber> eqNoList = EquipmentNumber::generateRandomEquipmentNumbers(containersWithEmptyEquipment.size());
    Q_FOREACH(Container* container, containersWithEmptyEquipment) {
        container->setEquipmentNumber(eqNoList.takeFirst());
    }
}

// TODO #688 hack to be corrected in #1370 : filter Tank stuff out of this class into own edi TANSTA parser
void edifact_container_reader_t::handleTankCondition(const TankCondition& tankCondition) {
    TankMapping* tm = new TankMapping();
    tm->ediTankCondition = tankCondition;

    //bool matched = false;
    Q_FOREACH(ange::vessel::VesselTank* tank, m_document->vessel()->tanks()) {
        // matching TANSTA tanks with vessel profile tanks on either TANSTA tank code or tank name
        if ( (tank->description() == tm->ediTankCondition.tankCode) || (tank->description() == tm->ediTankCondition.tankName) ) {
            tm->vesselTank = tank;
            break;
        }
    }
    mTankMappings.append(tm);
}

void edifact_container_reader_t::addTanksToDocument() {
    Q_ASSERT(m_parsed);
    if (mTankMappings.isEmpty()) {
        return; // In this case, there is nothing to add
    }

    tank_state_command_t* tank_state = new tank_state_command_t(m_document, mCurrentCall);
    Q_FOREACH(const TankMapping* tankMapping, tankMappings()) {
        if (tankMapping->vesselTank) {
            tank_state->insert_tank_condition(tankMapping->vesselTank, tankMapping->ediTankCondition.weight);
        }
    }
    m_document->undostack()->push(tank_state);
}


void edifact_container_reader_t::add_containers_to_document() {
  Q_ASSERT(m_parsed);
  if (m_containers.isEmpty()) {
    return; // In this case, there is nothing to add
  }
  if (!m_load_call) {
    throw std::runtime_error(QString("Failed to determine port for name \"%1\" and voyage code \"%2\"").arg(port()).arg(voyage_code()).toStdString());
    return;
  }

  ensureEquipmentId();

  QList<BundleContainerCommand*> bundleCommands = bundleCollapsibleContainers();

  ange::schedule::Schedule* schedule = m_document->schedule();
  QUndoStack* undo_stack = m_document->undostack();
  QScopedPointer<merger_command_t> import_edifact(new merger_command_t(tr("Import EDIFACT file %1").arg(m_filename)));
  stowage_t* stowage = m_document->stowage();
  const ContainerLeg* container_route = stowage->container_routes();
  QSharedPointer<stowage_signal_lock_t> lock(stowage->prepare_for_major_stowage_change());
  QHash<const Container*, QList<container_move_t> > existing_container_moves;
  if(m_doctype == ange::edifact::edifact_parser_t::BAPLIE || m_doctype == ange::edifact::edifact_parser_t::COARRI ){
    container_stow_command_t* unstow_cmd = new container_stow_command_t(m_document, m_selected_pool, false);
    container_list_change_command_t* del_cmd = new container_list_change_command_t(m_document);
    // To enable loading a release BAPLIE or a release COARRI on top of an existing stowage,
    // we need to prepare the document, we need to delete containers which are also going to be imported:
    // Remove containers in stowage which have equipment numbers found in BAPLIE or COARRI to avoid duplicates
    QSet<const Container*> mapped_containers;
    Q_FOREACH(Container* edifact_container, m_containers) {
      if (const Container* stowed_container = m_document->containers()->get_container_by_equipment_number(edifact_container->equipmentNumber())) {
        if(stowed_container->bundleParent() || !stowed_container->bundleChildren().isEmpty()) {
            const Container* bundleParentConst = stowed_container->bundleParent() ? stowed_container->bundleParent() : stowed_container;
            Container* bundleParent = m_document->containers()->get_container_by_id(bundleParentConst->id());
            BundleContainerCommand* command = new BundleContainerCommand(BundleContainerCommand::Remove,m_document,bundleParent);
            Q_FOREACH(Container* bundleChild, bundleParent->bundleChildren()) {
                command->addContainer(bundleChild);
            }
            import_edifact->push(command);
        }
        mapped_containers << stowed_container;
        QList<container_move_t> existing_moves;
        Q_FOREACH(const container_move_t* stowed_move, stowage->moves(stowed_container)) {
          existing_moves << container_move_t(edifact_container, stowed_move->slot(), stowed_move->call(), stowed_move->type());
        }
        existing_container_moves.insert(edifact_container, existing_moves);
        unstow_cmd->add_discharge(stowed_container, container_route->portOfLoad(stowed_container), MicroStowedType);
        del_cmd->remove_container(stowed_container);
      }
    }
    // Containers that remain on board at current port are compared to release BAPLIE or COARRI containers to see if they should be discharged here.
    Q_FOREACH(const Container* stowed_container, m_document->containers()->list()) {
        if (mapped_containers.contains(stowed_container)) {
            continue;
        }

        // all BAPLIE or COARRI containers with stowage position at current call 'm_load_call', but never loaded in release BAPLIE or COARRI
        // - are unstowed if planned loaded in current call
        // - are unstowed if already on ship from earlier call and the document is BAPLIE
        if (const Slot* slot = stowage->container_position(stowed_container, m_load_call)) {
            if (container_route->portOfLoad(stowed_container) == m_load_call
                || m_doctype == ange::edifact::edifact_parser_t::BAPLIE ) {
                unstow_cmd->add_discharge(stowed_container, m_load_call, MicroStowedType);
                m_unplacedcontainerlocations << container_error_t(stowed_container,slot->brt(),QString("Container moved to yard at %1").arg(m_load_call->uncode()));
            }
        }
    }
    import_edifact->push(unstow_cmd);
    import_edifact->push(del_cmd);
  }

  // BAPLIE, COARRI, COPRAR and MOVINS - create container list to be added
  container_list_change_command_t*  add_containers = new container_list_change_command_t(m_document);
  Q_FOREACH(Container* container, m_containers) {
    const Call* discharge_call = m_discharge_port_mapping.value(container->dischargePort(), 0L);
    if (!discharge_call) {
      discharge_call = schedule->getCallByUncodeAfterCall(container->dischargePort(), m_load_call);
    }
    if (!discharge_call) {
      discharge_call = schedule->calls().last();
    }
    const Call* load_call = find_load_call_for_container(container);
    add_containers->add_container(container,
                                  load_call,
                                  discharge_call);
  }
  import_edifact->push(add_containers);
  Q_FOREACH(Container* edifact_container, m_containers) {
    QHash<const Container*,BayRowTier>::const_iterator brtit = m_containerlocations.constFind(edifact_container);
    if(brtit != m_containerlocations.constEnd()){
      const BayRowTier brt = brtit.value();
      const Container* container = m_document->containers()->get_container_by_id(edifact_container->id());
      const Call* load_call = stowage->container_routes()->portOfLoad(container);
      const Call* discharge_call = stowage->container_routes()->portOfDischarge(container);
      Q_ASSERT(container);
      Q_ASSERT(discharge_call);
      Q_ASSERT(load_call);
      if (const ange::vessel::Slot* slot = m_document->vessel()->slotAt(brt) ) {
        // If this is a container already in the stowage, find out it's position and reuse (rebuild) moves until this position)
        QList<container_move_t> moves;

        QList<container_move_t> existing_moves = existing_container_moves.value(edifact_container);
        if (!existing_moves.empty() && existing_moves.front().call() == load_call) {
          // Add any old shiftings
          for (int i=1; i<existing_moves.size(); ++i) {
            const container_move_t& move_a = existing_moves.at(i-1);
            const container_move_t& move_b = existing_moves.at(i);
            if (m_load_call->distance(move_b.call())<0) {
              StowArbiter arbiter(stowage, m_document->schedule(), move_a.slot(), move_a.call());
              arbiter.setEssentialChecksOnly(true);
              arbiter.setCutoff(move_b.call());
              if (arbiter.isLegal(container)) {
                moves << container_move_t(container, move_a.slot(), move_a.call(), move_a.type());
              } else {
                // Abort. This is probably due to mismatched port mapping. This will make the import load the container as if the stowed_moves were non-existent.
                moves.clear();
                break;
              }
            } else if (move_a.call()->distance(m_load_call)>0) {
              StowArbiter arbiter(stowage, m_document->schedule(), move_a.slot(), move_a.call());
              arbiter.setEssentialChecksOnly(true);
              arbiter.setMicroplacedOnly(false);
              arbiter.setCutoff(m_load_call);
              if (arbiter.isLegal(container)) {
                moves << container_move_t(container, move_a.slot(), move_a.call(), move_a.type());
              } else {
                // Abort. This is probably due to mismatched port mapping. This will make the import load the container as if the stowed_moves were non-existent.
                moves.clear();
                break;
              }
            }
          }
        }
        if (!moves.empty() && existing_moves.last().call()->distance(m_load_call)>0) {
          // If the discharge call has changed, or there somehow else is an interval between the existing moves
          // and the current port (m_load_call), a conflict could appear in this space. So check for it.
          StowArbiter arbiter(stowage, m_document->schedule() ,moves.last().slot(), moves.last().call());
          arbiter.setEssentialChecksOnly(true);
          arbiter.setMicroplacedOnly(false);
          arbiter.setCutoff(m_load_call);
          const Call* available_until = arbiter.slotAvailableUntil(container);
          if (available_until->distance(m_load_call)>0) {
            // Attempt to resolve this by shifting earlier
            arbiter.setCutoff(available_until);
            if (arbiter.isLegal(container)) {
              // check that we can shift TO slot
              StowArbiter postshift_arbiter(stowage, m_document->schedule(), slot, available_until);
              postshift_arbiter.setEssentialChecksOnly(true);
              postshift_arbiter.setMicroplacedOnly(false);
              postshift_arbiter.setCutoff(m_load_call);
              if (postshift_arbiter.isLegal(container)) {
                // ok, perform shift
                moves << container_move_t(container, slot, available_until, MicroStowedType);
              } else {
                // unable to shift to current position. clear moves, this container cannot be loaded. Error and so on
                // will be handled futher down.
                moves.clear();
              }
            } else {
              // Not legal at old slot for some other reason (this is pretty unlikely). Abort
              moves.clear();
            }
          } else if (!arbiter.isLegal(container)) {
            // Abort. Something has happened in the interim (which we cannot know anything about).
            moves.clear();
          }
        }

        // If still needed of transferring the existing moves (if any), load the container
        if (moves.empty() || moves.last().slot() != slot) {
          bool loaded = false;
          const Call* next_call = schedule->next(m_load_call);
          for (const Call* from_call = moves.empty() ? load_call : m_load_call; !loaded && from_call != next_call; from_call = schedule->next(from_call)) {
            StowArbiter arbiter(stowage, m_document->schedule() ,slot, from_call);
            arbiter.setEssentialChecksOnly(true);
            arbiter.setMicroplacedOnly(false);
            arbiter.setCutoff(next_call);
            if (arbiter.isLegal(container)) {
              moves << container_move_t(container, slot, from_call, MicroStowedType);
              if (load_call != from_call) {
                m_containers_pol_remapped << container_pol_remapped_t(container, brt, load_call, from_call);
                //load_call = from_call; // Map the load call to the earliest possible, see ticket #551.
                //This is not a good idea, as it can drastically impact the history of the stowage (which should be changed as little as
                //possible (only minor error corrections), since it is history)
                //instead of using the first possible call, we set it to the actual port (the port the baplie applies to)
                load_call = m_load_call;
              }
              loaded = true;
            }
          }
          if (!loaded) {
            moves.clear(); // Not legal at slot or before: don't load it at all.
            StowArbiter arbiter(stowage, m_document->schedule(), slot, m_load_call);
            arbiter.setEssentialChecksOnly(true);
            arbiter.setMicroplacedOnly(false);
            arbiter.setCutoff(next_call);
            m_unplacedcontainerlocations << container_error_t(container, brt, arbiter.toStrings(arbiter.whyNot(container)).join("; "));
          }
        }

        // If loaded, discharge
        if (!moves.empty()) {
          StowArbiter arbiter(stowage, m_document->schedule() ,slot, moves.last().call());
          arbiter.setEssentialChecksOnly(true);
          arbiter.setMicroplacedOnly(false);
          while (!arbiter.isLegal(container)) {
                const Call* offendingLoadCall = arbiter.slotAvailableUntil(container);
                Q_ASSERT(offendingLoadCall);
                slot_call_content_t content = stowage->contents(slot,offendingLoadCall);
                const Container* offendingContainer = content.container();
                if(!offendingContainer) {
                    Q_ASSERT(container->isoLength() != IsoCode::Twenty);
                    Q_ASSERT(slot->sister()),
                    //let's try the sister slot
                    content = stowage->contents(slot->sister(), offendingLoadCall);
                    offendingContainer = content.container();
                }
                Q_ASSERT(offendingContainer);
                container_stow_command_t* cmd = new container_stow_command_t(m_document ,m_selected_pool, false);
                cmd->add_discharge(offendingContainer,offendingLoadCall,content.type());
                import_edifact->push(cmd);
                m_unplacedcontainerlocations << container_error_t(offendingContainer,slot->brt(),QString("Future container placing undone: %1").arg(offendingLoadCall->uncode()));
                arbiter.setEssentialChecksOnly(true); // this also resets the internal state to ask the stowage again.
          }
          moves << container_move_t(container, 0L, discharge_call, MicroStowedType);
        }
        if (!moves.empty()) {
          container_stow_command_t* stow_command = new container_stow_command_t(m_document, m_selected_pool, false);
          stow_command->change_moves(container, QList<const container_move_t*>(), moves);
          import_edifact->push(stow_command);
        }
      } else {
        // Unknown brt->slot
        m_unplacedcontainerlocations << container_error_t(container,brt, tr("No such position on vessel"));
      }
    }
  }
    Q_FOREACH (BundleContainerCommand* bundleCommand, bundleCommands){
        import_edifact->push(bundleCommand);
    }
    undo_stack->push(import_edifact.take());
}

QList<ParserProblem*> edifact_container_reader_t::problems() const {
  return m_problems;
}

QString edifact_container_reader_t::error_message() const {
  QString rv;
  Q_FOREACH(ParserProblem* problem, m_problems) {
    if (problem->severity() == ParserProblem::ERROR_) {
      rv += "Error: "  + problem->details() + "\n";
    }
  }
  return rv;
}

void edifact_container_reader_t::handle_error(int error) {
  edifact_parser_t::handle_error(error); // TODO: Create a proper error, remove the catch
}

void edifact_container_reader_t::handle_warning(int error) {
  m_problems << ParserProblem::parserWarning(m_filename, error_string(error));
}

bool edifact_container_reader_t::has_errors() const {
  Q_FOREACH(ParserProblem* problem, m_problems) {
    if (problem->severity() == ParserProblem::ERROR_) {
      return true;
    }
  }
  return false;
}

void edifact_container_reader_t::problem_solved(ParserProblem* problem) {
  m_problems.removeAll(problem);
  problem->deleteLater();
}

void edifact_container_reader_t::problem_with_load_port_solved(ParserProblem* problem) {
  m_load_call = problem->data("solution").value<const ange::schedule::Call*>();
  if (m_load_call) {
    update_problems_with_load_call();
    problem_solved(problem);
  }
}

void edifact_container_reader_t::update_problems_with_load_call() {
    const Schedule* schedule = m_document->schedule();
    Q_FOREACH (ParserProblem* problem, m_problems) {
        if (problem->data("load_mapping").toBool()) {
            Q_ASSERT(!problem->data("first_possible_call").value<const Call*>());
            Q_ASSERT(!problem->data("last_possible_call").value<const Call*>());
            problem->storeData("first_possible_call", QVariant::fromValue<const Call*>(schedule->calls().first()));
            problem->storeData("last_possible_call", QVariant::fromValue<const Call*>(m_load_call));
        } else if (problem->data("discharge_mapping").toBool()) {
            Q_ASSERT(!problem->data("first_possible_call").value<const Call*>());
            Q_ASSERT(!problem->data("last_possible_call").value<const Call*>());
            problem->storeData("first_possible_call", QVariant::fromValue<const Call*>(schedule->next(m_load_call)));
            problem->storeData("last_possible_call", QVariant::fromValue<const Call*>(schedule->calls().last()));
        }
    }
}

void edifact_container_reader_t::problem_with_unmapped_port_solved(ParserProblem* problem) {
  const Call* call = problem->data("solution").value<const ange::schedule::Call*>();
  const QString unmapped_uncode = problem->data("uncode").toString();
  if (problem->data("load_mapping").toBool()) {
    m_load_port_mapping.insert(unmapped_uncode, call);
  } else if (problem->data("discharge_mapping").toBool()) {
    m_discharge_port_mapping.insert(unmapped_uncode, call);
  } else {
    Q_ASSERT(false);
  }
  problem_solved(problem);
}

QStringList edifact_container_reader_t::warnings() const {
  QStringList rv;
  rv << edifact_parser_t::warnings();
  Q_FOREACH(ParserProblem* problem, m_problems) {
    rv << problem->details();
  }
  return rv;
}

QString edifact_container_reader_t::doctype_string() {
    switch(m_doctype) {
        case ange::edifact::edifact_parser_t::BAPLIE :
            return "BAPLIE";
        case ange::edifact::edifact_parser_t::COARRI :
            return "COARRI";
        case ange::edifact::edifact_parser_t::COPRAR :
            return "COPRAR";
        case ange::edifact::edifact_parser_t::MOVINS :
            return "MOVINS";
        case ange::edifact::edifact_parser_t::TANSTA :
            return "TANSTA";
        case ange::edifact::edifact_parser_t::UNKNOWN_DOC_TYPE :
            return "UNKNOWN";
    }
    return "UNKNOWN";
}

void edifact_container_reader_t::handle_doc_meta(ange::edifact::edifact_parser_t::meta_data_type_t type, QString data) {
    switch (type) {
        case VESSEL_ARRIVAL_TIME:
            m_vessel_arrival = QDateTime().fromString(data,Qt::ISODate);
            break;
        case MESSAGE_TIME:
            m_message_date = QDateTime().fromString(data,Qt::ISODate);
            break;
        case MESSAGE_REF:
            m_message_ref = data;
            break;
        case VESSEL_CODE:
            m_vessel_code = data;
            break;
        case VESSEL_NAME:
            m_vessel_name = data;
            break;
        case VOYAGE_CODE:
            m_voyage_code = data;
            break;
        case CARRIER_CODE:
            break;
        case LOAD_UNCODE:
            break;
        case DISCHARGE_UNCODE:
            break;
        case PORT_UNCODE:
            break;
    }
}

const ange::schedule::Call* edifact_container_reader_t::find_load_call_for_container(const ange::containers::Container* container){
  const Call* load_call = m_load_port_mapping.value(container->loadPort(), 0L);
  if (load_call) {
    return load_call;
  }
  const ange::schedule::Schedule* schedule = m_document->schedule();
    switch (m_doctype) {
        case edifact_parser_t::BAPLIE:
            load_call = schedule->getCallByUncodeBeforeCall(container->loadPort(), m_load_call);
            break;
        case edifact_parser_t::COARRI:
            load_call = m_load_call;
            break;
        case edifact_parser_t::COPRAR:
            load_call = m_load_call;
            break;
        case edifact_parser_t::MOVINS:
            Q_ASSERT_X(false, __func__, "MOVINS unsupported");
            break;
        case edifact_parser_t::TANSTA:
            Q_ASSERT_X(false, __func__, "TANSTA unsupported");
            break;
        case edifact_parser_t::UNKNOWN_DOC_TYPE:
            Q_ASSERT_X(false, __func__, "Unknown doc type");
        break;
    }

  if (!load_call) {
    load_call = schedule->calls().first();
  }

  return load_call;
}

#include "edifact_container_reader.moc"
