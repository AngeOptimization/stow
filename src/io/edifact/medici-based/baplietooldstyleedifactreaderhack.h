#ifndef BAPLIETOOLDSTYLEEDIFACTREADERHACK_H
#define BAPLIETOOLDSTYLEEDIFACTREADERHACK_H

#include "edifact_container_reader.h"

class BaplieParserResult;

/**
 * A bridge between the old bad way of parsing BAPLIE files to the new QtEdith based.
 */
class BaplieToOldStyleEdifactReaderHack : public edifact_container_reader_t {

    Q_OBJECT

public:

    BaplieToOldStyleEdifactReaderHack(document_t* document, pool_t* pool,
                                      const BaplieParserResult& result, const QString& fileName);

    ~BaplieToOldStyleEdifactReaderHack();

    /**
     * Function for replacing "new edifact_container_reader_t(); reader->parser()" in tests
     */
    static edifact_container_reader_t* testRead(const QString& fileName, document_t* document, pool_t* pool);

};

#endif // BAPLIETOOLDSTYLEEDIFACTREADERHACK_H
