#ifndef EDIFACT_CONTAINER_READER_H
#define EDIFACT_CONTAINER_READER_H

#include <ange/edifact/edifact_parser.h>
#include <QStringList>
#include <QDateTime>

class stowage_t;
namespace ange {
namespace schedule {
class Call;
class Schedule;
}
namespace vessel{
class BayRowTier;
class VesselTank;
}
}

class ParserProblem;
class document_t;
class pool_t;
class BundleContainerCommand;

/**
 * Handles import of container list from an abitrary EDIFACT files
 * The idea is (after construction) that parse is called in the selected
 * file. If desired, the containers can then be added to the document with
 * add_containers_to_document
 *
 * TODO #688 hack to be corrected in #1370 : filter Tank stuff out of this class into own edi TANSTA parser
 */
class edifact_container_reader_t : public QObject, protected ange::edifact::edifact_parser_t {

    Q_OBJECT

public:

    edifact_container_reader_t(document_t* document, pool_t* selected_pool, const ange::schedule::Call* currentCall);

    ~edifact_container_reader_t();

    /**
     * Attempt to parse
     * @param file EDIFACT file
     * @return true if parsed succesfully, perhaps with warnings. Otherwise, false is returned.
     */
    bool parse(QString filename);

    /**
     * Add containers to document
     */
    void add_containers_to_document();

    /**
     * Add tanks to document
     * TODO #688 hack to be corrected in #1370 : filter Tank stuff out of this class into own edi TANSTA parser
     */
    void addTanksToDocument();

    /**
     * @return any warnings from the parsing
     */
    QStringList warnings() const;

    /**
     * @return explation why parser failed by looking into all
     * problems with type=ERROR
     */
    QString error_message() const;

    /**
     * @return true if any errors remain
     */
    bool has_errors() const;

    /**
     * @returns all problems found during parsing
     */
    QList<ParserProblem*> problems() const ;

    /**
     * @return the call corresponding to the edifact file parsed
     */
    const ange::schedule::Call* loadcall() const { return m_load_call; }

    /**
     * @return the current active call in angelstow gui
     */
    const ange::schedule::Call* currentCall() const { return mCurrentCall; }

    struct container_error_t {
      container_error_t(const ange::containers::Container* container, ange::vessel::BayRowTier position, QString message)
        : container(container), position(position),message(message) {}
      const ange::containers::Container* container;
      ange::vessel::BayRowTier position;
      QString message;
    };

    struct container_pol_remapped_t {
      container_pol_remapped_t(const ange::containers::Container* container, ange::vessel::BayRowTier position, const ange::schedule::Call* from, const ange::schedule::Call* to)
       : container(container),
         position(position),
         from(from),
         to(to)
      {}
      const ange::containers::Container* container;
      ange::vessel::BayRowTier position;
      const ange::schedule::Call* from;
      const ange::schedule::Call* to;
    };

    struct TankMapping {
        //TankMapping() : tankCondition(), vesselTank(null) {};
        ange::edifact::edifact_parser_t::TankCondition ediTankCondition;
        ange::vessel::VesselTank* vesselTank;
        };

    /**
     * @return a list of errors and warnings for containers
     */
    QList<container_error_t> unplacedcontainers() const { return m_unplacedcontainerlocations; }

    /**
     * @return a list of containers that could not be placed correctly.
     */
    QHash<const ange::containers::Container*, QPair<ange::vessel::BayRowTier , const ange::schedule::Call* > > partiallyplacedcontainers() const { return m_partiallyplacedcontainerlocations; }

    /**
     * @return a list of containers that had their POL remapped to get them on board
     */
    QList<container_pol_remapped_t> pol_remapped_containers() const {
      return m_containers_pol_remapped;
    }

    /**
     * @return the document type enum
     */
    ange::edifact::edifact_parser_t::doc_type_t doctype() const {return m_doctype;}
    /**
     * @return the document type as string
     */
    QString doctype_string();

    /**
     * @return the list of containers imported
     */
    QList<ange::containers::Container*> containers() const { return m_containers;}

    /**
     * @return the list of tank conditions imported
     * TODO #688 hack to be corrected in #1370 : filter Tank stuff out of this class into own edi TANSTA parser
     */
    QList<const TankMapping*> tankMappings() const { return mTankMappings;}


    QDateTime message_date() const { return m_message_date; }
    QDateTime vessel_arrival() const { return m_vessel_arrival; }
    QString message_ref() const { return m_message_ref;}
    QString vessel_code() const { return m_vessel_code;}
    QString voyage_code() const { return m_voyage_code;}
    QString vessel_name() const { return m_vessel_name;}
    QString port() const { return m_forcePort.isNull() ? ange::edifact::edifact_parser_t::port() : m_forcePort;}

protected:
    /**
     * Copies the given Container
     */
    virtual void handle_container_location(const ange::containers::Container& container, const ange::vessel::BayRowTier& position);
    virtual void handle_error(int error);
    virtual void handle_warning(int error);
    virtual void handle_doc_type(const ange::edifact::edifact_parser_t::doc_type_t type);
    virtual void handle_doc_meta(meta_data_type_t type, QString data);
    // TODO #688 hack to be corrected in #1370 : filter Tank stuff out of this class into own edi TANSTA parser
    virtual void handleTankCondition(const TankCondition& tankCondition);

    /**
     * Update problems with last/first possible port when a (edifact-document level) load port has been found
     */
    void update_problems_with_load_call();

public Q_SLOTS:
    void problem_solved(ParserProblem* problem);

    /**
     * load port resolved. Calls problem solved, and sets m_load_port
     */
    void problem_with_load_port_solved(ParserProblem* problem);

    /**
     * unmapped port resolved. Calls problem solved, and sets port mapping
     */
    void problem_with_unmapped_port_solved(ParserProblem* problem);
private:
    /**
     * Utility function to check the legality of a container in existing stowage
     */
    const ange::schedule::Call* find_load_call_for_container(const ange::containers::Container* container);

    /**
     * Check that uncode is known or mapping exists. If not, create a problem to be fixed.
     */
    void verify_mapping(QString uncode, bool is_load);

    /**
     * Bundle flatracks if they are of collapsible type and empty
     * @return the list of bundle commands to run for use in undo stack
     */
     QList<BundleContainerCommand*> bundleCollapsibleContainers();

     /**
      * assign (unique?) random equipment ID to all containers not having an ID
      */
     void ensureEquipmentId();

    document_t* m_document;
    pool_t* m_selected_pool;
protected:
    bool m_parsed;
private:
    QList<ange::containers::Container*> m_containers;
    QHash<const ange::containers::Container*, ange::vessel::BayRowTier> m_containerlocations;
    QList<container_error_t> m_unplacedcontainerlocations;
    QList<container_pol_remapped_t> m_containers_pol_remapped;
    QHash<const ange::containers::Container*, QPair<ange::vessel::BayRowTier , const ange::schedule::Call* > > m_partiallyplacedcontainerlocations;
protected:
    QList<ParserProblem*> m_problems;
    QString m_filename;
    const ange::schedule::Call* m_load_call;
    QString m_forcePort;
private:
    ange::edifact::edifact_parser_t::doc_type_t m_doctype;
    QDateTime m_message_date;
    QString m_message_ref;
    QDateTime m_vessel_arrival;
    QString m_vessel_code;
    QString m_voyage_code;
    QString m_vessel_name;
    QHash<QString, const ange::schedule::Call*> m_load_port_mapping;
    QHash<QString, const ange::schedule::Call*> m_discharge_port_mapping;

    // TODO #688 hack to be corrected in #1370 : filter Tank stuff out of this class into own edi TANSTA parser
    QList<const TankMapping*> mTankMappings;
    const ange::schedule::Call* mCurrentCall;

};

#endif // EDIFACT_CONTAINER_READER_H
