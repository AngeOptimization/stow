#ifndef COPRAR20TOOLDSTYLEEDIFACTREADERHACK_H
#define COPRAR20TOOLDSTYLEEDIFACTREADERHACK_H

#include "edifact_container_reader.h"

class CoprarParserResult;
class Coprar20toOldStyleEdifactReaderHack : public edifact_container_reader_t {
    Q_OBJECT
    public:
        ~Coprar20toOldStyleEdifactReaderHack();
        Coprar20toOldStyleEdifactReaderHack(document_t* document, pool_t* selected_pool, const ange::schedule::Call* currentCall);
        /**
        * shoves a CoprarParserResult in thru the old edifact parser class
        * \param result result of CoprarParser
        * \param filename a optional filename argument for translating error messages
        */
        bool parse(const CoprarParserResult& result, QString filename = QString());

    protected:
        virtual void handleTankCondition(const ange::edifact::edifact_parser_t::TankCondition& tankCondition);
};

#endif // COPRAR20TOOLDSTYLEEDIFACTREADERHACK_H
