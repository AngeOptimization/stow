#include "coprar20tooldstyleedifactreaderhack.h"

#include "coprar20parser.h"
#include "parserproblem.h"

Coprar20toOldStyleEdifactReaderHack::~Coprar20toOldStyleEdifactReaderHack()
{
}

Coprar20toOldStyleEdifactReaderHack::Coprar20toOldStyleEdifactReaderHack(document_t* document, pool_t* selected_pool, const ange::schedule::Call* currentCall) : edifact_container_reader_t(document, selected_pool, currentCall) {
    m_load_call = currentCall;
}

void Coprar20toOldStyleEdifactReaderHack::handleTankCondition(const ange::edifact::edifact_parser_t::TankCondition& tankCondition) {
    Q_UNUSED(tankCondition);
    Q_ASSERT(false);
}

bool Coprar20toOldStyleEdifactReaderHack::parse(const CoprarParserResult& result, QString filename) {
    m_filename = filename;
    Q_FOREACH(const EdiProblem& error, result.errorList()) {
        switch (error.type()) {
            case EdiProblem::Error: {
            m_problems << ParserProblem::parserError("Error COPRAR20 '" + filename + "'", error.message());
            break;
            }
            case EdiProblem::Warning: {
                m_problems << ParserProblem::parserWarning("Warning COPRAR20 '" + filename + "'", error.message());
                break;
            }
            case EdiProblem::Info: {
                m_problems << ParserProblem::parserWarning("Info COPRAR20 '" + filename + "'", error.message());
                break;
            }
        }
    }

    if (result.hasErrors()) {
        return false;
    }

    if(result.containerList().isEmpty()) {
        m_problems << ParserProblem::parserError("No containers found in COPRAR", filename);
        return false;
    }

    handle_doc_type(ange::edifact::edifact_parser_t::COPRAR); // lol

    handle_doc_meta(ange::edifact::edifact_parser_t::CARRIER_CODE,QString::fromLocal8Bit(result.coprarTransport().carrierId()));
    handle_doc_meta(ange::edifact::edifact_parser_t::MESSAGE_TIME, result.ediHeader().timeStamp().toString(Qt::ISODate));
    handle_doc_meta(ange::edifact::edifact_parser_t::VESSEL_NAME, QString::fromLocal8Bit(result.coprarTransport().vesselName()));
    handle_doc_meta(ange::edifact::edifact_parser_t::VOYAGE_CODE, QString::fromLocal8Bit(result.coprarTransport().voyageCode()));
    // not found in Coprar 2.0 // handle_doc_meta(ange::edifact::edifact_parser_t::DISCHARGE_UNCODE, QString::fromLocal8Bit(result.coprarTransport().nextPortOfCall()));
    handle_doc_meta(ange::edifact::edifact_parser_t::LOAD_UNCODE, QString::fromLocal8Bit(result.coprarTransport().portCode()));

    Q_FOREACH(ange::containers::Container* container, result.containerList()) {
        handle_container_location(*container, ange::vessel::BayRowTier());
    }

    m_parsed = true;
    return true;
}


#include "coprar20tooldstyleedifactreaderhack.moc"
