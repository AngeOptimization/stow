#include <QTest>

#include "baplietooldstyleedifactreaderhack.h"
#include "container_list.h"
#include "document.h"
#include "edifact_container_reader.h"
#include "fileopener.h"
#include "parserproblem.h"
#include "pool.h"

#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>

using namespace ange::containers;
using namespace ange::schedule;

class BaplieImportTest : public QObject {
    Q_OBJECT
private Q_SLOTS:
    void initTestCase();
    void testNumberOfImportedContainers();
    void testImportWrongSegmentOrder();
    void testLoadDischRestowStayFake1();
    void testLoadDischRestowStayFake2();
    void testLoadDischRestowStayFake3();
    void testWrongUncodePOL();
    void testImportIntoEmptyDocument();
};
QTEST_GUILESS_MAIN(BaplieImportTest);

void BaplieImportTest::initTestCase(){
}

void BaplieImportTest::testNumberOfImportedContainers() {
    document_t* document = FileOpener::openStoFile(QFINDTESTDATA("data/1330W_HANSA_ALG_DEP.sto")).document;
    pool_t* pool = new pool_t(document->containers());
    edifact_container_reader_t* reader
        = BaplieToOldStyleEdifactReaderHack::testRead(QFINDTESTDATA("data/HANSA_1330W_ALG_BAPLIE_corrected.EDI"), document, pool);

    reader->add_containers_to_document();
    QCOMPARE(document->containers()->size(), 343);

    delete reader;
    delete pool;
    delete document;
}

void BaplieImportTest::testImportWrongSegmentOrder() {
    document_t* document = FileOpener::openStoFile(QFINDTESTDATA("data/1330W_HANSA_ALG_DEP.sto")).document;
    pool_t* pool = new pool_t(document->containers());
    edifact_container_reader_t* reader
        = BaplieToOldStyleEdifactReaderHack::testRead(QFINDTESTDATA("data/HANSA_1330W_ALG_BAPLIE_wrong_order.EDI"), document, pool);

    reader->add_containers_to_document();
    // sloppy parsing of wrong segment order EQD - DGS - NAD, ticket #1043  results in 3 containers, not 2 as in strict parsing
    QCOMPARE(document->containers()->size(), 3);

    delete reader;
    delete pool;
    delete document;
}

void BaplieImportTest::testLoadDischRestowStayFake1() {
    document_t* document = FileOpener::openStoFile(QFINDTESTDATA("data/micro_fake_position.sto")).document;

    QCOMPARE(document->containers()->size(), 5);
    // STAY0000001 DISC0000002 LOAD0000003 REST0000004 FAKE0000005
    QVERIFY(document->containers()->get_container_by_equipment_number("STAY0000001"));
    QVERIFY(document->containers()->get_container_by_equipment_number("DISC0000002"));
    QVERIFY(document->containers()->get_container_by_equipment_number("LOAD0000003"));
    QVERIFY(document->containers()->get_container_by_equipment_number("REST0000004"));
    QVERIFY(document->containers()->get_container_by_equipment_number("FAKE0000005"));

    QCOMPARE(document->containers()->get_container_by_equipment_number("STAY0000001")->loadPort(), QString("Befor"));
    QCOMPARE(document->containers()->get_container_by_equipment_number("STAY0000001")->dischargePort(), QString("USLAX"));
    QVERIFY(document->containers()->get_container_by_equipment_number("STAY0000001")->placeOfDelivery().isEmpty());

    QCOMPARE(document->containers()->get_container_by_equipment_number("DISC0000002")->loadPort(), QString("Befor"));
    QCOMPARE(document->containers()->get_container_by_equipment_number("DISC0000002")->dischargePort(), QString("CNTAG"));
    QVERIFY(document->containers()->get_container_by_equipment_number("DISC0000002")->placeOfDelivery().isEmpty());

    QCOMPARE(document->containers()->get_container_by_equipment_number("LOAD0000003")->loadPort(), QString("CNTAG"));
    QCOMPARE(document->containers()->get_container_by_equipment_number("LOAD0000003")->dischargePort(), QString("USLAX"));
    QVERIFY(document->containers()->get_container_by_equipment_number("LOAD0000003")->placeOfDelivery().isEmpty());

    QCOMPARE(document->containers()->get_container_by_equipment_number("REST0000004")->loadPort(), QString("Befor"));
    QCOMPARE(document->containers()->get_container_by_equipment_number("REST0000004")->dischargePort(), QString("USLAX"));
    QVERIFY(document->containers()->get_container_by_equipment_number("REST0000004")->placeOfDelivery().isEmpty());

    QCOMPARE(document->containers()->get_container_by_equipment_number("FAKE0000005")->loadPort(), QString("CNTAG"));
    QCOMPARE(document->containers()->get_container_by_equipment_number("FAKE0000005")->dischargePort(), QString("USLAX"));
    QVERIFY(document->containers()->get_container_by_equipment_number("FAKE0000005")->placeOfDelivery().isEmpty());

    pool_t* pool = new pool_t(document->containers());
    edifact_container_reader_t* reader
        = BaplieToOldStyleEdifactReaderHack::testRead(QFINDTESTDATA("data/micro_baplie_CNTAG.xls.edi"), document, pool);
    reader->add_containers_to_document();

    QCOMPARE(document->containers()->size(), 5); // containers are not removed from document - only altered

    QCOMPARE(document->containers()->get_container_by_equipment_number("STAY0000001")->loadPort(), QString("Befor"));
    QCOMPARE(document->containers()->get_container_by_equipment_number("STAY0000001")->dischargePort(), QString("USLAX"));
    QCOMPARE(document->containers()->get_container_by_equipment_number("STAY0000001")->placeOfDelivery(), QString("USWQJ")); // changed place of delivery in BAPLIE

    QCOMPARE(document->containers()->get_container_by_equipment_number("DISC0000002")->loadPort(), QString("Befor"));
    QCOMPARE(document->containers()->get_container_by_equipment_number("DISC0000002")->dischargePort(), QString("CNTAG"));
    QVERIFY(document->containers()->get_container_by_equipment_number("DISC0000002")->placeOfDelivery().isEmpty());

    QCOMPARE(document->containers()->get_container_by_equipment_number("LOAD0000003")->loadPort(), QString("CNTAG"));
    QCOMPARE(document->containers()->get_container_by_equipment_number("LOAD0000003")->dischargePort(), QString("USLAX"));
    QCOMPARE(document->containers()->get_container_by_equipment_number("LOAD0000003")->placeOfDelivery(), QString("USETA")); // changed place of delivery in BAPLIE

    QCOMPARE(document->containers()->get_container_by_equipment_number("REST0000004")->loadPort(), QString("CNTAG"));  // Restow alters POL
    QCOMPARE(document->containers()->get_container_by_equipment_number("REST0000004")->dischargePort(), QString("USLAX"));
    QCOMPARE(document->containers()->get_container_by_equipment_number("REST0000004")->placeOfDelivery(), QString("USLAX")); // changed place of delivery in BAPLIE

    QCOMPARE(document->containers()->get_container_by_equipment_number("FAKE0000005")->loadPort(), QString("CNTAG"));
    QCOMPARE(document->containers()->get_container_by_equipment_number("FAKE0000005")->dischargePort(), QString("USLAX")); // Container left at port
    QVERIFY(document->containers()->get_container_by_equipment_number("FAKE0000005")->placeOfDelivery().isEmpty());

    delete reader;
    delete pool;
    delete document;
}

void BaplieImportTest::testLoadDischRestowStayFake2() {
    document_t* document = FileOpener::openStoFile(QFINDTESTDATA("data/micro_fake_position.sto")).document;

    QCOMPARE(document->containers()->size(), 5);
    // STAY0000001 DISC0000002 LOAD0000003 REST0000004 FAKE0000005
    QVERIFY(document->containers()->get_container_by_equipment_number("STAY0000001"));
    QVERIFY(document->containers()->get_container_by_equipment_number("DISC0000002"));
    QVERIFY(document->containers()->get_container_by_equipment_number("LOAD0000003"));
    QVERIFY(document->containers()->get_container_by_equipment_number("REST0000004"));
    QVERIFY(document->containers()->get_container_by_equipment_number("FAKE0000005"));

    // Further sto document validity tests before BAPLIE import omitted, as these are done in testLoadDischRestowStayFake1()

    pool_t* pool = new pool_t(document->containers());
    edifact_container_reader_t* reader
        = BaplieToOldStyleEdifactReaderHack::testRead(QFINDTESTDATA("data/micro_baplie_CNTAG_no_stay.xls.edi"), document, pool);

    reader->add_containers_to_document();

    QCOMPARE(document->containers()->size(), 5); // containers are not removed from document - only altered

    // Only difference to testLoadDischRestowStayFake1() is STAY0000001 which now is not mentioned in release BAPLIE, i.e. has been discharged in CNTAG
    QCOMPARE(document->containers()->get_container_by_equipment_number("STAY0000001")->loadPort(), QString("Befor"));
    // QCOMPARE(document->containers()->get_container_by_equipment_number("STAY0000001")->discharge_port(), QString("CNTAG")); // should be CNTAG - investigate!

    delete reader;
    delete pool;
    delete document;
}


void BaplieImportTest::testLoadDischRestowStayFake3() {
    document_t* document = FileOpener::openStoFile(QFINDTESTDATA("data/micro_fake_position.sto")).document;

    QCOMPARE(document->containers()->size(), 5);
    // STAY0000001 DISC0000002 LOAD0000003 REST0000004 FAKE0000005
    QVERIFY(document->containers()->get_container_by_equipment_number("STAY0000001"));
    QVERIFY(document->containers()->get_container_by_equipment_number("DISC0000002"));
    QVERIFY(document->containers()->get_container_by_equipment_number("LOAD0000003"));
    QVERIFY(document->containers()->get_container_by_equipment_number("REST0000004"));
    QVERIFY(document->containers()->get_container_by_equipment_number("FAKE0000005"));

    // Further sto document validity tests before BAPLIE import omitted, as these are done in testLoadDischRestowStayFake1()

    pool_t* pool = new pool_t(document->containers());
    edifact_container_reader_t* reader = new edifact_container_reader_t(document, pool, 0L);
    reader->parse(QFINDTESTDATA("data/micro_coarri_CNTAG.xls.edi"));
    reader->add_containers_to_document();

    QCOMPARE(document->containers()->size(), 5); // containers are not removed from document - only altered
    // same test results expected than in testLoadDischRestowStayFake1(), now with COARRI import

    QCOMPARE(document->containers()->get_container_by_equipment_number("STAY0000001")->loadPort(), QString("Befor"));
    QCOMPARE(document->containers()->get_container_by_equipment_number("STAY0000001")->dischargePort(), QString("USLAX"));

    QCOMPARE(document->containers()->get_container_by_equipment_number("DISC0000002")->loadPort(), QString("Befor"));
    QCOMPARE(document->containers()->get_container_by_equipment_number("DISC0000002")->dischargePort(), QString("CNTAG"));

    QCOMPARE(document->containers()->get_container_by_equipment_number("LOAD0000003")->loadPort(), QString("CNTAG"));
    QCOMPARE(document->containers()->get_container_by_equipment_number("LOAD0000003")->dischargePort(), QString("USLAX"));

    QCOMPARE(document->containers()->get_container_by_equipment_number("REST0000004")->loadPort(), QString("CNTAG"));  // Restow alters POL
    QCOMPARE(document->containers()->get_container_by_equipment_number("REST0000004")->dischargePort(), QString("USLAX"));

    QCOMPARE(document->containers()->get_container_by_equipment_number("FAKE0000005")->loadPort(), QString("CNTAG"));
    QCOMPARE(document->containers()->get_container_by_equipment_number("FAKE0000005")->dischargePort(), QString("USLAX")); // Container left at port

    delete reader;
    delete pool;
    delete document;
}

void BaplieImportTest::testWrongUncodePOL() {
    document_t* document = FileOpener::openStoFile(QFINDTESTDATA("data/1330W_HANSA_ALG_DEP.sto")).document;
    pool_t* pool = new pool_t(document->containers());
    edifact_container_reader_t* reader
        = BaplieToOldStyleEdifactReaderHack::testRead(QFINDTESTDATA("data/Hansa_Limburg-ESALG-wrong-POL.BAPLIE.edi"),
                                                      document, pool);

    ParserProblem* problem = reader->problems().at(0);
    QVERIFY(problem->data("first_possible_call").value<const Call*>());
    QVERIFY(problem->data("last_possible_call").value<const Call*>());

    reader->add_containers_to_document();
    QCOMPARE(document->containers()->size(), 1);

    delete reader;
    delete pool;
    delete document;
}

void BaplieImportTest::testImportIntoEmptyDocument() {
    Q_INIT_RESOURCE(data);
    QScopedPointer<document_t> document(FileOpener::openBlankStowage().document);
    QScopedPointer<pool_t> pool(new pool_t(document->containers()));
    QScopedPointer<edifact_container_reader_t> reader(
        BaplieToOldStyleEdifactReaderHack::testRead(QFINDTESTDATA("data/Hansa_Limburg-ESALG-wrong-POL.BAPLIE.edi"),
                                                    document.data(), pool.data()));
    QCOMPARE(reader->problems().count(), 3);
    reader->problems().at(0)->solve(QVariant::fromValue<const Call*>(document->schedule()->at(0)));
    QCOMPARE(reader->problems().count(), 2);
    reader->add_containers_to_document();
    QCOMPARE(document->containers()->size(), 1);
}

#include "baplieimporttest.moc"
