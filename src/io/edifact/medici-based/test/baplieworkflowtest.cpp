#include <QtTest>

#include <edifact_container_reader.h>
#include <baplietooldstyleedifactreaderhack.h>
#include <fileopener.h>
#include <document/document.h>
#include <document/pools/pool.h>
#include <document/stowage/stowage.h>
#include <document/stowage/container_move.h>
#include <document/containers/container_list.h>

#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>
#include <ange/vessel/slot.h>

#include <QObject>

class BaplieWorkflowTest : public QObject {
    Q_OBJECT
    private Q_SLOTS:
    void testLoadAndLocations1110();
    void testLoadAndLocations1110_2();
};


using ange::containers::Container;
using ange::schedule::Call;
using ange::schedule::Schedule;
using ange::vessel::BayRowTier;
using ange::vessel::Slot;

namespace QTest {
    template<>
    inline char* toString(const ange::vessel::BayRowTier& brt) {
        return QTest::toString<QString>(brt.toString());
    }
}


void BaplieWorkflowTest::testLoadAndLocations1110() {
    document_t* document = FileOpener::openStoFile(QFINDTESTDATA("data/micro_baplie_workflow_1110.sto")).document;

    //sanity check document
    const Call* A = document->schedule()->getCallByUncodeAfterCall("AAAAA",document->schedule()->calls().first());
    QVERIFY(A);
    const Call* B = document->schedule()->getCallByUncodeAfterCall("BBBBB",A);
    QVERIFY(B);
    const Call* C = document->schedule()->getCallByUncodeAfterCall("CCCCC",B);
    QVERIFY(C);
    const Call* D = document->schedule()->getCallByUncodeAfterCall("DDDDD",C);
    QVERIFY(D);
    QCOMPARE(document->containers()->size(),5);
    {
        const Container* stay10 = document->containers()->get_container_by_equipment_number("STAY0000010");
        QVERIFY(stay10);
        BayRowTier stay10location(2,0,2);
        BayRowTier brtA = document->stowage()->nominal_position(stay10, A);
        QCOMPARE(brtA,stay10location);
        BayRowTier brtB = document->stowage()->nominal_position(stay10, B);
        QCOMPARE(brtB,stay10location);
        BayRowTier brtC = document->stowage()->nominal_position(stay10, C);
        QCOMPARE(brtC,stay10location);
        BayRowTier brtD = document->stowage()->nominal_position(stay10, D);
        QCOMPARE(brtD,BayRowTier());
    }
    {
        const Container* cona11 = document->containers()->get_container_by_equipment_number("CONA0000011");
        QVERIFY(cona11);
        BayRowTier cona11location(2,0,80);
        BayRowTier brtA = document->stowage()->nominal_position(cona11, A);
        QCOMPARE(brtA,cona11location);
        BayRowTier brtB = document->stowage()->nominal_position(cona11, B);
        QCOMPARE(brtB,cona11location);
        BayRowTier brtC = document->stowage()->nominal_position(cona11, C);
        QCOMPARE(brtC,BayRowTier());
        BayRowTier brtD = document->stowage()->nominal_position(cona11, D);
        QCOMPARE(brtD,BayRowTier());
    }
    {
        const Container* conc12 = document->containers()->get_container_by_equipment_number("CONC0000012");
        QVERIFY(conc12);
        BayRowTier conc12location(2,0,4);
        BayRowTier brtA = document->stowage()->nominal_position(conc12, A);
        QCOMPARE(brtA,BayRowTier());
        BayRowTier brtB = document->stowage()->nominal_position(conc12, B);
        QCOMPARE(brtB,BayRowTier());
        BayRowTier brtC = document->stowage()->nominal_position(conc12, C);
        QCOMPARE(brtC,conc12location);
        BayRowTier brtD = document->stowage()->nominal_position(conc12, D);
        QCOMPARE(brtD,BayRowTier());
    }
    {
        const Container* disc13 = document->containers()->get_container_by_equipment_number("DISC0000013");
        QVERIFY(disc13);
        BayRowTier disc13location(2,0,82);
        BayRowTier brtA = document->stowage()->nominal_position(disc13, A);
        QCOMPARE(brtA,disc13location);
        BayRowTier brtB = document->stowage()->nominal_position(disc13, B);
        QCOMPARE(brtB,BayRowTier());
        BayRowTier brtC = document->stowage()->nominal_position(disc13, C);
        QCOMPARE(brtC,BayRowTier());
        BayRowTier brtD = document->stowage()->nominal_position(disc13, D);
        QCOMPARE(brtD,BayRowTier());
    }
    {
        const Container* load14 = document->containers()->get_container_by_equipment_number("LOAD0000014");
        QVERIFY(load14);
        BayRowTier load14location(2,0,82);
        BayRowTier brtA = document->stowage()->nominal_position(load14, A);
        QCOMPARE(brtA,BayRowTier());
        BayRowTier brtB = document->stowage()->nominal_position(load14, B);
        QCOMPARE(brtB,load14location);
        BayRowTier brtC = document->stowage()->nominal_position(load14, C);
        QCOMPARE(brtC,load14location);
        BayRowTier brtD = document->stowage()->nominal_position(load14, D);
        QCOMPARE(brtD,BayRowTier());
    }
    {
        const Container* newa15 = document->containers()->get_container_by_equipment_number("NEWA0000015");
        QVERIFY(!newa15);
    }
    {
        const Container* newc16 = document->containers()->get_container_by_equipment_number("NEWC0000016");
        QVERIFY(!newc16);
    }

    pool_t* pool = new pool_t(document->containers());

    edifact_container_reader_t* reader
        = BaplieToOldStyleEdifactReaderHack::testRead(QFINDTESTDATA("data/micro_baplie_workflow_BBBBB_B_1110.xls.edi"), document, pool);

    reader->add_containers_to_document();

    QCOMPARE(reader->partiallyplacedcontainers().size(),0);
    QCOMPARE(reader->unplacedcontainers().size(),2);
    {
        const Container* stay10 = document->containers()->get_container_by_equipment_number("STAY0000010");
        QVERIFY(stay10);
        BayRowTier stay10location(2,0,2);
        BayRowTier brtA = document->stowage()->nominal_position(stay10, A);
        QCOMPARE(brtA,stay10location);
        BayRowTier brtB = document->stowage()->nominal_position(stay10, B);
        QCOMPARE(brtB,stay10location);
        BayRowTier brtC = document->stowage()->nominal_position(stay10, C);
        QCOMPARE(brtC,stay10location);
        BayRowTier brtD = document->stowage()->nominal_position(stay10, D);
        QCOMPARE(brtD,BayRowTier());
    }
    {
        const Container* cona11 = document->containers()->get_container_by_equipment_number("CONA0000011");
        QVERIFY(cona11);
        BayRowTier cona11location(2,0,80);
        BayRowTier brtA = document->stowage()->nominal_position(cona11, A);
        QCOMPARE(brtA,cona11location);
        BayRowTier brtB = document->stowage()->nominal_position(cona11, B);
        QCOMPARE(brtB,BayRowTier());
        BayRowTier brtC = document->stowage()->nominal_position(cona11, C);
        QCOMPARE(brtC,BayRowTier());
        BayRowTier brtD = document->stowage()->nominal_position(cona11, D);
        QCOMPARE(brtD,BayRowTier());
    }
    {
        const Container* conc12 = document->containers()->get_container_by_equipment_number("CONC0000012");
        QVERIFY(conc12);
        BayRowTier conc12location(2,0,4);
        BayRowTier brtA = document->stowage()->nominal_position(conc12, A);
        QCOMPARE(brtA,BayRowTier());
        BayRowTier brtB = document->stowage()->nominal_position(conc12, B);
        QCOMPARE(brtB,BayRowTier());
        BayRowTier brtC = document->stowage()->nominal_position(conc12, C);
        QCOMPARE(brtC,BayRowTier());
        BayRowTier brtD = document->stowage()->nominal_position(conc12, D);
        QCOMPARE(brtD,BayRowTier());
    }
    {
        const Container* disc13 = document->containers()->get_container_by_equipment_number("DISC0000013");
        QVERIFY(disc13);
        BayRowTier disc13location(2,0,82);
        BayRowTier brtA = document->stowage()->nominal_position(disc13, A);
        QCOMPARE(brtA,disc13location);
        BayRowTier brtB = document->stowage()->nominal_position(disc13, B);
        QCOMPARE(brtB,BayRowTier());
        BayRowTier brtC = document->stowage()->nominal_position(disc13, C);
        QCOMPARE(brtC,BayRowTier());
        BayRowTier brtD = document->stowage()->nominal_position(disc13, D);
        QCOMPARE(brtD,BayRowTier());
    }
    {
        const Container* load14 = document->containers()->get_container_by_equipment_number("LOAD0000014");
        QVERIFY(load14);
        BayRowTier load14location(2,0,82);
        BayRowTier brtA = document->stowage()->nominal_position(load14, A);
        QCOMPARE(brtA,BayRowTier());
        BayRowTier brtB = document->stowage()->nominal_position(load14, B);
        QCOMPARE(brtB,load14location);
        BayRowTier brtC = document->stowage()->nominal_position(load14, C);
        QCOMPARE(brtC,load14location);
        BayRowTier brtD = document->stowage()->nominal_position(load14, D);
        QCOMPARE(brtD,BayRowTier());
    }
    {
        const Container* newa15 = document->containers()->get_container_by_equipment_number("NEWA0000015");
        QVERIFY(newa15);
        BayRowTier newa15location(2,0,80);
        BayRowTier brtA = document->stowage()->nominal_position(newa15, A);
        QCOMPARE(brtA,BayRowTier());
        BayRowTier brtB = document->stowage()->nominal_position(newa15, B);
        QCOMPARE(brtB,newa15location);
        BayRowTier brtC = document->stowage()->nominal_position(newa15, C);
        QCOMPARE(brtC,newa15location);
        BayRowTier brtD = document->stowage()->nominal_position(newa15, D);
        QCOMPARE(brtD,BayRowTier());
    }
    {
        const Container* newc16 = document->containers()->get_container_by_equipment_number("NEWC0000016");
        QVERIFY(newc16);
        BayRowTier newc16location(2,0,4);
        BayRowTier brtA = document->stowage()->nominal_position(newc16, A);
        QCOMPARE(brtA,BayRowTier());
        BayRowTier brtB = document->stowage()->nominal_position(newc16, B);
        QCOMPARE(brtB,newc16location);
        BayRowTier brtC = document->stowage()->nominal_position(newc16, C);
        QCOMPARE(brtC,newc16location);
        BayRowTier brtD = document->stowage()->nominal_position(newc16, D);
        QCOMPARE(brtD,BayRowTier());
    }

    delete reader;
    delete pool;
    delete document;

}


void BaplieWorkflowTest::testLoadAndLocations1110_2() {
    document_t* document = FileOpener::openStoFile(QFINDTESTDATA("data/micro_baplie_workflow_1110_2.sto")).document;

    //sanity check document
    const Call* A = document->schedule()->getCallByUncodeAfterCall("AAAAA",document->schedule()->calls().first());
    QVERIFY(A);
    const Call* B = document->schedule()->getCallByUncodeAfterCall("BBBBB",A);
    QVERIFY(B);
    const Call* C = document->schedule()->getCallByUncodeAfterCall("CCCCC",B);
    QVERIFY(C);
    const Call* D = document->schedule()->getCallByUncodeAfterCall("DDDDD",C);
    QVERIFY(D);
    QCOMPARE(document->containers()->size(),1);
    {
        const Container* conc12 = document->containers()->get_container_by_equipment_number("CONC0000012");
        QVERIFY(conc12);
        BayRowTier conc12location(2,0,2);
        BayRowTier brtA = document->stowage()->nominal_position(conc12, A);
        QCOMPARE(brtA,BayRowTier());
        BayRowTier brtB = document->stowage()->nominal_position(conc12, B);
        QCOMPARE(brtB,conc12location);
        BayRowTier brtC = document->stowage()->nominal_position(conc12, C);
        QCOMPARE(brtC,conc12location);
        BayRowTier brtD = document->stowage()->nominal_position(conc12, D);
        QCOMPARE(brtD,BayRowTier());
    }
    {
        const Container* newc16 = document->containers()->get_container_by_equipment_number("NEWC0000016");
        QVERIFY(!newc16);
    }

    pool_t* pool = new pool_t(document->containers());
    edifact_container_reader_t* reader
        = BaplieToOldStyleEdifactReaderHack::testRead(QFINDTESTDATA("data/micro_baplie_workflow_BBBBB_B_1110_2.xls.edi"), document, pool);

    reader->add_containers_to_document();

    QCOMPARE(reader->partiallyplacedcontainers().size(),0);
    QCOMPARE(reader->unplacedcontainers().size(),1);
    {
        const Container* conc12 = document->containers()->get_container_by_equipment_number("CONC0000012");
        QVERIFY(conc12);
        BayRowTier conc12location(2,0,2);
        BayRowTier brtA = document->stowage()->nominal_position(conc12, A);
        QCOMPARE(brtA,BayRowTier());
        BayRowTier brtB = document->stowage()->nominal_position(conc12, B);
        QCOMPARE(brtB,BayRowTier());
        BayRowTier brtC = document->stowage()->nominal_position(conc12, C);
        QCOMPARE(brtC,BayRowTier());
        BayRowTier brtD = document->stowage()->nominal_position(conc12, D);
        QCOMPARE(brtD,BayRowTier());
    }
    {
        const Container* newc16 = document->containers()->get_container_by_equipment_number("NEWC0000016");
        QVERIFY(newc16);
        BayRowTier newc16location(2,0,2);
        BayRowTier brtA = document->stowage()->nominal_position(newc16, A);
        QCOMPARE(brtA,BayRowTier());
        BayRowTier brtB = document->stowage()->nominal_position(newc16, B);
        QCOMPARE(brtB,newc16location);
        BayRowTier brtC = document->stowage()->nominal_position(newc16, C);
        QCOMPARE(brtC,newc16location);
        BayRowTier brtD = document->stowage()->nominal_position(newc16, D);
        QCOMPARE(brtD,BayRowTier());
    }

    delete reader;
    delete pool;
    delete document;
}


QTEST_GUILESS_MAIN(BaplieWorkflowTest);
#include "baplieworkflowtest.moc"
