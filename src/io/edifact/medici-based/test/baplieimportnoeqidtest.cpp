#include <QTest>

#include "document/document.h"
#include "document/containers/container_list.h"
#include "edifact_container_reader.h"
#include <baplietooldstyleedifactreaderhack.h>
#include <fileopener.h>
#include "document/pools/pool.h"
#include <stowage.h>

#include <ange/containers/container.h>
#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>

using ange::containers::Container;
using ange::containers::EquipmentNumber;

/**
 * A test to ensure that containers without equipment ID get loaded (ticket #1689)
 */
class BaplieImportNoEqIdTest : public QObject {
    Q_OBJECT
private Q_SLOTS:
    void testNoUnplanned();
private:
};

void BaplieImportNoEqIdTest::testNoUnplanned() {
    document_t* document = FileOpener::openStoFile(QFINDTESTDATA("data/micro2ports.sto")).document;
    pool_t* pool = new pool_t(document->containers());
    edifact_container_reader_t* reader
        = BaplieToOldStyleEdifactReaderHack::testRead(QFINDTESTDATA("data/baplie-no-eq-id.edi"), document, pool);
    reader->add_containers_to_document();
    const ange::schedule::Call* loadCall = document->schedule()->calls().at(1);
    QCOMPARE(loadCall->uncode(), QString("MYPKG"));
    for(int i = 0; i < document->containers()->list().count(); ++i) {
        const Container* container = document->containers()->list().at(i);
        QVERIFY2(document->stowage()->container_position(container, loadCall),
                 QString("Container number %1 (%2) did not get a position.")
                 .arg(i).arg(container->equipmentNumber()).toLocal8Bit());
    }
    delete reader;
    delete pool;
    delete document;
}

QTEST_GUILESS_MAIN(BaplieImportNoEqIdTest);
#include "baplieimportnoeqidtest.moc"
