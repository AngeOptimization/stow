/**
 *
 * COPRAR 2.0 parser implementation based on own QtEdith EDIFACT segment grouper
 *
 * SMDG
 * User Manual( Implementation Guide )
 * UN/EDIFACT MESSAGE
 * COPRAR (Container discharge/loading order)
 * Version 2.0
 * D00B
 *
 * Copyright Ange Optimization Aps, 2014
 * Author Marc Cromme <marc@ange.dk>
 */

#include "coprar20parser.h"

#include <QByteArray>
#include <QDateTime>
#include <QFile>
#include <QList>
#include <QString>

#include <QtEdith/predefinedformats.h>
#include <QtEdith/segmentgrouper.h>
#include <QtEdith/edifactsegmentreader.h>

#include <ange/containers/container.h>
#include <ange/containers/dangerousgoodscode.h>
#include <ange/containers/equipmentnumber.h>
#include <ange/containers/handlingcode.h>
#include <ange/containers/oog.h>
#include <ange/units/units.h>


// change this to get more noisy debug output for tests
#define NOISY_DEBUG 0

using namespace ange::units;
using namespace QtEdith;

CoprarParserResult Coprar20Parser::parse(EdifactSegmentReader* ediReader, const QtEdith::Segment& unb, const QString& expectedLoadPort) {

    Coprar20Parser that;
    that.m_expectedLoadPort = expectedLoadPort;

    const EdiHeader ediHeader = that.parseUnb(unb);

    const Segment unh = ediReader->peek();
    if (!Coprar20Parser::unhIsCoprar20(unh)) {
        that.m_errorList.append(EdiProblem(EdiProblem::Error, QString("Expected Coprar 2.0 UNH segment, got: ") + unh.print()));
        return CoprarParserResult(ediHeader, that.m_errorList);
    }

    // only allowing one Coprar message per interchange, as Angelstow can not import multiple Coprar messages
    const CoprarParserResult coprarResult = that.parseMessage(ediReader, ediHeader);
    if (ediReader->hasNext()) {
        const Segment unz = ediReader->next();
        if (unz.tag() != QByteArray("UNZ")) {
            that.m_errorList.append(EdiProblem(EdiProblem::Error, QString("Expected UNZ segment, got: ") + unz.print()));
        }
    } else {
        that.m_errorList.append(EdiProblem(EdiProblem::Warning, QString("Expected UNZ segment, got end of file")));
    }

    return coprarResult;
};

bool Coprar20Parser::unhIsCoprar20(const Segment& unh) {
    if (unh.tag() != QByteArray("UNH")) {
        return false;
    }
    if (unh.data(2, 1).asString() != QByteArray("COPRAR")) {
        return false;
    }
    if (unh.data(2, 2).asString() != QByteArray("D")) {
        return false;
    }
    if (unh.data(2, 3).asString() != QByteArray("00B")) {
        return false;
    }
    if (unh.data(2, 4).asString() != QByteArray("UN")) {
        return false;
    }
    if (unh.data(2, 5).asString() != QByteArray("SMDG20")) {
        return false;
    }
    return true;
}

/**
 * parses EDIACT UNB segment of form UNB+UNOA:2+UASC:ZZZ+CARGOSMART:ZZZ+140203:2302+669501'
 * @return EdiHeader in parsed form
 */
EdiHeader Coprar20Parser::parseUnb(const QtEdith::Segment& unb) {
    if(unb.tag() != QByteArray("UNB")) {
        m_errorList.append(EdiProblem(EdiProblem::Error, "Expected UNB segment UNB+UNOA:2+...+...+...+...' , got: " + unb.print()));
    }

    bool hasSenderId;
    const QByteArray senderId = unb.data(2, 1).asString(&hasSenderId);

    bool hasRecepientId;
    const QByteArray recipientId =  unb.data(3, 1).asString(&hasRecepientId);

    bool hasDate;
    bool hasTime;
    const QByteArray timeStampString = unb.data(4, 1).asString(&hasDate) + ":" + unb.data(4, 2).asString(&hasTime);

    if (!hasSenderId || !hasRecepientId || !hasDate || !hasTime) {
        m_errorList.append(EdiProblem(EdiProblem::Error, "Expected UNB segment UNB+UNOA:2+...+...+...+...' with senderId, recepientId and dateTime, got: " + unb.print()));
    }

    QDateTime interchangeTimeStamp = QDateTime().fromString(QString::fromUtf8(timeStampString), "yyMMdd:HHmm");
    if (!interchangeTimeStamp.isValid()) {
        m_errorList.append(EdiProblem(EdiProblem::Error, "Expected UNB segment UNB+UNOA:2+...+...+...+...' with valid dateTime, got: " + unb.print()));
    }
    if(interchangeTimeStamp.date().year() < 1950 ){
        interchangeTimeStamp = interchangeTimeStamp.addYears(100);
    }

    return EdiHeader(senderId, recipientId, interchangeTimeStamp);
}


CoprarParserResult Coprar20Parser::parseMessage(EdifactSegmentReader* ediReader, const EdiHeader& ediHeader) {

    const Segment unh = ediReader->peek();
    if (!Coprar20Parser::unhIsCoprar20(unh)) {
        m_errorList.append(EdiProblem(EdiProblem::Error, QString("Expected Coprar 2.0 UNH segment , got: ") + unh.print()));
        return CoprarParserResult(ediHeader, m_errorList);
    }

    SegmentGrouper grouper(PredefinedFormats::createCoprar00BTransitionTable());
    Group coprar = grouper.group(ediReader);
    if(grouper.errorCode() != 0) {
        m_errorList.append(EdiProblem(EdiProblem::Error, QString("Expected Coprar 2.0, got grouper error: ") + QString::number(grouper.errorCode()) + " " + grouper.errorMessage()));
        return CoprarParserResult(ediHeader, m_errorList);
    }
    if (coprar.isNull()) {
        m_errorList.append(EdiProblem(EdiProblem::Error, QString("Expected Coprar 2.0, got empty grouping")));
        return CoprarParserResult(ediHeader, m_errorList);
    }

    // SG0: UNH-BGM-DTM-SG1-SG2-SG4-SG6-CNT-UNT
    // SG1: RFF
    // SG2: TDT-RFF-SG3
    // SG3: LOC-DTM
    // SG4: NAD-SG5
    // SG5: CTA-COM
    const CoprarHeader coprarTransport = parseUnhBgmDtmSg1Sg2Sg4(coprar);

    // SG6: EQD-RFF-EQN-TMD-DTM-LOC-MEA-DIM-SG7-SEL-FTX-SG8-EQA-HAN-SG10-NAD
    // SG7: TMP-RNG
    // SG8: DGS-FTX-SG9
    // SG9: CTA-COM
    // SG10: TDT-DTM-RFF-SG11
    // SG11: LOC
    QList<ange::containers::Container*> containerList;

    Q_FOREACH(const Group group6, coprar.groupList(6)) {
        Coprar20Parser::parseSG6(group6, &containerList, coprarTransport.portCode() /*TODO fixme. Get the current call instead*/);
    }

    return CoprarParserResult(ediHeader, coprarTransport, containerList, m_errorList);
}

/**
 * SG0: UNH-BGM-DTM-SG1-SG2-SG4-SG6-CNT-UNT
 * SG1: RFF
 * SG2: TDT-RFF-SG3
 * SG3: LOC-DTM
 * SG4: NAD-SG5
 * SG5: CTA-COM
 */
CoprarHeader Coprar20Parser::parseUnhBgmDtmSg1Sg2Sg4(const Group& group0) {

    // SG0: UNH-BGM-DTM-(SG1-SG2-SG4-SG6-CNT-UNT)

    QByteArray messageReference;
    {
        // UNH+TAS34000009832+COPRAR:D:00B:UN:SMDG20'
        const QList<QtEdith::Segment> unhList = group0.segmentList("UNH");
        if (unhList.size() != 1) {
            m_errorList.append(EdiProblem(EdiProblem::Error, QString("Expected exactly one UNH segment, got: %1").arg(unhList.size())));
        } else {
            const Segment unh = unhList.first();
            messageReference = unh.data(1, 1).asString(QByteArray(), 0);
        }
    }

    QByteArray messageId;
    {
        //BGM+121+15044612062014150446120614+9+AB'
        const QList<QtEdith::Segment> bgmList = group0.segmentList("BGM");
        if (bgmList.size() != 1) {
            m_errorList.append(EdiProblem(EdiProblem::Error, QString("Expected exactly one BGM segment, got: %1").arg(bgmList.size())));
        } else {
            const Segment bgm = bgmList.first();
            messageId = bgm.data(2, 1).asString(QByteArray(), 0);
        }
    }

    QDateTime messageDateTime;
    {
        //DTM+137:201406121504:203'
        const QList<QtEdith::Segment> dtmList = group0.segmentList("DTM");
        if (dtmList.size() > 0) {
            const Segment dtm = dtmList.first();
            messageDateTime = Coprar20Parser::parseDtm(dtm, 137);
        }
        if (dtmList.size() > 1) {
            m_errorList.append(EdiProblem(EdiProblem::Info, QString("Reading only first DTM segment, ignoring: %1").arg(dtmList.size() - 1)));
        }
    }

    // SG1: RFF
        // RFF+AAY:15044612062014'
        // RFF+AAY:CSW'
        // RFF+AAY:EDIGTR868271'
        // ignored
        // const Group group1 = group0.groupList(1).first();

    // SG2: TDT-RFF-SG3
    QByteArray voyageCode;
    QByteArray vesselName;
    QByteArray imoNumber;
    QByteArray carrierId;
    // SG3: LOC-DTM
    QByteArray portOfLoad;
    QByteArray terminalOfLoad;
    // QByteArray portOfDischarge; // ignored
    // QByteArray terminalOfDischarge; // ignored
    {
        // TDT+20+1420W+1++CSC+++9152272:146::AL RAIN'
        // TDT+20+004W+1++COS:172:87+++9152272:146::AL RAIN'
        // TDT+20+1420W+1++UAC:::RAIN+++9152272:146::AL RAIN'
        const QList<Group> group2List = group0.groupList(2);
        if (group2List.size() != 1) {
            m_errorList.append(EdiProblem(EdiProblem::Error, QString("Expected exactly one group 2 starting with TDT, got: %1").arg(group2List.size())));
        }
        if (group2List.size() > 0) {
            const Group group2 = group2List.first();

            const QList<QtEdith::Segment> tdtList = group2.segmentList("TDT");
            if (tdtList.size() != 1) {
                m_errorList.append(EdiProblem(EdiProblem::Error, QString("Expected exactly one TDT segment, got: %1").arg(tdtList.size())));
            } else {
                const Segment tdt = tdtList.at(0);
                voyageCode = tdt.data(2, 1).asString(QByteArray(), 0);
                vesselName = tdt.data(8, 4).asString(QByteArray(), 0);
                imoNumber = tdt.data(8, 1).asString(QByteArray(), 0);
                carrierId = tdt.data(5, 1).asString(QByteArray(), 0);
            }

            // SG2 RFF
            // RFF+VON:00000'
            // RFF+ATZ:'
            // RFF+VON:506036'
            // ignored

            // SG3: LOC-DTM
            Q_FOREACH(const Group group3, group2.groupList(3)) {
                // LOC+9+ESALG+:72:ZZZ'
                // LOC+9+ESALG+A72116205:72:ZZZ:TERMINAL TTI DE ALGECIRAS'
                // LOC+9+ESALG+A72116205:72:ZZZ:TOTAL TERMINAL INTERNATIONAL ALGECIRAS (TTI)'
                const QList<QtEdith::Segment> locList = group3.segmentList("LOC");
                if (locList.size() != 1) {
                    // should not be possible, as each LOC starts a new SG3
                    m_errorList.append(EdiProblem(EdiProblem::Error, QString("Expected only one SG3 LOC segment, got: %1").arg(locList.size())));
                }
                if (locList.size() > 0 ) {
                    const Segment loc = locList.first();
                    switch ((int) loc.data(1, 1).asDouble(0, 0)) {
                        case 9: {
                            portOfLoad = loc.data(2, 1).asString(QByteArray(), 0);
                            terminalOfLoad = loc.data(3, 4).asString(QByteArray(), 0);
                            break;
                        }
                        case 11: {
                            m_errorList.append(EdiProblem(EdiProblem::Info, QString("Ignored SG3 LOC+11+... segment %1").arg(loc.print().constData())));
                            break;
                        }
                        default: {
                            m_errorList.append(EdiProblem(EdiProblem::Warning, QString("Expected SG3 LOC+..+... segment with function code 9 or 11, got: %1")
                                .arg(loc.print().constData())));
                        }
                    }
                }

                // SG3 DTM
                // ignored
            }
        }
    }

    if(portOfLoad != m_expectedLoadPort) {
        m_errorList.append(EdiProblem(EdiProblem::Warning, QString("Expected load port '%1', got: '%2'").arg(m_expectedLoadPort).arg(portOfLoad.constData())));
        portOfLoad = m_expectedLoadPort.toLatin1();
    }

    // SG4: NAD-SG5
    // NAD+MS+CSSC'
    // NAD+MR+'
    // NAD+MR+A72116205'
    // NAD+MS+A61361796'
    // NAD+MS+UAC'
    // NAD+MR+HTL'
    // ignored

    // SG5: CTA-COM
    // ignored

    return CoprarHeader(messageReference, messageId, messageDateTime, voyageCode, vesselName, imoNumber, carrierId, portOfLoad, terminalOfLoad);
}

/**
 *  DIM+13+CMT:::23'
 */
ange::containers::Oog Coprar20Parser::parseDimList(QList<QtEdith::Segment> dimList, const ange::containers::EquipmentNumber& equipment_number) {
    using ange::containers::Oog;
    double oogTop = 0;
    double oogLeft = 0;
    double oogRight = 0;
    double oogFront = 0;
    double oogBack = 0;
    Q_FOREACH(const Segment& dim, dimList) {
        bool success;
        int type = dim.data(1, 1).asDouble(&success);
        if(!success) {
            m_errorList.append(EdiProblem(EdiProblem::Warning,
                                         QString("Expected SG6 segment DIM+...+CMT:::...' with dimension type qualifier 1, 5, 6, 7, 8, 10 or 13 for container %1, got:%2")
                                         .arg(equipment_number).arg(dim.print().constData())));
            continue;
        }
        QByteArray unit = dim.data(2, 1).asString();
        if(unit != QByteArray("CMT") && unit != QByteArray("CM") ) { // accept common error 'CM' instead of 'CMT'
            m_errorList.append(EdiProblem(EdiProblem::EdiProblem::Warning,
                                         QString("Expected SG6 segment DIM+...+CMT:::...' with unit CMT for container %1, got unit %2 in: %3")
                                         .arg(equipment_number).arg(unit.constData()).arg(dim.print().constData())));
            continue;
        }
        switch(type) {
            case 1: {
                m_errorList.append(EdiProblem(EdiProblem::Info,
                                             QString("Ignored SG6 segment DIM+1+CMT:...:...:...' with dimension type qualifier 1 (Break Bulk) for container %1, got: %2")
                                             .arg(equipment_number).arg(dim.print().constData())));
                continue;
            }
            case 5: { // front
                bool ok;
                oogFront = dim.data(2, 2).asDouble(&ok);
                if(!ok) {
                    m_errorList.append(EdiProblem(EdiProblem::EdiProblem::Warning,
                                                 QString("Expected SG6 segment DIM+5+CMT:...::' with dimension type qualifier 5 (front OOG) data for container %1 got: %2")
                                                 .arg(equipment_number).arg(dim.print().constData())));
                }
                continue;
            }
            case 6: { // back
                bool ok;
                oogBack = dim.data(2, 2).asDouble(&ok);
                if(!ok) {
                    m_errorList.append(EdiProblem(EdiProblem::EdiProblem::Warning,
                                                 QString("Expected SG6 segment DIM+6+CMT:...::' with dimension type qualifier 6 (back OOG) data for container %1 got: %2")
                                                 .arg(equipment_number).arg(dim.print().constData())));
                }
                continue;
            }
            case 7: { // right
                bool ok;
                oogRight = dim.data(2, 3).asDouble(&ok);
                if(!ok) {
                    m_errorList.append(EdiProblem(EdiProblem::EdiProblem::Warning,
                                                 QString("Expected SG6 segment DIM+7+CMT::...:' with dimension type qualifier 7 (right OOG) data for container %1 got: %2")
                                                 .arg(equipment_number).arg(dim.print().constData())));
                }
                continue;
            }
            case 8: { // left
                bool ok;
                oogLeft = dim.data(2, 3).asDouble(&ok);
                if(!ok) {
                    m_errorList.append(EdiProblem(EdiProblem::EdiProblem::Warning,
                                                 QString("Expected SG6 segment DIM+8+CMT::...:' with dimension type qualifier 8 (left OOG) data for container %1 got: %2")
                                                 .arg(equipment_number).arg(dim.print().constData())));
                }
                continue;
            }
            case 13: { // top
                bool ok;
                oogTop = dim.data(2, 4).asDouble(&ok);
                if(!ok) {
                    m_errorList.append(EdiProblem(EdiProblem::EdiProblem::Warning,
                                                 QString("Expected SG6 segment DIM+13+CMT:::...' with dimension type qualifier 13 (top OOG) data for container %1 got: %2")
                                                 .arg(equipment_number).arg(dim.print().constData())));
                }
                continue;

            }
            default: {
                m_errorList.append(EdiProblem(EdiProblem::Info,
                                             QString("Ignored SG6 segment DIM+...+CMT:...:...:...' with unknown dimension type qualifier %1 for container %2, got:%3")
                                             .arg(type).arg(equipment_number).arg(dim.print().constData())));
                }
        }
    }

    if(!qFuzzyIsNull(oogTop) || !qFuzzyIsNull(oogBack) || !qFuzzyIsNull(oogFront) || !qFuzzyIsNull(oogLeft) || !qFuzzyIsNull(oogRight)) {
        Oog oog;
        oog.setBack(oogBack * centimeter);
        oog.setFront(oogFront * centimeter);
        oog.setLeft(oogLeft * centimeter);
        oog.setRight(oogRight * centimeter);
        oog.setTop(oogTop * centimeter);
        return oog;
    } else {
        return Oog();
    }
}

/**
 * Let's just go for the first temperature
 * TMP+2+-3:CEL'
 * TMP+2+048:FAH'
 */
double Coprar20Parser::parseGroup7(QList<Group> group7list) {
    Q_FOREACH(const Group& group, group7list) {
        QList<Segment> tmpSegments = group.segmentList(QByteArray("TMP"));
        if (tmpSegments.size() > 1) {
            m_errorList.append(EdiProblem(EdiProblem::EdiProblem::Info, QString("Expected only one SG6 segment TMP+2+...:...', ignoring %1 following segments")
            .arg(tmpSegments.size() -1)));
        }
        Q_FOREACH(const Segment& tmp, tmpSegments) {
            bool ok;
            double tmpValue = tmp.data(2, 1).asDouble(qQNaN(), &ok);
            if(!ok) {
                m_errorList.append(EdiProblem(EdiProblem::EdiProblem::Warning, QString("Expected SG6 segment TMP+2+...:...' with temperature data, got: %1")
                .arg(tmp.print().constData())));
                return qQNaN();
            }
            if(tmp.data(2, 2).asString() == QByteArray("CEL")) {
                return tmpValue;
            } else if(tmp.data(2, 2).asString() == QByteArray("FAH")) {
                tmpValue = (tmpValue - 32) * 5/9;
                return tmpValue;
            } else {
                m_errorList.append(EdiProblem(EdiProblem::EdiProblem::Warning, QString("Expected SG6 segment TMP+2+...:...' with unit code CEL or FAH, got: %1")
                .arg(tmp.print().constData())));
                return qQNaN();
            }
        }
    }
    return qQNaN();
}
/**
 * DGS+IMD+3+2348+037:CEL+3+3-03' (ignoring flash point !!!!)
 */
QList<ange::containers::DangerousGoodsCode> Coprar20Parser::parseGroup8(QList<Group> group8List) {
    QList<ange::containers::DangerousGoodsCode> dgcodes;
    Q_FOREACH(const Group& group8, group8List) {
        QList<Segment> dgsList = group8.segmentList("DGS");
        Q_FOREACH(const Segment& dgs, dgsList) {
            bool ok;
            QString undgNumber = QString::fromLocal8Bit(dgs.data(3, 1).asString(&ok));
            if (!ok) {
                m_errorList.append(EdiProblem(EdiProblem::EdiProblem::Warning, QString("Expected SG6 segment DGS+IMD+...+...+...' with UNDG number, got: %1")
                .arg(dgs.print().constData())));
            } else {
                dgcodes << ange::containers::DangerousGoodsCode(undgNumber);
            }
        }
    }
    return dgcodes;
}

QList< ange::containers::HandlingCode > Coprar20Parser::parseHandlingList(QList< Segment > hanList) {
    QList<ange::containers::HandlingCode> handlingCodes;
    Q_FOREACH(const Segment& segment, hanList) {
        handlingCodes << ange::containers::HandlingCode(QString::fromLocal8Bit(segment.data(1,1).asString()));
    }
    return handlingCodes;
}


/**
 * TODO parsing of all containers #1459 implement COPRAR:D:00B:UN:SMDG20
 *
 * SG6: EQD-RFF-EQN-TMD-DTM-LOC-MEA-DIM-SG7-SEL-FTX-SG8-EQA-HAN-SG10-NAD
 * SG7: TMP-RNG
 * SG8: DGS-FTX-SG9
 * SG9: CTA-COM
 * SG10: TDT-DTM-RFF-SG11
 * SG11: LOC
 **/
void Coprar20Parser::parseSG6(const QtEdith::Group& group6, QList<ange::containers::Container*>* containerList, const QString& coprarLoadPort) {

    using ange::containers::Container;
    using ange::containers::EquipmentNumber;
    using ange::containers::IsoCode;
    using ange::containers::DangerousGoodsCode;
    using ange::containers::Oog;
    using ange::units::Mass;

    EquipmentNumber equipment_number;
    Mass weight;
    IsoCode isocode("42G1"); // need default here, but error later if isocode not found correctly
    bool powered = false;
    double reefer_temperature = Container::unknownTemperature();
    bool empty = false;
    QList<DangerousGoodsCode> dangerous_goods;
    Oog oog;
    QString booking_number;
    QString carrier_code;


    // SG6: EQD-RFF-EQN-TMD-DTM-LOC-MEA-DIM-SG7-SEL-FTX-SG8-EQA-HAN-SG10-NAD

    {
        // EQD+CN+CCLU6602202+45G1++2+5'
        // EQD+CN+UETU5137317+45G1++2+5'
        // EQD+CN+40OTTNB1+42U1++6+4'
        const QList<QtEdith::Segment> eqdList = group6.segmentList("EQD");
        if (eqdList.size() != 1) {
            // should not be possible, as each EQD starts a new SG6
            m_errorList.append(EdiProblem(EdiProblem::Error, QString("Expected exactly one SG6 EQD segment, got: %1").arg(eqdList.size())));
            return;
        }
        const Segment eqd = eqdList.first();
        if (eqd.data(1, 1).asString(QByteArray(), 0) != QByteArray("CN")) {
            m_errorList.append(EdiProblem(EdiProblem::Info, QString("Ignored SG6 EQD segment %1").arg(eqd.print().constData())));
            return;
        }

        equipment_number = EquipmentNumber(eqd.data(2, 1).asString(QByteArray(), 0)); // accept empty equipment numbers
        {
            const QByteArray isocodeString = eqd.data(3, 1).asString(QByteArray(), 0);
            if (isocodeString.size() != 4) {
                m_errorList.append(EdiProblem(EdiProblem::Error, QString("Expected SG6 EQD segment with ISO code, got: %1").arg(eqd.print().constData())));
            } else {
                isocode = IsoCode(isocodeString);
            }
        }
        {
            int emptyCode = (int) eqd.data(6, 1).asDouble(0.0, 0);
            switch (emptyCode) {
                case 4: {
                    empty = true;
                    break;
                }
                case 5: {
                    empty = false;
                    break;
                }
                default: {
                    m_errorList.append(EdiProblem(EdiProblem::Error, QString("Expected SG6 EQD segment with empty code 4 or 5, got: %1").arg(eqd.print().constData())));
                }
            }
        }
    }

    {
        // RFF
        // RFF+BN:ALENYC100089'
        // RFF+BL:ALENYC100089'
        // RFF+BN:4504315480'
        // RFF+BN:CIABJ000516'
        // RFF+BM:CIABJ000516'
        const QList<QtEdith::Segment> rffList = group6.segmentList("RFF");
        Q_FOREACH(const Segment& rff, rffList) {
            if(rff.data(1, 1).asString(QByteArray(), 0) == QByteArray("BN")) {
                booking_number = rff.data(1, 2).asString(QByteArray(), 0); // overwriting multiple bookung numbers, last wins
            }
        }
    }

    // EQN
    // ignored

    // TMD
    // TODO #1459 implement COPRAR:D:00B:UN:SMDG20
    // TDM skipped. We don't really support it yet.

    // DTM
    // TODO #1459 implement COPRAR:D:00B:UN:SMDG20
    // DTM_skipped. We don't really support that.

    // LOC
    QString portOfLoad;
    QString portOfDischarge;
    QString placeOfDelivery;
    const QList<QtEdith::Segment> locList = group6.segmentList("LOC");
    if (locList.size() < 2) {
        m_errorList.append(EdiProblem(EdiProblem::Warning, QString("Too few LOC segments for container %1 got: %2").arg(equipment_number).arg(locList.size())));
    }
    Q_FOREACH(const Segment& loc, locList) {
        switch ((int) loc.data(1, 1).asDouble()) {
            case 7:
            case 8: { // We treat 'place of delivery' and 'place of destination' being the same.
                placeOfDelivery = loc.data(2,1).asString(0);
                break;
            }
            case 76: // 76 is not described in coprar standard, but is original port of loading in the edifact standard
            case 9: {
                portOfLoad = loc.data(2, 1).asString(QByteArray(), 0);
                break;
            }
            case 11: {
                portOfDischarge = loc.data(2,1).asString(0);
                break;
            }
            default: {
                m_errorList.append(EdiProblem(EdiProblem::Warning, QString("Unexpected LOC segment for container %1: %2")
                    .arg(equipment_number).arg(loc.print().constData())));
            }
        }
    }
    if(!portOfLoad.isEmpty()) {
        if(portOfLoad != coprarLoadPort) {
            m_errorList.append(EdiProblem(EdiProblem::Warning, QString("Expected all containers having load port %1, got container %2 with load port %3")
            .arg(m_expectedLoadPort).arg(equipment_number).arg(portOfLoad)));
        }
    }
    portOfLoad = coprarLoadPort;

    // MEA
    // TODO #1459 implement COPRAR:D:00B:UN:SMDG20
    // MEA+AAE+G+KGM:26728
    const QList<QtEdith::Segment> meaList = group6.segmentList("MEA");
    if(meaList.size() != 1) {
        m_errorList.append(EdiProblem(EdiProblem::Info, QString("Expected one MEA segment for container %1, got: %2").arg(equipment_number).arg(meaList.size())));
    }
    int weightInKg = -1;
    Q_FOREACH(const Segment& mea, meaList) {
        QByteArray type = mea.data(2,1).asString();
        if(type == QByteArray("G") || type == QByteArray("AET")) {
            QByteArray unit = mea.data(3,1).asString();
            if(unit == QByteArray("KGM")) {
                bool success;
                int readWeight = mea.data(3,2).asDouble(&success);
                if(success) {
                    weightInKg = readWeight;
                } else {
                    m_errorList.append(EdiProblem(EdiProblem::Warning, QString("Expected SG6 MEA segment with weight data in 'KGM', got: %1").arg(mea.print().constData())));
                }
            } else {
                m_errorList.append(EdiProblem(EdiProblem::Error, QString("Expected SG6 MEA segment with unit 'KGM', got: %1").arg(mea.print().constData())));
            }
        }
    }
    if(weightInKg == -1) {
        m_errorList.append(EdiProblem(EdiProblem::Info,QString("Expected SG6 MEA segment with weight data for container %1, but got none").arg(equipment_number)));
    }

    // DIM
    QList<QtEdith::Segment> dimList = group6.segmentList("DIM");
    oog = parseDimList(dimList, equipment_number);

    // SG7: TMP-RNG
    QList<Group> group7 = group6.groupList(7);
    if(!group7.isEmpty()) {
        double tmp = parseGroup7(group7);
        if(!qIsNaN(tmp)) {
            reefer_temperature = tmp;
        }
    }
    // group 6: SEL. Ignored
    // group 6: FTX. Ignored

    // SG8: DGS-FTX-SG9
    // TODO #1459 implement COPRAR:D:00B:UN:SMDG20
    QList<Group> group8 = group6.groupList(8);
    if(!group8.isEmpty()) {
        dangerous_goods = parseGroup8(group8);
    }

    // SG9: CTA-COM - not yet supported
    // TODO #1459 implement COPRAR:D:00B:UN:SMDG20

    // TODO group6 HAN

    // SG10: TDT-DTM-RFF-SG11 - ignored
    // TODO #1459 implement COPRAR:D:00B:UN:SMDG20

    // SG11: LOC -- ignored
    // TODO #1459 implement COPRAR:D:00B:UN:SMDG20

    Container* container = new Container(equipment_number, weightInKg * kilogram, isocode);
    container->setLive(powered ? Container::Live : Container::NonLive);
    container->setTemperature(reefer_temperature);
    container->setEmpty(empty);
    container->setDangerousGoodsCodes(dangerous_goods);
    container->setOog(oog);
    container->setBookingNumber(booking_number);
    container->setCarrierCode(carrier_code);
    container->setDischargePort(portOfDischarge);
    container->setLoadPort(portOfLoad);
    container->setPlaceOfDelivery(placeOfDelivery);
    containerList->append(container);

};

QDateTime Coprar20Parser::parseDtm(const QtEdith::Segment& dtm, int expectedTypeCode) {
    // DTM+137:201406121504:203'

    if(dtm.tag() != QByteArray("DTM")) {
        m_errorList.append(EdiProblem(EdiProblem::Error, "Expected DTM segment DTM+...:...:...' , got: " + dtm.print()));
        return QDateTime();
    }

    bool hasTypeCode;
    int typeCode = (int) dtm.data(1, 1).asDouble(&hasTypeCode);
    if(!hasTypeCode || (typeCode != expectedTypeCode)) {
        m_errorList.append(EdiProblem(EdiProblem::Error, "Expected DTM segment DTM+...:...:...' with type code "
                    + QString::number(expectedTypeCode) + ", got: " + dtm.print()));
        return QDateTime();
    }

    bool hasFormatCode;
    int formatCode = (int) dtm.data(1, 3).asDouble(&hasFormatCode);
    if(!hasFormatCode) {
        m_errorList.append(EdiProblem(EdiProblem::Error, "Expected DTM segment DTM+...:...:...' with type format 101, 201, 203 or 301, got: "
            + dtm.print()));
        return QDateTime();
    }

    QString format;
    if ( formatCode == 101) {
        format = "yyMMdd";
    } else if ( formatCode == 201) {
        format = "yyMMddHHmm";
    } else if ( formatCode == 301 ) {
        format = "yyMMddHHmm";
    } else if ( formatCode == 203) {
        format = "yyyyMMddHHmm";
    } else {
        m_errorList.append(EdiProblem(EdiProblem::Error, "Expected DTM segment DTM+...:...:...' with type format 101, 201, 203 or 301, got: "
        + dtm.print()));
        return QDateTime();
    }

    bool hasDateTime;
    QByteArray dateTimeString = dtm.data(1, 2).asString(&hasDateTime);
    if(!hasDateTime) {
        m_errorList.append(EdiProblem(EdiProblem::Error, "Expected DTM segment DTM+...:...:...' with valid date time, got: "
        + dtm.print()));
        return QDateTime();
    }

    QDateTime dateTime = QDateTime().fromString(QString::fromUtf8(dateTimeString), format);
    if (!dateTime.isValid())  {
        m_errorList.append(EdiProblem(EdiProblem::Error, "Expected DTM segment DTM+...:...:...' with valid date time, got: "
        + dtm.print()));
        return QDateTime();
    }

    if(dateTime.date().year() < 1950 ){
        dateTime = dateTime.addYears(100);
    }

    return dateTime;
}



