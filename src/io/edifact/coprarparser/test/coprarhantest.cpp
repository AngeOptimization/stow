#include <QtTest>
#include <QObject>
#include <coprar20parser.h>

namespace QTest {
    template <>
    inline char* toString(const ange::containers::HandlingCode& code) {
        return QTest::toString<QString>(code.code());
    }
}

class CoprarHanTest : public QObject {
    Q_OBJECT
    private Q_SLOTS:
        void parseSimpleHan();
        void parseMultipleHan();
};

void CoprarHanTest::parseSimpleHan() {
    QByteArray data("HAN+RFR:130:184'");

    QBuffer ioDevice(&data);
    QVERIFY(ioDevice.open(QIODevice::ReadOnly));

    QtEdith::EdifactSegmentReader reader(&ioDevice);

    QList<QtEdith::Segment> segments;
    while(reader.hasNext()) {
        segments << reader.next();
    }
    QCOMPARE(reader.errorCode(),0);

    QCOMPARE(segments.size(),1);

    Coprar20Parser coprarParser;
    QList<ange::containers::HandlingCode> handlingCodes = coprarParser.parseHandlingList(segments);

    #if 0
    Q_FOREACH(const EdiProblem problem, coprarParser.m_errorList) {
        qDebug() << problem.typeString() << "" << problem.message();
    }
    #endif
    QVERIFY(coprarParser.m_errorList.isEmpty());

    QCOMPARE(handlingCodes.size(), 1);

    QCOMPARE(handlingCodes.first(), ange::containers::HandlingCode(QStringLiteral("RFR")));
}

void CoprarHanTest::parseMultipleHan() {
    QByteArray data("HAN+RFR:130:184'HAN+HEST:130:148'");

    QBuffer ioDevice(&data);
    QVERIFY(ioDevice.open(QIODevice::ReadOnly));

    QtEdith::EdifactSegmentReader reader(&ioDevice);

    QList<QtEdith::Segment> segments;
    while(reader.hasNext()) {
        segments << reader.next();
    }
    QCOMPARE(reader.errorCode(),0);

    QCOMPARE(segments.size(),2);

    Coprar20Parser coprarParser;
    QList<ange::containers::HandlingCode> handlingCodes = coprarParser.parseHandlingList(segments);

    #if 0
    Q_FOREACH(const EdiProblem problem, coprarParser.m_errorList) {
        qDebug() << problem.typeString() << "" << problem.message();
    }
    #endif
    QVERIFY(coprarParser.m_errorList.isEmpty());
    QCOMPARE(handlingCodes.size(), 2);

    QCOMPARE(handlingCodes.first(), ange::containers::HandlingCode(QStringLiteral("RFR")));
    QCOMPARE(handlingCodes.last(), ange::containers::HandlingCode(QStringLiteral("HEST")));
}


QTEST_GUILESS_MAIN(CoprarHanTest);
#include "coprarhantest.moc"
