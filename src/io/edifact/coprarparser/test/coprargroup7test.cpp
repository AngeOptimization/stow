#include <QtTest>

#include "coprar20parser.h"

#include <QObject>
#include <QtEdith/edifactsegmentreader.h>
#include <QtEdith/group.h>

class CoprarGroup7Test : public QObject {
    Q_OBJECT
    private Q_SLOTS:
        void testParseTmpFah();
        void testParseTmpCel();
        void testParseTmpInvalid();
        void testParseTmpEmpty();
};

void CoprarGroup7Test::testParseTmpFah() {
    QByteArray data("TMP+2+048:FAH'");
    QBuffer ioDevice(&data);
    QVERIFY(ioDevice.open(QIODevice::ReadOnly));
    QtEdith::EdifactSegmentReader reader(&ioDevice);

    QtEdith::GroupBuilder groupBuilder;
    while(reader.hasNext()) {
        groupBuilder.add(reader.next());
    }

    QtEdith::Group group7 = groupBuilder.build();

    QCOMPARE(reader.errorCode(),0);

    Coprar20Parser coprarParser;
    double tmp = coprarParser.parseGroup7(QList<QtEdith::Group>() << group7);
    QCOMPARE(tmp, 8.88888888888888);
    QVERIFY(coprarParser.m_errorList.isEmpty());
}

void CoprarGroup7Test::testParseTmpCel() {
    QByteArray data("TMP+2+5.5:CEL'");
    QBuffer ioDevice(&data);
    QVERIFY(ioDevice.open(QIODevice::ReadOnly));
    QtEdith::EdifactSegmentReader reader(&ioDevice);

    QtEdith::GroupBuilder groupBuilder;
    while(reader.hasNext()) {
        groupBuilder.add(reader.next());
    }

    QtEdith::Group group7 = groupBuilder.build();

    QCOMPARE(reader.errorCode(),0);

    Coprar20Parser coprarParser;
    double tmp = coprarParser.parseGroup7(QList<QtEdith::Group>() << group7);
    QCOMPARE(tmp, 5.5);
    QVERIFY(coprarParser.m_errorList.isEmpty());
}

void CoprarGroup7Test::testParseTmpInvalid() {
    QByteArray data("TMP+2+5.5:LOL'");
    QBuffer ioDevice(&data);
    QVERIFY(ioDevice.open(QIODevice::ReadOnly));
    QtEdith::EdifactSegmentReader reader(&ioDevice);

    QtEdith::GroupBuilder groupBuilder;
    while(reader.hasNext()) {
        groupBuilder.add(reader.next());
    }

    QtEdith::Group group7 = groupBuilder.build();

    QCOMPARE(reader.errorCode(), 0);

    Coprar20Parser coprarParser;
    double tmp = coprarParser.parseGroup7(QList<QtEdith::Group>() << group7);
    QVERIFY(qIsNaN(tmp));
    QCOMPARE(coprarParser.m_errorList.size(), 1);
    QVERIFY(coprarParser.m_errorList.first().message().contains(QStringLiteral("LOL")));
}

void CoprarGroup7Test::testParseTmpEmpty() {
    QtEdith::GroupBuilder groupBuilder;
    QtEdith::Group group7 = groupBuilder.build();

    Coprar20Parser coprarParser;
    double tmp = coprarParser.parseGroup7(QList<QtEdith::Group>() << group7);
    QVERIFY(qIsNaN(tmp));
    QVERIFY(coprarParser.m_errorList.isEmpty());
}




QTEST_GUILESS_MAIN(CoprarGroup7Test);
#include "coprargroup7test.moc"
