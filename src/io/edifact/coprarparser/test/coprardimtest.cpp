#include <QtTest>
#include "coprar20parser.h"

#include <ange/containers/oog.h>
#include <ange/units/units.h>
#include <QtEdith/edifactsegmentreader.h>

using namespace ange::units;

class CoprarDimTest : public QObject {
    Q_OBJECT
    private Q_SLOTS:
        void testSimpleDim();
        void testSimpleDimWithUnitWarning();
};

void CoprarDimTest::testSimpleDim() {
    QByteArray data("DIM+5+CMT:50'DIM+6+CMT:60'DIM+7+CMT::70'DIM+8+CMT::80'DIM+13+CMT:::130'");

    QBuffer ioDevice(&data);
    QVERIFY(ioDevice.open(QIODevice::ReadOnly));

    QtEdith::EdifactSegmentReader reader(&ioDevice);

    QList<QtEdith::Segment> segments;
    while(reader.hasNext()) {
        segments << reader.next();
    }
    QCOMPARE(reader.errorCode(), 0);

    QCOMPARE(segments.size(), 5);

    ange::containers::EquipmentNumber eqnum("TESTCONTAINER");

    Coprar20Parser coprarParser;
    ange::containers::Oog oog = coprarParser.parseDimList(segments, eqnum);

    #if 0
    Q_FOREACH(const EdiProblem problem, coprarParser.m_errorList) {
        qDebug() << problem.typeString() << "" << problem.message();
    }
    #endif
    QVERIFY(coprarParser.m_errorList.isEmpty());
    QVERIFY(oog);

    QCOMPARE(oog.front(), 50.0 * centimeter);
    QCOMPARE(oog.back(), 60.0 * centimeter);
    QCOMPARE(oog.right(), 70.0 * centimeter);
    QCOMPARE(oog.left(), 80.0 * centimeter);
    QCOMPARE(oog.top(), 130.0 * centimeter);
}

void CoprarDimTest::testSimpleDimWithUnitWarning() {
    // accept common error 'CM' instead of 'CMT'
    QByteArray data("DIM+5+CM:50'DIM+7+CMT::70'DIM+8+CMT::80'DIM+13+FAH:::130'");

    QBuffer ioDevice(&data);
    QVERIFY(ioDevice.open(QIODevice::ReadOnly));

    QtEdith::EdifactSegmentReader reader(&ioDevice);

    QList<QtEdith::Segment> segments;
    while(reader.hasNext()) {
        segments << reader.next();
    }
    QCOMPARE(reader.errorCode(), 0);

    QCOMPARE(segments.size(), 4);

    ange::containers::EquipmentNumber eqnum("TESTCONTAINER");

    Coprar20Parser coprarParser;
    ange::containers::Oog oog = coprarParser.parseDimList(segments, eqnum);

    #if 0
    Q_FOREACH(const EdiProblem problem, coprarParser.m_errorList) {
        qDebug() << problem.typeString() << "" << problem.message();
    }
    #endif
    QCOMPARE(coprarParser.m_errorList.size(), 1);
    QVERIFY(coprarParser.m_errorList.first().message().contains(QStringLiteral("FAH")));
    QVERIFY(oog);

    QCOMPARE(oog.front(), 50.0 * centimeter);
    QCOMPARE(oog.back(), 0.0 * centimeter);
    QCOMPARE(oog.right(), 70.0 * centimeter);
    QCOMPARE(oog.left(), 80.0 * centimeter);
    QCOMPARE(oog.top(), 0.0 * centimeter);
}


QTEST_GUILESS_MAIN(CoprarDimTest);
#include "coprardimtest.moc"
