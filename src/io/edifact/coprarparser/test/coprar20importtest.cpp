#include <QtTest>

#include <QObject>
#include <coprar20parser.h>
#include <QtEdith/segment.h>
#include <QtEdith/edifactsegmentreader.h>

using namespace ange::units;
using namespace QtEdith;

class Coprar20ImportTest : public QObject {
    Q_OBJECT
    private Q_SLOTS:
        void testFile01();
        void testFile01DifferentLoad();
        void testFile02();
};

void Coprar20ImportTest::testFile01() {
    QString file = QFINDTESTDATA("data/coprar20_BK2COPRARYL74_1420_W_ESALG_X_5110156.edi");
    QVERIFY(QFile::exists(file));

    QFile f(file);
    QVERIFY(f.open(QIODevice::ReadOnly));

    EdifactSegmentReader ediReader(&f);

    Segment unb = ediReader.next();
    QCOMPARE(unb.tag(),QByteArray("UNB"));

    Segment unh = ediReader.peek();
    QVERIFY(Coprar20Parser::unhIsCoprar20(unh));

    CoprarParserResult result = Coprar20Parser::parse(&ediReader, unb, QStringLiteral("ESALG"));
    QVERIFY(!result.hasErrors());

    int warningCount = 0;
    int errorCount = 0;
    int infoCount = 0;
    Q_FOREACH(const EdiProblem& error, result.errorList()) {
        switch(error.type()) {
            case EdiProblem::Warning: {
                warningCount++;
                break;
            }
            case EdiProblem::Error: {
                errorCount++;
                break;
            }
            case EdiProblem::Info: {
                infoCount++;
                break;
            }
        }
    }
    QCOMPARE(warningCount,0);
    QCOMPARE(errorCount,0);
    QCOMPARE(infoCount,0);

    QCOMPARE(QString::fromLocal8Bit(result.coprarTransport().carrierId()), QString::fromLocal8Bit(QByteArray("CSC")));
    QCOMPARE(QString::fromLocal8Bit(result.coprarTransport().imoNumber()), QString::fromLocal8Bit(QByteArray("9152272")));
    QCOMPARE(QString::fromLocal8Bit(result.coprarTransport().portCode()), QString::fromLocal8Bit(QByteArray("ESALG")));
    QCOMPARE(QString::fromLocal8Bit(result.coprarTransport().voyageCode()), QString::fromLocal8Bit(QByteArray("1420W")));

    QCOMPARE(result.containerList().size(),2);

    ange::containers::Container* container1 = result.containerList().at(0);

    QCOMPARE(container1->isoCode(), ange::containers::IsoCode("45G1"));
    QCOMPARE(container1->equipmentNumber(), ange::containers::EquipmentNumber("CCLU7089584"));
    QCOMPARE(container1->bookingNumber(), QStringLiteral("ALENYC100089"));
    QVERIFY(!container1->empty());
    QCOMPARE(container1->placeOfDelivery(), QStringLiteral("USNYC"));
    QCOMPARE(container1->dischargePort(), QStringLiteral("USNYC"));
    QCOMPARE(container1->loadPort(), QStringLiteral("ESALG"));
    QCOMPARE(container1->weight()/kilogram, double(22664));

    ange::containers::Container* container2 = result.containerList().at(1);

    QCOMPARE(container2->isoCode(), ange::containers::IsoCode("45G1"));
    QCOMPARE(container2->equipmentNumber(), ange::containers::EquipmentNumber("CCLU6602202"));
    QCOMPARE(container2->bookingNumber(), QStringLiteral("ALENYC100090"));
    QVERIFY(!container2->empty());
    QCOMPARE(container2->placeOfDelivery(), QStringLiteral("USNYC"));
    QCOMPARE(container2->dischargePort(), QStringLiteral("USNYC"));
    QCOMPARE(container2->loadPort(), QStringLiteral("ESALG"));
    QCOMPARE(container2->weight()/kilogram, double(22720));
}

void Coprar20ImportTest::testFile01DifferentLoad() {
    QString file = QFINDTESTDATA("data/coprar20_BK2COPRARYL74_1420_W_ESALG_X_5110156.edi");
    QVERIFY(QFile::exists(file));

    QFile f(file);
    QVERIFY(f.open(QIODevice::ReadOnly));

    EdifactSegmentReader ediReader(&f);

    Segment unb = ediReader.next();
    QCOMPARE(unb.tag(),QByteArray("UNB"));

    Segment unh = ediReader.peek();
    QVERIFY(Coprar20Parser::unhIsCoprar20(unh));

    CoprarParserResult result = Coprar20Parser::parse(&ediReader, unb, QStringLiteral("TESTE"));
    QVERIFY(!result.hasErrors());

    QCOMPARE(QString::fromLocal8Bit(result.coprarTransport().carrierId()), QString::fromLocal8Bit(QByteArray("CSC")));
    QCOMPARE(QString::fromLocal8Bit(result.coprarTransport().imoNumber()), QString::fromLocal8Bit(QByteArray("9152272")));
    QCOMPARE(QString::fromLocal8Bit(result.coprarTransport().portCode()), QString::fromLocal8Bit(QByteArray("TESTE")));
    QCOMPARE(QString::fromLocal8Bit(result.coprarTransport().voyageCode()), QString::fromLocal8Bit(QByteArray("1420W")));

    QCOMPARE(result.containerList().size(),2);

    QVERIFY(result.hasWarnings());

    int warningCount = 0;
    int errorCount = 0;
    int infoCount = 0;
    int loadPortWarning = 0;
    Q_FOREACH(const EdiProblem& error, result.errorList()) {
        switch(error.type()) {
            case EdiProblem::Warning: {
                warningCount++;
                if(error.message().contains("TESTE")) {
                    loadPortWarning++;
                }
                break;
            }
            case EdiProblem::Error: {
                errorCount++;
                break;
            }
            case EdiProblem::Info: {
                infoCount++;
                break;
            }
        }
    }
    QCOMPARE(warningCount,3);
    QCOMPARE(loadPortWarning,3);
    QCOMPARE(errorCount,0);
    QCOMPARE(infoCount,0);

    ange::containers::Container* container1 = result.containerList().at(0);

    QCOMPARE(container1->isoCode(), ange::containers::IsoCode("45G1"));
    QCOMPARE(container1->equipmentNumber(), ange::containers::EquipmentNumber("CCLU7089584"));
    QCOMPARE(container1->bookingNumber(), QStringLiteral("ALENYC100089"));
    QVERIFY(!container1->empty());
    QCOMPARE(container1->placeOfDelivery(), QStringLiteral("USNYC"));
    QCOMPARE(container1->dischargePort(), QStringLiteral("USNYC"));
    QCOMPARE(container1->loadPort(), QStringLiteral("TESTE"));
    QCOMPARE(container1->weight()/kilogram, double(22664));

    ange::containers::Container* container2 = result.containerList().at(1);

    QCOMPARE(container2->isoCode(), ange::containers::IsoCode("45G1"));
    QCOMPARE(container2->equipmentNumber(), ange::containers::EquipmentNumber("CCLU6602202"));
    QCOMPARE(container2->bookingNumber(), QStringLiteral("ALENYC100090"));
    QVERIFY(!container2->empty());
    QCOMPARE(container2->placeOfDelivery(), QStringLiteral("USNYC"));
    QCOMPARE(container2->dischargePort(), QStringLiteral("USNYC"));
    QCOMPARE(container2->loadPort(), QStringLiteral("TESTE"));
    QCOMPARE(container2->weight()/kilogram, double(22720));
}

void Coprar20ImportTest::testFile02() {
    QString file = QFINDTESTDATA("data/coprar20_EDIGTR868271.edi");
    QVERIFY(QFile::exists(file));

    QFile f(file);
    QVERIFY(f.open(QIODevice::ReadOnly));

    EdifactSegmentReader ediReader(&f);

    Segment unb = ediReader.next();
    QCOMPARE(unb.tag(),QByteArray("UNB"));

    Segment unh = ediReader.peek();
    QVERIFY(Coprar20Parser::unhIsCoprar20(unh));

    CoprarParserResult result = Coprar20Parser::parse(&ediReader, unb, QStringLiteral("ESALG"));
    QVERIFY(!result.hasErrors());

    int warningCount = 0;
    int errorCount = 0;
    int infoCount = 0;
    Q_FOREACH(const EdiProblem& error, result.errorList()) {
        switch(error.type()) {
            case EdiProblem::Warning: {
                warningCount++;
                break;
            }
            case EdiProblem::Error: {
                errorCount++;
                break;
            }
            case EdiProblem::Info: {
                infoCount++;
                break;
            }
        }
    }
    QCOMPARE(warningCount,0);
    QCOMPARE(errorCount,0);
    QCOMPARE(infoCount,0);

    QCOMPARE(QString::fromLocal8Bit(result.coprarTransport().carrierId()), QString::fromLocal8Bit(QByteArray("UAC")));
    QCOMPARE(QString::fromLocal8Bit(result.coprarTransport().imoNumber()), QString::fromLocal8Bit(QByteArray("9152272")));
    QCOMPARE(QString::fromLocal8Bit(result.coprarTransport().portCode()), QString::fromLocal8Bit(QByteArray("ESALG")));
    QCOMPARE(QString::fromLocal8Bit(result.coprarTransport().voyageCode()), QString::fromLocal8Bit(QByteArray("1420W")));

    QCOMPARE(result.containerList().size(),48);

    ange::containers::Container* container1 = result.containerList().at(0);

    QCOMPARE(container1->isoCode(), ange::containers::IsoCode("22G1"));
    QCOMPARE(container1->equipmentNumber(), ange::containers::EquipmentNumber("UACU3297179"));
    QCOMPARE(container1->bookingNumber(), QStringLiteral("CIABJ000516"));
    QVERIFY(!container1->empty());
    QCOMPARE(container1->placeOfDelivery(), QStringLiteral("USNYC"));
    QCOMPARE(container1->dischargePort(), QStringLiteral("USNYC"));
    QCOMPARE(container1->loadPort(), QStringLiteral("ESALG"));
    QCOMPARE(container1->weight()/kilogram, double(26728));

    ange::containers::Container* container2 = result.containerList().at(1);

    QCOMPARE(container2->isoCode(), ange::containers::IsoCode("42G1"));
    QCOMPARE(container2->equipmentNumber(), ange::containers::EquipmentNumber("WLNU4905972"));
    QCOMPARE(container2->bookingNumber(), QStringLiteral("ITGOA187108"));
    QVERIFY(!container2->empty());
    QCOMPARE(container2->placeOfDelivery(), QStringLiteral("USNYC"));
    QCOMPARE(container2->dischargePort(), QStringLiteral("USNYC"));
    QCOMPARE(container2->loadPort(), QStringLiteral("ESALG"));
    QCOMPARE(container2->weight()/kilogram, double(16028));

    /*
    EQD+CN+UACU8193198+42G1++2+5'
    RFF+BN:ESBCN035868'
    RFF+BM:ESBCN035868'
    LOC+9+ESALG'
    LOC+11+USORF'
    LOC+8+USCHI'
    MEA+AAE+G+KGM:12424'
    */
    ange::containers::Container* container42 = result.containerList().at(41);

    QCOMPARE(container42->equipmentNumber(), ange::containers::EquipmentNumber("UACU8193198"));
    QCOMPARE(container42->isoCode(), ange::containers::IsoCode("42G1"));
    QCOMPARE(container42->bookingNumber(), QStringLiteral("ESBCN035868"));
    QVERIFY(!container42->empty());
    QCOMPARE(container42->placeOfDelivery(), QStringLiteral("USCHI"));
    QCOMPARE(container42->dischargePort(), QStringLiteral("USORF"));
    QCOMPARE(container42->loadPort(), QStringLiteral("ESALG"));
    QCOMPARE(container42->weight()/kilogram, double(12424));

}


QTEST_GUILESS_MAIN(Coprar20ImportTest);
#include "coprar20importtest.moc"
