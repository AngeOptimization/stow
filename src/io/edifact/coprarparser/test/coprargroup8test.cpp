#include <QtTest>
#include <QObject>
#include <coprar20parser.h>
#include <ange/containers/dangerousgoodscode.h>

class CoprarGroup8Test : public QObject {
    Q_OBJECT
    private Q_SLOTS:
        void testParseDGSSegment();
};

void CoprarGroup8Test::testParseDGSSegment() {
    QByteArray data("DGS+IMD+3+2348+037:CEL+3+3-03'");
    QBuffer ioDevice(&data);
    QVERIFY(ioDevice.open(QIODevice::ReadOnly));
    QtEdith::EdifactSegmentReader reader(&ioDevice);

    QtEdith::GroupBuilder groupBuilder;
    while(reader.hasNext()) {
        groupBuilder.add(reader.next());
    }

    QtEdith::Group group8 = groupBuilder.build();

    QCOMPARE(reader.errorCode(), 0);

    Coprar20Parser coprarParser;
    QList<ange::containers::DangerousGoodsCode> dgsList = coprarParser.parseGroup8(QList<QtEdith::Group>() << group8);
    QCOMPARE(dgsList.size(), 1);
    QCOMPARE(dgsList.first(), ange::containers::DangerousGoodsCode("2348"));
    #if 0
        Q_FOREACH(const EdiProblem problem, coprarParser.m_errorList) {
        qDebug() << problem.typeString() << "" << problem.message();
    }
    #endif
    QVERIFY(coprarParser.m_errorList.isEmpty());
}


QTEST_GUILESS_MAIN(CoprarGroup8Test)
#include "coprargroup8test.moc"
