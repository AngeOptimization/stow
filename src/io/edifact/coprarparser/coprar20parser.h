/**
 *
 * COPRAR 2.0 parser implemetation based on own QtEdith EDIFACT segment grouper
 *
 * SMDG
 * User Manual( Implementation Guide )
 * UN/EDIFACT MESSAGE
 * COPRAR (Container discharge/loading order)
 * Version 2.0
 * D00B
 *
 * Copyright Ange Optimization Aps, 2014
 * Author Marc Cromme <marc@ange.dk>
 */

#include "ediproblem.h"

#include <ange/containers/container.h>

#include <QtEdith/segment.h>
#include <QtEdith/edifactsegmentreader.h>
#include <QtEdith/segmentgrouper.h>

#include <QByteArray>
#include <QDateTime>
#include <QList>

/**
 * EdiHeader is an immutable object being part of an EDIFACT parser result and represents the information captured in the
 * interchange header
 */
class EdiHeader {

public:

    /**
     * empty constructor
     */
    EdiHeader()
    : m_senderId(QByteArray()),
      m_recipientId(QByteArray()),
      m_timeStamp(QDateTime())
    {
    };

    /**
     * @param senderId
     * @param recipientId
     * @param timeStamp
     */
    EdiHeader(const QByteArray& senderId, const QByteArray& recipientId, const QDateTime& timeStamp)
    : m_senderId(senderId),
      m_recipientId(recipientId),
      m_timeStamp(timeStamp)
    {
    };

    /**
     * @return the senderId
     */
    QByteArray senderId() const {
        return m_senderId;
    };

    /**
     * @return the recipientId
     */
    QByteArray recipientId() const {
        return m_recipientId;
    };

    /**
     * @return the timeStamp
     */
    QDateTime timeStamp() const {
        return m_timeStamp;
    };


private:

    QByteArray m_senderId;

    QByteArray m_recipientId;

    QDateTime m_timeStamp;
};

/**
 * COPRAR message header data. Immutable.
 */
class CoprarHeader {

public:

    /**
     * empty constructor
     */
    CoprarHeader()
    : m_messageReference(QByteArray()),
      m_messageId(QByteArray()),
      m_messageDateTime(),
      m_voyageCode(QByteArray()),
      m_vesselName(QByteArray()),
      m_imoNumber(QByteArray()),
      m_carrierId(QByteArray()),
      m_portCode(QByteArray()),
      m_terminalName(QByteArray())
    {
    };

    /**
     * @param messageReference
     * @param messageId
     * @param voyageCode
     * @param vesselName
     * @param imoNumber
     * @param carrierId
     * @param portCode
     * @param terminalName
     */
    CoprarHeader(const QByteArray& messageReference,
                const QByteArray& messageId,
                const QDateTime messageDateTime,
                const QByteArray& voyageCode,
                const QByteArray& vesselName,
                const QByteArray& imoNumber,
                const QByteArray& carrierId,
                const QByteArray& portCode,
                const QByteArray& terminalName)
    : m_messageReference(messageReference),
      m_messageId(messageId),
      m_messageDateTime(messageDateTime),
      m_voyageCode(voyageCode),
      m_vesselName(vesselName),
      m_imoNumber(imoNumber),
      m_carrierId(carrierId),
      m_portCode(portCode),
      m_terminalName(terminalName)
    {
    };


    /**
     * @return the messageReference
     */
    const QByteArray& messageReference() const {
        return m_messageReference;
    };

    /**
     * @return the messageId
     */
    const QByteArray& messageId() const {
        return m_messageId;
    };

    /**
     * @return the message date&time
     */
    const QDateTime& messageDateTime() const {
        return m_messageDateTime;
    }

    /**
     * @return the voyageCode
     */
    const QByteArray& voyageCode() const{
        return m_voyageCode;
    };

    /**
     * @return the vesselName
     */
    const QByteArray& vesselName() const {
        return m_vesselName;
    };

    /**
     * @return the imoNumber
     */
    const QByteArray& imoNumber() const {
        return m_imoNumber;
    };

    /**
     * @return the carrierId
     */
    const QByteArray& carrierId() const {
        return m_carrierId;
    };

    /**
     * @return the portOfDeparture
     */
    const QByteArray& portCode() const {
        return m_portCode;
    };

    /**
     * @return the nextPortOfCall
     */
    const QByteArray& terminalName() const {
        return m_terminalName;
    };

private:

    QByteArray m_messageReference;

    QByteArray m_messageId;

    QDateTime m_messageDateTime;

    QByteArray m_voyageCode;

    QByteArray m_vesselName;

    QByteArray m_imoNumber;

    QByteArray m_carrierId;

    QByteArray m_portCode;

    QByteArray m_terminalName;

};


/**
 * The result of parsing a COPRAR. Immutable.
 */

class CoprarParserResult {

public:

    /**
     * @param errorList
     */
    CoprarParserResult(const QList<EdiProblem>& errorList)
    : m_ediHeader(),
    m_coprarTransport(),
    m_containerList(),
    m_errorList(errorList)
    {
    };

    /**
     * @param ediHeader
     * @param errorList
     */
    CoprarParserResult(const EdiHeader& ediHeader,
                       const QList<EdiProblem>& errorList)
    : m_ediHeader(ediHeader),
      m_coprarTransport(),
      m_containerList(),
      m_errorList(errorList)
    {
    };

    /**
     * @param ediHeader
     * @param coprarTransport
     * @param containerList
     * @param errorList
     */
    CoprarParserResult(const EdiHeader& ediHeader,
                       CoprarHeader coprarTransport,
                       const QList<ange::containers::Container*>& containerList,
                       const QList<EdiProblem>& errorList)
    : m_ediHeader(ediHeader),
      m_coprarTransport(coprarTransport),
      m_containerList(containerList),
      m_errorList(errorList)
    {
    };

    /**
     * @return the ediHeader
     */
    EdiHeader ediHeader() const {
        return m_ediHeader;
    };

    /**
     * @return the coprarHeader
     */
    CoprarHeader coprarTransport() const {
        return m_coprarTransport;
    };

    /**
     * @return the containers
     */
    QList<ange::containers::Container*> containerList() const {
        return m_containerList;
    };

    /**
     * @return the parsing error list
     */
    QList<EdiProblem> errorList() const {
        return m_errorList;
    };

    /**
     * @return true if there are parsing errors
     */
    bool hasErrors() const {
        Q_FOREACH(const EdiProblem& error, m_errorList) {
            if (error.type() == EdiProblem::Error) {
                return true;
            }
        }
        return false;
    };

    /**
     * @return true if there are parsing warnings
     */
    bool hasWarnings() const {
        Q_FOREACH(const EdiProblem& error, m_errorList) {
            if (error.type() == EdiProblem::Warning) {
                return true;
            }
        }
        return false;
    };

private:

    EdiHeader m_ediHeader;

    CoprarHeader m_coprarTransport;

    QList<ange::containers::Container*> m_containerList;

    QList<EdiProblem> m_errorList;

};


class Coprar20Parser {

public:

    /**
     * Parses on COPRAR 2.0 message from the EDIFACT stream
     *
     * @param ediReader the reader is expected to be positioned at first UNH segment UNH+...+COPRAR:D:00B:UN:SMDG20'
     * @param unb the UNB segment common for all messages in the EDIFACT interchange
     * @param expectedLoadPort the expected load port that will be used as load port for all containers. Warnings will be given if it doesn't match
     * @return parsed COPRAR:D:00B:UN:SMDG20 edifact messages from EdifactSegmentReader
     *
     */
    static CoprarParserResult parse(QtEdith::EdifactSegmentReader* ediReader, const QtEdith::Segment& unb, const QString& expectedLoadPort);

    /**
     * Check UNH segment that it represents a Coprar 2.0 message
     *
     * @return true if UNH is Coprar 2.0 segment UNH+...+COPRAR:D:00B:UN:SMDG20'
     */
    static bool unhIsCoprar20(const QtEdith::Segment& unh);

private:

    /**
     * Parses EDIFACT interchange header from UNB segment
     *
     * @param unb the UNB segment common for all messages in the EDIFACT interchange
     * @return parsed EdiHeader
     *
     */
    EdiHeader parseUnb(const QtEdith::Segment& unb);

    /**
     * parses a list of Dim tags into a OOG structure
     * \param dimList segments to parse
     * \param equipmentnumber a equipment number for the container. Only used for error reporting
     */
    ange::containers::Oog parseDimList(QList<QtEdith::Segment> dimList, const ange::containers::EquipmentNumber& equipmentnumber);

    /**
     * tries to parse the first temperature out of a group7
     */
    double parseGroup7(QList<QtEdith::Group> group7list);

    /**
     * tries to parse the dangerous goods information out of group 8
     */
    QList<ange::containers::DangerousGoodsCode> parseGroup8(QList<QtEdith::Group> group8List);

    /**
     * Parses the HAN tags into handling code objects
     */
    QList<ange::containers::HandlingCode> parseHandlingList(QList<QtEdith::Segment> hanList );

    QList<EdiProblem> m_errorList;

    QString m_expectedLoadPort;

    CoprarParserResult parseMessage(QtEdith::EdifactSegmentReader* ediReader, const EdiHeader& ediheader);

    CoprarHeader parseUnhBgmDtmSg1Sg2Sg4(const QtEdith::Group& group0);

    QDateTime parseDtm(const QtEdith::Segment& dtm, int expectedTypeCode);

    void parseSG6(const QtEdith::Group& group6, QList< ange::containers::Container* >* containerList, const QString& coprarLoadPort);


    /**
     * Test access only
     */
    friend class CoprarDimTest;

    /**
     * Test access only
     */
    friend class CoprarGroup7Test;

    /**
     * Test access only
     */
    friend class CoprarGroup8Test;

    /**
     * Test access only
     */
    friend class CoprarHanTest;

};


