#ifndef BAPLIE20PARSER_H
#define BAPLIE20PARSER_H

#include "baplieparserresult.h"

class QIODevice;
namespace QtEdith {
class EdifactSegmentReader;
class Group;
class Segment;
}

/**
 * BAPLIE parser that will parse both SMDG15 (version 1.5) and SMDG20 (version 2.1.1) files.
 */
class BaplieParser {

public:

    /**
     * Read BAPLIE file from QIODevice and parse it.
     * Expects device to be open and ready to read.
     * Will filter out invalid chars from the stream (0x1A and 0xFF).
     * Expects the file to end at the end of the device.
     */
    static BaplieParserResult parse(QIODevice* device);

    /**
     * The given UNH segment if from a file this BAPLIE parser can parse
     */
    static bool acceptedUnh(const QtEdith::Segment& unh);

    /**
     * Parse the BAPLIE message from the segment stream, expects the first segment to be UNH and will stop when UNT is
     * reached. Should only be called once on a parser.
     */
    void parse(QtEdith::EdifactSegmentReader* ediReader);

    /**
     * The result from the call to parse()
     */
    BaplieParserResult result() const;

private:

    void parseGroup0(const QtEdith::Group& group0);
    void parseGroup1(const QtEdith::Group& group1);
    void parseGroup2(const QtEdith::Group& group2);
    void parseGroup3(const QtEdith::Group& group3, ange::containers::Container* container);
    void parseGroup4(const QtEdith::Group& group4, ange::containers::Container* container);

private:

    BaplieParserResult m_result;

};

#endif // BAPLIE20PARSER_H
