#include "baplieparser.h"

#include "filterwrongcharsiodevice.h"

#include <ange/containers/container.h>
#include <ange/containers/oog.h>
#include <ange/units/units.h>
#include <ange/vessel/bayrowtier.h>

#include <QtEdith/edifactsegmentreader.h>
#include <QtEdith/group.h>
#include <QtEdith/predefinedformats.h>
#include <QtEdith/segment.h>
#include <QtEdith/segmentgrouper.h>

#include <QByteArray>
#include <QString>

using namespace ange::containers;
using namespace ange::units;
using namespace ange::vessel;
using namespace QtEdith;

BaplieParserResult BaplieParser::parse(QIODevice* device) {
    BaplieParserResult result;

    FilterWrongCharsIODevice filter(device);
    EdifactSegmentReader ediReader(&filter);

    // Check UNB
    if (!ediReader.hasNext()) {
        result.addError("UNB segment missing");
        return result;
    }
    Segment unb = ediReader.next();
    if (unb.tag() != QByteArrayLiteral("UNB"))  {
        result.addError(QString("Wrong segment where UNB expected: %1").arg(QString(unb.print())));
        return result;
    }

    // Check UNH
    if (!ediReader.hasNext()) {
        result.addError("UNH segment missing");
        return result;
    }
    Segment unh = ediReader.peek();
    if (!BaplieParser::acceptedUnh(unh))  {
        result.addError(QString("UNH segment not of accepted form: %1").arg(QString(unh.print())));
        return result;
    }

    // Parse the message (UNH..UNT)
    BaplieParser parser;
    parser.parse(&ediReader);
    result = parser.result();
    if (result.hasErrors()) {
        return result;
    }

    // Check UNZ
    if (!ediReader.hasNext()) {
        result.addError("UNZ segment missing");
        return result;
    }
    Segment unz = ediReader.next();
    if (unz.tag() != QByteArrayLiteral("UNZ"))  {
        result.addError(QString("Wrong segment where UNZ expected: %1").arg(QString(unz.print())));
        return result;
    }

    // Check end of file
    if (ediReader.hasNext()) {
        result.addError("Data after UNZ segment");
        return result;
    }

    if (ediReader.errorCode()) {
        result.addError(QString("Error from EdifactSegmentReader: %1").arg(ediReader.errorCode()));
        return result;
    }

    return result;
}


// Correct values:
//   UNH+...+BAPLIE:1:911:UN:SMDG15'
//   UNH+...+BAPLIE:D:95B:UN:SMDG20'
// Incorrect values we accept as they are in common use:
//   UNH+...+BAPLIE:1:94B:UN:SMDG20'
//   UNH+...+BAPLIE:1:95B:UN:SMDG20'
//   UNH+...+BAPLIE:D:95B:KE:SMDG20'
bool BaplieParser::acceptedUnh(const Segment& unh) {
    if (unh.tag() != QByteArrayLiteral("UNH")) {
        return false;
    }
    if (unh.data(2, 1).asString() != QByteArrayLiteral("BAPLIE")) {
        return false;
    }
    // Ignore components 2, 3 and 4
    if (unh.data(2, 5).asString() != QByteArrayLiteral("SMDG15")
            && unh.data(2, 5).asString() != QByteArrayLiteral("SMDG20")
            && unh.data(2, 5).asString() != QByteArrayLiteral("SMDG21")
            && unh.data(2, 5).asString() != QByteArrayLiteral("SMDG22")
	    ) {
        return false;
    }
    return true;
}

static QDateTime parseDtm(const Segment& dtm, BaplieParserResult& result) {
    Q_ASSERT(dtm.tag() == "DTM");
    if (dtm.data(1, 1).asInt() != 137) {
        result.addWarning(QString("Expected DTM type to be 137: %1").arg(QString(dtm.print())));
        return QDateTime();
    } else {
        QString dateTimeString = dtm.data(1, 2).asString();
        int formatCode = dtm.data(1, 3).asInt();
        switch (formatCode) {
            case 101:
                if (dateTimeString.size() == 6) {
                    return QDateTime::fromString(dateTimeString, "yyMMdd").addYears(100);
                } else if (dateTimeString.size() == 8 && dateTimeString.startsWith("20")) {
                    return QDateTime::fromString(dateTimeString, "yyyyMMdd");
                } else {
                    result.addWarning(QString("DTM segment with bad format ignored: %1").arg(QString(dtm.print())));
                }
                break;
            case 201:
                if (dateTimeString.size() == 10 && dateTimeString.startsWith("20")) {
                    // Will give bad result for 2020-01-01 formatted in other format
                    return QDateTime::fromString(dateTimeString, "yyyyMMddHH");
                } else if (dateTimeString.size() == 10 && !dateTimeString.startsWith("20")) {
                    // Will give bad result for 1999-12-31 formatted in other format
                    return QDateTime::fromString(dateTimeString, "yyMMddHHmm").addYears(100);
                } else if (dateTimeString.size() == 12 && dateTimeString.startsWith("20")) {
                    return QDateTime::fromString(dateTimeString, "yyyyMMddHHmm");
                } else {
                    result.addWarning(QString("DTM segment with bad format ignored: %1").arg(QString(dtm.print())));
                }
                break;
            case 203:
                if (dateTimeString.size() == 12 && dateTimeString.startsWith("20")) {
                    return QDateTime::fromString(dateTimeString, "yyyyMMddHHmm");
                } else {
                    result.addWarning(QString("DTM segment with bad format ignored: %1").arg(QString(dtm.print())));
                }
                break;
            // case 301: format "yyMMddHHmmZZZ", where ZZZ is time zone
            // Ignored as the format is not in use and is not easily supported as the time zone format is unknown
            default:
                result.addWarning(QString("Unknown DTM format: %1").arg(QString(dtm.print())));
                return QDateTime();
        }
    }
    Q_ASSERT(false); //should be impossible to get here
    return QDateTime();
}

void BaplieParser::parse(EdifactSegmentReader* ediReader) {
    m_result = BaplieParserResult();
    // We can use the same parser for SMDG15 and SMDG20 files as the differences between the two formats are few
    // and the differences are not incompatible
    SegmentGrouper grouper(PredefinedFormats::createBaplie95BTransitionTable());
    const Group& group0 = grouper.group(ediReader);
    if (grouper.errorCode() != 0) {
        m_result.addError(QStringLiteral("Bad structure in BAPLIE file, error code %1").arg(grouper.errorCode()));
    }
    Q_ASSERT(!group0.isNull());
    parseGroup0(group0);
}

void BaplieParser::parseGroup0(const Group& group0) {
    // UNH - M1: Message header (ignore)
    // BGM - M1 (ignore)

    // DTM - M1: Date/time of compiling the message
    const QList<Segment>& dtms = group0.segmentList("DTM");
    if (dtms.isEmpty()) {
        m_result.addError("No DTM segment found in group 0");
    } else {
        if (dtms.size() > 1) {
            m_result.addError("Multiple DTM segments found in group 0, expected only one, took data from the first");
        }
        const Segment& dtm = dtms.first();
        m_result.messageTime = parseDtm(dtm, m_result);
    }

    // RFF - C1 (ignore)
    // NAD - C9 (ignore)

    // Group 1 - M1: TDT, LOC, DTM, RFF, FTX
    const QList<Group>& group1s = group0.groupList(1);
    if (group1s.isEmpty()) {
        m_result.addError("No group 1 found in group 0");
    } else {
        if (group1s.size() > 1) {
            m_result.addError("Multiple group 1 found in group 0, expected only one, took data from the first");
        }
        const Group& group1 = group1s.first();
        parseGroup1(group1);
    }

    // Group 2 - C9999: LOC, GID, GDS, FTX, MEA, DIM, TMP, RNG, LOC, RFF, grp3, grp4
    Q_FOREACH (const Group& group2, group0.groupList(2)) {
        parseGroup2(group2);
    }
}

void BaplieParser::parseGroup1(const Group& group1) {
    // TDT - M1: Details of transport (voyage identification)
    const QList<Segment>& tdts = group1.segmentList("TDT");
    if (tdts.isEmpty()) {
        Q_ASSERT(false); // Will not happen as TDT is trigger segment for group 1
        m_result.addError("No TDT segment found in group 1");
    } else {
        if (tdts.size() > 1) {
            m_result.addError("Multiple TDT segments found in group 1, expected only one, took data from the first");
        }
        const Segment& tdt = tdts.first();
        m_result.voyageCode = tdt.data(2, 1).asString();
        if (tdt.data(5, 1).isPresent()) { // SMDG20
            m_result.carrierCode = tdt.data(5, 1).asString();
        }
        if (tdt.data(6, 1).isPresent()) { // SMDG15
            m_result.carrierCode = tdt.data(6, 1).asString();
        }
        // Composite position 4 is used in SMDG15, position 8 in SMDG20
        Q_FOREACH (int compositePosition, QVector<int>() << 4 << 8) {
            if (tdt.data(compositePosition, 1).isPresent()) { // SMDG15
                QString identificationType = tdt.data(compositePosition, 2).asString();
                if (identificationType == "146" || identificationType == "ZZZ") {
                    // BAPLIE 2.1.1 says:
                    //   146: Means of Transport Identification (Lloyd's Code or IMO number)
                    //   ZZZ: Mutually defined or IMO number
                    // ZZZ has been used by angelstow when writing BAPLIE files
                    m_result.imoNumber = tdt.data(compositePosition, 1).asString();
                } else {
                    // BAPLIE 2.1.1 says:
                    //   103: Call Sign Directory
                    // Call sign chosen as default case that is what is used generally in the shipping industry
                    m_result.callSign = tdt.data(compositePosition, 1).asString();
                }
            }
            if (tdt.data(compositePosition, 4).isPresent()) {
                m_result.vesselName = tdt.data(compositePosition, 4).asString();
            }
        }
    }

    // LOC - M9: Place/location identification (ports)
    const QList<Segment>& locs = group1.segmentList("LOC");
    if (locs.isEmpty()) {
        m_result.addError("No LOC segment found in group 1");
    }
    Q_FOREACH (const Segment& loc, locs) {
        if (loc.data(1, 1).asInt() == 5) {
            m_result.departurePortCode = loc.data(2, 1).asString();
        }
        if (loc.data(1, 1).asInt() == 61) {
            m_result.nextPortCode = loc.data(2, 1).asString();
        }
    }

    // DTM - M99 (ignore)
    // RFF - C1 (ignore)
    // FTX - C1 (ignore)
}

static double fahrenheitToCelsius(double fah) {
    return (fah - 32) * 5 / 9;
}

void BaplieParser::parseGroup2(const Group& group2) {
    QScopedPointer<Container> container(new Container(QString(), 0 * kilogram, IsoCode("42ZZ")));
    BayRowTier position;

    // LOC/1 - M1: Place/Location Qualifier: Code "147" (Stowage Cell)
    const QList<Segment>& loc1s = group2.segmentList("LOC", 1);
    if (loc1s.isEmpty()) {
        m_result.addWarning("No LOC/1 segment found in group 2");
    } else {
        if (loc1s.size() > 1) {
            m_result.addWarning("Multiple LOC/1 segments found in group 2, expected only one, took data from the first");
        }
        const Segment& loc1 = loc1s.first();
        if (loc1.data(1, 1).asInt() != 147) {
            m_result.addWarning(QString("LOC/1 segment has type other than 147: %1").arg(QString(loc1.print())));
        } else {
            QString rawLocation = loc1.data(2, 1).asString();
            bool okBay = false;
            bool okRow = false;
            bool okTier = false;
            int bay;
            int row;
            int tier;
            if (rawLocation.length() == 7) {
                bay = rawLocation.midRef(0, 3).toInt(&okBay);
                row = rawLocation.midRef(3, 2).toInt(&okRow);
                tier = rawLocation.midRef(5, 2).toInt(&okTier);
            } else if (rawLocation.length() == 6) {
                bay = rawLocation.midRef(0, 2).toInt(&okBay);
                row = rawLocation.midRef(2, 2).toInt(&okRow);
                tier = rawLocation.midRef(4, 2).toInt(&okTier);
            }
            if (okBay && okRow && okTier) {
                if (bay == 99 && row == 99 && tier == 99) {
                    position = BayRowTier(); // position 99,99,99 is used containers in unknown places
                } else {
                    position = BayRowTier(bay, row, tier);
                }
            } else {
                m_result.addWarning(QString("Problem when parsing the position out of: %1").arg(QString(loc1.print())));
            }
        }
    }

    // GID - C1: Goods item details (ignore)
    // GDS - C9: Nature of cargo (ignore)

    // FTX - C9: Free text (handling)
    Q_FOREACH (const Segment& ftx, group2.segmentList("FTX", 2)) {
        if (ftx.data(1, 1).asString() == "FTX" && ftx.data(4, 1).isPresent()) {
            container->addHandlingCode(HandlingCode(ftx.data(4, 1).asString()));
        }
        // Known other types to ignore: AAA, AAI, CLR, SIN, ZZZ
        // also ignores all unknown types
    }

    // MEA - M9: Measurements (weight)
    const QList<Segment>& meas = group2.segmentList("MEA");
    if (meas.isEmpty()) {
        m_result.addWarning("No MEA segment found in group 2");
    } else {
        if (meas.size() > 1) {
            m_result.addWarning("Multiple MEA segments found in group 2, expected only one, took data from the first");
        }
        const Segment& mea = meas.first();
        if (mea.data(1, 1).asString() != "WT" && mea.data(1, 1).asString() != "VGM") {
            m_result.addWarning(QString("MEA segment of type different from WT ignored: %1").arg(QString(mea.print())));
        } else if (mea.data(3, 1).asString() != "KGM") {
            m_result.addWarning(QString("MEA segment unit different from KGM ignored: %1").arg(QString(mea.print())));
        } else {
            bool ok;
            double weightInKilogram = mea.data(3, 2).asDouble(&ok);
            if (!ok) {
                m_result.addWarning(QString("MEA segment with bad weight ignored: %1").arg(QString(mea.print())));
            } else {
                container->setWeight(weightInKilogram * kilogram);
                container->setWeightIsVerified(mea.data(1, 1).asString() == "VGM");
            }
        }
    }

    // DIM - C9: Dimensions (OOG size)
    Oog oog;
    Q_FOREACH (const Segment& dim, group2.segmentList("DIM")) {
        QString unit = dim.data(2, 1).asString();
        if (!(unit == "CM" || unit == "CMT")) {
            m_result.addWarning(QString("DIM segment with bad unit ignored: %1").arg(QString(dim.print())));
            continue;
        }
        switch (dim.data(1, 1).asInt()) {
            case 1: // ignore silently, usually combined with types 5,6,7,8,9 that we read
                break;
            case 5: {
                bool ok;
                double lengthInCm = dim.data(2, 2).asDouble(&ok);
                if (!ok) {
                    m_result.addWarning(QString("DIM segment with bad length ignored: %1").arg(QString(dim.print())));
                } else {
                    oog.setFront(lengthInCm * centimeter);
                }
                break;
            }
            case 6: {
                bool ok;
                double lengthInCm = dim.data(2, 2).asDouble(&ok);
                if (!ok) {
                    m_result.addWarning(QString("DIM segment with bad length ignored: %1").arg(QString(dim.print())));
                } else {
                    oog.setBack(lengthInCm * centimeter);
                }
                break;
            }
            case 7: {
                bool ok;
                double lengthInCm = dim.data(2, 3).asDouble(&ok);
                if (!ok) {
                    m_result.addWarning(QString("DIM segment with bad length ignored: %1").arg(QString(dim.print())));
                } else {
                    oog.setRight(lengthInCm * centimeter);
                }
                break;
            }
            case 8: {
                bool ok;
                double lengthInCm = dim.data(2, 3).asDouble(&ok);
                if (!ok) {
                    m_result.addWarning(QString("DIM segment with bad length ignored: %1").arg(QString(dim.print())));
                } else {
                    oog.setLeft(lengthInCm * centimeter);
                }
                break;
            }
            case 9: {
                bool ok;
                double lengthInCm = dim.data(2, 4).asDouble(&ok);
                if (!ok) {
                    m_result.addWarning(QString("DIM segment with bad length ignored: %1").arg(QString(dim.print())));
                } else {
                    oog.setTop(lengthInCm * centimeter);
                }
                break;
            }
            default:
                m_result.addWarning(QString("DIM segment with unknown type ignored: %1").arg(QString(dim.print())));
                break;
        }
    }
    container->setOog(oog);

    // TMP - C1: Temperature (reefer temperature)
    const QList<Segment>& tmps = group2.segmentList("TMP");
    if (!tmps.isEmpty()) {
        if (meas.size() > 1) {
            m_result.addWarning("Multiple TMP segments found in group 2, expected only one, took data from the first");
        }
        const Segment& tmp = tmps.first();
        if (tmp.data(1, 1).asInt() != 2) {  // 2 = Transport Temperature
            m_result.addWarning(QString("TMP segment of type different from 2 ignored: %1").arg(QString(tmp.print())));
        } else {
            bool ok;
            double temperatureInUnit = QString(tmp.data(2, 1).asString(&ok)).trimmed().toDouble();
            if (!ok) {
                m_result.addWarning(QString("TMP segment bad temperature value ignored: %1").arg(QString(tmp.print())));
            } else {
                QString unit = tmp.data(2, 2).asString();
                if (unit == "CEL") {
                    container->setTemperature(temperatureInUnit);
                    container->setLive(Container::Live);
                } else if (unit == "FAH") {
                    container->setTemperature(fahrenheitToCelsius(temperatureInUnit));
                    container->setLive(Container::Live);
                } else {
                    m_result.addWarning(QString("TMP segment bad temperature unit ignored: %1").arg(QString(tmp.print())));
                }
            }
        }
    }

    // RNG - C1: Range details (reefer temperature interval) (ignore)

    // LOC/2 - C9: Place/location identification (ports)
    Q_FOREACH (const Segment& loc2, group2.segmentList("LOC", 2)) {
        switch (loc2.data(1, 1).asInt()) {
            case 6: // SMDG15
            case 9: // SMDG20
                container->setLoadPort(loc2.data(2, 1).asString());
                break;
            case 12: // SMDG15
            case 11: // SMDG20
                container->setDischargePort(loc2.data(2, 1).asString());
                break;
            case 83: // SMDG15 and SMDG20
                container->setPlaceOfDelivery(loc2.data(2, 1).asString());
                break;
        }
    }

    // RFF - M9: REFERENCE (booking number)
    const QList<Segment>& rffs = group2.segmentList("RFF");
    if (rffs.isEmpty()) {
        m_result.addWarning("No RFF segment found in group 2");
    }
    Q_FOREACH (const Segment& rff, rffs) {
        if (rff.data(1, 1).asString() == "BN") {
            container->setBookingNumber(rff.data(2, 1).asString());
        } else {
            // Silently ignore other types, e.g. "BM:1" is ok
            // m_result.addWarning(QString("Expected RFF type to be BN: %1").arg(QString(rff.print())));
        }
    }

    // Group 3 - C9: EQD, EQA, NAD
    const QList<Group>& group3s = group2.groupList(3);
    if (group3s.isEmpty()) {
        // Silently ignore missing group 3, this is used for reserved OOG slots and perhaps break bulk
        return; // Skip, this is not a container
    } else {
        if (group3s.size() > 1) {
            m_result.addWarning("Multiple group 3 found in group 2, expected only one, took data from the first");
        }
        const Group& group3 = group3s.first();
        parseGroup3(group3, container.data());
    }

    // Group 4 - C999: DGS, FTX
    Q_FOREACH (const Group& group4, group2.groupList(4)) {
        parseGroup4(group4, container.data());
    }

    m_result.addContainer(container.take(), position);
}

void BaplieParser::parseGroup3(const Group& group3, Container* container) {
    // EQD - M1: Equipment details
    const QList<Segment>& eqds = group3.segmentList("EQD");
    if (eqds.isEmpty()) {
        m_result.addWarning("No EQD segment found in group 3");
    } else {
        if (eqds.size() > 1) {
            m_result.addWarning("Multiple EQD segments found in group 3, expected only one, took data from the first");
        }
        const Segment& eqd = eqds.first();
        if (eqd.data(1, 1).asString() != "CN") {
            m_result.addWarning(QString("Expected EQD type to be CN: %1").arg(QString(eqd.print())));
        } else {
            container->setEquipmentNumber(QString(eqd.data(2, 1).asString()).remove(" "));
            QString isoCodeString = QString(eqd.data(3, 1).asString());
            if (isoCodeString.size() != 4) {
                m_result.addWarning(QString("Could not parse ISO code: %1").arg(QString(eqd.print())));
            } else {
                container->setIsoCode(IsoCode(isoCodeString));
            }
            switch (eqd.data(6, 1).asInt()) {
                case 4:
                    container->setEmpty(true);
                    break;
                case 5:
                    container->setEmpty(false);
                    break;
                default:
                    m_result.addWarning(QString("Could not parse empty: %1").arg(QString(eqd.print())));
                    break;
            }
        }
    }

    // EQA - C9: Equipment attached (ignore)

    // NAD - C1: Carrier (carrier code)
    const QList<Segment>& nads = group3.segmentList("NAD");
    if (!nads.isEmpty()) {
        if (nads.size() > 1) {
            m_result.addWarning("Multiple NAD segments found in group 3, expected only one, took data from the first");
        }
        const Segment& nad = nads.first();
        if (nad.data(1, 1).asString() != "CA") {
            m_result.addWarning(QString("Expected NAD type to be CA: %1").arg(QString(nad.print())));
        } else {
            container->setCarrierCode(nad.data(2, 1).asString());
        }
    }
}

void BaplieParser::parseGroup4(const Group& group4, Container* container) {
    // DGS - M1: Dangerous goods
    const QList<Segment>& dgss = group4.segmentList("DGS");
    if (dgss.isEmpty()) {
        m_result.addWarning("No DGS segment found in group 4");
    } else {
        if (dgss.size() > 1) {
            m_result.addWarning("Multiple DGS segments found in group 4, expected only one, took data from the first");
        }
        const Segment& dgs = dgss.first();
        QString undgCodeString = dgs.data(3, 1).asString();
        container->addDangerousGoodsCode(DangerousGoodsCode(undgCodeString));
    }

    // FTX - C1: Free text (ignore)
}

BaplieParserResult BaplieParser::result() const {
    return m_result;
}
