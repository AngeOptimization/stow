#include "baplieparserresult.h"

#include <ange/containers/container.h>

using namespace ange::containers;
using namespace ange::vessel;

void BaplieParserResult::addContainer(const Container* container, BayRowTier position) {
    m_containerList << ContainerPosition(container, position);
}

QList<BaplieParserResult::ContainerPosition> BaplieParserResult::containerList() const {
    return m_containerList;
}

int BaplieParserResult::containersParsed() const {
    return m_containerList.size();
}

void BaplieParserResult::addError(const QString& message) {
    problemList << EdiProblem(EdiProblem::Error, message);
}

void BaplieParserResult::addWarning(const QString& message) {
    problemList << EdiProblem(EdiProblem::Warning, message);
}

bool BaplieParserResult::hasErrors() const {
    Q_FOREACH (const EdiProblem& error, problemList) {
        if (error.type() == EdiProblem::Error) {
            return true;
        }
    }
    return false;
}

BaplieParserResult::ContainerPosition::ContainerPosition(const Container* container, BayRowTier position)
  : container(container), position(position)
{
    // Empty
}
