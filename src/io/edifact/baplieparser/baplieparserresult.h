#ifndef BAPLIEPARSERRESULT_H
#define BAPLIEPARSERRESULT_H

#include "ediproblem.h"

#include <ange/vessel/bayrowtier.h>

#include <QtEdith/group.h>

#include <QDateTime>
#include <QList>
#include <QSharedPointer>
#include <QString>

namespace ange {
namespace containers {
class Container;
}
}

/**
 * The result of parsing a BAPLIE file
 */
class BaplieParserResult {

public:

    struct ContainerPosition {
        ContainerPosition(const ange::containers::Container* container, ange::vessel::BayRowTier position);
        QSharedPointer<const ange::containers::Container> container;
        ange::vessel::BayRowTier position;
    };

public:

    /**
     * Add container, takes ownership of it
     */
    void addContainer(const ange::containers::Container* container, ange::vessel::BayRowTier position);

    /**
     * List of parsed containers with their positions
     */
    QList<ContainerPosition> containerList() const;

    /**
     * Number of containers succesfully parsed
     */
    int containersParsed() const;

    /**
     * Add error to problemList
     */
    void addError(const QString& message);

    /**
     * Add warning to problemList
     */
    void addWarning(const QString& message);

    /**
     * True if there were errors when parsing the BAPLIE file
     */
    bool hasErrors() const;

public:

    QList<EdiProblem> problemList;

    QDateTime messageTime;

    /**
     * Port code of departure port
     */
    QString departurePortCode;

    /**
     * Port code of next port
     */
    QString nextPortCode;

    /**
     * Voyage code
     */
    QString voyageCode;

    /**
     * Carrier code as assigned by BIC
     */
    QString carrierCode;

    /**
     * Call sign of vessel
     */
    QString callSign;

    /**
     * IMO number of vessel
     */
    QString imoNumber;

    /**
     * Name of vessel
     */
    QString vesselName;

private:

    QList<ContainerPosition> m_containerList;

};

#endif // BAPLIEPARSERRESULT_H
