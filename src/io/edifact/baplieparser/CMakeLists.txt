add_library(baplieparser
    baplieparser.cpp
    baplieparserresult.cpp
)
target_link_libraries(baplieparser
    edifactutils
    ioutils
    Ange::Containers
    Ange::Vessel
    QtEdith
    Qt5::Core
)

add_subdirectory(test)
