#include <QtTest>
#include <QtEdith/edifactsegmentreader.h>
#include <KCompressionDevice>
#include "baplieparser.h"
#include "filterwrongcharsiodevice.h"

using namespace QtEdith;

class ReadDifferentLOC147lengths : public QObject {
    Q_OBJECT
private Q_SLOTS:
    void parseAndCompare();
    void parseAndCompare_data();
};

QTEST_GUILESS_MAIN(ReadDifferentLOC147lengths);

void ReadDifferentLOC147lengths::parseAndCompare() {
    QFETCH(QString, fileName);
    QFETCH(int, bay);
    QFETCH(int, row);
    QFETCH(int, tier);
    KCompressionDevice file(fileName, KCompressionDevice::GZip);
    QVERIFY(file.open(QIODevice::ReadOnly));
    BaplieParserResult result = BaplieParser::parse(&file);
    QVERIFY(!result.hasErrors());
    QCOMPARE(result.containerList()[0].position.bay(), bay);
    QCOMPARE(result.containerList()[0].position.row(), row);
    QCOMPARE(result.containerList()[0].position.tier(), tier);
}

static void newRow(const char* fileName, int bay, int row, int tier) {
    QTest::newRow(fileName) << QFINDTESTDATA(QString("data/") + fileName + ".gz") << bay << row << tier;
}

void ReadDifferentLOC147lengths::parseAndCompare_data() {
    QTest::addColumn<QString>("fileName");
    QTest::addColumn<int>("bay");
    QTest::addColumn<int>("row");
    QTest::addColumn<int>("tier");

    newRow("MAERSK-BAPLIE_OXAW2_SANTA_TERESA_USORF_2019020307052514.edi", 6, 6, 14);
    newRow("MSC-BAPLIE_CQYJ_CONTI_CORTESIA_INMUN_2019020311250810.edi",65, 10, 86);
}

#include "readdifferentloc147lengthstest.moc"
