#include <QtTest>
#include <QtEdith/edifactsegmentreader.h>
#include <KCompressionDevice>
#include "baplieparser.h"
#include "filterwrongcharsiodevice.h"

using namespace QtEdith;

class BaplieParserTest : public QObject {
    Q_OBJECT
private Q_SLOTS:
    void parseAndCount();
    void parseAndCount_data();
};

QTEST_GUILESS_MAIN(BaplieParserTest);

void BaplieParserTest::parseAndCount() {
    QFETCH(QString, fileName);
    QFETCH(int, containers);
    QFETCH(int, problems);

    // Open file
    KCompressionDevice file(fileName, KCompressionDevice::GZip);
    QVERIFY(file.open(QIODevice::ReadOnly));

    // Parse
    BaplieParserResult result = BaplieParser::parse(&file);

    // Validate the result
    if (result.hasErrors() || result.problemList.size() != problems) {
        Q_FOREACH (const EdiProblem& problem, result.problemList) {
            qDebug() << problem.typeString() << problem.message();
        }
    }
    QVERIFY(!result.hasErrors());
    QCOMPARE(result.problemList.size(), problems);
    QCOMPARE(result.containersParsed(), containers);
}

static void newRow(const char* fileName, int containers, int problems) {
    QTest::newRow(fileName) << QFINDTESTDATA(QString("data/") + fileName + ".gz") << containers << problems;
}

static void newRow(const char* fileName, int containers) {
    newRow(fileName, containers, 0);
}

void BaplieParserTest::parseAndCount_data() {
    QTest::addColumn<QString>("fileName");
    QTest::addColumn<int>("containers");
    QTest::addColumn<int>("problems");

    // SMDG15 BAPLIE files:
    newRow("10_dep.edi", 1303);
    newRow("ArrYOK.edi", 1524);
//     newRow("BAPLIE-AMC1-E-AMC1_JAZA_1304E_.KLF.edi", 4143);
//     newRow("BAPLIE-AMC1-E-AMC1_SAFA_1332E_PSE.edi", 2841);
//     newRow("BAPLIE-AMC1-E-CAME1240E-KLF.edi", 4813);

    // Contains break bulk that has group 2 without "EQD+CN+..." that should be ignored
    newRow("BAPLIE-AUC1-E-CBNE0092E-SHYDEP.edi", 1605);
    newRow("BAPLIE-AUC1-E-XWUH0242E-SHYDEP.edi", 2063);
    newRow("BAPLIE-GEM--0020N_AJMN_DEP_KLF.edi", 1638);

    // Has invalid BGM segment
    newRow("BAPLIE-GEM--RAMD_0051S_DEP_SOH.edi", 1560);

    // Has containers with missing ISO codes
    // TODO Improve parser with a length, height and reefer guesser
    newRow("BAPLIE-GEM--XGNS_0357S_DEP_ALI.edi", 1675, 16);

    // Has garbage (^Z) at end of file
    newRow("BAPLIE-JAS1--HUZM_1410W_DEP_HKT2.edi", 297);
    newRow("BAPLIE-WAF2-E-AEEM_0005E_DEP_LFW.edi", 867);
//     newRow("CNNGB_01111_E_B_2.edi", 2613);
    newRow("CNSHA_01111_W_B_1.edi", 1239);

    // Has bad DIM segments: DIM+9+CM:5:6'
    newRow("CPUS_RTM_LOC147_ZZZ.edi", 4655, 4);
    newRow("HANSA_1330W_ALG_BAPLIE_corrected.EDI", 343);
//     newRow("KRPUS_01111_E_B_1.edi", 2931);
    newRow("Lego_Maersk_Small-AAAAA-0001.BAPLIE15.edi", 66);
    newRow("Lego_Maersk_Small-DDDDD-0001.BAPLIE15.edi", 0);
    newRow("MLKS_1405E_XMN_PRE.edi", 1158);
//     newRow("MXZLO_01111_W_B_0.edi", 3938);
    newRow("S703BRV1.0601_baplie.txt", 1816);
    newRow("TWKEL_01111_W_B_1.edi", 1120);
    newRow("baplie-import-test.edi", 12);
//     newRow("finalkobbaplie.edi", 3512);
//     newRow("fixed-baplie-1-smdg15.edi", 3806);
//     newRow("job_43002_input_Release_data.txt", 3624);
//     newRow("job_43005_input_Release_data.txt", 5609);
    newRow("release-13449.txt", 1873);
    newRow("release-20469.txt", 1597);
//     newRow("release-21472.txt", 2171);

    // SMDG20 BAPLIE files
//     newRow("BAPLIE-AAC1-S-AAC1_JUBL_1403_SIN.edi", 2081);
    newRow("BAPLIE-AEC2-E-003E-CMCC-LEH.edi", 1387);
    newRow("BAPLIE-AEC2-E-013E_CMAV-JEA.edi", 1630);
    newRow("BAPLIE-AEC2-E-999E_CMMT_-_ZEE.edi", 1234);
    newRow("BAPLIE-AEC2-E-FLA15EELEH_UAS.edi", 1485);
    newRow("BAPLIE-AEC2-W-1000W_CMMT-PKG.edi", 931);
//     newRow("BAPLIE-AEC8-E-0020E_CSST-JED.edi", 7463);
    newRow("BAPLIE-AEC8-E-728E_CMNV-_FRLEH.edi", 1119);
//     newRow("BAPLIE-AEC8-E-UPDATED2_1334E_RIFA_JEDDEP.edi", 7587);
    newRow("BAPLIE-AEC8-W-727W_CMNV-_MYPKG.edi", 1579);
//     newRow("BAPLIE-AMC1-E-AMC1_JAZA_1315E_KLF.edi", 3955);
    newRow("BAPLIE-GEM--0053N_SHUK_DEP_ALI.edi", 1372);

    // Contains multiple RFF segments in group 2
    newRow("BAPLIE-GEM--KTRN_0049E_DEP_HZA.edi", 1002);
    newRow("BAPLIE-GEM--XGNS_0359EDEP_KHI.edi", 1573);
    newRow("BAPLIE-GEM--XGNS_358S_DEP_SOH.edi", 1820);
    newRow("BAPLIE-WAF1-E-HNST_0027__DEP_LOS.edi", 1837);

    // Has group 2 without group 3 for 5 reserved OOG slots
    // Has bad segment: DIM+8+CMT::58.0'
    newRow("BAPLIE-WAF1-W-HNLM_0014W_DEP_ALG.edi", 1405);
    newRow("Lego_Maersk_Small-AAAAA-0001.BAPLIE.edi", 66);

    //SMDG22
    newRow("FG30851WLKCMBange.edi", 1264);
    newRow("MAERSK-BAPLIE_OXAW2_SANTA_TERESA_USORF_2019020307052514.edi", 629);
    newRow("MSC-BAPLIE_CQYJ_CONTI_CORTESIA_INMUN_2019020311250810.edi",274);
}

#include "baplieparsertest.moc"
