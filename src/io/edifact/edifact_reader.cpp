#include <fstream>
#include <iostream>
#include <ange/edifact/edifact_parser.h>
#include <stdlib.h>
#include <QFile>
#include <QDir>

#include "coprar20parser.h"

using namespace QtEdith;

Q_NORETURN void usage(char* name) {
        std::cerr << "Usage: " << name << " /path/to/edifact" << "ExpectedLoadPort uncode (for coprar files)" << std::endl;
        exit(1);
}

int main(int argc, char** argv) {
    if (argc!=2 && argc != 3) {
        usage(argv[0]);
    }

    if (!QFile::exists(argv[1])){
        std::cerr << "EDIFACT file '" << argv[1] << "' does not exist" << std::endl;
        exit(1);
    }

    QFile f(argv[1]);
    if (!f.open(QIODevice::ReadOnly)) {
        std::cerr << "EDIFACT file '" <<  argv[1] << "' can not be opened for reading" << std::endl;
        exit(1);
    }

    EdifactSegmentReader ediReader(&f);

    Segment unb = ediReader.next();
    if(unb.tag() != QByteArray("UNB")) {
        std::cerr << "EDIFACT file '" << argv[1] << "' Expected UNB segment" << std::endl;
    }

    Segment unh = ediReader.peek();

    if (Coprar20Parser::unhIsCoprar20(unh)) {
        if(argc != 3) {
            usage(argv[0]);
        }
        CoprarParserResult result = Coprar20Parser::parse(&ediReader, unb, argv[2]);
        std::cerr << "EDIFACT Coprar20 file '" << argv[1] << "' parsed with QtEdit" << std::endl;
        std::cerr << "parse errors: " << (result.hasErrors() ? "yes" : "no") << std::endl;
        std::cerr << "parse warnings: " << (result.hasWarnings() ? "yes" : "no") << std::endl;
        std::cerr << std::endl;
        std::cerr << "header: " << result.ediHeader().senderId().constData() << " -> " << result.ediHeader().recipientId().constData()
                << " @ " <<  result.ediHeader().timeStamp().toString().toUtf8().constData() << std::endl;
        std::cerr << "message: " << result.coprarTransport().messageReference().constData()
                << " " << result.coprarTransport().messageId().constData()
                << " @ " << result.coprarTransport().messageDateTime().toString().toUtf8().constData() << std::endl;
        std::cerr << "transport: " << result.coprarTransport().carrierId().constData()
                << " " << result.coprarTransport().voyageCode().constData()
                << " " << result.coprarTransport().imoNumber().constData()
                << " '" << result.coprarTransport().vesselName().constData() << "'" << std::endl;
        std::cerr << "port: " << result.coprarTransport().portCode().constData()
                << " '" << result.coprarTransport().terminalName().constData() << "'" << std::endl;
        std::cerr << "containers: " << result.containerList().size() << std::endl;
        std::cerr << std::endl;
        Q_FOREACH(const EdiProblem& error, result.errorList()) {
            std::cerr <<  error.typeString().toUtf8().constData() << " " << error.message().toUtf8().constData() << std::endl;
        }

        exit(0);
    } else {
        f.reset();
        ange::edifact::edifact_parser_t parser;
        parser.parse(&f);
        std::cerr << "EDIFACT file '" << argv[1] << "' parsed with medici" << std::endl;
        exit(0);
    }
}
