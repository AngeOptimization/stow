#include "filterwrongcharsiodevice.h"

FilterWrongCharsIODevice::FilterWrongCharsIODevice(QIODevice* parentDevice)
  : QIODevice(parentDevice), m_parentDevice(parentDevice)
{
    Q_ASSERT(parentDevice);
    setOpenMode(parentDevice->openMode());
}

FilterWrongCharsIODevice::~FilterWrongCharsIODevice() {
    // Empty
}

// Delegators that are implemented:

bool FilterWrongCharsIODevice::open(QIODevice::OpenMode mode) {
    super::open(mode);
    return m_parentDevice->open(mode);
}

bool FilterWrongCharsIODevice::isSequential() const {
    return false;  // Do not allow random access
}

qint64 FilterWrongCharsIODevice::readData(char* data, qint64 maxlen) {
    int charsRead = m_parentDevice->read(data, maxlen);
    int removed = 0;
    for (char* d = data, *end = data + charsRead; d < end; ++d) {
        if (*d == '\x1A' || *d == '\xFF') {
            ++removed;
        } else {
            if (removed) {
                *(d - removed) = *d;
            }
        }
    }
    return charsRead - removed;
}

// Delegators that are NOT implemented, they all have stub code of Q_ASSERT(false) and a simple forward to the parent

bool FilterWrongCharsIODevice::atEnd() const {
    Q_ASSERT(false);
    return m_parentDevice->atEnd();
}

qint64 FilterWrongCharsIODevice::bytesAvailable() const {
    Q_ASSERT(false);
    return m_parentDevice->bytesAvailable();
}

qint64 FilterWrongCharsIODevice::bytesToWrite() const {
    Q_ASSERT(false);
    return m_parentDevice->bytesToWrite();
}

bool FilterWrongCharsIODevice::canReadLine() const {
    Q_ASSERT(false);
    return m_parentDevice->canReadLine();
}

void FilterWrongCharsIODevice::close() {
    Q_ASSERT(false);
    m_parentDevice->close();
}

qint64 FilterWrongCharsIODevice::pos() const {
    Q_ASSERT(false);
    return m_parentDevice->pos();
}

qint64 FilterWrongCharsIODevice::readLineData(char* data, qint64 maxlen) {
    Q_ASSERT(false);
    // Forward to readLine() as readLineData() is protected
    return m_parentDevice->readLine(data, maxlen);
}

bool FilterWrongCharsIODevice::reset() {
    Q_ASSERT(false);
    return m_parentDevice->reset();
}

bool FilterWrongCharsIODevice::seek(qint64 pos) {
    Q_ASSERT(false);
    return m_parentDevice->seek(pos);
}

qint64 FilterWrongCharsIODevice::size() const {
    Q_ASSERT(false);
    return m_parentDevice->size();
}

bool FilterWrongCharsIODevice::waitForBytesWritten(int msecs) {
    Q_ASSERT(false);
    return m_parentDevice->waitForBytesWritten(msecs);
}

bool FilterWrongCharsIODevice::waitForReadyRead(int msecs) {
    Q_ASSERT(false);
    return m_parentDevice->waitForReadyRead(msecs);
}

qint64 FilterWrongCharsIODevice::writeData(const char* data, qint64 len) {
    Q_ASSERT(false);
    // Forward to write() as writeData() is protected
    return m_parentDevice->write(data, len);
}

#include "filterwrongcharsiodevice.moc"
