#ifndef FILTERIODEVICE_H
#define FILTERIODEVICE_H

#include <QIODevice>

/**
 * Device that filters away chars 0x1A and 0xFF.
 *
 * This object is only implemented for simple reading, all the other overrides are done in order to detect use of
 * methods that are not done. Signals are not implemented.
 */
class FilterWrongCharsIODevice : public QIODevice {
    typedef QIODevice super;

    Q_OBJECT

public:

    /**
     * @param parentDevice is the device to filter on and the QObject parent.
     * Will copy open mode from parentDevice.
     */
    FilterWrongCharsIODevice(QIODevice* parentDevice);

    ~FilterWrongCharsIODevice();

    // Delegators that are implemented:
    virtual bool open(OpenMode mode);
    virtual bool isSequential() const;

    // Delegators that are NOT implemented:
    virtual bool atEnd() const;
    virtual qint64 bytesAvailable() const;
    virtual qint64 bytesToWrite() const;
    virtual bool canReadLine() const;
    virtual void close();
    virtual qint64 pos() const;
    virtual bool reset();
    virtual bool seek(qint64 pos);
    virtual qint64 size() const;
    virtual bool waitForBytesWritten(int msecs);
    virtual bool waitForReadyRead(int msecs);

protected:

    // Delegators that are implemented:
    virtual qint64 readData(char* data, qint64 maxlen);

    // Delegators that are NOT implemented:
    virtual qint64 readLineData(char* data, qint64 maxlen);
    virtual qint64 writeData(const char* data, qint64 len);

private:

    QIODevice* m_parentDevice;

};

#endif // FILTERIODEVICE_H
