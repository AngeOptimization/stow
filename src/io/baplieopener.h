#ifndef BAPLIEOPENER_H
#define BAPLIEOPENER_H

#include "baplieparserresult.h"
#include "ondiskdata.h"

#include <QByteArray>
#include <QList>
#include <QString>

class document_t;
class QIODevice;

/**
 * Opens a BAPLIE file as a document by using it for creating a vessel and
 * schedule and then importing the containers.
 */
class BaplieOpener {

public:

    /**
     * Opens file as BAPLIE file
     * @throws std::runtime_error on file format errors
     */
    static OnDiskData openBaplieFile(const QString& fileName);

private:

    /**
     * Create empty BAPLIE reader
     */
    BaplieOpener();

    /**
     * Read BAPLIE files into the BaplieOpener
     * @throws std::runtime_error on file format errors
     */
    void readBaplie(QIODevice* device, const QString& fileName);

    OnDiskData newOnDiskData() const;
    document_t* newDocument() const;
    void importContainers(document_t* document) const;
    QList<QString> loadPorts() const;
    QList<QString> dischargePorts() const;

private:

    QString m_fileName;
    QByteArray m_baplieData;
    BaplieParserResult m_baplieParserResult;

};

#endif // BAPLIEOPENER_H
