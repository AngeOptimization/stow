#ifndef FILEOPENER_H
#define FILEOPENER_H

#include "ondiskdata.h"

#include <QString>

/**
 * Opens a file and reads a document_t out of it.
 * Works on .sto and BAPLIE files.
 */
class FileOpener {

public:

    /**
     * Opens file as .sto file
     * @throws std::runtime_error on file format errors
     */
    static OnDiskData openStoFile(const QString& fileName);

    /**
     * Opens file as BAPLIE file
     * @throws std::runtime_error on file format errors
     */
    static OnDiskData openBaplieFile(const QString& fileName);

    /**
     * Opens general file.
     * Will open file as baplie file if it has extensions 'edi', 'txt' or
     * 'baplie', will otherwise open the file as .sto file.
     * @throws std::runtime_error on file format errors
     */
    static OnDiskData openFile(const QString& fileName);

    /**
     * Opens the blank stowage used for program startup
     */
    static OnDiskData openBlankStowage();

};

#endif // FILEOPENER_H
