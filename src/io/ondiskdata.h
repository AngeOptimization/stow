#ifndef ONDISKDATA_H
#define ONDISKDATA_H

namespace ange {
namespace schedule {
class Call;
}
}
class document_t;

/**
 * Struct representing what's read in from a file.
 * This represents both the document and the application state.
 * Note that a nullpointer is a valid value for currentCall
 */
struct OnDiskData {
    document_t* document;
    const ange::schedule::Call* currentCall;
    OnDiskData() : document(0), currentCall(0) { }
};

#endif // ONDISKDATA_H
