#include "stowagereader.h"

#include "bayweightlimits.h"
#include "containerlist_reader.h"
#include "currentcallreader.h"
#include "document/document.h"
#include "document/tanks/tankconditions.h"
#include "document/userconfiguration/userconfiguration.h"
#include "draftsurveyreader.h"
#include <portwaterdensity.h>
#include "stabilityforcer.h"
#include "macrostowagereader.h"
#include "multivesselreader.h"
#include "ondiskdata.h"
#include "pivotcallreader.h"
#include "customportwaterdensityreader.h"

#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>
#include <ange/stowbase/bundle.h>
#include <ange/stowbase/jsonutils.h>
#include <ange/stowbase/schedulereader.h>
#include <ange/vessel/vessel.h>
#include <ange/vessel/vesseltank.h>

#include <KArchive/KZip>

#include <QFile>
#include <QMimeDatabase>
#include <QBuffer>
#include <QUndoStack>

using ange::schedule::Call;
using ange::schedule::Schedule;
using ange::stowbase::Bundle;
using ange::stowbase::readMandatoryValue;
using ange::stowbase::readOptionalValue;
using ange::stowbase::ReferenceObject;
using ange::stowbase::ScheduleReader;
using ange::stowbase::vcast;
using ange::vessel::Vessel;
using ange::vessel::VesselTank;

StowageReader::StowageReader()
    : m_scheduleReader(new ScheduleReader),
      m_vesselReader(new MultiVesselReader) {
}

StowageReader::~StowageReader() {
    // Do nothing, used for QScopedPointer
}

OnDiskData StowageReader::read(const QString& filename, QObject* parent) {
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly)) {
        throw std::runtime_error(QString("Failed to open file '%1' for reading: %2").arg(filename).arg(file.errorString()).toStdString());
    }
    return read(&file, filename, parent);
}

OnDiskData StowageReader::read(QIODevice* device, QString filename, QObject* parent) {
    QMimeDatabase mimeDataBase;
    QMimeType mimeType = mimeDataBase.mimeTypeForData(device);
    if (mimeType.inherits("application/x-gzip") || mimeType.inherits("text/plain")) {
        return readOldJsonFormat(device, parent);
    } else if (mimeType.inherits("application/zip")) {
        return readZip1Format(device, filename, parent);
    } else {
        throw std::runtime_error(QString("Unsupported mime type '%1'").arg(mimeType.name()).toStdString());
    }
}

Vessel* StowageReader::read_vessel(const QString& filename) {
    Q_ASSERT(false); // XXX This doesn't work! Not a huge problem as it is only used when importing *.sto files as vessel
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly)) {
        throw std::runtime_error(QString("Failed to open file '%1' for reading: %2").arg(filename).arg(file.errorString()).toStdString());
    }
    return m_vesselReader->readVessel(&file);
}

OnDiskData StowageReader::readOldJsonFormat(QIODevice* device, QObject* parent) {
    QByteArray data = device->readAll();
    QBuffer dataBuffer(&data);
    Bundle bundle;
    bundle.read(&dataBuffer);
    ReferenceObject stowageRo = bundle.find("stowage");
    ReferenceObject vesselRo = stowageRo.readOptionalReferenceObject("vessel", bundle);
    if (vesselRo.empty()) {
        throw std::runtime_error("Vessel not found in stowage file");
    }
    QScopedPointer<Vessel> vessel(m_vesselReader->readVessel(bundle, vesselRo));
    Q_ASSERT(vessel.data());
    OnDiskData result;
    result.document = readStowageObject(bundle, vessel.take(), data, UseTankReferences, parent);
    return result;
}

static int parseVersion(const KArchiveFile* file) {
    Q_ASSERT(file);
    QByteArray data = file->data();
    if (data == QByteArray("VERSION = 1\n")) {
        return 1;
    }
    qDebug() << data;
    Q_ASSERT(false);
    return -1;
}

OnDiskData StowageReader::readZip1Format(QIODevice* device, QString filename, QObject* parent) {
    KZip zipfile(device);
    if (!zipfile.open(QIODevice::ReadOnly)) {
        throw std::runtime_error(QString("Failed to open file '%1' for reading").arg(filename).toStdString());
    }
    const KArchiveDirectory* zipRootDir = zipfile.directory();
    const KArchiveEntry* metaInfEntry = zipRootDir->entry("META-INF");
    Q_ASSERT(metaInfEntry);
    if (!metaInfEntry) {
        throw std::runtime_error(QString("Failed to open file '%1' for reading").arg(filename).toStdString());
    }
    const KArchiveDirectory* metaInfDir = dynamic_cast<const KArchiveDirectory*>(metaInfEntry);
    if (!metaInfDir) {
        throw std::runtime_error(QString("Failed to open file '%1' for reading").arg(filename).toStdString());
    }
    const KArchiveEntry* versionIniEntry = metaInfDir->entry("version.ini");
    if (!versionIniEntry) {
        throw std::runtime_error(QString("Failed to open file '%1' for reading").arg(filename).toStdString());
    }
    const KArchiveFile* versionIni = dynamic_cast<const KArchiveFile*>(versionIniEntry);
    if (!versionIni) {
        throw std::runtime_error(QString("Failed to open file '%1' for reading").arg(filename).toStdString());
    }
    int version = parseVersion(versionIni);
    Q_ASSERT(version == 1);
    if (version != 1) {
        throw std::runtime_error(qPrintable(QString("Unsupported file version. This application only supports version 1. This file has version %1").arg(version)));
    }

    const KArchiveEntry* vesselDirEntry = zipRootDir->entry("vessel");
    Q_ASSERT(vesselDirEntry);
    if (!vesselDirEntry) {
        throw std::runtime_error(QString("Failed to open file '%1' for reading").arg(filename).toStdString());
    }
    const KArchiveDirectory* vesselDir = dynamic_cast<const KArchiveDirectory*>(vesselDirEntry);
    Q_ASSERT(vesselDir);
    if (!vesselDir) {
        throw std::runtime_error(QString("Failed to open file '%1' for reading").arg(filename).toStdString());
    }
    const KArchiveEntry* vesselFileEntry = vesselDir->entry("vessel.json");
    if (!vesselFileEntry) {
        throw std::runtime_error(QString("Failed to open file '%1' for reading").arg(filename).toStdString());
    }
    const KArchiveFile* vesselFile = dynamic_cast<const KArchiveFile*>(vesselFileEntry);
    if (!vesselFile) {
        throw std::runtime_error(QString("Failed to open file '%1' for reading").arg(filename).toStdString());
    }
    QByteArray vesselData = vesselFile->data();
    QBuffer vesselDataBuffer(&vesselData);
    vesselDataBuffer.open(QIODevice::ReadOnly);
    ange::vessel::Vessel* vessel = m_vesselReader->readVessel(&vesselDataBuffer);
    Q_ASSERT(vessel);
    const KArchiveEntry* oldStoFileEntry = zipRootDir->entry("old_sto_file.json");
    ange::stowbase::Bundle oldStoFileBundle;
    if (oldStoFileEntry) {
        const KArchiveFile* oldStoFileFile = dynamic_cast<const KArchiveFile*>(oldStoFileEntry);
        Q_ASSERT(oldStoFileFile);
        if (!oldStoFileFile) {
            throw std::runtime_error(QString("Failed to open file '%1' for reading").arg(filename).toStdString());
        }
        QIODevice* oldStoFileIoDevice = oldStoFileFile->createDevice();
        oldStoFileBundle.read(oldStoFileIoDevice);
        delete oldStoFileIoDevice;
    }
    int pivotCallIndex = -1;
    {
        const KArchiveEntry* pivotCallEntry = zipRootDir->entry("schedule/pivot_call.xml");
        if (pivotCallEntry) {
            const KArchiveFile* pivotCallFile = dynamic_cast<const KArchiveFile*>(pivotCallEntry);
            if (!pivotCallFile) {
                throw std::runtime_error(QString("Failed to open file '%1' for reading").arg(filename).toStdString());
            }
            QIODevice* pivotCallDevice = pivotCallFile->createDevice();
            PivotCallReader reader;
            if (reader.parse(pivotCallDevice)) {
                pivotCallIndex = reader.pivotPortIndex();
            } else {
                throw std::runtime_error(QString("Failed to open file '%1' for reading").arg(filename).toStdString());
            }
            delete pivotCallDevice;
        }
    }
    int currentCallIndex = -1;
    {
        const KArchiveEntry* currentCallEntry = zipRootDir->entry("application_state/application_state.xml");
        if (currentCallEntry) {
            const KArchiveFile* currentCallFile = dynamic_cast<const KArchiveFile*>(currentCallEntry);
            if (!currentCallFile) {
                throw std::runtime_error(QString("Failed to open file '%1' for reading").arg(filename).toStdString());
            }
            QIODevice* currentCallDevice = currentCallFile->createDevice();
            CurrentCallReader reader;
            if (reader.parse(currentCallDevice)) {
                currentCallIndex = reader.currentCallIndex();
            } else {
                throw std::runtime_error(QString("Failed to open file '%1' for reading").arg(filename).toStdString());
            }
            delete currentCallDevice;
        }
    }
    QHash<int, ange::units::Density> customPortWaterDensities;
    {
        const KArchiveEntry* customPortWaterDensitiesEntry = zipRootDir->entry("stability/custom_port_water_densities.xml");
        if(customPortWaterDensitiesEntry) {
            const KArchiveFile* customPortWaterDensitiesFile = dynamic_cast<const KArchiveFile*>(customPortWaterDensitiesEntry);
            if(!customPortWaterDensitiesFile) {
                throw std::runtime_error(QString("Failed to open file '%1' for reading").arg(filename).toStdString());
            }
            QScopedPointer<QIODevice> customPortWaterDensitiesDevice(customPortWaterDensitiesFile->createDevice());
            CustomPortWaterDensityReader reader;
            if(reader.parse(customPortWaterDensitiesDevice.data())) {
                customPortWaterDensities = reader.customPortWaterDensities();
            } else {
                throw std::runtime_error(QString("Failed to open file '%1' for reading").arg(filename).toStdString());
            }
        }
    }
    document_t* document = readStowageObject(oldStoFileBundle, vessel, vesselData, UseTankDescriptions, parent);
    // if pivot port is set in the file, set it on the document (overwriting the default set in get_schedule)
    if (pivotCallIndex > 0) {
        document->schedule()->setPivotCall(pivotCallIndex);
    }
    for(QHash<int, ange::units::Density>::iterator it = customPortWaterDensities.begin(); it != customPortWaterDensities.end(); it++) {
        Call* call = document->schedule()->at(it.key());
        Q_ASSERT(call);
        ange::units::Density density = it.value();
        document->portWaterDensity()->setCustomWaterDensity(call, density);
    }

    { // draftsurvey at stability/draftsurvey.xml
        const KArchiveEntry* draftSurveyEntry = zipRootDir->entry("stability/draftsurvey.xml");
        if(draftSurveyEntry) {
            const KArchiveFile* draftSurveyFile = dynamic_cast<const KArchiveFile*>(draftSurveyEntry);
            if(!draftSurveyFile) {
                throw std::runtime_error(QString("Failed to open file stability/draftsurvey.xml from '%1' for reading").arg(filename).toStdString());
            }
            QScopedPointer<QIODevice> draftSurveyDevice(draftSurveyFile->createDevice());
            DraftSurveyReader reader;
            if(!reader.parse(draftSurveyDevice.data())) {
                throw std::runtime_error(QString("Failed to parse XML file stability/draftsurvey.xml from '%1'").arg(filename).toStdString());
            }
            {
                QHash<int, ange::vessel::BlockWeight> deadLoads = reader.deadLoads();
                for(QHash<int, ange::vessel::BlockWeight>::iterator it = deadLoads.begin(); it != deadLoads.end(); it++) {
                    Call* call = document->schedule()->at(it.key());
                    Q_ASSERT(call);
                    document->stabilityForcer()->setDeadLoad(call, it.value());
                }
            }
            {
                QHash<int, ange::units::Length> forcedDrafts = reader.forcedDrafts();
                for(QHash<int, ange::units::Length>::iterator it = forcedDrafts.begin(); it != forcedDrafts.end(); it++) {
                    Call* call = document->schedule()->at(it.key());
                    Q_ASSERT(call);
                    document->stabilityForcer()->setForcedDraft(call, it.value());
                }
            }
            {
                QHash<int, ange::units::Length> forcedTrims = reader.forcedTrims();
                for(QHash<int, ange::units::Length>::iterator it = forcedTrims.begin(); it != forcedTrims.end(); it++) {
                    Call* call = document->schedule()->at(it.key());
                    Q_ASSERT(call);
                    document->stabilityForcer()->setForcedTrim(call, it.value());
                }
            }
            {
                QHash<int, ange::units::Length> forcedGms = reader.forcedGms();
                for(QHash<int, ange::units::Length>::iterator it = forcedGms.begin(); it != forcedGms.end(); it++) {
                    Call* call = document->schedule()->at(it.key());
                    Q_ASSERT(call);
                    document->stabilityForcer()->forceGm(call, it.value());
                }
            }
        }
    }

    Call* currentCall = document->schedule()->at(currentCallIndex);
    OnDiskData result;
    result.document = document;
    result.currentCall = currentCall;
    return result;
}

document_t* StowageReader::readStowageObject(const Bundle& bundle, Vessel* rawVessel, QByteArray vesselData,
                                             TankConditionsReadMode tankReadMode, QObject* parent) {
    Q_ASSERT(rawVessel);
    QScopedPointer<Vessel> vessel(rawVessel);
    ReferenceObject stowageRo = bundle.find("stowage");
    QScopedPointer<Schedule> schedule(readSchedule(bundle, stowageRo));
    Q_ASSERT(schedule.data());
    QScopedPointer<stowage_t> stowage(new stowage_t(schedule.data()));
    QScopedPointer<document_t> document(new document_t(schedule.take(), vessel.take(), stowage.take(), parent));
    document.data()->setVesselFileData(vesselData);

    // MacroStowageReader().read
    ReferenceObject macroStowageRo = stowageRo.readOptionalReferenceObject("macroStowage", bundle);
    if (!macroStowageRo.group().isEmpty()) {
        ReferenceObject schedule_ro = stowageRo.readMandatoryReferenceObject("schedule", bundle);
        MacroStowageReader().read(bundle, macroStowageRo, schedule_ro, document.data());
    }

    // containerlistReader.read
    containerlist_reader_t containerlistReader(document->schedule()->calls().at(1));
    containerlistReader.read(stowageRo, bundle, document.data(), m_scheduleReader.data());

    // document->stowage()->set_weight_limit_for_bay
    Q_FOREACH (ReferenceObject bwlRo, stowageRo.readAllReferencedObjects("bayWeightLimits", bundle)) {
        if (const ange::vessel::BaySlice* baySlice = document->vessel()->baySliceContaining(bwlRo.readMandatoryValue<QString>("bayNumber").toInt())) {
            if (const Call* call = m_scheduleReader->call(bwlRo.readOptionalReference("call"))) {
                document->bayWeightLimits()->setWeightLimitForBay(call, baySlice, bwlRo.readMandatoryValue<double>("limit")*ange::units::kilogram);
            } else {
                qWarning("No call with reference %s", bwlRo.readOptionalReference("call").toLocal8Bit().data());
            }
        } else {
            qWarning("No bay slice for %s", bwlRo.readMandatoryValue<QString>("bayNumber").toLocal8Bit().data());
        }
    }

    // document->tankConditions()->changeTankCondition
    QHash<QString, VesselTank*> vesselTankDescriptionMap;
    Q_FOREACH (VesselTank* tank, document->vessel()->tanks()) {
        vesselTankDescriptionMap.insert(tank->description(), tank);
    }
    Q_FOREACH (ReferenceObject tankRo, stowageRo.readAllReferencedObjects("tankConditions", bundle)) {
        VesselTank* tank;
        if (tankReadMode == UseTankReferences) {
            tank = m_vesselReader->vesselTank(tankRo.readOptionalReference("tank"));
        } else if (tankReadMode == UseTankDescriptions) {
            tank = vesselTankDescriptionMap.value(tankRo.readOptionalReference("tank"), 0) ;
        } else {
            Q_ASSERT(false);
            tank = 0;
        }
        const Call* call = m_scheduleReader->call(tankRo.readOptionalReference("call"));
        const ange::units::Mass condition = tankRo.readOptionalValue("condition", 0.0) * ange::units::kilogram;
        if (tank && call) {
            document->tankConditions()->changeTankCondition(call, tank, condition);
        } else {
            qWarning("Could not read tank id %s", tankRo.reference().toLocal8Bit().data());
        }
    }

    return document.take();
}

Schedule* StowageReader::readSchedule(const Bundle& bundle, const ReferenceObject& stowageRo) {
    QScopedPointer<Schedule> schedule;
    ReferenceObject schedule_ro = stowageRo.readOptionalReferenceObject("schedule", bundle);
    schedule.reset(m_scheduleReader->readSchedule(bundle, schedule_ro));
    schedule->insert(0, new Call(QLatin1String("Befor"), QLatin1String("XXXB")));
    schedule->insert(schedule->size(), new Call(QLatin1String("After"), QLatin1String("XXXA")));
    // insert somekind of sane default as pivot port in schedule.
    // this may be overwritten later in document reading if we get a better value
    schedule->setPivotCall(schedule->size() / 2);
    return schedule.take();
}
