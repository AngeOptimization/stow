#ifndef MACROSTOWAGEWRITER_H
#define MACROSTOWAGEWRITER_H

#include <QString>

namespace ange {
namespace schedule {
class Schedule;
}
namespace vessel {
class Vessel;
}
namespace stowbase {
class Bundle;
}
namespace angelstow {
class MacroPlan;
}
}

class MacroStowageWriter {

public:
    /**
     * Write macro stowage to bundle.
     */
    static QString write(ange::stowbase::Bundle& bundle, const ange::angelstow::MacroPlan* macro_stowage,
                         const ange::vessel::Vessel* vessel, const ange::schedule::Schedule* schedule);
};

#endif // MACROSTOWAGEWRITER_H
