
#ifndef MACROSTOWAGEREADER_H
#define MACROSTOWAGEREADER_H

namespace ange {
namespace stowbase {
class ReferenceObject;
class Bundle;
}
}
class document_t;

class MacroStowageReader {

public:
    /**
     * Reads macro stowage
     * @param macroStowageRo
     * @param document
     */
    void read(const ange::stowbase::Bundle& bundle,
              const ange::stowbase::ReferenceObject& macroStowageRo,
              const ange::stowbase::ReferenceObject& scheduleRo,
              document_t* document);

};

#endif // MACROSTOWAGEREADER_H
