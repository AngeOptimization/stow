/*
 Author: Ange Optimization <contact@ange.dk>  (C) Ange Optimization ApS 2010

 Copyright: See COPYING file that comes with this distribution

*/

#ifndef CONTAINERLIST_READER_H
#define CONTAINERLIST_READER_H
#include <QList>
#include <QString>
#include <QHash>

#include <ange/stowbase/containermovereader.h>

class merger_command_t;
class pool_t;
class container_stow_command_t;
class container_list_change_command_t;
class container_move_t;
class document_t;
namespace ange {
namespace stowbase {
class Bundle;
class ReferenceObject;
class ContainerReader;
}
}

class containerlist_reader_t {
  public:
    containerlist_reader_t(const ange::schedule::Call* current_call);
    ~containerlist_reader_t();

    void read(const ange::stowbase::ReferenceObject& stowage_ro, const ange::stowbase::Bundle& bundle, document_t* document, const ange::stowbase::ScheduleReader* schedule_reader);
  private:
    QHash<QString, ange::containers::Container*> m_container_refs;
    typedef QHash<ange::containers::Container*, QList<ange::stowbase::ContainerMoveReader::Move> > moves_t;
    moves_t m_moves;
    void add_move(const ange::stowbase::ContainerMoveReader::Move& raw_move, ange::stowbase::ContainerReader* container_reader);
    const ange::schedule::Call* m_current_call;
    void read_container_into_document( ange::containers::Container* container, QList< ange::stowbase::ContainerMoveReader::Move > moves, document_t* document, container_list_change_command_t* add_command);
    void read_container_moves_into_document(ange::containers::Container* container, const QList< ange::stowbase::ContainerMoveReader::Move >& moves, document_t* document, merger_command_t* command);
};

#endif // CONTAINERLIST_READER_H
