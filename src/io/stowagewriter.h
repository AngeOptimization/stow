#ifndef STOWAGEWRITER_H
#define STOWAGEWRITER_H

#include <QString>
#include <QStringList>
#include <QVariant>
#include <QFuture>

class QIODevice;
class document_t;
template<class T>
class QFutureWatcher;
class VesselWriter;
namespace ange {
namespace schedule {
class Call;
class Schedule;
}
namespace vessel {
class VesselTank;
}
namespace stowbase {
class Bundle;
class ReferenceObjectFactory;
class ScheduleWriter;
class ContainerWriter;
class ContainerMoveWriter;
}
namespace containers {
class Container;
}
}

class StowageWriter : public QObject {

    Q_OBJECT

public:
    enum SaveResult {
        NoError = 0,
        FileOpenError = 1,
        FileWriteError = 2
    };

public:

    StowageWriter(const document_t* document, const ange::schedule::Call* currentCall);

    /**
     * Write document to stowage file (.sto)
     */
    void write(QString filename);

    /**
     *  does a async write of the stowage to filename. emits save_finished() when done
     */
    void writeAsync(QString filename);

    SaveResult error() const;

    QString errorMessage() const;

Q_SIGNALS:
    void documentSaved();

    void documentSaveFailed();

private Q_SLOTS:
    void saveJobFinished();

private:
    struct MovesReferences {
        /**
         * references to container moves
         */
        QStringList moves;
        /*
         * references to bundle moves
         */
        QStringList bundleMoves;
    };

private:

    /**
     * Creates a bundle out of the document for "old_sto_file.json"
     */
    ange::stowbase::Bundle convertToOldStoFileBundle() const;

    QVariantList writeTankConditions(ange::stowbase::Bundle& bundle, const QList<ange::vessel::VesselTank*> tanks,
                                     const ange::stowbase::ScheduleWriter& scheduleWriter) const;

    /**
     * Save containers (only) to bundle
     */
    MovesReferences saveContainers(ange::stowbase::Bundle& bundle, const ange::stowbase::ScheduleWriter* scheduleWriter) const;

    /**
     * Serializes the moves and adds to bundle using the provided move_writer.
     * This also saves moves for container bundles, it just needs a proper move writer.
     * @return list of references to the moves added
     */
    QStringList saveMoves(ange::stowbase::Bundle& bundle, ange::stowbase::ContainerMoveWriter& moveWriter,
                          const ange::containers::Container* container, const QString& containerReference) const;

private:
    const document_t* m_document;
    const ange::schedule::Call* m_currentCall;
    QFuture<SaveResult> m_saveFuture;
    QFutureWatcher<SaveResult>* m_futureWatcher;
};

#endif // STOWAGEWRITER_H
