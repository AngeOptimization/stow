#include "macrostowagewriter.h"
#include "stowplugininterface/macroplan.h"
#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>
#include <ange/stowbase/bundle.h>
#include <ange/stowbase/jsonutils.h>
#include <ange/stowbase/referenceobject.h>
#include <ange/vessel/bayslice.h>
#include <ange/vessel/compartment.h>
#include <ange/vessel/stack.h>
#include <ange/vessel/vessel.h>
#include <QStringList>

using ange::vessel::BaySlice;
using ange::vessel::Compartment;
using ange::schedule::Call;
using ange::stowbase::ReferenceObject;

// static
QString MacroStowageWriter::write(ange::stowbase::Bundle& bundle, const ange::angelstow::MacroPlan* macro_stowage,
                                  const ange::vessel::Vessel* vessel, const ange::schedule::Schedule* schedule) {
    if (schedule->size() <= 2) {
        return QString();
    }

    QVariantList macroStowageLocations;
    const Call* const firstCall = schedule->calls().at(1);
    const Call* const beforeCall = schedule->calls().front();
    ReferenceObject scheduleRo = bundle.find("schedule", QVariantMap());
    QVariantList calls = scheduleRo.readMandatoryValue<QVariantList>("entries");
    Q_ASSERT(calls.size() == schedule->size() - 2);
    Q_FOREACH (BaySlice* baySlice, vessel->baySlices()) {
        Q_FOREACH (Compartment* compartment, baySlice->compartments()) {
            Q_FOREACH (const Call* loadCall, schedule->calls()) {
                const Call* allowedDischarge = macro_stowage->dischargeCall(compartment, loadCall);
                if (allowedDischarge) {
                    ReferenceObject object = bundle.build("macroStowageLocation");
                    if (loadCall != beforeCall) {
                        object.insertReference("loadScheduleEntry", calls.at(firstCall->distance(loadCall)));
                    }
                    object.insertReference("allowedDischargeEntries", calls.at(firstCall->distance(allowedDischarge)));
                    QVariantList stackList;
                    Q_FOREACH (const ange::vessel::Stack* stack, compartment->stacks()) {
                        const int bay = stack->baySlice()->joinedBay();
                        stackList << ange::stowbase::vessel_stack_position_to_urn(vessel->imoNumber(), bay,
                                                                                   stack->row(), stack->level());
                    }
                    object.insertStringList("stacks", stackList);
                    bundle << object;
                    macroStowageLocations << object.reference();
                }
            }
        }
    }

    if (!macroStowageLocations.empty())  {
        ReferenceObject macroStowageObject = bundle.build("macroStowage");
        macroStowageObject.insertReferences("locations", macroStowageLocations);
        bundle << macroStowageObject;
        return macroStowageObject.reference();
    } else {
        return QString();
    }
}
