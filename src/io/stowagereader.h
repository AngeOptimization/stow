#ifndef STOWAGEREADER_H
#define STOWAGEREADER_H

#include "ondiskdata.h"
#include "document/stowage/stowage.h"
#include <ange/stowbase/bundle.h>

class document_t;
class MultiVesselReader;
namespace ange {
namespace stowbase {
class ScheduleReader;
}
namespace vessel {
class Vessel;
}
}

class StowageReader {

public:
    StowageReader();
    ~StowageReader();

    /**
     * Convert from file, works both with zip based and json based.
     * See also read(QIODevice* device, QString filename, QObject* parent)
     */
    OnDiskData read(const QString& filename, QObject* parent);

    /**
     * Convert from IO device.
     * \param device to read from
     * \param filename used for error messages
     * \param parent object used for parenting of created objects
     * \throws std::runtime_error on file format errors
     */
    OnDiskData read(QIODevice* device, QString filename, QObject* parent);

    /**
     * Extract vessel only
     * TODO remove?
     */
    ange::vessel::Vessel* read_vessel(const QString& filename);

private:
    enum TankConditionsReadMode {
        // For 'old sto format' with one file where the vessel could be referenced from the tank conditions
        UseTankReferences,
        // For 'new sto format' in muliple files (Zip1)
        UseTankDescriptions
    };

private:
    // Convert from parsed JSON
    OnDiskData readOldJsonFormat(QIODevice* device, QObject* parent);

    // Convert from new Zip1 format
    OnDiskData readZip1Format(QIODevice* device, QString filename, QObject* parent);

    document_t* readStowageObject(const ange::stowbase::Bundle& bundle,
                                  ange::vessel::Vessel* rawVessel, QByteArray vesselData,
                                  TankConditionsReadMode tankReadMode, QObject* parent);
    ange::schedule::Schedule* readSchedule(const ange::stowbase::Bundle& bundle,
                                             const ange::stowbase::ReferenceObject& stowageRo);

private:
    QScopedPointer<ange::stowbase::ScheduleReader> m_scheduleReader;
    QScopedPointer<MultiVesselReader> m_vesselReader;
};

#endif // STOWAGEREADER_H
