#include "baplieopener.h"

#include "baplieparser.h"
#include "baplieparserresult.h"
#include "baplietooldstyleedifactreaderhack.h"
#include "baplievesselreader.h"
#include "document.h"
#include "edifact_container_reader.h"
#include "ediproblem.h"
#include "parserproblem.h"
#include "stowage.h"

#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>
#include <ange/vessel/vessel.h>

#include <QBuffer>
#include <QFile>
#include <QUndoStack>

#include <stdexcept>

using namespace ange::schedule;
using namespace ange::units;
using namespace ange::vessel;

OnDiskData BaplieOpener::openBaplieFile(const QString& fileName) {
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly)) {
        throw std::runtime_error(QString("Could not open file '%1': %2").arg(fileName).arg(file.errorString()).toStdString());
    }
    BaplieOpener baplieOpener;
    baplieOpener.readBaplie(&file, fileName);
    return baplieOpener.newOnDiskData();
}

BaplieOpener::BaplieOpener()
{
    // Empty
}

void BaplieOpener::readBaplie(QIODevice* device, const QString& fileName) {
    m_fileName = fileName;
    // Read and parse the BAPLIE file
    m_baplieData = device->readAll();
    QBuffer buffer(&m_baplieData);
    buffer.open(QIODevice::ReadOnly);
    m_baplieParserResult = BaplieParser::parse(&buffer);
    QString errors;
    Q_FOREACH (const EdiProblem& ediProblem, m_baplieParserResult.problemList) {
        if (ediProblem.type() == EdiProblem::Error) {
            errors += ediProblem.message() + ",\n";
        }
    }
    Q_ASSERT(m_baplieParserResult.hasErrors() == !errors.isNull());
    if (!errors.isNull()) {
        errors.resize(errors.size() - 2);  // Chop off last extra ",\n"
        throw std::runtime_error(QString("BAPLIE format error in '%1':\n%2").arg(fileName).arg(errors).toStdString());
    }
}

QList<QString> BaplieOpener::loadPorts() const {
    QMap<QString, bool> calls; // Use a map as an ordered set
    Q_FOREACH (const BaplieParserResult::ContainerPosition& cp, m_baplieParserResult.containerList()) {
        calls[cp.container.data()->loadPort()] = true;
    }
    return calls.keys();
}

QList<QString> BaplieOpener::dischargePorts() const {
    QMap<QString, bool> calls; // Use a map as an ordered set
    Q_FOREACH (const BaplieParserResult::ContainerPosition& cp, m_baplieParserResult.containerList()) {
        calls[cp.container.data()->dischargePort()] = true;
    }
    return calls.keys();
}

OnDiskData BaplieOpener::newOnDiskData() const {
    OnDiskData onDiskData;
    onDiskData.document = newDocument();
    Schedule* schedule = onDiskData.document->schedule();
    onDiskData.currentCall = schedule->getCallByUncodeAfterCall(m_baplieParserResult.departurePortCode, schedule->at(0));
    return onDiskData;
}

document_t* BaplieOpener::newDocument() const {
    QList<Call*> calls;
    calls << new Call(QLatin1String("Befor"), QLatin1String("XXXB"));
    // Add load ports in alfabetical order, could be improved by looking at overstows, but that would be very complicated
    Q_FOREACH (const QString& loadPort, loadPorts()) {
        if (loadPort == m_baplieParserResult.departurePortCode) {
            continue; // Skip departure port
        }
        QString voyageCode = m_baplieParserResult.voyageCode + "b";
        calls << new Call(loadPort, voyageCode);
    }
    int pivotCallIndex = calls.size(); // Make departurePortCode the pivot call
    calls << new Call(m_baplieParserResult.departurePortCode, m_baplieParserResult.voyageCode);
    // Add discharge ports in alfabetical order, could be improved by looking at overstows, but that would be very complicated
    Q_FOREACH (const QString& dischargePort, dischargePorts()) {
        QString voyageCode = m_baplieParserResult.voyageCode + "a";
        calls << new Call(dischargePort, voyageCode);
    }
    calls << new Call(QLatin1String("After"), QLatin1String("XXXA"));
    Schedule* schedule = new Schedule(calls);
    schedule->setPivotCall(pivotCallIndex);

    Vessel* vessel = BaplieVesselReader::newVessel(m_baplieParserResult);
    stowage_t* stowage = new stowage_t(schedule);
    document_t* document = new document_t(schedule, vessel, stowage);
    document->setVesselFileData(BaplieVesselReader::transformatToJson(m_baplieData));
    importContainers(document);
    document->undostack()->clear(); // importContainers modifies the undo stack
    document->undostack()->push(new QUndoCommand("Open BAPLIE file as stowage"));
    document->stowage_filename(m_fileName + ".sto");
    return document;
}

void BaplieOpener::importContainers(document_t* document) const {
    edifact_container_reader_t* edifactReader
        = new BaplieToOldStyleEdifactReaderHack(document, 0, m_baplieParserResult, m_fileName);
    edifactReader->add_containers_to_document();
    delete edifactReader;
}
