#include <QtTest>

#include "baplieopener.h"

#include <QObject>

#include <stdexcept>

class BaplieOpenerTest : public QObject {
    Q_OBJECT
private Q_SLOTS:
    void testMvTkosPse();
};

QTEST_GUILESS_MAIN(BaplieOpenerTest);

void BaplieOpenerTest::testMvTkosPse() {
    QString fileName = QFINDTESTDATA("data/MV_TKOS_PSE.EDI");
    try {
        BaplieOpener::openBaplieFile(fileName);
        QVERIFY(false); // Should not reach this line
    } catch (const std::runtime_error& e) {
        QCOMPARE(e.what(), QString("BAPLIE format error in '%1':\n"
                                   "No LOC segment found in group 1").arg(fileName).toLocal8Bit().data());
    }
}

#include "baplieopenertest.moc"
