#include <QObject>
#include <QFile>
#include <QMimeDatabase>
#include <QTest>
#include <document.h>
#include <test/util/vesselcomparator.h>
#include <test/util/tankconditioncomparator.h>
#include <fileopener.h>


class GzipZip1FormatTanksTest : public QObject {
    Q_OBJECT
    public:
    private Q_SLOTS:
        void compareDocuments();
        void compareDocuments_data();
};

void GzipZip1FormatTanksTest::compareDocuments() {
    QFETCH(QString, gzfilename);
    QFETCH(QString, zip1filename);

    QString zip1filepath = QFINDTESTDATA("zip1-gz-format-tanks/"+zip1filename);
    QString gzfilepath = QFINDTESTDATA("zip1-gz-format-tanks/"+gzfilename);

    QVERIFY(QFile::exists(zip1filepath));
    QVERIFY(QFile::exists(gzfilepath));

    QFile zip1file(zip1filepath);
    QFile gzfile(gzfilepath);

    QVERIFY(zip1file.open(QIODevice::ReadOnly));
    QVERIFY(gzfile.open(QIODevice::ReadOnly));

    QMimeDatabase db;
    QMimeType zip1type = db.mimeTypeForData(&zip1file);
    QMimeType gztype = db.mimeTypeForData(&gzfile);
    QVERIFY(zip1type.inherits("application/zip"));
    QVERIFY(gztype.inherits("application/x-gzip"));
    zip1file.close();
    gzfile.close();

    document_t* zip1document = FileOpener::openStoFile(zip1filepath).document;
    document_t* gzdocument = FileOpener::openStoFile(gzfilepath).document;

    VesselComparator vesselcomparator(zip1document->vessel(), gzdocument->vessel());
    ERUN(vesselcomparator.compareAll());

    TankConditionsComparator tankconditionscomparator(zip1document->tankConditions(), zip1document->schedule(), gzdocument->tankConditions(), gzdocument->schedule());
    ERUN(tankconditionscomparator.compare());
}

void GzipZip1FormatTanksTest::compareDocuments_data() {
    QTest::addColumn<QString>("gzfilename");
    QTest::addColumn<QString>("zip1filename");

    QTest::newRow("lego-maersk-w-stability-with-tank-contents") << QString("lego-maersk-w-stability-with-tank-contents.xls.json.sto") << QString("lego-maersk-w-stability-with-tank-contents.xls.json-new.sto");
}



QTEST_GUILESS_MAIN(GzipZip1FormatTanksTest);
#include "gzip-zip1-format-tanks.moc"
