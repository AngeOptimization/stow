#include <QTest>

#include "document/document.h"
#include "document/undo/schedule_change_command.h"
#include <document/stowage/stowage.h>
#include <document/undo/changecontainercommand.h>
#include <document/containers/container_list.h>
#include <fileopener.h>

#include <test/util/schedulecomparator.h>
#include <test/util/tankconditioncomparator.h>
#include <test/util/vesselcomparator.h>

#include <stowplugininterface/validatorplugin.h>

#include <ange/containers/container.h>
#include <ange/schedule/schedule.h>
#include <ange/vessel/bilinearinterpolator.h>
#include <ange/vessel/vessel.h>
#include <ange/vessel/vesseltank.h>

#include <QObject>
#include <QScopedPointer>
#include <QTemporaryFile>

using namespace ange::vessel;
using namespace ange::containers;
using namespace ange::angelstow;

class IoTest : public QObject {
    Q_OBJECT
public:
    virtual ~IoTest();
private Q_SLOTS:
    void initTestCase();
    void testGeneralVesselInfo();
    void testContainers();
    void testSlotTable();
    void testVesselHull();
    void testSchedule();
    void testTankConditions();
    void testCurrentCall();
private:
    QScopedPointer<document_t> m_document1;
    QScopedPointer<document_t> m_document1Reloaded;
    const ange::vessel::Vessel* m_vessel1;
    const ange::vessel::Vessel* m_vessel1Reloaded;
};

void IoTest::initTestCase(){
    m_document1.reset(FileOpener::openStoFile(QFINDTESTDATA("resources/real-test-vessel.sto")).document);
    m_vessel1 = m_document1->vessel();
    QTemporaryFile testSaveFile;
    testSaveFile.open();
    const Slot* slot1 = m_vessel1->slotAt(BayRowTier(4,4,82));
    const Container* container1 = m_document1->stowage()->containers_in_slot(slot1).at(0);
    QList<DangerousGoodsCode> new_dg_list;
    new_dg_list.append(DangerousGoodsCode("1907L"));
    ChangeContainerCommand* command1
        = ChangeContainerCommand::change_feature(m_document1.data()->containers(), container1, ChangeContainerCommand::Dg,
                                                    qVariantFromValue<QList<DangerousGoodsCode> >(new_dg_list));
    m_document1->undostack()->push(command1);
    m_document1->save_stowage(testSaveFile.fileName(),0);
    m_document1Reloaded.reset(FileOpener::openStoFile(testSaveFile.fileName()).document);
    m_vessel1Reloaded = m_document1Reloaded->vessel();
    testSaveFile.close();
}

void IoTest::testGeneralVesselInfo() {
    VesselComparator comparator(m_vessel1, m_vessel1Reloaded);
    ERUN(comparator.compareGeneralVesselInfo());
}

void IoTest::testContainers() {
    const Slot* slot1 = m_vessel1Reloaded->slotAt(BayRowTier(4,4,82));
    const Container* container1 = m_document1Reloaded->stowage()->containers_in_slot(slot1).at(0);
    QCOMPARE(m_document1Reloaded->containers()->size(), m_document1->containers()->size());
    QCOMPARE(container1->dangerousGoodsCodes().at(0).unNumber(), QString("1907"));
    QVERIFY(container1->dangerousGoodsCodes().at(0).isLimitedQuantity());
    //TODO: run through whole list of containers and compare on all attributes #1082
}

void IoTest::testSlotTable() {
    //TODO: run through the lists of slots, stacks, stack_supports and compare on all attributes #1083
}

void IoTest::testVesselHull() {
    VesselComparator comparator(m_vessel1, m_vessel1Reloaded);
    ERUN(comparator.compareHull());
}

void IoTest::testSchedule() {
    ScheduleComparator comparator(m_document1->schedule(), m_document1Reloaded->schedule());
    ERUN(comparator.compareAll());
}

void IoTest::testTankConditions() {
    TankConditionsComparator comparator(m_document1->tankConditions(), m_document1->schedule(), m_document1Reloaded->tankConditions(), m_document1Reloaded->schedule());
    ERUN(comparator.compare());
}

void IoTest::testCurrentCall() {
    QTemporaryFile testfile;
    testfile.open();
    const ange::schedule::Call* call = m_document1->schedule()->at(1);
    m_document1->save_stowage(testfile.fileName(),call);
    OnDiskData data = FileOpener::openStoFile(testfile.fileName());
    ERUN(ScheduleComparator::compareCall(call, data.currentCall));
    delete data.document;
}

IoTest::~IoTest() {

}

QTEST_GUILESS_MAIN(IoTest);
#include "io_test.moc"
