#include <QObject>
#include <QScopedPointer>
#include <QTemporaryFile>
#include "document/document.h"
#include <QTest>
#include <test/util/vesselcomparator.h>
#include <fileopener.h>

class TestClassification : public QObject {
    Q_OBJECT
private Q_SLOTS:
    void testLoadSaveClassification();
};

void TestClassification::testLoadSaveClassification() {
    QScopedPointer<document_t> document;
    QScopedPointer<document_t> documentReloaded;
    const ange::vessel::Vessel* vessel;
    const ange::vessel::Vessel* vesselReloaded;
    QTemporaryFile testSaveFile;

    document.reset(FileOpener::openStoFile(QFINDTESTDATA("resources/vessel-w-classification.sto")).document);
    vessel = document->vessel();
    testSaveFile.open();
    document->save_stowage(testSaveFile.fileName(),0);
    documentReloaded.reset(FileOpener::openStoFile(testSaveFile.fileName()).document);
    vesselReloaded = documentReloaded->vessel();

    QCOMPARE(document->vessel()->classificationSociety(), QString("GL"));
    VesselComparator comparator(vessel, vesselReloaded);
    ERUN(comparator.compareGeneralVesselInfo());
}

QTEST_GUILESS_MAIN(TestClassification);

#include "testclassification.moc"
