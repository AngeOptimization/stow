#include "stowagewriter.h"

#include "bayweightlimits.h"
#include "currentcallwriter.h"
#include "document/utils/callutils.h"
#include "document/containers/container_list.h"
#include "document/document.h"
#include "document/stowage/container_leg.h"
#include "document/stowage/container_move.h"
#include "document/stowage/stowage.h"
#include "document/tanks/tankconditions.h"
#include "stabilityforcer.h"
#include "portwaterdensity/portwaterdensity.h"
#include "macrostowagewriter.h"
#include "pivotcallwriter.h"
#include "customportwaterdensitywriter.h"
#include "draftsurveywriter.h"
#include "version.h"

#include <ange/containers/container.h>
#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>
#include <ange/stowbase/bundle.h>
#include <ange/stowbase/containermovewriter.h>
#include <ange/stowbase/containerwriter.h>
#include <ange/stowbase/containerbundlingwriter.h>
#include <ange/stowbase/jsonutils.h>
#include <ange/stowbase/schedulewriter.h>
#include <ange/vessel/bayrowtier.h>
#include <ange/vessel/bayslice.h>
#include <ange/vessel/slot.h>
#include <ange/vessel/stack.h>
#include <ange/vessel/vessel.h>
#include <ange/vessel/vesseltank.h>

#include <KZip>

#include <QBuffer>
#include <QFile>
#include <QFutureWatcher>
#include <QSettings>
#include <QtConcurrentRun>

using namespace ange::units;
using ange::angelstow::MasterPlannedType;
using ange::angelstow::MicroStowedType;
using ange::stowbase::Bundle;
using ange::stowbase::ReferenceObjectFactory;
using ange::stowbase::ReferenceObject;
using ange::stowbase::ContainerWriter;
using ange::stowbase::ContainerMoveWriter;
using ange::stowbase::ScheduleWriter;
using ange::stowbase::ContainerBundlingWriter;
using ange::schedule::Call;
using ange::schedule::Schedule;
using ange::containers::Container;
using ange::vessel::BayRowTier;

StowageWriter::StowageWriter(const document_t* document, const Call* currentCall)
    : m_document(document), m_currentCall(currentCall), m_futureWatcher(new QFutureWatcher<StowageWriter::SaveResult>(this))
{
    setObjectName("StowageWriter");
    connect(m_futureWatcher, SIGNAL(finished()), SLOT(saveJobFinished()));
}

static QByteArray fetchVersion1File() {
    return QByteArray("VERSION = 1\n");
}

static QByteArray fetchInfoFile(QString filename) {
    QSettings s;
    QString user = s.value("user").toString();
    return QString("SaveFileName = %1\nSaveDateTime = %2\nUser = %3\nProgramVersion = %4\n")
                  .arg(filename).arg(QDateTime::currentDateTime().toString(Qt::ISODate)).arg(user).arg(ANGESTOW_VERSION).toUtf8();
}

struct WriteData {
    QString filename;
    ange::stowbase::Bundle oldStoFileBundle;
    QByteArray vesselFileData;
    int currentCallIndex;
    int pivotCallIndex;
    QHash<int, ange::units::Density> customPortWaterDensities;
    QHash<int, ange::vessel::BlockWeight> deadLoads;
    QHash<int, ange::units::Length> forcedDrafts;
    QHash<int, ange::units::Length> forcedTrims;
    QHash<int, ange::units::Length> forcedGms;
};

static StowageWriter::SaveResult threadWriteZip1(WriteData data) {
    QFile file(data.filename);
    if (file.open(QIODevice::WriteOnly)) {
        KZip zipfile(&file);
        if (!zipfile.open(QIODevice::WriteOnly)) {
            return  StowageWriter::FileWriteError;
        }

        if (!zipfile.writeFile("META-INF/version.ini", fetchVersion1File())) {
            return StowageWriter::FileWriteError;
        }
        if (!zipfile.writeFile("META-INF/info.ini", fetchInfoFile(data.filename))) {
            return StowageWriter::FileWriteError;
        }
        {
            Q_ASSERT(!data.vesselFileData.isEmpty());
            if (!zipfile.writeFile("vessel/vessel.json", data.vesselFileData)) {
                return StowageWriter::FileWriteError;
            }
        }
        {
            QByteArray oldStoFileData;
            QBuffer oldStoFileBuffer(&oldStoFileData);
            if (!oldStoFileBuffer.open(QIODevice::WriteOnly)) {
                return StowageWriter::FileWriteError;
            }
            data.oldStoFileBundle.write_uncompressed(&oldStoFileBuffer);
            oldStoFileBuffer.close();
            if (!zipfile.writeFile("old_sto_file.json", oldStoFileData)) {
                return StowageWriter::FileWriteError;
            }
        }
        if (data.pivotCallIndex > 0) {
            QByteArray pivotCallData;
            QBuffer pivotCallBuffer(&pivotCallData);
            if (!pivotCallBuffer.open(QIODevice::WriteOnly)) {
                return StowageWriter::FileWriteError;
            }
            PivotCallWriter writer;
            writer.setPivotPortIndex(data.pivotCallIndex);
            writer.write(&pivotCallBuffer);
            pivotCallBuffer.close();
            if (!zipfile.writeFile("schedule/pivot_call.xml", pivotCallData)) {
                return StowageWriter::FileWriteError;
            }
        }
        if (data.currentCallIndex > 0) {
            QByteArray currentCallData;
            QBuffer currentCallBuffer(&currentCallData);
            if (!currentCallBuffer.open(QIODevice::WriteOnly)) {
                return StowageWriter::FileWriteError;
            }
            CurrentCallWriter writer;
            writer.setCurrentCallIndex(data.currentCallIndex);
            writer.write(&currentCallBuffer);
            currentCallBuffer.close();
            if (!zipfile.writeFile("application_state/application_state.xml", currentCallData)) {
                return StowageWriter::FileWriteError;
            }
        }
        if (!data.customPortWaterDensities.isEmpty()) {
            QByteArray customPortWaterDensitiesData;
            QBuffer customPortWaterDensitiesBuffer(&customPortWaterDensitiesData);
            if (!customPortWaterDensitiesBuffer.open(QIODevice::WriteOnly)) {
                return StowageWriter::FileWriteError;
            }
            CustomPortWaterDensityWriter writer;
            writer.setCustomPortWaterDensities(data.customPortWaterDensities);
            writer.write(&customPortWaterDensitiesBuffer);
            customPortWaterDensitiesBuffer.close();
            if (!zipfile.writeFile("stability/custom_port_water_densities.xml", customPortWaterDensitiesData)) {
                return StowageWriter::FileWriteError;
            }
        }
        if (!data.deadLoads.isEmpty() || !data.forcedDrafts.isEmpty() || !data.forcedTrims.isEmpty() || !data.forcedGms.isEmpty()) {
            QByteArray draftSurveyData;
            QBuffer draftSurveyBuffer(&draftSurveyData);
            if (!draftSurveyBuffer.open(QIODevice::WriteOnly)) {
                return StowageWriter::FileWriteError;
            }
            DraftSurveyWriter writer;
            writer.setDeadLoads(data.deadLoads);
            writer.setForcedDrafts(data.forcedDrafts);
            writer.setForcedTrims(data.forcedTrims);
            writer.setForcedGms(data.forcedGms);
            writer.write(&draftSurveyBuffer);
            draftSurveyBuffer.close();
            if (!zipfile.writeFile("stability/draftsurvey.xml", draftSurveyData)) {
                return StowageWriter::FileWriteError;
            }
        }
        zipfile.close();
        if (file.error() != QFile::NoError) {
            return StowageWriter::FileWriteError;
        }
    } else {
        return StowageWriter::FileOpenError;
    }
    return StowageWriter::NoError;
}

ange::stowbase::Bundle StowageWriter::convertToOldStoFileBundle() const {
    Bundle bundle;

    // Write schedule
    ScheduleWriter scheduleWriter;
    QList<Call*> calls = m_document->schedule()->calls();
    calls.removeFirst();
    calls.removeLast();
    Q_FOREACH(const Call * call, calls) {
        scheduleWriter.writeCall(bundle, call);
    }
    scheduleWriter.writeSchedule(bundle);

    // Write containers
    MovesReferences movesReferences = saveContainers(bundle, &scheduleWriter);

    // Write macro stowage
    QString macroStowageRef = MacroStowageWriter::write(bundle, m_document->macro_stowage(), m_document->vessel(),
                                                        m_document->schedule());

    // Write weight limits
    QVariantList bayWeightLimitsReferences;
    Q_FOREACH (const Call * call, m_document->schedule()->calls()) {
        Q_FOREACH (ange::vessel::BaySlice * bay_slice, m_document->vessel()->baySlices()) {
            const Mass weightLimit = m_document->bayWeightLimits()->weightLimitForBay(call, bay_slice);
            if (!std::isinf(weightLimit / kilogram)) {
                ReferenceObject ro(bundle.build("bayWeightLimit"));
                ro.insertDouble("limit", weightLimit / kilogram);
                ro.insertString("bayNumber", QString::number(bay_slice->joinedBay()));
                ro.insertReference("call", scheduleWriter.reference(call));
                bayWeightLimitsReferences << ro.reference();
                bundle << ro;
            }
        }
    }

    // Write tank conditions
    QVariantList tankConditionsReferences = writeTankConditions(bundle, m_document->vessel()->tanks(), scheduleWriter);

    // Build stowage object
    ReferenceObject stowageObject = bundle.build("stowage");
    stowageObject.insertReferences("moves", QVariant(movesReferences.moves).toList());
    stowageObject.insertReferences("containerBundlings", QVariant(movesReferences.bundleMoves).toList());
    stowageObject.insertReference("schedule", scheduleWriter.reference());
    if (!macroStowageRef.isEmpty()) {
        stowageObject.insertReference("macroStowage", macroStowageRef);
    }
    if (!tankConditionsReferences.isEmpty()) {
        stowageObject.insertReferences("tankConditions", tankConditionsReferences);
    }
    if (!bayWeightLimitsReferences.isEmpty()) {
        stowageObject.insertReferences("bayWeightLimits", bayWeightLimitsReferences);
    }
    bundle << stowageObject;

    return bundle;
}

void StowageWriter::write(QString filename) {
    writeAsync(filename);
    m_futureWatcher->waitForFinished();
    if (m_saveFuture.result() != NoError) {
        throw std::runtime_error(QString("Failed to open file '%1' for writing: %2").arg(filename).arg(errorMessage()).toStdString());
    }
}

void StowageWriter::writeAsync(QString filename) {
    Bundle oldStoFileBundle = convertToOldStoFileBundle();
    int currentCallIndex;
    if (m_currentCall) {
        currentCallIndex = m_document->schedule()->indexOf(m_currentCall);
    } else {
        currentCallIndex = -1;
    }
    int pivotCallIndex;
    if (m_document->schedule()->pivotCall()) {
        pivotCallIndex = m_document->schedule()->indexOf(m_document->schedule()->pivotCall());
    } else {
        pivotCallIndex = -1;
    }

    WriteData data;
    data.filename = filename;
    data.oldStoFileBundle = oldStoFileBundle;
    data.vesselFileData = m_document->vesselFileData();
    data.currentCallIndex = currentCallIndex;
    data.pivotCallIndex = pivotCallIndex;

    for(int i = 0 ; i < m_document->schedule()->size(); i++) {
        Call* call = m_document->schedule()->at(i);
        if(m_document->portWaterDensity()->hasCustomWaterDensity(call)) {
            data.customPortWaterDensities[i] = m_document->portWaterDensity()->waterDensity(call);
        }
        if(m_document->stabilityForcer()->deadLoadIsActive(call)) {
            data.deadLoads[i] = m_document->stabilityForcer()->deadLoad(call);
        }
        if(m_document->stabilityForcer()->forcedDraftTrimIsActive(call)) {
            data.forcedDrafts[i] = m_document->stabilityForcer()->draft(call);
            data.forcedTrims[i] = m_document->stabilityForcer()->trim(call);
        }
        if (m_document->stabilityForcer()->forcedGmIsActive(call)) {
            data.forcedGms[i] = m_document->stabilityForcer()->gm(call, qQNaN() * meter);
        }
    }

    m_saveFuture = QtConcurrent::run(threadWriteZip1, data);
    m_futureWatcher->setFuture(m_saveFuture);
}

StowageWriter::SaveResult StowageWriter::error() const {
    Q_ASSERT(m_saveFuture.isFinished());
    return m_saveFuture.result();
}

QStringList StowageWriter::saveMoves(Bundle& bundle, ContainerMoveWriter& moveWriter, const Container* container,
                                     const QString& containerReference) const {
    QStringList moves;
    BayRowTier currentPos;
    bool currentPosSet = false;
    const stowage_t* stowage = m_document->stowage();
    const ange::vessel::Vessel* vessel = m_document->vessel();
    const Call* dischargeCall = stowage->container_routes()->portOfDischarge(container);
    const Call* loadCall = stowage->container_routes()->portOfLoad(container);
    Q_FOREACH (container_move_t move, stowage->moves_inclusive_induced(container)) {
        const Call* call = is_befor_or_after(move.call()) ? 0L : move.call();
        QString stowageType;
        if (move.type() == MasterPlannedType) {
            stowageType = "macro-stowed";
        } else if (move.type() == MicroStowedType) {
            stowageType = "micro-stowed";
        }
        if (move.slot()) {  // Load or shift move
            if (currentPosSet) {
                // Note that for the nomiel code, a shift move would never be in the load/discharge port of the container and thus
                // the call's uncode is the correct one
                ReferenceObject shiftMove = moveWriter.buildDischarge(containerReference, vessel, currentPos, call, call->uncode());
                moves << shiftMove.reference();
                if (!stowageType.isEmpty()) {
                    shiftMove.insertString("stowageType", stowageType);
                }
                bundle << shiftMove;
            }
            const BayRowTier pos = stowage->nominal_position(container, move.call());
            QString uncode = (!call || call == loadCall) ? container->loadPort() : call->uncode();
            ReferenceObject loadMove = moveWriter.buildLoad(containerReference, vessel, pos, call, uncode);
            moves << loadMove.reference();
            if (!stowageType.isEmpty()) {
                loadMove.insertString("stowageType", stowageType);
            }
            bundle << loadMove;
            currentPos = pos;
            currentPosSet = true;
        } else {  // Discharge move
            QString uncode = (!call || call == dischargeCall) ? container->dischargePort() : call->uncode();
            ReferenceObject dischargeMove = moveWriter.buildDischarge(containerReference, vessel, currentPos, call, uncode);
            if (!stowageType.isEmpty()) {
                dischargeMove.insertString("stowageType", stowageType);
            }
            bundle << dischargeMove;
            moves << dischargeMove.reference();
            if (move.call() != dischargeCall) {
                // The container is partially unplanned. In the stowbase format, this means the container is loaded at an unknown position
                currentPos = BayRowTier();
                currentPosSet = true;
                // Again, this cannot happen at load or discharge port, and thus the call's code is correct
                ReferenceObject loadMove = moveWriter.buildLoad(containerReference, vessel, currentPos, call, call->uncode());
                if (!stowageType.isEmpty()) {
                    loadMove.insertString("stowageType", stowageType);
                }
                bundle << loadMove;
                moves <<  loadMove.reference();
            } else {
                currentPosSet = false;
            }
        }
    }
    return moves;
}

StowageWriter::MovesReferences StowageWriter::saveContainers(Bundle& bundle, const ScheduleWriter* scheduleWriter) const {
    MovesReferences refs;
    ContainerMoveWriter containerMoveWriter(bundle, scheduleWriter);
    ContainerWriter containerWriter(bundle);
    ContainerBundlingWriter bundlingWriter(bundle);
    Q_FOREACH (const Container* container, m_document->containers()->list()) {
        if (container->bundleParent()) {
            //we'll deal with the containers in the bundles
            //when we deal with the carrying container.
            continue;
        }
        if (!container->bundleChildren().isEmpty()) {
            QStringList bundledContainersReferences;
            Q_FOREACH (const Container* children, container->bundleChildren()) {
                QString bundledContainersReference = containerWriter(children);
                refs.moves << saveMoves(bundle, containerMoveWriter, children, bundledContainersReference);
                bundledContainersReferences << bundledContainersReference;
            }
            QString bottomContainerReference = containerWriter(container);
            refs.moves << saveMoves(bundle, containerMoveWriter, container, bottomContainerReference);
            refs.bundleMoves << bundlingWriter(bottomContainerReference, bundledContainersReferences);
        } else {
            QString reference = containerWriter(container);
            refs.moves << saveMoves(bundle, containerMoveWriter, container, reference);
        }
    }
    return refs;
}

QVariantList StowageWriter::writeTankConditions(Bundle& bundle, const QList<ange::vessel::VesselTank*> tanks,
                                                const ScheduleWriter& scheduleWriter) const {
    // Note that vessel_writer is only valid if tankConditionsWriteMode is useTankReferences. Else, it is invalid.
    QVariantList tankConditionRos;
    Q_FOREACH (const ange::schedule::Call* call, m_document->schedule()->calls()) {
        if (!is_befor_or_after(call)) {
            Q_FOREACH (ange::vessel::VesselTank* tank, tanks) {
                const Mass condition = m_document->tankConditions()->tankCondition(call, tank);
                if (condition > 0.0 * ton) {
                    QString callReference = scheduleWriter.reference(call);
                    ReferenceObject tankConditionRo = bundle.build("tankCondition");
                    tankConditionRo.insertReference("tank", tank->description());
                    tankConditionRo.insertReference("call", callReference);
                    tankConditionRo.insertDouble("condition", condition / kilogram);
                    tankConditionRos << tankConditionRo.reference();
                    bundle << tankConditionRo;
                }
            }
        }
    }
    return tankConditionRos;
}

QString StowageWriter::errorMessage() const {
    Q_ASSERT(m_saveFuture.isFinished());
    switch (m_saveFuture.result()) {
        case NoError:
            return QLatin1String("No error");
        case FileOpenError:
            return QLatin1String("Cannot open file for writing");
        case FileWriteError:
            return QLatin1String("Error writing file");
    }
    return QString();
}

void StowageWriter::saveJobFinished() {
    Q_ASSERT(m_saveFuture.isFinished());
    if (m_saveFuture.result() == NoError) {
        emit documentSaved();
    } else {
        emit documentSaveFailed();
    }
}

#include "stowagewriter.moc"
