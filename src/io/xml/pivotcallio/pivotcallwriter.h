#ifndef PIVOTPORTWRITER_H
#define PIVOTPORTWRITER_H

class QIODevice;

class PivotCallWriter {
    public:
        PivotCallWriter();
        void setPivotPortIndex(int index);
        bool write(QIODevice* device);
    private:
        int m_pivotPortIndex;
};

#endif // PIVOTPORTWRITER_H
