#include "pivotcallwriter.h"

#include "xmlutils.h"

#include <QXmlStreamReader>

PivotCallWriter::PivotCallWriter() : m_pivotPortIndex(-1) {
    // Empty
}

void PivotCallWriter::setPivotPortIndex(int index) {
    m_pivotPortIndex = index;
}

bool PivotCallWriter::write(QIODevice* device) {
    QXmlStreamWriter writer(device);
    writer.setAutoFormatting(true);
    writer.writeStartDocument();
    writer.writeStartElement("pivot_call");
    writer.writeAttribute("index", QString::number(m_pivotPortIndex));
    writer.writeEndElement();
    device->write("\n");
    return writer.hasError();
}
