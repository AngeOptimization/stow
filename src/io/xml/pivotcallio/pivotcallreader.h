#ifndef PIVOTPORTREADER_H
#define PIVOTPORTREADER_H

class QIODevice;

class PivotCallReader {
    public:
        PivotCallReader();
        int pivotPortIndex() const;
        bool parse(QIODevice* device);
    private:
        int m_pivotPortIndex;
};

#endif // PIVOTPORTREADER_H
