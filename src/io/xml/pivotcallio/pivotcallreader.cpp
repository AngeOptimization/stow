#include "pivotcallreader.h"

#include "xmlutils.h"

#include <QDebug>
#include <QXmlStreamReader>

PivotCallReader::PivotCallReader() : m_pivotPortIndex(-1) {
    // Empty
}

int PivotCallReader::pivotPortIndex() const {
    return m_pivotPortIndex;
}

bool PivotCallReader::parse(QIODevice* device) {
    QXmlStreamReader reader(device);

    int result = -1;
    bool success = false;
    int level = 0;

    while (!reader.atEnd()) {
        QXmlStreamReader::TokenType token = reader.readNext();
        if (token == QXmlStreamReader::StartDocument) {
            continue;
        }
        if (token == QXmlStreamReader::StartElement) {
            if (reader.name() == "pivot_call") {
                level++;
                if (reader.attributes().hasAttribute("index")) {
                    result = XmlUtils::parseStringRefAsInt(reader.attributes().value("index"), &success);
                } else {
                    success = false;
                }
            }
        } else if (token == QXmlStreamReader::EndElement) {
            if (reader.name() == "pivot_call") {
                level--;
            }
        }
    }
    if (reader.hasError()) {
        qDebug() << "has error";
        return false;
    }
    if (!success) {
        qDebug() << "!success";
        return false;
    }
    if (level != 0) {
        qDebug() << "level";
        return false;
    }

    m_pivotPortIndex = result;

    return true;
}
