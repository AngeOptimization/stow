
#include <QObject>
#include <QFile>
#include <QBuffer>
#include <QTest>
#include <pivotcallreader.h>
#include <pivotcallwriter.h>
#include <testfilecontents.h>
#include <test/util/extramacros.h>

class PivotCallReadWriteTest : public QObject {
    Q_OBJECT
public:
private Q_SLOTS:
    void readValidXml();
    void readInvalidXml();
    void readMalformedXml();
    void writeXml();
};

void PivotCallReadWriteTest::readInvalidXml() {
    QString filename = QFINDTESTDATA("data/pivot_call-invalid.xml");

    QFile f(filename);
    QVERIFY(f.open(QIODevice::ReadOnly));

    PivotCallReader c;
    QVERIFY(!c.parse(&f));
    QCOMPARE(c.pivotPortIndex(),-1);
}

void PivotCallReadWriteTest::readMalformedXml() {
    QString filename = QFINDTESTDATA("data/pivot_call-malformed.xml");

    QFile f(filename);
    QVERIFY(f.open(QIODevice::ReadOnly));

    PivotCallReader c;
    QVERIFY(!c.parse(&f));
    QCOMPARE(c.pivotPortIndex(),-1);
}

void PivotCallReadWriteTest::readValidXml() {
    QString filename = QFINDTESTDATA("data/pivot_call.xml");

    QFile f(filename);
    QVERIFY(f.open(QIODevice::ReadOnly));

    PivotCallReader c;
    QVERIFY(c.parse(&f));
    QCOMPARE(c.pivotPortIndex(),3);
}

void PivotCallReadWriteTest::writeXml() {
    QByteArray a;
    QBuffer b(&a);
    b.open(QIODevice::ReadWrite);

    PivotCallWriter w;
    w.setPivotPortIndex(3);
    w.write(&b);
    b.reset();

    QString expectedDataFile = QFINDTESTDATA("data/pivot_call.xml");
    QFile expectedData(expectedDataFile);
    QVERIFY(expectedData.open(QIODevice::ReadOnly));

    TestFileContents c;
    c.setControlData(&expectedData);
    c.setTestData(&b);
    ERUN(c.compare());
}

QTEST_GUILESS_MAIN(PivotCallReadWriteTest);
#include "pivotcallreadwritertest.moc"
