#ifndef CUSTOMPORTWATERDENSITYWRITER_H
#define CUSTOMPORTWATERDENSITYWRITER_H

#include <ange/units/units.h>
#include <QHash>

class QIODevice;

class CustomPortWaterDensityWriter {
    public:
        /**
         * \param densities a map with schedule index => custom water densities for ports with custom densities
         */
        void setCustomPortWaterDensities(QHash<int, ange::units::Density> densities);

        /**
         * \param device to write to
         * \return true on success, false on error
         */
        bool write(QIODevice* device);
    private:
        QHash<int, ange::units::Density> m_densities;
};

#endif // CUSTOMPORTWATERDENSITYWRITER_H
