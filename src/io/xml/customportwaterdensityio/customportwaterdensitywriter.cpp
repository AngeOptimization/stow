#include "customportwaterdensitywriter.h"

#include <QXmlStreamWriter>
#include <QIODevice>

void CustomPortWaterDensityWriter::setCustomPortWaterDensities(QHash< int, ange::units::Density > densities) {
    m_densities = densities;
}

bool CustomPortWaterDensityWriter::write(QIODevice* device) {
    QXmlStreamWriter writer(device);
    writer.setAutoFormatting(true);
    writer.writeStartDocument();
    writer.writeStartElement("custom_port_water_density");
    QList<int> keys = m_densities.keys();
    qSort(keys);
    Q_FOREACH(int key, keys) {
        writer.writeStartElement("call");
        writer.writeAttribute("index", QString::number(key));
        writer.writeAttribute("kilogram_per_meter3", QString::number(m_densities.value(key)/ange::units::kilogram_per_meter3));
        writer.writeEndElement();
    }
    writer.writeEndElement();
    device->write("\n");
    return writer.hasError();
}


