#include "customportwaterdensityreader.h"

#include "xmlutils.h"

#include <QXmlStreamReader>
#include <QDebug>

QHash< int, ange::units::Density > CustomPortWaterDensityReader::customPortWaterDensities() const {
    return m_customPortWaterDensities;
}

bool CustomPortWaterDensityReader::parse(QIODevice* device) {
    QXmlStreamReader reader(device);

    QHash<int, ange::units::Density> readValues;
    bool success = true;
    int level = 0;

    while (!reader.atEnd() && success) {
        QXmlStreamReader::TokenType token = reader.readNext();
        if (token == QXmlStreamReader::StartDocument) {
            continue;
        }
        if (token == QXmlStreamReader::StartElement) {
            if (reader.name() == "custom_port_water_density") {
                level++;
                continue;
            }
            if (reader.name() == "call") {
                level++;
                if (reader.attributes().hasAttribute("index") && reader.attributes().hasAttribute("kilogram_per_meter3")) {
                    int index = XmlUtils::parseStringRefAsInt(reader.attributes().value("index"), &success);
                    int densityValue = XmlUtils::parseStringRefAsInt(reader.attributes().value("kilogram_per_meter3"), &success);
                    ange::units::Density density = densityValue * ange::units::kilogram_per_meter3;
                    readValues[index] = density;
                } else {
                    success = false;
                }
            }
        } else if (token == QXmlStreamReader::EndElement) {
            if (reader.name() == "custom_port_water_density") {
                level--;
                continue;
            }
            if (reader.name() == "call") {
                level--;
            }
        }
    }
    if (reader.hasError()) {
        qDebug() << Q_FUNC_INFO << reader.errorString();
        return false;
    }
    if (!success) {
        qDebug() << Q_FUNC_INFO << "!success";
        return false;
    }
    if (level != 0) {
        qDebug() << Q_FUNC_INFO << level;
        return false;
    }

    m_customPortWaterDensities = readValues;

    return true;

}


