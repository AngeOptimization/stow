#ifndef CUSTOMPORTWATERDENSITYREADER_H
#define CUSTOMPORTWATERDENSITYREADER_H

#include <QHash>
#include <ange/units/units.h>

class QIODevice;

/**
 * Reader for the custom port water densities files
 */
class CustomPortWaterDensityReader {
    public:
        /**
         * \return custom portwater densities with a index => density. Note that it can be a empty list or a sparse list
         */
        QHash<int, ange::units::Density> customPortWaterDensities() const;
        /**
         * \param device to parse from
         * \return if parsing succeeded
         */
        bool parse(QIODevice* device);
    private:
        QHash<int, ange::units::Density> m_customPortWaterDensities;
};

#endif // CUSTOMPORTWATERDENSITYREADER_H
