#include <QTest>
#include <QTemporaryFile>
#include <document/document.h>
#include <document/portwaterdensity/portwaterdensity.h>
#include <fileopener.h>
#include <ange/schedule/schedule.h>
#include <ange/schedule/call.h>

using ange::schedule::Call;

/**
 * Tests opening a file, setting some custom water densities, saving, reopeninng and checks that the correct data is restored
 */

class CustomPortWaterDensityDocumentReadWriteTest : public QObject {
    Q_OBJECT
    private Q_SLOTS:
        void readWriteFile();
};

void CustomPortWaterDensityDocumentReadWriteTest::readWriteFile() {
    QTemporaryFile tmp;
    tmp.open();

    {
        document_t* document1 = FileOpener::openStoFile(QFINDTESTDATA("data/lego-maersk-with-containers.sto")).document;

        Q_FOREACH(Call* call, document1->schedule()->calls()) {
            QVERIFY(!document1->portWaterDensity()->hasCustomWaterDensity(call));
        }

        const Call* AAAAA = document1->schedule()->getCallByUncodeAfterCall(QStringLiteral("AAAAA"), document1->schedule()->at(0));
        QVERIFY(AAAAA);
        const Call* CCCCC = document1->schedule()->getCallByUncodeAfterCall(QStringLiteral("CCCCC"), document1->schedule()->at(0));
        QVERIFY(CCCCC);

        document1->portWaterDensity()->setCustomWaterDensity(AAAAA, 10000 * ange::units::kilogram_per_meter3);
        document1->portWaterDensity()->setCustomWaterDensity(CCCCC, 100 * ange::units::kilogram_per_meter3);

        Q_FOREACH(Call* call, document1->schedule()->calls()) {
            if(call == AAAAA || call == CCCCC) {
                QVERIFY(document1->portWaterDensity()->hasCustomWaterDensity(call));
            } else {
                QVERIFY(!document1->portWaterDensity()->hasCustomWaterDensity(call));
            }
        }
        document1->save_stowage(tmp.fileName(),CCCCC);

        delete document1;
    }

    {
        document_t* document2 = FileOpener::openStoFile(tmp.fileName()).document;

        const Call* AAAAA = document2->schedule()->getCallByUncodeAfterCall(QStringLiteral("AAAAA"), document2->schedule()->at(0));
        QVERIFY(AAAAA);
        const Call* CCCCC = document2->schedule()->getCallByUncodeAfterCall(QStringLiteral("CCCCC"), document2->schedule()->at(0));
        QVERIFY(CCCCC);

        Q_FOREACH(const Call* call, document2->schedule()->calls()) {
            if(call == AAAAA || call == CCCCC) {
                QVERIFY(document2->portWaterDensity()->hasCustomWaterDensity(call));
            } else {
                QVERIFY(!document2->portWaterDensity()->hasCustomWaterDensity(call));
            }
        }
        QCOMPARE(document2->portWaterDensity()->waterDensity(AAAAA)/ange::units::kilogram_per_meter3, 10000.0);
        QCOMPARE(document2->portWaterDensity()->waterDensity(CCCCC)/ange::units::kilogram_per_meter3, 100.0);
        delete document2;
    }

    tmp.remove();
}


QTEST_GUILESS_MAIN(CustomPortWaterDensityDocumentReadWriteTest);
#include "customportwaterdensitydocumentreadwritetest.moc"
