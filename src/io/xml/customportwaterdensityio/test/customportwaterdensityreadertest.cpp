#include <QTest>

#include <customportwaterdensityreader.h>

#include <QFile>

/**
 * Checks reading of the xml files with custom port water density
 */
class CustomPortWaterDensityReaderTest : public QObject {
    Q_OBJECT
    private Q_SLOTS:
        void readValidXml2Calls();
        void readValidXml0Calls();
        void readInvalidXml2Calls();
};

void CustomPortWaterDensityReaderTest::readValidXml2Calls() {
    QString filename = QFINDTESTDATA("data/valid2calls.xml");

    QFile f(filename);
    QVERIFY(f.open(QIODevice::ReadOnly));

    CustomPortWaterDensityReader r;
    QVERIFY(r.parse(&f));
    QHash<int, ange::units::Density> densities = r.customPortWaterDensities();

    QCOMPARE(densities.size(),2);
    QVERIFY(densities.contains(2));
    QVERIFY(densities.contains(5));
    QVERIFY(!densities.contains(3));

    QCOMPARE(densities.value(2)/ange::units::kilogram_per_meter3,980.0);
    QCOMPARE(densities.value(5)/ange::units::kilogram_per_meter3,1050.0);
}

void CustomPortWaterDensityReaderTest::readValidXml0Calls() {
    QString filename = QFINDTESTDATA("data/valid0calls.xml");

    QFile f(filename);
    QVERIFY(f.open(QIODevice::ReadOnly));

    CustomPortWaterDensityReader r;
    QVERIFY(r.parse(&f));
    QHash<int, ange::units::Density> densities = r.customPortWaterDensities();

    QVERIFY(densities.isEmpty());

}

void CustomPortWaterDensityReaderTest::readInvalidXml2Calls() {
    QString filename = QFINDTESTDATA("data/invalid2calls.xml");

    QFile f(filename);
    QVERIFY(f.open(QIODevice::ReadOnly));

    CustomPortWaterDensityReader r;
    QVERIFY(!r.parse(&f));
    QHash<int, ange::units::Density> densities = r.customPortWaterDensities();
    QVERIFY(densities.isEmpty());

}




QTEST_GUILESS_MAIN(CustomPortWaterDensityReaderTest);
#include "customportwaterdensityreadertest.moc"
