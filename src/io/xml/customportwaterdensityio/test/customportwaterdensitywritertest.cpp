#include <QTest>
#include <customportwaterdensitywriter.h>
#include <QBuffer>
#include <QByteArray>
#include <QFile>
#include <testfilecontents.h>
#include <test/util/extramacros.h>

/**
 * Tests writing the custom port water densities
 */

class CustomPortWaterDensityWriterTest : public QObject {
    Q_OBJECT
    private Q_SLOTS:
        void writeNoCustom();
        void writeTwoCalls();
};

void CustomPortWaterDensityWriterTest::writeNoCustom() {
    CustomPortWaterDensityWriter w;

    QByteArray data;
    QBuffer b(&data);
    b.open(QIODevice::ReadWrite);


    w.write(&b);
    b.reset();

    QString expectedDataFile = QFINDTESTDATA("data/valid0calls.xml");
    QFile expectedData(expectedDataFile);
    QVERIFY(expectedData.open(QIODevice::ReadOnly));

    TestFileContents c;
    c.setControlData(&expectedData);
    c.setTestData(&b);
    ERUN(c.compare());

}

void CustomPortWaterDensityWriterTest::writeTwoCalls() {
    CustomPortWaterDensityWriter w;

    QHash<int, ange::units::Density> densities;
    densities[2] = 980 * ange::units::kilogram_per_meter3;
    densities[5] = 1050 * ange::units::kilogram_per_meter3;

    w.setCustomPortWaterDensities(densities);

    QByteArray data;
    QBuffer b(&data);
    b.open(QIODevice::ReadWrite);

    w.write(&b);
    b.reset();

    QString expectedDataFile = QFINDTESTDATA("data/valid2calls.xml");
    QFile expectedData(expectedDataFile);
    QVERIFY(expectedData.open(QIODevice::ReadOnly));

    TestFileContents c;
    c.setControlData(&expectedData);
    c.setTestData(&b);
    ERUN(c.compare());

}

QTEST_GUILESS_MAIN(CustomPortWaterDensityWriterTest)
#include "customportwaterdensitywritertest.moc"
