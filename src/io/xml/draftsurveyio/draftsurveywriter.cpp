#include "draftsurveywriter.h"

#include <ange/vessel/blockweight.h>

#include <QXmlStreamWriter>
#include <QSet>

using ange::units::meter;
using ange::units::Length;
using ange::units::ton;
using ange::vessel::BlockWeight;

void DraftSurveyWriter::setForcedDrafts(QHash< int, Length > forcedDrafts) {
    m_forcedDrafts = forcedDrafts;
}

void DraftSurveyWriter::setForcedTrims(QHash< int, Length > forcedTrims) {
    m_forcedTrims = forcedTrims;
}

void DraftSurveyWriter::setDeadLoads(QHash< int, BlockWeight > deadLoads) {
    m_deadLoads = deadLoads;
}

void DraftSurveyWriter::setForcedGms(QHash< int, Length > forcedGms) {
    m_forcedGms = forcedGms;
}

bool DraftSurveyWriter::write(QIODevice* device) {
    QXmlStreamWriter writer(device);
    writer.setAutoFormatting(true);
    writer.writeStartDocument();
    writer.writeStartElement("draftsurvey");

    QSet<int> keys; // filter double entries out and sort
    keys += m_forcedDrafts.keys().toSet();
    keys += m_forcedTrims.keys().toSet();
    keys += m_deadLoads.keys().toSet();
    keys += m_forcedGms.keys().toSet();
    QList<int> keyList = keys.toList();
    qSort(keyList);

    Q_FOREACH(int key, keyList) {
        writer.writeStartElement("call");
        writer.writeAttribute("index", QString::number(key));

        if (m_deadLoads.contains(key)) {
            BlockWeight block = m_deadLoads.value(key);
            writer.writeStartElement("deadload");
            writer.writeAttribute("weight_ton", QString::number(block.weight()/ton));
            writer.writeAttribute("aft_meter", QString::number(block.aftLimit()/meter));
            writer.writeAttribute("fore_meter", QString::number(block.foreLimit()/meter));
            writer.writeAttribute("lcg_meter", QString::number(block.lcg()/meter));
            writer.writeAttribute("tcg_meter", QString::number(block.tcg()/meter));
            writer.writeAttribute("vcg_meter", QString::number(block.vcg()/meter));
            writer.writeAttribute("description", block.description());
            writer.writeEndElement(); // "deadload"
        }

        if (m_forcedDrafts.contains(key) || m_forcedTrims.contains(key) || m_forcedGms.contains(key)) {
            writer.writeStartElement("forced");
            if (m_forcedDrafts.contains(key)) {
                writer.writeAttribute("draft_meter", QString::number(m_forcedDrafts.value(key)/meter));
            }
            if (m_forcedTrims.contains(key)) {
                writer.writeAttribute("trim_meter", QString::number(m_forcedTrims.value(key)/meter));
            }
            if (m_forcedGms.contains(key)) {
                writer.writeAttribute("gm_meter", QString::number(m_forcedGms.value(key)/meter));
            }
            writer.writeEndElement(); // "forced"
        }

        writer.writeEndElement(); // "call"
    }
    writer.writeEndElement();
    device->write("\n");
    return writer.hasError();
}


