#include <QTest>
#include <draftsurveyreader.h>

#include <QFile>

using ange::units::Length;
using ange::units::meter;
using ange::units::millimeter;
using ange::units::kilogram;
using ange::units::ton;
using ange::vessel::BlockWeight;

/**
 * Checks reading of the per-call draftsurvey XML file
 */
class DraftSurveyReaderTest : public QObject {
    Q_OBJECT
    private Q_SLOTS:
        void readValidXml1Call();
        void readValidXml2Calls();
        void readInvalidXml2Calls();
        void readMalformedXml();
};

void DraftSurveyReaderTest::readValidXml1Call() {
    QString filename = QFINDTESTDATA("data/draftsurvey_1call.xml");

    QFile f(filename);
    QVERIFY(f.open(QIODevice::ReadOnly));

    DraftSurveyReader r;
    QVERIFY(r.parse(&f));

    {
        QHash<int, BlockWeight> deadLoads = r.deadLoads();

        QCOMPARE(deadLoads.size(), 1);
        QVERIFY(!deadLoads.contains(1));
        QVERIFY(deadLoads.contains(2));
        QVERIFY(!deadLoads.contains(3));

        QVERIFY(qAbs((deadLoads.value(2).weight() - 203*ton)/kilogram) < 1.0);
        QVERIFY(qAbs((deadLoads.value(2).aftLimit() + 40.005*meter)/millimeter) < 1.0);
        QVERIFY(qAbs((deadLoads.value(2).foreLimit() - 20.995*meter)/millimeter) < 1.0);
        QVERIFY(qAbs((deadLoads.value(2).lcg() + 13.785*meter)/millimeter) < 1.0);
        QVERIFY(qAbs((deadLoads.value(2).tcg() - 0.0*meter)/millimeter) < 1.0);
        QVERIFY(qAbs((deadLoads.value(2).vcg() - 8.05*meter)/millimeter) < 1.0);
    }

    {
        QHash<int, Length> forcedDrafts = r.forcedDrafts();
        QCOMPARE(forcedDrafts.size(), 1);
        QVERIFY(!forcedDrafts.contains(1));
        QVERIFY(forcedDrafts.contains(2));
        QVERIFY(!forcedDrafts.contains(3));

        QVERIFY(qAbs((forcedDrafts.value(2) - 12.345*meter)/millimeter) < 1.0);
    }

    {
        QHash<int, Length> forcedTrims = r.forcedTrims();
        QCOMPARE(forcedTrims.size(), 1);
        QVERIFY(!forcedTrims.contains(1));
        QVERIFY(forcedTrims.contains(2));
        QVERIFY(!forcedTrims.contains(3));

        QVERIFY(qAbs((forcedTrims.value(2) + 2.345*meter)/millimeter) < 1.0);
    }
}

void DraftSurveyReaderTest::readValidXml2Calls() {
    QString filename = QFINDTESTDATA("data/draftsurvey_2call.xml");

    QFile f(filename);
    QVERIFY(f.open(QIODevice::ReadOnly));

    DraftSurveyReader r;
    QVERIFY(r.parse(&f));

    {
        QHash<int, BlockWeight> deadLoads = r.deadLoads();

        QCOMPARE(deadLoads.size(), 2);
        QVERIFY(!deadLoads.contains(1));
        QVERIFY(deadLoads.contains(2));
        QVERIFY(!deadLoads.contains(3));
        QVERIFY(!deadLoads.contains(4));
        QVERIFY(deadLoads.contains(5));
        QVERIFY(!deadLoads.contains(6));

        QVERIFY(qAbs((deadLoads.value(2).weight() - 203*ton)/kilogram) < 1.0);
        QVERIFY(qAbs((deadLoads.value(2).aftLimit() + 40.005*meter)/millimeter) < 1.0);
        QVERIFY(qAbs((deadLoads.value(2).foreLimit() - 20.995*meter)/millimeter) < 1.0);
        QVERIFY(qAbs((deadLoads.value(2).lcg() + 13.785*meter)/millimeter) < 1.0);
        QVERIFY(qAbs((deadLoads.value(2).tcg() - 0.0*meter)/millimeter) < 1.0);
        QVERIFY(qAbs((deadLoads.value(2).vcg() - 8.05*meter)/millimeter) < 1.0);

        // this is a weired, but legal blockweight with negative weight and LCG outside aft-fore interval
        QVERIFY(qAbs((deadLoads.value(5).weight() + 345.678*ton)/kilogram) < 1.0);
        QVERIFY(qAbs((deadLoads.value(5).aftLimit() - 20.341*meter)/millimeter) < 1.0);
        QVERIFY(qAbs((deadLoads.value(5).foreLimit() - 56.784*meter)/millimeter) < 1.0);
        QVERIFY(qAbs((deadLoads.value(5).lcg() + 39.876*meter)/millimeter) < 1.0);
        QVERIFY(qAbs((deadLoads.value(5).tcg() - 0.0*meter)/millimeter) < 1.0);
        QVERIFY(qAbs((deadLoads.value(5).vcg() - 4.567*meter)/millimeter) < 1.0);
    }

    {
        QHash<int, Length> forcedDrafts = r.forcedDrafts();
        QCOMPARE(forcedDrafts.size(), 2);
        QVERIFY(!forcedDrafts.contains(1));
        QVERIFY(!forcedDrafts.contains(2));
        QVERIFY(forcedDrafts.contains(3));
        QVERIFY(!forcedDrafts.contains(4));
        QVERIFY(forcedDrafts.contains(5));

        QVERIFY(qAbs((forcedDrafts.value(3) - 12.345*meter)/millimeter) < 1.0);
        QVERIFY(qAbs((forcedDrafts.value(5) - 12.345*meter)/millimeter) < 1.0);
    }

    {
        QHash<int, Length> forcedTrims = r.forcedTrims();
        QCOMPARE(forcedTrims.size(), 2);
        QVERIFY(!forcedTrims.contains(1));
        QVERIFY(forcedTrims.contains(2));
        QVERIFY(forcedTrims.contains(3));
        QVERIFY(!forcedTrims.contains(4));
        QVERIFY(!forcedTrims.contains(5));

        QVERIFY(qAbs((forcedTrims.value(2) + 2.345*meter)/millimeter) < 1.0);
        QVERIFY(qAbs((forcedTrims.value(3) + 2.345*meter)/millimeter) < 1.0);
    }

    {
        QHash<int, Length> forcedGms = r.forcedGms();
        QCOMPARE(forcedGms.size(), 1);
        QVERIFY(forcedGms.contains(6));
        QCOMPARE(forcedGms.value(6) / meter, 2.66);
    }
}

void DraftSurveyReaderTest::readInvalidXml2Calls() {
    QString filename = QFINDTESTDATA("data/draftsurvey_invalid.xml");

    QFile f(filename);
    QVERIFY(f.open(QIODevice::ReadOnly));

    DraftSurveyReader r;
    QVERIFY(!r.parse(&f));
}

void DraftSurveyReaderTest::readMalformedXml() {
    QString filename = QFINDTESTDATA("data/draftsurvey_malformed.xml");

    QFile f(filename);
    QVERIFY(f.open(QIODevice::ReadOnly));

    DraftSurveyReader r;
    QVERIFY(!r.parse(&f));
}

QTEST_GUILESS_MAIN(DraftSurveyReaderTest);
#include "draftsurveyreadertest.moc"
