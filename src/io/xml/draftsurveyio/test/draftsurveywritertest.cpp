#include <QTest>
#include <draftsurveywriter.h>
#include <QBuffer>
#include <QByteArray>
#include <QFile>
#include <testfilecontents.h>
#include <test/util/extramacros.h>


using ange::units::meter;
using ange::units::Length;
using ange::units::Mass;
using ange::units::ton;
using ange::vessel::BlockWeight;

/**
 * Tests writing the per-call draftsurvey data
 */

class DraftSurveyWriterTest : public QObject {
    Q_OBJECT
    private Q_SLOTS:
        void write1Call();
        void write2Calls();
};

void DraftSurveyWriterTest::write1Call() {

    DraftSurveyWriter w;

    {
        Length lcg2 = -13.785*meter;
        Length aftLimit2 = -40.005*meter;
        Length foreLimit2 = 20.995*meter;
        Length vcg2 = 8.05*meter;
        Length tcg2 = 0.0*meter;
        Mass weight2 = 203*ton;
        BlockWeight deadLoad2(lcg2, aftLimit2, foreLimit2, vcg2, tcg2, weight2);
        deadLoad2.setDescription("Deadload weight adjustment");

        QHash<int, BlockWeight> deadLoads;
        deadLoads[2] = deadLoad2;

        w.setDeadLoads(deadLoads);
    }

    {
        QHash<int, Length> forcedDrafts;
        forcedDrafts[2] = 12.345*meter;
        w.setForcedDrafts(forcedDrafts);
    }

    {
        QHash<int, Length> forcedTrims;
        forcedTrims[2] = -2.345*meter;
        w.setForcedTrims(forcedTrims);
    }

    {
        QHash<int, Length> forcedGms;
        forcedGms[2] = 2.88*meter;
        w.setForcedGms(forcedGms);
    }

    QByteArray data;
    QBuffer b(&data);
    b.open(QIODevice::ReadWrite);

    w.write(&b);
    b.reset();

    QString expectedDataFile = QFINDTESTDATA("data/draftsurvey_1call.xml");
    QFile expectedData(expectedDataFile);
    QVERIFY(expectedData.open(QIODevice::ReadOnly));

    TestFileContents c;
    c.setControlData(&expectedData);
    c.setTestData(&b);
    ERUN(c.compare());

}

void DraftSurveyWriterTest::write2Calls() {

    DraftSurveyWriter w;

    {
        Length lcg2 = -13.785*meter;
        Length aftLimit2 = -40.005*meter;
        Length foreLimit2 = 20.995*meter;
        Length vcg2 = 8.05*meter;
        Length tcg2 = 0.0*meter;
        Mass weight2 = 203*ton;
        BlockWeight deadLoad2(lcg2, aftLimit2, foreLimit2, vcg2, tcg2, weight2);
        deadLoad2.setDescription("Deadload weight adjustment");

        // this is a weired, but legal blockweight with negative weight and LCG outside aft-fore interval
        Length lcg5 = -39.876*meter;
        Length aftLimit5 = 20.341*meter;
        Length foreLimit5 = 56.784*meter;
        Length vcg5 = 4.567*meter;
        Length tcg5 = 0.0*meter;
        Mass weight5 = -345.678*ton;
        BlockWeight deadLoad5(lcg5, aftLimit5, foreLimit5, vcg5, tcg5, weight5);
        deadLoad5.setDescription("Deadload weight adjustment");

        QHash<int, BlockWeight> deadLoads;
        deadLoads[2] = deadLoad2;
        deadLoads[5] = deadLoad5;

        w.setDeadLoads(deadLoads);
    }

    {
        QHash<int, Length> forcedDrafts;
        forcedDrafts[3] = 12.345*meter;
        forcedDrafts[5] = 12.345*meter;
        w.setForcedDrafts(forcedDrafts);
    }

    {
        QHash<int, Length> forcedTrims;
        forcedTrims[2] = -2.345*meter;
        forcedTrims[3] = -2.345*meter;
        w.setForcedTrims(forcedTrims);
    }

    {
        QHash<int, Length> forcedGms;
        forcedGms[6] = 2.66*meter;
        w.setForcedGms(forcedGms);
    }

    QByteArray data;
    QBuffer b(&data);
    b.open(QIODevice::ReadWrite);

    w.write(&b);
    b.reset();

    QString expectedDataFile = QFINDTESTDATA("data/draftsurvey_2call.xml");
    QFile expectedData(expectedDataFile);
    QVERIFY(expectedData.open(QIODevice::ReadOnly));

    TestFileContents c;
    c.setControlData(&expectedData);
    c.setTestData(&b);
    ERUN(c.compare());
}

QTEST_GUILESS_MAIN(DraftSurveyWriterTest)
#include "draftsurveywritertest.moc"
