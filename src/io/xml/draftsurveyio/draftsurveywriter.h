#ifndef DRAFTSURVEYWRITER_H
#define DRAFTSURVEYWRITER_H

#include <ange/vessel/blockweight.h>
#include <ange/units/units.h>

#include <QHash>

class QIODevice;

class DraftSurveyWriter {

    public:

        /**
         * \param forcedDrafs a map with schedule index => drafts for ports with forced drafts
         */
        void setForcedDrafts(QHash< int, ange::units::Length > forcedDrafts);

        /**
         * \param forcedTrims a map with schedule index => trims for ports with forced trims
         */
        void setForcedTrims(QHash< int, ange::units::Length > forcedTrims);

        /**
         * \param deadLoads a map with schedule index => blockweights for ports with deadload blockweights
         */
        void setDeadLoads(QHash<int, ange::vessel::BlockWeight> deadLoads);

        /**
         * \param forcedGms a map with schedule index => GM for ports with forced GM
         */
        void setForcedGms(QHash< int, ange::units::Length> forcedGms);

        /**
         * \param device to write to
         * \return true on success, false on error
         */
        bool write(QIODevice* device);

    private:

        QHash< int, ange::units::Length > m_forcedDrafts;

        QHash< int, ange::units::Length > m_forcedTrims;

        QHash<int, ange::vessel::BlockWeight> m_deadLoads;

        QHash< int, ange::units::Length > m_forcedGms;

};

#endif // DRAFTSURVEYWRITER_H
