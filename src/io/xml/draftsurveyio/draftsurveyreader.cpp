#include "draftsurveyreader.h"

#include "xmlutils.h"

#include <ange/units/units.h>

#include <QDebug>
#include <QXmlStreamReader>

using ange::units::Length;
using ange::units::Mass;
using ange::units::MassMoment;
using ange::units::meter;
using ange::units::ton;
using ange::units::tonmeter;
using ange::vessel::BlockWeight;


QHash< int, Length > DraftSurveyReader::forcedDrafts() const {
    return m_forcedDrafts;
}

QHash< int, Length > DraftSurveyReader::forcedTrims() const {
    return m_forcedTrims;
}

QHash< int, BlockWeight > DraftSurveyReader::deadLoads() const {
    return m_deadLoads;
}

QHash< int, Length > DraftSurveyReader::forcedGms() const {
    return m_forcedGms;
}

bool DraftSurveyReader::parse(QIODevice* device) {
    QXmlStreamReader reader(device);

    QHash<int, BlockWeight> readDeadLoads;
    QHash<int, Length> readForcedDrafts;
    QHash<int, Length> readForcedTrims;
    QHash<int, Length> readForcedGms;

    bool success = true;
    int level = 0;
    int index = 0;
    while (!reader.atEnd() && success) {
        QXmlStreamReader::TokenType token = reader.readNext();

        switch (token) {
            case QXmlStreamReader::StartDocument: {
                continue;
            }
            case QXmlStreamReader::StartElement: {
                if (reader.name() == "draftsurvey") {
                    level++;
                    continue;
                } else if (reader.name() == "call" && reader.attributes().hasAttribute("index")) {
                    level++;
                    if (!success) { continue; }
                    index = XmlUtils::parseStringRefAsInt(reader.attributes().value("index"), &success);
                    continue;
                } else if (reader.name() == "deadload") {
                    level++;
                    if (!success) { continue; }
                    Mass weight = XmlUtils::parseStringRefAsDouble(reader.attributes().value("weight_ton"), &success) * ton;
                    if (!success) { continue; }
                    Length aftLimit = XmlUtils::parseStringRefAsDouble(reader.attributes().value("aft_meter"), &success) * meter;
                    if (!success) { continue; }
                    Length foreLimit = XmlUtils::parseStringRefAsDouble(reader.attributes().value("fore_meter"), &success) * meter;
                    if (!success) { continue; }
                    Length lcg = XmlUtils::parseStringRefAsDouble(reader.attributes().value("lcg_meter"), &success) * meter;
                    if (!success) { continue; }
                    Length tcg = XmlUtils::parseStringRefAsDouble(reader.attributes().value("tcg_meter"), &success) * meter;
                    if (!success) { continue; }
                    Length vcg = XmlUtils::parseStringRefAsDouble(reader.attributes().value("vcg_meter"), &success) * meter;
                    if (!success) { continue; }
                    QString description = XmlUtils::parseStringRefAsString(reader.attributes().value("description"));
                    if (!success) { continue; }

                    BlockWeight deadLoad(lcg, aftLimit, foreLimit, vcg, tcg, weight);
                    deadLoad.setDescription(description);
                    readDeadLoads[index] = deadLoad;
                    continue;
                } else if (reader.name() == "forced") {
                    level++;
                    if (reader.attributes().hasAttribute("draft_meter")) {
                        Length forcedDraft = XmlUtils::parseStringRefAsDouble(reader.attributes().value("draft_meter"), &success) * meter;
                        if (!success) { continue; }
                        readForcedDrafts[index] = forcedDraft;
                    }
                    if (reader.attributes().hasAttribute("trim_meter")) {
                        Length forcedTrim = XmlUtils::parseStringRefAsDouble(reader.attributes().value("trim_meter"), &success) * meter;
                        if (!success) { continue; }
                        readForcedTrims[index] = forcedTrim;
                    }
                    if (reader.attributes().hasAttribute("gm_meter")) {
                        Length forcedGm = XmlUtils::parseStringRefAsDouble(reader.attributes().value("gm_meter"), &success) * meter;
                        if (!success) { continue; }
                        readForcedGms[index] = forcedGm;
                    }
                    continue;
                } else {
                    success = false;
                    continue;
                }
            }
            case QXmlStreamReader::EndElement: {
                if (reader.name() == "draftsurvey") {
                    level--;
                    continue;
                } else if (reader.name() == "call") {
                    level--;
                    continue;
                } else if (reader.name() == "deadload") {
                    level--;
                    continue;
                } else if (reader.name() == "forced") {
                    level--;
                    continue;
                } else {
                    success = false;
                    continue;
                }
            }
            default: {
                continue;
            }
        }
    } // while

    if (reader.hasError()) {
        qDebug() << Q_FUNC_INFO << reader.errorString();
        return false;
    }
    if (!success) {
        qDebug() << Q_FUNC_INFO << "!success";
        return false;
    }
    if (level != 0) {
        qDebug() << Q_FUNC_INFO << level;
        return false;
    }

    m_forcedDrafts = readForcedDrafts;
    m_forcedTrims = readForcedTrims;
    m_deadLoads = readDeadLoads;
    m_forcedGms = readForcedGms;

    return true;

}


