#ifndef DRAFTSURVEYREADER_H
#define DRAFTSURVEYREADER_H

#include <ange/vessel/blockweight.h>
#include <ange/units/units.h>

#include <QHash>

class QIODevice;

/**
 * Reader for per-call draftsurvey data
 */
class DraftSurveyReader {

    public:

        /**
         * \return forced trims with a call index => draft
         */
        QHash<int, ange::units::Length> forcedDrafts() const;

        /**
         * \return forced trims with a call index => trim
         */
        QHash<int, ange::units::Length> forcedTrims() const;

        /**
         * \return deadload blockweights with a call index => blockweight
         */
        QHash<int, ange::vessel::BlockWeight> deadLoads() const;

        /**
         * \return forced GM with a call index => gm
         */
        QHash<int, ange::units::Length> forcedGms() const;

        /**
         * \param device to parse from
         * \return if parsing succeeded
         */
        bool parse(QIODevice* device);

    private:

        QHash<int, ange::units::Length> m_forcedDrafts;

        QHash<int, ange::units::Length> m_forcedTrims;

        QHash<int, ange::vessel::BlockWeight> m_deadLoads;

        QHash<int, ange::units::Length> m_forcedGms;

};

#endif // DRAFTSURVEYREADER_H
