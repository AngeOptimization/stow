#ifndef XMLUTILS_H
#define XMLUTILS_H

#include <QXmlStreamReader>

namespace XmlUtils {

inline int parseCharactersAsInt(QXmlStreamReader* reader, bool* success = 0) {
    QXmlStreamReader::TokenType token = reader->readNext();
    if(token != QXmlStreamReader::Characters) {
        if(success) {
            *success = false;
        }
        return 0;
    }
    QString data = reader->text().toString();
    return data.toInt(success);
}

inline int parseStringRefAsInt(const QStringRef& stringref, bool* success = 0) {
    QString string = stringref.toString();
    return string.toInt(success);
}

inline double parseStringRefAsDouble(const QStringRef& stringref, bool* success = 0) {
    QString string = stringref.toString();
    return string.toDouble(success);
}

inline QString parseStringRefAsString(const QStringRef& stringref) {
    return stringref.toString();
}

}

#endif // XMLUTILS_H
