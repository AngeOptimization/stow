
#include <QObject>
#include <QFile>
#include <QBuffer>
#include <QTest>
#include <currentcallreader.h>
#include <currentcallwriter.h>
#include <testfilecontents.h>
#include <test/util/extramacros.h>

class CurrentCallReadWriteTest : public QObject {
    Q_OBJECT
    public:
    private Q_SLOTS:
        void readValidXml();
        void readInvalidXml();
        void readMalformedXml();
        void writeXml();
};

void CurrentCallReadWriteTest::readInvalidXml() {
    QString filename = QFINDTESTDATA("data/application_state-invalid.xml");

    QFile f(filename);
    QVERIFY(f.open(QIODevice::ReadOnly));

    CurrentCallReader c;
    QVERIFY(!c.parse(&f));
    QCOMPARE(c.currentCallIndex(), -1);
}

void CurrentCallReadWriteTest::readMalformedXml() {
    QString filename = QFINDTESTDATA("data/application_state-malformed.xml");

    QFile f(filename);
    QVERIFY(f.open(QIODevice::ReadOnly));

    CurrentCallReader c;
    QVERIFY(!c.parse(&f));
    QCOMPARE(c.currentCallIndex(), -1);
}

void CurrentCallReadWriteTest::readValidXml() {
    QString filename = QFINDTESTDATA("data/application_state.xml");

    QFile f(filename);
    QVERIFY(f.open(QIODevice::ReadOnly));

    CurrentCallReader c;
    QVERIFY(c.parse(&f));
    QCOMPARE(c.currentCallIndex(), 8);
}

void CurrentCallReadWriteTest::writeXml() {

    QByteArray a;
    QBuffer b(&a);
    b.open(QIODevice::ReadWrite);

    CurrentCallWriter w;
    w.setCurrentCallIndex(8);
    w.write(&b);
    b.reset();

    QString expectedDataFile = QFINDTESTDATA("data/application_state.xml");
    QFile expectedData(expectedDataFile);
    QVERIFY(expectedData.open(QIODevice::ReadOnly));

    TestFileContents c;
    c.setControlData(&expectedData);
    c.setTestData(&b);
    ERUN(c.compare());
}

QTEST_GUILESS_MAIN(CurrentCallReadWriteTest);
#include "currentcallreadwritertest.moc"
