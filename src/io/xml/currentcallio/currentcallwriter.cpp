#include "currentcallwriter.h"

#include "xmlutils.h"

#include <QXmlStreamReader>

CurrentCallWriter::CurrentCallWriter() : m_currentCallIndex(-1) {
    // Empty
}

void CurrentCallWriter::setCurrentCallIndex(int index) {
    m_currentCallIndex = index;
}

bool CurrentCallWriter::write(QIODevice* device) {
    QXmlStreamWriter writer(device);
    writer.setAutoFormatting(true);
    writer.writeStartDocument();
    writer.writeStartElement("application_state");
    writer.writeStartElement("current_call");
    writer.writeAttribute("index", QString::number(m_currentCallIndex));
    writer.writeEndElement();
    writer.writeEndElement();
    device->write("\n");
    return writer.hasError();
}
