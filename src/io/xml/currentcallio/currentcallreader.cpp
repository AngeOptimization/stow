#include "currentcallreader.h"

#include "xmlutils.h"

#include <QXmlStreamReader>

CurrentCallReader::CurrentCallReader() : m_currentCallIndex(-1) {
    // Empty
}

int CurrentCallReader::currentCallIndex() const {
    return m_currentCallIndex;
}

bool CurrentCallReader::parse(QIODevice* device) {
    QXmlStreamReader reader(device);

    int result = -1;
    bool success = false;
    int level = 0;

    while (!reader.atEnd()) {
        QXmlStreamReader::TokenType token = reader.readNext();
        if (token == QXmlStreamReader::StartDocument) {
            continue;
        }
        if (token == QXmlStreamReader::StartElement) {
            if (reader.name() == "application_state") {
                level++;
                continue;
            }
            if (reader.name() == "current_call") {
                level++;
                if (reader.attributes().hasAttribute("index")) {
                    result = XmlUtils::parseStringRefAsInt(reader.attributes().value("index"), &success);
                } else {
                    success = false;
                }
            }
        } else if (token == QXmlStreamReader::EndElement) {
            if (reader.name() == "application_state") {
                level--;
                continue;
            }
            if (reader.name() == "current_call") {
                level--;
            }
        }
    }
    if (reader.hasError()) {
        return false;
    }
    if (!success) {
        return false;
    }
    if (level != 0) {
        return false;
    }

    m_currentCallIndex = result;

    return true;
}
