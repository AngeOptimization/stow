#ifndef CURRENTCALLREADER_H
#define CURRENTCALLREADER_H

class QIODevice;

class CurrentCallReader {

public:

    CurrentCallReader();

    int currentCallIndex() const;

    bool parse(QIODevice* device);

private:
    int m_currentCallIndex;

};

#endif // CURRENTCALLREADER_H
