#ifndef CURRENTCALLWRITER_H
#define CURRENTCALLWRITER_H

class QIODevice;

class CurrentCallWriter {

public:

    CurrentCallWriter();

    void setCurrentCallIndex(int index);

    bool write(QIODevice* device);

private:
    int m_currentCallIndex;

};

#endif // CURRENTCALLWRITER_H
