Notes on XML (RelaxNG) schema generation and checking
=====================================================

Debian packages
---------------

 * jing - RELAX NG validator
 * trang - XML schema generator and converter - XML -> RELAX NG - XML 1.0 DTD - W3C XML Schema (write only)
 * libxml2-utils - command line XML utilities containing xmllint, a tool for validating and reformatting XML documents,


Construct initial RelaxNG compact schema
----------------------------------------

    trang */schedule/pivot_call.xml pivot_call.rnc
    trang */application_state/application_state.xml application_state.rnc

inspect and edit if need be, in this case we did not allow negative index attribute values

    xsd:integer  --> xsd:nonNegativeInteger


Convert  RelaxNG compact schema to  RelaxNG XML schema
------------------------------------------------------

convert RelaxNG compact format to equivalent RelaxNG XML format (RelaxNG compact is much nicer for humans and git!)

    trang pivot_call.rnc pivot_call.rng
    trang application_state.rnc application_state.rng


Check/validate xml against RelaxNG XML schema using jing or xmllint
-------------------------------------------------------------------

    for xml in */application_state/application_state.xml ; do
        echo -n "$xml ";
        jing application_state.rng $xml && echo -n "OK";
        echo "";
    done

    for xml in */*/pivot_call.xml ; do
        echo -n "$xml ";
        xmllint --noout --relaxng pivot_call.rng $xml && echo -n "OK";
        echo "";
    done
