
#include "macrostowagereader.h"

#include <ange/vessel/vessel.h>
#include <ange/vessel/compartment.h>
#include <ange/vessel/bayslice.h>
#include <ange/vessel/stack.h>

#include <ange/schedule/schedule.h>
#include <ange/schedule/call.h>

#include <ange/stowbase/bundle.h>

#include "document/document.h"
#include "document/stowage/stowage.h"
#include "stowplugininterface/macroplan.h"

using ange::stowbase::Bundle;
using ange::stowbase::ReferenceObject;
using ange::vessel::Compartment;
using ange::vessel::DeckLevel;
using ange::vessel::Vessel;
using ange::schedule::Call;
using ange::schedule::Schedule;

static Compartment* parseCompartment(const ReferenceObject& macroStowageLocation, const Vessel* vessel) {
    int bayNo;
    int row;
    DeckLevel level;
    QString imo;
    QString urn = macroStowageLocation.readMandatoryValue<QStringList>("stacks").first();
    ange::stowbase::urn_to_vessel_stack_position(urn, imo, bayNo, row, level);
    Compartment* compartment = 0L;
    if (const ange::vessel::BaySlice* slice = vessel->baySliceContaining(bayNo)) {
        compartment = slice->compartmentContainingRow(row, level);
    }
    return compartment;
}

static const Call* parseLoadCall(const ReferenceObject& macroStowageLocation, const QVariantList& calls, const Schedule* schedule) {
    QVariantList loadRefs = macroStowageLocation.readOptionalValue<QVariantList>("loadScheduleEntry");
    QVariant loadRef = loadRefs.isEmpty() ? QVariant() : loadRefs.front();
    int loadIndex = loadRef.isValid() ? calls.indexOf(loadRef) : 0;
    if (loadIndex == -1) {
        qWarning("Unknown loadScheduleEntry %s in macroLocation %s", loadRef.toString().toLocal8Bit().data(),
                 macroStowageLocation.reference().toLocal8Bit().data());
    }
    return schedule->calls().at(loadIndex + 1);
}


static const Call* parseDischargeCall(const QVariant& dischargeRef, const QVariantList& calls, const Schedule* schedule) {
    int dischargeIndex = dischargeRef.isValid() ? calls.indexOf(dischargeRef) : 0;
    if (dischargeIndex == -1) {
        qWarning("Unknown allowedDischargeEntry %s", dischargeRef.toString().toLocal8Bit().data());
    }
    return schedule->calls().at(dischargeIndex + 1);
}

void MacroStowageReader::read(const Bundle& bundle,
                              const ReferenceObject& macroStowageRo,
                              const ReferenceObject& scheduleRo,
                              document_t* document) {
    QVariantList calls = scheduleRo.readMandatoryValue<QVariantList>("entries");
    Q_FOREACH(ReferenceObject macroStowageLocation, macroStowageRo.readAllReferencedObjects("locations", bundle)) {
        Compartment* compartment = parseCompartment(macroStowageLocation, document->vessel());
        if (!compartment) {
            qWarning("No compartment for ??? in macroLocation %s", macroStowageLocation.reference().toLocal8Bit().data());
            continue;
        }

        const Call* load = parseLoadCall(macroStowageLocation, calls, document->schedule());

        QVariantList allowedDischargeEntries = macroStowageLocation.readMandatoryValue<QVariantList>("allowedDischargeEntries");
        Q_ASSERT(allowedDischargeEntries.count() == 1);
        Q_FOREACH(QVariant dischargeRef, allowedDischargeEntries) {
            const Call* discharge = parseDischargeCall(dischargeRef, calls, document->schedule());
            if (load->distance(discharge) <= 0) {
                qWarning("Allowing macro stowage from %s->%s makes no sense", load->uncode().toLocal8Bit().data(),
                         discharge->uncode().toLocal8Bit().data());
                continue;
            }
            if (document->macro_stowage()->conflicts(compartment, load, discharge) == ange::angelstow::MacroPlan::NoConflicts) {
                document->macro_stowage()->insertAllocation(compartment, load, discharge); // Only insert if it is ok
            } else {
                // The current save format saves the stowage AAA->DDD as AAA->DDD, BBB->DDD, CCC->DDD.
                // Ignore this special case silently and complaint with qWarning+Q_ASSERT about other cases
                const Call* existingLoad = document->macro_stowage()->loadCall(compartment, load);
                const Call* existingDischarge = document->macro_stowage()->dischargeCall(compartment, load);
                if (!(discharge == existingDischarge && existingLoad && existingLoad->lessThan(load))) {
                    qWarning() << "Strange data in macro stowage" << compartment << load << discharge
                               << existingLoad << existingDischarge;
//                                << document->macro_stowage()->sequenceForCompartment(compartment); TODO: reenable this
                    Q_ASSERT(false);
                }
            }
        }
    }
}
