#ifndef DOCUMENTLOADER_H
#define DOCUMENTLOADER_H

#include <QObject>

#include <stowplugininterface/stowtype.h>

class DocumentLoaderPrivate;
class QIODevice;
class QUndoStack;
class document_t;
class stowage_t;namespace ange {
namespace angelstow {
class IDocument;
class IStower;
class MasterPlanningPlugin;
class ValidatorPlugin;
}
namespace containers {
class Container;
class EquipmentNumber;
}
namespace schedule {
class Schedule;
}
}

/**
 * Simple class to load a document and expose the relevant interface class.
 * Provided for test cases in external plugins.
 *
 * DocumentLoader must exist in the lifetime of the document below.
 * Usual restrictions regarding istowing and idocument apply
 */
class DocumentLoader : public QObject {

    Q_OBJECT

public:

    DocumentLoader(QObject* parent = 0);
    ~DocumentLoader();

    /**
     * Load plugin and no document
     */
    void loadPlugin(const QString& filePath);

    /**
     * Loads document from the given iodevice.
     * Returns true on success, false on failure.
     */
    bool load(QIODevice* device);

    /**
     * Loads document from the given file.
     * Returns true on success, false on failure.
     */
    bool load(const QString& fileName);

    /**
     * Loads' document from given @param device and installs
     * the plugin from @param pluginPath
     * @return true on success, false on failure
     */
    bool load(QIODevice* device, QString pluginPath);

    ange::angelstow::IDocument* idocument();
    const ange::angelstow::IDocument* idocument() const;

    ange::angelstow::IStower* createIStower();

    /**
     * @return opaque pointer with the document loaded
     */
    document_t* document();

    /**
     * @return opaque pointer with the stowage of the document
     */
    stowage_t* stowage();

    /**
     * @return a non-const schedule. Document interface has a const schedule pointer for the normal cases.
     */
    ange::schedule::Schedule* schedule();

    /**
     * @return the loaded validatorplugins
     */
    QList<ange::angelstow::ValidatorPlugin*> validatorPlugins();

    /**
     * @return the loaded stowage plugin or 0 if no stowage plugin loaded
     */
    ange::angelstow::MasterPlanningPlugin* masterPlanningPlugin();

    /**
     * @return the container with equipment number @param eqNumber
     * Consider moving this function to idocument_t if it turns out to be relevant for anything but testing
     */
    const ange::containers::Container* containerByEquipmentId(const ange::containers::EquipmentNumber& eqNumber) const;

    /**
     * The undo stack from the document
     */
    QUndoStack* undoStack();

private:

    QScopedPointer<DocumentLoaderPrivate> d;

};

#endif // DOCUMENTLOADER_H
