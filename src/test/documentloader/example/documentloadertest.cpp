#include <test/documentloader.h>
#include <QApplication>
#include <QDebug>
#include <QFile>

int main(int argc, char** argv) {
    QApplication app(argc, argv);

    DocumentLoader loader;
    QFile f("/dev/null");
    f.open(QIODevice::ReadOnly);
    if(loader.load(&f)) {
        ange::angelstow::idocument_t* doc  = loader.idocument();
        qDebug() << doc;
    } else {
        qDebug() << "loading failed";
    }
    
}
