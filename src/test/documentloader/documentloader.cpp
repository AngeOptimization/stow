#include "documentloader.h"

#include "container_list.h"
#include "document.h"
#include "document_interface.h"
#include "pluginmanager.h"
#include "stowage.h"
#include "stowagereader.h"

#include <ange/containers/equipmentnumber.h>

#include <QUndoStack>

using ange::containers::Container;

class DocumentLoaderPrivate {
    public:
        DocumentLoaderPrivate() : document(0), pluginManager(new PluginManager()) {

        }
        document_t* document;
        PluginManager* pluginManager;
};

DocumentLoader::DocumentLoader(QObject* parent) : QObject(parent), d(new DocumentLoaderPrivate) {
    setObjectName("DocumentLoader");
    Q_INIT_RESOURCE(data);
    Q_INIT_RESOURCE(icons);
    Q_INIT_RESOURCE(widgets);
}

DocumentLoader::~DocumentLoader() {
    delete d->document;
    delete d->pluginManager;
}

ange::angelstow::IDocument* DocumentLoader::idocument() {
    return d->document->documentInterface();
}

const ange::angelstow::IDocument* DocumentLoader::idocument() const {
    return d->document->documentInterface();
}

ange::angelstow::IStower* DocumentLoader::createIStower() {
    return d->document->stowage()->create_stowing();
}

bool DocumentLoader::load(QIODevice* device) {
    StowageReader reader;
    OnDiskData data = reader.read(device,"Test file", this);
    d->document = data.document;
    d->document->setPluginManager(d->pluginManager);
    return d->document != 0;
}

bool DocumentLoader::load(const QString& fileName) {
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly)) {
        return false;
    }
    return load(&file);
}

void DocumentLoader::loadPlugin(const QString& filePath) {
    d->pluginManager->loadPlugin(filePath);
}

bool DocumentLoader::load(QIODevice* device, QString pluginpath) {
    loadPlugin(pluginpath);
    bool loadResult = load(device);
    return loadResult;
}

document_t* DocumentLoader::document() {
    Q_ASSERT_X(d->document,"","No document loaded");
    return d->document;
}

stowage_t* DocumentLoader::stowage() {
    Q_ASSERT_X(d->document,"","No document loaded");
    return d->document->stowage();
}

ange::schedule::Schedule* DocumentLoader::schedule() {
    Q_ASSERT_X(d->document,"","No document loaded");
    return d->document->schedule();
}

QList< ange::angelstow::ValidatorPlugin* > DocumentLoader::validatorPlugins() {
    Q_ASSERT_X(d->pluginManager, "", "No pluginmanager around. Did you load with the edition with plugin?");
    return d->pluginManager->validatorPlugins();
}

ange::angelstow::MasterPlanningPlugin* DocumentLoader::masterPlanningPlugin() {
    Q_ASSERT_X(d->pluginManager, "", "No pluginmanager around. Did you load with the edition with plugin?");
    return d->pluginManager->masterPlanningPlugin();
}

const Container* DocumentLoader::containerByEquipmentId(const ange::containers::EquipmentNumber& eqNumber) const {
    return d->document->containers()->get_container_by_equipment_number(eqNumber);
}

QUndoStack* DocumentLoader::undoStack() {
    return d->document->undostack();
}

#include "documentloader.moc"
