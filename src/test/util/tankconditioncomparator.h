#ifndef TEST_TANKCONDITIONS_COMPARATOR
#define TEST_TANKCONDITIONS_COMPARATOR

#include "tankconditions.h"
#include "schedulecomparator.h"

#include <ange/schedule/schedule.h>
#include <ange/vessel/vesseltank.h>

class TankConditionsComparator {
    public:
        TankConditionsComparator(TankConditions* conditions1, ange::schedule::Schedule* schedule1, TankConditions* conditions2, ange::schedule::Schedule* schedule2) :
            m_conditions1(conditions1), m_schedule1(schedule1), m_conditions2(conditions2), m_schedule2(schedule2) {

        }
        void compare() {
            /**
             * We need equal schedules to test for equal tank conditios
             */
            ScheduleComparator schedulecomparator(m_schedule1, m_schedule2);
            schedulecomparator.compareAll();

            for(int i = 0; i < m_schedule1->size(); i++) {
                QHash<const ange::vessel::VesselTank*, ange::units::Mass> conditions1atcall = m_conditions1->tankConditions(m_schedule1->at(i));
                QHash<const ange::vessel::VesselTank*, ange::units::Mass> conditions2atcall = m_conditions2->tankConditions(m_schedule2->at(i));

                QMap<QString, double> conditions1comparable = convertHash(conditions1atcall);
                QMap<QString, double> conditions2comparable = convertHash(conditions2atcall);

                QList<QString> keys1 = conditions1comparable.keys();
                QList<QString> keys2 = conditions2comparable.keys();
                EQCOMPARE(keys1.size(), keys2.size());

                for(int j = 0 ; j < keys1.size(); j++) {
                    EQCOMPARE(keys1.at(i), keys2.at(i));
                    EQCOMPARE(conditions1comparable.value(keys1.at(i)), conditions2comparable.value(keys2.at(i)));
                }
            }
        }
    private:
        TankConditions* m_conditions1;
        ange::schedule::Schedule* m_schedule1;
        TankConditions* m_conditions2;
        ange::schedule::Schedule* m_schedule2;
        static QMap<QString, double> convertHash(const QHash<const ange::vessel::VesselTank*, ange::units::Mass> conditions) {
            Q_UNUSED(conditions);
             QMap<QString,double> converted;
            for(QHash<const ange::vessel::VesselTank*, ange::units::Mass>::const_iterator it = conditions.constBegin(); it!= conditions.constEnd(); it++) {
                converted.insert(it.key()->description(), it.value() / ange::units::kilogram);
            }
            return converted;
        }

};

#endif // TEST_TANKCONDITIONS_COMPARATOR
