#ifndef VESSELTESTUTILS_H
#define VESSELTESTUTILS_H

#include "document/document.h"
#include "document/undo/container_list_change_command.h"
#include "document/undo/container_stow_command.h"
#include "document/stowage/container_move.h"
#include "document/stowage/container_leg.h"
#include "document/stowage/stowage.h"
#include "document/containers/container_list.h"
#include "io/vesselio/multivesselreader.h"

#include <ange/containers/container.h>
#include <ange/vessel/vessel.h>
#include <ange/vessel/slot.h>
#include <ange/schedule/call.h>
#include <ange/schedule/schedule.h>

#include <QFile>

/*
 * Utilities for writing simpler tests on vessels with containers on
 */

using namespace ange::units;

static inline ange::vessel::Vessel* loadVessel(QString fileName) {
    MultiVesselReader reader;
    QFile file(fileName);
    bool open = file.open(QIODevice::ReadOnly);
    Q_ASSERT(open);
    Q_UNUSED(open);
    ange::vessel::Vessel* vessel = reader.readVessel(&file);
    Q_ASSERT(vessel);
    Q_UNUSED(vessel);
    return vessel;
}

/**
 * Creates a 40' container and loads it on the vessel for the entire planned trip
 */
static inline ange::containers::Container* createContainerOnboard(document_t* document, ange::schedule::Call* from,
                                                             ange::schedule::Call* to, const ange::vessel::Slot* slot) {
    QString name = QString("%1>%2@%3").arg(from->uncode()).arg(to->uncode()).arg(slot->brt().toString());
    ange::containers::Container* newContainer =
        new ange::containers::Container(ange::containers::EquipmentNumber(name), 10 * ton,
                                          ange::containers::IsoCode("42G1"));

    container_list_change_command_t* listChangeCommand = new container_list_change_command_t(document);
    listChangeCommand->add_container(newContainer, from, to);
    document->undostack()->push(listChangeCommand);
    ange::containers::Container* container = document->containers()->get_container_by_id(newContainer->id());
    delete newContainer;
    stowage_t* stowage = document->stowage();
    Q_ASSERT(!stowage->container_moved_at_call(container, from));
    Q_ASSERT(!stowage->container_moved_at_call(container, to));
    Q_UNUSED(stowage);

    container_stow_command_t* stowCommand = new container_stow_command_t(document);
    stowCommand->add_stow(container, slot, from, to, ange::angelstow::MicroStowedType);
    document->undostack()->push(stowCommand);
    Q_ASSERT(stowage->container_moved_at_call(container, from));
    Q_ASSERT(stowage->container_moved_at_call(container, from)->slot() == slot);
    Q_ASSERT(stowage->container_moved_at_call(container, to));
    Q_ASSERT(stowage->container_moved_at_call(container, to)->slot() == (void*)0);

    return container;
}

/**
 * Build the schedule: "Befor", "CALL1", "CALL2", "CALL3", "After" if given numberOfCalls=5
 */
static inline ange::schedule::Schedule* buildSchedule(int numberOfCalls) {
    QList<ange::schedule::Call*> calls;
    calls << new ange::schedule::Call(QStringLiteral("Befor"), "");
    for (int i = 1; i < numberOfCalls - 1; ++i) {
        calls << new ange::schedule::Call(QStringLiteral("CALL%1").arg(i), "");
    }
    calls << new ange::schedule::Call(QStringLiteral("After"), "");
    return new ange::schedule::Schedule(calls);
}

#endif // VESSELTESTUTILS_H
