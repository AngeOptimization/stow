#ifndef VESSEL_COMPARATOR_H
#define VESSEL_COMPARATOR_H
#include "extramacros.h"
#include <ange/vessel/vessel.h>
#include <ange/vessel/vesseltank.h>
#include <ange/vessel/bilinearinterpolator.h>
#include <ange/vessel/blockweight.h>
#include <ange/units/units.h>

/**
 * test helper class for comparing two vessels
 * Note that function should be run inside ERUN to figure out if they work
 */


class VesselComparator {
    public:
        VesselComparator(const ange::vessel::Vessel* vessel1, const ange::vessel::Vessel* vessel2) : m_vessel1(vessel1), m_vessel2(vessel2) {

        }
        /**
         * Runs all compare functions sequentially
         */
        void compareAll() {
            compareGeneralVesselInfo();
            compareHull();

        }
        /**
         * Compares general vessel info, like imocode and name and such
         */
        void compareGeneralVesselInfo() {
            EQCOMPARE(m_vessel1->imoNumber(), m_vessel2->imoNumber());
            EQCOMPARE(m_vessel1->name(), m_vessel2->name());
            EQCOMPARE(m_vessel1->callSign(), m_vessel2->callSign());
            EQCOMPARE(m_vessel1->vesselCode(), m_vessel2->vesselCode());
            EQCOMPARE(m_vessel1->classificationSociety(), m_vessel2->classificationSociety());
        }
        /**
         * Compares the hull shape
         */
        void compareHull() {
            using ange::vessel::Vessel;
            using ange::vessel::VesselTank;
            using ange::vessel::BlockWeight;
            using ange::units::kilogram;
            using ange::units::meter;
            Q_FOREACH(qreal x, m_vessel1->trimData().xCoordinates()) {
                Q_FOREACH(qreal y, m_vessel1->trimData().yCoordinates()) {
                    EQCOMPARE(m_vessel1->trim(x*kilogram,y*meter, Vessel::oceanWaterDensity), m_vessel2->trim(x*kilogram,y*meter, Vessel::oceanWaterDensity));
                }
            }
            Q_FOREACH(qreal x, m_vessel1->draftData().xCoordinates()) {
                Q_FOREACH(qreal y, m_vessel1->draftData().yCoordinates()) {
                    EQCOMPARE(m_vessel1->draft(x*kilogram,y*meter, Vessel::oceanWaterDensity), m_vessel2->draft(x*kilogram,y*meter, Vessel::oceanWaterDensity));
                }
            }
            for(int i = 0; i < m_vessel1->tanks().size(); ++i) {
                VesselTank* tank1 = m_vessel1->tanks().at(i);
                VesselTank* tank2 = m_vessel2->tanks().at(i);
                Q_FOREACH(qreal x, tank1->lcgData().xCoordinates()) {
                    EQCOMPARE(tank1->lcg(x*kilogram), tank2->lcg(x*kilogram));
                }
                Q_FOREACH(qreal x, tank1->vcgData().xCoordinates()) {
                    EQCOMPARE(tank1->vcg(x*kilogram), tank2->vcg(x*kilogram));
                }
                Q_FOREACH(qreal x, tank1->tcgData().xCoordinates()) {
                    EQCOMPARE(tank1->tcg(x*kilogram), tank2->tcg(x*kilogram));
                }
                Q_FOREACH(qreal x, tank1->fsmData().xCoordinates()) {
                    EQCOMPARE(tank1->fsm(x*kilogram), tank2->fsm(x*kilogram));
                }
                EQCOMPARE(tank1->aftEnd(), tank2->aftEnd());
                EQCOMPARE(tank1->foreEnd(), tank2->foreEnd());
                EQCOMPARE(tank1->capacity(), tank2->capacity());
                EQCOMPARE(tank1->description(), tank2->description());
                EQCOMPARE(tank1->isBallast(), tank2->isBallast());
            }
            Q_FOREACH(const QString& tankGroup, m_vessel1->tankGroups()) {
                EQVERIFY(m_vessel2->tankGroups().contains(tankGroup));
            }
            Q_FOREACH(const QString& tankGroup, m_vessel2->tankGroups()) {
                EQVERIFY(m_vessel1->tankGroups().contains(tankGroup));
            }
            Q_FOREACH(qreal x, m_vessel1->maxBendingLimitsData().xCoordinates()) {
                EQCOMPARE(m_vessel1->maxBendingLimit(x*meter), m_vessel2->maxBendingLimit(x*meter));
            }
            Q_FOREACH(qreal x, m_vessel1->minBendingLimitsData().xCoordinates()) {
                EQCOMPARE(m_vessel1->minBendingLimit(x*meter), m_vessel2->minBendingLimit(x*meter));
            }
            Q_FOREACH(qreal x, m_vessel1->maxShearForceLimitsData().xCoordinates()) {
                EQCOMPARE(m_vessel1->maxShearForceLimit(x*meter), m_vessel2->maxShearForceLimit(x*meter));
            }
            Q_FOREACH(qreal x, m_vessel1->minShearForceLimitsData().xCoordinates()) {
                EQCOMPARE(m_vessel1->minShearForceLimit(x*meter), m_vessel2->minShearForceLimit(x*meter));
            }
            Q_FOREACH(qreal x, m_vessel1->maxTorsionLimitsData().xCoordinates()) {
                EQCOMPARE(m_vessel1->maxTorsionLimit(x*meter), m_vessel2->maxTorsionLimit(x*meter));
            }
            Q_FOREACH(qreal x, m_vessel1->minTorsionLimitsData().xCoordinates()) {
                EQCOMPARE(m_vessel1->minTorsionLimit(x*meter), m_vessel2->minTorsionLimit(x*meter));
            }
            Q_FOREACH(qreal x, m_vessel1->bonjeanCurves().xCoordinates()) {
                Q_FOREACH(qreal y, m_vessel1->bonjeanCurves().yCoordinates()) {
                    EQCOMPARE(m_vessel1->bonjean(x*meter,y*meter), m_vessel2->bonjean(x*meter,y*meter));
                }
            }
            Q_FOREACH(qreal x, m_vessel1->metacenterFunction().xCoordinates()) {
                Q_FOREACH(qreal y, m_vessel1->metacenterFunction().yCoordinates()) {
                    EQCOMPARE(m_vessel1->metacenter(x*meter, y*meter), m_vessel2->metacenter(x*meter, y*meter));
                }
            }
            if(!std::isnan(m_vessel1->observerLcg()/meter) || !std::isnan(m_vessel2->observerLcg()/meter)) {
                EQCOMPARE(m_vessel1->observerLcg()/meter, m_vessel2->observerLcg()/meter);
            }
            if(!std::isnan(m_vessel1->observerVcg()/meter) || !std::isnan(m_vessel2->observerVcg()/meter)) {
                EQCOMPARE(m_vessel1->observerVcg()/meter, m_vessel2->observerVcg()/meter);
            }
            if(!std::isnan(m_vessel1->lpp()/meter) || !std::isnan(m_vessel2->lpp()/meter)) {
                EQCOMPARE(m_vessel1->lpp()/meter, m_vessel2->lpp()/meter);
            }
            EQCOMPARE(m_vessel1->lightshipWeight(), m_vessel2->lightshipWeight());
            EQCOMPARE(m_vessel1->breadth(), m_vessel2->breadth());
            EQCOMPARE(m_vessel1->hullLcg(), m_vessel2->hullLcg());
            EQCOMPARE(m_vessel1->hullVcg(), m_vessel2->hullVcg());
            for(int i = 0; i < m_vessel1->lightshipWeights().size(); ++i) {
                BlockWeight blockWeight1 = m_vessel1->lightshipWeights().at(i);
                BlockWeight blockWeight2 = m_vessel2->lightshipWeights().at(i);
                EQCOMPARE(blockWeight1.aftLimit(), blockWeight2.aftLimit());
                EQCOMPARE(blockWeight1.foreLimit(), blockWeight2.foreLimit());
                EQCOMPARE(blockWeight1.foreDensity()/ange::units::kilogram_per_meter, blockWeight2.foreDensity()/ange::units::kilogram_per_meter);
                EQCOMPARE(blockWeight1.aftDensity()/ange::units::kilogram_per_meter, blockWeight2.aftDensity()/ange::units::kilogram_per_meter);
            }
            for(int i = 0; i < m_vessel1->constantWeights().size(); ++i) {
                BlockWeight blockWeight1 = m_vessel1->constantWeights().at(i);
                BlockWeight blockWeight2 = m_vessel2->constantWeights().at(i);
                EQCOMPARE(blockWeight1.aftLimit(), blockWeight2.aftLimit());
                EQCOMPARE(blockWeight1.foreLimit(), blockWeight2.foreLimit());
                EQCOMPARE(blockWeight1.foreDensity()/ange::units::kilogram_per_meter, blockWeight2.foreDensity()/ange::units::kilogram_per_meter);
                EQCOMPARE(blockWeight1.aftDensity()/ange::units::kilogram_per_meter, blockWeight2.aftDensity()/ange::units::kilogram_per_meter);
            }
        }
    private:
        const ange::vessel::Vessel* m_vessel1;
        const ange::vessel::Vessel* m_vessel2;
};


#endif // VESSEL_COMPARATOR_H
