#ifndef TEST_SCHEDULECOMPARATOR
#define TEST_SCHEDULECOMPARATOR
#include <ange/schedule/schedule.h>
#include "extramacros.h"
#include <ange/schedule/call.h>

class ScheduleComparator {
    public:
        ScheduleComparator(ange::schedule::Schedule* schedule1, ange::schedule::Schedule* schedule2) : m_schedule1(schedule1), m_schedule2(schedule2) {

        }
        void compareAll() {
            compareLength();
            compareCalls();
            comparePivot();
        }
        void compareLength() {
            EQCOMPARE(m_schedule1->size(), m_schedule2->size());
        }
        void comparePivot() {
            compareCall(m_schedule1->pivotCall(), m_schedule2->pivotCall());
        }
        void compareCalls() {
            compareLength();
            for(int i = 0 ; i < m_schedule1->size(); i++) {
                ange::schedule::Call* call1 = m_schedule1->at(i);
                ange::schedule::Call* call2 = m_schedule2->at(i);
                compareCall(call1, call2);
            }
        }
        static void compareCall(const ange::schedule::Call* call1, const ange::schedule::Call* call2) {
            using std::isnan;
            if(call1 == call2 ) {
                return;
            }
            EQVERIFY(call1);
            EQVERIFY(call2);

            EQCOMPARE(call1->uncode(), call2->uncode());
            EQCOMPARE(call1->voyageCode(), call2->voyageCode());
            //Note: EQCOMPARE will never return true for nan == nan, so we need to work around
            if(isnan(call1->cranes()) && isnan(call2->cranes())) {
                QVERIFY(true);
            }else {
                EQCOMPARE(call1->cranes(), call2->cranes());
            }
            EQCOMPARE(call1->craneRule(), call2->craneRule());
            EQCOMPARE(call1->eta(), call2->eta());
            EQCOMPARE(call1->etd(), call2->etd());
            if(isnan(call1->craneProductivity()) && isnan(call2->craneProductivity())) {
                QVERIFY(true);
            }else {
                EQCOMPARE(call1->craneProductivity(), call2->craneProductivity());
            }
        }
    private:
        ange::schedule::Schedule* m_schedule1;
        ange::schedule::Schedule* m_schedule2;
};

#endif // TEST_SCHEDULECOMPARATOR
