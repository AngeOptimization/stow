#ifndef TEST_EXTRAMACROS
#define TEST_EXTRAMACROS

#include <iostream>
#include <QtTest/QTest>

class TestException {
    public:
        TestException(const char* what) : m_what(what) {}
        const char* what() const { return m_what.constData(); }
        virtual ~TestException() { };
        virtual void report() const = 0;
    private:
        QByteArray m_what;
};

class TestVerifyException : public TestException {
    public:
        TestVerifyException(bool statement, const char *statementStr, const char *description,
                       const char *file, int line) : TestException(qPrintable(QString("%1 %2 ").arg(statementStr).arg(description))),
                       m_statement(statement), m_statementStr(statementStr), m_description(description), m_file(file), m_line(line) {
        }
        virtual void report() const {
            QTest::qVerify(m_statement, m_statementStr.constData(), m_description.constData(), m_file.constData(), m_line);
        }
    private:
        bool m_statement;
        QByteArray m_statementStr;
        QByteArray m_description;
        QByteArray m_file;
        int m_line;
};

class TestCompareException : public TestException {
    public:
        TestCompareException(bool success,
                             char *val1, char *val2,
                             const char *actual, const char *expected,
                             const char *file, int line) : TestException(qPrintable(QString("%1 %2").arg(val1).arg(val2))),
                             m_success(success), m_val1(val1), m_val2(val2), m_actual(actual), m_expected(expected),
                             m_file(file), m_line(line) {
                             }
        virtual void report() const {
            QTest::compare_helper(m_success, "Compared values are not the same", m_val1, m_val2, m_actual.constData(), m_expected.constData(), m_file.constData(), m_line);
        }
    private:
        bool m_success;
        char* m_val1;
        char* m_val2;
        QByteArray m_actual;
        QByteArray m_expected;
        QByteArray m_file;
        int m_line;
};

#define EFAILRUN(function, statement) \
do { \
    try { \
        function; \
        QFAIL("Exception expected:" statement); \
    } catch (const TestException& ex) { \
        QCOMPARE(ex.what(), statement); \
    }\
} while (0);


#define ERUN(function) \
do { \
    try { \
        function; \
    } catch (const TestException& ex) { \
        ex.report(); \
        return;\
    }\
} while (0);

#define EQVERIFY(statement) \
do {\
    if (!(statement))\
        throw TestVerifyException(statement, #statement, "", __FILE__, __LINE__);\
} while (0)

#if 0
#define EQFAIL(message) \
do {\
    QTest::qFail(message, __FILE__, __LINE__);\
        throw TestException(message);\
} while (0)
#endif

#define EQVERIFY2(statement, description) \
do {\
    if (statement) {\
        throw TestVerifyException(true,#statement,(description), __FILE__, __LINE__);\
    }\
} while (0)

namespace ange {
    namespace test {
        template <typename T> inline bool compare(T first, T second) {
            return first == second;
        }
        inline bool compare(float first, float second) {
            return qFuzzyCompare(first,second);
        }
        inline bool compare(double first, double second) {
            return qFuzzyCompare(first,second);
        }
    }
}

#define EQCOMPARE(actual, expected) \
do {\
    if (!(ange::test::compare(actual, expected))) \
        throw TestCompareException(ange::test::compare(actual,expected),QTest::toString(actual), QTest::toString(expected), #actual, #expected, __FILE__, __LINE__);\
} while (0)


#endif // TEST_EXTRAMACROS
