class QWidget;

/**
 * Will register a shortcut that can start and stop the profiler
 */
void enableProfilerShortcut(QWidget* parent);
