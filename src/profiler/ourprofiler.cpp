#include "profiler.h"

#include <QDebug>
#include <QObject>
#include <QShortcut>

#include <gperftools/profiler.h>

class ProfileHelper : public QObject {
    Q_OBJECT
public:
    ProfileHelper(QWidget* parent);
public Q_SLOTS:
    void startStopProfiler();
private:
    bool m_profilerRunning;
    int m_profileNumber;
};

ProfileHelper::ProfileHelper(QWidget* parent)
  : QObject(parent), m_profilerRunning(false), m_profileNumber(0)
{
    QShortcut* shortcut = new QShortcut(parent);
    shortcut->setAutoRepeat(false);
    shortcut->setContext(Qt::ApplicationShortcut);
    shortcut->setKey(Qt::SHIFT + Qt::CTRL + Qt::ALT + Qt::Key_S);
    connect(shortcut, &QShortcut::activated, this, &ProfileHelper::startStopProfiler);
}

void ProfileHelper::startStopProfiler() {
    m_profilerRunning = !m_profilerRunning;
    if (m_profilerRunning) {  // Start
        ++m_profileNumber;
        QString fileName = QString("angelstow-%1.gperf").arg(m_profileNumber);
        qDebug() << "Starting profiler, saving profile in:" << fileName;
        ProfilerStart(fileName.toStdString().c_str());
    } else {  // Stop
        qDebug() << "Stopping profiler";
        ProfilerStop();
    }
}

void enableProfilerShortcut(QWidget* parent) {
    new ProfileHelper(parent);
}

#include "ourprofiler.moc"
