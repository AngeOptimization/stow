#include <QWidget>
#include <QString>
#include <QPushButton>
#include <QFileDialog>
#include <QLineEdit>
#include "fileselectionlineedit.h"

class FileSelectionLineEditPrivate : public QObject {
  Q_OBJECT
  public:
    FileSelectionLineEditPrivate(FileSelectionLineEdit* parent);
    FileSelectionLineEdit* q;
    void setupUi(QWidget*);
    QLineEdit* lineedit;
    QPushButton* button;
    QFileDialog* dialog;
  Q_SIGNALS:
    void changed();
  public Q_SLOTS:
    void showDialog();
};