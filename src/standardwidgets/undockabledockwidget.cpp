#include "undockabledockwidget.h"

#include <QEvent>

//#define DEBUG_UNDOCKABLE_DOCKWIDGET
#ifdef DEBUG_UNDOCKABLE_DOCKWIDGET
#include "event_debug_helper.h"
#include <QVariant>
#include <QPainter>
#endif



UndockableDockWidget::UndockableDockWidget(const QString& title, QWidget* parent, Qt::WindowFlags flags): QDockWidget(title, parent, flags) {
  init();
}

UndockableDockWidget::UndockableDockWidget(QWidget* parent, Qt::WindowFlags flags): QDockWidget(parent, flags) {
  init();
}

void UndockableDockWidget::init() {
  connect(this,SIGNAL(topLevelChanged(bool)),SLOT(stay_undocked_until_clicked(bool)));
  connect(this,SIGNAL(allowedAreasChanged(Qt::DockWidgetAreas)),SLOT(debug_allowed_dock_areas_changed(Qt::DockWidgetAreas)));
  installEventFilter(this);
  if(isFloating()) {
    setAllowedAreas(Qt::NoDockWidgetArea);
  }
}

void UndockableDockWidget::debug_allowed_dock_areas_changed(Qt::DockWidgetAreas a) {
#ifdef DEBUG_UNDOCKABLE_DOCKWIDGET
  qDebug() << "Areas" << a;
#else
  Q_UNUSED(a);
#endif
}



void UndockableDockWidget::stay_undocked_until_clicked(bool toplevel) {
  if(!toplevel) {
    setAllowedAreas(Qt::AllDockWidgetAreas);
  }
}

bool UndockableDockWidget::event(QEvent* event) {
#ifdef DEBUG_UNDOCKABLE_DOCKWIDGET
  if(property("verbose").toBool()) {
      qDebug() << objectName() <<  debug_event_type(event->type()) << allowedAreas();
  }
#endif
  if(event->type()==QEvent::MouseButtonRelease) {
    if(isFloating()) {
      setAllowedAreas(Qt::NoDockWidgetArea);
      return QDockWidget::event(event);
    }
  }
  return QDockWidget::event(event);
}



void UndockableDockWidget::paintEvent(QPaintEvent* event) {
#ifdef DEBUG_UNDOCKABLE_DOCKWIDGET
  QPainter p(this);
  p.fillRect(rect(), Qt::cyan);
#endif
  QDockWidget::paintEvent(event);
}

#include "undockabledockwidget.moc"

