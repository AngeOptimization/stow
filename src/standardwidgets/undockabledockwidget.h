#ifndef UNDOCKABLEDOCKWIDGET_H
#define UNDOCKABLEDOCKWIDGET_H

#include <QDockWidget>


class UndockableDockWidget : public QDockWidget {
  Q_OBJECT
  public:
    UndockableDockWidget(const QString& title, QWidget* parent = 0, Qt::WindowFlags flags = 0);
    UndockableDockWidget(QWidget* parent = 0, Qt::WindowFlags flags = 0);
  protected:
    virtual bool event(QEvent* event);
    virtual void paintEvent(QPaintEvent* event);
  private Q_SLOTS:
    void stay_undocked_until_clicked(bool toplevel);
    void debug_allowed_dock_areas_changed(Qt::DockWidgetAreas a);
    void init();
};

#endif // UNDOCKABLEDOCKWIDGET_H
