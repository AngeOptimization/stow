#include "fileselectionlineedit.h"
#include "fileselectionlineedit_p.h"
#include <QPushButton>
#include <QFileDialog>
#include <QHBoxLayout>

FileSelectionLineEditPrivate::FileSelectionLineEditPrivate(FileSelectionLineEdit* parent) : QObject(parent) {
  q=parent;
  setupUi(parent);
  setObjectName("FileSelectionLineEditPrivate");
}

void FileSelectionLineEditPrivate::setupUi(QWidget* parent ) {
  button = new QPushButton(parent);
  button->setIcon(QIcon(":/icons/icons/document-open.png"));
  lineedit = new QLineEdit(parent);
  lineedit->setMinimumWidth(250);
  dialog = new QFileDialog(parent);
  QHBoxLayout* lay = new QHBoxLayout(parent);
  connect(lineedit,SIGNAL(textChanged(QString)),this,SIGNAL(changed()));
  lay->addWidget(lineedit);
  lay->addWidget(button);
  connect(button,SIGNAL(clicked()),this,SLOT(showDialog()));
}

void FileSelectionLineEditPrivate::showDialog() {
  if(dialog->exec()==QDialog::Accepted) {
    lineedit->setText(dialog->selectedFiles().first());
  }
}

FileSelectionLineEdit::FileSelectionLineEdit(QWidget* parent): QWidget(parent) {
  d=new FileSelectionLineEditPrivate(this);
  connect(d,SIGNAL(changed()),this,SIGNAL(changed()));
}


QString FileSelectionLineEdit::path() const {
  return d->lineedit->text();
}


void FileSelectionLineEdit::setNameFilter(const QString& filter) {
  d->dialog->setNameFilter(filter);
}


void FileSelectionLineEdit::setNameFilters(const QStringList& filters) {
 d->dialog->setNameFilters(filters);
}


QStringList FileSelectionLineEdit::nameFilters() const {
  return d->dialog->nameFilters();
}

void FileSelectionLineEdit::setDirectory(const QString& dir) {
  d->dialog->setDirectory(dir);
}


void FileSelectionLineEdit::setDefaultSuffix(const QString& suffix) {
  d->dialog->setDefaultSuffix(suffix);
}

void FileSelectionLineEdit::setAcceptMode(QFileDialog::AcceptMode acceptmode) {
  d->dialog->setAcceptMode(acceptmode);
}


void FileSelectionLineEdit::setPath(const QString& path) {
  d->lineedit->setText(path);

}
