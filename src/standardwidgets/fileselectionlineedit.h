#ifndef FILESELECTIONLINEEDIT_H
#define FILESELECTIONLINEEDIT_H

#include <QWidget>
#include <QFileDialog>

class FileSelectionLineEditPrivate;

class FileSelectionLineEdit : public QWidget {
  Q_OBJECT
  public:
    FileSelectionLineEdit(QWidget *parent=0);
    QString path() const;

    /*
     * The next functions just forwards directly into the QFIleDialog behind it
     */
    void setNameFilter(const QString& filter);
    void setNameFilters(const QStringList& filters);
    QStringList nameFilters() const;
    void setDirectory(const QString& dir);
    void setDefaultSuffix(const QString& suffix);
    void setAcceptMode (QFileDialog::AcceptMode mode );
  Q_SIGNALS:
    void changed();
  public Q_SLOTS:
    /**
     * Set the value of the line edit
     * @param QString path the new path
     */
    void setPath(const QString& path);
  private:
    FileSelectionLineEditPrivate *d;
};

#endif // FILESELECTIONLINEEDIT_H
